﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Data;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.Pensivo.BusinessLogic;
using System.Data;

using System.Diagnostics;

namespace PensivoUtility
{
    /// <summary>
    /// Define a programe to import data from postgre sql
    /// </summary>
    class Program
    {
        /// <summary>
        /// To define main function to import data
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            OdbcConnection DbConnection = new OdbcConnection(ConfigurationManager.AppSettings["PensivoDBConnection"]);
            try
            {
                DbConnection.Open();
                DataSet ds = new DataSet();
                DataSet dsDeleted = new DataSet();


                OdbcDataAdapter objAdapter = new OdbcDataAdapter("select * from view_tbs_essentials_for_pib  ", DbConnection);
                objAdapter.Fill(ds);

                objAdapter.Dispose();
                objAdapter = new OdbcDataAdapter("select uuid from view_tbs_essentials_for_pib_deleted ", DbConnection);
                objAdapter.Fill(dsDeleted);


                string paths = ConfigurationManager.AppSettings["DataToSave"] + "\\" + "DeletedUserDownloaded" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
                DataSetsToExcel(dsDeleted, paths);
                


                //return; 

                //objAdapter.Dispose();
                //objAdapter = new OdbcDataAdapter("select * from tbs_store_tax_loc ", DbConnection);
                //objAdapter.Fill(dsTBSTaxStorelocation);

                if (!Directory.Exists(ConfigurationManager.AppSettings["DataToSave"]))
                {
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["DataToSave"]);
                }


                //string path = ConfigurationManager.AppSettings["DataToSave"] + "\\" + "UserDownloaded" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
                //DataSetsToExcel(dsDeleted, path);

                string path = ConfigurationManager.AppSettings["DataToSave"] + "\\" + "UserDownloaded" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
                DataSetsToExcel(ds, path);

                ImportPensivoUsers objImport = new ImportPensivoUsers();
                objImport.UpdatePensivoDeletedUser(dsDeleted.Tables[0]);

                objImport = new ImportPensivoUsers();
                objImport.ImportUsers(ds.Tables[0]);
                //objImport.ImportTBS_Tax_Store_location(dsTBSTaxStorelocation.Tables[0]);
                if (SendLogEmail == true)
                {
                    string sMessage = "";
                    //string logPath = ConfigurationManager.AppSettings["LogsDiractory"] + "Log" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                    string logPath = ErrorLogPath + "Log" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

                    if (File.Exists(logPath))
                    {
                        sMessage = "Please find attached Log File";
                    }
                    else
                    {
                        sMessage = "Log file not found";
                        logPath = "";
                    }

                    if (logPath != "")
                    {
                        string[] mailTo = EmailTo.Split(',');
                        foreach (string sMailTo in mailTo)
                        {
                            EmailHelper.SendEmail(EmailFrom, sMailTo, sMessage, EmailSubject, logPath);
                        }
                    }
                    else
                    {
                        string[] mailTo = EmailTo.Split(',');
                        foreach (string sMailTo in mailTo)
                        {
                            EmailHelper.SendEmail(EmailFrom, sMailTo, sMessage, EmailSubject, false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.Message + ex.StackTrace);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                DbConnection.Close();
            }
        }


        /// <summary>
        /// To save dump in execel file data going to be imported
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="filepath"></param>
        public static void DataSetsToExcel(DataSet dataSet, string filepath)
        {
            try
            {
                string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + ";Extended Properties=Excel 12.0 Xml;";
                string tablename = "";
                DataTable dt = new DataTable();
                foreach (System.Data.DataTable dataTable in dataSet.Tables)
                {
                    dt = dataTable;
                    tablename = dataTable.TableName;
                    using (OleDbConnection con = new OleDbConnection(connString))
                    {
                        con.Open();
                        StringBuilder strSQL = new StringBuilder();
                        strSQL.Append("CREATE TABLE ").Append("[" + tablename + "]");
                        strSQL.Append("(");
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            strSQL.Append("[" + dt.Columns[i].ColumnName + "] text,");
                        }
                        strSQL = strSQL.Remove(strSQL.Length - 1, 1);
                        strSQL.Append(")");

                        OleDbCommand cmd = new OleDbCommand(strSQL.ToString(), con);
                        cmd.ExecuteNonQuery();

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strSQL = new StringBuilder();
                            StringBuilder strfield = new StringBuilder();
                            StringBuilder strvalue = new StringBuilder();
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                strfield.Append("[" + dt.Columns[j].ColumnName + "]");
                                strvalue.Append("'" + dt.Rows[i][j].ToString().Replace("'", "''") + "'");
                                if (j != dt.Columns.Count - 1)
                                {
                                    strfield.Append(",");
                                    strvalue.Append(",");
                                }
                                else
                                {
                                }
                            }
                            if (strvalue.ToString().Contains("<br/>"))
                            {
                                strvalue = strvalue.Replace("<br/>", Environment.NewLine);
                            }
                            cmd.CommandText = strSQL.Append(" insert into [" + tablename + "]( ")
                                .Append(strfield.ToString())
                                .Append(") values (").Append(strvalue).Append(")").ToString();
                            cmd.ExecuteNonQuery();
                        }
                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        /// <summary>
        /// To get log file should be mailed or not
        /// </summary>
        public static Boolean SendLogEmail
        {
            get
            {
                if (BusinessUtility.GetString(ConfigurationManager.AppSettings["SendLogEmail"]).ToUpper() == "TRUE")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// To get email to id
        /// </summary>
        public static string EmailTo
        {
            get
            {
                return BusinessUtility.GetString(ConfigurationManager.AppSettings["EmailTo"]);
            }
        }

        /// <summary>
        /// To get email from id
        /// </summary>
        public static string EmailFrom
        {
            get
            {
                return BusinessUtility.GetString(ConfigurationManager.AppSettings["EmailFrom"]);
            }
        }

        /// <summary>
        /// To get mail subject
        /// </summary>
        public static string EmailSubject
        {
            get
            {
                return BusinessUtility.GetString(ConfigurationManager.AppSettings["EmailSubject"]);
            }
        }


        public static string ErrorLogPath
        {
            get
            {
                if (BusinessUtility.GetString(ConfigurationManager.AppSettings["server-type"]).ToUpper() == "call-back".ToUpper())
                {
                    return BusinessUtility.GetString(AppConfig.GetAppConfigValue("LogsDirectory-callback"));
                }
                else
                {
                    return BusinessUtility.GetString(AppConfig.GetAppConfigValue("LogsDirectory"));
                }
            }
        }

    }
}

