﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTECH.Library.Utilities;
using iTECH.Library.Web;
using System.Configuration;
using System.Data.Odbc;
using System.Data;
using System.IO;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;

using System.Net;
using System.Net.Mail;
using System.Globalization;
using System.Threading;
using System.Reflection;
using iTECH.Pensivo.BusinessLogic;


namespace PensivoMsgUtility
{
    class Program
    {
        static void Main(string[] args)
        {
            DbHelper dbHelp = new DbHelper(false);
            try
            {
                bool bReturn = false;

                PensivoMessage objPensivoMessage = new PensivoMessage();

                //DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
                //dbTransactionHelper.BeginTransaction();
                //try
                //{
                StringBuilder strString = new StringBuilder();
                DataTable dt = new DataTable();

                //dt = dbTransactionHelper.GetDataTable("select msgID, smtpUserEmail,msgEmailSubject,msgContent,toEmailID , smtpServer , smtpPort, smtpUserEmail, smtpUserPassword, msgType   from syscommunication where  msgSend = false  and now() >= msgDeliveredDateTime; ", CommandType.Text, null);

                //StringBuilder sQuery = new StringBuilder();
                //sQuery.Append(" select msgID, fromEmailID, smtpUserEmail,msgEmailSubject,msgContent,toEmailID , smtpServer , smtpPort, smtpUserEmail, smtpUserPassword, msgType   from syscommunication where  msgSend = 0  and @createdDateTime >= msgDeliveredDateTime;	");
                //List<MySqlParameter> pList = new List<MySqlParameter>();
                //pList.Add(DbUtility.GetParameter("@createdDateTime", DateTime.Now, MyDbType.DateTime));
                //dt = dbHelp.GetDataTable(BusinessUtility.GetString(sQuery), CommandType.Text, pList.ToArray());

                dt = objPensivoMessage.GetMailRequestedList();

                //WriteLog(Convert.ToString( dt.Rows.Count));
                if (dt.Rows.Count > 0)
                {
                    MailMessage msg = new MailMessage();
                    foreach (DataRow dr in dt.Rows)
                    {
                        msg = new MailMessage();
                        msg.IsBodyHtml = true;
                        msg.From = new MailAddress(Convert.ToString(dr["fromEmailID"]));
                        msg.Subject = Convert.ToString(dr["msgEmailSubject"]);
                        msg.Body = Convert.ToString(dr["msgContent"]);
                        msg.To.Add(new MailAddress(Convert.ToString(dr["toEmailID"]))); //receiver's TO Email Id

                        if (Convert.ToString(dr["msgType"]).Contains("HTML"))
                        {
                            msg.IsBodyHtml = true;
                        }
                        else
                        {
                            msg.IsBodyHtml = false;
                        } 

                        //if (!string.IsNullOrEmpty(emailCCTo))
                        //{
                        //    msg.CC.Add(new MailAddress(emailCCTo)); //Adding CC email Id
                        //} 



                        SmtpClient smtpoj = new SmtpClient();
                        smtpoj.Host = Convert.ToString(dr["smtpServer"]);
                        smtpoj.UseDefaultCredentials = false;
                        smtpoj.Port = BusinessUtility.GetInt(dr["smtpPort"]);
                        smtpoj.EnableSsl = false;
                        smtpoj.Credentials = new System.Net.NetworkCredential(Convert.ToString(dr["smtpUserEmail"]), Convert.ToString(dr["smtpUserPassword"]));
                                                
                        smtpoj.Send(msg);
                        msg.Dispose();
                        smtpoj = null; 

                        // update current record flag
                        //dbHelp = new DbHelper(true);

                        //string sqlString = "update syscommunication set  msgSend = 1, msgSendDateTime =  @sendDateTime  where msgID =  @msgIDNew ";
                        //if (dbHelp.ExecuteNonQuery(sqlString, CommandType.Text, new MySqlParameter[] { 
                        //    DbUtility.GetParameter("@msgIDNew", Convert.ToInt32(dr["msgID"]), MyDbType.Int),
                        //    DbUtility.GetParameter("@sendDateTime", DateTime.Now, MyDbType.DateTime)}) > 0)

                        if(objPensivoMessage.MarkMailSent(Convert.ToInt32(dr["msgID"])))

                        {
                           // WriteLog("Message  Updated" + Convert.ToString(Convert.ToInt32(dr["msgID"])));
                        }
                        else
                        {
                           // WriteLog("Message  Failure");
                        }
                    }
                    //dbTransactionHelper.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                  ErrorLog.createLog(Convert.ToString(ex.ToString()));
                //dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
                //dbTransactionHelper.CloseDatabaseConnection();
            }
            
        }        
        
    }
}
