﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTECH.Library.Utilities; 
using iTECH.Library.Web;
using System.Configuration;
using System.Data.Odbc;
using System.Data;
using System.IO;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;
using iTECH.Pensivo.BusinessLogic;

 
namespace PensivoEmailNotification
{
    class AIEmailNotification
    {
        static void Main(string[] args)
        {
            // Define Connection with pensivo to read users
            //OdbcConnection DbConnection = new OdbcConnection(ConfigurationManager.AppSettings["PensivoDBConnection"]);
            StringBuilder strString = new StringBuilder();
            DbHelper dbHelp = new DbHelper();
            DataTable dt = new DataTable();
            string LocationType = string.Empty;
            StringBuilder bodyString = new StringBuilder();
 
            try
            {
                //strString.Append("select aiemailsender.aiEmailIDs, aiScenario.aiScenarioName,  aiScenario.aiScenarioQuestionIDYes , aiScenario.aiScenarioQuestionIDNo, aiScenario.aiScenarioFormID, TIMESTAMPDIFF(MINUTE, aiScenario.aiScenarioCreatedOn , Now()) as MinutesRemaining  from aiscenario join aiemailsender  on aiscenario.aiScenarioID = aiemailsender.aiEmailScenarioID ");
                //strString.Append("join aiformheader on  aiscenario.aiScenarioFormID =  aiformheader.aiFormID  where  TIMESTAMPDIFF(MINUTE, aiScenario.aiScenarioCreatedOn , Now()) >= @TimeSlot ");
                strString.Append("select *, (select group_concat(aiEmailIGroupIDs) from aiemailgroupmaster ");
                strString.Append("where ID in (aiemailsender.aiEmailGroupCCID)) as CCEMailID, (select group_concat(aiEmailIGroupIDs) from aiemailgroupmaster ");
                strString.Append("where ID in (aiemailsender.aiEmailGroupBCCID)) as BCCEMailID  from  aiemailsender join aiformHeader on   aiemailsender.aiEmailFormID =  aiformHeader.aiFormID");
                strString.Append("    where  TIMESTAMPDIFF(MINUTE, aiemailsender.aiEmailCreatedOn , Now()) >= @TimeSlot ");
                 
                dt = dbHelp.GetDataTable(Convert.ToString(strString), CommandType.Text, new  MySqlParameter []{
                            DbUtility.GetParameter("TimeSlot",  ConfigurationManager.AppSettings["EmailNotificationTime"] , MyDbType.Int)});
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Convert.ToString(dr["aiLocationType"]) == "R")
                        {
                            LocationType = "RETAIL A/I";
                        }
                        else 
                        {
                            LocationType="LOGISTICS A/I";
                        }
                        

                        bodyString.Clear();

                        bodyString.Append("<div>TBS - Site #" + dr["aisiteNo"] + " - <div>");
                        bodyString.Append("<div>Affected employee: " + dr["aiEmpName"] + " (#" + dr["aiaffectedEmpID"] + ")<div>");
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["aiIncidentAccidentDateTime"])))
                        {
                            bodyString.Append("<div>Date of accident/incident:  " + BusinessUtility.GetString(BusinessUtility.GetDateTimeString(Convert.ToDateTime(dr["aiIncidentAccidentDateTime"]), DateFormat.yyyyMMdd)) + " <div>");

                        }
                        else
                        {
                             bodyString.Append("<div>Date of accident/incident:  ");
                        }                        
                        bodyString.Append("<div>Nature of report:    " + dr["aiReportType"] + " <div>");
                        bodyString.Append("<div>Description:   " + dr["aiReportTypeDescription"] + " <div>");
                                                
                        EmailHelper.SendEmail(EmailFrom, Convert.ToString(dr["aiEmailGroupToID"]), Convert.ToString(bodyString), LocationType, true);                          
                        
                       

                        ////String[] strEmailIDS = Convert.ToString(dr["aiEmailIDs"]).Split(',');
                        // foreach (string stringEmailID in strEmailIDS)
                        //{  
                        //EmailHelper.SendEmail(EmailFrom, Convert.ToString(stringEmailID), "Test", "PensivoTest", true );
                        //// EmailHelper.SendEmail(EmailFrom, Convert.ToString(stringEmailID), Convert.ToString(""), EmailSubject, "Test");
                        ////WriteLog("To EmailID  with date and time"); 
                        //}

                       ErrorLog.createLog(Convert.ToString("Success:m " + Convert.ToString(dr["aiEmailGroupToID"])));

                    }
                }
            }
            catch (Exception ex) { ErrorLog.createLog(Convert.ToString(ex.ToString())); }

            
            finally { dbHelp.CloseDatabaseConnection(); }
        }

        public static Boolean SendLogEmail
        {
            get
            {
                if (BusinessUtility.GetString(ConfigurationManager.AppSettings["SendLogEmail"]).ToUpper() == "TRUE")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static string EmailTo
        {
            get
            {
                return BusinessUtility.GetString(ConfigurationManager.AppSettings["EmailTo"]);
            }
        }
        public static string EmailFrom
        {
            get
            {
                return BusinessUtility.GetString(ConfigurationManager.AppSettings["EmailFrom"]);
            }
        }
        public static string EmailSubject
        {
            get
            {
                return BusinessUtility.GetString(ConfigurationManager.AppSettings["EmailSubject"]);
            }
        }
    }
}
