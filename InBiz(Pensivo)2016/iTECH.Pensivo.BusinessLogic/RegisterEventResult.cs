﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;


namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Functionality For Saving XML Data in Table 
    /// </summary>
    public class RegisterEventResult
    {
        /// <summary>
        /// Get/Set Registration ID
        /// </summary>
        public int RegID { get; set; }

        /// <summary>
        /// Get/Set Course Status
        /// </summary>
        public string CourseStatus { get; set; }

        /// <summary>
        /// Get/Set Course Attempts
        /// </summary>
        public string CourseAttempts { get; set; }

        /// <summary>
        /// Get/Set Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Get/Set Completed
        /// </summary>
        public string Completed { get; set; }

        /// <summary>
        /// Get/Set Score
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// To Get/Set Total Time
        /// </summary>
        public string TotalTime { get; set; }

        /// <summary>
        /// To Get/Set Location
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// To Get/Set Progress Status
        /// </summary>
        public string ProgressStatus { get; set; }

        /// <summary>
        /// To Get/Set XML Post Type
        /// </summary>
        public string xmlPostType { get; set; }

        /// <summary>
        /// To Get/Set Result ID
        /// </summary>
        public int ResultID { get; set; }

        /// <summary>
        /// To Get/Set XML Parsed ID
        /// </summary>
        public int XMLParsedID { get; set; }

        /// <summary>
        /// To Save Course Result and Details in trainingeventresistrationresult Table
        /// </summary>
        /// <param name="dbTransactionHelper">Pass DB Transaction Connection</param>
        /// <returns>True/False</returns>
        public Boolean SaveRegistrationResult(DbTransactionHelper dbTransactionHelper)
        {
            StringBuilder sbInsertQuery = new StringBuilder();
            sbInsertQuery.Append(" INSERT INTO trainingeventresistrationresult ");
            sbInsertQuery.Append(" (regID, xmlparsedid, createdDateTime, course_status, course_attempts, title, completed, totaltime, location, progressstatus, postType, score_raw) ");
            sbInsertQuery.Append(" VALUES(@regID, @xmlparsedid, @createdDateTime, @course_status, @course_attempts, @title, @completed, @totaltime, @location, @progressstatus, @postType, @score_raw) ");

            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
            DbUtility.GetParameter("regID", this.RegID, MyDbType.Int),    
            DbUtility.GetParameter("xmlparsedid", this.XMLParsedID, MyDbType.Int), 
            DbUtility.GetParameter("createdDateTime", DateTime.Now , MyDbType.DateTime),
            DbUtility.GetParameter("course_status", this.CourseStatus , MyDbType.String),
            DbUtility.GetParameter("course_attempts", this.CourseAttempts, MyDbType.String),    
            DbUtility.GetParameter("title", this.Title , MyDbType.String),
            DbUtility.GetParameter("completed", this.Completed , MyDbType.String),
            DbUtility.GetParameter("totaltime", this.TotalTime, MyDbType.String),    
            DbUtility.GetParameter("location", this.Location , MyDbType.String),
            DbUtility.GetParameter("progressstatus", this.ProgressStatus , MyDbType.String),
            DbUtility.GetParameter("postType", this.xmlPostType , MyDbType.String),
            DbUtility.GetParameter("score_raw", this.Score, MyDbType.String), 
            });

            object obj = dbTransactionHelper.GetLastInsertID();
            this.ResultID = BusinessUtility.GetInt(obj);
            return true;
        }

        /// <summary>
        /// To Save Parsed XML Detail into trainingeventxmlparsed Table
        /// </summary>
        /// <param name="dbTransactionHelper">Pass DB Transaction Connection</param>
        /// <returns>True/False</returns>
        public Boolean SaveXMLParsed(DbTransactionHelper dbTransactionHelper)
        {
            StringBuilder sbInsertQuery = new StringBuilder();
            sbInsertQuery.Append(" INSERT INTO trainingeventxmlparsed ");
            sbInsertQuery.Append(" (regid, createddatetime, coursestatus, posttype) ");
            sbInsertQuery.Append(" VALUES(@regid, @createddatetime, @coursestatus, @posttype) ");

            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
            DbUtility.GetParameter("regID", this.RegID, MyDbType.Int),    
            DbUtility.GetParameter("createddatetime", DateTime.Now , MyDbType.DateTime),
            DbUtility.GetParameter("coursestatus", this.CourseStatus , MyDbType.String),
            DbUtility.GetParameter("posttype", this.xmlPostType , MyDbType.String),
            });

            object obj = dbTransactionHelper.GetLastInsertID();
            this.XMLParsedID = BusinessUtility.GetInt(obj);
            return true;
        }

        /// <summary>
        /// To Save XML Interactions in traininginteractions Table
        /// </summary>
        /// <param name="dbTransactionHelper">Pass DB Transaction Connection</param>
        /// <param name="resultID">Pass Result ID</param>
        /// <param name="learnerResponse">Pass Learner Response</param>
        /// <param name="result">Pass Learner Result</param>
        /// <param name="responseID">Pass Learner Response ID</param>
        /// <param name="description">Pass Learner Question Description</param>
        /// <returns>True/False</returns>
        public Boolean SaveRegistrationInteractions(DbTransactionHelper dbTransactionHelper, int resultID, string learnerResponse, string result, string responseID, string description)
        {
            StringBuilder sbInsertQuery = new StringBuilder();
            sbInsertQuery.Append(" INSERT INTO traininginteractions ");
            sbInsertQuery.Append(" (training_result_id, learner_response, result, response, description) ");
            sbInsertQuery.Append(" VALUES(@training_result_id, @learner_response, @result, @response, @description) ");

            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
            DbUtility.GetParameter("training_result_id", resultID, MyDbType.Int),    
            DbUtility.GetParameter("learner_response", learnerResponse , MyDbType.String),
            DbUtility.GetParameter("result", result, MyDbType.String),    
            DbUtility.GetParameter("response", responseID, MyDbType.String),
            DbUtility.GetParameter("description", description, MyDbType.String)
            });

            object obj = dbTransactionHelper.GetLastInsertID();
            return true;
        }

        /// <summary>
        /// To Delete Registration Interactions From traininginteractions Table
        /// </summary>
        /// <param name="dbTransactionHelper">Pass DB Transaction Connection</param>
        /// <param name="regID">Pass Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean DeleteRegistrationInteractions(DbTransactionHelper dbTransactionHelper, int regID)
        {
            StringBuilder sbInsertQuery = new StringBuilder();
            sbInsertQuery.Append(" delete traininginteractions FROM traininginteractions join  trainingeventresistrationresult on training_result_id =  idtrainingeventresistrationresult and regID = @regID  ");
            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
            DbUtility.GetParameter("regID", regID, MyDbType.Int)
            });
            object obj = dbTransactionHelper.GetLastInsertID();
            return true;
        }

        /// <summary>
        /// To Delete Registration Result From traininginteractions and trainingeventresistrationresult Table
        /// </summary>
        /// <param name="dbTransactionHelper">Pass DB Transaction Connection</param>
        /// <param name="regID">Pass Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean DeleteRegistrationResult(DbTransactionHelper dbTransactionHelper, int regID)
        {
            bool rValue = false;
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" delete traininginteractions FROM traininginteractions join  trainingeventresistrationresult on training_result_id =  idtrainingeventresistrationresult and regID = @regID;  ");
                sbInsertQuery.Append(" Delete FROM trainingeventresistrationresult Where regID = @regID  ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
            DbUtility.GetParameter("regID", regID, MyDbType.Int)
            });
                object obj = dbTransactionHelper.GetLastInsertID();
                rValue = true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            return rValue;
        }

        /// <summary>
        /// To Check if Registration ID Passed is Test Registration ID or Not From employeecourseregistration and traningevent Table
        /// </summary>
        /// <param name="regID">Pass Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean IsRegTestCourse(int regID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select cReg.idRegistration from employeecourseregistration as cReg ");
                sbInsertQuery.Append(" inner join traningevent	tevent on tevent.courseHdrID = cReg.courseHeader_courseHdrID  ");
                sbInsertQuery.Append(" and tevent.TestType ='TO' and cReg.idRegistration=@regID ");

                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("regID", regID, MyDbType.Int)
                });
                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Check if "G" Type Request is Completed or Not From trainingeventxmlparsed Table
        /// </summary>
        /// <param name="regID">Pass Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean IsGTypeRequestCompleted(int regID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select idtrainingeventxmlparsed from trainingeventxmlparsed where regid=@regID and coursestatus ='true' and posttype='G' ");
                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("regID", regID, MyDbType.Int)
                });
                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Last Test Interaction ID From trainingeventresistrationresult Table
        /// </summary>
        /// <param name="testRegistrationID">Pass Test Registration ID</param>
        /// <returns>int</returns>
        public Int64 GetLastTestIntrecationID(int testRegistrationID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select idtrainingeventresistrationresult  ");
                sbInsertQuery.Append(" from trainingeventresistrationresult   ");
                sbInsertQuery.Append(" where regID = @testRegistrationID order by idtrainingeventresistrationresult desc limit 1 ");

                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("testRegistrationID", testRegistrationID, MyDbType.Int)
                });

                return BusinessUtility.GetLong(rValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// Get Test Interaction Detail From traininginteractions Table
        /// </summary>
        /// <param name="testInteractionID">Pass Test Interation ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetTestIntrecationDetails(Int64 testInteractionID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT  ");
                sbInsertQuery.Append(" * ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" (SELECT  ");
                sbInsertQuery.Append(" * ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" traininginteractions ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" training_result_id = @testInteractionID ");
                sbInsertQuery.Append(" ORDER BY idtraininginteractions DESC) AS inq ");
                sbInsertQuery.Append(" GROUP BY inq.description ");
                sbInsertQuery.Append(" ORDER BY inq.idtraininginteractions ");
                return dbHelp.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("testInteractionID", testInteractionID, MyDbType.Double)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        public Boolean SaveEmployeeCourseRepeation(DbTransactionHelper dbTransactionHelper, int courseEmpHdrID, int courseID, int courseVer, int daysAfterRepeat)
        {
            bool rValue = false;
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Insert into empassignedcourse (CourseEmpHdrID, CourseID, CourseVer, recordActive, startdatetime ) ");
                sbInsertQuery.Append(" values (@courseEmpHdrID, @courseID, @courseVer, @recordActive, @startdatetime ) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("courseEmpHdrID", courseEmpHdrID, MyDbType.Int),    
                DbUtility.GetParameter("courseID", courseID , MyDbType.Int),
                DbUtility.GetParameter("courseVer", courseVer, MyDbType.Int),    
                DbUtility.GetParameter("recordActive", 1, MyDbType.Int),
                DbUtility.GetParameter("repeatedDays", daysAfterRepeat, MyDbType.Int)
                });
                object obj = dbTransactionHelper.GetLastInsertID();
                rValue = true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            return rValue;
        }




    }
}
