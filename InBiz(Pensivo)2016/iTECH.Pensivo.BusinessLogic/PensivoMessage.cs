﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Web;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Functionality For Pensivo Message
    /// </summary>
    public class PensivoMessage
    {
        /// <summary>
        /// To Get Requested Mail List
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetMailRequestedList()
        {
            DataTable dt = new DataTable();
            DbHelper dbHelp = new DbHelper(false);
            try
            {
                StringBuilder sQuery = new StringBuilder();
                sQuery.Append(" select msgID, fromEmailID, smtpUserEmail,msgEmailSubject,msgContent,toEmailID , smtpServer , smtpPort, smtpUserEmail, smtpUserPassword, msgType   from syscommunication where  msgSend = 0  and @createdDateTime >= msgDeliveredDateTime;	");
                List<MySqlParameter> pList = new List<MySqlParameter>();
                pList.Add(DbUtility.GetParameter("@createdDateTime", DateTime.Now, MyDbType.DateTime));
                dt = dbHelp.GetDataTable(BusinessUtility.GetString(sQuery), CommandType.Text, pList.ToArray());
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dt;
        }

        /// <summary>
        /// To Sent Mail Sent 
        /// </summary>
        /// <param name="messageID">Pass Message ID</param>
        /// <returns>True/False</returns>
        public bool MarkMailSent(int messageID)
        {
            bool rValue = false;
            DataTable dt = new DataTable();
            DbHelper dbHelp = new DbHelper(false);
            try
            {
                string sqlString = "update syscommunication set  msgSend = 1, msgSendDateTime =  @sendDateTime  where msgID =  @msgIDNew ";
                dbHelp.ExecuteNonQuery(sqlString, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("@msgIDNew", messageID, MyDbType.Int),
                            DbUtility.GetParameter("@sendDateTime", DateTime.Now, MyDbType.DateTime)});

                rValue = true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return rValue;

        }
    }
}
