﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;

using iTECH.Library.Utilities;
using System.Data;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using System.Xml;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define App Setting
    /// </summary>
    public static class AppConfig
    {
        /// <summary>
        /// To Get App Key Value
        /// </summary>
        /// <param name="appKey">Pass App Key</param>
        /// <returns>String</returns>
        public static string GetAppConfigValue(string appKey)
        {
            DbHelper dbHelper = new DbHelper(false);
            string sReturn = string.Empty;
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append("select value from appconfig where appkey= @appkey ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("appkey",  appKey, MyDbType.String),
                });
                sReturn = BusinessUtility.GetString(rValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return sReturn;
        }
    }
}
