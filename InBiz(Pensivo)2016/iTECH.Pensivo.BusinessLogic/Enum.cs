﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Course Status
    /// </summary>
    public enum CourseStatus
    {
        Traning_Employee_Taking = 1,
        Traning_Employee_must_take = 2,
        Traning_Employee_should_take = 3,
        Traning_Employee_can_take = 4,
        Traning_Employee_have_taken = 5
    }

    /// <summary>
    /// To Define Culture Languages
    /// </summary>
    public struct AppCulture
    {
        public const string ENGLISH_CANADA = "en-CA";
        public const string FRENCH_CANADA = "fr-CA";
        public const string DEFAULT = "en-CA";
    }

    /// <summary>
    /// To Define Culture Languages Code 
    /// </summary>
    public struct AppLanguageCode
    {
        public const string EN = "en";
        public const string FR = "fr";
        public const string DEFAULT = "en";
    }

    /// <summary>
    /// To Define Course Activity 
    /// </summary>
    public struct CourseAction
    {
        public const string CourseLaunch = "Launch";
        public const string CourseLaunchTest = "LaunchTest";
        public const string CourseDelete = "Delete";
        public const string ProgressStatus = "ProgressStatus";
        public const string DownloadTest = "DownloadTest";
        public const string ReviewCourseContent = "ReviewCourseContent";
    }

    /// <summary>
    /// To Define User Reference Type
    /// </summary>
    public struct EmpRefCode
    {
        public const string AccountType = "ACT";
        public const string Department = "DPT";
        public const string District = "TDIST";
        public const string JobCode = "JOB";
        public const string Region = "REG";
        public const string RegionTBS = "TREGN";
        public const string Store = "STR";
        public const string Division = "DIV";
        public const string DivisionTBS = "TDIVN";
        public const string Type = "TYP";
        public const string TypeParsed = "TPP";
        public const string Site = "SITE";
        public const string CSite = "CSITE";
        public const string Province = "PABBR";
    }

    /// <summary>
    /// To Define User Type
    /// </summary>
    public struct EmpType
    {
        public const string Employee = "E";
        public const string SuperAdmin = "S";
    }

    /// <summary>
    /// To Define Error Code When Importing User From Excel
    /// </summary>
    public enum EmpImportErrCode
    {
        InvalidEmpCode = 401,
        DuplicateEmpCode = 402,
        UnkownError = 403,
    }


    /// <summary>
    /// To Define Role Activity
    /// </summary>
    public enum RoleUserActivity
    {
        Remove = 0,
        Add = 1,
    }

    /// <summary>
    /// To Define User Search Codes
    /// </summary>
    public struct EmpSearchBy
    {
        public const string Name = "NAM";
        public const string EmpCode = "ECD";
        public const string Department = "DPT";
        public const string District = "TDIST";
        public const string JobCode = "JOB";
        public const string Region = "REG";
        public const string RegionTBS = "TREGN";
        public const string Store = "STR";
        public const string Division = "DIV";
        public const string DivisionTBS = "TDIVN";
        public const string Type = "TYP";
        public const string TypeParsed = "TPP";
        public const string Site = "SITE";
        public const string SiteLogisticsReport = "SLR";
        public const string CSite = "CSITE";
        public const string Province = "PABBR";
    }



    /// <summary>
    /// To Define Role Types
    /// </summary>
    public struct RoleType
    {
        public const string Menu = "M";
        public const string System = "S";
    }

    /// <summary>
    /// To Define AI Report Type
    /// </summary>
    public struct AIReportType
    {
        public const string WVHOnly = "WVHO";
        public const string WVHConditional = "WVHC";
        public const string Incident = "INCD";
        public const string AI = "AION";
    }

    /// <summary>
    /// To Define Role Functionality Types
    /// </summary>
    public enum RoleAction
    {
        Administration = 3,
        Reporting = 4,
        AI_Report_Initiation = 1,
        AI_Reporting = 2,
        Beer_College = 5,
        Store_Manager = 6,
        Admin_Reporting = 7,
        Assign_Training = 8,
        AI_2015 = 9,
        Manage_List = 10
        //Reset_User = 11
    }

    /// <summary>
    /// To Define Report Options
    /// </summary>
    public struct ReportOption
    {
        public const string Who = "Who";
        public const string Which = "Which";
        public const string When = "When";
        public const string What = "What";
    }

    /// <summary>
    /// To Define Report Filter Options
    /// </summary>
    public struct ReportFilterOption
    {
        public const string ByList = "LST";
        public const string ByEventName = "NAM";
        public const string ByCategory = "CAT";
    }

    /// <summary>
    /// To Define Report Filter According to Course Progress Status
    /// </summary>
    public struct ReportEventProgressStatus
    {
        public const string All_Course_Progress = "ALL";
        public const string Course_Once_Completed = "OCM";
        public const string Course_Not_Once_Completed = "NCM";
        public const string Course_Not_Once_Started = "NST";
        public const string Only_Those_Not_Completed_Training = "OTNCT";
    }

    /// <summary>
    /// To Define Report Date Filter Options
    /// </summary>
    public struct ReportDateFilterOption
    {
        public const string CurrentMonth = "CMT";
        public const string CurrentQuarter = "CQD";
        public const string CurrentYearToDate = "CYR";
        public const string SpecficDate = "SDT";
    }

    /// <summary>
    /// To Define Report Date Filter 
    /// </summary>
    public struct ReportDateFilter
    {
        public const string IncludeSpecificDate = "ISD";
        public const string BetweenTwoDates = "BTD";
    }

    /// <summary>
    /// To Define Course Type
    /// </summary>
    public struct CourseTestType
    {
        public const string CourseOnly = "CO";
        public const string CourseTest = "CT";
        public const string TestOnly = "TO";
    }

    /// <summary>
    /// To Define Pensivo User Creation Source
    /// </summary>
    public struct UserCreatedSource
    {
        public const string Pensivo = "PENIS";
        public const string PostGreySQL = "PGSQL";
        public const string PensivoNew = "PENNE";
        public const string Import = "IMPRT";
        public const string New = "NEW";
        public const string SignIn = "SIGNIN";
    }

    /// <summary>
    /// To Define Scorm Course Type
    /// </summary>
    public struct ScormType
    {
        public const string Regular = "REG";
        public const string SingleSCO = "SIG";
    }

    /// <summary>
    /// To Define Pensivo Standalone Authentication Destination
    /// </summary>
    public enum StandAloneAuthenticationDestination
    {
        beercollege = 1,
        ai = 2,
        aidash = 3,
        storemgr = 4,
        ai2015 = 5
    }

    /// <summary>
    /// To Define Pensivo Standalone Authentication Destination Type
    /// </summary>
    public struct StandAloneAuthenticationDestinationType
    {
        public const string BeerCollege = "beercollege";
        public const string AI = "ai";
        public const string AiDashboard = "aidash";
        public const string StoreManager = "storemgr";
        public const string AI2015 = "ai2015";
    }

    /// <summary>
    /// To Define System Role selection operator
    /// </summary>
    public struct SysRoleSelOperator
    {
        public const string AND = "AND";
        public const string OR = "OR";
    }

    /// <summary>
    /// To Define User Action ID
    /// </summary>
    public enum UserTrackingActionItemID
    {
        SignedInToPIB = 1,
        SignedOutofPIB = 2,
        TransferredToRustici = 3,
        ReturnedToPibFromRustici = 4,
        TransferredToOldSystem = 5,
        ReturnToPibFromOldSystem = 6,
    }

    /// <summary>
    /// To Define Rustici Registration Status Code
    /// </summary>
    public enum RusticRegStatusCode
    {
        Sucess = 1,
        Failure = 2,
        None = 0,
    }

    /// <summary>
    /// To Define AI Report Question Type
    /// </summary>
    public struct QuestionType
    {
        public const string TextType = "Text";
        public const string IntegerType = "Integer";
        public const string BooleanType = "Boolean";
    }

    /// <summary>
    /// To Define AI Report Sections
    /// </summary>
    public struct PageName
    {
        public const string Location = "Location";
        public const string IncidentConfirmation = "IncidentConfirmation";
        public const string WVHGeneralPersonalInfo = "WVHGeneralPersonalInfo";
        public const string PropertyDamagedDetail = "PropertyDamagedDetail";
        public const string PropertyDamageQuestion = "PropertyDamageQuestion";
        public const string PersonalInjury = "Personal Injury";
        public const string LostTime = "LostTime";
        public const string LicenseeDetail = "LicenseeDetail";
        public const string InjuryIllness = "InjuryIllness";
        public const string IncidentDetail = "IncidentDetail";
        public const string Incident = "Incident";
        public const string HealthCareDetail = "HealthCareDetail";
        public const string HealthCareConfirmation = "HealthCareConfirmation";
        public const string FirstAidBox = "FirstAidBox";
        public const string WVH = "WVH";
        public const string GeneralInformation = "GeneralInformation";
        public const string GetGeneralInformation = "GetGeneralInformation";
        public const string GetEmpGeneralInformation = "GetEmpGeneralInformation";
        public const string IncidentAccidentDetail = "IncidentAccidentDetail";
        public const string IncidentAccidentDetail1 = "IncidentAccidentDetail1";
        public const string IncidentAccidentDetail2 = "IncidentAccidentDetail2";
        public const string IncidentAccidentDetail3 = "IncidentAccidentDetail3";
        public const string IncidentAccidentDetail4 = "IncidentAccidentDetail4";
        public const string IncidentAccidentDetail5 = "IncidentAccidentDetail5";
        public const string IncidentAccidentDetail6 = "IncidentAccidentDetail6";
        public const string IncidentAccidentDetail7 = "IncidentAccidentDetail7";
        public const string AccidentIncedentDescDetail = "AccidentIncedentDescDetail";
        public const string ContributingFactor = "ContributingFactor";
        public const string ActionPreventionForm = "ActionPreventionForm";
        public const string ActionPreventionConfirmation = "ActionPreventionConfirmation";
        public const string ActionPreventionCorrection = "ActionPreventionCorrection";
        public const string WitnessCoworkersForm = "WitnessCoworkersForm";
        public const string WorkersCommentsForm = "WorkersCommentsForm";
        public const string PeopleInvolved = "PeopleInvolved";
        public const string DistCenterConfirm = "DistCenterConfirm";
        public const string CauseofInjury = "CauseofInjury";
        public const string NatureofInjury = "NatureofInjury";
        public const string PersonalProtectiveEquipment = "PersonalProtectiveEquipment";
        public const string ReturnToWork = "ReturnToWork";
        public const string ReturnToWork1 = "ReturnToWork1";
        public const string IsReatilLogistic = "IsReatilLogistic";
        public const string IsLogistic = "IsLogistic";
        public const string WVHComments = "WVHComments";
        public const string WVHDescription = "WVHDescription";
        public const string WVHEventReportedBy = "WVHEventReportedBy";
        public const string WVHOffenders = "WVHOffenders";
        public const string WVHEventOccurrence = "WVHEventOccurrence";
        public const string WVHEmergencyResponse = "WVHEmergencyResponse";
        public const string WVHEmployeesPresent = "WVHEmployeesPresent";
        public const string WVHFollowUp = "WVHFollowUp";
        public const string WVHManagerSection = "WVHManagerSection";
        public const string WVHSupervisorSection = "WVHSupervisorSection";
    }

    /// <summary>
    /// To Define AI Report Question Values
    /// </summary>
    public struct ConfirmationYesNo
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotAnswered = "2";
    }

    /// <summary>
    /// To Define AI Report Status
    /// </summary>
    public struct AIReportStatus
    {
        public const string AIISCompletedYes = "1";
        public const string AIISCompletedNo = "0";
        public const string AIISDeletedYes = "1";
        public const string AIISDeletedNo = "0";
        public const string AIISReOpenYes = "1";
        public const string AIISReOpenNo = "0";
    }

    /// <summary>
    /// To Define AI Report Scenarios
    /// </summary>
    public struct AIScenarioName
    {
        public const string Incident = "Incident";
        public const string PersonalInjury = "Personal Injury";
        public const string PersonalInjuryANDFirstAid = "Personal Injury AND First Aid";
        public const string PersonalInjuryANDFirstAidANDPropertyDamage = "Personal Injury AND First Aid AND Property Damage";
        public const string PersonalInjuryANDFirstAidANDPropertyDamageANDLostTime = "Personal Injury AND First Aid AND Property Damage AND Lost Time";
        public const string PersonalInjuryANDFirstAidANDLostTime = "Personal Injury AND First Aid AND Lost Time";
        public const string PersonalInjuryANDFirstAidANDHealthCare = "Personal Injury AND First Aid AND Health Care";
        public const string PersonalInjuryANDFirstAidANDHealthCareANDPropertyDamage = "Personal Injury AND First Aid AND Health Care AND Property Damage";
        public const string PersonalInjuryANDFirstAidANDHealthCareANDPropertyDamageANDLostTime = "Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time";
        public const string PersonalInjuryANDFirstAidANDHealthCareANDLostTime = "Personal Injury AND First Aid AND Health Care AND Lost Time";
        public const string PersonalInjuryANDHealthCare = "Personal Injury AND Health Care";
        public const string PersonalInjuryANDHealthCareANDPropertyDamage = "Personal Injury AND Health Care AND Property Damage";
        public const string PersonalInjuryANDHealthCareANDPropertyDamageANDLostTime = "Personal Injury AND Health Care AND Property Damage AND Lost Time";
        public const string PersonalInjuryANDHealthCareANDLostTime = "Personal Injury AND Health Care AND Lost Time";
        public const string PersonalInjuryANDFirstAidANDWVH = "Personal Injury AND First Aid AND WVH";
        public const string PersonalInjuryANDFirstAidANDPropertyDamageANDWVH = "Personal Injury AND First Aid AND Property Damage AND WVH";
        public const string PersonalInjuryANDFirstAidANDPropertyDamageANDLostTimeANDWVH = "Personal Injury AND First Aid AND Property Damage AND Lost Time AND WVH";
        public const string PersonalInjuryANDFirstAidANDLostTimeANDWVH = "Personal Injury AND First Aid AND Lost Time AND WVH";
        public const string PersonalInjuryANDFirstAidANDHealthCareANDWVH = "Personal Injury AND First Aid AND Health Care AND WVH";
        public const string PersonalInjuryANDFirstAidANDHealthCareANDPropertyDamageANDWVH = "Personal Injury AND First Aid AND Health Care AND Property Damage AND WVH";
        public const string PersonalInjuryANDFirstAidANDHealthCareANDPropertyDamageANDLostTimeANDWVH = "Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time AND WVH";
        public const string PersonalInjuryANDFirstAidANDHealthCareANDLostTimeANDWVH = "Personal Injury AND First Aid AND Health Care AND Lost Time AND WVH";
        public const string PersonalInjuryANDHealthCareANDWVH = "Personal Injury AND Health Care AND WVH";
        public const string PersonalInjuryANDHealthCareANDPropertyDamageANDWVH = "Personal Injury AND Health Care AND Property Damage AND WVH";
        public const string PersonalInjuryANDHealthCareANDPropertyDamageANDLostTimeANDWVH = "Personal Injury AND Health Care AND Property Damage AND Lost Time AND WVH";
        public const string PersonalInjuryANDHealthCareANDLostTimeANDWVH = "Personal Injury AND Health Care AND Lost Time AND WVH";
        public const string PropertyDamage = "Property Damage";
        public const string PropertyDamageANDLostTime = "Property Damage AND Lost Time";
        public const string LostTime = "Lost Time";
        public const string PropertyDamageANDWVH = "Property Damage  AND WVH";
        public const string PropertyDamageANDLostTimeANDWVH = "Property Damage AND Lost Time AND WVH";
        public const string LostTimeANDWVH = "Lost Time AND WVH";

    }

    /// <summary>
    /// To Define Application Instance 
    /// </summary>
    public enum Institute
    {
        beercollege = 1,
        bdl = 2,
        tdc = 3,
        navcanada = 4,
        AlMurrayDentistry = 5,
        EDE2 = 6,
        //Feedback = 7,
    }

    public struct TrainingRepeatTriggerType
    {
        public const string FixedDate = "F";
        public const string RelativeNoDaysFromStartDate = "S";
        public const string RelativeNoDaysFromCompleteDate = "C";
        public const string RepeatFromEligibleDate = "E";
    }

    public enum AssignedCourseInActiveReason
    {
        BySysAdminRole = 1,
        ByUserProfileChange = 2,
        ByPensivoUtility = 3,
        ByItechWithSQL =4
    }

    public enum AssignedCourseUpdatedSource
    {
        BySysAdminRoleUpdate = 1,
        ByAssignCourseRoleUpdate = 2,
        ByPensivoUtility = 3,
        ByItechWithSQL =4,
        ByUserProfileChange =5
    }

}
