﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Web;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Report Functionality
    /// </summary>
    public class Report
    {
        /// <summary>
        /// To get report data based on different conditions by Report Filter
        /// </summary>
        /// <param name="lang">Pass Language Code</param>
        /// <param name="applicationType">Pass Application Type</param>
        /// <param name="isSiteReporting">Pass IsSiteReporting</param>
        /// <returns>DataTable</returns>
        public DataTable GetReport(string lang, int applicationType, bool isSiteReporting, string sTextYes, string sTextNo, string sTextNotStatrted)
        {
            StringBuilder sbQuery = new StringBuilder();
            StringBuilder sbReportFilter = new StringBuilder();
            DataTable dtReport = new DataTable();

            #region CreateReportFilters
            ReportFilter objReportFilter = new ReportFilter();
            List<ReportFilter> reportFilterList = objReportFilter.GetReportFilter().OrderBy(o => o.ReportOption).ToList();
            if (reportFilterList != null && reportFilterList.Count > 0)
            {
                foreach (var filterItem in reportFilterList)
                {
                    if (filterItem.ReportOption.ToLower() == ReportOption.Who.ToLower())
                    {
                        if (filterItem.SearchBy.ToLower() == EmpSearchBy.Name.ToLower())
                        {
                            sbReportFilter.Append(" AND empHdr.empHdrID IN (" + filterItem.SearchValue + ") ");
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.EmpCode.ToLower())
                        {
                            sbReportFilter.Append(" AND empHdr.empExtID = '" + filterItem.SearchValue + "' ");
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Store.ToLower())
                        {
                            //sbReportFilter.Append(" AND iqSite.empSiteID IN ( ");
                            sbReportFilter.Append(" AND iqCSite.empSiteID IN ( ");
                            string[] sSearchValue = filterItem.SearchValue.Split(',');
                            int i = 0;

                            string empSiteID = "";

                            foreach (string sValue in sSearchValue)
                            {
                                if (empSiteID == "")
                                {
                                    empSiteID += "'" + sValue + "'";
                                }
                                else
                                {
                                    empSiteID += "," + "'" + sValue + "'";
                                }
                                i += 1;
                            }
                            sbReportFilter.Append(" " + empSiteID + "  ) ");
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Region.ToLower())
                        {
                            sbReportFilter.Append(" AND iRegion.empRegID IN ( ");
                            string[] sSearchValue = filterItem.SearchValue.Split(',');
                            int i = 0;

                            string empRegID = "";

                            foreach (string sValue in sSearchValue)
                            {
                                if (empRegID == "")
                                {
                                    empRegID += "'" + sValue + "'";
                                }
                                else
                                {
                                    empRegID += "," + "'" + sValue + "'";
                                }
                                i += 1;
                            }
                            sbReportFilter.Append(" " + empRegID + " ) ");
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Division.ToLower())
                        {
                            if (filterItem.SearchBy.ToLower() == EmpSearchBy.Division.ToLower() && filterItem.SearchValue.ToLower() == "All Users".ToLower())
                            {
                            }
                            else
                            {
                                sbReportFilter.Append(" AND iqLocation.empLocTypeValue IN ( ");
                                string[] sSearchValue = filterItem.SearchValue.Split(',');
                                int i = 0;

                                string empLocType = "";

                                foreach (string sValue in sSearchValue)
                                {
                                    if (empLocType == "")
                                    {
                                        empLocType += "'" + sValue + "'";
                                    }
                                    else
                                    {
                                        empLocType += "," + "'" + sValue + "'";
                                    }
                                    i += 1;
                                }
                                sbReportFilter.Append(" " + empLocType + "  ) ");
                            }
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.JobCode.ToLower())
                        {
                            sbReportFilter.Append(" AND iJobCode.empJobID IN ( ");
                            string[] sSearchValue = filterItem.SearchValue.Split(',');
                            int i = 0;

                            string empJobID = "";

                            foreach (string sValue in sSearchValue)
                            {
                                if (empJobID == "")
                                {
                                    empJobID += "'" + sValue + "'";
                                }
                                else
                                {
                                    empJobID += "," + "'" + sValue + "'";
                                }
                                i += 1;
                            }
                            sbReportFilter.Append(" " + empJobID + "  ) ");
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Type.ToLower())
                        {
                            sbReportFilter.Append(" AND iempType.empUnionType IN ( ");
                            string[] sSearchValue = filterItem.SearchValue.Split(',');
                            int i = 0;

                            string empUnionType = "";

                            foreach (string sValue in sSearchValue)
                            {
                                if (empUnionType == "")
                                {
                                    empUnionType += "'" + sValue + "'";
                                }
                                else
                                {
                                    empUnionType += "," + "'" + sValue + "'";
                                }
                                i += 1;
                            }
                            sbReportFilter.Append(" " + empUnionType + " ) ");
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.DivisionTBS.ToLower())
                        {
                            sbReportFilter.Append(" AND iDivision.empDivID IN ( ");
                            string[] sSearchValue = filterItem.SearchValue.Split(',');
                            int i = 0;

                            string empDivID = "";

                            foreach (string sValue in sSearchValue)
                            {
                                if (empDivID == "")
                                {
                                    empDivID += "'" + sValue + "'";
                                }
                                else
                                {
                                    empDivID += "," + "'" + sValue + "'";
                                }
                                i += 1;
                            }
                            sbReportFilter.Append(" " + empDivID + " ) ");
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.RegionTBS.ToLower())
                        {

                            sbReportFilter.Append(" AND iRegion.empRegID IN ( ");
                            string[] sSearchValue = filterItem.SearchValue.Split(',');
                            int i = 0;

                            string empRegID = "";

                            foreach (string sValue in sSearchValue)
                            {
                                if (empRegID == "")
                                {
                                    empRegID += "'" + sValue + "'";
                                }
                                else
                                {
                                    empRegID += "," + "'" + sValue + "'";
                                }
                                i += 1;
                            }
                            sbReportFilter.Append(" " + empRegID + " ) ");
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.District.ToLower())
                        {
                            sbReportFilter.Append(" AND iDistrict.empDistID IN ( ");
                            string[] sSearchValue = filterItem.SearchValue.Split(',');
                            int i = 0;

                            string empDistID = "";

                            foreach (string sValue in sSearchValue)
                            {
                                if (empDistID == "")
                                {
                                    empDistID += "'" + sValue + "'";
                                }
                                else
                                {
                                    empDistID += "," + "'" + sValue + "'";
                                }
                                i += 1;
                            }
                            sbReportFilter.Append(" " + empDistID + " ) ");
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Site.ToLower())
                        {
                            sbReportFilter.Append(" AND iempSite.empSite IN ( ");
                            //sbReportFilter.Append(" AND iqCSite.empSiteID IN ( ");
                            string[] sSearchValue = filterItem.SearchValue.Split(',');
                            int i = 0;

                            string empSite = "";

                            foreach (string sValue in sSearchValue)
                            {
                                if (empSite == "")
                                {
                                    empSite += "'" + sValue + "'";
                                }
                                else
                                {
                                    empSite += "," + "'" + sValue + "'";
                                }
                                i += 1;
                            }
                            sbReportFilter.Append(" " + empSite + " ) ");
                        }

                    }
                    else if (filterItem.ReportOption.ToLower() == ReportOption.Which.ToLower())
                    {
                        if (BusinessUtility.GetInt(filterItem.IsAllEvent) == 0)
                        {
                            if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByEventName.ToLower()) || (filterItem.SearchBy.ToLower() == ReportFilterOption.ByList.ToLower()))
                            {
                                sbReportFilter.Append(" AND courseHdr.courseHdrID IN (" + filterItem.SearchValue + ") ");
                            }
                            else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByCategory.ToLower()))
                            {
                                //sbReportFilter.Append(" AND courseCategory= '" + searchValue + "' ");
                            }
                        }
                    }
                    else if (filterItem.ReportOption.ToLower() == ReportOption.What.ToLower())
                    {
                        if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Once_Completed.ToLower()))
                        {
                            sbReportFilter.Append(" AND IFNULL(rpsummary.IsCompleted, 'No') = 'Yes' ");
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Completed.ToLower()))
                        {
                            sbReportFilter.Append(" AND IFNULL(rpsummary.IsCompleted, 'No') = 'No' ");
                            //sbReportFilter.Append(" AND IFNULL(startdatetime, 'Not Started')!= '" + "Not Started" + "' ");
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Started.ToLower()))
                        {
                            sbReportFilter.Append(" AND IFNULL(startdatetime, 'Not Started')= '" + "Not Started" + "' ");
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Only_Those_Not_Completed_Training.ToLower()))
                        {
                            sbReportFilter.Append(" AND (IFNULL(startdatetime, 'Not Started')= '" + "Not Started" + "'  OR IFNULL(rpsummary.IsCompleted, 'No') = 'No')  ");
                        }
                    }
                }
            }

            #endregion
            sbQuery.Append(" select * from (  ");
            sbQuery.Append(" SELECT ");
            sbQuery.Append(" DISTINCT courseHdr.courseHdrID,  courseHdr.courseTitle{0} AS CourseName, empHdr.empLastName As LastName, empHdr.empFirstName As FirstName,  empHdr.empLoginID As EmployeeID,  ");
            sbQuery.Append(" iJobCode.empJobID as JobCode,    ");   //iqSite.empSiteID AS SiteNumber,

            sbQuery.Append("             if( iqSite.empSiteID is null , iqSite.empSiteID,   ");
            sbQuery.Append(" 		if(iqSite.empSiteID = iqCSite.empSiteID , iqSite.empSiteID,  if ( iqCSite.empSiteID is null, iqSite.empSiteID ,  	CONCAT_WS(' ', iqSite.empSiteID, '</br>(Cross Dock at ', iqCSite.empSiteID , ')' ))	) ");
            sbQuery.Append("  ) as SiteNumber, ");

            sbQuery.Append(" iempSite.empSite, iqLocation.empLocType AS LocationType, iRegion.empRegID AS Region, iDistrict.empDistID AS District, iDivision.empDivID AS Division, iempType.EmployeeType, ");

            sbQuery.Append(" '" + sTextNotStatrted + "' as DateStarted, ");
            sbQuery.Append(" '" + sTextNotStatrted + "' as DateStartedYearMonth, ");
            sbQuery.Append(" '0%' as CourseContentProgress,  ");
            sbQuery.Append(" IF( (IFNULL(rpsummary.IsCompleted, 'No') ='No'), '" + sTextNo + "','" + sTextYes + "')  as SuccessfullyCompleted, ");
            sbQuery.Append(" IFNULL(NULL, '" + sTextNotStatrted + "') as DateCompleted, ");
            sbQuery.Append(" IFNULL(NULL, '" + sTextNotStatrted + "') as DateCompletedYearMonth ");
            //sbQuery.Append(" FROM tblAssignedCourse join empheader empHdr on tmpEmpHdrID = empHdr.empHdrID  and tmpFieldActive = 1 ");
            sbQuery.Append(" FROM empassignedcourse as  rpsummary join empheader empHdr on CourseEmpHdrID = empHdr.empHdrID  and recordActive = 1 and rpsummary.isUsed= 0 and rpsummary.startdatetime <= current_timestamp() "); //and empassignedcourse.isUsed= 0 and empassignedcourse.startdatetime <= current_timestamp()
            sbQuery.Append(" and (empHdr.isDeleted = 0 or empHdr.isDeleted is null) and empHdr.empActive = 1 ");
            sbQuery.Append(" join traningevent AS courseHdr ON courseHdr.courseHdrID = CourseID  AND courseHdr.CourseActive = 1 AND courseHdr.isVersionActive = 1  "); // and courseHdr.courseVerNo = CourseVer
            sbQuery.Append(" INNER JOIN coursetype AS courstyp ON  courstyp.Type = courseHdr.courseType ");
            sbQuery.Append(" left join ( ");
            sbQuery.Append(" SELECT syrefCode.tilesen as empLocType, empHeader_empHdrID as iqEmpHdrID, sysEmpReferenceCodes_sysRefCodeValue as empLocTypeValue ");
            sbQuery.Append(" from empreference  ");
            sbQuery.Append(" join sysempreferencecodes syrefCode on empreference.sysEmpReferenceCodes_sysRefCodeValue =syrefCode.sysRefCodeValue and syrefCode.sysRefCode='DIV' ");
            sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV' ");
            sbQuery.Append(" ) as iqLocation on iqLocation.iqEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" left join ( ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSiteID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference  ");
            sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'STR' ");
            sbQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empHdr.empHdrID ");

            sbQuery.Append(" left join ( ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSiteID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference  ");
            sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'CSITE' ");
            sbQuery.Append(" ) as iqCSite on iqCSite.iEmpHdrID = empHdr.empHdrID ");

            sbQuery.Append(" left join ( ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference ");
            sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TDIVN' ");
            sbQuery.Append(" ) as iDivision ON iDivision.iEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" left join ( ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empRegID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference  ");
            sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TREGN' ");
            sbQuery.Append(" ) as iRegion ON iRegion.iEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" left join (  ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDistID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference ");
            sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TDIST' ");
            sbQuery.Append(" ) as iDistrict ON iDistrict.iEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" left join (  ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference ");
            sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'JOB' ");
            sbQuery.Append(" ) as iJobCode ON iJobCode.iEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" left join (  ");
            sbQuery.Append(" SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empUnionType, syrefCode.sysRefCodeText as employeeType, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" join sysempreferencecodes syrefCode on empreference.sysEmpReferenceCodes_sysRefCodeValue =syrefCode.sysRefCodeValue and syrefCode.sysRefCode='TYP' ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'TYP' ");
            sbQuery.Append(" ) as iempType ON iempType.iEmpHdrID = empHdr.empHdrID ");

            sbQuery.Append(" left join (  ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSite, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference ");
            sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'SITE' ");
            sbQuery.Append(" ) as iempSite ON iempSite.iEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" where 1 = 1 ");
            //sbQuery.Append(" where (Select count(*) from employeecourseregistration AS empCourseReg where empCourseReg.empHeader_empHdrID = empHdr.empHdrID ");
            //sbQuery.Append(" and CourseID = empCourseReg.courseHeader_courseHdrID and CourseVer = empCourseReg.courseHeader_courseVerNo and empHdr.empHdrID =  empHdr.empHdrID) < 1 ");
            sbQuery.Append(" AND  empHdr.empHdrID NOT IN(select empHeaderID ");
            sbQuery.Append(" from emphdrstatus where empExcludeReports =1 )  ");//and  sp_Get_EmpEligableforCourse(empHdr.empHdrID,courseHdr.courseHdrID) =1 


            // not to show courses in TICT logic if entry in employeecourseregistration present 
            sbQuery.Append(" and courseHdr.courseHdrID NOT IN (SELECT DISTINCT ");
            sbQuery.Append(" courseHeader_courseHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" employeecourseregistration ");
            sbQuery.Append(" WHERE createdDateTime > '2015-12-31' and  ");
            sbQuery.Append(" empHeader_empHdrID =   ");
            sbQuery.Append(" empHdr.empHdrID ");
            sbQuery.Append(" ) ");
            // not to show courses in TICT logic if entry in employeecourseregistration present   


            //if (isSiteReporting)
            //{
            //    sbQuery.Append(" AND CASE IFNULL(IsCompleted, 'No') when 'No' then sp_Get_EmpEligableforCourse(empHdr.empHdrID,courseHdr.courseHdrID) =1 else sp_Get_EmpEligableforCourse(empHdr.empHdrID,courseHdr.courseHdrID)=sp_Get_EmpEligableforCourse(empHdr.empHdrID,courseHdr.courseHdrID) end ");
            //}

            sbQuery.Append(BusinessUtility.GetString(sbReportFilter));

            sbQuery.Append(" UNION ");

            sbQuery.Append(" SELECT rpsummary.courseHdrID,  rpsummary.courseName AS CourseName, empHdr.empLastName As LastName, empHdr.empFirstName As FirstName, empHdr.empLoginID As EmployeeID, ");
            sbQuery.Append(" iJobCode.empJobID as JobCode,   "); // iqSite.empSiteID AS SiteNumber,


            sbQuery.Append("             if( iqSite.empSiteID is null , iqSite.empSiteID,   ");
            sbQuery.Append(" 		if(iqSite.empSiteID = iqCSite.empSiteID , iqSite.empSiteID,  if ( iqCSite.empSiteID is null, iqSite.empSiteID ,  	CONCAT_WS(' ', iqSite.empSiteID, '</br>(Cross Dock at ', iqCSite.empSiteID , ')' ))	) ");
            sbQuery.Append("  ) as SiteNumber, ");

            sbQuery.Append(" iempSite.empSite, iqLocation.empLocType AS LocationType, iRegion.empRegID AS Region, iDistrict.empDistID AS District, iDivision.empDivID AS Division, iempType.EmployeeType,   ");

            sbQuery.Append(" IF(ISNULL(fun_get_Start_Date(rpsummary.registrationID)) ,'Not Started', Date_Format(fun_get_Start_Date(rpsummary.registrationID), '%d-%m-%Y' )) as DateStarted,  ");
            sbQuery.Append(" IF(ISNULL(fun_get_Start_Date(rpsummary.registrationID)) ,'Not Started', Date_Format(fun_get_Start_Date(rpsummary.registrationID), '%m-%Y' )) as DateStartedYearMonth,  ");
            //if (applicationType == (int)Institute.tdc)
            if ((applicationType == (int)Institute.tdc) || (applicationType == (int)Institute.navcanada) || (applicationType == (int)Institute.AlMurrayDentistry) || (applicationType == (int)Institute.EDE2)) //|| (applicationType == (int)Institute.Feedback)
            {
                sbQuery.Append(" CAST(IF((courseHdr.TestType='CO' and courseHdr.scorm_type= 'SIG' ) and rpsummary.IsCompleted='No' and rpsummary.startdatetime is not null and rpsummary.progress !=100, 'Have started but not finished', IF(rpsummary.completeddatetime  is not null, CONCAT_WS(' ', '100', '%'), CONCAT_WS(' ',IFNULL(rpsummary.progress, 0), '%') ) )  as CHAR (50)) as CourseContentProgress,  ");
            }
            else
            {
                sbQuery.Append(" CAST(IF((courseHdr.TestType='CO' or courseHdr.scorm_type= 'SIG' ) and rpsummary.IsCompleted='No' and rpsummary.startdatetime is not null and rpsummary.progress !=100, 'Have started but not finished', IF(rpsummary.completeddatetime  is not null, CONCAT_WS(' ', '100', '%'), CONCAT_WS(' ',IFNULL(rpsummary.progress, 0), '%') ) )  as CHAR (50)) as CourseContentProgress,  ");
            }

            sbQuery.Append(" IF( (IFNULL(rpsummary.IsCompleted, 'No') ='No'), '" + sTextNo + "','" + sTextYes + "')  as SuccessfullyCompleted, ");
            sbQuery.Append(" if(fun_get_Start_Date(rpsummary.registrationID) is null ,'Not Started',if((IFNULL(rpsummary.IsCompleted, 'No') ='No'),'', IFNULL(DATE_FORMAT(rpsummary.completeddatetime, '%d-%m-%Y'), ''))) AS DateCompleted, ");
            sbQuery.Append(" if(fun_get_Start_Date(rpsummary.registrationID) is null ,'Not Started',if((IFNULL(rpsummary.IsCompleted, 'No') ='No'),'', IFNULL(DATE_FORMAT(rpsummary.completeddatetime, '%m-%Y'), ''))) AS DateCompletedYearMonth ");
            sbQuery.Append(" from reportsummary rpsummary ");
            sbQuery.Append(" join employeecourseregistration creg on creg.idRegistration = rpsummary.registrationID  ");
            sbQuery.Append(" join traningevent courseHdr on courseHdr.courseHdrID	= rpsummary.courseHdrID AND  courseHdr.courseVerNo =  creg.courseHeader_courseVerNo and courseHdr.CourseActive = 1  "); //and rpsummary.isEligible = 1 AND courseHdr.isVersionActive = 1
            //sbQuery.Append(" and rpsummary.recordExpiryDateTime >=  CURRENT_TIMESTAMP() ");
            //sbQuery.Append(" AND ifnull(rpsummary.recordExpiryDateTime, CURRENT_TIMESTAMP()) >= CURRENT_TIMESTAMP() ");
            sbQuery.Append(" and ( rpsummary.recordExpiryDateTime >=  CURRENT_TIMESTAMP()  OR rpsummary.recordExpiryDateTime IS NULL) ");

            sbQuery.Append(" join empheader as empHdr on empHdr.empHdrID = rpsummary.emphdrID and (empHdr.isDeleted = 0 or empHdr.isDeleted is null) and empHdr.empActive = 1  ");

            sbQuery.Append(" left join ( ");
            sbQuery.Append(" SELECT syrefCode.tilesen as empLocType, empHeader_empHdrID as iqEmpHdrID, sysEmpReferenceCodes_sysRefCodeValue as empLocTypeValue  ");
            sbQuery.Append(" from empreference  ");
            sbQuery.Append(" join sysempreferencecodes syrefCode on empreference.sysEmpReferenceCodes_sysRefCodeValue =syrefCode.sysRefCodeValue and syrefCode.sysRefCode='DIV' ");
            sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV' ");
            sbQuery.Append(" ) as iqLocation on iqLocation.iqEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" left join ( ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSiteID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference  ");
            sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'STR' ");
            sbQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empHdr.empHdrID ");

            sbQuery.Append(" left join ( ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSiteID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference  ");
            sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'CSITE' ");
            sbQuery.Append(" ) as iqCSite on iqCSite.iEmpHdrID = empHdr.empHdrID ");

            sbQuery.Append(" left join ( ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference ");
            sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TDIVN' ");
            sbQuery.Append(" ) as iDivision ON iDivision.iEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" left join ( ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empRegID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference  ");
            sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TREGN' ");
            sbQuery.Append(" ) as iRegion ON iRegion.iEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" left join (  ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDistID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference ");
            sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TDIST' ");
            sbQuery.Append(" ) as iDistrict ON iDistrict.iEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" left join (  ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference ");
            sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'JOB' ");
            sbQuery.Append(" ) as iJobCode ON iJobCode.iEmpHdrID = empHdr.empHdrID ");
            sbQuery.Append(" left join (  ");


            sbQuery.Append(" SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empUnionType, syrefCode.sysRefCodeText as employeeType, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" join sysempreferencecodes syrefCode on empreference.sysEmpReferenceCodes_sysRefCodeValue =syrefCode.sysRefCodeValue and syrefCode.sysRefCode='TYP' ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'TYP' ");

            sbQuery.Append(" ) as iempType ON iempType.iEmpHdrID = empHdr.empHdrID ");


            sbQuery.Append(" left join (  ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSite, empHeader_empHdrID as iEmpHdrID ");
            sbQuery.Append(" from empreference ");
            sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'SITE' ");
            sbQuery.Append(" ) as iempSite ON iempSite.iEmpHdrID = empHdr.empHdrID ");


            sbQuery.Append(" WHERE 1=1  ");//and  sp_Get_EmpEligableforCourse(empHdr.empHdrID,courseHdr.courseHdrID) =1
            //sbQuery.Append(" AND CASE IFNULL(rpsummary.IsCompleted, 'No') when 'No' then rpsummary.isEligible = 1 else true end ");
            sbQuery.Append(" AND CASE IFNULL(rpsummary.IsCompleted, 'No') when 'No' then sp_Get_EmpEligableforCourse(empHdr.empHdrID,courseHdr.courseHdrID) = 1  ");

            // not to show courses in TIAT logic if entry in employeecourseregistration present  
            sbQuery.Append("  and courseHdr.courseHdrID NOT IN (SELECT DISTINCT ");
            sbQuery.Append(" courseHeader_courseHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" employeecourseregistration reg join reportsummary rs on rs.registrationid = reg.idregistration  ");
            sbQuery.Append(" WHERE reg.createdDateTime > '2015-12-31' and rs.courseCompleted = 1 and ");
            sbQuery.Append(" reg.empHeader_empHdrID = empHdr.empHdrID  ");
            sbQuery.Append(" ) ");
            sbQuery.Append(" else true end ");
            // not to show courses in TIAT logic if entry in employeecourseregistration present  


            sbQuery.Append(" AND  empHdr.empHdrID NOT IN(select empHeaderID ");
            sbQuery.Append(" from emphdrstatus where empExcludeReports =1) ");



            sbQuery.Append(BusinessUtility.GetString(sbReportFilter));


            sbQuery.Append(" ) as ReportResult order by SiteNumber, LastName, FirstName, EmployeeID, CourseName, DateStarted  ");


            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtReport = dbHelp.GetDataTable(BusinessUtility.GetString(string.Format(BusinessUtility.GetString(sbQuery), lang)), System.Data.CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(BusinessUtility.GetString(sbQuery));
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtReport;
        }

        /// <summary>
        /// To Save Report Query or Report Search Parameters in empreportfilter Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="reportFilterList">Pass Report Filter</param>
        /// <returns>True/False</returns>
        public Boolean SaveReportFilters(int empID, List<ReportFilter> reportFilterList)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" INSERT INTO empreportfilter ");
                sbInsertQuery.Append(" (empID,  createdDateTime) ");
                sbInsertQuery.Append(" VALUES(@empID,  @createdDateTime) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empID", empID, MyDbType.Int),    
                    DbUtility.GetParameter("createdDateTime", DateTime.Now, MyDbType.DateTime),    
                    });
                object obj = dbTransactionHelper.GetLastInsertID();

                foreach (var filterItem in reportFilterList)
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" INSERT INTO empreportsummarydetails ");
                    sbInsertQuery.Append(" (empReportFilterID, reportType, searchBy, searchValue, isAllEvent) ");
                    sbInsertQuery.Append(" VALUES(@empReportFilterID, @reportType ,@searchBy, @searchValue, @isAllEvent) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empReportFilterID", BusinessUtility.GetInt(obj), MyDbType.Int),    
                    DbUtility.GetParameter("reportType", filterItem.ReportOption , MyDbType.String),    
                    DbUtility.GetParameter("searchBy", filterItem.SearchBy , MyDbType.String),   
                    DbUtility.GetParameter("searchValue", filterItem.SearchValue , MyDbType.String), 
                    DbUtility.GetParameter("isAllEvent", BusinessUtility.GetInt( filterItem.IsAllEvent) , MyDbType.Int), 
                    });
                }

                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Get Recent Report History From empreportfilter Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>Datatable</returns>
        public DataTable GetReportRecentHistory(int empID)
        {
            DataTable dt = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            StringBuilder sbReportFilter = new StringBuilder();
            sbQuery.Append(" SELECT * FROM empreportfilter ");
            sbQuery.Append(" WHERE empID = @empID order by createdDateTime Desc ");

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empID", empID, MyDbType.Int)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dt;
        }

        /// <summary>
        /// To Get Report Data Using Query or Search Parameters From empreportfilter Table
        /// </summary>
        /// <param name="reportID">Pass Report ID</param>
        /// <returns>string</returns>
        public List<ReportFilter> GetReportFilter(int reportID)
        {
            List<ReportFilter> lstReportFilter = new List<BusinessLogic.ReportFilter>();

            string sReportFilter = string.Empty;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT * FROM empreportsummarydetails ");
            sbQuery.Append(" WHERE empReportFilterID = @reportID ");

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                DataTable dtRptSummaryDetails = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("reportID", reportID, MyDbType.Int)
                });

                foreach (DataRow dRow in dtRptSummaryDetails.Rows)
                {
                    lstReportFilter.Add(new ReportFilter { ReportOption = BusinessUtility.GetString(dRow["reportType"]), SearchBy = BusinessUtility.GetString(dRow["searchBy"]), SearchValue = BusinessUtility.GetString(dRow["searchValue"]), IsAllEvent = BusinessUtility.GetString(BusinessUtility.GetInt(dRow["isAllEvent"])) });
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return lstReportFilter;
        }


        /// <summary>
        /// To Save Course Registration Status in reportsummary Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="regID">Pass Course Registration ID</param>
        /// <param name="courseName">Pass Course Name</param>
        /// <param name="courseHdrID">Pass Course ID</param>
        /// <param name="courseCategory">Pass Course Category</param>
        /// <param name="empExtID">Pass Employee External ID</param>
        /// <param name="courseType">Pass Course Type</param>
        /// <returns>True/False</returns>
        public Boolean SaveReportSummary(int empID, int regID, string courseName, int courseHdrID, string courseCategory, string empExtID, string courseType)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            StringBuilder sbInsertQuery = new StringBuilder();
            try
            {
               

                sbInsertQuery.Append(" insert into reportsummary ");
                sbInsertQuery.Append(" ( emphdrID, registrationID, courseName, createddatetime, courseHdrID, courseCategory, empJobID, empSiteID, empExtID, empLocType, empDivID, empRegID, empDistID, empUnionType, courseType, isEligible) ");
                sbInsertQuery.Append(" values ( @emphdrID, @registrationID, @courseName, @createddatetime, @courseHdrID, @courseCategory, fun_get_emp_ref_val('" + EmpRefCode.JobCode + "', @emphdrID),  ");
                sbInsertQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.Store + "', @emphdrID) , @empExtID , fun_get_emp_ref_val('" + EmpRefCode.Division + "', @emphdrID) , ");
                sbInsertQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.DivisionTBS + "', @emphdrID) , fun_get_emp_ref_val('" + EmpRefCode.RegionTBS + "', @emphdrID) ,  ");
                sbInsertQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.District + "', @emphdrID) , fun_get_emp_ref_val('" + EmpRefCode.Type + "', @emphdrID), ");
                sbInsertQuery.Append(" @courseType, sp_Get_EmpEligableforCourse(@emphdrID, @courseHdrID)  ");
                sbInsertQuery.Append(" ) ");

                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("emphdrID", empID, MyDbType.Int),    
                    DbUtility.GetParameter("registrationID", regID, MyDbType.Int),
                    DbUtility.GetParameter("courseName", courseName , MyDbType.String),
                    DbUtility.GetParameter("createdDateTime", DateTime.Now, MyDbType.DateTime),    
                    DbUtility.GetParameter("courseHdrID", courseHdrID, MyDbType.Int),
                    DbUtility.GetParameter("courseCategory", courseCategory , MyDbType.String),
                    DbUtility.GetParameter("empExtID", empExtID, MyDbType.String),
                    DbUtility.GetParameter("courseType", courseType, MyDbType.String)
                    });
                dbTransactionHelper.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                
                ErrorLog.createLog("Report Summary Insert Failure " + BusinessUtility.GetString(regID));
                ErrorLog.createLog("Report Update Insert Query " + BusinessUtility.GetString(sbInsertQuery));
                ErrorLog.createLog("Report Update Insert Param1 :- " + empID + "  Param2 :- " + regID + "  Param3 :- " + courseName + "  Param4 :- " + courseHdrID + "  Param5 :- " + courseCategory + "  Param6 :- " + empExtID + "  Param7 :- " + courseType + "  ");
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// To Update Course Registration Status in reportsummary table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="regID">Pass Course Registration ID</param>
        /// <param name="totalSlideNo">Pass Course Total No of Slide</param>
        /// <param name="courseType">Pass Course Type</param>
        /// <param name="lastTestScore">Pass Last Test Score</param>
        /// <param name="noOfTestAttempts">Pass No of Test Attempts</param>
        /// <param name="passTestScore">Pass Test Score</param>
        /// <param name="courseCompleted">Pass Course Completed</param>
        /// <param name="empExtID">Pass Employee External ID</param>
        /// <returns></returns>
        public Boolean UpdateReportSummary(int empID, int regID, int totalSlideNo, string courseType, float lastTestScore, int noOfTestAttempts, float passTestScore, int courseCompleted, string empExtID)
        {
            StringBuilder sbInsertQuery = new StringBuilder();
            DbTransactionHelper dbTransactionHelperReportSummary = new DbTransactionHelper();
            dbTransactionHelperReportSummary.BeginTransaction();
            try
            {
                sbInsertQuery.Append(" update reportsummary  ");
                sbInsertQuery.Append(" set  ");
                sbInsertQuery.Append(" progress = fun_get_Progress_status(@regID, @totalslide), ");
                sbInsertQuery.Append(" startdatetime =  fun_get_Start_Date(@regID),  ");
                sbInsertQuery.Append(" completeddatetime = fun_get_End_Date(@regID), ");
                sbInsertQuery.Append(" IsCompleted = IFNULL(fun_get_Course_completed(@regID), ");
                sbInsertQuery.Append(" 'No'), ");
                sbInsertQuery.Append(" lastupdatetime = @lastupdateddatetime,  ");

                if (courseType.ToLower() == CourseTestType.CourseTest.ToLower())
                {
                    sbInsertQuery.Append(" lastTestScore = @lastTestScore,  ");
                    sbInsertQuery.Append(" noOfTestAttempts = @noOfTestAttempts,  ");
                    sbInsertQuery.Append(" passTestScore = @passTestScore,  ");

                    if (courseCompleted == 0)
                    {
                    }
                    else
                    {
                        sbInsertQuery.Append(" courseCompletedDateTime = fun_get_End_Date(@regID), ");
                    }
                }
                else
                {
                    sbInsertQuery.Append(" courseCompletedDateTime = fun_get_End_Date(@regID), ");
                }
                sbInsertQuery.Append(" courseCompleted = @courseCompleted,  ");
                sbInsertQuery.Append(" empJobID = fun_get_emp_ref_val('" + EmpRefCode.JobCode + "', @empID) ,  ");
                sbInsertQuery.Append(" empSiteID = fun_get_emp_ref_val('" + EmpRefCode.Store + "', @empID) ,  ");
                sbInsertQuery.Append(" empExtID = @empExtID ,  ");
                sbInsertQuery.Append(" empLocType = fun_get_emp_ref_val('" + EmpRefCode.Division + "', @empID) ,  ");
                sbInsertQuery.Append(" empDivID = fun_get_emp_ref_val('" + EmpRefCode.DivisionTBS + "', @empID) ,  ");
                sbInsertQuery.Append(" empRegID = fun_get_emp_ref_val('" + EmpRefCode.RegionTBS + "', @empID) ,  ");
                sbInsertQuery.Append(" empDistID = fun_get_emp_ref_val('" + EmpRefCode.District + "', @empID) ,  ");
                sbInsertQuery.Append(" empUnionType = fun_get_emp_ref_val('" + EmpRefCode.Type + "', @empID),   ");
                sbInsertQuery.Append(" courseType = @courseType,    ");
                sbInsertQuery.Append(" isEligible= sp_Get_EmpEligableforCourse(empHdrID,courseHdrID) ");
                sbInsertQuery.Append(" where registrationID = @regID  ");

                List<MySqlParameter> pList = new List<MySqlParameter>();
                pList.Add(DbUtility.GetParameter("regID", regID, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("totalslide", totalSlideNo, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("lastupdateddatetime", DateTime.Now, MyDbType.DateTime));
                pList.Add(DbUtility.GetParameter("empID", empID, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("empExtID", empExtID, MyDbType.String));
                pList.Add(DbUtility.GetParameter("courseCompleted", courseCompleted, MyDbType.Int));
                if (courseType.ToLower() == CourseTestType.CourseTest.ToLower())
                {
                    pList.Add(DbUtility.GetParameter("passTestScore", passTestScore, MyDbType.Float));
                    pList.Add(DbUtility.GetParameter("noOfTestAttempts", noOfTestAttempts, MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("lastTestScore", lastTestScore, MyDbType.Float));
                }
                pList.Add(DbUtility.GetParameter("courseType", courseType, MyDbType.String));
                dbTransactionHelperReportSummary.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, pList.ToArray());
                dbTransactionHelperReportSummary.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelperReportSummary.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                ErrorLog.createLog("Report Summary Update Failure " + BusinessUtility.GetString(regID));
                ErrorLog.createLog("Report Update Failure Query " + BusinessUtility.GetString(sbInsertQuery));
                ErrorLog.createLog("Report Update Failure Param1 :- "+  empID +"  Param2 :- "+  regID +"  Param3 :- "+  totalSlideNo +"  Param4 :- "+  courseType +"  Param5 :- "+  lastTestScore +"  Param6 :- "+  noOfTestAttempts +"  Param7 :- "+  passTestScore +"  Param8 :- "+  courseCompleted +"  Param9 :- "+  empExtID +"  " );
                throw;
            }
            finally
            {
                dbTransactionHelperReportSummary.CloseDatabaseConnection();
            }
        }



        public DataTable GetCompletedFeedbackReport()
        {
            DataTable ResultDataTable = new DataTable();
            StringBuilder sbQuery = new StringBuilder();

           // MySqlParameter[] p = { new MySqlParameter("@EmpID", empID) };
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(" call sp_feedbackreport()");
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null);

                //DataView dv = dt.DefaultView;
                //dv.Sort = "isNew DESC, CourseTitle ASC";
                //ResultDataTable = dv.ToTable();
                ResultDataTable = dt;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return ResultDataTable;
        }


    }


   

    /// <summary>
    /// Define Report Filter
    /// </summary>
    public class ReportFilter
    {
        /// <summary>
        /// Set Report Type Option
        /// </summary>
        public string ReportOption { get; set; }

        /// <summary>
        /// Set Report Search By
        /// </summary>
        public string SearchBy { get; set; }

        /// <summary>
        /// Set Report Search Value
        /// </summary>
        public string SearchValue { get; set; }

        /// <summary>
        /// Set Report Is All Event For Which Type Report
        /// </summary>
        public string IsAllEvent { get; set; }

        /// <summary>
        /// To Save Report Filter To Show Report 
        /// </summary>
        /// <param name="reportOption">Pass Report Type Option</param>
        /// <param name="searchBy">Pass Report Search By</param>
        /// <param name="searchValue">Pass Report Search Value</param>
        /// <param name="isAllEvent">Pass Report Is All Event For Which Type Report</param>
        public void CreateNewFilter(string reportOption, string searchBy, string searchValue, string isAllEvent)
        {
            List<ReportFilter> objReportFilter = new List<ReportFilter>();
            try
            {
                objReportFilter = (List<ReportFilter>)HttpContext.Current.Session["ReportMultiFilter"];
                if (objReportFilter != null)
                {
                    var resultItem = objReportFilter.Where(x => x.ReportOption == reportOption && x.SearchBy == searchBy).ToList();
                    if (resultItem != null && resultItem.Count > 0)
                    {
                        foreach (var removeItem in resultItem)
                        {
                            objReportFilter.Remove(removeItem);
                        }
                    }
                    objReportFilter.Add(new ReportFilter { ReportOption = reportOption, SearchBy = searchBy, SearchValue = searchValue, IsAllEvent = isAllEvent });
                }
                else
                {
                    objReportFilter = new List<ReportFilter>();
                    objReportFilter.Add(new ReportFilter { ReportOption = reportOption, SearchBy = searchBy, SearchValue = searchValue, IsAllEvent = isAllEvent });
                }
                HttpContext.Current.Session["ReportMultiFilter"] = objReportFilter;
            }
            catch (Exception e)
            {
                ErrorLog.CreateLog(e);

            }
            finally { }
        }

        /// <summary>
        /// To Get Report Filter
        /// </summary>
        /// <returns>List<ReportFilter></returns>
        public List<ReportFilter> GetReportFilter()
        {
            List<ReportFilter> objReportFilter = new List<ReportFilter>();
            try
            {
                objReportFilter = (List<ReportFilter>)HttpContext.Current.Session["ReportMultiFilter"];
            }
            catch (Exception e) { ErrorLog.createLog(e.ToString()); }
            finally { }

            return objReportFilter;
        }

    }


    /// <summary>
    /// Define Report Type
    /// </summary>
    public class ReportType
    {
        /// <summary>
        /// Set Report Name
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// Get Available Report Type List
        /// </summary>
        /// <returns>List Object</returns>
        public List<ReportType> GetReportType()
        {
            List<ReportType> objReportType = new List<ReportType>();
            try
            {
                objReportType.Add(new ReportType { ReportName = EmpSearchBy.Store });
                objReportType.Add(new ReportType { ReportName = EmpSearchBy.SiteLogisticsReport });
                objReportType.Add(new ReportType { ReportName = EmpSearchBy.District });
                objReportType.Add(new ReportType { ReportName = EmpSearchBy.DivisionTBS });
                objReportType.Add(new ReportType { ReportName = EmpSearchBy.RegionTBS });
                objReportType.Add(new ReportType { ReportName = EmpSearchBy.Division });
            }
            catch (Exception e) { ErrorLog.createLog(e.ToString()); }
            finally { }

            return objReportType;
        }
    }


}
