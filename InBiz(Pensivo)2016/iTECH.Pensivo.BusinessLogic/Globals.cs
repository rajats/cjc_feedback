﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Threading;
using System.Globalization;
using System.Data;
using System.Reflection;

using iTECH.Library.Utilities;
using iTECH.Library.Web;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Global Setting Functionality 
    /// </summary>
    public class Globals
    {
        /// <summary>
        /// To Get Default or Current Culture Name
        /// </summary>
        public static string CurrentCultureName
        {
            get
            {
                string cName = string.Empty;
                cName = BusinessUtility.GetString(HttpContext.Current.Request.QueryString["lang"]);
                switch (cName.Trim().ToLower())
                {
                    case AppLanguageCode.EN:
                        return AppCulture.ENGLISH_CANADA;
                    case AppLanguageCode.FR:
                        return AppCulture.FRENCH_CANADA;
                    default:
                        if (HttpContext.Current.Session["Language"] == null)
                        {
                            return AppCulture.ENGLISH_CANADA; 
                        }
                        return HttpContext.Current.Session["Language"].ToString();
                }
            }
        }

        /// <summary>
        /// To Get Default Culture Information
        /// </summary>
        /// <param name="cultureName">Pass Culture Name</param>
        public static void SetCultureInfo(string cultureName)
        {
            try
            {
                HttpContext.Current.Session["Language"] = cultureName;
                CultureInfo cInfo = new CultureInfo(cultureName);
                Thread.CurrentThread.CurrentCulture = cInfo;
                Thread.CurrentThread.CurrentUICulture = cInfo;
            }
            catch
            {
                HttpContext.Current.Session["Language"] = AppCulture.DEFAULT;
                CultureInfo cInfo = new CultureInfo(AppCulture.DEFAULT);
                Thread.CurrentThread.CurrentCulture = cInfo;
                Thread.CurrentThread.CurrentUICulture = cInfo;
            }
        }

        /// <summary>
        /// To Get Current Culture Language Code
        /// </summary>
        public static string CurrentAppLanguageCode
        {
            get
            {
                switch (Globals.CurrentCultureName)
                {
                    case AppCulture.FRENCH_CANADA:
                        return AppLanguageCode.FR;
                    case AppCulture.ENGLISH_CANADA:
                        return AppLanguageCode.EN;
                    default:
                        return AppLanguageCode.DEFAULT;
                }
            }
        }

    }
}
