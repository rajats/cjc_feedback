﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;


namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Certificate Functionality
    /// </summary>
    public class Certificate
    {
        /// <summary>
        /// To Get/Set Certificate ID
        /// </summary>
        public int certificateID { get; set; }

        /// <summary>
        /// To Get/Set Employee/User Course Registration ID
        /// </summary>
        public int regID { get; set; }

        /// <summary>
        /// To Get/Set Employee/User ID
        /// </summary>
        public int empID { get; set; }

        /// <summary>
        /// To Get/Set Event ID
        /// </summary>
        public int eventID { get; set; }

        /// <summary>
        /// To Get/Set Event Version No
        /// </summary>
        public string eventVerNo { get; set; }

        /// <summary>
        /// To Get/Set Certificate Path
        /// </summary>
        public string certificatePath { get; set; }

        /// <summary>
        /// To Save Certificate Details into emphdrcerticiatelist Table
        /// </summary>
        /// <returns>True/False</returns>
        public Boolean SaveCertificate()
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" insert into  emphdrcerticiatelist ( empHdrID, trainingEventID, trainingEventIDVersionNo, certificateFilePath, certificateGeneratedOnDateTime, regID ) ");
                sbInsertQuery.Append(" values ( @empHdrID, @trainingEventID, @trainingEventIDVersionNo, @certificateFilePath, @certificateGeneratedOnDateTime, @regID ) ");

                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empHdrID", this.empID, MyDbType.Int),    
                DbUtility.GetParameter("trainingEventID", this.eventID, MyDbType.Int),    
                DbUtility.GetParameter("trainingEventIDVersionNo", this.eventVerNo  , MyDbType.String),
                DbUtility.GetParameter("certificateFilePath", this.certificatePath , MyDbType.String),
                DbUtility.GetParameter("certificateGeneratedOnDateTime", DateTime.Now , MyDbType.DateTime),
                DbUtility.GetParameter("regID", this.regID, MyDbType.Int),
                });
                this.certificateID = dbTransactionHelper.GetLastInsertID();
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Certificate Type From cetificatetype Table
        /// </summary>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetCertificateList(string lang)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT idCetificateType   ");
            if (lang == AppLanguageCode.EN)
            {
                sbQuery.Append(" , certificateName as certificateName ");
            }
            else
            {
                sbQuery.Append(" , certificateNameFr as certificateName  ");
            }

            sbQuery.Append(" FROM cetificatetype where isActive =1 ");

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, null);
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Certificate File Path From emphdrcerticiatelist Table
        /// </summary>
        /// <param name="regID">Pass Registration ID</param>
        /// <returns>string</returns>
        public string GetCertificateFilePath(int regID)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT certificateFilePath FROM emphdrcerticiatelist where regID = @regID  ");
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("regID", regID, MyDbType.Int)
                });
                return BusinessUtility.GetString(rValue);
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

    }
}
