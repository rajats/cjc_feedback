﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Data;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.Pensivo.BusinessLogic;
using System.Data;


namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Error Log Functionality
    /// </summary>
    public static class ErrorLog
    {
        /// <summary>
        /// Define Private variable to get Log Directory Folder
        /// </summary>
        static string slogFilePath = ErrorLog.ErrorLogPath;

        /// <summary>
        /// Define Private variable For File Name
        /// </summary>
        static string sFileName = "Log" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

        /// <summary>
        /// Define Private variable For File Path with File Name
        /// </summary>
        static string sFilePath = slogFilePath + sFileName;

        /// <summary>
        /// To Track Error Log Messages
        /// </summary>
        /// <param name="errorMessage">Pass Error Message</param>
        public static void createLog(string errorMessage)
        {
            //string path = BusinessUtility.GetString(AppConfig.GetAppConfigValue("LogsDiractory")) + "Log" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            string path = BusinessUtility.GetString(ErrorLog.ErrorLogPath) + "Log" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            if (BusinessUtility.GetString(AppConfig.GetAppConfigValue("LogError")).ToString().ToUpper() == "TRUE")
            {
                if (!File.Exists(path))
                {
                    StreamWriter sw = File.CreateText(path);
                    sw.Close();
                }
                using (System.IO.StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine("-------- " + DateTime.Now + " --------");
                    sw.WriteLine(errorMessage);
                    sw.WriteLine("------------------------");
                    sw.Close();
                }
            }
        }

        /// <summary>
        /// To Track Error Exception
        /// </summary>
        /// <param name="ex">Pass Error Exception</param>
        public static void CreateLog(Exception ex)
        {
            if (!Directory.Exists(slogFilePath))
            {
                Directory.CreateDirectory(slogFilePath);
            }
            if (!File.Exists(sFilePath))
            {
                var objFile = File.Create(sFilePath);
                objFile.Close();
            }


            StringBuilder sbLogText = new StringBuilder("Log starts :" + DateTime.Now);
            sbLogText.AppendLine();
            sbLogText.AppendLine();
            sbLogText.Append(ex.ToString());
            sbLogText.AppendLine();
            sbLogText.AppendLine();
            sbLogText.Append("Log ends :" + DateTime.Now);
            sbLogText.AppendLine();
            sbLogText.AppendLine();
            sbLogText.AppendLine();
            bool bAppend = true;
            StreamWriter oStreamWriter = new StreamWriter(sFilePath, bAppend);
            oStreamWriter.Write(sbLogText);
            oStreamWriter.Close();
        }


        /// <summary>
        /// To Get Error Log Path
        /// </summary>
        public static string ErrorLogPath
        {
            get
            {
                if (BusinessUtility.GetString(ConfigurationManager.AppSettings["server-type"]).ToUpper() == "call-back".ToUpper())
                {
                   return BusinessUtility.GetString(AppConfig.GetAppConfigValue("LogsDirectory-callback"));
                }
                else
                {
                  return  BusinessUtility.GetString(AppConfig.GetAppConfigValue("LogsDirectory"));
                }
            }
        }

    }
}
