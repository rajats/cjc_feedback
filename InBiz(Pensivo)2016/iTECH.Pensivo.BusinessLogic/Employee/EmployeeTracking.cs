﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Track User Activity in TBS
    /// </summary>
    public static class EmployeeTracking
    {
        /// <summary>
        /// To Save User Different Activities in trackactionitem Table Such as User Login/Logout and SCORM Request
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="actionItemID">Pass Action Item ID</param>
        /// <param name="sessionID">Pass User Logged In Session ID</param>
        /// <param name="browserAgent">Pass Browser Agent Details</param>
        /// <param name="ipAddress">Pass IP Address</param>
        /// <param name="courseName">Pass Course Name</param>
        /// <param name="regID">Pass Employess Course/Test Registration ID</param>
        /// <param name="regErrorCode">Pass Registration Error Code</param>
        /// <param name="isNewCourse">Pass Is New Course</param>
        /// <returns>True/False</returns>
        public static Boolean TrackEmployeeAction(int empID, int actionItemID, string sessionID, string browserAgent, string ipAddress, string courseName, int regID, int regErrorCode, string isNewCourse)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" INSERT INTO trackactionitem (empHdrID, createdDatetime, actionID, sessionID, browserAgent, ipAddress, courseName, regID, regErrorCode, isNewCourse ");
                sbInsertQuery.Append(" )");
                sbInsertQuery.Append(" VALUES (@empHdrID, @createdDatetime, @actionID, @sessionID, @browserAgent, @ipAddress, @courseName, @regID, @regErrorCode, @isNewCourse ");
                sbInsertQuery.Append(" ) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empHdrID",  empID , MyDbType.Int),    
                            DbUtility.GetParameter("createdDatetime", DateTime.Now, MyDbType.DateTime),
                            DbUtility.GetParameter("actionID", actionItemID , MyDbType.Int),
                            DbUtility.GetParameter("sessionID", sessionID , MyDbType.String),
                            DbUtility.GetParameter("browserAgent", browserAgent , MyDbType.String),
                            DbUtility.GetParameter("ipAddress", ipAddress , MyDbType.String),
                            DbUtility.GetParameter("courseName", courseName , MyDbType.String),
                            DbUtility.GetParameter("regID", regID , MyDbType.Int),
                            DbUtility.GetParameter("isNewCourse", isNewCourse , MyDbType.String),
                            DbUtility.GetParameter("regErrorCode", regErrorCode , MyDbType.Int),
                            });
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }
    }
}
