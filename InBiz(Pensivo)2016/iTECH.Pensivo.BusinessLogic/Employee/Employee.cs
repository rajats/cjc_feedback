﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Functionality Regarding Employee/users 
    /// </summary>
    public class Employee
    {
        /// <summary>
        /// To Get/Set Employee ID
        /// </summary>
        public Int64 EmpID { get; set; }

        /// <summary>
        /// To Get/Set Employee Password 
        /// </summary>
        public string EmpPassword { get; set; }

        /// <summary>
        /// To Get/Set Employee First Name
        /// </summary>
        public string EmpFirstName { get; set; }

        /// <summary>
        /// To Get/Set Employee Last Name
        /// </summary>
        public string EmpLastName { get; set; }

        /// <summary>
        /// To Get/Set Employee Email ID
        /// </summary>
        public string EmpEmail { get; set; }

        /// <summary>
        /// To Get/Set Employee Date of Birth
        /// </summary>
        public DateTime EmpDateOfBirth { get; set; }

        /// <summary>
        /// To Get/Set Employee Phone
        /// </summary>
        public string EmpPhone { get; set; }

        /// <summary>
        /// To Get/Set Employee Phone Ext
        /// </summary>
        public string EmpPhoneExt { get; set; }

        /// <summary>
        /// To Get/Set Employee Office Address Line 1
        /// </summary>
        public string EmpOfficeAddrLine1 { get; set; }

        /// <summary>
        /// To Get/Set Employee Office Address Line 2 
        /// </summary>
        public string EmpOfficeAddrLine2 { get; set; }

        /// <summary>
        /// To Get/Set Employee Office City
        /// </summary>
        public string EmpOfficeCity { get; set; }

        /// <summary>
        /// To Get/Set Employee Postal Code
        /// </summary>
        public string EmpPostalCode { get; set; }

        /// <summary>
        /// To Get/Set Employee State Code
        /// </summary>
        public string EmpStateCode { get; set; }

        /// <summary>
        /// To Get/Set Employee Country Code
        /// </summary>
        public string EmpCountryCode { get; set; }

        /// <summary>
        /// To Get/Set Employee Office PCD
        /// </summary>
        public string EmpOfficePCD { get; set; }

        /// <summary>
        /// To Get/Set Employee Mobile No
        /// </summary>
        public string EmpMobileNo { get; set; }

        /// <summary>
        /// To Get/Set Employee Gender 
        /// </summary>
        public string EmpGender { get; set; }

        /// <summary>
        /// To Get/Set Employee Language Pref
        /// </summary>
        public string EmpLangPref { get; set; }

        /// <summary>
        /// To Get/Set Employee Name With First name and Last Name
        /// </summary>
        public string EmpName { get; set; }

        /// <summary>
        /// To Get/Set Employee External ID/ User ID 
        /// </summary>
        public string EmpExtID { get; set; }

        /// <summary>
        /// To Get/Set Employee Login ID
        /// </summary>
        public string EmpLogInID { get; set; }

        /// <summary>
        /// To Get/Set Employee Job Code
        /// </summary>
        public string JobCode { get; set; }

        /// <summary>
        /// To Get/Set Employee Department
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// To Get/Set Employee District
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// To Get/Set Employee Division
        /// </summary>
        public string Division { get; set; }

        /// <summary>
        /// To Get/Set Region
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// To Get/Set Location
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// To Get/Set Account Type
        /// </summary>
        public string AccountType { get; set; }

        /// <summary>
        /// To Get/Set Employee Login Type
        /// </summary>
        public string EmpType { get; set; }

        /// <summary>
        /// To Get/Set Employee New Password
        /// </summary>
        public string EmpNewPassword { get; set; }

        /// <summary>
        /// To Get/Set Type
        /// </summary>
        public string Type { get; set; }


        /// <summary>
        /// To Get/Set Province
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// To Get/Set Employee Type Parsed
        /// </summary>
        public string TypeParsed { get; set; }

        /// <summary>
        /// To Get/Set Created Source
        /// </summary>
        public string CreatedSource { get; set; }

        /// <summary>
        /// To Get/Set PensivoUUID
        /// </summary>
        public string PensivoUUID { get; set; }

        /// <summary>
        /// To Get/Set AI Report InitiatorEmail ID
        /// </summary>
        public string AIReportInitiatorEmailID { get; set; }


        public bool ExcludeReportUser { get; set; }

        /// <summary>
        /// To Get Employee/users Detail from EmmHeader Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        public void GetEmployeeDetail(int empID)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT empHdrID, EmpExtID, EmpEmail, empLogInID, CONCAT_WS(' ',  EmpFirstName, EmpLastName) AS EmpName, EmpFirstName, EmpLastName, EmpPassword, ");
            sbQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.JobCode + "', @EmpID) AS JobCode, fun_get_emp_ref_val('" + EmpRefCode.Department + "', @EmpID) AS Department,  ");
            sbQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.District + "', @EmpID) AS District, fun_get_emp_ref_val('" + EmpRefCode.Region + "', @EmpID) AS Region,  ");
            sbQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.AccountType + "', @EmpID) AS AccountType,   fun_get_emp_ref_val('" + EmpRefCode.Store + "', @EmpID) AS Store,  ");
            sbQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.Division + "', @EmpID) AS Division,   EmpType, fun_get_emp_ref_val('" + EmpRefCode.Type + "', @EmpID) emp_type , fun_get_emp_ref_val('" + EmpRefCode.TypeParsed + "', @EmpID) emp_type_parse, fun_get_emp_ref_val('" + EmpRefCode.Province + "', @EmpID) province, PensivoUUID ");
            sbQuery.Append(" FROM empheader WHERE  empHdrID = @EmpID ");

            MySqlParameter[] p = { new MySqlParameter("@EmpID", empID) };
            MySqlDataReader objDr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                objDr = dbHelp.GetDataReader(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, p);
                while (objDr.Read())
                {
                    this.EmpID = BusinessUtility.GetInt(objDr["empHdrID"]);
                    this.EmpLogInID = BusinessUtility.GetString(objDr["EmpLogInID"]);
                    this.EmpExtID = BusinessUtility.GetString(objDr["EmpExtID"]);
                    this.EmpName = BusinessUtility.GetString(objDr["EmpName"]);
                    this.EmpEmail = BusinessUtility.GetString(objDr["EmpEmail"]);
                    this.EmpFirstName = BusinessUtility.GetString(objDr["EmpFirstName"]);
                    this.EmpLastName = BusinessUtility.GetString(objDr["EmpLastName"]);
                    this.EmpPassword = BusinessUtility.GetString(objDr["EmpPassword"]);
                    this.JobCode = BusinessUtility.GetString(objDr["JobCode"]);
                    this.Department = BusinessUtility.GetString(objDr["Department"]);
                    this.District = BusinessUtility.GetString(objDr["District"]);
                    this.Region = BusinessUtility.GetString(objDr["Region"]);
                    this.Location = BusinessUtility.GetString(objDr["Store"]);
                    this.Division = BusinessUtility.GetString(objDr["Division"]);
                    this.AccountType = BusinessUtility.GetString(objDr["AccountType"]);
                    this.EmpType = BusinessUtility.GetString(objDr["EmpType"]);
                    this.Type = BusinessUtility.GetString(objDr["emp_type"]);
                    this.TypeParsed = BusinessUtility.GetString(objDr["emp_type_parse"]);
                    this.PensivoUUID = BusinessUtility.GetString(objDr["PensivoUUID"]);
                    this.Province = BusinessUtility.GetString(objDr["Province"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Update Employee/User Detail in EmpHeader Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>True/False</returns>
        public Boolean UpdateEmployee(int empID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" UPDATE empheader SET  empLastUpdatedOn = @empLastUpdatedOn, empEmail= @empEmail  WHERE empHdrID = @empHdrID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("@empHdrID", empID, MyDbType.String),
                DbUtility.GetParameter("@empFirstName", this.EmpFirstName, MyDbType.String),
                DbUtility.GetParameter("@empLastName", this.EmpLastName, MyDbType.String),
                DbUtility.GetParameter("@empLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                DbUtility.GetParameter("@empEmail", this.EmpEmail, MyDbType.String),
                });

                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                dbTransactionHelper.RollBackTransaction();
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Get SysReference Value from sysempreferencecodes Table
        /// </summary>
        /// <param name="sysRefCode">Pass System Reference Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmpSysCodes(string sysRefCode)
        {
            DataTable dtEmpSysCodes = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" select sysRefCodeValue, sysRefCodeText from sysempreferencecodes  where sysRefCodeActive =1 AND sysRefCode=@sysRefCode and sysRefCodeText!='' AND  sysRefCodeValue !='' ");
            MySqlParameter[] p = { new MySqlParameter("@sysRefCode", sysRefCode) };
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtEmpSysCodes = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, p);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmpSysCodes;
        }

        /// <summary>
        /// To Get Employee/User List from empheader Table
        /// </summary>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeList()
        {
            DataTable dtEmployeeList = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            StringBuilder sQuery = new StringBuilder();

            sbQuery = new StringBuilder();
            sQuery = new StringBuilder();

            sQuery.Append(" SELECT empHdrID, EmpExtID, EmpEmail, empLogInID, CONCAT_WS(' ',  EmpLastName, EmpFirstName) AS EmpName, EmpFirstName, EmpLastName, EmpPassword,  ");
            sQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.JobCode + "', empHdrID) AS JobCode, fun_get_emp_ref_val('" + EmpRefCode.Department + "', empHdrID) AS Department,  ");
            sQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.District + "', empHdrID) AS District, fun_get_emp_ref_val('" + EmpRefCode.Region + "', empHdrID) AS Region,  ");
            sQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.AccountType + "', empHdrID) AS AccountType, fun_get_emp_ref_val('" + EmpRefCode.Store + "', empHdrID) AS Store, EmpType  ");
            sQuery.Append(" FROM empheader  ");
            sQuery.Append(" WHERE 1=1 ");
            sQuery.Append(" AND  empHdrID NOT IN ( "+ CurrentEmployee.EmpID +" )");
            if (this.ExcludeReportUser)
            {
                sQuery.Append(" AND  empHdrID NOT IN(select empHeaderID ");
                sQuery.Append(" from emphdrstatus where empExcludeReports =1) ");
            }

            if (BusinessUtility.GetString(this.EmpName) != "")
            {
                sbQuery.Append(sQuery);
                sbQuery.Append("AND ( EmpFirstName = '" + this.EmpName + "' OR EmpLastName = '" + this.EmpName + "' OR  concat_ws(' ', trim(empfirstname), trim(empLastname)) = '" + this.EmpName + "'  OR  concat_ws(' ', trim(empfirstname), trim(empLastname), trim(EmpExtID)) = '" + this.EmpName + "'  OR EmpExtID = '" + this.EmpName + "') ");

                string[] sEmpName = this.EmpName.Split(' ');
                string empClause = "";

                if (sEmpName.Length > 2)
                {
                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and (EmpExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "' OR  EmpExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ) ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' and  EmpExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                }
                else if (sEmpName.Length == 2)
                {
                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and (EmpExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "'  ) ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' AND  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                }
                else if (sEmpName.Length == 1)
                {
                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and (EmpExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ) ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpLastName = '" + BusinessUtility.GetString(sEmpName[0]) + "' ");
                }

                sbQuery.Append(" union ");
                sbQuery.Append(sQuery);
                sbQuery.Append("AND (  ");

                sEmpName = this.EmpName.Split(' ');
                empClause = "";
                foreach (string sName in sEmpName)
                {
                    if (empClause == "")
                    {
                        empClause += " EmpFirstName = '" + sName + "' OR EmpLastName = '" + sName + "' OR  EmpExtID = '" + sName + "' ";
                    }
                    else
                    {
                        empClause += " OR EmpFirstName = '" + sName + "' OR EmpLastName = '" + sName + "' OR  EmpExtID = '" + sName + "' ";
                    }
                }

                sbQuery.Append(empClause);
                sbQuery.Append(" ) ");

                sbQuery.Append(" union ");
                sbQuery.Append(sQuery);
                sbQuery.Append("AND (  ");

                sEmpName = this.EmpName.Split(' ');
                foreach (string sName in sEmpName)
                {
                    if (empClause == "")
                    {
                        empClause += " EmpFirstName like '%" + sName + "%' OR EmpLastName like '%" + sName + "%' OR  EmpExtID like '%" + sName + "%' ";
                    }
                    else
                    {
                        empClause += " OR EmpFirstName like '%" + sName + "%' OR EmpLastName like '%" + sName + "%' OR  EmpExtID like '%" + sName + "%' ";
                    }
                }

                sbQuery.Append(empClause);
                sbQuery.Append(" ) ");
            }
            else
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(sQuery);
                if (BusinessUtility.GetString(this.EmpExtID) != "")
                {
                    sbQuery.Append("  AND ( EmpExtID = '" + this.EmpExtID + "' ) ");
                }

                if (BusinessUtility.GetString(this.Location) != "")
                {
                    sbQuery.Append("  AND ( fun_get_emp_ref_val('" + EmpRefCode.Store + "', empHdrID) = '" + this.Location + "' ) ");
                }
            }

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtEmployeeList = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmployeeList;
        }

        /// <summary>
        /// To Check if UserID Exists or Not in EmpHeader Table
        /// </summary>
        /// <param name="empLogInID">Pass Employee External ID/UserID</param>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>True/False</returns>
        public Boolean EmployeeIDExists(string empLogInID, int empID)
        {
            StringBuilder sbValidate = new StringBuilder();
            bool bReturn = false;
            sbValidate.Append(" SELECT empHdrID FROM empheader WHERE EmpExtID = @EmpLogInID ");
            if (empID > 0)
            {
                sbValidate.Append(" AND empHdrID !=" + empID + " ");
            }

            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(BusinessUtility.GetString(sbValidate), CommandType.Text,
                new MySqlParameter[] { DbUtility.GetParameter("EmpLogInID", empLogInID, MyDbType.String) });
                int rValue = BusinessUtility.GetInt(scalar);
                if (rValue > 0)
                {
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Check if EmailID Exists or Not in empheader Table
        /// </summary>
        /// <param name="empEmail">Pass Employee Email ID</param>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>True/False</returns>
        public Boolean EmployeeEmailExists(string empEmail, int empID)
        {
            StringBuilder sbValidate = new StringBuilder();
            bool bReturn = false;
            sbValidate.Append(" SELECT empHdrID FROM empheader WHERE empEmail = @empEmail ");
            if (empID > 0)
            {
                sbValidate.Append(" AND empHdrID !=" + empID + " ");
            }

            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(BusinessUtility.GetString(sbValidate), CommandType.Text,
                new MySqlParameter[] { DbUtility.GetParameter("empEmail", empEmail, MyDbType.String) });
                int rValue = BusinessUtility.GetInt(scalar);
                if (rValue > 0)
                {
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Check if Reference Value Exists or Not From empHeader Table
        /// </summary>
        /// <param name="empRefType">Pass Employee Refrence Code</param>
        /// <param name="empRefValue">Pass Employee Refrence Code Value</param>
        /// <returns>True/False</returns>
        public Boolean IsValidEmpRef(string empRefType, string empRefValue)
        {
            StringBuilder sbValidate = new StringBuilder();
            bool bReturn = false;
            sbValidate.Append(" SELECT empHdrID ");
            sbValidate.Append(" FROM empheader WHERE 1 = 1 ");
            sbValidate.Append("  AND ( fun_get_emp_ref_val('" + empRefType + "', empHdrID) = '" + empRefValue + "' ) ");
            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(BusinessUtility.GetString(sbValidate), CommandType.Text,
                null);
                int rValue = BusinessUtility.GetInt(scalar);
                if (rValue > 0)
                {
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Save Employee and Employee Reference Code Value in empheaderTable and empreference Table
        /// </summary>
        /// <returns>True/False</returns>
        public Boolean InsertEmployee()
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            bool bReturn = false;
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" INSERT INTO empheader (empPassword, empFirstName, empLastName, empCreatedOn, empEmail, empActive, empNotifyPref, empLastUpdatedOn, empExtID, empLoginID, emptype,  ");
                sbInsertQuery.Append(" sysSates_sysStateCode, sysCountries_sysCountryCode, createdSource, PensivoUUID)");
                sbInsertQuery.Append(" VALUES (MD5(@empPassword), @empFirstName, @empLastName, @empCreatedOn, @empEmail, @empActive, @empNotifyPref, @empLastUpdatedOn, @empExtID, @empLoginID, @emptype,  ");
                sbInsertQuery.Append("  @sysSates_sysStateCode, @sysCountries_sysCountryCode, @createdSource, @pensivoUUID");
                sbInsertQuery.Append(" ) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empPassword",  this.EmpPassword , MyDbType.String),    
                            DbUtility.GetParameter("empFirstName", this.EmpFirstName, MyDbType.String),
                            DbUtility.GetParameter("empLastName", this.EmpLastName, MyDbType.String),
                            DbUtility.GetParameter("empCreatedOn", DateTime.Now, MyDbType.DateTime),
                            DbUtility.GetParameter("empEmail", this.EmpEmail, MyDbType.String),
                            DbUtility.GetParameter("empActive", 1, MyDbType.Int),
                            DbUtility.GetParameter("empNotifyPref", "1", MyDbType.String),
                            DbUtility.GetParameter("empLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                            DbUtility.GetParameter("empExtID", this.EmpLogInID , MyDbType.String),
                            DbUtility.GetParameter("empLoginID", this.EmpLogInID , MyDbType.String),
                            DbUtility.GetParameter("emptype", "E" , MyDbType.String),
                            DbUtility.GetParameter("sysSates_sysStateCode", this.EmpStateCode , MyDbType.String),
                            DbUtility.GetParameter("sysCountries_sysCountryCode", this.EmpCountryCode , MyDbType.String),
                            DbUtility.GetParameter("createdSource", this.CreatedSource , MyDbType.String),
                            DbUtility.GetParameter("pensivoUUID", this.PensivoUUID , MyDbType.String)
                            });

                int iInsertedEmpID = dbTransactionHelper.GetLastInsertID();

                if (iInsertedEmpID > 0)
                {
                    if (this.Division != "")
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Division, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.Division , MyDbType.String)
                                    });
                    }

                    if (this.Region != "")
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Region, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.Region, MyDbType.String)
                                    });
                    }

                    if (this.District != "")
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.District, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.District, MyDbType.String)
                                    });
                    }

                    if (this.Department != "")
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Department, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.Department, MyDbType.String)
                                    });
                    }

                    if (this.JobCode != "")
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.JobCode, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.JobCode, MyDbType.String)
                                    });
                    }


                    if (this.Location != "")
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Store, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.Location, MyDbType.String)
                                    });
                    }


                    if (this.Type != "")
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Type, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.Type, MyDbType.String)
                                    });
                    }


                    if (this.TypeParsed != "")
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.TypeParsed, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.TypeParsed, MyDbType.String)
                                    });
                    }


                    if (this.EmpLogInID.Length <= 5)
                    {
                        RoleManageLists objRoleManageList = new RoleManageLists();
                        string sCSite = string.Empty;
                        sCSite = objRoleManageList.GetFixedUserCSite(this.EmpLogInID);

                        if (sCSite == "")
                        {
                            sCSite = this.Location;
                        }
                        if (sCSite != "")
                        {
                            sCSite = sCSite.PadLeft(4, '0');
                            ImportPensivoUsers objImportPensivoUser = new ImportPensivoUsers();
                            if (objImportPensivoUser.sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.CSite, sCSite))
                            {
                                sbInsertQuery = new StringBuilder();
                                sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                sbInsertQuery.Append(" ) ");

                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.CSite, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sCSite, MyDbType.String)
                                            });
                            }
                        }
                    }
                }

                dbTransactionHelper.CommitTransaction();
                this.EmpID = iInsertedEmpID;
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Update Employee/User Detail and Employee Reference Code Value in empheader and empreference Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>True/False</returns>
        public Boolean UpdateEmployeeDetail(int empID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                StringBuilder sbDelete = new StringBuilder();



                StringBuilder sbUpdate = new StringBuilder();
                sbUpdate.Append(" INSERT INTO empheaderhistory (emp_HdrID, empPassword, empFirstName, empLastName, empCreatedOn, empExtID, empEmail, empActive, empLoginID, isDeleted, PensivoUUID, createdon ) ");
                sbUpdate.Append(" select ");
                sbUpdate.Append(" empHdrID, empPassword, empFirstName, empLastName, empCreatedOn, empExtID, empEmail, empActive, empLoginID, isDeleted, PensivoUUID, now() ");
                sbUpdate.Append(" FROM empheader Where empHdrID = @userID; ");

                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbUpdate), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("userID", empID, MyDbType.Int),
                                });

                int iHistoryID = dbTransactionHelper.GetLastInsertID();
                if (iHistoryID > 0)
                {
                    sbUpdate = new StringBuilder();
                    sbUpdate.Append(" insert into empreferencehistroy ");
                    sbUpdate.Append(" (historyID, empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue ) ");
                    sbUpdate.Append(" select @HistoryID, empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue from empreference WHERE empHeader_empHdrID =@userID; ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbUpdate), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("userID", empID, MyDbType.Int),
                                    DbUtility.GetParameter("HistoryID", iHistoryID, MyDbType.Int),
                                    });
                }

                sbInsertQuery.Append(" Update empheader Set empFirstName = @empFirstName, empLastName = @empLastName, empEmail = @empEmail, empActive = @empActive, empNotifyPref = @empNotifyPref,   ");
                sbInsertQuery.Append(" empLastUpdatedOn = @empLastUpdatedOn, empExtID = @empExtID, empLoginID = @empLoginID,  sysSates_sysStateCode = @sysSates_sysStateCode, ");
                sbInsertQuery.Append(" sysCountries_sysCountryCode = @sysCountries_sysCountryCode Where empHdrID = @empID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empID",  empID , MyDbType.Int), 
                            DbUtility.GetParameter("empFirstName", this.EmpFirstName, MyDbType.String),
                            DbUtility.GetParameter("empLastName", this.EmpLastName, MyDbType.String),
                            DbUtility.GetParameter("empCreatedOn", DateTime.Now, MyDbType.DateTime),
                            DbUtility.GetParameter("empEmail", this.EmpEmail, MyDbType.String),
                            DbUtility.GetParameter("empActive", 1, MyDbType.Int),
                            DbUtility.GetParameter("empNotifyPref", "1", MyDbType.String),
                            DbUtility.GetParameter("empLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                            DbUtility.GetParameter("empExtID", this.EmpLogInID , MyDbType.String),
                            DbUtility.GetParameter("empLoginID", this.EmpLogInID , MyDbType.String),
                            DbUtility.GetParameter("sysSates_sysStateCode", this.EmpStateCode , MyDbType.String),
                            DbUtility.GetParameter("sysCountries_sysCountryCode", this.EmpCountryCode , MyDbType.String),
                            });

                int iInsertedEmpID = empID;
                if (iInsertedEmpID > 0)
                {
                    if (this.Division != "")
                    {
                        sbDelete = new StringBuilder();
                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Division, MyDbType.String),
                                    });


                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Division, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.Division , MyDbType.String)
                                    });
                    }

                    if (this.Region != "")
                    {
                        sbDelete = new StringBuilder();
                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Region, MyDbType.String),
                                    });

                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Region, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.Region, MyDbType.String)
                                    });
                    }

                    if (this.District != "")
                    {
                        sbDelete = new StringBuilder();
                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.District, MyDbType.String),
                                    });

                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.District, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.District, MyDbType.String)
                                    });
                    }

                    if (this.Department != "")
                    {
                        sbDelete = new StringBuilder();
                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Department, MyDbType.String),
                                    });

                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Department, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.Department, MyDbType.String)
                                    });
                    }

                    if (this.JobCode != "")
                    {
                        sbDelete = new StringBuilder();
                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.JobCode, MyDbType.String),
                                    });

                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.JobCode, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.JobCode, MyDbType.String)
                                    });
                    }


                    if (this.Location != "")
                    {
                        sbDelete = new StringBuilder();
                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Store, MyDbType.String),
                                    });

                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Store, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.Location, MyDbType.String)
                                    });
                    }


                    if (this.Type != "")
                    {
                        sbDelete = new StringBuilder();
                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Type, MyDbType.String),
                                    });

                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Type, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.Type, MyDbType.String)
                                    });
                    }

                    if (this.TypeParsed != "")
                    {
                        sbDelete = new StringBuilder();
                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.TypeParsed, MyDbType.String),
                                    });

                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                        sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.TypeParsed, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", this.TypeParsed, MyDbType.String)
                                    });
                    }

                    if (this.EmpLogInID.Length <= 5)
                    {
                        RoleManageLists objRoleManageList = new RoleManageLists();
                        string sCSite = string.Empty;
                        sCSite = objRoleManageList.GetFixedUserCSite(this.EmpLogInID);

                        if (sCSite == "")
                        {
                            sCSite = this.Location;
                        }
                        if (sCSite != "")
                        {
                            sCSite = sCSite.PadLeft(4, '0');
                            ImportPensivoUsers objImportPensivoUser = new ImportPensivoUsers();
                            if (objImportPensivoUser.sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.CSite, sCSite))
                            {
                                sbDelete = new StringBuilder();
                                sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.CSite, MyDbType.String),
                                    });

                                sbInsertQuery = new StringBuilder();
                                sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                sbInsertQuery.Append(" ) ");
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.CSite, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sCSite, MyDbType.String)
                                    });
                            }
                        }
                    }

                }

                if (this.EmpNewPassword != "")
                {
                    Employee objEmp = new Employee();
                    objEmp.GetEmployeeDetail(empID);

                    if (objEmp.EmpPassword != this.EmpNewPassword)
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" Update empheader Set  empPassword =  MD5(@empPassword) ");
                        sbInsertQuery.Append("  Where empHdrID = @empID ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empID",  empID , MyDbType.Int), 
                            DbUtility.GetParameter("empPassword",  this.EmpNewPassword , MyDbType.String),    
                            });
                    }
                }

                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Save Employee/User Short Detail in EmpHeader Table
        /// </summary>
        /// <returns>True/False</returns>
        public Boolean InsertEmployeeShortDesc()
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" INSERT INTO empheader (empPassword, empCreatedOn, empActive, empLastUpdatedOn, empExtID, empLoginID, createdSource, PensivoUUID ");
                sbInsertQuery.Append(" )");
                sbInsertQuery.Append(" VALUES (MD5(@empPassword), @empCreatedOn, @empActive,@empLastUpdatedOn, @empExtID, @empLoginID, @createdSource, @PensivoUUID ");
                sbInsertQuery.Append(" ) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empPassword",  this.EmpPassword , MyDbType.String),    
                            DbUtility.GetParameter("empCreatedOn", DateTime.Now, MyDbType.DateTime),
                            DbUtility.GetParameter("empActive", 1, MyDbType.Int),
                            DbUtility.GetParameter("empLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                            DbUtility.GetParameter("empExtID", this.EmpLogInID , MyDbType.String),
                            DbUtility.GetParameter("empLoginID", this.EmpLogInID , MyDbType.String),
                            DbUtility.GetParameter("createdSource", this.CreatedSource , MyDbType.String),
                            DbUtility.GetParameter("PensivoUUID", this.PensivoUUID , MyDbType.String),
                            });
                int iInsertedEmpID = dbTransactionHelper.GetLastInsertID();
                this.EmpID = iInsertedEmpID;
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Update Employee First/Last Name in EmpHeader Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>True/False</returns>
        public Boolean UpdateEmployeeFirstLastName(int empID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();

                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" UPDATE empheader SET empFirstName = @empFirstName, empLastName = @empLastName, empLastUpdatedOn = @empLastUpdatedOn  WHERE empHdrID = @empHdrID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("@empHdrID", empID, MyDbType.String),
                DbUtility.GetParameter("@empFirstName", this.EmpFirstName, MyDbType.String),
                DbUtility.GetParameter("@empLastName", this.EmpLastName, MyDbType.String),
                DbUtility.GetParameter("@empLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                });

                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Get Question List From employeequestions Table
        /// </summary>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeQuestion(string lang)
        {
            DataTable dtEmployeeQuestion = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" select idemployeequestions   ");

            if (lang == AppLanguageCode.EN)
            {
                sbQuery.Append(" , question as question ");
            }
            else
            {
                sbQuery.Append(" , questionfr as question  ");
            }

            sbQuery.Append(" from employeequestions ");

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtEmployeeQuestion = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmployeeQuestion;
        }



        /// <summary>
        /// To Get Employee Selected Question From employeequestion table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeSelectedQuestionList(int empID, string lang)
        {
            DataTable dtEmployeeSelectedQuestionList = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" select Distinct empQs.idemployeequestions ");

            if (lang == AppLanguageCode.EN)
            {
                sbQuery.Append(" , empQs.question as question ");
            }
            else
            {
                sbQuery.Append(" , empQs.questionfr as question  ");
            }

            sbQuery.Append(" from employeeanswer as empAns ");
            sbQuery.Append(" inner join employeequestions as empQs on empQs.idemployeequestions = empAns.questionid and  empAns.emp_headerid=@empID ");


            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtEmployeeSelectedQuestionList = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empID",  empID , MyDbType.Int)
                            });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmployeeSelectedQuestionList;
        }

        /// <summary>
        /// To Save Employee Question/Answer in employeeAnswer Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="questionID">Pass Question ID</param>
        /// <param name="empAnswer">Pass Employee Answer</param>
        /// <returns>True/False</returns>
        public Boolean InsertEmployeeAnswer(int empID, int questionID, string empAnswer)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            bool bReturn = false;
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" INSERT INTO employeeanswer (emp_headerid, questionid, empanswer ");
                sbInsertQuery.Append(" )");
                sbInsertQuery.Append(" VALUES (@emp_headerid, @questionid, @empanswer ");
                sbInsertQuery.Append(" ) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("emp_headerid",  empID , MyDbType.Int),    
                            DbUtility.GetParameter("questionid", questionID, MyDbType.Int),
                            DbUtility.GetParameter("empanswer", empAnswer.ToLower() , MyDbType.String),
                            });
                int iInsertedEmpID = dbTransactionHelper.GetLastInsertID();
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Update Employee Password in EmpHeader Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="empPass">Pass Employee Password</param>
        /// <returns>True/False</returns>
        public Boolean UpdateEmployeePassword(int empID, string empPass)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            bool bReturn = false;
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" UPDATE empheader SET empPassword = MD5(@empPassword), empLastUpdatedOn =@empLastUpdatedOn  WHERE empHdrID = @empHdrID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("@empHdrID", empID, MyDbType.String),
                DbUtility.GetParameter("@empPassword", empPass, MyDbType.String),
                DbUtility.GetParameter("@empLastUpdatedOn", DateTime.Now, MyDbType.DateTime)
                
                });

                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Save Employee Reference Code Value in Table "empreference" 
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="refCode">Pass Employee Reference Code</param>
        /// <param name="refValue">Pass Employee Reference Code Value</param>
        /// <returns>True/False</returns>
        public Boolean InsertEmployeeSysRef(int empID, string refCode, string refValue)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            bool bReturn = false;
            dbTransactionHelper.BeginTransaction();
            try
            {

                if (refCode == EmpRefCode.CSite)
                {
                    Employee objEmp = new Employee();
                    objEmp.GetEmployeeDetail(empID);

                    RoleManageLists objRoleManageList = new RoleManageLists();
                    string sCSite = string.Empty;
                    sCSite = objRoleManageList.GetFixedUserCSite(objEmp.EmpExtID);

                    if (sCSite != "")
                    {
                        refValue = sCSite;
                    }
                }


                ImportPensivoUsers objImportPensivoUser = new ImportPensivoUsers();
                objImportPensivoUser.sysRefCreateNotExist(dbTransactionHelper, refCode, refValue);

                StringBuilder sbDelete = new StringBuilder();
                sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", empID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", refCode, MyDbType.String),
                                    });

                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                sbInsertQuery.Append(" ) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", empID, MyDbType.Int),    
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", refCode, MyDbType.String),
                                    DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", refValue, MyDbType.String)
                                    });
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Check if UserID Exists or Not From EmpHeader Table
        /// </summary>
        /// <param name="sLoginID">Pass Employee External ID</param>
        /// <returns>True/False</returns>
        public Boolean IsLoginIDExists(string sLoginID)
        {
            bool bReturn = false;
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select EmpExtID from empheader ");
                sbInsertQuery.Append(" Where EmpExtID = @empLoginID ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empLoginID", sLoginID , MyDbType.String),
                });

                if (string.IsNullOrEmpty(BusinessUtility.GetString(rValue)))
                {
                    bReturn = true;
                }

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Check if UserID and UUID Exist or Not From EmpHeader Table
        /// </summary>
        /// <param name="sLoginID">Pass Employee External ID</param>
        /// <param name="sPensivoUUID">Pass Pensivo UUID</param>
        /// <returns>True/False</returns>
        public Boolean IsLoginIDUUIDExists(string sLoginID, string sPensivoUUID)
        {
            bool bReturn = false;
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select EmpExtID from empheader ");
                sbInsertQuery.Append(" Where EmpExtID = @empLoginID AND PensivoUUID = @PensivoUUID ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empLoginID", sLoginID , MyDbType.String),
                DbUtility.GetParameter("PensivoUUID", sPensivoUUID , MyDbType.String),
                });

                bReturn = ((BusinessUtility.GetString(rValue) == "") && rValue == null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Check if UUID Exists or Not From EmpHeader Table
        /// </summary>
        /// <param name="sPensivoUUID">Pass Pensivo UUID</param>
        /// <returns>True/False</returns>
        public Boolean IsUUIDExists(string sPensivoUUID)
        {
            bool bReturn = false;
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select EmpExtID from empheader ");
                sbInsertQuery.Append(" Where PensivoUUID = @PensivoUUID ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("PensivoUUID", sPensivoUUID , MyDbType.String),
                });

                bReturn = ((BusinessUtility.GetString(rValue) == "") && rValue == null);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Check if Employee/User Answered The Question or Not From EmployeeAnswer Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>True/False</returns>
        public Boolean IsEmployeeAnswerSecurityQuestion(int empID)
        {
            bool bReturn = false;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" select idemployeeanswer from employeeanswer where emp_headerid = @empID  ");

            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text,
                new MySqlParameter[] { DbUtility.GetParameter("empID", empID, MyDbType.Int) });
                int rValue = BusinessUtility.GetInt(scalar);
                bReturn = rValue > 0;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }


        /// <summary>
        /// To Get User Save There Two Question To Varify OR Not During Reset Password Process
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>True/False</returns>
        public int IsEmployeeSavedAllTwoQuestion(int empID)
        {
            int bReturn = 0;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" select count( idemployeeanswer) from employeeanswer where emp_headerid = @empID  ");

            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text,
                new MySqlParameter[] { DbUtility.GetParameter("empID", empID, MyDbType.Int) });
                int rValue = BusinessUtility.GetInt(scalar);
                bReturn = rValue;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Get Employee/User ID based on Their LoginID From EmpHeader Table
        /// </summary>
        /// <param name="sLoginID">Pass Employee External ID</param>
        /// <returns>int</returns>
        public int GetEmpID(string sLoginID)
        {
            int iEmpID = 0;
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select empHdrID from empheader ");
                sbInsertQuery.Append(" Where EmpExtID = @empLoginID ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empLoginID", sLoginID , MyDbType.String),
                });
                iEmpID = BusinessUtility.GetInt(rValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return iEmpID;
        }

        /// <summary>
        /// To Get Employee/User Selected Question From EmployeeAnswer Table 
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>int</returns>
        public int GetEmployeeSelectedQuestion(int empID)
        {
            int iEmployeeSelectedQuestion = 0;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT questionid FROM employeeanswer where emp_headerid = @empid   ");
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empid", empID , MyDbType.Int),
                });
                iEmployeeSelectedQuestion = BusinessUtility.GetInt(rValue);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return iEmployeeSelectedQuestion;
        }

        /// <summary>
        /// To Check if Employee/User Question/Answer is Valid or not From EmployeeAnswer Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="questionID">Pass Employeed Question ID</param>
        /// <param name="empAnswer">Pass Employee Answer</param>
        /// <returns>True/False</returns>
        public Boolean ValidadateEmployeeAnswer(int empID, int questionID, string empAnswer)
        {
            bool bReturn = false;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT idemployeeanswer FROM employeeanswer where emp_headerid = @empid and  questionid = @empQstID and empanswer = @answer  ");
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empid", empID , MyDbType.Int),
                DbUtility.GetParameter("empQstID", questionID , MyDbType.Int),
                DbUtility.GetParameter("answer", empAnswer.ToLower() , MyDbType.String),
                });

                bReturn = (BusinessUtility.GetInt(rValue) > 0);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }


        /// <summary>
        /// To Get EmpID on New Username From emphistoricusernames Table
        /// </summary>
        /// <param name="empExtID">Pass Employee External ID</param>
        /// <returns>Int</returns>
        public int GetEmpIDChanged(string empExtID)
        {
            int rValue = 0;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" select empHdrID  from emphistoricusernames where empHistoricExtID = @empExtID and isActive = 1 ");

            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text,
                new MySqlParameter[] { DbUtility.GetParameter("empExtID", empExtID, MyDbType.String) });
                rValue = BusinessUtility.GetInt(scalar);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return rValue;
        }

        /// <summary>
        /// Get Employee Refrence Code Value
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="refCode">Pass Refrence Code</param>
        /// <returns>String</returns>
        public string GetEmpRefCodeValue(int empID, string refCode)
        {
            DbHelper dbHelper = new DbHelper(false);
            string sReturn = string.Empty;
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append("select fun_get_emp_ref_val(@refCode, @empID) ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empID", empID, MyDbType.Int),
                DbUtility.GetParameter("refCode", refCode, MyDbType.String),
                });
                sReturn = BusinessUtility.GetString(rValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return sReturn;
        }

        /// <summary>
        /// Save Sys Error Log for user not created
        /// </summary>
        /// <param name="empExtID">Pass User ID</param>
        /// <returns>True/False</returns>
        public Boolean SaveSysErrorLog(string empExtID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" INSERT INTO syserror (empExtID, createdDateTime ");
                sbInsertQuery.Append(" )");
                sbInsertQuery.Append(" VALUES (@empExtID, @empCreatedOn");
                sbInsertQuery.Append(" ) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empExtID",  empExtID , MyDbType.String),    
                            DbUtility.GetParameter("empCreatedOn", DateTime.Now, MyDbType.DateTime)
                            });
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }


        /// <summary>
        /// Get Employee Other Sites From Table empotherreportsites
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetEmployeeOtherSites(int empID)
        {
            DataTable dtEmployeeOtherSites = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" select empHdrID, SiteNo from empotherreportsites where empHdrID = @empID  ");
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtEmployeeOtherSites = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text,
                    new MySqlParameter[] { DbUtility.GetParameter("empID", empID, MyDbType.Int) });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmployeeOtherSites;
        }

        ///// <summary>
        ///// Get Employee Other Sites From Table empotherreportsites
        ///// </summary>
        ///// <param name="empID">Pass Employee ID</param>
        ///// <returns>DataTable</returns>
        //public DataTable GetEmployeeOtherSites(int empID)
        //{
        //    DataTable dtEmployeeOtherSites = new DataTable();
        //    StringBuilder sbQuery = new StringBuilder();
        //    //sbQuery.Append(" select empHdrID, SiteNo from empotherreportsites where empHdrID = @empID and isActive = 1  ");
        //    sbQuery.Append(" select empLstIncl.empListInclEmpID as empHdrID, empLstIncl.sysRefCodeValue as SiteNo  ");
        //    sbQuery.Append(" from empmstlistincl empLstIncl ");
        //    sbQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID = empLstIncl.empMstListTblID ");
        //    sbQuery.Append(" and empLstIncl.empListInclEmpID =@empID and mstLstTbl.mstListID=1 ");
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        dtEmployeeOtherSites = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text,
        //            new MySqlParameter[] { DbUtility.GetParameter("empID", empID, MyDbType.Int) });
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.CreateLog(ex);
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //    return dtEmployeeOtherSites;
        //}


        /// <summary>
        /// To Get Employee Site Logistics
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeSiteLogistics(int empID)
        {
            DataTable dtEmployeeOtherSites = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            //sbQuery.Append(" select empListInclEmpID as empHdrID, sysRefCodeValue as SiteNo from empmstlistincl   where empMstListTblID = 2 and empListInclEmpID =@empID; ");

            sbQuery.Append(" select empLstIncl.empListInclEmpID as empHdrID, empLstIncl.sysRefCodeValue as SiteNo  ");
            sbQuery.Append(" from empmstlistincl empLstIncl ");
            sbQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID = empLstIncl.empMstListTblID ");
            sbQuery.Append(" and empLstIncl.empListInclEmpID =@empID and mstLstTbl.mstListID=2 ");

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtEmployeeOtherSites = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text,
                    new MySqlParameter[] { DbUtility.GetParameter("empID", empID, MyDbType.Int) });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmployeeOtherSites;
        }


        /// <summary>
        /// To Get Employee Report Sites
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetEmployeeSites(int empID)
        {
            DataTable dtEmployeeOtherSites = new DataTable();
            StringBuilder sbQuery = new StringBuilder();

            sbQuery.Append(" Select Distinct *  from (");

            sbQuery.Append(" select  iqSite.refCodeValue  as SiteNo from empheader   ");//empheader.emphdrID as empHdrID,
            sbQuery.Append(" JOIN ( ");
            sbQuery.Append(" SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'STR') AS iqSite ON iqSite.iEmpHdrID = empheader.empHdrID ");

            sbQuery.Append(" join (     ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID      ");
            sbQuery.Append(" from empreference      ");
            sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'      ");
            sbQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID      ");

            sbQuery.Append(" left join (     ");
            sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID     ");
            sbQuery.Append(" from empreference      ");
            sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'    ");
            sbQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID      ");

            sbQuery.Append(" and	((iqDiv.empDivID = 'RETL')   OR  (iqDiv.empDivID ='WR')  )  and ( iqJob.empJobID)     ");
            sbQuery.Append(" in (      ");
            sbQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,      ");
            sbQuery.Append(" 0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,      ");
            sbQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,      ");
            sbQuery.Append(" 1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760,      ");
            sbQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,      ");
            sbQuery.Append(" 1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921,      ");
            sbQuery.Append(" 1930,1931,1940,1941,1950      ");
            sbQuery.Append(" )  ");


            sbQuery.Append(" JOIN employeehasfunctionality empFun ON empFun.empHdrID = empheader.emphdrID and empheader.emphdrID = @empID ");
            sbQuery.Append(" and (empheader.isDeleted = 0 or empheader.isDeleted is null) and empheader.empActive = 1  AND empFun.reporting > 0 ");

            sbQuery.Append(" AND iqSite.refCodeValue NOT IN ( ");
            sbQuery.Append(" SELECT  ");
            sbQuery.Append(" sysrefcodeValue ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistexcl lstexcl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
            sbQuery.Append(" AND mstLstTbl.mstListID = 1 ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.empListExclEmpID = @empID ");
            sbQuery.Append(" AND lstexcl.sysRefCode = 'STR'      ");
            sbQuery.Append(" ) ");


            sbQuery.Append(" UNION  ");

            sbQuery.Append(" SELECT  ");
            sbQuery.Append("  iqSite.refCodeValue  as SiteNo ");    //empheader.emphdrID AS empHdrID,
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empheader ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'STR') AS iqSite ON iqSite.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'CSITE') AS iqCSite ON iqCSite.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empJobID, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'JOB') AS iqJob ON iqJob.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empDivID, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'DIV') AS iqDiv ON iqDiv.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN employeehasfunctionality empFun ON empFun.empHdrID = empheader.emphdrID ");
            sbQuery.Append(" AND empheader.emphdrID = @empID ");
            sbQuery.Append(" AND (empheader.isDeleted = 0 ");
            sbQuery.Append(" OR empheader.isDeleted IS NULL) ");
            sbQuery.Append(" AND empheader.empActive = 1 ");
            sbQuery.Append(" AND empFun.reporting > 0 ");
            sbQuery.Append(" AND iqSite.refCodeValue NOT IN (SELECT  ");
            sbQuery.Append(" sysrefcodeValue ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistexcl lstexcl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
            sbQuery.Append(" AND mstLstTbl.mstListID = 1 ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
            sbQuery.Append(" AND lstexcl.empListExclEmpID = @empID ");
            sbQuery.Append(" AND lstexcl.sysRefCode = 'STR')  ");

            sbQuery.Append(" union ");

            sbQuery.Append(" select  empLstIncl.sysRefCodeValue as SiteNo  ");  //empLstIncl.empListInclEmpID as empHdrID,
            sbQuery.Append(" from empmstlistincl empLstIncl ");
            sbQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID = empLstIncl.empMstListTblID ");
            sbQuery.Append(" and empLstIncl.empListInclEmpID =@empID and mstLstTbl.mstListID=1 ");

            sbQuery.Append(" and empLstIncl.sysRefCodeValue ");
            sbQuery.Append(" NOT IN (   ");
            sbQuery.Append(" SELECT    ");
            sbQuery.Append(" sysrefcodeValue   ");
            sbQuery.Append(" FROM   ");
            sbQuery.Append(" empmstlistexcl lstexcl   ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID   ");
            sbQuery.Append(" AND mstLstTbl.mstListID = 1   ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.empListExclEmpID = @empID  ");
            sbQuery.Append("  AND lstexcl.sysRefCode = 'STR'        ");
            sbQuery.Append(" )  ");

            sbQuery.Append(" 	Join empheader emphdr ON emphdr.empHdrID = empLstIncl.empListInclEmpID    ");
            sbQuery.Append(" AND (emphdr.isDeleted = 0 ");
            sbQuery.Append(" OR emphdr.isDeleted IS NULL) ");
            sbQuery.Append(" AND emphdr.empActive = 1 ");

            sbQuery.Append(" union ");
            sbQuery.Append(" select  empLstIncl.sysRefCodeValue as SiteNo  ");  // empLstIncl.empListInclEmpID as empHdrID,
            sbQuery.Append(" from empmstlistincl empLstIncl ");
            sbQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID = empLstIncl.empMstListTblID ");
            sbQuery.Append(" and empLstIncl.empListInclEmpID =@empID and mstLstTbl.mstListID=2 ");
            sbQuery.Append(" and empLstIncl.sysRefCodeValue ");
            sbQuery.Append(" NOT IN (  ");
            sbQuery.Append(" SELECT  ");
            sbQuery.Append(" sysrefcodeValue   ");
            sbQuery.Append(" FROM   ");
            sbQuery.Append(" empmstlistexcl lstexcl   ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID   ");
            sbQuery.Append(" AND mstLstTbl.mstListID = 2   ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID  and lstexcl.empListExclEmpID = @empID ");
            sbQuery.Append("   AND lstexcl.sysRefCode = 'STR'    ");

            sbQuery.Append(" )  ");

            sbQuery.Append(" Join empheader emphdr ON emphdr.empHdrID = empLstIncl.empListInclEmpID    ");
            sbQuery.Append(" AND (emphdr.isDeleted = 0 ");
            sbQuery.Append(" OR emphdr.isDeleted IS NULL) ");
            sbQuery.Append(" AND emphdr.empActive = 1 ");

            sbQuery.Append(" union ");

            sbQuery.Append(" SELECT  ");
            //sbQuery.Append(" empheader.emphdrID AS empHdrID, ");
            sbQuery.Append(" iqSite.refCodeValue AS SiteNo ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empheader ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'STR') AS iqSite ON iqSite.iEmpHdrID = empheader.empHdrID   ");

            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'TDIST') AS iqDist ON iqDist.iEmpHdrID = empheader.empHdrID   ");

            sbQuery.Append(" JOIN     ");

            sbQuery.Append(" (     ");
            sbQuery.Append(" SELECT  ");
            sbQuery.Append(" empLstIncl.sysRefCodeValue AS DistNo ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistincl empLstIncl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = empLstIncl.empMstListTblID ");
            sbQuery.Append(" AND empLstIncl.empListInclEmpID = @empID ");
            sbQuery.Append(" AND ( mstLstTbl.mstListID = 4 OR mstLstTbl.mstListID = 5 ) ");
            sbQuery.Append(" AND empLstIncl.sysRefCodeValue NOT IN (SELECT  ");
            sbQuery.Append(" sysrefcodeValue ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistexcl lstexcl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
            sbQuery.Append(" AND  (mstLstTbl.mstListID = 4 OR mstLstTbl.mstListID = 5) ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
            sbQuery.Append(" AND lstexcl.empListExclEmpID = @empID ");
            sbQuery.Append(" AND lstexcl.sysRefCode = 'TDIST') ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = empLstIncl.empListInclEmpID ");
            sbQuery.Append(" AND (emphdr.isDeleted = 0 ");
            sbQuery.Append(" OR emphdr.isDeleted IS NULL) ");
            sbQuery.Append(" AND emphdr.empActive = 1 ");

            sbQuery.Append(" union ");

            sbQuery.Append(" SELECT  ");

            sbQuery.Append(" iqSite.refCodeValue AS DistNo ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empheader ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'TDIST') AS iqSite ON iqSite.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empJobID, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'JOB') AS iqJob ON iqJob.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empDivID, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'DIV') AS iqDiv ON iqDiv.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" AND (iqJob.empJobID) IN (1602) ");
            sbQuery.Append(" JOIN employeehasfunctionality empFun ON empFun.empHdrID = empheader.emphdrID ");
            sbQuery.Append(" AND empheader.emphdrID = @empID ");
            sbQuery.Append(" AND (empheader.isDeleted = 0 ");
            sbQuery.Append(" OR empheader.isDeleted IS NULL) ");
            sbQuery.Append(" AND empheader.empActive = 1 ");
            sbQuery.Append(" AND empFun.reporting > 0 ");
            sbQuery.Append(" AND iqSite.refCodeValue NOT IN (SELECT  ");
            sbQuery.Append(" sysrefcodeValue ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistexcl lstexcl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
            sbQuery.Append(" AND ( mstLstTbl.mstListID = 4  OR mstLstTbl.mstListID = 5) ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
            sbQuery.Append(" AND lstexcl.empListExclEmpID = @empID ");
            sbQuery.Append(" AND lstexcl.sysRefCode = 'TDIST')      ");

            sbQuery.Append(" ) as resultDistrict on iqDist.refCodeValue  = resultDistrict.DistNo ");



            sbQuery.Append(" ) as Result  order by SiteNo ");


            /*
            sbQuery.Append(" SELECT DISTINCT ");
            sbQuery.Append(" * ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" ( ");
            sbQuery.Append(" SELECT  ");
            sbQuery.Append(" empheader.emphdrID AS empHdrID, ");
            sbQuery.Append(" if(empheader.empExtID in(03565, 03576, 03749, 03785, 03937, 03956, 03971, 03986, 03990, 04015, 04110, 04111, 04283, 04316, 04343, 04477, 04516, 04547, 04582, 04624, 04758,  ");
            sbQuery.Append(" 04826, 04842, 04868, 04979, 05001, 05722, 05972, 06393, 06585, 06640, 06719, 06761, 06804, 07072, 07073, 07148, 07173, 07256, 07403, 07438, 41994,  ");
            sbQuery.Append(" 44156, 44648, 51408, 61402, 61951, 62282, 62380, 63073, 63085, 63453, 63872, 63887, 63888, 64411, 64445, 64455, 64456, 65028, 67464, 69022, 69925,  ");
            sbQuery.Append(" 70019, 70471, 75210, 75218, 75219, 75468, 75572, 75616, 75950, 75960, 76070, 76075, 76080, 76082, 76092, 76264, 76284, 76439, 76442, 76443, 76444,  ");
            sbQuery.Append(" 03681, 04023, 04074, 04079, 04127, 04137, 04150, 04399, 04568, 04621, 04660, 04707, 04978, 05046, 05048, 05899, 06074, 06109, 06320, 06326, 06530,  ");
            sbQuery.Append(" 06721, 06729, 07480, 07546, 07547, 43681, 44462, 44589, 55561, 56164, 56166, 56199, 57170, 57197, 58386, 66457, 66713, 67087, 67517, 03393, 03752,  ");
            sbQuery.Append(" 03942, 03944, 03965, 04005, 04092, 04218, 04251, 04295, 04353, 04422, 04425, 04432, 04444, 04512, 04535, 04548, 04665, 04751, 04820, 04919, 04962,  ");
            sbQuery.Append(" 05060, 05061, 05305, 05621, 06144, 06501, 06671, 06789, 06805, 06873, 07199, 07518, 07545, 53134, 61641, 62183, 62188, 62761, 62789, 62939, 63000,  ");
            sbQuery.Append(" 63475, 63824, 64396, 64463, 64676, 64785, 68869, 75173, 75375, 75644, 75785, 76050, 76366, 76598, 04641, 05268, 05632, 06145, 06519, 06555, 60754,  ");
            sbQuery.Append(" 64743, 64759, 64793, 64876, 75542, 75579, 75588, 76021, 76054, 76117, 76514, 76524, 76525, 76669, 03922, 04030, 04059, 04205, 04586, 05030, 06661,  ");
            sbQuery.Append(" 63683, 75012, 76170, 76342, 03819, 04259, 04960, 04999, 05297, 06373, 61282, 62594, 62810, 68191, 76670, 03407, 04435, 04457, 04558, 04787, 04863,  ");
            sbQuery.Append(" 04956, 05023, 05460, 06359, 06360, 06701, 06780, 06781, 06847, 07059, 07555, 48540, 61631, 63515, 64031, 64102, 64103, 75067, 75451, 75594, 76665,  ");
            sbQuery.Append(" 04639, 04822, 04891, 05022, 06093, 06257, 07004, 07364, 60122, 60685, 70035, 75601, 75705, 76151, 76292, 76512, 76513, 76563, 04213, 04288, 07511,  ");
            sbQuery.Append(" 07542, 68853, 71993, 72115, 72116, 72321, 07200  ), iqCSite.refCodeValue, iqSite.refCodeValue)  as SiteNo ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empheader ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'STR') AS iqSite ON iqSite.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'CSITE') AS iqCSite ON iqCSite.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empJobID, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'JOB') AS iqJob ON iqJob.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empDivID, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'DIV') AS iqDiv ON iqDiv.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" AND ((iqDiv.empDivID = 'RETL') ");
            sbQuery.Append(" OR (iqDiv.empDivID = 'WR')) ");
            sbQuery.Append(" AND (iqJob.empJobID) IN (0030 , 0095, 0401, 0415, 0419, 0425, 0810, 0815, 0819, 0820, 0821, 0824, 0829, 0830, 1002, 1004, 1602, 1609, 1610, 1612, 1616, 1619, 1621, 1631, 1635, 1640, 1653, 1654, 1656, 1660, 1666, 1670, 1681, 1682, 1694, 1708, 1709, 1710, 1711, 1713, 1716, 1724, 1725, 1726, 1730, 1740, 1749, 1760, 1761, 1762, 1767, 1768, 1770, 1772, 1777, 1782, 1789, 1790, 1800, 1801, 1804, 1807, 1813, 1820, 1823, 1826, 1847, 1867, 1910, 1911, 1920, 1921, 1930, 1931, 1940, 1941, 1950) ");
            sbQuery.Append(" JOIN employeehasfunctionality empFun ON empFun.empHdrID = empheader.emphdrID ");
            sbQuery.Append(" AND empheader.emphdrID = @empID ");
            sbQuery.Append(" AND (empheader.isDeleted = 0 ");
            sbQuery.Append(" OR empheader.isDeleted IS NULL) ");
            sbQuery.Append(" AND empheader.empActive = 1 ");
            sbQuery.Append(" AND empFun.reporting > 0 ");
            sbQuery.Append(" AND iqSite.refCodeValue NOT IN (SELECT  ");
            sbQuery.Append(" sysrefcodeValue ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistexcl lstexcl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
            sbQuery.Append(" AND mstLstTbl.mstListID = 1 ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
            sbQuery.Append(" AND lstexcl.empListExclEmpID = @empID ");
            sbQuery.Append(" AND lstexcl.sysRefCode = 'STR')  ");



            sbQuery.Append(" UNION  ");

            sbQuery.Append(" SELECT  ");
            sbQuery.Append(" empheader.emphdrID AS empHdrID, ");
            sbQuery.Append(" if(empheader.empExtID in(03565, 03576, 03749, 03785, 03937, 03956, 03971, 03986, 03990, 04015, 04110, 04111, 04283, 04316, 04343, 04477, 04516, 04547, 04582, 04624, 04758,  ");
            sbQuery.Append(" 04826, 04842, 04868, 04979, 05001, 05722, 05972, 06393, 06585, 06640, 06719, 06761, 06804, 07072, 07073, 07148, 07173, 07256, 07403, 07438, 41994,  ");
            sbQuery.Append(" 44156, 44648, 51408, 61402, 61951, 62282, 62380, 63073, 63085, 63453, 63872, 63887, 63888, 64411, 64445, 64455, 64456, 65028, 67464, 69022, 69925,  ");
            sbQuery.Append(" 70019, 70471, 75210, 75218, 75219, 75468, 75572, 75616, 75950, 75960, 76070, 76075, 76080, 76082, 76092, 76264, 76284, 76439, 76442, 76443, 76444,  ");
            sbQuery.Append(" 03681, 04023, 04074, 04079, 04127, 04137, 04150, 04399, 04568, 04621, 04660, 04707, 04978, 05046, 05048, 05899, 06074, 06109, 06320, 06326, 06530,  ");
            sbQuery.Append(" 06721, 06729, 07480, 07546, 07547, 43681, 44462, 44589, 55561, 56164, 56166, 56199, 57170, 57197, 58386, 66457, 66713, 67087, 67517, 03393, 03752,  ");
            sbQuery.Append(" 03942, 03944, 03965, 04005, 04092, 04218, 04251, 04295, 04353, 04422, 04425, 04432, 04444, 04512, 04535, 04548, 04665, 04751, 04820, 04919, 04962,  ");
            sbQuery.Append(" 05060, 05061, 05305, 05621, 06144, 06501, 06671, 06789, 06805, 06873, 07199, 07518, 07545, 53134, 61641, 62183, 62188, 62761, 62789, 62939, 63000,  ");
            sbQuery.Append(" 63475, 63824, 64396, 64463, 64676, 64785, 68869, 75173, 75375, 75644, 75785, 76050, 76366, 76598, 04641, 05268, 05632, 06145, 06519, 06555, 60754,  ");
            sbQuery.Append(" 64743, 64759, 64793, 64876, 75542, 75579, 75588, 76021, 76054, 76117, 76514, 76524, 76525, 76669, 03922, 04030, 04059, 04205, 04586, 05030, 06661,  ");
            sbQuery.Append(" 63683, 75012, 76170, 76342, 03819, 04259, 04960, 04999, 05297, 06373, 61282, 62594, 62810, 68191, 76670, 03407, 04435, 04457, 04558, 04787, 04863,  ");
            sbQuery.Append(" 04956, 05023, 05460, 06359, 06360, 06701, 06780, 06781, 06847, 07059, 07555, 48540, 61631, 63515, 64031, 64102, 64103, 75067, 75451, 75594, 76665,  ");
            sbQuery.Append(" 04639, 04822, 04891, 05022, 06093, 06257, 07004, 07364, 60122, 60685, 70035, 75601, 75705, 76151, 76292, 76512, 76513, 76563, 04213, 04288, 07511,  ");
            sbQuery.Append(" 07542, 68853, 71993, 72115, 72116, 72321, 07200  ), iqCSite.refCodeValue, iqSite.refCodeValue)  as SiteNo ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empheader ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'STR') AS iqSite ON iqSite.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'CSITE') AS iqCSite ON iqCSite.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empJobID, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'JOB') AS iqJob ON iqJob.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empDivID, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'DIV') AS iqDiv ON iqDiv.iEmpHdrID = empheader.empHdrID ");
            sbQuery.Append(" and  empheader.empExtID in(03565, 03576, 03749, 03785, 03937, 03956, 03971, 03986, 03990, 04015, 04110, 04111, 04283, 04316, 04343, 04477, 04516, 04547, 04582, 04624, 04758,  ");
            sbQuery.Append(" 04826, 04842, 04868, 04979, 05001, 05722, 05972, 06393, 06585, 06640, 06719, 06761, 06804, 07072, 07073, 07148, 07173, 07256, 07403, 07438, 41994,  ");
            sbQuery.Append(" 44156, 44648, 51408, 61402, 61951, 62282, 62380, 63073, 63085, 63453, 63872, 63887, 63888, 64411, 64445, 64455, 64456, 65028, 67464, 69022, 69925,  ");
            sbQuery.Append(" 70019, 70471, 75210, 75218, 75219, 75468, 75572, 75616, 75950, 75960, 76070, 76075, 76080, 76082, 76092, 76264, 76284, 76439, 76442, 76443, 76444,  ");
            sbQuery.Append(" 03681, 04023, 04074, 04079, 04127, 04137, 04150, 04399, 04568, 04621, 04660, 04707, 04978, 05046, 05048, 05899, 06074, 06109, 06320, 06326, 06530,  ");
            sbQuery.Append(" 06721, 06729, 07480, 07546, 07547, 43681, 44462, 44589, 55561, 56164, 56166, 56199, 57170, 57197, 58386, 66457, 66713, 67087, 67517, 03393, 03752,  ");
            sbQuery.Append(" 03942, 03944, 03965, 04005, 04092, 04218, 04251, 04295, 04353, 04422, 04425, 04432, 04444, 04512, 04535, 04548, 04665, 04751, 04820, 04919, 04962,  ");
            sbQuery.Append(" 05060, 05061, 05305, 05621, 06144, 06501, 06671, 06789, 06805, 06873, 07199, 07518, 07545, 53134, 61641, 62183, 62188, 62761, 62789, 62939, 63000,  ");
            sbQuery.Append(" 63475, 63824, 64396, 64463, 64676, 64785, 68869, 75173, 75375, 75644, 75785, 76050, 76366, 76598, 04641, 05268, 05632, 06145, 06519, 06555, 60754,  ");
            sbQuery.Append(" 64743, 64759, 64793, 64876, 75542, 75579, 75588, 76021, 76054, 76117, 76514, 76524, 76525, 76669, 03922, 04030, 04059, 04205, 04586, 05030, 06661,  ");
            sbQuery.Append(" 63683, 75012, 76170, 76342, 03819, 04259, 04960, 04999, 05297, 06373, 61282, 62594, 62810, 68191, 76670, 03407, 04435, 04457, 04558, 04787, 04863,  ");
            sbQuery.Append(" 04956, 05023, 05460, 06359, 06360, 06701, 06780, 06781, 06847, 07059, 07555, 48540, 61631, 63515, 64031, 64102, 64103, 75067, 75451, 75594, 76665,  ");
            sbQuery.Append(" 04639, 04822, 04891, 05022, 06093, 06257, 07004, 07364, 60122, 60685, 70035, 75601, 75705, 76151, 76292, 76512, 76513, 76563, 04213, 04288, 07511,  ");
            sbQuery.Append(" 07542, 68853, 71993, 72115, 72116, 72321, 07200  ) ");
            sbQuery.Append(" JOIN employeehasfunctionality empFun ON empFun.empHdrID = empheader.emphdrID ");
            sbQuery.Append(" AND empheader.emphdrID = @empID ");
            sbQuery.Append(" AND (empheader.isDeleted = 0 ");
            sbQuery.Append(" OR empheader.isDeleted IS NULL) ");
            sbQuery.Append(" AND empheader.empActive = 1 ");
            sbQuery.Append(" AND empFun.reporting > 0 ");
            sbQuery.Append(" AND iqSite.refCodeValue NOT IN (SELECT  ");
            sbQuery.Append(" sysrefcodeValue ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistexcl lstexcl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
            sbQuery.Append(" AND mstLstTbl.mstListID = 1 ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
            sbQuery.Append(" AND lstexcl.empListExclEmpID = @empID ");
            sbQuery.Append(" AND lstexcl.sysRefCode = 'STR')  ");

            sbQuery.Append(" UNION SELECT  ");
            sbQuery.Append(" empLstIncl.empListInclEmpID AS empHdrID, ");
            sbQuery.Append(" if((emphdr.empExtID in(03565, 03576, 03749, 03785, 03937, 03956, 03971, 03986, 03990, 04015, 04110, 04111, 04283, 04316, 04343, 04477, 04516, 04547, 04582, 04624, 04758,  ");
            sbQuery.Append(" 04826, 04842, 04868, 04979, 05001, 05722, 05972, 06393, 06585, 06640, 06719, 06761, 06804, 07072, 07073, 07148, 07173, 07256, 07403, 07438, 41994,  ");
            sbQuery.Append(" 44156, 44648, 51408, 61402, 61951, 62282, 62380, 63073, 63085, 63453, 63872, 63887, 63888, 64411, 64445, 64455, 64456, 65028, 67464, 69022, 69925,  ");
            sbQuery.Append(" 70019, 70471, 75210, 75218, 75219, 75468, 75572, 75616, 75950, 75960, 76070, 76075, 76080, 76082, 76092, 76264, 76284, 76439, 76442, 76443, 76444,  ");
            sbQuery.Append(" 03681, 04023, 04074, 04079, 04127, 04137, 04150, 04399, 04568, 04621, 04660, 04707, 04978, 05046, 05048, 05899, 06074, 06109, 06320, 06326, 06530,  ");
            sbQuery.Append(" 06721, 06729, 07480, 07546, 07547, 43681, 44462, 44589, 55561, 56164, 56166, 56199, 57170, 57197, 58386, 66457, 66713, 67087, 67517, 03393, 03752,  ");
            sbQuery.Append(" 03942, 03944, 03965, 04005, 04092, 04218, 04251, 04295, 04353, 04422, 04425, 04432, 04444, 04512, 04535, 04548, 04665, 04751, 04820, 04919, 04962,  ");
            sbQuery.Append(" 05060, 05061, 05305, 05621, 06144, 06501, 06671, 06789, 06805, 06873, 07199, 07518, 07545, 53134, 61641, 62183, 62188, 62761, 62789, 62939, 63000,  ");
            sbQuery.Append(" 63475, 63824, 64396, 64463, 64676, 64785, 68869, 75173, 75375, 75644, 75785, 76050, 76366, 76598, 04641, 05268, 05632, 06145, 06519, 06555, 60754,  ");
            sbQuery.Append(" 64743, 64759, 64793, 64876, 75542, 75579, 75588, 76021, 76054, 76117, 76514, 76524, 76525, 76669, 03922, 04030, 04059, 04205, 04586, 05030, 06661,  ");
            sbQuery.Append(" 63683, 75012, 76170, 76342, 03819, 04259, 04960, 04999, 05297, 06373, 61282, 62594, 62810, 68191, 76670, 03407, 04435, 04457, 04558, 04787, 04863,  ");
            sbQuery.Append(" 04956, 05023, 05460, 06359, 06360, 06701, 06780, 06781, 06847, 07059, 07555, 48540, 61631, 63515, 64031, 64102, 64103, 75067, 75451, 75594, 76665,  ");
            sbQuery.Append(" 04639, 04822, 04891, 05022, 06093, 06257, 07004, 07364, 60122, 60685, 70035, 75601, 75705, 76151, 76292, 76512, 76513, 76563, 04213, 04288, 07511,  ");
            sbQuery.Append(" 07542, 68853, 71993, 72115, 72116, 72321, 07200 ) and (empLstIncl.sysRefCodeValue = iqSite.refCodeValue )), iqCSite.refCodeValue, empLstIncl.sysRefCodeValue)  as SiteNo ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistincl empLstIncl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = empLstIncl.empMstListTblID ");
            sbQuery.Append(" AND empLstIncl.empListInclEmpID = @empID ");
            sbQuery.Append(" AND mstLstTbl.mstListID = 1 ");
            sbQuery.Append(" AND empLstIncl.sysRefCodeValue NOT IN (SELECT  ");
            sbQuery.Append(" sysrefcodeValue ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistexcl lstexcl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
            sbQuery.Append(" AND mstLstTbl.mstListID = 1 ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
            sbQuery.Append(" AND lstexcl.empListExclEmpID = @empID ");
            sbQuery.Append(" AND lstexcl.sysRefCode = 'STR') ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = empLstIncl.empListInclEmpID ");
            sbQuery.Append(" AND (emphdr.isDeleted = 0 ");
            sbQuery.Append(" OR emphdr.isDeleted IS NULL) ");
            sbQuery.Append(" AND emphdr.empActive = 1  ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'CSITE') AS iqCSite ON iqCSite.iEmpHdrID = empLstIncl.empListInclEmpID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue,  ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'STR') AS iqSite ON iqSite.iEmpHdrID = empLstIncl.empListInclEmpID  ");
            sbQuery.Append(" UNION SELECT  ");
            sbQuery.Append(" empLstIncl.empListInclEmpID AS empHdrID, ");
            sbQuery.Append(" if((emphdr.empExtID in(03565, 03576, 03749, 03785, 03937, 03956, 03971, 03986, 03990, 04015, 04110, 04111, 04283, 04316, 04343, 04477, 04516, 04547, 04582, 04624, 04758, ");
            sbQuery.Append(" 04826, 04842, 04868, 04979, 05001, 05722, 05972, 06393, 06585, 06640, 06719, 06761, 06804, 07072, 07073, 07148, 07173, 07256, 07403, 07438, 41994,  ");
            sbQuery.Append(" 44156, 44648, 51408, 61402, 61951, 62282, 62380, 63073, 63085, 63453, 63872, 63887, 63888, 64411, 64445, 64455, 64456, 65028, 67464, 69022, 69925,  ");
            sbQuery.Append(" 70019, 70471, 75210, 75218, 75219, 75468, 75572, 75616, 75950, 75960, 76070, 76075, 76080, 76082, 76092, 76264, 76284, 76439, 76442, 76443, 76444,  ");
            sbQuery.Append(" 03681, 04023, 04074, 04079, 04127, 04137, 04150, 04399, 04568, 04621, 04660, 04707, 04978, 05046, 05048, 05899, 06074, 06109, 06320, 06326, 06530,  ");
            sbQuery.Append(" 06721, 06729, 07480, 07546, 07547, 43681, 44462, 44589, 55561, 56164, 56166, 56199, 57170, 57197, 58386, 66457, 66713, 67087, 67517, 03393, 03752,  ");
            sbQuery.Append(" 03942, 03944, 03965, 04005, 04092, 04218, 04251, 04295, 04353, 04422, 04425, 04432, 04444, 04512, 04535, 04548, 04665, 04751, 04820, 04919, 04962,  ");
            sbQuery.Append(" 05060, 05061, 05305, 05621, 06144, 06501, 06671, 06789, 06805, 06873, 07199, 07518, 07545, 53134, 61641, 62183, 62188, 62761, 62789, 62939, 63000,  ");
            sbQuery.Append(" 63475, 63824, 64396, 64463, 64676, 64785, 68869, 75173, 75375, 75644, 75785, 76050, 76366, 76598, 04641, 05268, 05632, 06145, 06519, 06555, 60754,  ");
            sbQuery.Append(" 64743, 64759, 64793, 64876, 75542, 75579, 75588, 76021, 76054, 76117, 76514, 76524, 76525, 76669, 03922, 04030, 04059, 04205, 04586, 05030, 06661,  ");
            sbQuery.Append(" 63683, 75012, 76170, 76342, 03819, 04259, 04960, 04999, 05297, 06373, 61282, 62594, 62810, 68191, 76670, 03407, 04435, 04457, 04558, 04787, 04863,  ");
            sbQuery.Append(" 04956, 05023, 05460, 06359, 06360, 06701, 06780, 06781, 06847, 07059, 07555, 48540, 61631, 63515, 64031, 64102, 64103, 75067, 75451, 75594, 76665,  ");
            sbQuery.Append(" 04639, 04822, 04891, 05022, 06093, 06257, 07004, 07364, 60122, 60685, 70035, 75601, 75705, 76151, 76292, 76512, 76513, 76563, 04213, 04288, 07511,  ");
            sbQuery.Append(" 07542, 68853, 71993, 72115, 72116, 72321, 07200 ) and (empLstIncl.sysRefCodeValue = iqSite.refCodeValue )), iqCSite.refCodeValue, empLstIncl.sysRefCodeValue)  as SiteNo ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistincl empLstIncl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = empLstIncl.empMstListTblID ");
            sbQuery.Append(" AND empLstIncl.empListInclEmpID = @empID ");
            sbQuery.Append(" AND mstLstTbl.mstListID = 2 ");
            sbQuery.Append(" AND empLstIncl.sysRefCodeValue NOT IN (SELECT  ");
            sbQuery.Append(" sysrefcodeValue ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empmstlistexcl lstexcl ");
            sbQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
            sbQuery.Append(" AND mstLstTbl.mstListID = 2 ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
            sbQuery.Append(" AND lstexcl.empListExclEmpID = @empID ");
            sbQuery.Append(" AND lstexcl.sysRefCode = 'STR') ");
            sbQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = empLstIncl.empListInclEmpID ");
            sbQuery.Append(" AND (emphdr.isDeleted = 0 ");
            sbQuery.Append(" OR emphdr.isDeleted IS NULL) ");
            sbQuery.Append(" AND emphdr.empActive = 1 ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'CSITE') AS iqCSite ON iqCSite.iEmpHdrID = empLstIncl.empListInclEmpID ");
            sbQuery.Append(" JOIN (SELECT  ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue,  ");
            sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sbQuery.Append(" FROM ");
            sbQuery.Append(" empreference ");
            sbQuery.Append(" WHERE ");
            sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'STR') AS iqSite ON iqSite.iEmpHdrID = empLstIncl.empListInclEmpID  ");
            sbQuery.Append(" ) AS Result ");
            sbQuery.Append(" ORDER BY SiteNo    ");
            */
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtEmployeeOtherSites = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text,
                    new MySqlParameter[] { DbUtility.GetParameter("empID", empID, MyDbType.Int) });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmployeeOtherSites;
        }


        /// <summary>
        /// To Get Employee List By Sys Refrencevalye using Table empheader, empreference
        /// </summary>
        /// <param name="searchBy">Pass SysRefCode/SearchBy</param>
        /// <param name="searchValue">Pass SysRefCodeValue/SearchValue</param>
        /// <returns>DataTable</returns>
        public DataTable GetEmployeeListByEmpRefValue(string searchBy, string searchValue)
        {
            DataTable dtEmployeeList = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            StringBuilder sQuery = new StringBuilder();

            sbQuery = new StringBuilder();
            sQuery = new StringBuilder();
            sQuery.Append(" SELECT empHdrID, EmpExtID, EmpEmail, empLogInID, CONCAT_WS(' ',  EmpLastName, EmpFirstName) AS EmpName, EmpFirstName, EmpLastName, EmpPassword  ");
            sQuery.Append(" FROM empheader  ");
            sQuery.Append(" JOIN (SELECT  ");
            sQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS searchValue, ");
            sQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
            sQuery.Append(" FROM ");
            sQuery.Append(" empreference ");
            sQuery.Append(" WHERE ");
            sQuery.Append(" sysEmpReferenceCodes_sysRefCode = @searchBy ) AS tmpSearchBy ON tmpSearchBy.iEmpHdrID = empheader.empHdrID and tmpSearchBy.searchValue  = @searchValue");
            sQuery.Append(" WHERE 1=1 ");

            if (BusinessUtility.GetString(this.EmpName) != "")
            {
                sbQuery.Append(sQuery);
                sbQuery.Append("AND ( EmpFirstName = '" + this.EmpName + "' OR EmpLastName = '" + this.EmpName + "' OR  concat_ws(' ', trim(empfirstname), trim(empLastname)) = '" + this.EmpName + "'  OR  concat_ws(' ', trim(empfirstname), trim(empLastname), trim(EmpExtID)) = '" + this.EmpName + "'  OR EmpExtID = '" + this.EmpName + "') ");

                string[] sEmpName = this.EmpName.Split(' ');
                string empClause = "";

                if (sEmpName.Length > 2)
                {
                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and (EmpExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "' OR  EmpExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ) ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' and  EmpExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                }
                else if (sEmpName.Length == 2)
                {

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and (EmpExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "'  ) ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' AND  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                }
                else if (sEmpName.Length == 1)
                {
                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and (EmpExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ) ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpLastName = '" + BusinessUtility.GetString(sEmpName[0]) + "' ");
                }

                sbQuery.Append(" union ");
                sbQuery.Append(sQuery);
                sbQuery.Append("AND (  ");

                sEmpName = this.EmpName.Split(' ');
                empClause = "";
                foreach (string sName in sEmpName)
                {
                    if (empClause == "")
                    {
                        empClause += " EmpFirstName = '" + sName + "' OR EmpLastName = '" + sName + "' OR  EmpExtID = '" + sName + "' ";
                    }
                    else
                    {
                        empClause += " OR EmpFirstName = '" + sName + "' OR EmpLastName = '" + sName + "' OR  EmpExtID = '" + sName + "' ";
                    }
                }

                sbQuery.Append(empClause);
                sbQuery.Append(" ) ");

                sbQuery.Append(" union ");
                sbQuery.Append(sQuery);
                sbQuery.Append("AND (  ");

                sEmpName = this.EmpName.Split(' ');
                foreach (string sName in sEmpName)
                {
                    if (empClause == "")
                    {
                        empClause += " EmpFirstName like '%" + sName + "%' OR EmpLastName like '%" + sName + "%' OR  EmpExtID like '%" + sName + "%' ";
                    }
                    else
                    {
                        empClause += " OR EmpFirstName like '%" + sName + "%' OR EmpLastName like '%" + sName + "%' OR  EmpExtID like '%" + sName + "%' ";
                    }
                }

                sbQuery.Append(empClause);
                sbQuery.Append(" ) ");
            }
            else
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(sQuery);
                if (BusinessUtility.GetString(this.EmpExtID) != "")
                {
                    sbQuery.Append("  AND ( EmpExtID = '" + this.EmpExtID + "' ) ");
                }
            }

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtEmployeeList = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text,
                    new MySqlParameter[] { DbUtility.GetParameter("searchBy", searchBy, MyDbType.String),
                    DbUtility.GetParameter("searchValue", searchValue, MyDbType.String)
                    });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmployeeList;
        }


        /// <summary>
        /// To Get Employee List Given Employee ID Spreated by ","
        /// </summary>
        /// <param name="empIDSpreatedByComma">Employee ID Spreated by ","</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeListEmpIDIn(string empIDSpreatedByComma)
        {
            DataTable dtEmployeeList = new DataTable();
            StringBuilder sbQuery = new StringBuilder();

            sbQuery = new StringBuilder();
            sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT empHdrID, EmpExtID, EmpEmail, empLogInID, CONCAT_WS(' ',  EmpLastName, EmpFirstName) AS EmpName, EmpFirstName, EmpLastName, EmpPassword  ");
            sbQuery.Append(" FROM empheader  ");
            if (empIDSpreatedByComma != "")
            {
                sbQuery.Append(" WHERE empHdrID in( " + empIDSpreatedByComma + ") ");
            }
            sbQuery.Append(" order by  CONCAT_WS(' ',  EmpLastName, EmpFirstName)  ");

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtEmployeeList = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmployeeList;
        }



        /// <summary>
        /// To Reset Employee Password and Question
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>True/False</returns>
        public Boolean ResetEmployeePasswordQuestion(int empID, string empExtID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" UPDATE empheader SET empPassword = MD5(empExtID)  WHERE empHdrID = @empHdrID; ");
                sbInsertQuery.Append(" delete from employeeanswer  where emp_headerid = @empHdrID; ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("@empHdrID", empID, MyDbType.String),                
                });

                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                dbTransactionHelper.RollBackTransaction();
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }


        public DataTable GetIsChangedEmployeeEmpRefDetails(int empID)
        {
            bool rStatus = false;
            DataTable dtEmployeeList = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            sbQuery = new StringBuilder();


            DbHelper dbHelp = new DbHelper(true);
            try
            {

                sbQuery.Append(" select * from (  ");
                sbQuery.Append(" select empHdr.empHdrID, ifnull(iqLocation.empLocTypeValue,'') as 'EMPDIV', ifnull(iJobCode.empJobID,'') as 'EMPJOB', ");
                sbQuery.Append(" ifnull(iqSite.empSiteID,'') as 'EMPSTR', ifnull(iempType.empUnionType,'') as 'EMPTYP', ifnull( iempTypeParsed.empUnionTypeParsed,'') as 'EMPTPP', ");
                sbQuery.Append(" ifnull(iDistrict.empDistID,'') as 'EMPTDIST', ifnull(iRegion.empRegID,'') as 'EMPTREGN', ifnull( iDivision.empDivID,'') as 'EMPTDIVN',  ");
                sbQuery.Append(" ifnull(iempSite.empSite,'') as 'EMPSITE', ");
                sbQuery.Append(" ifnull(iempProvince.empProvince,'') as 'EMPPABBR' ");
                sbQuery.Append(" From empheader empHdr  ");
                sbQuery.Append(" left join (  ");
                sbQuery.Append(" SELECT syrefCode.tilesen as empLocType, empHeader_empHdrID as iqEmpHdrID, sysEmpReferenceCodes_sysRefCodeValue as empLocTypeValue  ");
                sbQuery.Append(" from empreference   ");
                sbQuery.Append(" join sysempreferencecodes syrefCode on empreference.sysEmpReferenceCodes_sysRefCodeValue =syrefCode.sysRefCodeValue and syrefCode.sysRefCode='DIV'  ");
                sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'  ");
                sbQuery.Append(" ) as iqLocation on iqLocation.iqEmpHdrID = empHdr.empHdrID  ");
                sbQuery.Append(" left join (  ");
                sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSiteID, empHeader_empHdrID as iEmpHdrID  ");
                sbQuery.Append(" from empreference   ");
                sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'STR'  ");
                sbQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empHdr.empHdrID  ");
                sbQuery.Append(" left join (  ");
                sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSiteID, empHeader_empHdrID as iEmpHdrID  ");
                sbQuery.Append(" from empreference   ");
                sbQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'CSITE'  ");
                sbQuery.Append(" ) as iqCSite on iqCSite.iEmpHdrID = empHdr.empHdrID  ");
                sbQuery.Append(" left join (  ");
                sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID  ");
                sbQuery.Append(" from empreference  ");
                sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TDIVN'  ");
                sbQuery.Append(" ) as iDivision ON iDivision.iEmpHdrID = empHdr.empHdrID  ");
                sbQuery.Append(" left join (  ");
                sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empRegID, empHeader_empHdrID as iEmpHdrID  ");
                sbQuery.Append(" from empreference   ");
                sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TREGN'  ");
                sbQuery.Append(" ) as iRegion ON iRegion.iEmpHdrID = empHdr.empHdrID  ");
                sbQuery.Append(" left join (   ");
                sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDistID, empHeader_empHdrID as iEmpHdrID  ");
                sbQuery.Append(" from empreference  ");
                sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TDIST'  ");
                sbQuery.Append(" ) as iDistrict ON iDistrict.iEmpHdrID = empHdr.empHdrID  ");
                sbQuery.Append(" left join (   ");
                sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID  ");
                sbQuery.Append(" from empreference  ");
                sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'JOB'  ");
                sbQuery.Append(" ) as iJobCode ON iJobCode.iEmpHdrID = empHdr.empHdrID   ");
                sbQuery.Append(" left join (   ");
                sbQuery.Append(" SELECT   ");
                sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empUnionType, syrefCode.sysRefCodeText as employeeType,  ");
                sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID  ");
                sbQuery.Append(" FROM  ");
                sbQuery.Append(" empreference  ");
                sbQuery.Append(" join sysempreferencecodes syrefCode on empreference.sysEmpReferenceCodes_sysRefCodeValue =syrefCode.sysRefCodeValue and syrefCode.sysRefCode='TYP'  ");
                sbQuery.Append(" WHERE  ");
                sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'TYP'  ");
                sbQuery.Append(" ) as iempType ON iempType.iEmpHdrID = empHdr.empHdrID  ");
                sbQuery.Append(" left join (  ");
                sbQuery.Append(" SELECT   ");
                sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empUnionTypeParsed, syrefCode.sysRefCodeText as employeeType,  ");
                sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID  ");
                sbQuery.Append(" FROM  ");
                sbQuery.Append(" empreference  ");
                sbQuery.Append(" join sysempreferencecodes syrefCode on empreference.sysEmpReferenceCodes_sysRefCodeValue =syrefCode.sysRefCodeValue and syrefCode.sysRefCode='TPP'  ");
                sbQuery.Append(" WHERE  ");
                sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'TPP'  ");
                sbQuery.Append(" ) as iempTypeParsed ON iempTypeParsed.iEmpHdrID = empHdr.empHdrID ");
                sbQuery.Append(" left join (   ");
                sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSite, empHeader_empHdrID as iEmpHdrID  ");
                sbQuery.Append(" from empreference  ");
                sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'SITE'  ");
                sbQuery.Append(" ) as iempSite ON iempSite.iEmpHdrID = empHdr.empHdrID  ");
                sbQuery.Append(" left join (   ");
                sbQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empProvince, empHeader_empHdrID as iEmpHdrID  ");
                sbQuery.Append(" from empreference  ");
                sbQuery.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'PABBR'  ");
                sbQuery.Append(" ) as iempProvince ON iempProvince.iEmpHdrID = empHdr.empHdrID  ");
                sbQuery.Append(" where 1=1  and empHdr.empHdrID = @empID ");
                sbQuery.Append(" ) as resutl where 1=1  ");
                //sbQuery.Append(" and EMPDIV = '" + sDivisionID + "' and EMPJOB = '" + sJobID + "'  and EMPSTR  = '" + sLocation + "' and EMPTYP  = '" + sType + "' and EMPTPP  = '" + sTypeParsed + "' and EMPTDIST  = '" + sDistrict + "'   ");
                //sbQuery.Append(" and EMPTREGN  = '" + sRegionTBS + "' and EMPTDIVN  = '" + sDivisionTBS + "' and EMPSITE  = '" + sSite + "' and EMPPABBR  = '" + sProvinceAbbr + "' ");

                dtEmployeeList = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text,
                    new MySqlParameter[] { DbUtility.GetParameter("empID", empID, MyDbType.Int)
                    });

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmployeeList;
        }

        /// <summary>
        /// To Get Initiator Employee/users Detail from EmmHeader join sysrolemaster Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        public void GetAssignedFeedbackEmployeeDetailByRole(int roleID)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append("select eh.empHdrID, CONCAT_WS(' ',  eh.empFirstName, eh.empLastName) AS EmpName,eh.empFirstName,eh.empLastName,eh.empExtID from empheader eh , sysrolemaster srm where eh.empHdrID = srm.empRoleCreatedBy and srm.empRoleID = @roleID");

            MySqlParameter[] p = { new MySqlParameter("@roleID", roleID) };
            MySqlDataReader objDr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                objDr = dbHelp.GetDataReader(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, p);
                while (objDr.Read())
                {
                    this.EmpID = BusinessUtility.GetInt(objDr["empHdrID"]);
                    this.EmpExtID = BusinessUtility.GetString(objDr["EmpExtID"]);
                    this.EmpName = BusinessUtility.GetString(objDr["EmpName"]);
                    this.EmpFirstName = BusinessUtility.GetString(objDr["empFirstName"]);
                    this.EmpLastName = BusinessUtility.GetString(objDr["empLastName"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Initiator Employee/users Detail from EmmHeader join sysrolemaster Table
        /// </summary>
        /// <param name="regID">Pass Reg ID</param>
        public void GetAssignedFeedbackEmployeeDetailByRegID(int regID)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append("select eh.empHdrID, CONCAT_WS(' ',  eh.empFirstName, eh.empLastName) AS EmpName,eh.empFirstName,eh.empLastName,eh.empExtID,srm.empRoleID,srm.empRoleName ");
            sbQuery.Append(" from empheader eh, sysrolemaster srm,  employeecourseregistration ecr ");
            sbQuery.Append(" where eh.empHdrID = srm.empRoleCreatedBy and ecr.roleID = srm.empRoleID and ecr.idRegistration = @regID");
            
            MySqlParameter[] p = { new MySqlParameter("@regID", regID) };
            MySqlDataReader objDr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                objDr = dbHelp.GetDataReader(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, p);
                while (objDr.Read())
                {
                    this.EmpID = BusinessUtility.GetInt(objDr["empHdrID"]);
                    this.EmpExtID = BusinessUtility.GetString(objDr["EmpExtID"]);
                    this.EmpName = BusinessUtility.GetString(objDr["EmpName"]);
                    this.EmpFirstName = BusinessUtility.GetString(objDr["empFirstName"]);
                    this.EmpLastName = BusinessUtility.GetString(objDr["empLastName"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
