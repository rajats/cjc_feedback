﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Get User's Course List and Course Status
    /// </summary>
    public class EmployeeCourses
    {
        /// <summary>
        /// To Get/Set Employee Registration ID
        /// </summary>
        public int regID { get; set; }

        /// <summary>
        /// To Get List of Courses User is Taking From sp_Get_TakingCourseDescription Stored Procedure
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeTakingCourses(int empID, string lang)
        {

            DataTable ResultDataTable = new DataTable();
            StringBuilder sbQuery = new StringBuilder();

            MySqlParameter[] p = { new MySqlParameter("@EmpID", empID) };
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(" call sp_Get_TakingCourseDescription(" + empID + ")   ");
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null);

                DataView dv = dt.DefaultView;
                dv.Sort = "isNew DESC, CourseTitle ASC";
                ResultDataTable = dv.ToTable();
                return ResultDataTable;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return ResultDataTable;
        }


        /// <summary>
        /// To Get List of Courses User Should Take From sp_Get_CanTakeCourseDescription Stored Procedure
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeShouldTakeCourses(int empID, string lang)
        {
            DataTable ResultDataTable = new DataTable();
            StringBuilder sbQuery = new StringBuilder();

            MySqlParameter[] p = { new MySqlParameter("@EmpID", empID) };
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(" call sp_Get_CanTakeCourseDescription(" + empID + ")   ");
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null);

                //DataView dv = dt.DefaultView;
                //dv.Sort = "isNew DESC, CourseTitle ASC";
                //ResultDataTable = dv.ToTable();
                ResultDataTable = dt;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return ResultDataTable;
        }


        /// <summary>
        /// To Check if employee with particular role when came through mail link has a record in empassignedcourse table or not
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>True/False</returns>
        public Boolean CheckEmployeeFeedbackExistence(int empID, int roleID)
        {
            bool bReturn = false;
            StringBuilder sbQuery = new StringBuilder();
            
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(" select count(courseEmpRecordID) from empassignedcourse where CourseEmpHdrID = "+ empID + " and roleID = "+ roleID + "; ");
                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text,null);
                int count = BusinessUtility.GetInt(rValue);
                if (count >= 1)
                {
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }



        /// <summary>
        /// To Get List of Feedbacks to be Taken By User From sp_Get_CanTakeCourseDescriptionForFeedback Stored Procedure
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeFeedback(int empID, int roleID, string lang)
        {
            DataTable ResultDataTable = new DataTable();
            StringBuilder sbQuery = new StringBuilder();

            //MySqlParameter[] p = { new MySqlParameter("@EmpID", empID) };
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(" call sp_Get_CanTakeCourseDescriptionForFeedback(" + empID + ", "+ roleID + ")   ");
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null);

                //DataView dv = dt.DefaultView;
                //dv.Sort = "isNew DESC, CourseTitle ASC";
                //ResultDataTable = dv.ToTable();
                ResultDataTable = dt;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return ResultDataTable;
        }

        
        /// <summary>
        /// To Get List of Courses Taken By User From sp_Get_TakenCourseDescription Stored Procedure
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <param name="traningYear">Pass Traning Year</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeHaveTakenCourses(int empID, string lang, int traningYear )
        {
            DataTable ResultDataTable = new DataTable();
            StringBuilder sbQuery = new StringBuilder();

            MySqlParameter[] p = { new MySqlParameter("@EmpID", empID) };
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(" call sp_Get_TakenCourseDescription(" + empID + ", "+  traningYear +")   ");
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null);

                if (this.regID > 0)
                {
                    DataView dvFilter = dt.DefaultView;
                    dvFilter.RowFilter = ("idRegistration = " + this.regID + "");
                    dt = dvFilter.ToTable();
                }

                DataView dv = dt.DefaultView;
                dv.Sort = "isNew DESC, CourseTitle ASC";
                ResultDataTable = dv.ToTable();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return ResultDataTable;
        }


        /// <summary>
        /// To Check if Course Content is Completed or Not From fun_get_Event_completed Function
        /// </summary>
        /// <param name="regID">Pass Employee Course Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean IsEmployeeCompleteEvent(int regID)
        {
            DbHelper dbHelper = new DbHelper(false);
            bool bReturn = false;
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select fun_get_Event_completed(@regID) ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("regID", regID, MyDbType.Int)
                });
                bReturn = (BusinessUtility.GetString(rValue).ToUpper() == "Yes".ToUpper());
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Check if Course Along With The Test is Completed or Not From fun_get_Course_completed Function
        /// </summary>
        /// <param name="regID">Pass Employee Course Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean IsCourseCompleted(int regID)
        {
            DbHelper dbHelper = new DbHelper(false);
            bool bReturn = false;
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select fun_get_Course_completed(@regID) ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("regID", regID, MyDbType.Int)
                });
                bReturn = (BusinessUtility.GetString(rValue).ToUpper() == "Yes".ToUpper());
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Get Course Completion Date From fun_get_End_Date Function
        /// </summary>
        /// <param name="regID">Pass Course Registration ID</param>
        /// <returns>string</returns>
        public string CourseCompletedDate(int regID)
        {
            DbHelper dbHelper = new DbHelper(false);
            string sReturn = string.Empty;
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select fun_get_End_Date(@regID) ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("regID", regID, MyDbType.Int)
                });

                sReturn = BusinessUtility.GetString(rValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return sReturn;
        }

        /// <summary>
        /// To Get Course Test Attempt Status- Date Started, Progress% etc. From trainingeventresistrationresult Table
        /// </summary>
        /// <param name="regID">Pass Course Test Registration ID</param>
        /// <param name="isCountCompletedTest">Pass True/False if is count completed Test</param>
        /// <returns>Datatable</returns>
        public DataTable TestCourseAttempts(int regID, Boolean isCountCompletedTest)
        {
            DbHelper dbHelper = new DbHelper(false);
            DataTable dt = new DataTable();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();

                StringBuilder sbQuery = new StringBuilder();

                sbQuery.Append(" select count(*) From trainingeventresistrationresult where regid=@regID  and postType='G' ");
                if (isCountCompletedTest == true)
                {
                    sbQuery.Append(" and completed='true' ");
                }

                object rVal = dbHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("regID", regID, MyDbType.Int)
                });


                if (BusinessUtility.GetInt(rVal) > 0)
                {
                    //sbInsertQuery.Append(" select * From trainingeventresistrationresult where regid=@regID  and postType='G' ");
                    sbInsertQuery.Append(" select idtrainingeventresistrationresult, xmlparsedid, regID, createdDateTime, course_status, course_attempts, title, completed, totaltime, location, progressstatus, postType , IFNULL(score_raw, 0) as score_raw From trainingeventresistrationresult where regid=@regID  and postType='G' ");
                }
                else
                {
                    //sbInsertQuery.Append(" select * From trainingeventresistrationresult where regid=@regID  and postType='C' ");
                    sbInsertQuery.Append(" select idtrainingeventresistrationresult, xmlparsedid, regID, createdDateTime, course_status, course_attempts, title, completed, totaltime, location, progressstatus, postType , IFNULL(score_raw, 0) as score_raw From trainingeventresistrationresult where regid=@regID  and postType='C' ");
                }

                if (isCountCompletedTest == true)
                {
                    sbInsertQuery.Append(" and completed='true' ");
                }

                dt = dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("regID", regID, MyDbType.Int)
                });
                return dt;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return dt;
        }

    }
}
