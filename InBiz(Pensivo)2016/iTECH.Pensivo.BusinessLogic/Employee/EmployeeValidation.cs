﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Web;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Validate User 
    /// </summary>
    public class EmployeeValidation
    {
        /// <summary>
        /// To Validate If User Is Authorized or Not From empheader Table
        /// </summary>
        /// <param name="empLogInID">Pass Employee External ID</param>
        /// <param name="empPassword">Pass Employee Password</param>
        /// <returns>True/False</returns>
        public Boolean ValidateEmployee(string empLogInID, string empPassword)
        {
            bool bReturn = false;
            StringBuilder sbValidate = new StringBuilder();
            sbValidate.Append(" SELECT empHdrID FROM empheader WHERE EmpActive = 1 AND ifnull( isDeleted,0) = 0 AND empExtID = @EmpLogInID  AND empPassword = MD5( @EmpPassword ) ");

            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(BusinessUtility.GetString(sbValidate), CommandType.Text,
                new MySqlParameter[] { DbUtility.GetParameter("EmpLogInID", empLogInID, MyDbType.String), DbUtility.GetParameter("EmpPassword", empPassword, MyDbType.String) });
                int empID = BusinessUtility.GetInt(scalar);
                if (empID > 0)
                {
                    Employee objEmp = new Employee();
                    objEmp.GetEmployeeDetail(empID);
                    CurrentEmployee.SetCurrentEmployeeSession(BusinessUtility.GetInt(objEmp.EmpID), objEmp.EmpExtID, objEmp.EmpExtID, objEmp.EmpName, objEmp.EmpEmail, objEmp.EmpType);
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }


        /// <summary>
        /// To Validate If User Is Authorized or Not From empheader Table
        /// </summary>
        /// <param name="empLogInID">Pass Employee External ID</param>
        /// <param name="empPassword">Pass Employee Password</param>
        /// <returns>True/False</returns>
        public Boolean ValidateEmployeeWhenCameThroughMailLink(int empHdrID)
        {
            bool bReturn = false;
            StringBuilder sbValidate = new StringBuilder();
            sbValidate.Append(" SELECT empHdrID FROM empheader WHERE EmpActive = 1 AND ifnull( isDeleted,0) = 0 AND empHdrID = @empHdrID");

            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(BusinessUtility.GetString(sbValidate), CommandType.Text,
                new MySqlParameter[] { DbUtility.GetParameter("empHdrID", empHdrID, MyDbType.String) });
                int empID = BusinessUtility.GetInt(scalar);
                if (empID > 0)
                {
                    Employee objEmp = new Employee();
                    objEmp.GetEmployeeDetail(empID);
                    CurrentEmployee.SetCurrentEmployeeSession(BusinessUtility.GetInt(objEmp.EmpID), objEmp.EmpExtID, objEmp.EmpExtID, objEmp.EmpName, objEmp.EmpEmail, objEmp.EmpType);
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }
    }

    /// <summary>
    /// To Maintain Session During User Validation
    /// </summary>
    public class CurrentEmployee
    {

        /// <summary>
        /// To Get Employee ID
        /// </summary>
        public static int EmpID
        {
            get
            {

                return BusinessUtility.GetInt(HttpContext.Current.Session["EmpID"]);
            }
        }

        /// <summary>
        /// To Get Employee LoginID
        /// </summary>
        public static string EmpLogInID
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmpLogInID"]);
            }
        }

        /// <summary>
        /// To Get Employee External ID
        /// </summary>
        public static string EmpExtID
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmpExtID"]);

            }
        }

        /// <summary>
        /// To Get Employee Name
        /// </summary>
        public static string EmpName
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmpName"]);
            }
        }

        /// <summary>
        /// To Get Employee First Name
        /// </summary>
        public static string EmpFirstName
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmpFirstName"]);
            }
        }

        /// <summary>
        /// To Get Employee Last Name
        /// </summary>
        public static string EmpLastName
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmpLastName"]);
            }
        }



        /// <summary>
        /// To Get Employee Email
        /// </summary>
        public static string EmpEmail
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmpEmail"]);
            }
        }

        /// <summary>
        /// To Get Employee Login Type
        /// </summary>
        public static string EmpType
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmpType"]);
            }
        }

        /// <summary>
        /// To Get Employee Session ID
        /// </summary>
        public static string EmpSessionID
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmpSessionID"]);
            }
        }

        /// <summary>
        /// To Get Employee Browser Agent Detail
        /// </summary>
        public static string EmpBrowserAgent
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmpBrowserAgent"]);
            }
        }

        /// <summary>
        /// To Get Employee IP Address
        /// </summary>
        public static string EmpIPAddress
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmpIPAddress"]);
            }
        }

        /// <summary>
        /// To Get Super Admin Emp ID after login as Another User
        /// </summary>
        public static string SuperAdminEmpID
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["SuperAdminEmpID"]);
            }
        }

        /// <summary>
        /// To Get Super Admin Login ID after login as Another User
        /// </summary>
        public static string SuperAdminLoginID
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["SuperAdminEmpLogInID"]);
            }
        }

        /// <summary>
        /// To Get Is SuperAdmin Login as Another User
        /// </summary>
        public static bool IsSuperAdminLoginAsAnotherUser
        {
            get
            {
                if (HttpContext.Current.Session["SuperAdminEmpID"] != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// To Save User Details in Session
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="emploginID">Pass Employee Login ID</param>
        /// <param name="empExtID">Pass Employee External ID</param>
        /// <param name="empName">Pass Employee Name</param>
        /// <param name="empEmail">Pass Employee Email</param>
        /// <param name="empType">Pass Employee Login Type</param>
        public static void SetCurrentEmployeeSession(int empID, string emploginID, string empExtID, string empName, string empEmail, string empType)
        {
            HttpContext.Current.Session["EmpID"] = empID;
            HttpContext.Current.Session["EmpLogInID"] = emploginID;
            HttpContext.Current.Session["EmpExtID"] = empExtID;
            HttpContext.Current.Session["EmpFirstName"] = empName;
            HttpContext.Current.Session["EmpLastName"] = empName;
            HttpContext.Current.Session["EmpName"] = empName;
            HttpContext.Current.Session["EmpEmail"] = empEmail;
            HttpContext.Current.Session["EmpType"] = empType;

            string sSessionID = HttpContext.Current.Session.SessionID;
            string sBrowserAgent = "";
            string sIPAddress = "";

            sIPAddress = (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();
            HttpBrowserCapabilities browse = HttpContext.Current.Request.Browser;
            sBrowserAgent = HttpContext.Current.Request.UserAgent;

            HttpContext.Current.Session["EmpSessionID"] = sSessionID;
            HttpContext.Current.Session["EmpBrowserAgent"] = sBrowserAgent;
            HttpContext.Current.Session["EmpIPAddress"] = sIPAddress;

        }

        /// <summary>
        /// To Remove User Saved Session
        /// </summary>
        public static void RemoveEmployeeFromSession()
        {
            HttpContext.Current.Session.Remove("EmpID");
            HttpContext.Current.Session.Remove("EmpLogInID");
            HttpContext.Current.Session.Remove("EmpExtID");
            HttpContext.Current.Session.Remove("EmpName");
            HttpContext.Current.Session.Remove("EmpFirstName");
            HttpContext.Current.Session.Remove("EmpLastName");
            HttpContext.Current.Session.Remove("EmpEmail");
            HttpContext.Current.Session.Remove("EmpType");
            HttpContext.Current.Session.Remove("dtBrdCrumb");

            HttpContext.Current.Session.Remove("EmpSessionID");
            HttpContext.Current.Session.Remove("EmpBrowserAgent");
            HttpContext.Current.Session.Remove("EmpIPAddress");
        }


        /// <summary>
        /// To Set Super Admin Login Session Before Login as Another user 
        /// </summary>
        /// <param name="empID">Pass Super Admin Employee ID</param>
        /// <param name="emploginID">Pass Super Admin Employee LoginID</param>
        public static void SetSuperAdminSession(int empID, string emploginID)
        {
            HttpContext.Current.Session["SuperAdminEmpID"] = empID;
            HttpContext.Current.Session["SuperAdminEmpLogInID"] = emploginID;
        }

        /// <summary>
        /// To Remove User Saved Session
        /// </summary>
        public static void RemoveSuperAdminFromSession()
        {
            HttpContext.Current.Session.Remove("SuperAdminEmpID");
            HttpContext.Current.Session.Remove("SuperAdminEmpLogInID");
        }

    }
}
