﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Functionality Regarding User Course Registration And Related Features
    /// </summary>
    public class EmployeeCourseRegistration
    {
        /// <summary>
        /// To Get/Set Employee ID
        /// </summary>
        public int empID { get; set; }

        /// <summary>
        ///  To Get Set Event ID
        /// </summary>
        public int eventID { get; set; }

        /// <summary>
        /// To Get/Set Event Ver No
        /// </summary>
        public string eventVerNo { get; set; }

        /// <summary>
        /// To Get/Set Test Attempt EligiableDateTime
        /// </summary>
        public DateTime testAttempEligiableDateTime { get; set; }

        /// <summary>
        /// To Get/Set Attempt Request ID
        /// </summary>
        public int testAttemptRequestID { get; set; }

        /// <summary>
        /// To Get/Set Message Alert ID
        /// </summary>
        public int messageAlertID { get; set; }

        /// <summary>
        /// To Get/Set Event Launched Language
        /// </summary>
        public string eventLaunchedLanguage { get; set; }

        /// <summary>
        /// To Get/Set Registration RoleID
        /// </summary>
        public int roleID { get; set; }

        public bool RepeatRequired { get; set; }
        public bool AlertRequired { get; set; }
        public int orderSeqInDisplay { get; set; }
        public int repeatInDays { get; set; }
        public string repeatTriggerType { get; set; }
        public int repeatRateInDays { get; set; }
        public string reportingKeywords { get; set; }
        public string attemptResetType { get; set; }
        public int attemptGrantWaitingTime { get; set; }
        public string alertType { get; set; }
        public int alertInDays { get; set; }
        public string alertFormat { get; set; }
        public int isRepeated { get; set; }
        public int courseTestRegID { get; set; }
        public int empCourseAssignedID { get; set; }
        public string passingScore { get; set; }
        public DateTime endDate { get; set; }


        /// <summary>
        /// To Save User Courses Registered with RUSTICI SCORM Player in employeecourseregistration Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="courseID">Pass Course ID</param>
        /// <param name="courseVerNo">Pass Course Version No</param>
        /// <returns>int</returns>
        public Int64 InsertEmployeeRegistraionID(Int64 empID, Int64 courseID, Int64 courseVerNo, Int64 roleID, string lang, int isRepeated, int empCourseAssignedID)
        {
            int iEmployeeRegistraionID = 0;
            StringBuilder sbQuery = new StringBuilder();
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {

                EmpAssignedCourses objempAssignedCourse = new EmpAssignedCourses();
                DataTable dtRoleCourseDetail = new DataTable();
                if (roleID > 0)
                {
                    dtRoleCourseDetail = objempAssignedCourse.GetRoleCourseDetail(BusinessUtility.GetInt(empID), BusinessUtility.GetInt(courseID), BusinessUtility.GetInt(roleID));
                }



                sbQuery = new StringBuilder();
                sbQuery.Append(" INSERT INTO employeecourseregistration (empHeader_empHdrID, courseHeader_courseHdrID, courseHeader_courseVerNo, createdDateTime, roleID, passingGrade, isRepeat, lang, IsAlert, 	"); //
                sbQuery.Append(" orderSeqInDisplay, repeatInDays, repeatTriggerType, repeatRateInDays, reportingKeywords, attemptResetType,  ");
                sbQuery.Append(" attemptGrantWaitingTime, alertType, alertInDays, alertFormat, isRepeated, empCourseAssignedID ) ");
                sbQuery.Append(" VALUE (@empHeader_empHdrID, @courseHeader_courseHdrID, @courseHeader_courseVerNo, @createdDateTime, @roleID, @passingGrade, @isRepeat, @lang, @IsAlert, "); //
                sbQuery.Append(" @orderSeqInDisplay, @repeatInDays, @repeatTriggerType, @repeatRateInDays, @reportingKeywords, @attemptResetType,  ");
                sbQuery.Append(" @attemptGrantWaitingTime, @alertType, @alertInDays, @alertFormat, @isRepeated, @empCourseAssignedID ) ");

                List<MySqlParameter> pList = new List<MySqlParameter>();
                pList.Add(DbUtility.GetParameter("empHeader_empHdrID", empID, MyDbType.String));
                pList.Add(DbUtility.GetParameter("courseHeader_courseHdrID", courseID, MyDbType.String));
                pList.Add(DbUtility.GetParameter("courseHeader_courseVerNo", courseVerNo, MyDbType.String));
                pList.Add(DbUtility.GetParameter("createdDateTime", DateTime.Now, MyDbType.DateTime));
                pList.Add(DbUtility.GetParameter("roleID", roleID, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("lang", lang, MyDbType.String));
                pList.Add(DbUtility.GetParameter("empCourseAssignedID", empCourseAssignedID, MyDbType.Int));


                if (dtRoleCourseDetail.Rows.Count == 0)
                {
                    pList.Add(DbUtility.GetParameter("passingGrade", 0, MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("isRepeat", 0, MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("IsAlert", 0, MyDbType.Int));

                    pList.Add(DbUtility.GetParameter("orderSeqInDisplay", 0, MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("repeatInDays", 0, MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("repeatTriggerType", "", MyDbType.String));
                    pList.Add(DbUtility.GetParameter("repeatRateInDays", 0, MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("reportingKeywords", "", MyDbType.String));
                    pList.Add(DbUtility.GetParameter("attemptResetType", "", MyDbType.String));
                    pList.Add(DbUtility.GetParameter("attemptGrantWaitingTime", 0, MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("alertType", "", MyDbType.String));
                    pList.Add(DbUtility.GetParameter("alertInDays", 0, MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("alertFormat", "", MyDbType.String));
                }
                else
                {
                    pList.Add(DbUtility.GetParameter("passingGrade", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["minimumPassScore"]), MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("isRepeat", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["repeatRequired"]), MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("IsAlert", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["alertRequired"]), MyDbType.Int));

                    pList.Add(DbUtility.GetParameter("orderSeqInDisplay", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["orderSeqInDisplay"]), MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("repeatInDays", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["repeatInDays"]), MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("repeatTriggerType", BusinessUtility.GetString(dtRoleCourseDetail.Rows[0]["repeatTriggerType"]), MyDbType.String));
                    pList.Add(DbUtility.GetParameter("repeatRateInDays", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["repeatRate"]), MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("reportingKeywords", BusinessUtility.GetString(dtRoleCourseDetail.Rows[0]["reportingKeywords"]), MyDbType.String));
                    pList.Add(DbUtility.GetParameter("attemptResetType", BusinessUtility.GetString(dtRoleCourseDetail.Rows[0]["attemptResetType"]), MyDbType.String));
                    pList.Add(DbUtility.GetParameter("attemptGrantWaitingTime", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["attemptGrantWaitingTime"]), MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("alertType", BusinessUtility.GetString(dtRoleCourseDetail.Rows[0]["alertType"]), MyDbType.String));
                    pList.Add(DbUtility.GetParameter("alertInDays", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["alertInDays"]), MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("alertFormat", BusinessUtility.GetString(dtRoleCourseDetail.Rows[0]["alertFormat"]), MyDbType.String));
                }
                pList.Add(DbUtility.GetParameter("isRepeated", isRepeated, MyDbType.Int));


                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                
                iEmployeeRegistraionID = dbTransactionHelper.GetLastInsertID();

                
                sbQuery = new StringBuilder();
                sbQuery.Append("update employeecourseregistration set courseTestRegID = @iEmployeeRegistraionID where idRegistration = @iEmployeeRegistraionID") ; //
                pList = new List<MySqlParameter>();
                pList.Add(DbUtility.GetParameter("iEmployeeRegistraionID", iEmployeeRegistraionID, MyDbType.String));
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                dbTransactionHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return iEmployeeRegistraionID;
        }

        /// <summary>
        /// To Get RUSTICI SCORM Users Registration ID Saved in Pensivo  From employeecourseregistration Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="courseID">Pass Course ID</param>
        /// <param name="courseVerNo">Pass Course Version No </param>
        /// <returns>int</returns>
        public Int64 GetEmployeeRegistraionID(Int64 empID, Int64 courseID, Int64 courseVerNo, int isRepeated)
        {


            Int64 iEmployeeRegistraionID = 0;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT idRegistration FROM employeecourseregistration ");
            sbQuery.Append(" WHERE empHeader_empHdrID = @empHeader_empHdrID AND courseHeader_courseHdrID = @courseHeader_courseHdrID AND roleID = @courseHeader_courseVerNo and isRepeated = @isRepeated  and date(createdDateTime) > '2015-12-31'");
            List<MySqlParameter> pList = new List<MySqlParameter>();
            pList.Add(DbUtility.GetParameter("empHeader_empHdrID", empID, MyDbType.String));
            pList.Add(DbUtility.GetParameter("courseHeader_courseHdrID", courseID, MyDbType.String));

            pList.Add(DbUtility.GetParameter("courseHeader_courseVerNo", courseVerNo, MyDbType.String));
            pList.Add(DbUtility.GetParameter("isRepeated", isRepeated, MyDbType.Int));

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                //ErrorLog.createLog("Sql Qurey Changed by Aman : " + BusinessUtility.GetString(sbQuery) + " Parameter Input is empID:" + empID + " courseID:" + courseID + " courseVerNo:" + courseVerNo + " isRepeated:" + isRepeated);

                object returnValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                iEmployeeRegistraionID = BusinessUtility.GetLong(returnValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return iEmployeeRegistraionID;
        }

        /// <summary>
        /// To Get Course Detail Registered With RUSTICI SCORM From employeecourseregistration Table
        /// </summary>
        /// <param name="regID">Pass Employee Course Registration ID</param>
        public void GetEmployeeRegistraionDetail(Int64 regID)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT empHeader_empHdrID, courseHeader_courseHdrID, courseHeader_courseVerNo, lang, ifnull(roleID,0) as roleID, passingGrade,  isRepeat,IsAlert,  ");
            sbQuery.Append(" orderSeqInDisplay, repeatInDays, repeatTriggerType, repeatRateInDays, reportingKeywords, attemptResetType,  ");
            sbQuery.Append(" attemptResetType, attemptGrantWaitingTime, alertType, alertInDays, alertFormat, isRepeated, courseTestRegID, ifnull(empCourseAssignedID,0) as empCourseAssignedID  ");
            sbQuery.Append(" FROM employeecourseregistration  ");
            sbQuery.Append(" WHERE idRegistration = @regID ");
            List<MySqlParameter> pList = new List<MySqlParameter>();
            pList.Add(DbUtility.GetParameter("regID", regID, MyDbType.Int));
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        this.empID = BusinessUtility.GetInt(dt.Rows[0]["empHeader_empHdrID"]);
                        this.eventID = BusinessUtility.GetInt(dt.Rows[0]["courseHeader_courseHdrID"]);
                        this.eventVerNo = BusinessUtility.GetString(dt.Rows[0]["courseHeader_courseVerNo"]);
                        this.eventLaunchedLanguage = BusinessUtility.GetString(dt.Rows[0]["lang"]);
                        this.roleID = BusinessUtility.GetInt(dt.Rows[0]["roleID"]);
                        this.passingScore = BusinessUtility.GetString(BusinessUtility.GetInt(dt.Rows[0]["passingGrade"]));
                        this.RepeatRequired = BusinessUtility.GetBool(dt.Rows[0]["isRepeat"]);
                        this.AlertRequired = BusinessUtility.GetBool(dt.Rows[0]["IsAlert"]);
                        this.orderSeqInDisplay = BusinessUtility.GetInt(dt.Rows[0]["orderSeqInDisplay"]);
                        this.repeatInDays = BusinessUtility.GetInt(dt.Rows[0]["repeatInDays"]);
                        this.repeatTriggerType = BusinessUtility.GetString(dt.Rows[0]["repeatTriggerType"]);
                        this.repeatRateInDays = BusinessUtility.GetInt(dt.Rows[0]["repeatRateInDays"]);
                        this.reportingKeywords = BusinessUtility.GetString(dt.Rows[0]["reportingKeywords"]);
                        this.attemptResetType = BusinessUtility.GetString(dt.Rows[0]["attemptResetType"]);
                        this.attemptGrantWaitingTime = BusinessUtility.GetInt(dt.Rows[0]["attemptGrantWaitingTime"]);
                        this.alertType = BusinessUtility.GetString(dt.Rows[0]["alertType"]);
                        this.alertInDays = BusinessUtility.GetInt(dt.Rows[0]["alertInDays"]);
                        this.alertFormat = BusinessUtility.GetString(dt.Rows[0]["alertFormat"]);
                        this.isRepeated = BusinessUtility.GetInt(dt.Rows[0]["isRepeated"]);
                        this.courseTestRegID = BusinessUtility.GetInt(dt.Rows[0]["courseTestRegID"]);
                        this.empCourseAssignedID = BusinessUtility.GetInt(dt.Rows[0]["empCourseAssignedID"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// To Remove SCORM Users Registration ID From Pensivo From employeecourseregistration Table
        /// </summary>
        /// <param name="regID">Pass Employee Course Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean RemoveEmployeeRegistraionID(Int64 regID)
        {
            bool bReturn = false;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" delete  from empcourseattempts where regID = @idRegistration ; ");
            sbQuery.Append(" delete from reportsummary where registrationID = @idRegistration ; ");
            sbQuery.Append(" Delete  FROM employeecourseregistration ");
            sbQuery.Append(" WHERE idRegistration = @idRegistration;  ");
            List<MySqlParameter> pList = new List<MySqlParameter>();
            pList.Add(DbUtility.GetParameter("idRegistration", regID, MyDbType.Int));
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                bReturn = true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Show if User Started the Course or Not From employeecourseregistration Table
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="eventID">Pass Event ID</param>
        /// <param name="verNo">Pass Event Version No</param>
        /// <returns>True/False</returns>
        public Boolean IsUserStartCourse(int empID, int eventID, int verNo, int courseRegID)
        {
            bool bReturn = false;
            DbHelper dbHelper = new DbHelper(false);

            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select idRegistration FROM employeecourseregistration where empHeader_empHdrID = @empID AND courseHeader_courseHdrID = @eventID AND courseHeader_courseVerNo = @verNo and courseTestRegID =  @courseRegID  ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empID", empID, MyDbType.Int),
                DbUtility.GetParameter("eventID", eventID, MyDbType.Int),
                DbUtility.GetParameter("verNo", verNo, MyDbType.Int),
                DbUtility.GetParameter("courseRegID", courseRegID, MyDbType.Int)
                
                });
                bReturn = (BusinessUtility.GetInt(rValue) > 0);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Save User Course Test Attempts in empcourseattempts Table
        /// </summary>
        /// <param name="regID">Pass Employee Course Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean SaveCourseAttempts(int regID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select idempcourseattempts FROM empcourseattempts where regID = @regID ");
                object rValue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("regID", regID, MyDbType.Int)
                });
                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    sbQuery = new StringBuilder();
                    sbQuery.Append(" update empcourseattempts set attempts = attempts+1 where regID = @regID ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("regID", regID, MyDbType.Int)
                    });
                }
                else
                {
                    sbQuery = new StringBuilder();
                    sbQuery.Append(" select idRegistration from employeecourseregistration where idRegistration = @regID ");
                    rValue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("regID", regID, MyDbType.Int)
                    });

                    if (BusinessUtility.GetInt(rValue) > 0)
                    {
                        sbQuery = new StringBuilder();
                        sbQuery.Append(" INSERT INTO empcourseattempts (regID, attempts) values (@regID, 1) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("regID", regID, MyDbType.Int)
                        });
                    }
                }

                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }


        /// <summary>
        /// To Set Course Content to Complete in employeecourseregistration Table
        /// </summary>
        /// <param name="iRegID">Pass Employee Course Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean SaveCourseCompleted(int iRegID)
        {
            bool bReturn = false;
            StringBuilder sbQuery = new StringBuilder();
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(" UPDATE employeecourseregistration SET IsCompleted = 1 Where idRegistration = @regID	");
                List<MySqlParameter> pList = new List<MySqlParameter>();
                pList.Add(DbUtility.GetParameter("regID", iRegID, MyDbType.Int));

                dbHelp.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                bReturn = true;
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("employeecourseregistration UPDATE Failure " + BusinessUtility.GetString(iRegID));
                ErrorLog.createLog("employeecourseregistration UPDATE Query " + BusinessUtility.GetString(sbQuery));
                ErrorLog.createLog("employeecourseregistration UPDATE Param1 :- " + iRegID + "  ");
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Set Course Test Registration ID in employeecourseregistration Table
        /// </summary>
        /// <param name="courseRegID">Pass Employee Course Registration ID</param>
        /// <param name="courseTestRegID">Pass Employee Test Course Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean SaveCourseTestRegID(int courseRegID, int courseTestRegID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" update employeecourseregistration set courseTestRegID = @courseTestRegID where idRegistration = @courseRegID ");
                dbHelp.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("courseRegID", courseRegID, MyDbType.Int),
                    DbUtility.GetParameter("courseTestRegID", courseTestRegID, MyDbType.Int)
                    });
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                ErrorLog.createLog("Failure Update Test RegID , Course Reg ID : " + BusinessUtility.GetString(courseRegID) + " , Test Course Reg ID =" + BusinessUtility.GetString(courseTestRegID) + "");
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// To Get Course Registration ID Based on Course Test Registration ID From employeecourseregistration Table
        /// </summary>
        /// <param name="courseTestRegID">Pass Employee Test Course Registration ID</param>
        /// <returns>int</returns>
        public int GetTestCourseRegID(int courseTestRegID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select idRegistration  from  employeecourseregistration where courseTestRegID = @courseTestRegID ");
                object objValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("courseTestRegID", courseTestRegID, MyDbType.Int),
                    });
                return BusinessUtility.GetInt(objValue);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// To Get Course Test Attempt Count from fun_get_Course_Attempted function
        /// </summary>
        /// <param name="courseRegID">Pass Employee Test Course Registration ID</param>
        /// <returns>Int</returns>
        public int GetTestAttemptCount(int courseRegID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select fun_get_Course_Attempted(@courseTestRegID) ");
                object objValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("courseTestRegID", courseRegID, MyDbType.Int),
                    });
                return BusinessUtility.GetInt(objValue);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// To Save Requested Test Attempts in emptestattemptrequests Table
        /// </summary>
        /// <param name="regID">Pass Employee Test Course Registration ID</param>
        /// <param name="iAttempts">Pass No of Test Attempts Requested</param>
        /// <param name="alertEmailID">Pass Alert Email ID</param>
        /// <param name="iAttemptsMin">Pass No Attempt Min After User Retry to Attempt Test</param>
        /// <returns>True/False</returns>
        public Boolean SaveTestAttemptRequest(int regID, int iAttempts, string alertEmailID, int iAttemptsMin)
        {
            StringBuilder sbQuery = new StringBuilder();
            Boolean rStatus = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                if (GetRequestedTestEligable(regID) == false)
                {
                    DateTime dtNow = DateTime.Now;
                    DateTime dtEligibleDateTime = DateTime.Now.AddMinutes(iAttemptsMin);

                    sbQuery = new StringBuilder();
                    sbQuery.Append(" INSERT INTO emptestattemptrequests (regID, createdDateTime, attempts, eligibleDateTime, alertEmailID)	");
                    sbQuery.Append(" VALUE (@regID, @createdDateTime, @attempts, @eligibleDateTime, @alertEmailID) ");
                    List<MySqlParameter> pList = new List<MySqlParameter>();
                    pList.Add(DbUtility.GetParameter("regID", regID, MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("createdDateTime", dtNow, MyDbType.DateTime));
                    pList.Add(DbUtility.GetParameter("attempts", iAttempts, MyDbType.Int));
                    pList.Add(DbUtility.GetParameter("eligibleDateTime", dtEligibleDateTime, MyDbType.DateTime));
                    pList.Add(DbUtility.GetParameter("alertEmailID", alertEmailID, MyDbType.String));
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                    dbTransactionHelper.CommitTransaction();
                    testAttemptRequestID = dbTransactionHelper.GetLastInsertID();
                    testAttempEligiableDateTime = dtEligibleDateTime;
                }
                else
                {
                    if (alertEmailID != "")
                    {
                        int attemptID = GetRequestedTestID(regID);
                        DateTime dtEligibleDateTime = GetRequestedTestEligibleDate(regID);
                        sbQuery = new StringBuilder();
                        sbQuery.Append(" update emptestattemptrequests set alertEmailID= @alertEmailID where idempTestAttemptRequests = @attemptRequestID; ");
                        List<MySqlParameter> pList = new List<MySqlParameter>();
                        pList.Add(DbUtility.GetParameter("alertEmailID", alertEmailID, MyDbType.String));
                        pList.Add(DbUtility.GetParameter("attemptRequestID", attemptID, MyDbType.Int));
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                        dbTransactionHelper.CommitTransaction();

                        testAttemptRequestID = attemptID;
                        testAttempEligiableDateTime = dtEligibleDateTime;
                    }
                }
                rStatus = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return rStatus;
        }


        /// <summary>
        /// To Get Test Attempt Request Count From emptestattemptrequests Table
        /// </summary>
        /// <param name="courseRegID">Pass Employee Course Test Registration ID</param>
        /// <returns>Int</returns>
        public int GetRequestedTestAttemptCount(int courseRegID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select sum(attempts) from emptestattemptrequests where regID =@courseTestRegID and @currentDateTime >= eligibledatetime ");
                object objValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("courseTestRegID", courseRegID, MyDbType.Int),
                    DbUtility.GetParameter("currentDateTime", DateTime.Now, MyDbType.DateTime),
                    });
                return BusinessUtility.GetInt(objValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Check if Course Test Eligible Time is Remaining or not From emptestattemptrequests Table
        /// </summary>
        /// <param name="courseTestRegID">Pass Employee Course Test Registration ID</param>
        /// <returns>True/False</returns>
        public Boolean GetRequestedTestEligable(int courseTestRegID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select idempTestAttemptRequests from emptestattemptrequests where regID =@courseTestRegID and @currentDateTime <= eligibledatetime ");
                object objValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("courseTestRegID", courseTestRegID, MyDbType.Int),
                    DbUtility.GetParameter("currentDateTime", DateTime.Now, MyDbType.DateTime),
                    });
                return BusinessUtility.GetInt(objValue) > 0;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// Get Test Attempt Request ID
        /// </summary>
        /// <param name="courseTestRegID">Course Test Registration ID</param>
        /// <returns>int</returns>
        public int GetRequestedTestID(int courseTestRegID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select idempTestAttemptRequests from emptestattemptrequests where regID =@courseTestRegID and @currentDateTime <= eligibledatetime ");
                object objValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("courseTestRegID", courseTestRegID, MyDbType.Int),
                    DbUtility.GetParameter("currentDateTime", DateTime.Now, MyDbType.DateTime),
                    });
                return BusinessUtility.GetInt(objValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// Get Test Attempt Eligible DateTime
        /// </summary>
        /// <param name="courseTestRegID">Course Test Registration ID</param>
        /// <returns>Date Time</returns>
        public DateTime GetRequestedTestEligibleDate(int courseTestRegID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select eligibleDateTime from emptestattemptrequests where regID =@courseTestRegID and @currentDateTime <= eligibledatetime ");
                object objValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("courseTestRegID", courseTestRegID, MyDbType.Int),
                    DbUtility.GetParameter("currentDateTime", DateTime.Now, MyDbType.DateTime),
                    });
                return BusinessUtility.GetDateTime(objValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }



        /// <summary>
        /// To Save Test Attempt Alert Email Sending Detail in syscommunication
        /// </summary>
        /// <param name="fromEmailID">Pass From Email ID</param>
        /// <param name="toEmailID">Pass To Email ID</param>
        /// <param name="smtpServer">Pass SMTP Server</param>
        /// <param name="smtpPort">Pass SMTP Port</param>
        /// <param name="smtpUserEmail">Pass SMTP User EMail</param>
        /// <param name="smtpUserPassword">Pass SMTP User Password</param>
        /// <param name="smtpDefaultCredentials">Pass STMP Is Default Credintial</param>
        /// <param name="msgType">Pass Message Type</param>
        /// <param name="msgDeliveredDateTime">Pass Message Delivery DateTime</param>
        /// <param name="msgSend">Pass Message Send</param>
        /// <param name="msgCreatedBy">Pass Message Created By Employee ID</param>
        /// <param name="smtpIsSSL">Pass SMTP is SSL</param>
        /// <param name="msgEmailSubject">Pass Message Subject</param>
        /// <param name="msgContent">Pass Message Content</param>
        /// <returns>True/False</returns>
        public Boolean SaveTestAttemptAlert(string fromEmailID, string toEmailID, string smtpServer, int smtpPort, string smtpUserEmail, string smtpUserPassword, string smtpDefaultCredentials,
            string msgType, DateTime msgDeliveredDateTime, int msgSend, int msgCreatedBy, int smtpIsSSL, string msgEmailSubject, string msgContent)
        {
            StringBuilder sbQuery = new StringBuilder();
            Boolean rStatus = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(" insert into syscommunication ");
                sbQuery.Append(" ( ");
                sbQuery.Append(" fromEmailID, toEmailID, smtpServer, smtpPort, smtpUserEmail, smtpUserPassword, smtpDefaultCredentials, msgType, msgDeliveredDateTime,  ");
                sbQuery.Append(" msgSend, msgCreatedBy, smtpIsSSL, msgEmailSubject, msgContent  ");
                sbQuery.Append(" ) ");
                sbQuery.Append(" values ( ");
                sbQuery.Append(" @fromEmailID, @toEmailID, @smtpServer, @smtpPort, @smtpUserEmail, @smtpUserPassword, @smtpDefaultCredentials, @msgType, @msgDeliveredDateTime,  ");
                sbQuery.Append(" @msgSend, @msgCreatedBy, @smtpIsSSL, @msgEmailSubject, @msgContent  ");
                sbQuery.Append(" ) ");

                List<MySqlParameter> pList = new List<MySqlParameter>();
                pList.Add(DbUtility.GetParameter("fromEmailID", fromEmailID, MyDbType.String));
                pList.Add(DbUtility.GetParameter("toEmailID", toEmailID, MyDbType.String));
                pList.Add(DbUtility.GetParameter("smtpServer", smtpServer, MyDbType.String));
                pList.Add(DbUtility.GetParameter("smtpPort", smtpPort, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("smtpUserEmail", smtpUserEmail, MyDbType.String));
                pList.Add(DbUtility.GetParameter("smtpUserPassword", smtpUserPassword, MyDbType.String));
                pList.Add(DbUtility.GetParameter("smtpDefaultCredentials", smtpDefaultCredentials, MyDbType.String));
                pList.Add(DbUtility.GetParameter("msgType", msgType, MyDbType.String));
                pList.Add(DbUtility.GetParameter("msgDeliveredDateTime", msgDeliveredDateTime, MyDbType.DateTime));
                pList.Add(DbUtility.GetParameter("msgSend", msgSend, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("msgCreatedBy", msgCreatedBy, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("smtpIsSSL", smtpIsSSL, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("msgEmailSubject", msgEmailSubject, MyDbType.String));
                pList.Add(DbUtility.GetParameter("msgContent", msgContent, MyDbType.String));

                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                dbTransactionHelper.CommitTransaction();
                messageAlertID = dbTransactionHelper.GetLastInsertID();

                rStatus = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return rStatus;
        }


        /// <summary>
        /// To Update Attempt Request Test Alert Email ID in emptestattemptrequests and syscommunication Table
        /// </summary>
        /// <param name="alertEmailID">Pass Alter Email ID</param>
        /// <param name="attemptRequestID">Pass Attempts Request ID</param>
        /// <param name="messageAlertID">Pass Message Alert ID</param>
        /// <returns>True/False</returns>
        public Boolean UpdateTestAttemptAlertMailID(string alertEmailID, Int64 attemptRequestID, Int64 messageAlertID)
        {
            StringBuilder sbQuery = new StringBuilder();
            Boolean rStatus = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(" update emptestattemptrequests set alertEmailID= @alertEmailID where idempTestAttemptRequests = @attemptRequestID; ");
                sbQuery.Append(" update syscommunication set toEmailID= @alertEmailID where msgID = @messageAlertID; ");

                List<MySqlParameter> pList = new List<MySqlParameter>();
                pList.Add(DbUtility.GetParameter("alertEmailID", alertEmailID, MyDbType.String));
                pList.Add(DbUtility.GetParameter("attemptRequestID", attemptRequestID, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("messageAlertID", messageAlertID, MyDbType.Int));

                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                dbTransactionHelper.CommitTransaction();

                rStatus = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return rStatus;
        }



        /// <summary>
        /// To Get Course Test Percentage
        /// </summary>
        /// <param name="courseRegID">Pass Course Test Registation ID</param>
        /// <returns>Float</returns>
        public float GetTestPercentage(int courseRegID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select  round( ifnull(fun_get_Course_Percentage(@courseTestRegID),0) ,2)  ");
                object objValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("courseTestRegID", courseRegID, MyDbType.Int),
                    });
                return BusinessUtility.GetFloat(objValue);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// To Set Is Repeat Event False
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="eventID">Pass Event ID</param>
        /// <returns>True/False</returns>
        public Boolean SetEmpAssignedRepeatFalse(int empCourseAssignedID)//int empID, int eventID
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbQuery = new StringBuilder();

                sbQuery = new StringBuilder();
                sbQuery.Append(" update empassignedcourse set isRepeated = 0 , isUsed = 1  where courseEmpRecordID = @courseEmpRecordID  ");

                List<MySqlParameter> pListArry = new List<MySqlParameter>();
                pListArry.Add(DbUtility.GetParameter("courseEmpRecordID", empCourseAssignedID, MyDbType.Int));
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pListArry.ToArray());
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }


        public Int64 GetEmployeeRegistraionID(Int64 empID, Int64 courseID)
        {
            Int64 iEmployeeRegistraionID = 0;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT idRegistration FROM employeecourseregistration ");
            sbQuery.Append(" WHERE empHeader_empHdrID = @empHeader_empHdrID AND courseHeader_courseHdrID = @courseHeader_courseHdrID order by idRegistration desc limit 1 "); // AND courseHeader_courseVerNo = @courseHeader_courseVerNo and isRepeated = @isRepeated
            List<MySqlParameter> pList = new List<MySqlParameter>();
            pList.Add(DbUtility.GetParameter("empHeader_empHdrID", empID, MyDbType.String));
            pList.Add(DbUtility.GetParameter("courseHeader_courseHdrID", courseID, MyDbType.String));
            //pList.Add(DbUtility.GetParameter("courseHeader_courseVerNo", courseVerNo, MyDbType.String));
            //pList.Add(DbUtility.GetParameter("isRepeated", isRepeated, MyDbType.Int));

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object returnValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                iEmployeeRegistraionID = BusinessUtility.GetLong(returnValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return iEmployeeRegistraionID;
        }


        /// <summary>
        /// To Get Registration Course Certificate Detail
        /// </summary>
        /// <param name="regID"></param>
        public void GetEmpRegCourseCertificateDetail(Int64 regID)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT empHeader_empHdrID, courseHeader_courseHdrID, courseHeader_courseVerNo, lang, ifnull(roleID,0) as roleID,  isRepeat,IsAlert,  ");
            sbQuery.Append(" orderSeqInDisplay, repeatInDays, repeatTriggerType, repeatRateInDays, reportingKeywords, attemptResetType,  ");
            sbQuery.Append(" attemptResetType, attemptGrantWaitingTime, alertType, alertInDays, alertFormat, isRepeated, courseTestRegID, ifnull(empCourseAssignedID,0) as empCourseAssignedID ,  ");
            sbQuery.Append(" fun_get_Course_Percentage(idRegistration) as score,  fun_get_End_Date(idRegistration) as enddate ");
            sbQuery.Append(" FROM employeecourseregistration  ");
            sbQuery.Append(" WHERE idRegistration = @regID ");
            List<MySqlParameter> pList = new List<MySqlParameter>();
            pList.Add(DbUtility.GetParameter("regID", regID, MyDbType.Int));
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        this.empID = BusinessUtility.GetInt(dt.Rows[0]["empHeader_empHdrID"]);
                        this.eventID = BusinessUtility.GetInt(dt.Rows[0]["courseHeader_courseHdrID"]);
                        this.eventVerNo = BusinessUtility.GetString(dt.Rows[0]["courseHeader_courseVerNo"]);
                        this.eventLaunchedLanguage = BusinessUtility.GetString(dt.Rows[0]["lang"]);
                        this.roleID = BusinessUtility.GetInt(dt.Rows[0]["roleID"]);
                        this.RepeatRequired = BusinessUtility.GetBool(dt.Rows[0]["isRepeat"]);
                        this.AlertRequired = BusinessUtility.GetBool(dt.Rows[0]["IsAlert"]);
                        this.orderSeqInDisplay = BusinessUtility.GetInt(dt.Rows[0]["orderSeqInDisplay"]);
                        this.repeatInDays = BusinessUtility.GetInt(dt.Rows[0]["repeatInDays"]);
                        this.repeatTriggerType = BusinessUtility.GetString(dt.Rows[0]["repeatTriggerType"]);
                        this.repeatRateInDays = BusinessUtility.GetInt(dt.Rows[0]["repeatRateInDays"]);
                        this.reportingKeywords = BusinessUtility.GetString(dt.Rows[0]["reportingKeywords"]);
                        this.attemptResetType = BusinessUtility.GetString(dt.Rows[0]["attemptResetType"]);
                        this.attemptGrantWaitingTime = BusinessUtility.GetInt(dt.Rows[0]["attemptGrantWaitingTime"]);
                        this.alertType = BusinessUtility.GetString(dt.Rows[0]["alertType"]);
                        this.alertInDays = BusinessUtility.GetInt(dt.Rows[0]["alertInDays"]);
                        this.alertFormat = BusinessUtility.GetString(dt.Rows[0]["alertFormat"]);
                        this.isRepeated = BusinessUtility.GetInt(dt.Rows[0]["isRepeated"]);
                        this.courseTestRegID = BusinessUtility.GetInt(dt.Rows[0]["courseTestRegID"]);
                        this.empCourseAssignedID = BusinessUtility.GetInt(dt.Rows[0]["empCourseAssignedID"]);
                        this.passingScore = BusinessUtility.GetString(dt.Rows[0]["score"]);
                        this.endDate = BusinessUtility.GetDateTime(dt.Rows[0]["enddate"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }




        public int GetCourseVersionPassingGrade(Int64 courseID, int courseVer)
        {
            int iEmployeeRegistraionID = 0;
            StringBuilder sbInsertQuery = new StringBuilder();
            sbInsertQuery = new StringBuilder();
            sbInsertQuery.Append(" SELECT ");
            sbInsertQuery.Append(" teventdefault.minimumPassScore coursePassingGrade ");
            sbInsertQuery.Append(" FROM traningevent AS courseHdr ");
            sbInsertQuery.Append(" JOIN trainingeventdefaults AS teventdefault ON teventdefault.trainingEventID = courseHdr.courseHdrID   ");
            sbInsertQuery.Append(" and teventdefault.trainingEventVer = courseHdr.courseVerNo ");
            sbInsertQuery.Append(" WHERE courseHdr.courseHdrID = @eventID and courseHdr.courseVerNo = @eventVersion ");


            List<MySqlParameter> pList = new List<MySqlParameter>();
            pList.Add(DbUtility.GetParameter("eventID", courseID, MyDbType.String));
            pList.Add(DbUtility.GetParameter("eventVersion", courseVer, MyDbType.Int));

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object returnValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, pList.ToArray());
                iEmployeeRegistraionID = BusinessUtility.GetInt (returnValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return iEmployeeRegistraionID;
        }



        public Int64 GetEmployeeRegistraionID(Int64 empID, Int64 courseID, Int64 courseVerNo)
        {


            Int64 iEmployeeRegistraionID = 0;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT idRegistration FROM employeecourseregistration ");
            sbQuery.Append(" WHERE empHeader_empHdrID = @empHeader_empHdrID AND courseHeader_courseHdrID = @courseHeader_courseHdrID AND courseHeader_courseVerNo = @courseHeader_courseVerNo ");
            List<MySqlParameter> pList = new List<MySqlParameter>();
            pList.Add(DbUtility.GetParameter("empHeader_empHdrID", empID, MyDbType.String));
            pList.Add(DbUtility.GetParameter("courseHeader_courseHdrID", courseID, MyDbType.String));

            pList.Add(DbUtility.GetParameter("courseHeader_courseVerNo", courseVerNo, MyDbType.String));
            pList.Add(DbUtility.GetParameter("isRepeated", isRepeated, MyDbType.Int));

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                

                object returnValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                iEmployeeRegistraionID = BusinessUtility.GetLong(returnValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return iEmployeeRegistraionID;
        }

    }
}
