﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Functionality for Course Version
    /// </summary>
    public class EventVersion
    {
        /// <summary>
        /// To Create Course Version
        /// </summary>
        /// <param name="eventID">Pass Event ID</param>
        /// <param name="createdByUserID">Pass Created By User ID</param>
        /// <returns>True/False</returns>
        public Boolean CreateEventVersion(int eventID, int createdByUserID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select     if (trainingEventVer is null , 0 ,     ifnull(max(trainingEventVer),0) +1 )     from trainingeventversion where trainingeventID = @trainingeventID  "); // 
                //object objEventVersions = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, null);

                object objEventVersions = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("@trainingeventID", eventID, MyDbType.Int),
                });

                



                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery = new StringBuilder();

                // Set isVersionActive =0 for old version created 
                sbInsertQuery.Append(" update traningevent set  isVersionActive = 0  where courseHdrID = @trainingeventID order by courseVerNo Desc Limit 1; ");
                sbInsertQuery.Append(" update trainingeventdefaults set isVersionActive = 0 where trainingEventID = @trainingeventID order by trainingEventVer Desc Limit 1; ");

                sbInsertQuery.Append(" INSERT INTO trainingeventversion ( trainingeventID, trainingEventVer, createdBy, createdDatetime ) ");    // 
                sbInsertQuery.Append(" VALUES ( @trainingeventID, ");
                sbInsertQuery.Append(" @trainingeventVersion , ");
                sbInsertQuery.Append(" @createdByUserID, @createdDatetime ");
                sbInsertQuery.Append(" ); ");

                sbInsertQuery.Append(" insert into traningevent ");
                sbInsertQuery.Append(" ( ");
                sbInsertQuery.Append(" courseHdrID, courseVerNo, courseTitleEn, courseTitleFr, courseActive, courseExtID, courseExtIDFr, courseExtLink, courseCreatedOn,  ");
                sbInsertQuery.Append(" courseCreatedBy, courseLastModOn, courseLastModBy, courseQueNo, courseTestQueNo, courseCertOffered, courseType, coursePassingGrade,  ");
                sbInsertQuery.Append(" course_no_of_attempts, course_version_typ, is_new, is_updated, scorm_type, total_no_slides, TestType, TestExtID, certificateID, testCompletedCount,  ");
                sbInsertQuery.Append(" isReviewContent, isVersionActive ");
                sbInsertQuery.Append(" ) ");
                sbInsertQuery.Append(" select courseHdrID, @trainingeventVersion, courseTitleEn, courseTitleFr, courseActive, courseExtID, courseExtIDFr, courseExtLink, NOW(),  ");
                sbInsertQuery.Append(" @createdByUserID, NOW(), @createdByUserID, courseQueNo, courseTestQueNo, courseCertOffered, courseType, coursePassingGrade,  ");
                sbInsertQuery.Append(" course_no_of_attempts, course_version_typ, is_new, is_updated, scorm_type, total_no_slides, TestType, TestExtID, certificateID, testCompletedCount,  ");
                sbInsertQuery.Append(" isReviewContent, 1 from  ");
                sbInsertQuery.Append(" traningevent where courseHdrID = @trainingeventID order by courseCreatedOn Limit 1; ");

                sbInsertQuery.Append(" insert into trainingeventdefaults ");
                sbInsertQuery.Append(" ( ");
                sbInsertQuery.Append(" trainingEventID, trainingEventVer, reportingKeywords, minimumPassScore, maximumPassScore, attemptResetType, attemptGrantWaitingTime, orderSeqInDisplay, userDisplayPriority,  ");
                sbInsertQuery.Append(" repeatRequired, repeatInDays, repeatTriggerType, alertRequired, alertType, alertInDays, alertFormat, alertPortalMsg, alertEmailMsg, alertEscalationEmailMsg,  ");
                sbInsertQuery.Append(" alertEmailSubject, alertEscalationEmailSubject, alertBeforeLaunch, alertNotCompleted, alertManagerNotCompleted, alertRepeatInDays, repeatFixedDate, fixedDay,  ");
                sbInsertQuery.Append(" fixedMonth, startdatetime, isVersionActive ) ");
                sbInsertQuery.Append(" select  ");
                sbInsertQuery.Append(" trainingEventID, @trainingeventVersion, reportingKeywords, minimumPassScore, maximumPassScore, attemptResetType, attemptGrantWaitingTime, orderSeqInDisplay, userDisplayPriority,  ");
                sbInsertQuery.Append(" repeatRequired, repeatInDays, repeatTriggerType, alertRequired, alertType, alertInDays, alertFormat, alertPortalMsg, alertEmailMsg, alertEscalationEmailMsg,  ");
                sbInsertQuery.Append(" alertEmailSubject, alertEscalationEmailSubject, alertBeforeLaunch, alertNotCompleted, alertManagerNotCompleted, alertRepeatInDays, repeatFixedDate, fixedDay,  ");
                sbInsertQuery.Append(" fixedMonth, startdatetime, 1  ");
                sbInsertQuery.Append(" from trainingeventdefaults where trainingEventID = @trainingeventID order by trainingEventVer Desc Limit 1; ");

                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("trainingeventID", eventID, MyDbType.Int),
                DbUtility.GetParameter("trainingeventVersion", BusinessUtility.GetInt(objEventVersions), MyDbType.Int),
                DbUtility.GetParameter("createdByUserID", createdByUserID, MyDbType.Int),
                DbUtility.GetParameter("createdDatetime", DateTime.Now, MyDbType.DateTime),
                });

                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Get Course Current Version
        /// </summary>
        /// <returns>Int</returns>
        public int GetCourseCurrentVersion(int eventID)
        {
            DbHelper dbHelper = new DbHelper(false);
            int iCourseVersion = 0;
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT  if (trainingEventVer is null , 0 ,     ifnull(max(trainingEventVer),0) )  FROM trainingeventversion "); // 
                sbInsertQuery.Append(" where trainingeventID = @eventID  ");
                //object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, null);

                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("eventID", eventID, MyDbType.Int),
                });
                iCourseVersion = BusinessUtility.GetInt(rValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return iCourseVersion;
        }


        /// <summary>
        /// To Get Course Version List 
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetCourseVersionList(int eventID)
        {
            DbHelper dbHelper = new DbHelper(false);
            DataTable dtCourseVersion = new DataTable();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT trainingEventVer FROM trainingeventversion  ");
                sbInsertQuery.Append(" where trainingeventID = @eventID  ");
                //dtCourseVersion = dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, null);

                dtCourseVersion = dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("eventID", eventID, MyDbType.Int),
                });

                //object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                //DbUtility.GetParameter("eventID", eventID, MyDbType.Int),
                //});
                //iCourseVersion = BusinessUtility.GetInt(rValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }

            return dtCourseVersion;
        }

    }
}
