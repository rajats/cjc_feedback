﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Web;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Functionality To Assigned Employee Courses
    /// </summary>
    public class EmpAssignedCourses
    {

        /// <summary>
        /// TO Get/Set Role ID
        /// </summary>
        public int RoleID { get; set; }

        public int CourseID { get; set; }

        public string EmailFrom { get; set; }

        public string EmailTo { get; set; }

        public int CurrentUserLogInID { get; set; }

        public int EmployeeID { get; set; }

        public int InActiveReason { get; set; }

        public int LastRecordUpdateSource { get; set; }

        /// <summary>
        /// To Get Master Role ID To Be Applied on Employee Assigned Courses
        /// </summary>
        /// <param name="empID">Pass Emplyee ID</param>
        /// <param name="courseHdrID">Pass Course ID</param>
        /// <returns>DataTable</returns>
        public int GetRoleIDToApply(int empID, int courseHdrID)
        {
            int roleID = 0;


            DataTable dtEmployeeList = new DataTable();
            StringBuilder sbQuery = new StringBuilder();

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbQueryEmployeeCourse = new StringBuilder();


                sbQueryEmployeeCourse.Append(" select * from (");

                sbQueryEmployeeCourse.Append(" select emprolecourseID.CourseEmpHdrID, emprolecourseID.CourseID, emprolecourseID.CourseVer, emprolecourseID.RoleID,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.repeatRequired, teventdefault.repeatRequired) as repeatRequired ,  ");
                sbQueryEmployeeCourse.Append(" ifnull(roletraning.minimumPassScore, teventdefault.minimumPassScore) as minimumPassScore,  ");
                sbQueryEmployeeCourse.Append(" ifnull(roletraning.orderSeqInDisplay, teventdefault.orderSeqInDisplay) as orderSeqInDisplay,  ");
                sbQueryEmployeeCourse.Append(" ifnull(roletraning.repeatInDays, teventdefault.repeatInDays) as repeatInDays,  ");
                sbQueryEmployeeCourse.Append(" ifnull(roletraning.repeatTriggerType, teventdefault.repeatTriggerType) as repeatTriggerType,  ");
                sbQueryEmployeeCourse.Append(" ifnull(roletraning.startdatetime, teventdefault.startdatetime) as startdatetime, rolemaster.emprolecreatedon, ");
                sbQueryEmployeeCourse.Append(" case ifnull(roletraning.repeatTriggerType, teventdefault.repeatTriggerType) when 'F' then 365 else ifnull(roletraning.repeatInDays, teventdefault.repeatInDays)  end  as RepeatRate ");
                sbQueryEmployeeCourse.Append(" from  ");
                sbQueryEmployeeCourse.Append(" ( ");
                sbQueryEmployeeCourse.Append(" select *    ");//,
                //sbQueryEmployeeCourse.Append(" fun_get_Course_completed(  (SELECT   ");
                //sbQueryEmployeeCourse.Append(" idRegistration  ");
                //sbQueryEmployeeCourse.Append(" FROM  ");
                //sbQueryEmployeeCourse.Append(" employeecourseregistration  ");
                //sbQueryEmployeeCourse.Append(" WHERE  ");
                //sbQueryEmployeeCourse.Append(" empHeader_empHdrID = Result.CourseEmpHdrID  ");
                //sbQueryEmployeeCourse.Append(" AND courseHeader_courseHdrID = Result.CourseID   ");
                //sbQueryEmployeeCourse.Append(" AND courseHeader_courseVerNo = Result.CourseVer)) AS IsCompleted ,  ");
                //sbQueryEmployeeCourse.Append(" fun_get_Event_completed(  (SELECT   ");
                //sbQueryEmployeeCourse.Append(" idRegistration  ");
                //sbQueryEmployeeCourse.Append(" FROM  ");
                //sbQueryEmployeeCourse.Append(" employeecourseregistration  ");
                //sbQueryEmployeeCourse.Append(" WHERE  ");
                //sbQueryEmployeeCourse.Append(" empHeader_empHdrID = Result.CourseEmpHdrID  ");
                //sbQueryEmployeeCourse.Append(" AND courseHeader_courseHdrID = Result.CourseID   ");
                //sbQueryEmployeeCourse.Append(" AND courseHeader_courseVerNo = Result.CourseVer)) AS IsContentCompleted  ");
                sbQueryEmployeeCourse.Append(" from   ");
                sbQueryEmployeeCourse.Append(" (  ");
                sbQueryEmployeeCourse.Append(" SELECT DISTINCT @empID  AS CourseEmpHdrID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseHdrID AS CourseID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseVerNo AS CourseVer, CURRENT_TIMESTAMP(), 1, empRolesIn.empRoles_empRoleID as RoleID  ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" emprolesincludes AS empRolesIn ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" emprolecourses AS empRolCourses ON empRolCourses.empRoles_empRoleID = empRolesIn.empRoles_empRoleID ");
                sbQueryEmployeeCourse.Append(" AND empRolesIn.empHeader_empHdrID = @empID  ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" traningevent AS courseHdr ON courseHdr.courseHdrID = empRolCourses.courseHeader_courseHdrID ");
                sbQueryEmployeeCourse.Append(" AND courseHdr.CourseActive = 1 AND courseHdr.isVersionActive = 1 ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" coursetype AS courstyp ON courstyp.Type = courseHdr.courseType ");
                sbQueryEmployeeCourse.Append(" LEFT JOIN ");
                sbQueryEmployeeCourse.Append(" trainingeventdetail trnDtl ON trnDtl.trn_course_hdr_id = empRolCourses.courseHeader_courseHdrID ");
                sbQueryEmployeeCourse.Append(" WHERE ");
                sbQueryEmployeeCourse.Append(" 1=1 ");
                //sbQueryEmployeeCourse.Append(" empRolCourses.courseHeader_courseHdrID NOT IN (SELECT DISTINCT ");
                //sbQueryEmployeeCourse.Append(" courseHeader_courseHdrID ");
                //sbQueryEmployeeCourse.Append(" FROM ");
                //sbQueryEmployeeCourse.Append(" employeecourseregistration ");
                //sbQueryEmployeeCourse.Append(" WHERE ");
                //sbQueryEmployeeCourse.Append(" empHeader_empHdrID = @empID)   ");
                sbQueryEmployeeCourse.Append("  UNION SELECT DISTINCT @empID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseHdrID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseVerNo, CURRENT_TIMESTAMP(), 1, syroleSel.emp_roleID as RoleID ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel AS syroleSel ");
                sbQueryEmployeeCourse.Append(" JOIN ");
                sbQueryEmployeeCourse.Append(" empreference eRef ON eRef.sysEmpReferenceCodes_sysRefCode = syroleSel.sys_ref_code ");
                sbQueryEmployeeCourse.Append(" AND eRef.empHeader_empHdrID = @empID  ");
                sbQueryEmployeeCourse.Append(" AND (syroleSel.sys_cond IS NULL  ");
                sbQueryEmployeeCourse.Append(" OR syroleSel.sys_cond = 'OR')  ");
                sbQueryEmployeeCourse.Append(" AND  ");
                sbQueryEmployeeCourse.Append(" (   ");
                sbQueryEmployeeCourse.Append(" ( ");
                sbQueryEmployeeCourse.Append(" (SELECT  ");
                sbQueryEmployeeCourse.Append(" COUNT(*) ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel AS AND1syroleSel ");
                sbQueryEmployeeCourse.Append(" JOIN ");
                sbQueryEmployeeCourse.Append(" empreference eRef1 ON eRef1.sysEmpReferenceCodes_sysRefCode = AND1syroleSel.sys_ref_code ");
                sbQueryEmployeeCourse.Append(" AND eRef1.empHeader_empHdrID =    @empID  ");
                sbQueryEmployeeCourse.Append(" AND AND1syroleSel.sys_cond = 'AND' AND AND1syroleSel.sys_cond IS NOT NULL  ");
                sbQueryEmployeeCourse.Append(" AND AND1syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(AND1syroleSel.sys_ref_code,  ");
                sbQueryEmployeeCourse.Append(" eRef1.empHeader_empHdrID) where AND1syroleSel.emp_roleid = syroleSel.emp_roleid) =  ( select count(*)  FROM  sysrolerefsel as AND2syroleSel WHERE  ");
                sbQueryEmployeeCourse.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbQueryEmployeeCourse.Append(" AND AND2syroleSel.sys_cond = 'AND')  ");
                sbQueryEmployeeCourse.Append(" AND (SELECT  ");
                sbQueryEmployeeCourse.Append(" COUNT(*) ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel as AND2syroleSel ");
                sbQueryEmployeeCourse.Append(" WHERE ");
                sbQueryEmployeeCourse.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbQueryEmployeeCourse.Append(" AND AND2syroleSel.sys_cond = 'AND') > 0  ");
                sbQueryEmployeeCourse.Append(" )  ");
                sbQueryEmployeeCourse.Append(" OR  ");
                sbQueryEmployeeCourse.Append(" ( ");
                sbQueryEmployeeCourse.Append(" (SELECT  ");
                sbQueryEmployeeCourse.Append(" COUNT(*) ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel as ORsyroleSel ");
                sbQueryEmployeeCourse.Append(" WHERE ");
                sbQueryEmployeeCourse.Append(" ORsyroleSel.emp_roleid =  syroleSel.emp_roleid ");
                sbQueryEmployeeCourse.Append(" AND ORsyroleSel.sys_cond = 'AND') = 0 ");
                sbQueryEmployeeCourse.Append(" )   ");
                sbQueryEmployeeCourse.Append(" ) ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" emprolecourses AS empRolCourses ON empRolCourses.empRoles_empRoleID = syroleSel.emp_roleID ");
                sbQueryEmployeeCourse.Append(" AND (syroleSel.sys_ref_value = fun_get_emp_ref_val(syroleSel.sys_ref_code, ");
                sbQueryEmployeeCourse.Append(" empHeader_empHdrID)) ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" traningevent AS courseHdr ON courseHdr.courseHdrID = empRolCourses.courseHeader_courseHdrID ");
                sbQueryEmployeeCourse.Append(" AND courseHdr.CourseActive = 1 AND courseHdr.isVersionActive = 1 ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" coursetype AS courstyp ON courstyp.Type = courseHdr.courseType ");
                sbQueryEmployeeCourse.Append(" LEFT JOIN ");
                sbQueryEmployeeCourse.Append(" trainingeventdetail trnDtl ON trnDtl.trn_course_hdr_id = empRolCourses.courseHeader_courseHdrID ");
                sbQueryEmployeeCourse.Append(" WHERE ");
                sbQueryEmployeeCourse.Append(" 1=1 ");
                //sbQueryEmployeeCourse.Append(" empRolCourses.courseHeader_courseHdrID NOT IN (SELECT DISTINCT ");
                //sbQueryEmployeeCourse.Append(" courseHeader_courseHdrID ");
                //sbQueryEmployeeCourse.Append(" FROM ");
                //sbQueryEmployeeCourse.Append(" employeecourseregistration ");
                //sbQueryEmployeeCourse.Append(" WHERE ");
                //sbQueryEmployeeCourse.Append(" empHeader_empHdrID =   @empID  )   ");
                sbQueryEmployeeCourse.Append(" ) as Result   ");

                sbQueryEmployeeCourse.Append(" )  ");
                sbQueryEmployeeCourse.Append(" as emprolecourseID ");
                sbQueryEmployeeCourse.Append(" left join emproletrainingevtvals as roletraning on roletraning.empRoleID = emprolecourseID.RoleID and roletraning.trainingEventID = emprolecourseID.CourseID   ");
                sbQueryEmployeeCourse.Append(" left join trainingeventdefaults as teventdefault on teventdefault.trainingEventID = emprolecourseID.CourseID ");
                sbQueryEmployeeCourse.Append(" join sysrolemaster as rolemaster on rolemaster.empRoleID = emprolecourseID.RoleID ");
                sbQueryEmployeeCourse.Append("  ) as FinalResult ");
                sbQueryEmployeeCourse.Append(" where CourseID = @courseID and CourseEmpHdrID = @empID ");

                DataTable dtEmployeeCourse = dbHelp.GetDataTable(BusinessUtility.GetString(sbQueryEmployeeCourse), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("empID", empID, MyDbType.Int),
                        DbUtility.GetParameter("courseID", courseHdrID, MyDbType.Int),
                        });


                if (dtEmployeeCourse.Rows.Count == 1)
                {
                    roleID = BusinessUtility.GetInt(dtEmployeeCourse.Rows[0]["RoleID"]);
                }
                else
                {
                    int minRepeatRate = 0;
                    int minPassingScore = 0;

                    minRepeatRate = BusinessUtility.GetInt(dtEmployeeCourse.Compute("min(RepeatRate)", string.Empty));
                    minPassingScore = BusinessUtility.GetInt(dtEmployeeCourse.Compute("max(minimumPassScore)", string.Empty));

                    sbQuery = new StringBuilder();
                    sbQuery.Append(sbQueryEmployeeCourse);
                    sbQuery.Append(" AND RepeatRate = " + minRepeatRate);
                    DataTable dtRepeatRate = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("empID", empID, MyDbType.Int),
                        DbUtility.GetParameter("courseID", courseHdrID, MyDbType.Int),
                        });

                    if (dtRepeatRate.Rows.Count == 1)
                    {
                        roleID = BusinessUtility.GetInt(dtRepeatRate.Rows[0]["RoleID"]);
                    }
                    else
                    {
                        sbQuery = new StringBuilder();
                        sbQuery.Append(sbQueryEmployeeCourse);
                        sbQuery.Append(" AND minimumPassScore = " + minPassingScore);
                        DataTable dtMinPassingScore = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("empID", empID, MyDbType.Int),
                        DbUtility.GetParameter("courseID", courseHdrID, MyDbType.Int),
                        });

                        if (dtMinPassingScore.Rows.Count == 1)
                        {
                            roleID = BusinessUtility.GetInt(dtMinPassingScore.Rows[0]["RoleID"]);
                        }
                        else
                        {
                            sbQuery = new StringBuilder();
                            sbQuery.Append(sbQueryEmployeeCourse);
                            sbQuery.Append(" Order By emprolecreatedon ASC Limit 1 ");
                            DataTable dtMinRoleCreatedDateTime = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empID", empID, MyDbType.Int),
                            DbUtility.GetParameter("courseID", courseHdrID, MyDbType.Int),
                            });

                            roleID = BusinessUtility.GetInt(dtMinRoleCreatedDateTime.Rows[0]["RoleID"]);

                        }
                    }

                }


            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }

            return roleID;
        }

        /// <summary>
        /// To Get Course Detail Applied on Master Role
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="courseHdrID">Pass Course ID</param>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetRoleCourseDetail(int empID, int courseHdrID, int roleID)
        {
            DataTable dtEmployeeList = new DataTable();
            StringBuilder sbQuery = new StringBuilder();

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbQueryEmployeeCourse = new StringBuilder();
                sbQueryEmployeeCourse.Append(" select * from (");

                sbQueryEmployeeCourse.Append(" select emprolecourseID.CourseEmpHdrID, emprolecourseID.CourseID, emprolecourseID.CourseVer, emprolecourseID.RoleID,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.repeatRequired, teventdefault.repeatRequired) as repeatRequired ,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertRequired, teventdefault.alertRequired) as alertRequired ,  ");
                sbQueryEmployeeCourse.Append(" ifnull(roletraning.minimumPassScore, teventdefault.minimumPassScore) as minimumPassScore,  ");
                sbQueryEmployeeCourse.Append(" ifnull(roletraning.orderSeqInDisplay, teventdefault.orderSeqInDisplay) as orderSeqInDisplay,  ");
                sbQueryEmployeeCourse.Append(" ifnull(roletraning.repeatInDays, teventdefault.repeatInDays) as repeatInDays,  ");
                sbQueryEmployeeCourse.Append(" ifnull(roletraning.repeatTriggerType, teventdefault.repeatTriggerType) as repeatTriggerType,  ");
                sbQueryEmployeeCourse.Append(" ifnull(roletraning.startdatetime, teventdefault.startdatetime) as startdatetime, rolemaster.emprolecreatedon, ");
                sbQueryEmployeeCourse.Append(" case ifnull(roletraning.repeatTriggerType, teventdefault.repeatTriggerType) when 'F' then 365 else ifnull(roletraning.repeatInDays, teventdefault.repeatInDays)  end  as RepeatRate, ");

                sbQueryEmployeeCourse.Append(" ifnull( roletraning.reportingKeywords, teventdefault.reportingKeywords) as reportingKeywords,   ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.attemptResetType, teventdefault.attemptResetType) as attemptResetType,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.attemptGrantWaitingTime, teventdefault.attemptGrantWaitingTime) as attemptGrantWaitingTime,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertType, teventdefault.alertType) as alertType,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertInDays, teventdefault.alertInDays) as alertInDays,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertFormat, teventdefault.alertFormat) as alertFormat,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertPortalMsg, teventdefault.alertPortalMsg) as alertPortalMsg,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertEmailMsg, teventdefault.alertEmailMsg) as alertEmailMsg,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertEscalationEmailMsg, teventdefault.alertEscalationEmailMsg) as alertEscalationEmailMsg,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertEmailSubject, teventdefault.alertEmailSubject) as alertEmailSubject,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertEscalationEmailSubject, teventdefault.alertEscalationEmailSubject) as alertEscalationEmailSubject,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertBeforeLaunch, teventdefault.alertBeforeLaunch) as alertBeforeLaunch,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertNotCompleted, teventdefault.alertNotCompleted) as alertNotCompleted,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertManagerNotCompleted, teventdefault.alertManagerNotCompleted) as alertManagerNotCompleted,  ");
                sbQueryEmployeeCourse.Append(" ifnull( roletraning.alertRepeatInDays, teventdefault.alertRepeatInDays) as alertRepeatInDays  ");

                sbQueryEmployeeCourse.Append(" from  ");
                sbQueryEmployeeCourse.Append(" ( ");
                sbQueryEmployeeCourse.Append(" select * ,  'No' AS IsCompleted , 'No' AS IsContentCompleted ");
                //sbQueryEmployeeCourse.Append(" fun_get_Course_completed(  (SELECT   ");
                //sbQueryEmployeeCourse.Append(" idRegistration  ");
                //sbQueryEmployeeCourse.Append(" FROM  ");
                //sbQueryEmployeeCourse.Append(" employeecourseregistration  ");
                //sbQueryEmployeeCourse.Append(" WHERE  ");
                //sbQueryEmployeeCourse.Append(" empHeader_empHdrID = Result.CourseEmpHdrID  ");
                //sbQueryEmployeeCourse.Append(" AND courseHeader_courseHdrID = Result.CourseID   ");
                //sbQueryEmployeeCourse.Append(" AND courseHeader_courseVerNo = Result.CourseVer)) AS IsCompleted ,  ");
                //sbQueryEmployeeCourse.Append(" fun_get_Event_completed(  (SELECT   ");
                //sbQueryEmployeeCourse.Append(" idRegistration  ");
                //sbQueryEmployeeCourse.Append(" FROM  ");
                //sbQueryEmployeeCourse.Append(" employeecourseregistration  ");
                //sbQueryEmployeeCourse.Append(" WHERE  ");
                //sbQueryEmployeeCourse.Append(" empHeader_empHdrID = Result.CourseEmpHdrID  ");
                //sbQueryEmployeeCourse.Append(" AND courseHeader_courseHdrID = Result.CourseID   ");
                //sbQueryEmployeeCourse.Append(" AND courseHeader_courseVerNo = Result.CourseVer)) AS IsContentCompleted  ");
                sbQueryEmployeeCourse.Append(" from   ");
                sbQueryEmployeeCourse.Append(" (  ");
                sbQueryEmployeeCourse.Append(" SELECT DISTINCT @empID  AS CourseEmpHdrID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseHdrID AS CourseID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseVerNo AS CourseVer, CURRENT_TIMESTAMP(), 1, empRolesIn.empRoles_empRoleID as RoleID  ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" emprolesincludes AS empRolesIn ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" emprolecourses AS empRolCourses ON empRolCourses.empRoles_empRoleID = empRolesIn.empRoles_empRoleID ");
                sbQueryEmployeeCourse.Append(" AND empRolesIn.empHeader_empHdrID = @empID  ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" traningevent AS courseHdr ON courseHdr.courseHdrID = empRolCourses.courseHeader_courseHdrID ");
                sbQueryEmployeeCourse.Append(" AND courseHdr.CourseActive = 1 and courseHdr.isVersionActive = 1 ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" coursetype AS courstyp ON courstyp.Type = courseHdr.courseType ");
                sbQueryEmployeeCourse.Append(" LEFT JOIN ");
                sbQueryEmployeeCourse.Append(" trainingeventdetail trnDtl ON trnDtl.trn_course_hdr_id = empRolCourses.courseHeader_courseHdrID ");
                //sbQueryEmployeeCourse.Append(" WHERE ");
                //sbQueryEmployeeCourse.Append(" empRolCourses.courseHeader_courseHdrID NOT IN (SELECT DISTINCT ");
                //sbQueryEmployeeCourse.Append(" courseHeader_courseHdrID ");
                //sbQueryEmployeeCourse.Append(" FROM ");
                //sbQueryEmployeeCourse.Append(" employeecourseregistration ");
                //sbQueryEmployeeCourse.Append(" WHERE ");
                //sbQueryEmployeeCourse.Append(" empHeader_empHdrID = @empID)    ");
                sbQueryEmployeeCourse.Append(" UNION SELECT DISTINCT @empID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseHdrID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseVerNo, CURRENT_TIMESTAMP(), 1, syroleSel.emp_roleID as RoleID ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel AS syroleSel ");
                sbQueryEmployeeCourse.Append(" JOIN ");
                sbQueryEmployeeCourse.Append(" empreference eRef ON eRef.sysEmpReferenceCodes_sysRefCode = syroleSel.sys_ref_code ");
                sbQueryEmployeeCourse.Append(" AND eRef.empHeader_empHdrID = @empID  ");
                sbQueryEmployeeCourse.Append(" AND (syroleSel.sys_cond IS NULL  ");
                sbQueryEmployeeCourse.Append(" OR syroleSel.sys_cond = 'OR')  ");
                sbQueryEmployeeCourse.Append(" AND  ");
                sbQueryEmployeeCourse.Append(" (   ");
                sbQueryEmployeeCourse.Append(" ( ");
                sbQueryEmployeeCourse.Append(" (SELECT  ");
                sbQueryEmployeeCourse.Append(" COUNT(*) ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel AS AND1syroleSel ");
                sbQueryEmployeeCourse.Append(" JOIN ");
                sbQueryEmployeeCourse.Append(" empreference eRef1 ON eRef1.sysEmpReferenceCodes_sysRefCode = AND1syroleSel.sys_ref_code ");
                sbQueryEmployeeCourse.Append(" AND eRef1.empHeader_empHdrID =    @empID  ");
                sbQueryEmployeeCourse.Append(" AND AND1syroleSel.sys_cond = 'AND' AND AND1syroleSel.sys_cond IS NOT NULL  ");
                sbQueryEmployeeCourse.Append(" AND AND1syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(AND1syroleSel.sys_ref_code,  ");
                sbQueryEmployeeCourse.Append(" eRef1.empHeader_empHdrID) where AND1syroleSel.emp_roleid = syroleSel.emp_roleid) =  ( select count(*)  FROM  sysrolerefsel as AND2syroleSel WHERE  ");
                sbQueryEmployeeCourse.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbQueryEmployeeCourse.Append(" AND AND2syroleSel.sys_cond = 'AND')  ");
                sbQueryEmployeeCourse.Append(" AND (SELECT  ");
                sbQueryEmployeeCourse.Append(" COUNT(*) ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel as AND2syroleSel ");
                sbQueryEmployeeCourse.Append(" WHERE ");
                sbQueryEmployeeCourse.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbQueryEmployeeCourse.Append(" AND AND2syroleSel.sys_cond = 'AND') > 0  ");
                sbQueryEmployeeCourse.Append(" )  ");
                sbQueryEmployeeCourse.Append(" OR  ");
                sbQueryEmployeeCourse.Append(" ( ");
                sbQueryEmployeeCourse.Append(" (SELECT  ");
                sbQueryEmployeeCourse.Append(" COUNT(*) ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel as ORsyroleSel ");
                sbQueryEmployeeCourse.Append(" WHERE ");
                sbQueryEmployeeCourse.Append(" ORsyroleSel.emp_roleid =  syroleSel.emp_roleid ");
                sbQueryEmployeeCourse.Append(" AND ORsyroleSel.sys_cond = 'AND') = 0 ");
                sbQueryEmployeeCourse.Append(" )   ");
                sbQueryEmployeeCourse.Append(" ) ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" emprolecourses AS empRolCourses ON empRolCourses.empRoles_empRoleID = syroleSel.emp_roleID ");
                sbQueryEmployeeCourse.Append(" AND (syroleSel.sys_ref_value = fun_get_emp_ref_val(syroleSel.sys_ref_code, ");
                sbQueryEmployeeCourse.Append(" empHeader_empHdrID)) ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" traningevent AS courseHdr ON courseHdr.courseHdrID = empRolCourses.courseHeader_courseHdrID ");
                sbQueryEmployeeCourse.Append(" AND courseHdr.CourseActive = 1  and courseHdr.isVersionActive = 1 ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" coursetype AS courstyp ON courstyp.Type = courseHdr.courseType ");
                sbQueryEmployeeCourse.Append(" LEFT JOIN ");
                sbQueryEmployeeCourse.Append(" trainingeventdetail trnDtl ON trnDtl.trn_course_hdr_id = empRolCourses.courseHeader_courseHdrID ");
                //sbQueryEmployeeCourse.Append(" WHERE ");
                //sbQueryEmployeeCourse.Append(" empRolCourses.courseHeader_courseHdrID NOT IN (SELECT DISTINCT ");
                //sbQueryEmployeeCourse.Append(" courseHeader_courseHdrID ");
                //sbQueryEmployeeCourse.Append(" FROM ");
                //sbQueryEmployeeCourse.Append(" employeecourseregistration ");
                //sbQueryEmployeeCourse.Append(" WHERE ");
                //sbQueryEmployeeCourse.Append(" empHeader_empHdrID =   @empID  )   ");
                sbQueryEmployeeCourse.Append(" ) as Result   ");

                sbQueryEmployeeCourse.Append(" )  ");
                sbQueryEmployeeCourse.Append(" as emprolecourseID ");
                sbQueryEmployeeCourse.Append(" left join emproletrainingevtvals as roletraning on roletraning.empRoleID = emprolecourseID.RoleID and roletraning.trainingEventID = emprolecourseID.CourseID   ");
                sbQueryEmployeeCourse.Append(" left join trainingeventdefaults as teventdefault on teventdefault.trainingEventID = emprolecourseID.CourseID ");
                sbQueryEmployeeCourse.Append(" join sysrolemaster as rolemaster on rolemaster.empRoleID = emprolecourseID.RoleID ");
                sbQueryEmployeeCourse.Append("  ) as FinalResult ");
                sbQueryEmployeeCourse.Append(" where CourseID = @courseID and CourseEmpHdrID = @empID and RoleID = @roleID ");

                dtEmployeeList = dbHelp.GetDataTable(BusinessUtility.GetString(sbQueryEmployeeCourse), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("empID", empID, MyDbType.Int),
                        DbUtility.GetParameter("courseID", courseHdrID, MyDbType.Int),
                        DbUtility.GetParameter("roleID", roleID, MyDbType.Int),
                        });

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }

            return dtEmployeeList;
        }


        /// <summary>
        /// Allow To Repeate Course
        /// </summary>
        /// <param name="regID">Pass Registration ID</param>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="repeatType">Pass Repeat Type</param>
        /// <param name="displaySeq">Pass Display Sequence</param>
        /// <param name="repeatDays">Pass Repeat Days</param>
        /// <returns>True/False</returns>
        public bool RepeatCourse(Int64 regID, Int64 roleID, string repeatType, int displaySeq, int repeatDays)
        {
            bool rStatus = false;
            StringBuilder sbQuery = new StringBuilder();
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                EmployeeCourseRegistration objEmpCreg = new EmployeeCourseRegistration();
                objEmpCreg.GetEmployeeRegistraionDetail(regID);

                List<MySqlParameter> pList = new List<MySqlParameter>();
                sbQuery.Append(" update employeecourseregistration set isRepeated =0 where idRegistration = @regID; ");
                sbQuery.Append(" update employeecourseregistration set isRepeated =0 where idRegistration= @courseTestRegID ; ");
                sbQuery.Append(" insert into empassignedcourse ");
                sbQuery.Append("  (CourseEmpHdrID, CourseID, CourseVer, recordActive,  startdatetime,  ");
                sbQuery.Append("  roleID, orderSeqInDisplay, isRepeated ) ");

                sbQuery.Append("  select empreg.empHeader_empHdrID as empHdrID, empreg.courseHeader_courseHdrID, tevnt.courseVerNo as CourseVer, ");
                sbQuery.Append("  1,     ");

                if (TrainingRepeatTriggerType.FixedDate == repeatType)
                {
                    EmployeeCourseRegistration objCourseRegDetail = new EmployeeCourseRegistration();
                    objCourseRegDetail.GetEmployeeRegistraionDetail(regID);

                    RoleTrainingEventDefault objRoleTraningEventDefatul = new RoleTrainingEventDefault();
                    objRoleTraningEventDefatul.GetRoleCourseDetail(BusinessUtility.GetInt(roleID), BusinessUtility.GetInt(objCourseRegDetail.eventID));


                    int iDays = objRoleTraningEventDefatul.FixedDay;
                    int iMonth = objRoleTraningEventDefatul.FixedMonth;
                    int iYear = DateTime.Now.Year;
                    int iNewYear = DateTime.Now.AddYears(1).Year;

                    DateTime dtFixedDateTime = BusinessUtility.GetDateTime(iDays.ToString("D2") + "/" + iMonth.ToString("D2") + "/" + iYear, DateFormat.ddMMyyyy);

                    if (DateTime.Now <= dtFixedDateTime)
                    {
                        sbQuery.Append("  @dtFixedDateTime  ");
                        dtFixedDateTime = BusinessUtility.GetDateTime(iDays.ToString("D2") + "/" + iMonth.ToString("D2") + "/" + iYear, DateFormat.ddMMyyyy);
                        pList.Add(DbUtility.GetParameter("dtFixedDateTime", dtFixedDateTime, MyDbType.DateTime));
                        //ErrorLog.createLog("3" + " " + BusinessUtility.GetString(dtFixedDateTime.ToString()));
                    }
                    else
                    {
                        sbQuery.Append("  @dtFixedDateTime  ");
                        dtFixedDateTime = BusinessUtility.GetDateTime(iDays.ToString("D2") + "/" + iMonth.ToString("D2") + "/" + iNewYear, DateFormat.ddMMyyyy);
                        pList.Add(DbUtility.GetParameter("dtFixedDateTime", dtFixedDateTime, MyDbType.DateTime));

                        //ErrorLog.createLog("4" + " " + BusinessUtility.GetString(dtFixedDateTime.ToString()));
                    }
                    //sbQuery.Append("  DATE_ADD( DATE_ADD(date(  DATE_ADD(rs.completeddatetime, INTERVAL 365 DAY)   ), interval 23 hour), interval 59 minute)  ");
                }
                else if (TrainingRepeatTriggerType.RelativeNoDaysFromCompleteDate == repeatType)
                {
                    sbQuery.Append("  DATE_ADD( DATE_ADD(date(  DATE_ADD(rs.completeddatetime, INTERVAL " + repeatDays + " DAY)   ), interval 00 hour), interval 00 minute)  ");
                }
                else if (TrainingRepeatTriggerType.RelativeNoDaysFromStartDate == repeatType)
                {
                    sbQuery.Append("  DATE_ADD( DATE_ADD(date(  DATE_ADD(rs.createddatetime, INTERVAL " + repeatDays + " DAY)   ), interval 00 hour), interval 00 minute)  ");
                }
                else if (TrainingRepeatTriggerType.RepeatFromEligibleDate == repeatType)
                {
                    sbQuery.Append("  DATE_ADD( DATE_ADD(date(  DATE_ADD(empcourse.startdatetime, INTERVAL " + repeatDays + " DAY)   ), interval 00 hour), interval 00 minute)  ");
                }
                //else
                //{
                //    sbQuery.Append("  CURRENT_TIMESTAMP() ");
                //}

                sbQuery.Append(" , @roleID, @orderSeqInDisplay, 1 from reportsummary rs ");
                sbQuery.Append("  join employeecourseregistration empreg on empreg.idRegistration = rs.registrationID ");
                sbQuery.Append("  join traningevent tevnt on tevnt.courseHdrID = empreg.courseHeader_courseHdrID and tevnt.isVersionActive = 1  ");
                sbQuery.Append("  left join empassignedcourse empcourse on empcourse.courseEmpRecordID = empreg.empCourseAssignedID ");
                sbQuery.Append("  where rs.registrationID = @regid; ");


                if (TrainingRepeatTriggerType.FixedDate == repeatType)
                {

                    EmployeeCourseRegistration objCourseRegDetail = new EmployeeCourseRegistration();
                    objCourseRegDetail.GetEmployeeRegistraionDetail(regID);

                    RoleTrainingEventDefault objRoleTraningEventDefatul = new RoleTrainingEventDefault();
                    objRoleTraningEventDefatul.GetRoleCourseDetail(BusinessUtility.GetInt(roleID), BusinessUtility.GetInt(objCourseRegDetail.eventID));


                    int iDays = objRoleTraningEventDefatul.FixedDay;
                    int iMonth = objRoleTraningEventDefatul.FixedMonth;
                    int iYear = DateTime.Now.Year;
                    int iNewYear = DateTime.Now.AddYears(1).Year;

                    DateTime dtExpirtyDateTime = BusinessUtility.GetDateTime(iDays.ToString("D2") + "/" + iMonth.ToString("D2") + "/" + iYear, DateFormat.ddMMyyyy);



                    sbQuery.Append("  update reportsummary rs set ");
                    //sbQuery.Append(" recordExpiryDateTime =  DATE_ADD( DATE_ADD(date(  DATE_ADD(rs.completeddatetime, INTERVAL 365 DAY)   ), interval 23 hour), interval 59 minute) ");
                    if (DateTime.Now <= dtExpirtyDateTime)
                    {

                        //sbQuery.Append("  @dtFixedDateTime  ");
                        dtExpirtyDateTime = BusinessUtility.GetDateTime(iDays.ToString("D2") + "/" + iMonth.ToString("D2") + "/" + iYear, DateFormat.ddMMyyyy);
                        //ErrorLog.createLog("1" + " " + BusinessUtility.GetString(dtExpirtyDateTime.ToString()));
                        pList.Add(DbUtility.GetParameter("dtExpirtyDateTime", dtExpirtyDateTime, MyDbType.DateTime));
                    }
                    else
                    {
                        //sbQuery.Append("  @dtFixedDateTime  ");
                        dtExpirtyDateTime = BusinessUtility.GetDateTime(iDays.ToString("D2") + "/" + iMonth.ToString("D2") + "/" + iNewYear, DateFormat.ddMMyyyy);
                        //ErrorLog.createLog("2" + " " + BusinessUtility.GetString(dtExpirtyDateTime.ToString()));
                        pList.Add(DbUtility.GetParameter("dtExpirtyDateTime", dtExpirtyDateTime, MyDbType.DateTime));
                    }
                    sbQuery.Append(" recordExpiryDateTime =   DATE_ADD( DATE_ADD( date(  DATE_SUB(  @dtExpirtyDateTime, interval 1 DAY) ) , interval 23 hour), interval 59 minute) ");
                    sbQuery.Append("  where rs.registrationID = @regid; ");
                }
                else if (TrainingRepeatTriggerType.RelativeNoDaysFromCompleteDate == repeatType)
                {
                    sbQuery.Append("  update reportsummary rs set ");
                    sbQuery.Append("  recordExpiryDateTime =  DATE_ADD( DATE_ADD(date( DATE_ADD(rs.completeddatetime, INTERVAL " + repeatDays + " DAY) ), interval 00 hour), interval 00 minute)  ");
                    sbQuery.Append("  where rs.registrationID = @regid; ");
                }
                else if (TrainingRepeatTriggerType.RelativeNoDaysFromStartDate == repeatType)
                {
                    sbQuery.Append("  update reportsummary rs set ");
                    sbQuery.Append("  recordExpiryDateTime = DATE_ADD( DATE_ADD(date(  DATE_ADD(rs.createddatetime, INTERVAL " + repeatDays + " DAY) ), interval 00 hour), interval 00 minute)  ");
                    sbQuery.Append("  where rs.registrationID = @regid; ");
                }
                else if (TrainingRepeatTriggerType.RepeatFromEligibleDate == repeatType)
                {
                    //sbQuery.Append("  update reportsummary rs set ");
                    //sbQuery.Append("  recordExpiryDateTime =  DATE_ADD( DATE_ADD(date( DATE_ADD(empcourse.startdatetime, INTERVAL " + repeatDays + " DAY) ), interval 23 hour), interval 59 minute)  ");
                    //sbQuery.Append("  where rs.registrationID = @regid; ");
                    sbQuery.Append("  update  reportsummary rs  ");
                    sbQuery.Append("  join employeecourseregistration empreg on empreg.idRegistration = rs.registrationID  ");
                    sbQuery.Append("  join traningevent tevnt on tevnt.courseHdrID = empreg.courseHeader_courseHdrID and tevnt.isVersionActive = 1 ");
                    sbQuery.Append("  left join empassignedcourse empcourse on empcourse.courseEmpRecordID = empreg.empCourseAssignedID  ");
                    sbQuery.Append("  set rs.recordExpiryDateTime =  DATE_ADD( DATE_ADD(date( DATE_ADD(empcourse.startdatetime, INTERVAL " + repeatDays + " DAY) ), interval 00 hour), interval 00 minute)  ");
                    sbQuery.Append("  where rs.registrationID = @regid;  ");
                }
                //else
                //{
                //    sbQuery.Append("  CURRENT_TIMESTAMP() ");
                //}



                pList.Add(DbUtility.GetParameter("regid", regID, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("roleID", roleID, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("orderSeqInDisplay", displaySeq, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("courseTestRegID", objEmpCreg.courseTestRegID, MyDbType.Int));
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());
                dbTransactionHelper.CommitTransaction();
                rStatus = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }

            return rStatus;
        }

        /// <summary>
        /// To Check Is Event Requested To Repeate For Employee
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="eventID">Pass Event ID</param>
        /// <returns>True/False</returns>
        public Boolean IsRequestedToRepeat(int empID, int eventID)
        {
            bool bReturn = false;
            DbHelper dbHelp = new DbHelper(true);

            try
            {
                StringBuilder sbQuery = new StringBuilder();

                sbQuery = new StringBuilder();
                sbQuery.Append(" select count(*) from  empassignedcourse  where CourseEmpHdrID = @empID and CourseID = @eventID and startdatetime >=CURRENT_TIMESTAMP()  ");
                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empID", empID, MyDbType.Int),
                    DbUtility.GetParameter("eventID", eventID, MyDbType.Int)
                    });

                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }



        //public Boolean UpdateEmpAssignedCoursesRoleID()
        //{
        //    bool bReturn = false;
        //    DbHelper dbHelp = new DbHelper(true);

        //    try
        //    {
        //        StringBuilder sbQuery = new StringBuilder();
        //        StringBuilder sbQueryEmployeeCourse = new StringBuilder();

        //        sbQuery = new StringBuilder();
        //        sbQuery.Append("  select courseEmpRecordID, CourseEmpHdrID, CourseID, CourseVer from empassignedcourse where recordActive = 1 ");
        //        sbQuery.Append("   ");  // where CourseEmpHdrID = @empID and CourseID = @courseID and CourseVer = @courseVer and isRepeated = @isRepeated

        //        DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null);

        //        foreach (DataRow dRow in dt.Rows)
        //        {
        //            int empID = BusinessUtility.GetInt(dRow["CourseEmpHdrID"]);
        //            int courseID = BusinessUtility.GetInt(dRow["CourseID"]);

        //            int roleID = GetRoleIDToApply(empID, courseID);


        //            if (roleID > 0)
        //            {
        //                DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
        //                dbTransactionHelper.BeginTransaction();

        //                try
        //                {
        //                    DataTable dtRoleCourseDetail = GetRoleCourseDetail(BusinessUtility.GetInt(empID), BusinessUtility.GetInt(dRow["CourseID"]), roleID);



        //                    List<MySqlParameter> pList = new List<MySqlParameter>();
        //                    pList.Add(DbUtility.GetParameter("courseEmpHdrID", empID, MyDbType.Int));
        //                    pList.Add(DbUtility.GetParameter("courseID", BusinessUtility.GetInt(dRow["CourseID"]), MyDbType.Int));
        //                    pList.Add(DbUtility.GetParameter("courseVer", BusinessUtility.GetInt(dRow["CourseVer"]), MyDbType.Int));
        //                    pList.Add(DbUtility.GetParameter("recordActive", 1, MyDbType.Int));
        //                    pList.Add(DbUtility.GetParameter("roleID", roleID, MyDbType.Int));
        //                    pList.Add(DbUtility.GetParameter("courseEmpRecordID", BusinessUtility.GetInt(dRow["courseEmpRecordID"]), MyDbType.Int));
        //                    sbQuery = new StringBuilder();
        //                    sbQuery.Append("  update empassignedcourse set roleID = @roleID , orderSeqInDisplay = @orderSeqInDisplay  ");
        //                    sbQuery.Append("  where courseEmpHdrID = @courseEmpHdrID and courseID = @courseID and courseEmpRecordID = @courseEmpRecordID ");
        //                    if (dtRoleCourseDetail.Rows.Count == 0)
        //                    {
        //                        pList.Add(DbUtility.GetParameter("orderSeqInDisplay", 0, MyDbType.Int));
        //                        pList.Add(DbUtility.GetParameter("isRepeated", 0, MyDbType.Int));
        //                    }
        //                    else
        //                    {
        //                        if (BusinessUtility.GetString(dtRoleCourseDetail.Rows[0]["startdatetime"]) == "")
        //                        {
        //                            pList.Add(DbUtility.GetParameter("orderSeqInDisplay", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["orderSeqInDisplay"]), MyDbType.Int));
        //                            pList.Add(DbUtility.GetParameter("isRepeated", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["repeatRequired"]), MyDbType.Int));
        //                        }
        //                        else
        //                        {
        //                            pList.Add(DbUtility.GetParameter("orderSeqInDisplay", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["orderSeqInDisplay"]), MyDbType.Int));
        //                            pList.Add(DbUtility.GetParameter("isRepeated", BusinessUtility.GetInt(dtRoleCourseDetail.Rows[0]["repeatRequired"]), MyDbType.Int));
        //                        }
        //                    }

        //                    //ErrorLog.createLog(BusinessUtility.GetString(sbQuery));
        //                    //foreach (MySqlParameter prmlist in pList)
        //                    //{
        //                    //    ErrorLog.createLog(BusinessUtility.GetString(prmlist.DbType));
        //                    //    ErrorLog.createLog(BusinessUtility.GetString(prmlist.ParameterName));
        //                    //    ErrorLog.createLog(BusinessUtility.GetString(prmlist.Value));

        //                    //}
        //                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, pList.ToArray());

        //                    dbTransactionHelper.CommitTransaction();
        //                }
        //                catch (Exception exe)
        //                {
        //                    dbTransactionHelper.RollBackTransaction();
        //                }
        //                finally
        //                {
        //                    dbTransactionHelper.CloseDatabaseConnection();
        //                }
        //            }
        //            else
        //            {
        //                ErrorLog.createLog("Role ID Not Found EmpID " + empID + " course ID " + courseID + "");
        //            }
        //        }

        //        bReturn = true;
        //    }
        //    catch (Exception ex)
        //    {

        //        //ErrorLog.createLog("EmpID " + BusinessUtility.GetString(empID));
        //        ErrorLog.CreateLog(ex);
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }

        //    return true;
        //}


        public void ResetEmployeeAssignedCoursesByRoleThread()
        {
            Role objRole = new Role();
            Event objEvent = new Event();
            int empID = 0;

            if (this.RoleID > 0)
            {
                try
                {
                    StringBuilder sbMessage = new StringBuilder();
                    string sRoleName = objRole.GetRoleName(this.RoleID);

                    if (this.CourseID > 0)
                    {
                        objEvent.GetEventDetail(this.CourseID, "en");
                        string sEventName = BusinessUtility.GetString(objEvent.EventName);
                        sbMessage.Append("Your Request for Role Edit - " + sRoleName + " and Course - " + sEventName + "");
                    }
                    else
                    {
                        sbMessage.Append("Your Request for Role Edit - " + sRoleName);
                    }

                    sbMessage.Append("<br />Started At " + DateTime.Now + "");


                    ResetEmployeeAssignedCoursesByRole(this.RoleID, this.CourseID, this.InActiveReason, this.LastRecordUpdateSource);

                    objRole.SetRoleUpdateDateTime(this.RoleID, CurrentUserLogInID);
                    sbMessage.Append("<br />Finished At " + DateTime.Now + "");
                    string sDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    string EmailFrom = this.EmailFrom; // Utils.SupportEmailFrom
                    string EmailSubject = "Role Edit Request Status";
                    string[] EmailTo = this.EmailTo.Split(','); ;//Utils.SupportEmailTo.Split(',');

                    try
                    {
                        foreach (string sMailTo in EmailTo)
                        {
                            if (EmailHelper.SendEmail(EmailFrom, sMailTo, BusinessUtility.GetString(sbMessage), EmailSubject, true))
                            {
                                //  For Future Logging Use
                            }
                            else
                            {
                                ErrorLog.createLog("Mail Not Sent for Role " + this.RoleID + " to user " + sMailTo + "  mail content " + BusinessUtility.GetString(sbMessage));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.createLog("Mail Not Sent for Role " + this.RoleID + "");
                        ErrorLog.CreateLog(ex);
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.CreateLog(ex);
                }
                finally
                {
                    objRole.UnLockRole(this.RoleID);
                }
            }
        }


        public Boolean IsLastRequestedUsed(int empID, int eventID)
        {
            bool bReturn = false;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery = new StringBuilder();
                sbQuery.Append(" select count(*) from  empassignedcourse  where CourseEmpHdrID = @empID and CourseID = @eventID and isUsed = 1 order by courseEmpRecordID Desc limit 1  ");
                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empID", empID, MyDbType.Int),
                    DbUtility.GetParameter("eventID", eventID, MyDbType.Int)
                    });

                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }


        public Boolean IsAnyRequested(int empID, int eventID)
        {
            bool bReturn = false;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery = new StringBuilder();
                sbQuery.Append(" select count(*) from  empassignedcourse  where CourseEmpHdrID = @empID and CourseID = @eventID order by courseEmpRecordID Desc limit 1  ");
                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empID", empID, MyDbType.Int),
                    DbUtility.GetParameter("eventID", eventID, MyDbType.Int)
                    });

                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bReturn;
        }



        public void EmployeeAssignedCoursesByThread()
        {
            Role objRole = new Role();
            Event objEvent = new Event();
            int empID = 0;

            if (this.EmployeeID > 0)
            {
                try
                {
                    StringBuilder sbMessage = new StringBuilder();
                    Employee objemp = new Employee();
                    objemp.GetEmployeeDetail(this.EmployeeID);

                    if (this.CourseID > 0)
                    {
                        objEvent.GetEventDetail(this.CourseID, "en");
                        string sEventName = BusinessUtility.GetString(objEvent.EventName);
                        sbMessage.Append("Employee  " + objemp.EmpName + " assigned Course - " + sEventName + "");
                    }
                    else
                    {
                        sbMessage.Append("Employee  " + objemp.EmpName);
                    }
                    sbMessage.Append("<br />Started At " + DateTime.Now + "");

                    try
                    {
                        ResetEmployeeAssignedCoursesByEmpID(this.EmployeeID, this.CourseID, this.InActiveReason, this.LastRecordUpdateSource);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.createLog("Issue on Assigned Course");
                    }


                    //ErrorLog.createLog(" Going To sent Mail  ");

                    sbMessage.Append("<br />Finished At " + DateTime.Now + "");
                    string sDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    string EmailFrom = this.EmailFrom;
                    string EmailSubject = "Role Edit Request Status";
                    string[] EmailTo = this.EmailTo.Split(',');
                    try
                    {
                        foreach (string sMailTo in EmailTo)
                        {
                            //ErrorLog.createLog(" Going To sent Mail  " + sMailTo);
                            if (EmailHelper.SendEmail(EmailFrom, sMailTo, BusinessUtility.GetString(sbMessage), EmailSubject, true))
                            {
                                //  For Future Logging Use
                            }
                            else
                            {
                                ErrorLog.createLog("Mail Not Sent for Emp " + this.EmployeeID + " to user " + sMailTo + "  mail content " + BusinessUtility.GetString(sbMessage));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.createLog("Mail Not Sent for Emp " + this.EmployeeID + "");
                        ErrorLog.CreateLog(ex);
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.CreateLog(ex);
                }
                finally
                {
                    objRole.UnLockRole(this.RoleID);
                }
            }
        }


        public bool ResetEmployeeAssignedCoursesByEmpID(int empID, int courseID, int inActiveReason, int lastRecordUpdateSource)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbQueryEmployeeCourse = new StringBuilder();
                sbQueryEmployeeCourse.Append(" set @empHdrID = " + empID + "; ");
                sbQueryEmployeeCourse.Append(" set @courseID = " + courseID + "; ");
                sbQueryEmployeeCourse.Append(" set @rStatus = 0; ");
                sbQueryEmployeeCourse.Append(" set @inActiveReason = " + inActiveReason + "; ");
                sbQueryEmployeeCourse.Append(" set @lastRecordUpdateSource = " + lastRecordUpdateSource + "; ");
                sbQueryEmployeeCourse.Append(" call sp_Reset_EmpAssignedCoursesByID(@empHdrID, @courseID, @inActiveReason, @lastRecordUpdateSource, @rStatus ); ");
                DataTable dtEmployeeCourse = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sbQueryEmployeeCourse), CommandType.Text, null
                        );
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.createLog("EmpID " + BusinessUtility.GetString(empID));
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }


        public bool ResetEmployeeAssignedCoursesByRole(int roleID, int courseID, int inActiveReason, int lastRecordUpdateSource)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbQueryEmployeeCourse = new StringBuilder();
                sbQueryEmployeeCourse.Append(" set @roleID = " + roleID + "; ");
                sbQueryEmployeeCourse.Append(" set @courseID = " + courseID + "; ");
                sbQueryEmployeeCourse.Append(" set @inActiveReason = " + inActiveReason + "; ");
                sbQueryEmployeeCourse.Append(" set @lastRecordUpdateSource = " + lastRecordUpdateSource + "; ");
                sbQueryEmployeeCourse.Append(" call sp_Reset_EmpAssignedCoursesByRole (@roleID, @courseID, @inActiveReason, @lastRecordUpdateSource); ");

                DataTable dtEmployeeCourse = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sbQueryEmployeeCourse), CommandType.Text, null
                        );
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.createLog("Add Role ID " + BusinessUtility.GetString(roleID));
                ErrorLog.createLog("Add Course ID " + BusinessUtility.GetString(courseID));
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }


        //public void RemoveEmployeeAssignedCoursesByRoleThread()
        //{
        //    Role objRole = new Role();
        //    Event objEvent = new Event();
        //    int empID = 0;

        //    if (this.RoleID > 0)
        //    {
        //        try
        //        {
        //            StringBuilder sbMessage = new StringBuilder();
        //            string sRoleName = objRole.GetRoleName(this.RoleID);

        //            if (this.CourseID > 0)
        //            {
        //                objEvent.GetEventDetail(this.CourseID, "en");
        //                string sEventName = BusinessUtility.GetString(objEvent.EventName);
        //                sbMessage.Append("Your Request for Role Edit - " + sRoleName + " and Course - " + sEventName + "");
        //            }
        //            else
        //            {
        //                sbMessage.Append("Your Request for Role Edit - " + sRoleName);
        //            }

        //            sbMessage.Append("<br />Started At " + DateTime.Now + "");


        //            RemoveEmployeeAssignedCoursesByRole(this.RoleID, this.CourseID);

        //            objRole.SetRoleUpdateDateTime(this.RoleID, CurrentUserLogInID);
        //            sbMessage.Append("<br />Finished At " + DateTime.Now + "");
        //            string sDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //            string EmailFrom = this.EmailFrom; // Utils.SupportEmailFrom
        //            string EmailSubject = "Role Edit Request Status";
        //            string[] EmailTo = this.EmailTo.Split(','); ;//Utils.SupportEmailTo.Split(',');

        //            try
        //            {
        //                foreach (string sMailTo in EmailTo)
        //                {
        //                    if (EmailHelper.SendEmail(EmailFrom, sMailTo, BusinessUtility.GetString(sbMessage), EmailSubject, true))
        //                    {
        //                        //  For Future Logging Use
        //                    }
        //                    else
        //                    {
        //                        ErrorLog.createLog("Mail Not Sent for Role " + this.RoleID + " to user " + sMailTo + "  mail content " + BusinessUtility.GetString(sbMessage));
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                ErrorLog.createLog("Mail Not Sent for Role " + this.RoleID + "");
        //                ErrorLog.CreateLog(ex);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorLog.CreateLog(ex);
        //        }
        //        finally
        //        {
        //            objRole.UnLockRole(this.RoleID);
        //        }
        //    }
        //}

        //public bool RemoveEmployeeAssignedCoursesByRole(int roleID, int courseID)
        //{
        //    bool bReturn = false;
        //    DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
        //    dbTransactionHelper.BeginTransaction();
        //    try
        //    {
        //        StringBuilder sbQueryEmployeeCourse = new StringBuilder();
        //        sbQueryEmployeeCourse.Append(" set @roleID = " + roleID + "; ");
        //        sbQueryEmployeeCourse.Append(" set @courseID = " + courseID + "; ");
        //        sbQueryEmployeeCourse.Append(" call sp_Reset_EmpAssignedCoursesByRoleRemove (@roleID, @courseID); ");
        //        DataTable dtEmployeeCourse = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sbQueryEmployeeCourse), CommandType.Text, null
        //                );
        //        dbTransactionHelper.CommitTransaction();
        //        bReturn = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        dbTransactionHelper.RollBackTransaction();
        //        ErrorLog.createLog("Remove Role ID " + BusinessUtility.GetString(roleID));
        //        ErrorLog.createLog("Remove Course ID " + BusinessUtility.GetString(courseID));
        //        ErrorLog.CreateLog(ex);
        //    }
        //    finally
        //    {
        //        dbTransactionHelper.CloseDatabaseConnection();
        //    }
        //    return bReturn;
        //}
    }
}
