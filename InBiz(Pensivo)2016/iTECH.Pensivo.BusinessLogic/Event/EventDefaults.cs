﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Event Default Functionality
    /// </summary>
    public class EventDefaults
    {
        /// <summary>
        /// To Get/Set Training Event ID
        /// </summary>
        public Int64 TrainingEventID { get; set; }

        /// <summary>
        /// To Get/Set Training Event Ver
        /// </summary>
        public int TrainingEventVer { get; set; }

        /// <summary>
        /// To Get/Set Training Reporting Keyword
        /// </summary>
        public string ReportingKeywords { get; set; }

        /// <summary>
        /// To Get/Set Minimum Pass Score
        /// </summary>
        public int MinimumPassScore { get; set; }

        /// <summary>
        /// To Get/Set Maximum Pass Score
        /// </summary>
        public int MaximumPassScore { get; set; }

        /// <summary>
        /// To Get/Set Attempt Reset Type
        /// </summary>
        public string AttemptResetType { get; set; }

        /// <summary>
        /// To Get/Set Attempt Grant Waiting Time
        /// </summary>
        public int AttemptGrantWaitingTime { get; set; }

        /// <summary>
        /// To Get/Set Order Seq In Display
        /// </summary>
        public int OrderSeqInDisplay { get; set; }

        /// <summary>
        /// To Get/Set User Display Priority
        /// </summary>
        public int UserDisplayPriority { get; set; }

        /// <summary>
        /// To Get/Set Repeat Required
        /// </summary>
        public bool RepeatRequired { get; set; }

        /// <summary>
        /// To Get/Set Repeat In Days
        /// </summary>
        public int RepeatInDays { get; set; }

        /// <summary>
        /// To Get/Set Repeat Trigger Type
        /// </summary>
        public string RepeatTriggerType { get; set; }

        /// <summary>
        /// To Get/Set Alert Required
        /// </summary>
        public bool AlertRequired { get; set; }

        /// <summary>
        /// To Get/Set Alert Type
        /// </summary>
        public string AlertType { get; set; }





        /// <summary>
        /// To Get/Set Alert In Days
        /// </summary>
        public int AlertInDays { get; set; }

        /// <summary>
        /// To Get/Set Alert Before Launch
        /// </summary>
        public bool AlertBeforeLaunch { get; set; }

        /// <summary>
        /// To Get/Set Alert Not Completed
        /// </summary>
        public bool AlertNotCompleted { get; set; }

        /// <summary>
        /// To Get/Set Alert Manager Not Completed
        /// </summary>
        public bool AlertManagerNotCompleted { get; set; }

        /// <summary>
        /// To Get/Set Alert Repeat In Days
        /// </summary>
        public int AlertRepeatInDays { get; set; }


        /// <summary>
        /// To Get/Set Alert Format
        /// </summary>
        public string AlertFormat { get; set; }

        /// <summary>
        /// To Get/Set Alert Portal Message
        /// </summary>
        public string AlertPortalMsg { get; set; }

        /// <summary>
        /// To Get/Set Alert Email Message
        /// </summary>
        public string AlertEmailMsg { get; set; }

        /// <summary>
        /// To Get/Set Alert Escalation Email Message
        /// </summary>
        public string AlertEscalationEmailMsg { get; set; }

        /// <summary>
        /// To Get/Set Alert Email Subject
        /// </summary>
        public string AlertEmailSubject { get; set; }

        /// <summary>
        /// To Get/Set Alert Escalation Email Subject
        /// </summary>
        public string AlertEscalationEmailSubject { get; set; }


        /// <summary>
        /// To Get/Set Repeat Fixed Date Time
        /// </summary>
        public DateTime RepeatFixedDateTime { get; set; }

        /// <summary>
        /// To Get/Set Course Start DateTime
        /// </summary>
        public DateTime CourseStartDateTime { get; set; }

        /// <summary>
        /// To Get/Set Fixed Day
        /// </summary>
        public int FixedDay { get; set; }

        /// <summary>
        /// To Get/Set Fixed Month
        /// </summary>
        public int FixedMonth { get; set; }

        /// <summary>
        /// To Save/Update Training Event Default 
        /// </summary>
        /// <returns>True/False</returns>
        public Boolean SaveUpdateTrainingEventDefault()
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                StringBuilder sbQuery = new StringBuilder();
                sbInsertQuery = new StringBuilder();

                sbInsertQuery.Append(" SELECT trainingEventID FROM trainingeventdefaults where trainingEventID = @EventID and trainingEventVer = @trainingEventVer and isVersionActive = 1 ");
                object rValue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("EventID", this.TrainingEventID, MyDbType.Int),
                DbUtility.GetParameter("@trainingEventVer", this.TrainingEventVer , MyDbType.Int),
                });

                if (BusinessUtility.GetInt(rValue) <= 0)
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" insert into trainingeventdefaults ");
                    sbInsertQuery.Append(" ( ");
                    sbInsertQuery.Append(" trainingEventID, trainingEventVer, reportingKeywords, minimumPassScore, maximumPassScore, attemptResetType, attemptGrantWaitingTime, orderSeqInDisplay, userDisplayPriority,  ");
                    sbInsertQuery.Append(" repeatRequired, repeatInDays, repeatTriggerType, alertRequired, alertType, alertBeforeLaunch, alertNotCompleted, alertManagerNotCompleted, alertRepeatInDays, alertInDays, alertFormat, alertPortalMsg, alertEmailMsg, alertEscalationEmailMsg,  ");
                    sbInsertQuery.Append(" alertEmailSubject, alertEscalationEmailSubject, repeatFixedDate, fixedDay, fixedMonth, startdatetime, isVersionActive  ");
                    sbInsertQuery.Append(" ) ");
                    sbInsertQuery.Append(" values ");
                    sbInsertQuery.Append(" ( ");
                    sbInsertQuery.Append(" @trainingEventID, @trainingEventVer, @reportingKeywords, @minimumPassScore, @maximumPassScore, @attemptResetType, @attemptGrantWaitingTime, @orderSeqInDisplay, @userDisplayPriority,  ");
                    sbInsertQuery.Append(" @repeatRequired, @repeatInDays, @repeatTriggerType, @alertRequired, @alertType, @alertBeforeLaunch, @alertNotCompleted, @alertManagerNotCompleted, @alertRepeatInDays, @alertInDays, @alertFormat, @alertPortalMsg, @alertEmailMsg, @alertEscalationEmailMsg,  ");
                    sbInsertQuery.Append(" @alertEmailSubject, @alertEscalationEmailSubject, @repeatFixedDate, @fixedDay, @fixedMonth, @startdatetime, 1  ");
                    sbInsertQuery.Append(" ) ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("@trainingEventID", this.TrainingEventID , MyDbType.Int),
                    DbUtility.GetParameter("@trainingEventVer", this.TrainingEventVer , MyDbType.Int),
                    DbUtility.GetParameter("@reportingKeywords", this.ReportingKeywords , MyDbType.String),
                    DbUtility.GetParameter("@minimumPassScore", this.MinimumPassScore , MyDbType.Int),
                    DbUtility.GetParameter("@maximumPassScore", this.MaximumPassScore , MyDbType.Int),
                    DbUtility.GetParameter("@attemptResetType", this.AttemptResetType , MyDbType.String),
                    DbUtility.GetParameter("@attemptGrantWaitingTime", this.AttemptGrantWaitingTime , MyDbType.Int),
                    DbUtility.GetParameter("@orderSeqInDisplay", this.OrderSeqInDisplay , MyDbType.Int),
                    DbUtility.GetParameter("@userDisplayPriority", this.UserDisplayPriority , MyDbType.Int),
                    DbUtility.GetParameter("@repeatRequired", this.RepeatRequired , MyDbType.Boolean),
                    DbUtility.GetParameter("@repeatInDays", this.RepeatInDays , MyDbType.Int),
                    DbUtility.GetParameter("@repeatTriggerType", this.RepeatTriggerType , MyDbType.String),
                    DbUtility.GetParameter("@alertRequired", this.AlertRequired , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertType", this.AlertType , MyDbType.String),
                    DbUtility.GetParameter("@alertBeforeLaunch", this.AlertBeforeLaunch , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertNotCompleted", this.AlertNotCompleted , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertManagerNotCompleted", this.AlertManagerNotCompleted , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertRepeatInDays", this.AlertRepeatInDays , MyDbType.Int),
                    DbUtility.GetParameter("@alertInDays", this.AlertInDays , MyDbType.Int),
                    DbUtility.GetParameter("@alertFormat", this.AlertFormat , MyDbType.String),
                    DbUtility.GetParameter("@alertPortalMsg", this.AlertPortalMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEmailMsg", this.AlertEmailMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEscalationEmailMsg", this.AlertEscalationEmailMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEmailSubject", this.AlertEmailSubject , MyDbType.String),
                    DbUtility.GetParameter("@alertEscalationEmailSubject", this.AlertEscalationEmailSubject , MyDbType.String),
                    DbUtility.GetParameter("@repeatFixedDate", this.RepeatFixedDateTime , MyDbType.DateTime),
                    DbUtility.GetParameter("@fixedDay", this.FixedDay , MyDbType.Int),
                    DbUtility.GetParameter("@fixedMonth", this.FixedMonth , MyDbType.Int),
                    DbUtility.GetParameter("@startdatetime", this.CourseStartDateTime , MyDbType.DateTime),
                    });
                }
                else
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" update trainingeventdefaults set  ");
                    sbInsertQuery.Append(" trainingEventVer = @trainingEventVer, reportingKeywords = @reportingKeywords, minimumPassScore = @minimumPassScore, maximumPassScore = @maximumPassScore,   ");
                    sbInsertQuery.Append(" attemptResetType = @attemptResetType, attemptGrantWaitingTime = @attemptGrantWaitingTime, orderSeqInDisplay = @orderSeqInDisplay,   ");
                    sbInsertQuery.Append(" userDisplayPriority = @userDisplayPriority, repeatRequired = @repeatRequired,  repeatInDays = @repeatInDays, repeatTriggerType = @repeatTriggerType,  ");
                    sbInsertQuery.Append(" alertRequired = @alertRequired, alertType = @alertType, alertBeforeLaunch = @alertBeforeLaunch, alertNotCompleted = @alertNotCompleted, alertManagerNotCompleted = @alertManagerNotCompleted, alertRepeatInDays = @alertRepeatInDays,   ");
                    sbInsertQuery.Append(" alertInDays = @alertInDays, alertFormat = @alertFormat, alertPortalMsg = @alertPortalMsg, alertEmailMsg = @alertEmailMsg, ");
                    sbInsertQuery.Append(" alertEscalationEmailMsg = @alertEscalationEmailMsg, alertEmailSubject =@alertEmailSubject, alertEscalationEmailSubject = @alertEscalationEmailSubject, repeatFixedDate = @repeatFixedDate,  ");
                    sbInsertQuery.Append(" fixedDay = @fixedDay, fixedMonth = @fixedMonth, startdatetime = @startdatetime ");
                    sbInsertQuery.Append(" where trainingEventID = @trainingEventID and trainingEventVer = @trainingEventVer and isVersionActive = 1  ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("@trainingEventID", this.TrainingEventID , MyDbType.Int),
                    DbUtility.GetParameter("@trainingEventVer", this.TrainingEventVer , MyDbType.Int),
                    DbUtility.GetParameter("@reportingKeywords", this.ReportingKeywords , MyDbType.String),
                    DbUtility.GetParameter("@minimumPassScore", this.MinimumPassScore , MyDbType.Int),
                    DbUtility.GetParameter("@maximumPassScore", this.MaximumPassScore , MyDbType.Int),
                    DbUtility.GetParameter("@attemptResetType", this.AttemptResetType , MyDbType.String),
                    DbUtility.GetParameter("@attemptGrantWaitingTime", this.AttemptGrantWaitingTime , MyDbType.Int),
                    DbUtility.GetParameter("@orderSeqInDisplay", this.OrderSeqInDisplay , MyDbType.Int),
                    DbUtility.GetParameter("@userDisplayPriority", this.UserDisplayPriority , MyDbType.Int),
                    DbUtility.GetParameter("@repeatRequired", this.RepeatRequired , MyDbType.Boolean),
                    DbUtility.GetParameter("@repeatInDays", this.RepeatInDays , MyDbType.Int),
                    DbUtility.GetParameter("@repeatTriggerType", this.RepeatTriggerType , MyDbType.String),
                    DbUtility.GetParameter("@alertRequired", this.AlertRequired , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertType", this.AlertType , MyDbType.String),
                    DbUtility.GetParameter("@alertBeforeLaunch", this.AlertBeforeLaunch , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertNotCompleted", this.AlertNotCompleted , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertManagerNotCompleted", this.AlertManagerNotCompleted , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertRepeatInDays", this.AlertRepeatInDays , MyDbType.Int),
                    DbUtility.GetParameter("@alertInDays", this.AlertInDays , MyDbType.Int),
                    DbUtility.GetParameter("@alertFormat", this.AlertFormat , MyDbType.String),
                    DbUtility.GetParameter("@alertPortalMsg", this.AlertPortalMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEmailMsg", this.AlertEmailMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEscalationEmailMsg", this.AlertEscalationEmailMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEmailSubject", this.AlertEmailSubject , MyDbType.String),
                    DbUtility.GetParameter("@alertEscalationEmailSubject", this.AlertEscalationEmailSubject , MyDbType.String),
                    DbUtility.GetParameter("@repeatFixedDate", this.RepeatFixedDateTime , MyDbType.DateTime),
                    DbUtility.GetParameter("@fixedDay", this.FixedDay , MyDbType.Int),
                    DbUtility.GetParameter("@fixedMonth", this.FixedMonth , MyDbType.Int),
                    DbUtility.GetParameter("@startdatetime", this.CourseStartDateTime , MyDbType.DateTime),
                    });
                }
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Get Training Event Default
        /// </summary>
        /// <param name="eventID">Pass Event ID</param>
        /// <param name="lang">Pass Language</param>
        public void GetTrainingEventDefault(int eventID, string lang)
        {
            StringBuilder sbQuery = new StringBuilder();

            sbQuery.Append(" select ");
            sbQuery.Append(" trainingEventID, trainingEventVer, reportingKeywords, minimumPassScore, maximumPassScore, attemptResetType, attemptGrantWaitingTime, orderSeqInDisplay, ");
            sbQuery.Append(" userDisplayPriority, repeatRequired, repeatInDays, repeatTriggerType, alertRequired, alertType, alertBeforeLaunch, alertNotCompleted, alertManagerNotCompleted,  ");
            sbQuery.Append(" alertRepeatInDays, alertInDays, alertFormat, alertPortalMsg, alertEmailMsg, alertEscalationEmailMsg, alertEmailSubject, alertEscalationEmailSubject, repeatFixedDate, ");
            sbQuery.Append(" fixedDay, fixedMonth, startdatetime ");
            sbQuery.Append(" from ");
            sbQuery.Append(" trainingeventdefaults");
            sbQuery.Append(" where trainingEventID = @trainingEventID and isVersionActive = 1 order by trainingEventVer desc  limit 1 ");

            MySqlParameter[] p = { new MySqlParameter("@trainingEventID", eventID) };
            MySqlDataReader objDr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                objDr = dbHelp.GetDataReader(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, p);
                while (objDr.Read())
                {

                    this.TrainingEventID = BusinessUtility.GetInt(objDr["trainingEventID"]);
                    this.TrainingEventVer = BusinessUtility.GetInt(objDr["trainingEventVer"]);
                    this.ReportingKeywords = BusinessUtility.GetString(objDr["reportingKeywords"]);
                    this.MinimumPassScore = BusinessUtility.GetInt(objDr["minimumPassScore"]);
                    this.MaximumPassScore = BusinessUtility.GetInt(objDr["maximumPassScore"]);
                    this.AttemptResetType = BusinessUtility.GetString(objDr["attemptResetType"]);
                    this.AttemptGrantWaitingTime = BusinessUtility.GetInt(objDr["attemptGrantWaitingTime"]);
                    this.OrderSeqInDisplay = BusinessUtility.GetInt(objDr["orderSeqInDisplay"]);
                    this.UserDisplayPriority = BusinessUtility.GetInt(objDr["userDisplayPriority"]);
                    this.RepeatRequired = BusinessUtility.GetBool(objDr["repeatRequired"]);
                    this.RepeatInDays = BusinessUtility.GetInt(objDr["repeatInDays"]);
                    this.RepeatTriggerType = BusinessUtility.GetString(objDr["repeatTriggerType"]);
                    this.AlertRequired = BusinessUtility.GetBool(objDr["alertRequired"]);
                    this.AlertType = BusinessUtility.GetString(objDr["alertType"]);
                    this.AlertBeforeLaunch = BusinessUtility.GetBool(objDr["alertBeforeLaunch"]);
                    this.AlertNotCompleted = BusinessUtility.GetBool(objDr["alertNotCompleted"]);
                    this.AlertManagerNotCompleted = BusinessUtility.GetBool(objDr["alertManagerNotCompleted"]);
                    this.AlertRepeatInDays = BusinessUtility.GetInt(objDr["alertRepeatInDays"]);
                    this.AlertInDays = BusinessUtility.GetInt(objDr["alertInDays"]);
                    this.AlertFormat = BusinessUtility.GetString(objDr["alertFormat"]);
                    this.AlertPortalMsg = BusinessUtility.GetString(objDr["alertPortalMsg"]);
                    this.AlertEmailMsg = BusinessUtility.GetString(objDr["alertEmailMsg"]);
                    this.AlertEscalationEmailMsg = BusinessUtility.GetString(objDr["alertEscalationEmailMsg"]);
                    this.AlertEmailSubject = BusinessUtility.GetString(objDr["alertEmailSubject"]);
                    this.AlertEscalationEmailSubject = BusinessUtility.GetString(objDr["alertEscalationEmailSubject"]);
                    this.RepeatFixedDateTime = BusinessUtility.GetDateTime(objDr["repeatFixedDate"]);
                    this.FixedDay = BusinessUtility.GetInt(objDr["fixedDay"]);
                    this.FixedMonth = BusinessUtility.GetInt(objDr["fixedMonth"]);
                    this.CourseStartDateTime = BusinessUtility.GetDateTime(objDr["startdatetime"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
