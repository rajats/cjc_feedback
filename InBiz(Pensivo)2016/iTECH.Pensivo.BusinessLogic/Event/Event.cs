﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Course Functionality Like Add,Delete, Update etc.  
    /// </summary>
    public class Event
    {
        /// <summary>
        /// To Get/Set Event Name
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// To Get/Set Course ID 
        /// </summary>
        public int CourseID { get; set; }

        /// <summary>
        /// To Get/Set Course Version No
        /// </summary>
        public int CourseVerNo { get; set; }

        /// <summary>
        /// To Get/Set Course Title English
        /// </summary>
        public string CourseTitleEn { get; set; }

        /// <summary>
        /// To Get/Set Course Title French
        /// </summary>
        public string CourseTitleFr { get; set; }

        /// <summary>
        /// To Get/Set Course Active
        /// </summary>
        public bool CourseActive { get; set; }

        /// <summary>
        /// To Get Set Course External ID
        /// </summary>
        public string CourseExtID { get; set; }

        /// <summary>
        /// To Get Set Course ExtID French
        /// </summary>
        public string CourseExtIDFr { get; set; }

        /// <summary>
        /// To Get/Set Course External Link
        /// </summary>
        public string CourseExtLink { get; set; }

        /// <summary>
        /// To Get/Set Course Question No
        /// </summary>
        public int CourseQueNo { get; set; }

        /// <summary>
        /// To Get/Set Course Test Question No
        /// </summary>
        public int CourseTestQueNo { get; set; }

        /// <summary>
        /// To Get/Set Course Certificate offered
        /// </summary>
        public bool CourseCertOffered { get; set; }


        /// <summary>
        /// To Get/Set Is Review Content
        /// </summary>
        public bool IsReviewContent { get; set; }

        /// <summary>
        /// To Get/Set Course Type
        /// </summary>
        public string CourseType { get; set; }

        /// <summary>
        /// To Get/Set Course Passing Grade
        /// </summary>
        public int CoursePassingGrade { get; set; }

        /// <summary>
        /// To Get/Set Course No of Attempts
        /// </summary>
        public int Course_no_of_attempts { get; set; }

        /// <summary>
        /// To Get/Set Course Version Type
        /// </summary>
        public string Course_version_typ { get; set; }

        /// <summary>
        /// To Get/Set Is New Course
        /// </summary>
        public bool Is_new { get; set; }

        /// <summary>
        /// To Get/Set Is Updated Course
        /// </summary>
        public bool Is_updated { get; set; }

        /// <summary>
        /// To Get/Set SCORM Type
        /// </summary>
        public string Scorm_type { get; set; }

        /// <summary>
        /// To Get/Set Total No of Slide
        /// </summary>
        public int Total_no_slides { get; set; }

        /// <summary>
        /// To Get/Set Event Category
        /// </summary>
        public string Trn_Catg { get; set; }

        /// <summary>
        /// To Get/Set Event Summary
        /// </summary>
        public string Trn_Summary { get; set; }

        /// <summary>
        /// To Get/Set Event Preview
        /// </summary>
        public string Trn_Preview { get; set; }

        /// <summary>
        /// To Get/Set Event Sample Test
        /// </summary>
        public string Trn_Sampletest { get; set; }

        /// <summary>
        /// To Get/Set Event Articles
        /// </summary>
        public string Trn_Articles { get; set; }

        /// <summary>
        /// To Get/Set Event File 1
        /// </summary>
        public string Trn_File_nm1 { get; set; }

        /// <summary>
        /// To Get/Set Event File 2
        /// </summary>
        public string Trn_File_nm2 { get; set; }

        /// <summary>
        /// To Get/Set Event File 3
        /// </summary>
        public string Trn_File_nm3 { get; set; }

        /// <summary>
        /// To Get/Set Event File 4
        /// </summary>
        public string Trn_File_nm4 { get; set; }

        /// <summary>
        /// To Get/Set Event Video Link
        /// </summary>
        public string Trn_Video_link { get; set; }

        /// <summary>
        /// To Get/Set Event Test Type
        /// </summary>
        public string TestType { get; set; }

        /// <summary>
        /// To Get/Set Test External ID
        /// </summary>
        public string TestExtID { get; set; }

        /// <summary>
        /// To Get/Set Certificate ID
        /// </summary>
        public int CertificateID { get; set; }

        /// <summary>
        /// To Get/Set Test Completed Count
        /// </summary>
        public int TestCompletedCount { get; set; }

        /// <summary>
        /// To Get Course/Training Event Detail From trainingevent Table
        /// </summary>
        /// <param name="eventID">Pass Training Event ID</param>
        /// <param name="lang">Pass Language Code</param>
        public void GetEventDetail(int eventID, string lang)
        {
            //SELECT if (max(trainingEventVer) is null , 0 , max(trainingEventVer)  ) FROM trainingeventversion where trainingeventID = @eventID
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT ");
            //sbQuery.Append(string.Format(" courseHdr.courseHdrID, courseHdr.courseVerNo, courseHdr.courseTitle{0} AS CourseTitle, courseHdr.courseTitleEn, courseHdr.courseTitleFr, courseHdr.courseActive, courseHdr.courseExtID, courseHdr.courseExtIDFr, courseHdr.courseExtLink, ", lang));
            sbQuery.Append(string.Format(" courseHdr.courseHdrID, courseHdr.courseVerNo, courseHdr.courseTitle{0} AS CourseTitle, courseHdr.courseTitleEn, courseHdr.courseTitleFr, courseHdr.courseActive, courseHdr.courseExtID, courseHdr.courseExtIDFr, courseHdr.courseExtLink, ", lang));
            sbQuery.Append(" courseHdr.courseCreatedOn, courseHdr.courseCreatedBy, courseHdr.courseLastModOn, courseHdr.courseLastModBy, ");
            sbQuery.Append(" courseHdr.courseQueNo, courseHdr.courseTestQueNo, courseHdr.courseCertOffered, courseHdr.isReviewContent, courseHdr.courseType, teventdefault.minimumPassScore coursePassingGrade, Course_no_of_attempts,  Course_version_typ,  courseHdr.is_new AS isNew, ");
            sbQuery.Append(" courseHdr.is_Updated AS isUpdated, Scorm_type, Total_no_slides,   trnDtl.trn_catg, trnDtl.trn_summary, trnDtl.trn_preview, trnDtl.trn_sampletest, trnDtl.trn_articles, trnDtl.Trn_File_nm1,  trnDtl.Trn_File_nm2,  trnDtl.Trn_File_nm3,  trnDtl.Trn_File_nm4,  trnDtl.Trn_Video_link,");
            sbQuery.Append(string.Format(" courstyp.type{0} AS course_type, TestType, TestExtID, certificateID, testCompletedCount  ", lang));
            sbQuery.Append(" FROM traningevent AS courseHdr ");
            sbQuery.Append(" INNER JOIN coursetype AS courstyp ON  courstyp.Type = courseHdr.courseType ");
            sbQuery.Append(" LEFT JOIN trainingeventdetail trnDtl ON trnDtl.trn_course_hdr_id = courseHdr.courseHdrID ");
            sbQuery.Append("  LEFT JOIN trainingeventdefaults AS teventdefault ON teventdefault.trainingEventID = courseHdr.courseHdrID and teventdefault.isVersionActive = 1 ");
            sbQuery.Append(" WHERE courseHdr.courseHdrID = @eventID and courseHdr.isVersionActive = 1 ");

            MySqlParameter[] p = { new MySqlParameter("@eventID", eventID) };
            MySqlDataReader objDr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                objDr = dbHelp.GetDataReader(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, p);
                while (objDr.Read())
                {
                    this.EventName = BusinessUtility.GetString(objDr["CourseTitle"]);
                    this.CourseVerNo = BusinessUtility.GetInt(objDr["courseVerNo"]);
                    this.CourseTitleEn = BusinessUtility.GetString(objDr["courseTitleEn"]);
                    this.CourseTitleFr = BusinessUtility.GetString(objDr["courseTitleFr"]);
                    this.CourseActive = BusinessUtility.GetBool(objDr["courseActive"]);
                    this.CourseExtID = BusinessUtility.GetString(objDr["courseExtID"]);
                    this.CourseExtIDFr = BusinessUtility.GetString(objDr["courseExtIDFr"]);
                    this.CourseExtLink = BusinessUtility.GetString(objDr["courseExtLink"]);
                    this.CourseQueNo = BusinessUtility.GetInt(objDr["courseQueNo"]);
                    this.CourseTestQueNo = BusinessUtility.GetInt(objDr["courseTestQueNo"]);
                    this.CourseCertOffered = BusinessUtility.GetBool(objDr["courseCertOffered"]);
                    this.IsReviewContent = BusinessUtility.GetBool(objDr["isReviewContent"]);
                    this.CourseType = BusinessUtility.GetString(objDr["courseType"]);
                    this.CoursePassingGrade = BusinessUtility.GetInt(objDr["coursePassingGrade"]);
                    this.Course_no_of_attempts = BusinessUtility.GetInt(objDr["Course_no_of_attempts"]);
                    this.Course_version_typ = BusinessUtility.GetString(objDr["Course_version_typ"]);
                    this.Is_new = BusinessUtility.GetBool(objDr["isNew"]);
                    this.Is_updated = BusinessUtility.GetBool(objDr["isUpdated"]);
                    this.Scorm_type = BusinessUtility.GetString(objDr["Scorm_type"]);
                    this.Total_no_slides = BusinessUtility.GetInt(objDr["Total_no_slides"]);
                    this.Trn_Catg = BusinessUtility.GetString(objDr["trn_catg"]);
                    this.Trn_Summary = BusinessUtility.GetString(objDr["trn_summary"]);
                    this.Trn_Preview = BusinessUtility.GetString(objDr["trn_preview"]);
                    this.Trn_Sampletest = BusinessUtility.GetString(objDr["trn_sampletest"]);
                    this.Trn_Articles = BusinessUtility.GetString(objDr["trn_articles"]);
                    this.Trn_File_nm1 = BusinessUtility.GetString(objDr["Trn_File_nm1"]);
                    this.Trn_File_nm2 = BusinessUtility.GetString(objDr["Trn_File_nm2"]);
                    this.Trn_File_nm3 = BusinessUtility.GetString(objDr["Trn_File_nm3"]);
                    this.Trn_File_nm4 = BusinessUtility.GetString(objDr["Trn_File_nm4"]);
                    this.Trn_Video_link = BusinessUtility.GetString(objDr["Trn_Video_link"]);
                    this.TestType = BusinessUtility.GetString(objDr["TestType"]);
                    this.TestExtID = BusinessUtility.GetString(objDr["TestExtID"]);
                    this.CertificateID = BusinessUtility.GetInt(objDr["certificateID"]);
                    this.TestCompletedCount = BusinessUtility.GetInt(objDr["testCompletedCount"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Course/Training Event List From trainingevent and trainingeventdetail Table
        /// </summary>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetEventList(string lang)
        {
            DataTable dtEventList = new DataTable();
            StringBuilder sbFinalQuery = new StringBuilder();
            StringBuilder sbQuery = new StringBuilder();
            StringBuilder sEventQuery = new StringBuilder();


            sEventQuery.Append(" SELECT ");
            sEventQuery.Append(string.Format(" courseHdr.courseHdrID as EventID, courseHdr.courseVerNo, courseHdr.courseTitle{0} AS EventTitle, courseHdr.courseActive, courseHdr.courseExtID,courseHdr.courseExtIDFr, courseHdr.courseExtLink, ", lang));
            sEventQuery.Append(" courseHdr.courseCreatedOn, courseHdr.courseCreatedBy, courseHdr.courseLastModOn, courseHdr.courseLastModBy, ");
            sEventQuery.Append(" courseHdr.courseQueNo, courseHdr.courseTestQueNo, courseHdr.courseCertOffered, courseHdr.isReviewContent, courseHdr.courseType, courseHdr.coursePassingGrade, courseHdr.is_new AS isNew, ");
            sEventQuery.Append(" courseHdr.is_Updated AS isUpdated,  trnDtl.trn_catg, trnDtl.trn_summary, trnDtl.trn_preview, trnDtl.trn_sampletest, trnDtl.trn_articles, 0 as Duration, ");
            sEventQuery.Append(string.Format(" courstyp.type{0} AS EventType  ", lang));
            sEventQuery.Append(" FROM traningevent AS courseHdr ");
            sEventQuery.Append(" INNER JOIN coursetype AS courstyp ON  courstyp.Type = courseHdr.courseType and courseHdr.isVersionActive = 1");
            sEventQuery.Append(" LEFT JOIN trainingeventdetail trnDtl ON trnDtl.trn_course_hdr_id = courseHdr.courseHdrID ");
            sEventQuery.Append(" WHERE 1=1 ");


            if (BusinessUtility.GetString(this.EventName) != "")
            {
                string[] arrEvent = this.EventName.Split(' ');
                int i = 0;
                foreach (string sEvent in arrEvent)
                {

                    if (i > 0)
                    {
                        sbQuery.Append(" union all");
                    }

                    sbQuery.Append(sEventQuery);
                    sbQuery.Append(string.Format(" AND courseHdr.courseTitle{0} like '%" +  sEvent + "%' ", lang));
                    if (BusinessUtility.GetString(this.TestType) != "")
                    {
                        if (this.TestType == CourseTestType.TestOnly)
                        {
                            sbQuery.Append(" AND courseHdr.TestType = '" + this.TestType + "' ");
                        }
                    }
                    i += 1;
                }
                sbFinalQuery = new StringBuilder();
                sbFinalQuery.Append(" Select fresult.*, count(*) as dispSeq  ");
                sbFinalQuery.Append(" from (  ");
                sbFinalQuery.Append(sbQuery);
                sbFinalQuery.Append(" ) as fresult  ");
                sbFinalQuery.Append(" group by EventID , courseVerNo  order by count(*) desc ");
                //sbFinalQuery.Append("    order by count(*) desc ");
            }
            else
            {
                sbFinalQuery = new StringBuilder();
                sbFinalQuery.Append(" Select fresult.*, count(*) as dispSeq  ");
                sbFinalQuery.Append(" from (  ");

                if (BusinessUtility.GetString(this.TestType) != "")
                {
                    if (this.TestType == CourseTestType.TestOnly)
                    {
                        sEventQuery.Append(" AND courseHdr.TestType = '" + this.TestType + "' ");
                    }
                }
                sbFinalQuery.Append(sEventQuery);
                sbFinalQuery.Append(" ) as fresult  ");
                sbFinalQuery.Append(" group by EventID, courseVerNo   order by count(*) desc ");
                //sbFinalQuery.Append("    order by count(*) desc ");
            }

            //if (BusinessUtility.GetString(this.EventName) != "")
            //{
            //    sbQuery.Append(string.Format(" AND courseHdr.courseTitle{0} like '%" + this.EventName + "%' ", lang));
            //}
            //if (BusinessUtility.GetString(this.TestType) != "")
            //{
            //    if (this.TestType == CourseTestType.TestOnly)
            //    {
            //        sbQuery.Append(" AND courseHdr.TestType <> '" + this.TestType + "' ");
            //    }
            //}



            DbHelper dbHelp = new DbHelper(true);
            try
            {
                //dtEventList = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, null);
                dtEventList = dbHelp.GetDataTable(BusinessUtility.GetString(sbFinalQuery), System.Data.CommandType.Text, null);



                DataView dv = dtEventList.DefaultView;
                dv.Sort = " dispSeq desc, courseLastModOn DESC";
                dtEventList = dv.ToTable();
                //return ResultDataTable;


            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEventList;
        }

        /// <summary>
        /// To Get Course/Training Event List From trainingevent and trainingeventdetail Table
        /// </summary>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetEventListForFeedbackChooseQuestionnaire(string lang)
        {
            DataTable dtEventList = new DataTable();
            StringBuilder sbFinalQuery = new StringBuilder();
            StringBuilder sbQuery = new StringBuilder();
            StringBuilder sEventQuery = new StringBuilder();


            sEventQuery.Append(" SELECT ");
            sEventQuery.Append(string.Format(" courseHdr.courseHdrID as EventID, courseHdr.courseVerNo, courseHdr.courseTitle{0} AS EventTitle, courseHdr.courseActive, courseHdr.courseExtID,courseHdr.courseExtIDFr, courseHdr.courseExtLink, ", lang));
            sEventQuery.Append(" courseHdr.courseCreatedOn, courseHdr.courseCreatedBy, courseHdr.courseLastModOn, courseHdr.courseLastModBy, ");
            sEventQuery.Append(" courseHdr.courseQueNo, courseHdr.courseTestQueNo, courseHdr.courseCertOffered, courseHdr.isReviewContent, courseHdr.courseType, courseHdr.coursePassingGrade, courseHdr.is_new AS isNew, ");
            sEventQuery.Append(" courseHdr.is_Updated AS isUpdated,  trnDtl.trn_catg, trnDtl.trn_summary, trnDtl.trn_preview, trnDtl.trn_sampletest, trnDtl.trn_articles, 0 as Duration, ");
            sEventQuery.Append(string.Format(" courstyp.type{0} AS EventType  ", lang));
            sEventQuery.Append("  ,ted.orderSeqInDisplay as orderSeqInDisplay ");// added by rajat on 20160428 for ordering seq
            sEventQuery.Append(" FROM traningevent AS courseHdr ");
            sEventQuery.Append(" INNER JOIN coursetype AS courstyp ON  courstyp.Type = courseHdr.courseType and courseHdr.isVersionActive = 1 and courseHdr.courseActive = 1");
            sEventQuery.Append(" inner join trainingeventdefaults ted on ted.trainingEventID = courseHdr.courseHdrID");  // added by rajat on 20160428 for ordering seq
            sEventQuery.Append(" LEFT JOIN trainingeventdetail trnDtl ON trnDtl.trn_course_hdr_id = courseHdr.courseHdrID ");
            sEventQuery.Append(" WHERE 1=1 ");


            if (BusinessUtility.GetString(this.EventName) != "")
            {
                string[] arrEvent = this.EventName.Split(' ');
                int i = 0;
                foreach (string sEvent in arrEvent)
                {

                    if (i > 0)
                    {
                        sbQuery.Append(" union all");
                    }

                    sbQuery.Append(sEventQuery);
                    sbQuery.Append(string.Format(" AND courseHdr.courseTitle{0} like '%" + sEvent + "%' ", lang));
                    if (BusinessUtility.GetString(this.TestType) != "")
                    {
                        if (this.TestType == CourseTestType.TestOnly)
                        {
                            sbQuery.Append(" AND courseHdr.TestType = '" + this.TestType + "' ");
                        }
                    }
                    i += 1;
                }
                sbFinalQuery = new StringBuilder();
                sbFinalQuery.Append(" Select fresult.*, count(*) as dispSeq  ");
                sbFinalQuery.Append(" from (  ");
                sbFinalQuery.Append(sbQuery);
                sbFinalQuery.Append(" ) as fresult  ");
                sbFinalQuery.Append(" group by EventID , courseVerNo  order by count(*) desc ");
                //sbFinalQuery.Append("    order by count(*) desc ");
            }
            else
            {
                sbFinalQuery = new StringBuilder();
                sbFinalQuery.Append(" Select fresult.*, count(*) as dispSeq  ");
                sbFinalQuery.Append(" from (  ");

                if (BusinessUtility.GetString(this.TestType) != "")
                {
                    if (this.TestType == CourseTestType.TestOnly)
                    {
                        sEventQuery.Append(" AND courseHdr.TestType = '" + this.TestType + "' ");
                    }
                }
                sbFinalQuery.Append(sEventQuery);
                sbFinalQuery.Append(" ) as fresult  ");
                sbFinalQuery.Append(" group by EventID, courseVerNo   order by orderSeqInDisplay,EventTitle asc"); //count(*) desc  // changed by rajat on 20160428 for ordering seq
                //sbFinalQuery.Append("    order by count(*) desc ");
            }

            //if (BusinessUtility.GetString(this.EventName) != "")
            //{
            //    sbQuery.Append(string.Format(" AND courseHdr.courseTitle{0} like '%" + this.EventName + "%' ", lang));
            //}
            //if (BusinessUtility.GetString(this.TestType) != "")
            //{
            //    if (this.TestType == CourseTestType.TestOnly)
            //    {
            //        sbQuery.Append(" AND courseHdr.TestType <> '" + this.TestType + "' ");
            //    }
            //}



            DbHelper dbHelp = new DbHelper(true);
            try
            {
                //dtEventList = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, null);
                dtEventList = dbHelp.GetDataTable(BusinessUtility.GetString(sbFinalQuery), System.Data.CommandType.Text, null);


                //commented by rajat on 20160428
                //DataView dv = dtEventList.DefaultView;
                //dv.Sort = " dispSeq desc, courseLastModOn DESC";
                //dtEventList = dv.ToTable();
                //commented by rajat on 20160428
                //return ResultDataTable;


            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEventList;
        }

        /// <summary>
        /// To Create Course/Training Event in trainingevent and trainingeventdetail Table
        /// </summary>
        /// <param name="userID">Pass Employee/User ID</param>
        /// <returns>True/False</returns>
        public Boolean CreateEvent(int userID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery = new StringBuilder();

                sbInsertQuery.Append(" INSERT INTO traningevent (courseVerNo, courseTitleEn, courseTitleFr, courseActive, courseExtID, courseExtIDFr, courseExtLink, courseCreatedOn, courseCreatedBy, courseQueNo, ");
                sbInsertQuery.Append(" courseTestQueNo, courseCertOffered, isReviewContent, courseType, coursePassingGrade, course_no_of_attempts, course_version_typ, ");
                sbInsertQuery.Append(" is_new, is_updated, scorm_type, total_no_slides, TestType, TestExtID, certificateID, testCompletedCount, isVersionActive ) ");
                sbInsertQuery.Append(" VALUES ( ");
                sbInsertQuery.Append(" @courseVerNo, @courseTitleEn, @courseTitleFr, @courseActive, @courseExtID, @courseExtIDFr, @courseExtLink, @courseCreatedOn, @courseCreatedBy, ");
                sbInsertQuery.Append(" @courseQueNo, @courseTestQueNo, @courseCertOffered, @isReviewContent, @courseType, @coursePassingGrade, @course_no_of_attempts, @course_version_typ, ");
                sbInsertQuery.Append(" @is_new, @is_updated, @scorm_type, @total_no_slides, @TestType, @TestExtID, @certificateID, @testCompletedCount, 1 ");
                sbInsertQuery.Append(" ) ");

                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("@courseVerNo", this.CourseVerNo, MyDbType.Int),
                DbUtility.GetParameter("@courseTitleEn", this.CourseTitleEn, MyDbType.String),
                DbUtility.GetParameter("@courseTitleFr", this.CourseTitleFr, MyDbType.String),
                DbUtility.GetParameter("@courseActive", this.CourseActive, MyDbType.Boolean),
                DbUtility.GetParameter("@courseExtID", this.CourseExtID, MyDbType.String),
                DbUtility.GetParameter("@courseExtIDFr", this.CourseExtIDFr, MyDbType.String),
                DbUtility.GetParameter("@courseExtLink", this.CourseExtLink, MyDbType.String),
                DbUtility.GetParameter("@courseCreatedOn", DateTime.Now, MyDbType.DateTime),
                DbUtility.GetParameter("@courseCreatedBy", userID, MyDbType.String),
                DbUtility.GetParameter("@courseQueNo", this.CourseQueNo, MyDbType.Int),
                DbUtility.GetParameter("@courseTestQueNo", this.CourseTestQueNo , MyDbType.Int),
                DbUtility.GetParameter("@courseCertOffered", this.CourseCertOffered , MyDbType.Boolean),
                DbUtility.GetParameter("@isReviewContent", this.IsReviewContent , MyDbType.Boolean),
                DbUtility.GetParameter("@courseType", this.CourseType , MyDbType.String),
                DbUtility.GetParameter("@coursePassingGrade", this.CoursePassingGrade, MyDbType.Int),
                DbUtility.GetParameter("@course_no_of_attempts", this.Course_no_of_attempts, MyDbType.Int),
                DbUtility.GetParameter("@course_version_typ", this.Course_version_typ , MyDbType.String),
                DbUtility.GetParameter("@is_new", this.Is_new, MyDbType.Boolean),
                DbUtility.GetParameter("@is_updated", this.Is_updated, MyDbType.Boolean),
                DbUtility.GetParameter("@scorm_type", this.Scorm_type, MyDbType.String),
                DbUtility.GetParameter("@total_no_slides", this.Total_no_slides, MyDbType.Int),
                DbUtility.GetParameter("@TestType", this.TestType, MyDbType.String),
                DbUtility.GetParameter("@TestExtID", this.TestExtID, MyDbType.String),
                DbUtility.GetParameter("@certificateID", this.CertificateID, MyDbType.Int),
                DbUtility.GetParameter("@testCompletedCount", this.TestCompletedCount, MyDbType.Int),
                });

                this.CourseID = dbTransactionHelper.GetLastInsertID();
                if (this.CourseID > 0)
                {


                    sbInsertQuery = new StringBuilder();

                    sbInsertQuery.Append(" INSERT INTO trainingeventversion ( trainingeventID, trainingEventVer, createdBy, createdDatetime ) ");    // 
                    sbInsertQuery.Append(" VALUES ( " + this.CourseID + ", ");
                    sbInsertQuery.Append(" " + this.CourseVerNo + " , ");
                    sbInsertQuery.Append(" " + userID + ", CURRENT_TIMESTAMP");
                    sbInsertQuery.Append(" ); ");

                    sbInsertQuery.Append(" INSERT INTO trainingeventdetail (trn_course_hdr_id, trn_catg, trn_summary, trn_preview, trn_sampletest, trn_articles, ");
                    sbInsertQuery.Append(" trn_file_nm1, trn_file_nm2, trn_file_nm3, trn_file_nm4, trn_video_link ");
                    sbInsertQuery.Append("  ) ");
                    sbInsertQuery.Append(" VALUES ( ");
                    sbInsertQuery.Append("@trn_course_hdr_id, @trn_catg, @trn_summary, @trn_preview, @trn_sampletest, @trn_articles, ");
                    sbInsertQuery.Append("@trn_file_nm1, @trn_file_nm2, @trn_file_nm3, @trn_file_nm4, @trn_video_link ");
                    sbInsertQuery.Append(" ) ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("@trn_course_hdr_id", this.CourseID, MyDbType.Int),
                    DbUtility.GetParameter("@trn_catg", this.Trn_Catg, MyDbType.String), 
                    DbUtility.GetParameter("@trn_summary", this.Trn_Summary, MyDbType.String), 
                    DbUtility.GetParameter("@trn_preview", this.Trn_Preview, MyDbType.String), 
                    DbUtility.GetParameter("@trn_sampletest", this.Trn_Sampletest, MyDbType.String), 
                    DbUtility.GetParameter("@trn_articles", this.Trn_Articles, MyDbType.String), 
                    DbUtility.GetParameter("@trn_file_nm1", this.Trn_File_nm1, MyDbType.String), 
                    DbUtility.GetParameter("@trn_file_nm2", this.Trn_File_nm2, MyDbType.String), 
                    DbUtility.GetParameter("@trn_file_nm3", this.Trn_File_nm3, MyDbType.String), 
                    DbUtility.GetParameter("@trn_file_nm4", this.Trn_File_nm4, MyDbType.String), 
                    DbUtility.GetParameter("@trn_video_link", this.Trn_Video_link, MyDbType.String), 
                    });
                }
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Update Course/Training Event Detail in trainingevent and trainingeventdetail Table
        /// </summary>
        /// <param name="userID">Pass Employee/User ID</param>
        /// <returns>True/False</returns>
        public Boolean UpdateEvent(int userID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" UPDATE traningevent SET ");
                sbInsertQuery.Append(" courseVerNo = @courseVerNo, courseTitleEn = @courseTitleEn, courseTitleFr = @courseTitleFr, courseActive = @courseActive, courseExtID = @courseExtID, ");
                sbInsertQuery.Append(" courseExtIDFr = @courseExtIDFr, courseExtLink = @courseExtLink,  courseLastModOn = @courseLastModOn, courseLastModBy = @courseLastModBy, courseQueNo = @courseQueNo, ");
                sbInsertQuery.Append(" courseTestQueNo = @courseTestQueNo, courseCertOffered = @courseCertOffered, isReviewContent = @isReviewContent, courseType = @courseType, coursePassingGrade = @coursePassingGrade, ");
                sbInsertQuery.Append(" course_no_of_attempts = @course_no_of_attempts, course_version_typ = @course_version_typ, ");
                sbInsertQuery.Append(" is_new = @is_new, is_updated = @is_updated, scorm_type = @scorm_type, total_no_slides = @total_no_slides, TestType = @TestType, TestExtID = @TestExtID, certificateID = @certificateID, testCompletedCount = @testCompletedCount ");
                sbInsertQuery.Append(" WHERE courseHdrID = @courseHdrID and courseVerNo = @courseVerNo and isVersionActive = 1 ");

                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("@courseVerNo", this.CourseVerNo, MyDbType.Int),
                DbUtility.GetParameter("@courseTitleEn", this.CourseTitleEn, MyDbType.String),
                DbUtility.GetParameter("@courseTitleFr", this.CourseTitleFr, MyDbType.String),
                DbUtility.GetParameter("@courseActive", this.CourseActive, MyDbType.Boolean),
                DbUtility.GetParameter("@courseExtID", this.CourseExtID, MyDbType.String),
                DbUtility.GetParameter("@courseExtIDFr", this.CourseExtIDFr, MyDbType.String),
                DbUtility.GetParameter("@courseExtLink", this.CourseExtLink, MyDbType.String),
                DbUtility.GetParameter("@courseLastModOn", DateTime.Now, MyDbType.DateTime),
                DbUtility.GetParameter("@courseLastModBy", userID, MyDbType.String),
                DbUtility.GetParameter("@courseQueNo", this.CourseQueNo, MyDbType.Int),
                DbUtility.GetParameter("@courseTestQueNo", this.CourseTestQueNo , MyDbType.Int),
                DbUtility.GetParameter("@courseCertOffered", this.CourseCertOffered , MyDbType.Boolean),
                DbUtility.GetParameter("@isReviewContent", this.IsReviewContent , MyDbType.Boolean),
                DbUtility.GetParameter("@courseType", this.CourseType , MyDbType.String),
                DbUtility.GetParameter("@coursePassingGrade", this.CoursePassingGrade, MyDbType.Int),
                DbUtility.GetParameter("@course_no_of_attempts", this.Course_no_of_attempts, MyDbType.Int),
                DbUtility.GetParameter("@course_version_typ", this.Course_version_typ , MyDbType.String),
                DbUtility.GetParameter("@is_new", this.Is_new, MyDbType.Boolean),
                DbUtility.GetParameter("@is_updated", this.Is_updated, MyDbType.Boolean),
                DbUtility.GetParameter("@scorm_type", this.Scorm_type, MyDbType.String),
                DbUtility.GetParameter("@total_no_slides", this.Total_no_slides, MyDbType.Int),
                DbUtility.GetParameter("@courseHdrID", this.CourseID, MyDbType.Int),
                DbUtility.GetParameter("@TestType", this.TestType, MyDbType.String),
                DbUtility.GetParameter("@TestExtID", this.TestExtID, MyDbType.String),
                DbUtility.GetParameter("@certificateID", this.CertificateID, MyDbType.Int),
                DbUtility.GetParameter("@testCompletedCount", this.TestCompletedCount, MyDbType.Int),
                });

                if (this.CourseID > 0)
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" UPDATE trainingeventdetail SET trn_catg = @trn_catg, trn_summary = @trn_summary, trn_preview = @trn_preview, trn_sampletest = @trn_sampletest, trn_articles = @trn_articles, ");
                    sbInsertQuery.Append(" trn_file_nm1 = @trn_file_nm1, trn_file_nm2 = @trn_file_nm2, trn_file_nm3 = @trn_file_nm3, trn_file_nm4 = @trn_file_nm4, trn_video_link = @trn_video_link ");
                    sbInsertQuery.Append(" WHERE trn_course_hdr_id = @trn_course_hdr_id ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("@trn_course_hdr_id", this.CourseID, MyDbType.Int),
                    DbUtility.GetParameter("@trn_catg", this.Trn_Catg, MyDbType.String), 
                    DbUtility.GetParameter("@trn_summary", this.Trn_Summary, MyDbType.String), 
                    DbUtility.GetParameter("@trn_preview", this.Trn_Preview, MyDbType.String), 
                    DbUtility.GetParameter("@trn_sampletest", this.Trn_Sampletest, MyDbType.String), 
                    DbUtility.GetParameter("@trn_articles", this.Trn_Articles, MyDbType.String), 
                    DbUtility.GetParameter("@trn_file_nm1", this.Trn_File_nm1, MyDbType.String), 
                    DbUtility.GetParameter("@trn_file_nm2", this.Trn_File_nm2, MyDbType.String), 
                    DbUtility.GetParameter("@trn_file_nm3", this.Trn_File_nm3, MyDbType.String), 
                    DbUtility.GetParameter("@trn_file_nm4", this.Trn_File_nm4, MyDbType.String), 
                    DbUtility.GetParameter("@trn_video_link", this.Trn_Video_link, MyDbType.String), 
                    });
                }
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Get Course Type List From coursetype Table
        /// </summary>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetCourseTypeList(string lang)
        {
            DataTable dtCourseTypeList = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(string.Format(" SELECT type as TypeValue, type{0} as TypeText FROM coursetype   ", lang));
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtCourseTypeList = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtCourseTypeList;
        }

        /// <summary>
        /// To Validate Course/Training Event Test ID From trainingevent Table
        /// </summary>
        /// <param name="eventID">Pass Event ID</param>
        /// <returns>True/False</returns>
        public Boolean IsValidTestCourse(int eventID)
        {
            DbHelper dbHelper = new DbHelper(false);
            bool bReturn = false;
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT courseHdrID FROM traningevent where courseHdrID = @EventID and testType= @TestType  and isVersionActive = 1; ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("EventID", eventID, MyDbType.Int),
                DbUtility.GetParameter("TestType", CourseTestType.TestOnly, MyDbType.String),
                });
                bReturn = (BusinessUtility.GetInt(rValue) > 0);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }


        /// <summary>
        /// To Update Event Version
        /// </summary>
        /// <param name="eventID">Pass Event ID</param>
        /// <param name="eventVersion">Pass Event Version</param>
        /// <returns>True/False</returns>
        public Boolean UpdatEventVersion(int eventID, int eventVersion)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" UPDATE traningevent SET  courseVerNo = @eventVersion  WHERE courseHdrID = @courseHdrID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("@courseHdrID", eventID, MyDbType.Int),
                DbUtility.GetParameter("@eventVersion", eventVersion, MyDbType.Int),
                });

                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                dbTransactionHelper.RollBackTransaction();
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }
    }
}
