﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Configuration;
using System.Data.Odbc;
using System.Text.RegularExpressions;
using System.Configuration;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define User Importing Feature From Pensivo Server
    /// </summary>
    public class ImportPensivoUsers
    {
        /// <summary>
        /// To Get/Set Updated Row Count
        /// </summary>
        public int iRowUpdate { get; set; }

        /// <summary>
        /// To Get/Set Inserted Row Count
        /// </summary>
        public int iRowInserted { get; set; }

        /// <summary>
        /// To Get/Set Row Failure Count
        /// </summary>
        public int iRowFailure { get; set; }

        /// <summary>
        /// To Set Pensivo(Old System) Deleted User to Delete After Imported To The New System
        /// </summary>
        /// <param name="dtDeleted">Pass Deleted Pensivo User List as Datatable</param>
        /// <returns>True/False</returns>
        public bool UpdatePensivoDeletedUser(DataTable dtDeleted)
        {
            foreach (DataRow dRow in dtDeleted.Rows)
            {
                int sPensivoUID = BusinessUtility.GetInt(dRow["uuid"]);
                if (sPensivoUID > 0)
                {
                    DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
                    try
                    {
                        dbTransactionHelper.BeginTransaction();
                        StringBuilder sbQuery = new StringBuilder();
                        sbQuery.Append(" SELECT empHdrID, empExtID, empActive FROM empheader  where PensivoUUID = @PensivoUUID ");

                        DataTable dtResult = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("PensivoUUID", BusinessUtility.GetInt(sPensivoUID), MyDbType.Int)});


                        if (dtResult != null)
                        {
                            if (dtResult.Rows.Count > 0)
                            {
                                int empHdrID = BusinessUtility.GetInt(dtResult.Rows[0]["empHdrID"]);
                                string empExtID = BusinessUtility.GetString(dtResult.Rows[0]["empExtID"]);
                                string empActive = BusinessUtility.GetString(dtResult.Rows[0]["empActive"]);
                                if (empActive.ToUpper() == "True".ToUpper())
                                {

                                    string sinsert = " INSERT INTO empextidlinks (empHeader_empHdrID, empOLDExtID, createddatetime, status  ) VALUE (@empHeader_empHdrID, @empOLDExtID,  CURRENT_TIMESTAMP, 'D');  ";
                                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sinsert), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empHeader_empHdrID", empHdrID, MyDbType.Int),
                                    DbUtility.GetParameter("empOLDExtID", empExtID, MyDbType.String)
                                    });

                                    StringBuilder sbInsertQuery = new StringBuilder();
                                    sbInsertQuery.Append(" Update empheader Set  empActive = @empActive,  ");
                                    sbInsertQuery.Append(" empLastUpdatedOn = CURRENT_TIMESTAMP,   ");
                                    sbInsertQuery.Append("  isDeleted = @isDeleted, empDisabledOn = CURRENT_TIMESTAMP  Where PensivoUUID = @PensivoUUID ");
                                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("empActive", 0, MyDbType.Int),
                                    DbUtility.GetParameter("isDeleted", 1, MyDbType.Int),
                                    DbUtility.GetParameter("PensivoUUID", BusinessUtility.GetInt(sPensivoUID) , MyDbType.Int),
                                    });

                                    //string sselect = "SELECT count(*) FROM empextidlinks  where empHeader_empHdrID = @empHeader_empHdrID and empOLDExtID = @empOLDExtID; ";
                                    //object returnObj = dbTransactionHelper.GetValue(sselect, CommandType.Text, new MySqlParameter[]{
                                    //DbUtility.GetParameter("empHeader_empHdrID", empHdrID, MyDbType.Int),
                                    //DbUtility.GetParameter("empOLDExtID", empExtID, MyDbType.String)
                                    //});

                                    //if (BusinessUtility.GetInt(returnObj) > 0)
                                    //{
                                    //    //if (empActive.ToUpper() == "True".ToUpper())
                                    //    //{
                                    //    StringBuilder sbInsertQuery = new StringBuilder();
                                    //    sbInsertQuery.Append(" Update empheader Set  empActive = @empActive,  ");
                                    //    sbInsertQuery.Append(" empLastUpdatedOn = CURRENT_TIMESTAMP,   ");
                                    //    sbInsertQuery.Append("  isDeleted = @isDeleted, empDisabledOn = CURRENT_TIMESTAMP  Where PensivoUUID = @PensivoUUID ");
                                    //    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    //    DbUtility.GetParameter("empActive", 0, MyDbType.Int),
                                    //    //DbUtility.GetParameter("empLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                                    //    DbUtility.GetParameter("isDeleted", 1, MyDbType.Int),
                                    //    DbUtility.GetParameter("PensivoUUID", BusinessUtility.GetInt(sPensivoUID) , MyDbType.Int),
                                    //    //DbUtility.GetParameter("empDisabledOn", DateTime.Now, MyDbType.DateTime),
                                    //    });
                                    //    //}
                                    //}
                                    //else
                                    //{
                                    //    string sinsert = " INSERT INTO empextidlinks (empHeader_empHdrID, empOLDExtID, createddatetime, status  ) VALUE (@empHeader_empHdrID, @empOLDExtID,  CURRENT_TIMESTAMP, 'D');  ";
                                    //    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sinsert), CommandType.Text, new MySqlParameter[]{
                                    //    DbUtility.GetParameter("empHeader_empHdrID", empHdrID, MyDbType.Int),
                                    //    DbUtility.GetParameter("empOLDExtID", empExtID, MyDbType.String)
                                    //    });
                                    //    //if (empActive.ToUpper() == "True".ToUpper())
                                    //    //{
                                    //    StringBuilder sbInsertQuery = new StringBuilder();
                                    //    sbInsertQuery.Append(" Update empheader Set  empActive = @empActive,  ");
                                    //    sbInsertQuery.Append(" empLastUpdatedOn = CURRENT_TIMESTAMP,   ");
                                    //    sbInsertQuery.Append("  isDeleted = @isDeleted, empDisabledOn = CURRENT_TIMESTAMP  Where PensivoUUID = @PensivoUUID ");
                                    //    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    //    DbUtility.GetParameter("empActive", 0, MyDbType.Int),
                                    //    //DbUtility.GetParameter("empLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                                    //    DbUtility.GetParameter("isDeleted", 1, MyDbType.Int),
                                    //    DbUtility.GetParameter("PensivoUUID", BusinessUtility.GetInt(sPensivoUID) , MyDbType.Int),
                                    //    //DbUtility.GetParameter("empDisabledOn", DateTime.Now, MyDbType.DateTime),
                                    //    });
                                    //    //}
                                    //}

                                }
                            }
                        }
                        dbTransactionHelper.CommitTransaction();
                    }
                    catch (Exception e)
                    {
                        iRowFailure += 1;
                        dbTransactionHelper.RollBackTransaction();
                        ErrorLog.createLog(sPensivoUID + "" + e.ToString() + "  " + e.InnerException);
                    }
                    finally
                    {
                        dbTransactionHelper.CloseDatabaseConnection();
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// To Import Pensvio Users From Pensivo PostgreSQL in empheader and empreference Table
        /// </summary>
        /// <param name="dt">Pass User List as Datatable to be imported</param>
        /// <returns>True/False</returns>
        public bool ImportUsers(DataTable dt)
        {
            RoleManageLists objRoleManageList = new RoleManageLists();
            foreach (DataRow dRow in dt.Rows)
            {
                string sEmpID = BusinessUtility.GetString(dRow["username"]);
                string sUserPass = BusinessUtility.GetString(dRow["userpass"]);
                string sPensivoUID = BusinessUtility.GetString(dRow["uuid"]);
                int iActive = BusinessUtility.GetInt(dRow["active"]);
                int iDeleted = BusinessUtility.GetInt(dRow["deleted"]);
                string empPhone = BusinessUtility.GetString(dRow["tbs_home_phone"]);
                string empAddress1 = BusinessUtility.GetString(dRow["tbs_address1"]);
                string empAddress2 = BusinessUtility.GetString(dRow["tbs_address2"]);
                string empCity = BusinessUtility.GetString(dRow["tbs_city"]);
                string empState = BusinessUtility.GetString(dRow["tbs_state"]);
                string empZipCode = BusinessUtility.GetString(dRow["tbs_postal"]);
                DateTime empDOB = BusinessUtility.GetDateTime(BusinessUtility.GetString(dRow["tbs_birthdate"]), "yyyy-MM-dd");
                string sDivisionID = "";
                string sLocation = "";
                string sJobID = "";
                string sDistrict = "";
                string sRegionTBS = "";
                string sDivisionTBS = "";
                string sSite = "";
                string sType = "";
                string sProvinceAbbr = "";
                string sTypeParsed = "";

                string sOldDivisionID = "";
                string sOldJobID = "";
                string sOldLocation = "";
                string sOldType = "";
                string sOldTypeParsed = "";
                string sOldDistrict = "";
                string sOldRegionTBS = "";
                string sOldDivisionTBS = "";
                string sOldSite = "";
                string sOldProvinceAbbr = "";

                bool isEmpOldData = false;
                bool isNewUser = false;
                int iInsertedEmpID = 0;

                //Regex rgx = new Regex(BusinessUtility.GetString(ConfigurationManager.AppSettings["EmpIdValidatorExpression"]));
                Regex rgx = new Regex(BusinessUtility.GetString(AppConfig.GetAppConfigValue("EmpIdValidatorExpression")));
                if (!rgx.IsMatch(sEmpID))
                {
                    ErrorLog.createLog(" Invalid User Name Format" + " UUID :- " + sPensivoUID + " EmpHdr :-  " + sEmpID + " Emp First Name :- " + BusinessUtility.GetString(dRow["firstname"]) + " Emp Last Name :- " + BusinessUtility.GetString(dRow["lastname"]));
                }
                else
                {
                    if (sEmpID != "" && sPensivoUID != "")
                    {
                        DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
                        try
                        {
                            dbTransactionHelper.BeginTransaction();
                            StringBuilder sbQuery = new StringBuilder();
                            sbQuery.Append(" SELECT empHdrID, empExtID FROM empheader  where PensivoUUID = @PensivoUUID ");

                            DataTable dtResult = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("PensivoUUID", BusinessUtility.GetInt( sPensivoUID), MyDbType.Int)});

                            int resultValue = 0;
                            string sempExtID = "";
                            if (dtResult != null)
                            {
                                if (dtResult.Rows.Count > 0)
                                {
                                    resultValue = BusinessUtility.GetInt(dtResult.Rows[0]["empHdrID"]);
                                    sempExtID = BusinessUtility.GetString(dtResult.Rows[0]["empExtID"]);
                                }
                            }

                            if (!string.IsNullOrEmpty(sempExtID))
                            {
                                //if (sempExtID != sEmpID)
                                {
                                    //string sselect = "SELECT count(*) FROM empextidlinks  where empHeader_empHdrID = @empHeader_empHdrID and empOLDExtID = @empOLDExtID; ";
                                    //object returnObj = dbTransactionHelper.GetValue(sselect, CommandType.Text, new MySqlParameter[]{
                                    //DbUtility.GetParameter("empHeader_empHdrID", resultValue, MyDbType.Int),
                                    //DbUtility.GetParameter("empOLDExtID", sempExtID, MyDbType.String)
                                    //});

                                    string sinsert = " INSERT INTO empextidlinks (empHeader_empHdrID, empOLDExtID, createddatetime, status ) VALUE (@empHeader_empHdrID, @empOLDExtID, CURRENT_TIMESTAMP, 'I' );  ";
                                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sinsert), CommandType.Text, new MySqlParameter[]{
                                        DbUtility.GetParameter("empHeader_empHdrID", resultValue, MyDbType.Int),
                                        DbUtility.GetParameter("empOLDExtID", sempExtID, MyDbType.String)
                                        });

                                    //if (BusinessUtility.GetInt(returnObj) > 0)
                                    //{

                                    //}
                                    //else
                                    //{
                                    //    string sinsert = " INSERT INTO empextidlinks (empHeader_empHdrID, empOLDExtID, createddatetime, status ) VALUE (@empHeader_empHdrID, @empOLDExtID, CURRENT_TIMESTAMP, 'I' );  ";
                                    //    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sinsert), CommandType.Text, new MySqlParameter[]{
                                    //    DbUtility.GetParameter("empHeader_empHdrID", resultValue, MyDbType.Int),
                                    //    DbUtility.GetParameter("empOLDExtID", sempExtID, MyDbType.String)
                                    //    });
                                    //}
                                }
                            }



                            if (resultValue > 0)
                            {
                                #region UpdateUserDetail
                                int iUserID = resultValue;

                                StringBuilder sbUpdate = new StringBuilder();
                                sbUpdate.Append(" INSERT INTO empheaderhistory (emp_HdrID, empPassword, empFirstName, empLastName, empCreatedOn, empExtID, empEmail, empActive, empLoginID, isDeleted, PensivoUUID, createdon ) ");
                                sbUpdate.Append(" select ");
                                sbUpdate.Append(" empHdrID, empPassword, empFirstName, empLastName, empCreatedOn, empExtID, empEmail, empActive, empLoginID, isDeleted, PensivoUUID, now() ");
                                sbUpdate.Append(" FROM empheader Where empHdrID = @userID; ");

                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbUpdate), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("userID", iUserID, MyDbType.Int),
                                });

                                int iHistoryID = dbTransactionHelper.GetLastInsertID();
                                if (iHistoryID > 0)
                                {
                                    sbUpdate = new StringBuilder();
                                    sbUpdate.Append(" insert into empreferencehistroy ");
                                    sbUpdate.Append(" (historyID, empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue ) ");
                                    sbUpdate.Append(" select @HistoryID, empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue from empreference WHERE empHeader_empHdrID =@userID; ");

                                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbUpdate), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("userID", iUserID, MyDbType.Int),
                                    DbUtility.GetParameter("HistoryID", iHistoryID, MyDbType.Int),
                                    });
                                }

                                StringBuilder sbInsertQuery = new StringBuilder();
                                StringBuilder sbDelete = new StringBuilder();

                                string sFirstName = BusinessUtility.GetString(dRow["firstname"]);
                                string sLastName = BusinessUtility.GetString(dRow["lastname"]);
                                string sEmail = BusinessUtility.GetString(dRow["email"]);

                                sbInsertQuery.Append(" Update empheader Set  empFirstName = @empFirstName, empLastName = @empLastName, empEmail = @empEmail, empActive = @empActive,    ");
                                sbInsertQuery.Append(" empLastUpdatedOn = @empLastUpdatedOn, empExtID = @empExtID, empLoginID = @empLoginID,  ");
                                sbInsertQuery.Append("  isDeleted = @isDeleted, PensivoUUID = @PensivoUUID Where empHdrID = @empID ");

                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("empID",  iUserID , MyDbType.Int), 
                                DbUtility.GetParameter("empFirstName", ( sFirstName), MyDbType.String),
                                DbUtility.GetParameter("empLastName", (sLastName), MyDbType.String),
                                DbUtility.GetParameter("empEmail", sEmail, MyDbType.String),
                                DbUtility.GetParameter("empActive", iActive, MyDbType.Int),
                                DbUtility.GetParameter("empLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                                DbUtility.GetParameter("empExtID", (sEmpID) , MyDbType.String),
                                DbUtility.GetParameter("empLoginID", (sEmpID) , MyDbType.String),
                                DbUtility.GetParameter("isDeleted", iDeleted, MyDbType.Int),
                                DbUtility.GetParameter("PensivoUUID", BusinessUtility.GetInt( sPensivoUID) , MyDbType.Int),
                                });




                                iInsertedEmpID = iUserID;
                                if (iInsertedEmpID > 0)
                                {

                                    //// Getting Saved Values
                                    Employee objEmpnew = new Employee();
                                    DataTable dtSavedValues = objEmpnew.GetIsChangedEmployeeEmpRefDetails(BusinessUtility.GetInt(iInsertedEmpID));
                                    if (dtSavedValues.Rows.Count > 0)
                                    {
                                        sOldDivisionID = BusinessUtility.GetString(dtSavedValues.Rows[0]["EMPDIV"]);
                                        sOldJobID = BusinessUtility.GetString(dtSavedValues.Rows[0]["EMPJOB"]);
                                        sOldLocation = BusinessUtility.GetString(dtSavedValues.Rows[0]["EMPSTR"]);
                                        sOldType = BusinessUtility.GetString(dtSavedValues.Rows[0]["EMPTYP"]);
                                        sOldTypeParsed = BusinessUtility.GetString(dtSavedValues.Rows[0]["EMPTPP"]);
                                        sOldDistrict = BusinessUtility.GetString(dtSavedValues.Rows[0]["EMPTDIST"]);
                                        sOldRegionTBS = BusinessUtility.GetString(dtSavedValues.Rows[0]["EMPTREGN"]);
                                        sOldDivisionTBS = BusinessUtility.GetString(dtSavedValues.Rows[0]["EMPTDIVN"]);
                                        sOldSite = BusinessUtility.GetString(dtSavedValues.Rows[0]["EMPSITE"]);
                                        sOldProvinceAbbr = BusinessUtility.GetString(dtSavedValues.Rows[0]["EMPPABBR"]);

                                        isEmpOldData = true;
                                    }

                                    #region UpdateUserRefrenceDetail
                                    sDivisionID = (BusinessUtility.GetString(dRow["tbs_tax_loc"]));
                                    sLocation = (BusinessUtility.GetString(dRow["tbs_department_id"]));
                                    sJobID = (BusinessUtility.GetString(dRow["tbs_job_code"]));

                                    if (dt.Columns.Contains("tbs_district"))
                                    {
                                        sDistrict = (BusinessUtility.GetString(dRow["tbs_district"]));
                                    }


                                    if (dt.Columns.Contains("tbs_region"))
                                    {
                                        sRegionTBS = (BusinessUtility.GetString(dRow["tbs_region"]));
                                    }

                                    if (dt.Columns.Contains("tbs_division"))
                                    {
                                        sDivisionTBS = (BusinessUtility.GetString(dRow["tbs_division"]));
                                    }


                                    if (dt.Columns.Contains("bdl_site_number"))
                                    {
                                        sSite = (BusinessUtility.GetString(dRow["bdl_site_number"]));
                                    }


                                    if (dt.Columns.Contains("employee_type_parsed"))
                                    {
                                        sType = (BusinessUtility.GetString(dRow["employee_type_parsed"]));
                                    }
                                    else
                                    {
                                        sType = "";
                                    }

                                    if (dt.Columns.Contains("province_abbr"))
                                    {
                                        sProvinceAbbr = (BusinessUtility.GetString(dRow["province_abbr"]));
                                    }

                                    sTypeParsed = (BusinessUtility.GetString(dRow["tbs_emp_type"]));


                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.Division, sDivisionID))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Division, MyDbType.String),
                                            });

                                        if (sDivisionID != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Division, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sDivisionID, MyDbType.String)
                                            });
                                        }
                                    }



                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.JobCode, sJobID))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.JobCode, MyDbType.String),
                                            });

                                        if (sJobID != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.JobCode, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sJobID, MyDbType.String)
                                            });
                                        }
                                    }



                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.Store, sLocation))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Store, MyDbType.String),
                                            });
                                        if (sLocation != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Store, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sLocation, MyDbType.String)
                                            });
                                        }
                                    }




                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.Type, sType))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Type, MyDbType.String),
                                            });

                                        if (sType != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Type, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sType, MyDbType.String)
                                            });
                                        }
                                    }




                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.TypeParsed, sTypeParsed))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.TypeParsed, MyDbType.String),
                                            });
                                        if (sTypeParsed != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.TypeParsed, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sTypeParsed, MyDbType.String)
                                            });
                                        }
                                    }



                                    if (sDistrict != "")
                                    {
                                        sDistrict = sDistrict.PadLeft(4, '0');
                                    }
                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.District, sDistrict))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.District, MyDbType.String),
                                            });

                                        if (sDistrict != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.District, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sDistrict, MyDbType.String)
                                            });
                                        }
                                    }


                                    if (sRegionTBS != "")
                                    {
                                        sRegionTBS = sRegionTBS.PadLeft(4, '0');
                                    }
                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.RegionTBS, sRegionTBS))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.RegionTBS, MyDbType.String),
                                            });


                                        if (sRegionTBS != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.RegionTBS, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sRegionTBS, MyDbType.String)
                                            });

                                        }
                                    }


                                    if (sDivisionTBS != "")
                                    {
                                        sDivisionTBS = sDivisionTBS.PadLeft(4, '0');
                                    }
                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.DivisionTBS, sDivisionTBS))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.DivisionTBS, MyDbType.String),
                                            });

                                        if (sDivisionTBS != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.DivisionTBS, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sDivisionTBS, MyDbType.String)
                                            });
                                        }
                                    }




                                    //sSite = sDivisionTBS.PadLeft(4, '0');
                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.Site, sSite))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Site, MyDbType.String),
                                            });

                                        if (sSite != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Site, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sSite, MyDbType.String)
                                            });
                                        }
                                    }




                                    string sCSite = string.Empty;
                                    sCSite = objRoleManageList.GetFixedUserCSite(sEmpID);

                                    if (sCSite == "")
                                    {
                                        sCSite = sLocation;
                                    }

                                    if (sCSite != "")
                                    {
                                        sCSite = sCSite.PadLeft(4, '0');
                                    }
                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.CSite, sCSite))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.CSite, MyDbType.String),
                                            });

                                        if (sCSite != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.CSite, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sCSite, MyDbType.String)
                                            });
                                        }
                                    }



                                    if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.Province, sProvinceAbbr))
                                    {
                                        sbDelete = new StringBuilder();
                                        sbDelete.Append(" Delete FROM empreference Where empHeader_empHdrID = @empHeader_empHdrID AND  sysEmpReferenceCodes_sysRefCode = @sysEmpReferenceCodes_sysRefCode");

                                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbDelete), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Province, MyDbType.String),
                                            });

                                        if (sProvinceAbbr != "")
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Province, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sProvinceAbbr, MyDbType.String)
                                            });
                                        }
                                    }



                                    //sDivisionID, sJobID, sLocation, sType, sTypeParsed, sDistrict, sRegionTBS, sDivisionTBS, sSite, sProvinceAbbr

                                    #endregion
                                }
                                iRowUpdate += 1;
                                #endregion
                            }
                            else
                            {
                                #region InsertUseretail
                                isNewUser = true;
                                string sFirstName = BusinessUtility.GetString(dRow["firstname"]);
                                string sLastName = BusinessUtility.GetString(dRow["lastname"]);
                                string sEmail = BusinessUtility.GetString(dRow["email"]);

                                StringBuilder sbInsertQuery = new StringBuilder();
                                sbInsertQuery.Append(" INSERT INTO empheader (empPassword, empFirstName, empLastName, empCreatedOn, empEmail, empActive, empNotifyPref, empLastUpdatedOn, empExtID, empLoginID, emptype,  ");
                                sbInsertQuery.Append("   isDeleted, PensivoUUID, createdSource )");
                                sbInsertQuery.Append(" VALUES (@empPassword, @empFirstName, @empLastName, @empCreatedOn, @empEmail, @empActive, @empNotifyPref, @empLastUpdatedOn, @empExtID, @empLoginID, @emptype,     ");
                                sbInsertQuery.Append("  @isDeleted, @PensivoUUID, @createdSource  ");
                                sbInsertQuery.Append(" ) ");

                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("empPassword", sUserPass, MyDbType.String),    
                                DbUtility.GetParameter("empFirstName", ( sFirstName), MyDbType.String),
                                DbUtility.GetParameter("empLastName", (sLastName), MyDbType.String),
                                DbUtility.GetParameter("empCreatedOn", DateTime.Now, MyDbType.DateTime),
                                DbUtility.GetParameter("empEmail", sEmail, MyDbType.String),
                                DbUtility.GetParameter("empActive", iActive, MyDbType.Int),
                                DbUtility.GetParameter("isDeleted", iDeleted, MyDbType.Int),
                                DbUtility.GetParameter("empNotifyPref", "1", MyDbType.String),
                                DbUtility.GetParameter("empLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                                DbUtility.GetParameter("empExtID", (sEmpID) , MyDbType.String),
                                DbUtility.GetParameter("empLoginID", (sEmpID) , MyDbType.String),
                                DbUtility.GetParameter("emptype", "E" , MyDbType.String),
                                DbUtility.GetParameter("PensivoUUID", BusinessUtility.GetInt( sPensivoUID) , MyDbType.Int),
                                DbUtility.GetParameter("createdSource", UserCreatedSource.PostGreySQL , MyDbType.String),
                                });

                                iInsertedEmpID = dbTransactionHelper.GetLastInsertID();
                                if (iInsertedEmpID > 0)
                                {
                                    #region InsertUserRefDetail
                                    sDivisionID = "";
                                    sLocation = "";
                                    sJobID = "";
                                    sType = "";
                                    sTypeParsed = "";
                                    sDistrict = "";
                                    sRegionTBS = "";
                                    sDivisionTBS = "";
                                    sSite = "";
                                    sProvinceAbbr = "";

                                    sDivisionID = (BusinessUtility.GetString(dRow["tbs_tax_loc"]));
                                    sLocation = (BusinessUtility.GetString(dRow["tbs_department_id"]));
                                    sJobID = (BusinessUtility.GetString(dRow["tbs_job_code"]));
                                    sType = (BusinessUtility.GetString(dRow["employee_type_parsed"]));
                                    sTypeParsed = (BusinessUtility.GetString(dRow["tbs_emp_type"]));

                                    if (dt.Columns.Contains("tbs_district"))
                                    {
                                        sDistrict = (BusinessUtility.GetString(dRow["tbs_district"]));
                                    }

                                    if (dt.Columns.Contains("tbs_region"))
                                    {
                                        sRegionTBS = (BusinessUtility.GetString(dRow["tbs_region"]));
                                    }

                                    if (dt.Columns.Contains("tbs_division"))
                                    {
                                        sDivisionTBS = (BusinessUtility.GetString(dRow["tbs_division"]));
                                    }

                                    if (dt.Columns.Contains("bdl_site_number"))
                                    {
                                        sSite = (BusinessUtility.GetString(dRow["bdl_site_number"]));
                                    }

                                    if (dt.Columns.Contains("province_abbr"))
                                    {
                                        sProvinceAbbr = (BusinessUtility.GetString(dRow["province_abbr"]));
                                    }

                                    if (sDivisionID != "")
                                    {
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.Division, sDivisionID))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Division, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sDivisionID, MyDbType.String)
                                            });
                                        }
                                    }

                                    if (sJobID != "")
                                    {
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.JobCode, sJobID))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.JobCode, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sJobID, MyDbType.String)
                                            });
                                        }
                                    }

                                    if (sLocation != "")
                                    {
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.Store, sLocation))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Store, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sLocation, MyDbType.String)
                                            });
                                        }
                                    }

                                    if (sType != "")
                                    {
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.Type, sType))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Type, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sType, MyDbType.String)
                                            });
                                        }
                                    }

                                    if (sTypeParsed != "")
                                    {
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.TypeParsed, sTypeParsed))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.TypeParsed, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sTypeParsed, MyDbType.String)
                                            });
                                        }
                                    }


                                    if (sDistrict != "")
                                    {
                                        sDistrict = sDistrict.PadLeft(4, '0');
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.District, sDistrict))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.District, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sDistrict, MyDbType.String)
                                            });
                                        }
                                    }

                                    if (sRegionTBS != "")
                                    {
                                        sRegionTBS = sRegionTBS.PadLeft(4, '0');
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.RegionTBS, sRegionTBS))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.RegionTBS, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sRegionTBS, MyDbType.String)
                                            });
                                        }
                                    }

                                    if (sDivisionTBS != "")
                                    {
                                        sDivisionTBS = sDivisionTBS.PadLeft(4, '0');
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.DivisionTBS, sDivisionTBS))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.DivisionTBS, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sDivisionTBS, MyDbType.String)
                                            });
                                        }
                                    }


                                    if (sSite != "")
                                    {
                                        //sDivisionTBS = sDivisionTBS.PadLeft(4, '0');
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.Site, sSite))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Site, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sSite, MyDbType.String)
                                            });
                                        }
                                    }


                                    string sCSite = string.Empty;
                                    sCSite = objRoleManageList.GetFixedUserCSite(sEmpID);

                                    if (sCSite == "")
                                    {
                                        sCSite = sLocation;
                                    }
                                    if (sCSite != "")
                                    {
                                        sCSite = sCSite.PadLeft(4, '0');
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.CSite, sCSite))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.CSite, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sCSite, MyDbType.String)
                                            });
                                        }
                                    }



                                    if (sProvinceAbbr != "")
                                    {
                                        //sDivisionTBS = sDivisionTBS.PadLeft(4, '0');
                                        if (sysRefCreateNotExist(dbTransactionHelper, EmpRefCode.Province, sProvinceAbbr))
                                        {
                                            sbInsertQuery = new StringBuilder();
                                            sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                            sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                            sbInsertQuery.Append(" ) ");

                                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Province, MyDbType.String),
                                            DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sProvinceAbbr, MyDbType.String)
                                            });
                                        }
                                    }
                                    #endregion
                                }

                                //#region InsertEmpHdrInfo

                                //sbInsertQuery = new StringBuilder();
                                //sbInsertQuery.Append(" insert into emphdrinfo ");
                                //sbInsertQuery.Append(" (empHdrID, empPhone, empAddress1, empAddress2, empCity, empState, empZipCode, empDOB ) ");
                                //sbInsertQuery.Append(" values ");
                                //sbInsertQuery.Append(" (@empHdrID, @empPhone, @empAddress1, @empAddress2, @empCity, @empState, @empZipCode, @empDOB ) ");

                                //dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                //DbUtility.GetParameter("empHdrID", iInsertedEmpID, MyDbType.Int),    
                                //DbUtility.GetParameter("empPhone", empPhone, MyDbType.String),
                                //DbUtility.GetParameter("empAddress1", empAddress1, MyDbType.String),
                                //DbUtility.GetParameter("empAddress2", empAddress2, MyDbType.String),
                                //DbUtility.GetParameter("empCity", empCity, MyDbType.String),
                                //DbUtility.GetParameter("empState", empState, MyDbType.String),
                                //DbUtility.GetParameter("empZipCode", empZipCode, MyDbType.String),
                                //DbUtility.GetParameter("empDOB", empDOB, MyDbType.DateTime)
                                //});

                                //#endregion
                                #endregion
                                iRowInserted += 1;
                            }

                            dbTransactionHelper.CommitTransaction();
                            if (iInsertedEmpID > 0)
                            {
                                objRoleManageList.UpdateEmployeeHasFunctionalityStatus(BusinessUtility.GetInt(iInsertedEmpID), "");
                                EmpAssignedCourses objAssignedCourse = new EmpAssignedCourses();

                                if (isNewUser)
                                {
                                    // Write Command To Reset/Create EmpCourse
                                    objAssignedCourse.ResetEmployeeAssignedCoursesByEmpID(BusinessUtility.GetInt(iInsertedEmpID), 0, (int)AssignedCourseInActiveReason.ByPensivoUtility , (int)AssignedCourseUpdatedSource.ByPensivoUtility);
                                    ErrorLog.createLog("Inserted New User " + BusinessUtility.GetString(iInsertedEmpID));
                                }
                                else
                                {
                                    if (isEmpOldData == true)
                                    {
                                        // Check Changed in Emp user Details
                                        if ((sOldDivisionID != sDivisionID) || (sOldJobID != sJobID) || (sOldLocation != sLocation) || (sOldType != sType)
                                            || (sOldTypeParsed != sTypeParsed) || (sOldDistrict != sDistrict) || (sOldRegionTBS != sRegionTBS) || (sOldDivisionTBS != sDivisionTBS)
                                            || (sOldSite != sSite) || (sOldProvinceAbbr != sProvinceAbbr))
                                        {
                                            ErrorLog.createLog("Changed In User Details " + BusinessUtility.GetString(iInsertedEmpID));
                                            objAssignedCourse.ResetEmployeeAssignedCoursesByEmpID(BusinessUtility.GetInt(iInsertedEmpID), 0, (int)AssignedCourseInActiveReason.ByPensivoUtility, (int)AssignedCourseUpdatedSource.ByPensivoUtility);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            iRowFailure += 1;
                            dbTransactionHelper.RollBackTransaction();
                            ErrorLog.CreateLog(e);
                            ErrorLog.createLog(e.Message + "  " + e.InnerException + " UUID :- " + sPensivoUID + " EmpHdr :-  " + sEmpID + " Emp First Name :- " + BusinessUtility.GetString(dRow["firstname"]) + " Emp Last Name :- " + BusinessUtility.GetString(dRow["lastname"]));
                        }
                        finally
                        {
                            dbTransactionHelper.CloseDatabaseConnection();
                        }
                    }

                    //#region UpdateEmployeeRefValueInTableEmpHeaderInfoForReport
                    //DbTransactionHelper dbTrans = new DbTransactionHelper();
                    //try
                    //{
                    //    dbTrans.BeginTransaction();
                    //    StringBuilder sbQuery = new StringBuilder();
                    //    sbQuery.Append(" SELECT empHdrID FROM empheader  where PensivoUUID = @PensivoUUID ");

                    //    object rValue = dbTrans.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                    //    DbUtility.GetParameter("PensivoUUID", BusinessUtility.GetInt(sPensivoUID), MyDbType.Int)});

                    //    if (BusinessUtility.GetInt(rValue) > 0)
                    //    {
                    //        StringBuilder sbInsertQuery = new StringBuilder();
                    //        sbInsertQuery.Append(" update emphdrinfo set ");
                    //        sbInsertQuery.Append(" empJobCode = fun_get_emp_ref_val('" + EmpRefCode.JobCode + "', @EmpID) , empDepartment = fun_get_emp_ref_val('" + EmpRefCode.Department + "', @EmpID) ,  ");
                    //        sbInsertQuery.Append(" empDistrict = fun_get_emp_ref_val('" + EmpRefCode.District + "', @EmpID) , empRegion = fun_get_emp_ref_val('" + EmpRefCode.Region + "', @EmpID) ,  ");
                    //        sbInsertQuery.Append(" empStore = fun_get_emp_ref_val('" + EmpRefCode.Store + "', @EmpID) ,  ");
                    //        sbInsertQuery.Append(" empDivision = fun_get_emp_ref_val('" + EmpRefCode.Division + "', @EmpID) , emptype =  fun_get_emp_ref_val('" + EmpRefCode.Type + "', @EmpID) ,  ");
                    //        sbInsertQuery.Append(" emptypeparse = fun_get_emp_ref_val('" + EmpRefCode.TypeParsed + "', @EmpID) ");
                    //        sbInsertQuery.Append(" WHERE  empHdrID = @EmpID ");

                    //        dbTrans.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    //        DbUtility.GetParameter("EmpID", BusinessUtility.GetInt( rValue), MyDbType.Int)
                    //        });
                    //    }
                    //    dbTrans.CommitTransaction();
                    //}
                    //catch (Exception e)
                    //{
                    //    iRowFailure += 1;
                    //    dbTrans.RollBackTransaction();
                    //    ErrorLog.createLog("Update Ref Error" + "  " + e.InnerException + " UUID :- " + sPensivoUID + " EmpHdr :-  " + sEmpID + " Emp First Name :- " + BusinessUtility.GetString(dRow["firstname"]) + " Emp Last Name :- " + BusinessUtility.GetString(dRow["lastname"]));
                    //}
                    //finally
                    //{
                    //    dbTrans.CloseDatabaseConnection();
                    //}
                    //#endregion

                }
            }
            return true;
        }

        /// <summary>
        /// To Save TBS Store Location in store_tax_loc Table
        /// </summary>
        /// <param name="dt">Pass TBS Store Location list as Datatable</param>
        /// <returns>True/False</returns>
        public bool ImportTBS_Tax_Store_location(DataTable dt)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" select count(*) from store_tax_loc ");
            try
            {
                dbTransactionHelper.BeginTransaction();
                object objEmpID = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, null);
                if (BusinessUtility.GetInt(objEmpID) <= 0)
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                        int id = BusinessUtility.GetInt(dRow["id"]);
                        string division = BusinessUtility.GetString(dRow["division"]);
                        string district = BusinessUtility.GetString(dRow["district"]);
                        string store_num = BusinessUtility.GetString(dRow["store_num"]);
                        string dept_id = BusinessUtility.GetString(dRow["dept_id"]);
                        string descr = BusinessUtility.GetString(dRow["descr"]);
                        string tax_loc = BusinessUtility.GetString(dRow["tax_loc"]);
                        string bdl_region_id = BusinessUtility.GetString(dRow["bdl_region_id"]);

                        #region InsertUpdateEmpHdrInfo
                        StringBuilder sbInsertQuery = new StringBuilder();

                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" insert into store_tax_loc  ");
                        sbInsertQuery.Append(" ( id, division, district, store_num, dept_id, descr, tax_loc, bdl_region_id)  ");
                        sbInsertQuery.Append(" values (@id, @division, @district, @store_num, @dept_id, @descr, @tax_loc, @bdl_region_id ) ");

                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("id", id, MyDbType.Int),    
                        DbUtility.GetParameter("division", division, MyDbType.String),
                        DbUtility.GetParameter("district", district, MyDbType.String),
                        DbUtility.GetParameter("store_num", store_num, MyDbType.String),
                        DbUtility.GetParameter("dept_id", dept_id, MyDbType.String),
                        DbUtility.GetParameter("descr", descr, MyDbType.String),
                        DbUtility.GetParameter("tax_loc", tax_loc, MyDbType.String),
                        DbUtility.GetParameter("bdl_region_id", bdl_region_id, MyDbType.String)
                        });
                        #endregion
                    }
                }
                dbTransactionHelper.CommitTransaction();
            }
            catch (Exception e)
            {
                ErrorLog.CreateLog(e);
                dbTransactionHelper.RollBackTransaction();
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }

            return true;
        }

        /// <summary>
        /// To Create Sysreference value If It Does Not Exist in sysempreferencecodes Table
        /// </summary>
        /// <param name="dbTransaction">Pass DB Transaction</param>
        /// <param name="refCode">Pass Reference Code</param>
        /// <param name="refCodeValue">Pass Reference Code Value</param>
        /// <returns>True/False</returns>
        public bool sysRefCreateNotExist(DbTransactionHelper dbTransaction, string refCode, string refCodeValue)
        {
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" Select sysRefCodeValue  from sysempreferencecodes as empRef Where ");
                sbQuery.Append(" empRef.sysRefCode = @refCode and sysRefCodeValue = @sysRefCodeValue  ");

                object rValue = dbTransaction.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("refCode", refCode, MyDbType.String),
                DbUtility.GetParameter("sysRefCodeValue", refCodeValue, MyDbType.String)
                });

                if (BusinessUtility.GetString(rValue) == "")
                {
                    if (refCodeValue != "")
                    {
                        StringBuilder sbQueryInsert = new StringBuilder();
                        sbQueryInsert.Append(" INSERT INTO sysempreferencecodes (sysRefCode, sysRefCodeValue, sysRefCodeActive, sysRefCodeText) ");
                        sbQueryInsert.Append(" VALUES (@sysRefCode, @sysRefCodeValue, @sysRefCodeActive, @sysRefCodeText) ");

                        dbTransaction.ExecuteNonQuery(BusinessUtility.GetString(sbQueryInsert), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("sysRefCode", refCode, MyDbType.String),
                        DbUtility.GetParameter("sysRefCodeValue", refCodeValue, MyDbType.String),
                        DbUtility.GetParameter("sysRefCodeActive", 1, MyDbType.Int),
                        DbUtility.GetParameter("sysRefCodeText", refCodeValue, MyDbType.String),
                        });
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                return false;
            }
            finally
            {
            }
        }
    }
}
