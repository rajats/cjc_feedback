﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Functionality,Users Assigned to Role
    /// </summary>
    public class Role
    {
        /// <summary>
        /// To Get/Set EmployeeID/User ID
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// To Get/Set Event ID
        /// </summary>
        public int EventID { get; set; }

        /// <summary>
        /// To Get/Set Role Action Name
        /// </summary>
        public string ActionName { get; set; }



        /// <summary>
        /// To Create Role in sysrolemaster Table
        /// </summary>
        /// <param name="roleName">Pass Role Name</param>
        /// <param name="createdBy">Pass Created by UserID/Employee ID</param>
        /// <returns>True/False</returns>
        public Boolean CreateRole(string roleName, int createdBy)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" INSERT INTO sysrolemaster (empRoleName, empRoleCreatedBy, empRoleCreatedOn, empRoleLastUpdatedOn, empRoleActive ) ");
                sbInsertQuery.Append(" VALUES (@empRoleName, @empRoleCreatedBy, @empRoleCreatedOn, @empRoleLastUpdatedOn, @empRoleActive  ) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empRoleName", roleName, MyDbType.String),    
                DbUtility.GetParameter("empRoleCreatedBy", createdBy, MyDbType.String),
                DbUtility.GetParameter("empRoleCreatedOn", DateTime.Now, MyDbType.DateTime),
                DbUtility.GetParameter("empRoleLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                DbUtility.GetParameter("empRoleActive", 1, MyDbType.Int)
                });
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// Update Role Name in SysRoleMaster Table
        /// </summary>
        /// <param name="roleName">Pass Role Name</param>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="updatedBy">Pass Updated by User ID</param>
        /// <returns>True/False</returns>
        public Boolean UpdateRoleName(string roleName, int roleID, int updatedBy)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" update sysrolemaster set empRoleName = @empRoleName, empRoleLastUpdatedBy =  @empRoleUpdatedBy,  empRoleLastUpdatedOn = @empRoleLastUpdatedOn where empRoleID = @roleID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empRoleName", roleName, MyDbType.String),    
                DbUtility.GetParameter("empRoleUpdatedBy", updatedBy, MyDbType.Int),
                DbUtility.GetParameter("empRoleLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Check if Role Already Exists or Not From sysrolemaster Table
        /// </summary>
        /// <param name="roleName">Pass Role Name</param>
        /// <returns>True/False</returns>
        public Boolean IsExistsRole(string roleName)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Select empRoleID from sysrolemaster Where empRoleName = @empRoleName ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empRoleName", roleName, MyDbType.String)
                });
                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Role List Available in The System with their member count From sysrolemaster and emprolesincludes Table
        /// </summary>
        /// <param name="roleName">Pass Role Name</param>
        /// <returns>Datatable</returns>
        public DataTable GetRoleList(string roleName)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbFinalQuery = new StringBuilder();
                StringBuilder sbQuery = new StringBuilder();
                StringBuilder sRoleQuery = new StringBuilder();
                StringBuilder sbInsertQuery = new StringBuilder();

                sRoleQuery.Append(" Select empRoleID, empRoleName, Count(rlInc.empHeader_empHdrID)  as membercount, empRoleLastUpdatedON   ");
                sRoleQuery.Append(" from sysrolemaster rlMst  ");
                sRoleQuery.Append(" Left JOIN emprolesincludes rlInc ON rlMst.empRoleID = rlInc.empRoles_empRoleID  ");
                sRoleQuery.Append("  where 1=1 and isSystemGeneratedRole = 0 and empRoleActive = 1 ");
                if (roleName != "")
                {
                    string[] arrRole = roleName.Split(' ');
                    int i = 0;
                    foreach (string sRole in arrRole)
                    {

                        if (i > 0)
                        {
                            sbQuery.Append(" union all");
                        }

                        sbQuery.Append(sRoleQuery);
                        sbQuery.Append(" and   empRoleName like '%" + sRole + "%' ");
                        sbQuery.Append("Group By empRoleID ");
                        i += 1;
                    }
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  as DisplaySeq  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sbQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by empRoleID   order by  empRoleID asc "); //count(*) desc,
                    //sbInsertQuery.Append("   and   empRoleName like '%" + roleName + "%' ");
                }
                else
                {
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  as DisplaySeq  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sRoleQuery);
                    sbFinalQuery.Append("Group By empRoleID ");
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by empRoleID   order by  empRoleID asc "); //by count(*) desc,
                }
                //sbInsertQuery.Append(" GROUP BY empRoleID  ");
                //DataTable dt = dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, null);
                DataTable dt = dbHelper.GetDataTable(BusinessUtility.GetString(sbFinalQuery), CommandType.Text, null);

                //sbInsertQuery = new StringBuilder();
                sRoleQuery = new StringBuilder();
                sRoleQuery.Append(" Select empRoleID, empRoleName from sysrolemaster where 1=1 and isSystemGeneratedRole = 0  and empRoleActive = 1  ");
                if (roleName != "")
                {
                    //sbInsertQuery.Append("   and   empRoleName like '%" + roleName + "%' ");
                    sbQuery = new StringBuilder();
                    string[] arrRole = roleName.Split(' ');
                    int i = 0;
                    foreach (string sRole in arrRole)
                    {

                        if (i > 0)
                        {
                            sbQuery.Append(" union all");
                        }

                        sbQuery.Append(sRoleQuery);
                        sbQuery.Append(" and   empRoleName like '%" + sRole + "%' ");
                        i += 1;
                    }
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  as DisplaySeq  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sbQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by empRoleID    "); //order by count(*) desc
                }
                else
                {
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*) as DisplaySeq  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sRoleQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by empRoleID    ");//order by count(*) desc
                }

                //DataTable dtAllRole = dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, null);
                DataTable dtAllRole = dbHelper.GetDataTable(BusinessUtility.GetString(sbFinalQuery), CommandType.Text, null);
                foreach (DataRow dRow in dtAllRole.Rows)
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" call sp_Get_EmpCount(" + BusinessUtility.GetInt(dRow["empRoleID"]) + ")   ");
                    object roleMemberCount = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, null);

                    DataRow[] HRow = dt.Select("empRoleID = " + BusinessUtility.GetInt(dRow["empRoleID"]) + "");
                    HRow[0]["membercount"] = BusinessUtility.GetInt(HRow[0]["membercount"]) + BusinessUtility.GetInt(roleMemberCount);
                    dt.AcceptChanges();
                }


                DataView dv = dt.DefaultView;
                dv.Sort = "DisplaySeq Desc, empRoleLastUpdatedON DESC";
                DataTable ResultDataTable = dv.ToTable();
                return ResultDataTable;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Role Name From sysrolemaster Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>string</returns>
        public string GetRoleName(int roleID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" Select empRoleName from sysrolemaster where empRoleID = @roleID  ");
                object rVal = dbHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });

                return BusinessUtility.GetString(rVal);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get User List Associated With The Role From emprolesincludes Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="searchBy">Pass Search by Reference Code</param>
        /// <param name="searchValue">Pass Search by Reference Code Value</param>
        /// <returns>Datatable</returns>
        public DataTable GetUsersInRole(int roleID, string searchBy, string searchValue)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select Distinct empHdr.empHdrID AS EmpID, empHdr.empFirstName, empHdr.empLastName, empHdr.empExtID from empheader	as empHdr ");
                sbQuery.Append(" INNER JOIN emprolesincludes	 as empRoleInc on empRoleInc.empHeader_empHdrID =  empHdr.empHdrID	AND empRoleInc.empRoles_empRoleID = @roleID ");
                if (searchBy == EmpSearchBy.Name)
                {
                    //sbQuery.Append(" AND ( empHdr.empFirstName like '%" + searchValue + "%' OR empHdr.empLastName like '%" + searchValue + "%' ) ");
                    StringBuilder sQuery = new StringBuilder();
                    sbQuery = new StringBuilder();
                    sQuery.Append(" select * from ( select Distinct empHdr.empHdrID AS EmpID, empHdr.empFirstName, empHdr.empLastName, empHdr.empExtID from empheader	as empHdr ");
                    sQuery.Append(" INNER JOIN emprolesincludes	 as empRoleInc on empRoleInc.empHeader_empHdrID =  empHdr.empHdrID	AND empRoleInc.empRoles_empRoleID = @roleID ) as result where 1=1 ");

                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND ( empFirstName = '" + searchValue + "' OR empLastName = '" + searchValue + "' OR  concat_ws(' ', trim(empFirstName), trim(empLastName)) = '" + searchValue + "'  OR  concat_ws(' ', trim(empFirstName), trim(empLastName), trim(empExtID)) = '" + searchValue + "'  OR empExtID = '" + searchValue + "') ");

                    string[] sEmpName = searchValue.Split(' ');
                    string empClause = "";

                    if (sEmpName.Length > 2)
                    {
                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ) ");


                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' and  empExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                    }
                    else if (sEmpName.Length == 2)
                    {

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "'  ) ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' AND  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                    }
                    else if (sEmpName.Length == 1)
                    {
                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ) ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[0]) + "' ");
                    }

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND (  ");

                    sEmpName = searchValue.Split(' ');
                    empClause = "";
                    foreach (string sName in sEmpName)
                    {
                        if (empClause == "")
                        {
                            empClause += " empFirstName = '" + sName + "' OR empLastName = '" + sName + "' OR  empExtID = '" + sName + "' ";
                        }
                        else
                        {
                            empClause += " OR empFirstName = '" + sName + "' OR empLastName = '" + sName + "' OR  empExtID = '" + sName + "' ";
                        }
                    }

                    sbQuery.Append(empClause);
                    sbQuery.Append(" ) ");

                    sbQuery.Append(" union ");

                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND (  ");

                    sEmpName = searchValue.Split(' ');
                    foreach (string sName in sEmpName)
                    {
                        if (empClause == "")
                        {
                            empClause += " empFirstName like '%" + sName + "%' OR empLastName  like '%" + sName + "%' OR  empExtID like '%" + sName + "%' ";
                        }
                        else
                        {
                            empClause += " OR empFirstName like '%" + sName + "%' OR empLastName  like '%" + sName + "%' OR  empExtID like '%" + sName + "%' ";
                        }
                    }

                    sbQuery.Append(empClause);
                    sbQuery.Append(" ) ");

                }
                else if (searchBy == EmpSearchBy.EmpCode)
                {
                    sbQuery.Append(" AND empHdr.empExtID = '" + searchValue + "' ");
                }
                else if (searchBy == EmpSearchBy.Store || searchBy == EmpSearchBy.Region || searchBy == EmpSearchBy.Division || searchBy == EmpSearchBy.JobCode)
                {
                    sbQuery.Append(" AND fun_get_emp_ref_val('" + searchBy + "', empHdr.empHdrID) = '" + searchValue + "' ");
                }

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get User List Not Associated With The Role From emprolesincludes Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="searchBy">Pass Search by Reference Code</param>
        /// <param name="searchValue">Pass Search by Reference Code Value</param>
        /// <returns>Datatable</returns>
        public DataTable GetUsersNotInRole(int roleID, string searchBy, string searchValue)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select Distinct empHdr.empHdrID AS EmpID, empHdr.empFirstName, empHdr.empLastName, empHdr.empExtID ");
                sbQuery.Append(" from empheader	as empHdr ");
                sbQuery.Append(" LEFT OUTER JOIN emprolesincludes as empRoleInc on empRoleInc.empHeader_empHdrID =  empHdr.empHdrID	AND empRoleInc.empRoles_empRoleID <> @roleID ");
                sbQuery.Append(" Where empHdr.empHdrID Not IN (SELECT empHeader_empHdrID FROM emprolesincludes where empRoles_empRoleID =@roleID ) ");

                if (searchBy == EmpSearchBy.Name)
                {
                    //sbQuery.Append(" AND ( empHdr.empFirstName like '%" + searchValue + "%' OR empHdr.empLastName like '%" + searchValue + "%' ) ");

                    StringBuilder sQuery = new StringBuilder();
                    sbQuery = new StringBuilder();
                    sQuery.Append(" select * from ( select Distinct empHdr.empHdrID AS EmpID, empHdr.empFirstName, empHdr.empLastName, empHdr.empExtID ");
                    sQuery.Append(" from empheader	as empHdr ");
                    sQuery.Append(" LEFT OUTER JOIN emprolesincludes as empRoleInc on empRoleInc.empHeader_empHdrID =  empHdr.empHdrID	AND empRoleInc.empRoles_empRoleID <> @roleID ");
                    sQuery.Append(" Where empHdr.empHdrID Not IN (SELECT empHeader_empHdrID FROM emprolesincludes where empRoles_empRoleID =@roleID ) ) as result where 1=1  ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND ( empFirstName = '" + searchValue + "' OR empLastName = '" + searchValue + "' OR  concat_ws(' ', trim(empFirstName), trim(empLastName)) = '" + searchValue + "'  OR  concat_ws(' ', trim(empFirstName), trim(empLastName), trim(empExtID)) = '" + searchValue + "'  OR empExtID = '" + searchValue + "') ");

                    string[] sEmpName = searchValue.Split(' ');
                    string empClause = "";

                    if (sEmpName.Length > 2)
                    {
                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ) ");


                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' and  empExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                    }
                    else if (sEmpName.Length == 2)
                    {

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "'  ) ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' AND  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                    }
                    else if (sEmpName.Length == 1)
                    {
                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ) ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[0]) + "' ");
                    }

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND (  ");

                    sEmpName = searchValue.Split(' ');
                    empClause = "";
                    foreach (string sName in sEmpName)
                    {
                        if (empClause == "")
                        {
                            empClause += " empFirstName = '" + sName + "' OR empLastName = '" + sName + "' OR  empExtID = '" + sName + "' ";
                        }
                        else
                        {
                            empClause += " OR empFirstName = '" + sName + "' OR empLastName = '" + sName + "' OR  empExtID = '" + sName + "' ";
                        }
                    }

                    sbQuery.Append(empClause);
                    sbQuery.Append(" ) ");

                    sbQuery.Append(" union ");

                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND (  ");

                    sEmpName = searchValue.Split(' ');
                    foreach (string sName in sEmpName)
                    {
                        if (empClause == "")
                        {
                            empClause += " empFirstName like '%" + sName + "%' OR empLastName  like '%" + sName + "%' OR  empExtID like '%" + sName + "%' ";
                        }
                        else
                        {
                            empClause += " OR empFirstName like '%" + sName + "%' OR empLastName  like '%" + sName + "%' OR  empExtID like '%" + sName + "%' ";
                        }
                    }

                    sbQuery.Append(empClause);
                    sbQuery.Append(" ) ");


                }
                else if (searchBy == EmpSearchBy.EmpCode)
                {
                    sbQuery.Append(" AND empHdr.empExtID = '" + searchValue + "' ");
                }
                else if (searchBy == EmpSearchBy.Store || searchBy == EmpSearchBy.Region || searchBy == EmpSearchBy.Division || searchBy == EmpSearchBy.JobCode)
                {
                    sbQuery.Append(" AND fun_get_emp_ref_val('" + searchBy + "', empHdr.empHdrID) = '" + searchValue + "' ");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Associate User With The Role in emprolesincludes Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lstRoleUsers">Pass List Employee/User ID </param>
        /// <returns>True/False</returns>
        public Boolean RoleAddUsers(int roleID, List<Role> lstRoleUsers)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                foreach (var lstUserRole in lstRoleUsers)
                {
                    StringBuilder sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" Insert into emprolesincludes (empRoles_empRoleID, empHeader_empHdrID) ");
                    sbInsertQuery.Append(" values (@roleID, @userID) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("userID", BusinessUtility.GetInt(lstUserRole.UserID), MyDbType.String)
                    });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Remove User Associated With The Role From emprolesincludes Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lstRoleUsers">Pass List Employee/User ID</param>
        /// <returns>True/False</returns>
        public Boolean RoleRemoveUsers(int roleID, List<Role> lstRoleUsers)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                foreach (var lstUserRole in lstRoleUsers)
                {
                    StringBuilder sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" delete from emprolesincludes Where empRoles_empRoleID = @roleID and empHeader_empHdrID = @userID ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("userID", BusinessUtility.GetInt(lstUserRole.UserID), MyDbType.String)
                    });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Course List Associated With The Role From emprolecourses and traningevent Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <param name="searchText">Pass Event Name To Search</param>
        /// <returns>Datatable</returns>
        public DataTable GetEventInRole(int roleID, string lang, string searchText)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbFinalQuery = new StringBuilder();
                StringBuilder sbQuery = new StringBuilder();
                StringBuilder sEventQuery = new StringBuilder();
                sEventQuery.Append(string.Format(" SELECT tEvnt.courseHdrID AS eventID, tEvnt.courseTitle{0} AS EventTitle,  evntType.type{0} AS EventType, 0 as Duration ", lang));
                sEventQuery.Append(" FROM traningevent AS tEvnt ");
                sEventQuery.Append(" INNER JOIN emprolecourses AS roleEvntInc ON roleEvntInc.courseHeader_courseHdrID = tEvnt.courseHdrID AND roleEvntInc.empRoles_empRoleID = @roleID AND tEvnt.courseActive =1 and tEvnt.isVersionActive =1 ");
                sEventQuery.Append(" INNER JOIN coursetype AS evntType ON evntType.type = tEvnt.courseType ");
                sEventQuery.Append(" where 1=1 ");

                if (searchText != "")
                {
                    //sbQuery.Append(string.Format(" AND tEvnt.courseTitle{0} like '%" + searchText + "%'", lang));
                    string[] arrEvent = searchText.Split(' ');
                    int i = 0;
                    foreach (string sEvent in arrEvent)
                    {

                        if (i > 0)
                        {
                            sbQuery.Append(" union all");
                        }

                        sbQuery.Append(sEventQuery);
                        sbQuery.Append(string.Format(" AND tEvnt.courseTitle{0} like '%" + sEvent + "%' ", lang));
                        i += 1;
                    }
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sbQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by EventID   order by count(*) desc ");
                }
                else
                {
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sEventQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by EventID   order by count(*) desc ");
                }
                //return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                //DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                //});
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbFinalQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Course List Not Associated With The Role From emprolecourses and emprolecourses Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <param name="searchText">Pass Training Event Name</param>
        /// <returns>Datatable</returns>
        public DataTable GetEventNotInRole(int roleID, string lang, string searchText)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbFinalQuery = new StringBuilder();
                StringBuilder sbQuery = new StringBuilder();
                StringBuilder sEventQuery = new StringBuilder();

                sEventQuery.Append(string.Format(" SELECT DISTINCT tEvnt.courseHdrID AS eventID, tEvnt.courseTitle{0} AS EventTitle,  evntType.type{0} AS EventType, 0 as Duration ", lang));
                sEventQuery.Append(" FROM traningevent AS tEvnt ");
                sEventQuery.Append(" LEFT OUTER JOIN emprolecourses AS roleEvntInc ON roleEvntInc.courseHeader_courseHdrID = tEvnt.courseHdrID AND roleEvntInc.empRoles_empRoleID <> @roleID AND tEvnt.courseActive =1 ");
                sEventQuery.Append(" INNER JOIN coursetype AS evntType ON evntType.type = tEvnt.courseType ");
                sEventQuery.Append(" WHERE tEvnt.courseHdrID Not IN (SELECT courseHeader_courseHdrID FROM emprolecourses where empRoles_empRoleID =@roleID ) and tEvnt.isVersionActive =1 ");
                sEventQuery.Append(" AND tEvnt.testType <> '" + CourseTestType.TestOnly + "' ");
                if (searchText != "")
                {
                    //sbQuery.Append(string.Format(" AND tEvnt.courseTitle{0} like '%" + searchText + "%'", lang));

                    string[] arrEvent = searchText.Split(' ');
                    int i = 0;
                    foreach (string sEvent in arrEvent)
                    {

                        if (i > 0)
                        {
                            sbQuery.Append(" union all");
                        }

                        sbQuery.Append(sEventQuery);
                        sbQuery.Append(string.Format(" AND tEvnt.courseTitle{0} like '%" + sEvent + "%' ", lang));
                        i += 1;
                    }
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sbQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by EventID   order by count(*) desc ");
                }
                else
                {
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sEventQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by EventID   order by count(*) desc ");
                }
                //return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                //DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                //});

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbFinalQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Associate Course With The Role in emprolecourses Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lstRoleEvent">Pass Training Event ID List</param>
        /// <returns>True/False</returns>
        public Boolean RoleAddEvent(int roleID, List<Role> lstRoleEvent)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                foreach (var lstUserRole in lstRoleEvent)
                {
                    StringBuilder sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" Insert into emprolecourses (empRoles_empRoleID, courseHeader_courseHdrID) ");
                    sbInsertQuery.Append(" values (@roleID, @eventID) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("eventID", BusinessUtility.GetInt(lstUserRole.EventID), MyDbType.Int)
                    });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Remove Course Associated With The Role From emprolecourses Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lstRoleEvent">Pass Training Event ID List</param>
        /// <returns>True/False</returns>
        public Boolean RoleRemoveEvent(int roleID, List<Role> lstRoleEvent)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                foreach (var lstUserRole in lstRoleEvent)
                {
                    StringBuilder sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" delete from emprolecourses Where empRoles_empRoleID = @roleID and courseHeader_courseHdrID = @eventID ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("eventID", BusinessUtility.GetInt(lstUserRole.EventID), MyDbType.String)
                    });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Functionality Associated With The Role From sysroletypeactionitems and emproletypes Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>True/False</returns>
        public DataTable GetFunctionalityInRole(int roleID, string lang)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(string.Format(" SELECT RoleType, ActionItem, ActionItemDesc{0} AS ItemName FROM sysroletypeactionitems as actItem ", lang));
                sbQuery.Append(" INNER JOIN emproletypes as incRole ON incRole.empRoles_ActionItem =  actItem.ActionItem ");
                sbQuery.Append(" where incRole.empRoles_empRoleID =@roleID ");

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Functionality Not Associated With The Role From sysroletypeactionitems Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>Datatable</returns>
        public DataTable GetFunctionalityNotInRole(int roleID, string lang)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(string.Format(" SELECT DISTINCT RoleType, ActionItem, ActionItemDesc{0} AS ItemName FROM sysroletypeactionitems as actItem ", lang));
                sbQuery.Append(" where ( actItem.ActionItem NOT IN(select empRoles_ActionItem FROM emproletypes where empRoles_empRoleID = @roleID) OR RoleType = 'M') ");

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Associate Functionality With The Role From emproletypes Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="functionalityID">Pass Functionality ID</param>
        /// <param name="functionalityType">Pass Functionality Type</param>
        /// <returns>True/False</returns>
        public Boolean RoleAddFunctionality(int roleID, int functionalityID, string functionalityType)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Insert into emproletypes (empRoles_empRoleID, empRoles_RoleType, empRoles_ActionItem) ");
                sbInsertQuery.Append(" values (@empRoles_empRoleID, @empRoles_RoleType, @empRoles_ActionItem) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empRoles_empRoleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("empRoles_RoleType", functionalityType , MyDbType.String),
                    DbUtility.GetParameter("empRoles_ActionItem", functionalityID , MyDbType.Int)
                    });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Remove Functionality Associated With The Role From emproletypes Table 
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="functionalityID">Pass Functionality ID</param>
        /// <param name="functionalityType">Pass Functionality Type</param>
        /// <returns>True/False</returns>
        public Boolean RoleRemoveFunctionality(int roleID, int functionalityID, string functionalityType)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Delete from emproletypes where empRoles_empRoleID = @empRoles_empRoleID AND empRoles_RoleType = @empRoles_RoleType AND empRoles_ActionItem = @empRoles_ActionItem ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empRoles_empRoleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("empRoles_RoleType", functionalityType , MyDbType.String),
                    DbUtility.GetParameter("empRoles_ActionItem", functionalityID , MyDbType.Int)
                    });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Check if Functionality is Associated With The Role or Not From emproletypes Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="functionalityID">Pass Functionality ID</param>
        /// <param name="functionalityType">Pass Functionality Type</param>
        /// <returns>True/False</returns>
        public Boolean ActionExistsInRole(int roleID, int functionalityID, string functionalityType)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT empRoles_ActionItem ");
                sbInsertQuery.Append(" FROM emproletypes ");
                sbInsertQuery.Append(" WHERE empRoles_empRoleID = @empRoles_empRoleID AND empRoles_ActionItem = @empRoles_ActionItem AND empRoles_RoleType = @empRoles_RoleType ");

                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empRoles_empRoleID", roleID, MyDbType.Int),
                DbUtility.GetParameter("empRoles_ActionItem", functionalityID, MyDbType.Int),
                DbUtility.GetParameter("empRoles_RoleType", functionalityType, MyDbType.String)
                });
                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }

        }

        /// <summary>
        /// To Get Functionality Detail From sysroletypeactionitems Table
        /// </summary>
        /// <param name="actionID">Pass Action ID</param>
        /// <param name="lang">Pass Language Code</param>
        public void GetActionDetail(int actionID, string lang)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(string.Format(" select ActionItem, ActionItemDesc{0} as FunctionalityName  from sysroletypeactionitems ", lang));
            if (actionID > 0)
            {
                sbQuery.Append(" WHERE ActionItem = " + actionID + " ");
            }

            MySqlDataReader objDr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                objDr = dbHelp.GetDataReader(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, null);
                while (objDr.Read())
                {
                    this.ActionName = BusinessUtility.GetString(objDr["FunctionalityName"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To get user list that not having Report/User Management From emprolereportusers and emproleusermgmtusers Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="searchBy">Pass Search By Reference Code</param>
        /// <param name="searchValue">Pass Search By Reference Code Value</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <returns>Datatable</returns>
        public DataTable GetUsersNotInReportUserManagment(int roleID, string searchBy, string searchValue, int actionID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select Distinct empHdr.empHdrID AS EmpID, empHdr.empFirstName, empHdr.empLastName, empHdr.empExtID ");
                sbQuery.Append(" from empheader	as empHdr ");
                if (actionID == 4)
                {
                    sbQuery.Append(" LEFT OUTER JOIN emprolereportusers as empRoleInc on empRoleInc.emp_headerID =  empHdr.empHdrID	AND empRoleInc.emp_RoleID <> @roleID ");
                    sbQuery.Append(" Where empHdr.empHdrID Not IN (SELECT emp_headerID FROM emprolereportusers where emp_RoleID =@roleID ) ");
                }
                else
                {
                    sbQuery.Append(" LEFT OUTER JOIN emproleusermgmtusers as empRoleInc on empRoleInc.emp_headerID =  empHdr.empHdrID	AND empRoleInc.emp_RoleID <> @roleID ");
                    sbQuery.Append(" Where empHdr.empHdrID Not IN (SELECT emp_headerID FROM emproleusermgmtusers where emp_RoleID =@roleID ) ");
                }


                if (searchBy == EmpSearchBy.Name)
                {
                    sbQuery.Append(" AND ( empHdr.empFirstName like '%" + searchValue + "%' OR empHdr.empLastName like '%" + searchValue + "%' ) ");
                }
                else if (searchBy == EmpSearchBy.EmpCode)
                {
                    sbQuery.Append(" AND empHdr.empExtID = '" + searchValue + "' ");
                }
                else if (searchBy == EmpSearchBy.Store || searchBy == EmpSearchBy.Region || searchBy == EmpSearchBy.Division || searchBy == EmpSearchBy.JobCode)
                {
                    sbQuery.Append(" AND fun_get_emp_ref_val('" + searchBy + "', empHdr.empHdrID) = '" + searchValue + "' ");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get User List Having Access to Report/User Management From emprolereportusers and emproleusermgmtusers Table 
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="searchBy">Pass Search By Reference Code </param>
        /// <param name="searchValue">Pass Search By Reference Code Value </param>
        /// <param name="actionID">Pass Action ID</param>
        /// <returns>Datatable</returns>
        public DataTable GetUsersInReportUserManagment(int roleID, string searchBy, string searchValue, int actionID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select Distinct empHdr.empHdrID AS EmpID, empHdr.empFirstName, empHdr.empLastName, empHdr.empExtID from empheader	as empHdr ");
                if (actionID == 4)
                {
                    sbQuery.Append(" INNER JOIN emprolereportusers	 as empRoleInc on empRoleInc.emp_headerID =  empHdr.empHdrID	AND empRoleInc.emp_RoleID = @roleID ");
                }
                else
                {
                    sbQuery.Append(" INNER JOIN emproleusermgmtusers as empRoleInc on empRoleInc.emp_headerID =  empHdr.empHdrID	AND empRoleInc.emp_RoleID = @roleID ");
                }

                sbQuery.Append(" Where 1=1  ");
                if (searchBy == EmpSearchBy.Name)
                {
                    sbQuery.Append(" AND ( empHdr.empFirstName like '%" + searchValue + "%' OR empHdr.empLastName like '%" + searchValue + "%' ) ");
                }
                else if (searchBy == EmpSearchBy.EmpCode)
                {
                    sbQuery.Append(" AND empHdr.empExtID = '" + searchValue + "' ");
                }
                else if (searchBy == EmpSearchBy.Store || searchBy == EmpSearchBy.Region || searchBy == EmpSearchBy.Division || searchBy == EmpSearchBy.JobCode)
                {
                    sbQuery.Append(" AND fun_get_emp_ref_val('" + searchBy + "', empHdr.empHdrID) = '" + searchValue + "' ");
                }

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Add User in Report/User Management in emprolereportusers and emproleusermgmtusers Table
        /// </summary>
        /// <param name="roleID">Pass Role ID </param>
        /// <param name="userID">Pass Employee/User ID</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <returns>True/False</returns>
        public Boolean AddUsersInReportUserManagment(int roleID, int userID, int actionID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                if (actionID == 4)
                {
                    sbInsertQuery.Append(" Insert into emprolereportusers (emp_roleID, emp_headerID) ");
                }
                else
                {
                    sbInsertQuery.Append(" Insert into emproleusermgmtusers (emp_roleID, emp_headerID) ");
                }
                sbInsertQuery.Append(" values (@roleID, @userID) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("userID", userID, MyDbType.String)
                    });
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Remove User From Report/User Management From emprolereportusers and emproleusermgmtusers Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="userID">Pass Employee/User ID</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <returns>True/False</returns>
        public Boolean RemoveUsersInReportUserManagment(int roleID, int userID, int actionID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" delete from  ");
                if (actionID == 4)
                {
                    sbInsertQuery.Append(" emprolereportusers ");
                }
                else
                {
                    sbInsertQuery.Append(" emproleusermgmtusers ");
                }
                sbInsertQuery.Append(" Where emp_roleID = @roleID and emp_headerID = @userID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("userID", userID, MyDbType.String)
                    });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get User List Which Don't Have Access to Report/User Management From sysrolereportrefsel and sysroleusermgmtrefsel Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="refCode">Pass Reference Code</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <param name="searchText">Pass Reference Code Value</param>
        /// <returns>Datatable</returns>
        public DataTable GetSysRefNotInReportUserManagment(int roleID, string refCode, int actionID, string searchText)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" Select empRef.sysRefCode, empRef.sysRefCodeValue, empRef.sysRefCodeText  from sysempreferencecodes as empRef Where ");
                sbQuery.Append(" empRef.sysRefCode = @refCode ");
                sbQuery.Append(" AND empRef.sysRefCodeValue NOT IN (   ");

                sbQuery.Append(" Select sys_ref_value from ");
                if (actionID == (int)RoleAction.Reporting)
                {
                    sbQuery.Append(" sysrolereportrefsel ");
                }
                else
                {
                    sbQuery.Append(" sysroleusermgmtrefsel ");
                }
                sbQuery.Append(" where sys_ref_Code = @refCode AND emp_RoleID =@roleID ) ");



                if (searchText != "")
                {
                    sbQuery.Append(" and empRef.sysRefCodeValue like '%" + searchText + "%'");
                }

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int),
                DbUtility.GetParameter("refCode", refCode, MyDbType.String)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get User sysRefCode Value of ActionID of Role From sysrolereportrefsel and sysroleusermgmtrefsel Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="refCode">Pass Reference Code</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <param name="searchText">Pass Reference Code Value</param>
        /// <returns>Datatable</returns>
        public DataTable GetSysRefInReportUserManagment(int roleID, string refCode, int actionID, string searchText)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select empRef.sysRefCode, empRef.sysRefCodeValue, empRef.sysRefCodeText from  ");
                if (actionID == (int)RoleAction.Reporting)
                {
                    sbQuery.Append(" sysrolereportrefsel  as refSel");
                }
                else
                {
                    sbQuery.Append(" sysroleusermgmtrefsel  as refSel");
                }
                sbQuery.Append(" INNER JOIN sysempreferencecodes as empRef ON empRef.sysRefCode = refSel.sys_ref_Code AND empRef.sysRefCodeValue =  refSel.sys_ref_value ");
                sbQuery.Append(" where refSel.sys_ref_Code = @refCode AND refSel.emp_RoleID =@roleID ");

                if (searchText != "")
                {
                    sbQuery.Append(" and empRef.sysRefCodeValue like '%" + searchText + "%'");
                }

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int),
                DbUtility.GetParameter("refCode", refCode, MyDbType.String)
                });

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To add user in report/user managment in sysrolereportrefsel and sysroleusermgmtrefsel Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="refCode">Pass Reference Code</param>
        /// <param name="refCodeValue">Pass Reference Code Value</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <returns>True/False</returns>
        public Boolean AddSysRefInReportUserManagment(int roleID, string refCode, string refCodeValue, int actionID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                string[] sRefCodeVal = refCodeValue.Split(',');
                foreach (string sRefValue in sRefCodeVal)
                {
                    StringBuilder sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" INSERT INTO  ");
                    if (actionID == (int)RoleAction.Reporting)
                    {
                        sbInsertQuery.Append(" sysrolereportrefsel  ");
                    }
                    else
                    {
                        sbInsertQuery.Append(" sysroleusermgmtrefsel  ");
                    }

                    sbInsertQuery.Append(" (emp_roleid, sys_ref_code, sys_ref_value ) ");
                    sbInsertQuery.Append(" VALUES (@emp_roleid, @sys_ref_code, @sys_ref_value) ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("emp_roleid", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("sys_ref_code", refCode , MyDbType.String),
                    DbUtility.GetParameter("sys_ref_value", sRefValue , MyDbType.String)
                    });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To remove user from report/user managment From sysrolereportrefsel and sysroleusermgmtrefsel Table
        /// </summary>
        /// <param name="roleID">Pass Role ID </param>
        /// <param name="refCode">Pass Reference Code</param>
        /// <param name="refCodeValue">Pass Reference Code Value</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <returns>True/False</returns>
        public Boolean RemoveSysRefInReportUserManagment(int roleID, string refCode, string refCodeValue, int actionID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                string[] sRefCodeVal = refCodeValue.Split(',');
                foreach (string sRefValue in sRefCodeVal)
                {
                    StringBuilder sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" DELETE FROM ");
                    if (actionID == (int)RoleAction.Reporting)
                    {
                        sbInsertQuery.Append(" sysrolereportrefsel  ");
                    }
                    else
                    {
                        sbInsertQuery.Append(" sysroleusermgmtrefsel  ");
                    }
                    sbInsertQuery.Append(" Where emp_roleid = @emp_roleid AND sys_ref_code = @sys_ref_code AND sys_ref_value = @sys_ref_value ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("emp_roleid", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("sys_ref_code", refCode , MyDbType.String),
                    DbUtility.GetParameter("sys_ref_value", sRefValue , MyDbType.String)
                    });
                }
                dbTransactionHelper.CommitTransaction();
                return true;

            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// To Get User Action/Functionality List From sysrolerefsel and empreference Table
        /// </summary>
        /// <param name="empID">Pass Employee/User ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <param name="userType">Pass User Type</param>
        /// <param name="siteValue">Pass Site Value</param>
        /// <returns>DataTable</returns>
        public DataTable GetRoleFunctionality(int empID, string lang, string userType, string siteValue)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select * from (   ");

                sbQuery.Append(string.Format(" select Distinct eItem.ActionItem, ActionItemDesc{0} Title, eItem.ActionUrl ", lang));
                sbQuery.Append(" FROM sysroletypeactionitems AS eItem ");
                if (CurrentEmployee.EmpType != EmpType.SuperAdmin)
                {
                    sbQuery.Append(" Inner JOIN emproletypes AS eType ON  eType.empRoles_ActionItem = eItem.ActionItem AND eType.empRoles_empRoleID IN( select empRoles_empRoleID from emprolesincludes where empHeader_empHdrID =@empID)  ");
                    sbQuery.Append(" and eItem.ActionItem  Not IN( ");
                    sbQuery.Append(" SELECT  ");
                    sbQuery.Append(" eItem.ActionItem ");
                    sbQuery.Append(" FROM ");
                    sbQuery.Append(" sysroletypeactionitems AS eItem ");
                    sbQuery.Append(" INNER JOIN empmstlistexcl AS eType ON eType.functionalityID = eItem.ActionItem ");
                    sbQuery.Append(" AND empListexclEmpID = @empID  and sysRefCodeValue =@siteValue   ");
                    sbQuery.Append(" )    ");

                    sbQuery.Append(" union  ");
                    sbQuery.Append(string.Format(" select Distinct eItem.ActionItem, ActionItemDesc{0} Title, eItem.ActionUrl ", lang));
                    sbQuery.Append(" FROM  sysrolerefsel AS syroleSel ");
                    sbQuery.Append(" JOIN ");
                    sbQuery.Append(" empreference eRef ON eRef.sysEmpReferenceCodes_sysRefCode = syroleSel.sys_ref_code ");
                    sbQuery.Append(" AND eRef.empHeader_empHdrID = @empID   ");
                    sbQuery.Append(" AND (syroleSel.sys_cond IS NULL ");
                    sbQuery.Append(" OR syroleSel.sys_cond = 'OR') ");
                    sbQuery.Append(" AND  ");
                    sbQuery.Append(" (        ");
                    sbQuery.Append(" ( ");
                    sbQuery.Append(" (SELECT  ");
                    sbQuery.Append(" COUNT(*) ");
                    sbQuery.Append(" FROM ");
                    sbQuery.Append(" sysrolerefsel AS AND1syroleSel ");
                    sbQuery.Append(" JOIN ");
                    sbQuery.Append(" empreference eRef1 ON eRef1.sysEmpReferenceCodes_sysRefCode = AND1syroleSel.sys_ref_code ");
                    sbQuery.Append(" AND eRef1.empHeader_empHdrID =    @empID   ");
                    sbQuery.Append(" AND AND1syroleSel.sys_cond = 'AND' AND AND1syroleSel.sys_cond IS NOT NULL ");
                    sbQuery.Append(" AND AND1syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(AND1syroleSel.sys_ref_code, ");
                    sbQuery.Append(" eRef1.empHeader_empHdrID) where AND1syroleSel.emp_roleid = syroleSel.emp_roleid) =  ( select count(*)         FROM  			sysrolerefsel as AND2syroleSel  						WHERE ");
                    sbQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                    sbQuery.Append(" AND AND2syroleSel.sys_cond = 'AND')  ");
                    sbQuery.Append(" AND (SELECT  ");
                    sbQuery.Append(" COUNT(*) ");
                    sbQuery.Append(" FROM ");
                    sbQuery.Append(" sysrolerefsel as AND2syroleSel ");
                    sbQuery.Append(" WHERE ");
                    sbQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                    sbQuery.Append(" AND AND2syroleSel.sys_cond = 'AND') > 0 ");
                    sbQuery.Append(" ) ");
                    sbQuery.Append(" OR  ");
                    sbQuery.Append(" ( ");
                    sbQuery.Append(" (SELECT  ");
                    sbQuery.Append(" COUNT(*) ");
                    sbQuery.Append(" FROM ");
                    sbQuery.Append(" sysrolerefsel as ORsyroleSel ");
                    sbQuery.Append(" WHERE ");
                    sbQuery.Append(" ORsyroleSel.emp_roleid =  syroleSel.emp_roleid ");
                    sbQuery.Append(" AND ORsyroleSel.sys_cond = 'AND') = 0 ");
                    sbQuery.Append(" )           ");
                    sbQuery.Append(" ) ");
                    sbQuery.Append(" AND (syroleSel.sys_ref_value = fun_get_emp_ref_val(syroleSel.sys_ref_code, ");
                    sbQuery.Append(" empHeader_empHdrID)) ");
                    sbQuery.Append(" Inner JOIN emproletypes AS eType ON  eType.empRoles_empRoleID = syroleSel.emp_roleid ");
                    sbQuery.Append(" inner join sysroletypeactionitems AS eItem ON eType.empRoles_ActionItem = eItem.ActionItem ");

                    sbQuery.Append(" and eItem.ActionItem  Not IN(   ");
                    sbQuery.Append(" SELECT  ");
                    sbQuery.Append(" eItem.ActionItem ");
                    sbQuery.Append(" FROM ");
                    sbQuery.Append(" sysroletypeactionitems AS eItem ");
                    sbQuery.Append(" INNER JOIN empmstlistexcl AS eType ON eType.functionalityID = eItem.ActionItem ");
                    sbQuery.Append(" AND empListexclEmpID = @empID  and sysRefCodeValue =@siteValue   ");
                    sbQuery.Append(" ) ");

                    sbQuery.Append(" UNION ");
                    sbQuery.Append(" SELECT ");
                    sbQuery.Append(" eItem.ActionItem, ActionItemDescfr Title, eItem.ActionUrl ");
                    sbQuery.Append(" FROM ");
                    sbQuery.Append(" sysroletypeactionitems AS eItem ");
                    sbQuery.Append(" INNER JOIN ");
                    sbQuery.Append(" empmstlistincl AS eType ON eType.functionalityID = eItem.ActionItem   and empListInclEmpID = @empID  "); //and sysRefCodeValue =@siteValue

                    if (this.empHasReporting(empID) > 0)
                    {
                        sbQuery.Append(" UNION ");
                        sbQuery.Append(" SELECT  ");
                        sbQuery.Append(" eItem.ActionItem, ActionItemDescfr Title, eItem.ActionUrl ");
                        sbQuery.Append(" FROM ");
                        sbQuery.Append(" sysroletypeactionitems AS eItem where eItem.actionitem=4 ");
                    }
                }

                sbQuery.Append(" ) as Result order by Title ");
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empID", empID, MyDbType.Int),
                DbUtility.GetParameter("siteValue", siteValue, MyDbType.String)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// To Get Employee Has Reporting
        /// </summary>
        /// <param name="empid">Pass Employee ID</param>
        /// <returns>Int</returns>
        private int empHasReporting(int empid)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select count(*) from employeehasfunctionality where empHdrID =@empID and reporting =1  ");
                object obj = dbHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empID", empid, MyDbType.Int)
                });
                return BusinessUtility.GetInt(obj);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }






        /// <summary>
        /// To Check If User Belongs To Action/Functionality or Not From sysroletypeactionitems, sysrolerefsel and empreference Table
        /// </summary>
        /// <param name="empID">Pass Employee/User ID</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <param name="siteValue">Pass Site Value</param>
        /// <returns>True/False</returns>
        public Boolean GetUserHasFunctionality(int empID, int actionID, string siteValue)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select  eItem.ActionItem ");
                sbQuery.Append(" FROM sysroletypeactionitems AS eItem ");
                sbQuery.Append(" Inner JOIN emproletypes AS eType ON  eType.empRoles_ActionItem = eItem.ActionItem AND eType.empRoles_empRoleID IN( select empRoles_empRoleID from emprolesincludes where empHeader_empHdrID =@empID and empHeader_empHdrID NOT IN(select empListExclEmpID from empmstlistexcl where functionalityID = @actionID and empListExclEmpID = @empID and sysRefCodeValue = @siteValue )) ");
                sbQuery.Append(" Where eItem.ActionItem = @actionID ");
                sbQuery.Append(" union  ");
                sbQuery.Append(" select eItem.ActionItem  ");
                sbQuery.Append(" FROM  sysrolerefsel AS syroleSel ");
                sbQuery.Append(" JOIN ");
                sbQuery.Append(" empreference eRef ON eRef.sysEmpReferenceCodes_sysRefCode = syroleSel.sys_ref_code ");
                sbQuery.Append(" AND eRef.empHeader_empHdrID = @empID   ");
                sbQuery.Append(" and eRef.empHeader_empHdrID NOT IN(select empListExclEmpID from empmstlistexcl where functionalityID = @actionID and empListExclEmpID = @empID and sysRefCodeValue = @siteValue ) ");
                sbQuery.Append(" AND (syroleSel.sys_cond IS NULL ");
                sbQuery.Append(" OR syroleSel.sys_cond = 'OR') ");
                sbQuery.Append(" AND  ");
                sbQuery.Append(" (        ");
                sbQuery.Append(" ( ");
                sbQuery.Append(" (SELECT  ");
                sbQuery.Append(" COUNT(*) ");
                sbQuery.Append(" FROM ");
                sbQuery.Append(" sysrolerefsel AS AND1syroleSel ");
                sbQuery.Append(" JOIN ");
                sbQuery.Append(" empreference eRef1 ON eRef1.sysEmpReferenceCodes_sysRefCode = AND1syroleSel.sys_ref_code ");
                sbQuery.Append(" AND eRef1.empHeader_empHdrID =    @empID   ");
                sbQuery.Append(" and eRef1.empHeader_empHdrID NOT IN(select empListExclEmpID from empmstlistexcl where functionalityID = @actionID and empListExclEmpID = @empID  and sysRefCodeValue = @siteValue  ) ");
                sbQuery.Append(" AND AND1syroleSel.sys_cond = 'AND' AND AND1syroleSel.sys_cond IS NOT NULL ");
                sbQuery.Append(" AND AND1syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(AND1syroleSel.sys_ref_code, ");
                sbQuery.Append(" eRef1.empHeader_empHdrID) where AND1syroleSel.emp_roleid = syroleSel.emp_roleid) =  ( select count(*)         FROM  			sysrolerefsel as AND2syroleSel  						WHERE ");
                sbQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbQuery.Append(" AND AND2syroleSel.sys_cond = 'AND')  ");
                sbQuery.Append(" AND (SELECT  ");
                sbQuery.Append(" COUNT(*) ");
                sbQuery.Append(" FROM ");
                sbQuery.Append(" sysrolerefsel as AND2syroleSel ");
                sbQuery.Append(" WHERE ");
                sbQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbQuery.Append(" AND AND2syroleSel.sys_cond = 'AND') > 0 ");
                sbQuery.Append(" ) ");
                sbQuery.Append(" OR  ");
                sbQuery.Append(" ( ");
                sbQuery.Append(" (SELECT  ");
                sbQuery.Append(" COUNT(*) ");
                sbQuery.Append(" FROM ");
                sbQuery.Append(" sysrolerefsel as ORsyroleSel ");
                sbQuery.Append(" WHERE ");
                sbQuery.Append(" ORsyroleSel.emp_roleid =  syroleSel.emp_roleid ");
                sbQuery.Append(" AND ORsyroleSel.sys_cond = 'AND') = 0 ");
                sbQuery.Append(" )           ");
                sbQuery.Append(" ) ");
                sbQuery.Append(" AND (syroleSel.sys_ref_value = fun_get_emp_ref_val(syroleSel.sys_ref_code, ");
                sbQuery.Append(" empHeader_empHdrID)) ");
                sbQuery.Append(" Inner JOIN emproletypes AS eType ON  eType.empRoles_empRoleID = syroleSel.emp_roleid ");
                sbQuery.Append(" inner join sysroletypeactionitems AS eItem ON eType.empRoles_ActionItem = eItem.ActionItem ");
                sbQuery.Append(" Where eItem.ActionItem = @actionID ");
                sbQuery.Append(" UNION ");
                sbQuery.Append(" SELECT ");
                sbQuery.Append(" eItem.ActionItem ");
                sbQuery.Append(" FROM ");
                sbQuery.Append(" sysroletypeactionitems AS eItem ");
                sbQuery.Append(" INNER JOIN ");
                sbQuery.Append(" empmstlistincl AS eType ON eType.functionalityID = eItem.ActionItem and eItem.ActionItem = @actionID  and empListInclEmpID = @empID   ");//and sysRefCodeValue = @siteValue 


                if (actionID == 4)
                {
                    if (this.empHasReporting(empID) > 0)
                    {
                        sbQuery.Append(" UNION ");
                        sbQuery.Append(" SELECT  ");
                        sbQuery.Append(" eItem.ActionItem ");
                        sbQuery.Append(" FROM ");
                        sbQuery.Append(" sysroletypeactionitems AS eItem where eItem.actionitem=@actionID ");
                    }
                }

                object objRvalue = dbHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empID", empID, MyDbType.Int),
                DbUtility.GetParameter("actionID", actionID, MyDbType.Int),
                DbUtility.GetParameter("siteValue", siteValue, MyDbType.String),
                });

                if (BusinessUtility.GetInt(objRvalue) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get SysEmpRef List Belonging to The Role From sysrolerefsel and sysempreferencecodes Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="refCode">Pass Reference Code</param>
        /// <param name="searchText">Pass Reference Code Value</param>
        /// <returns>Datatable</returns>
        public DataTable GetSysRefInRole(int roleID, string refCode, string searchText)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select empRef.sysRefCode, empRef.sysRefCodeValue, empRef.sysRefCodeText from sysrolerefsel as refSel ");
                sbQuery.Append(" INNER JOIN sysempreferencecodes as empRef ON empRef.sysRefCode = refSel.sys_ref_Code AND empRef.sysRefCodeValue =  refSel.sys_ref_value ");
                sbQuery.Append(" where refSel.sys_ref_Code = @refCode AND refSel.emp_RoleID =@roleID ");

                if (searchText != "")
                {
                    sbQuery.Append(" and empRef.sysRefCodeValue like '%" + searchText + "%'");
                }

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int),
                DbUtility.GetParameter("refCode", refCode, MyDbType.String)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Check if sysEmpRef List Belongs to The Role or Not From sysempreferencecodes Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="refCode">Pass Reference Code</param>
        /// <param name="searchText">Pass Reference Code Value</param>
        /// <returns>Datatable</returns>
        public DataTable GetSysRefNotInRole(int roleID, string refCode, string searchText)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" Select empRef.sysRefCode, empRef.sysRefCodeValue, empRef.sysRefCodeText  from sysempreferencecodes as empRef Where ");
                sbQuery.Append(" empRef.sysRefCode = @refCode AND empRef.sysRefCodeValue NOT IN ( Select sys_ref_value from sysrolerefsel where sys_ref_Code = @refCode AND emp_RoleID =@roleID ) ");
                if (searchText != "")
                {
                    sbQuery.Append(" and empRef.sysRefCodeValue like '%" + searchText + "%'");
                }

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int),
                DbUtility.GetParameter("refCode", refCode, MyDbType.String)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Add sysEmpRef to Role into sysrolerefsel Table 
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="refCode">Pass Reference Code</param>
        /// <param name="refCodeValue">Pass Reference Code Value</param>
        /// <param name="sysRoleOperator">Pass Role Operator AND/OR</param>
        /// <returns>True/False</returns>
        public Boolean RoleAddEmpRef(int roleID, string refCode, string refCodeValue, string sysRoleOperator)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                string[] sRefCodeVal = refCodeValue.Split(',');
                foreach (string sRefValue in sRefCodeVal)
                {
                    StringBuilder sbInsertQuery = new StringBuilder();

                    sbInsertQuery.Append(" select count(*) from sysrolerefsel where emp_roleid = @emp_roleid and sys_ref_code = @sys_ref_code and sys_ref_value = @sys_ref_value ");
                    object objRValue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("emp_roleid", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("sys_ref_code", refCode , MyDbType.String),
                    DbUtility.GetParameter("sys_ref_value", sRefValue , MyDbType.String),
                    DbUtility.GetParameter("sys_cond", sysRoleOperator , MyDbType.String),
                    });

                    if (BusinessUtility.GetInt(objRValue) <= 0)
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO sysrolerefsel (emp_roleid, sys_ref_code, sys_ref_value, sys_cond ) ");
                        sbInsertQuery.Append(" VALUES (@emp_roleid, @sys_ref_code, @sys_ref_value, @sys_cond) ");

                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("emp_roleid", roleID, MyDbType.Int),    
                        DbUtility.GetParameter("sys_ref_code", refCode , MyDbType.String),
                        DbUtility.GetParameter("sys_ref_value", sRefValue , MyDbType.String),
                        DbUtility.GetParameter("sys_cond", sysRoleOperator , MyDbType.String),
                        });
                    }
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception e)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(e);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Remove sysEmpRef from Role From sysrolerefsel Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="refCode">Pass Reference Code</param>
        /// <param name="refCodeValue">Pass Reference Code Value</param>
        /// <returns>True/False</returns>
        public Boolean RoleRemoveEmpRef(int roleID, string refCode, string refCodeValue)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                string[] sRefCodeVal = refCodeValue.Split(',');
                foreach (string sRefValue in sRefCodeVal)
                {
                    StringBuilder sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" DELETE FROM sysrolerefsel  ");
                    sbInsertQuery.Append(" Where emp_roleid = @emp_roleid AND sys_ref_code = @sys_ref_code AND sys_ref_value = @sys_ref_value ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("emp_roleid", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("sys_ref_code", refCode , MyDbType.String),
                    DbUtility.GetParameter("sys_ref_value", sRefValue , MyDbType.String)
                    });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Add Action/Functionality to Role into sysroletypeaction Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="functionalityID">Pass Functionality ID</param>
        /// <param name="functionalityType">Pass Functionality Type</param>
        /// <param name="AccessAllData">Pass To Allow Access All Data</param>
        /// <param name="AccessSpecificData">Pass To Allow Access Specific Data</param>
        /// <returns>True/False</returns>
        public Boolean RoleAddFunctionalityAccessAllSpecificData(int roleID, int functionalityID, string functionalityType, Boolean AccessAllData, Boolean AccessSpecificData)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Select idsysRoleTypeAction from sysroletypeaction");
                sbInsertQuery.Append(" Where empRoleID = @empRoles_empRoleID And RoleType = @empRoles_RoleType And ActionItem = @empRoles_ActionItem ");
                object rValue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empRoles_empRoleID", roleID, MyDbType.Int),    
                DbUtility.GetParameter("empRoles_RoleType", functionalityType , MyDbType.String),
                DbUtility.GetParameter("empRoles_ActionItem", functionalityID , MyDbType.Int)
                });

                if (BusinessUtility.GetInt(rValue) < 0 || rValue == null)
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" Insert into sysroletypeaction (empRoleID, RoleType, ActionItem) ");
                    sbInsertQuery.Append(" values (@empRoles_empRoleID, @empRoles_RoleType, @empRoles_ActionItem) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empRoles_empRoleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("empRoles_RoleType", functionalityType , MyDbType.String),
                    DbUtility.GetParameter("empRoles_ActionItem", functionalityID , MyDbType.Int)
                    });
                }

                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Update sysroletypeaction SET ");

                if (AccessAllData == true && AccessSpecificData == false)
                {
                    sbInsertQuery.Append(" AllowAllData = 1, ");
                    sbInsertQuery.Append(" AllowSpecificData = 0 ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("emp_roleid", roleID, MyDbType.Int),    
                    });

                }
                else if (AccessAllData == false && AccessSpecificData == true)
                {
                    sbInsertQuery.Append(" AllowAllData = 0, ");
                    sbInsertQuery.Append(" AllowSpecificData = 1 ");
                }

                sbInsertQuery.Append(" Where empRoleID = @empRoles_empRoleID And RoleType = @empRoles_RoleType And ActionItem = @empRoles_ActionItem ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empRoles_empRoleID", roleID, MyDbType.Int),    
                DbUtility.GetParameter("empRoles_RoleType", functionalityType , MyDbType.String),
                DbUtility.GetParameter("empRoles_ActionItem", functionalityID , MyDbType.Int)
                });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// To Check If User is Allowed to Access All Data or Not From sysroletypeaction Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="functionalityID">Pass Functionality ID</param>
        /// <param name="functionalityType">Pass Functionality Type</param>
        /// <returns>True/False</returns>
        public Boolean IsAllowAllAccessData(int roleID, int functionalityID, string functionalityType)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Select AllowAllData from sysroletypeaction");
                sbInsertQuery.Append(" Where empRoleID = @empRoles_empRoleID And RoleType = @empRoles_RoleType And ActionItem = @empRoles_ActionItem ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empRoles_empRoleID", roleID, MyDbType.Int),    
                DbUtility.GetParameter("empRoles_RoleType", functionalityType , MyDbType.String),
                DbUtility.GetParameter("empRoles_ActionItem", functionalityID , MyDbType.Int)
                });

                if ((BusinessUtility.GetInt(rValue) > 0) && rValue != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Check If User is Allowed to Access Specific Data or Not From sysroletypeaction Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="functionalityID">Pass Functionality ID</param>
        /// <param name="functionalityType">Pass Functionality Type</param>
        /// <returns>True/False</returns>
        public Boolean IsAllowAllSpecificAccessData(int roleID, int functionalityID, string functionalityType)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Select AllowSpecificData from sysroletypeaction");
                sbInsertQuery.Append(" Where empRoleID = @empRoles_empRoleID And RoleType = @empRoles_RoleType And ActionItem = @empRoles_ActionItem ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empRoles_empRoleID", roleID, MyDbType.Int),    
                DbUtility.GetParameter("empRoles_RoleType", functionalityType , MyDbType.String),
                DbUtility.GetParameter("empRoles_ActionItem", functionalityID , MyDbType.Int)
                });

                if ((BusinessUtility.GetInt(rValue) > 0) && rValue != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Remove Data Accessibility from a Role From sysroletypeaction Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="functionalityID">Pass Functionality ID</param>
        /// <param name="functionalityType">Pass Functionality Type</param>
        /// <returns>True/False</returns>
        public Boolean RoleRemoveFunctionalityAccessAllData(int roleID, int functionalityID, string functionalityType)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Update sysroletypeaction SET ");
                sbInsertQuery.Append(" AllowAllData = 0 ");
                sbInsertQuery.Append(" Where empRoleID = @empRoles_empRoleID And RoleType = @empRoles_RoleType And ActionItem = @empRoles_ActionItem ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empRoles_empRoleID", roleID, MyDbType.Int),    
                DbUtility.GetParameter("empRoles_RoleType", functionalityType , MyDbType.String),
                DbUtility.GetParameter("empRoles_ActionItem", functionalityID , MyDbType.Int)
                });
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get SysRef Detail From sysempreferencecodes Table
        /// </summary>
        /// <param name="refCode">Pass Reference Code</param>
        /// <param name="searchText">Pass Reference Code Value</param>
        /// <param name="lang">Pass Language Code</param>
        /// <returns>True/False</returns>
        public DataTable GetSysRef(string refCode, string searchText, string lang)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(string.Format(" Select empRef.sysRefCode, empRef.sysRefCodeValue, empRef.sysRefCodeText,  empRef.tiles{0} as tiles, empRef.subtitle   from sysempreferencecodes as empRef Where ", lang));
                sbQuery.Append(" empRef.sysRefCode = @refCode  ");

                if (searchText != "")
                {
                    sbQuery.Append(" and empRef.sysRefCodeValue like '%" + searchText + "%'");
                }
                sbQuery.Append("  and empRef.sysRefCodeValue not in('100', '21420.22537800.E1409826174') ");

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("refCode", refCode, MyDbType.String)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Check if Sysref value is Assigned to Role or Not From sysrolerefsel Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>True/False</returns>
        public Boolean RoleSysRefSelHavingUser(int roleID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select Count( *) from sysrolerefsel where emp_roleid = @empRoles_empRoleID ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empRoles_empRoleID", roleID, MyDbType.Int)
                    });

                if ((BusinessUtility.GetInt(rValue) > 0) && rValue != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Associated Report Type with Role in Table emprolereport
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="reportType">Pass Report Type</param>
        /// <returns>True/False</returns>
        public Boolean RoleAddReportType(int roleID, string reportType)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Insert into emprolereport (roleID, reportType) ");
                sbInsertQuery.Append(" values (@roleID, @reportType) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("reportType", reportType , MyDbType.String),
                    });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Remove Report Type From Role From Table emprolereport
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="reportType">Pass Report Type</param>
        /// <returns>True/False</returns>
        public Boolean RoleRemoveReportType(int roleID, string reportType)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Delete from emprolereport where roleID = @roleID AND reportType = @reportType  ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("reportType", reportType , MyDbType.String),
                    });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// Get Report Type Associated with Role From Table emprolereport
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="reportType">Pass Report Type</param>
        /// <returns>True/False</returns>
        public Boolean ReportTypeExistsInRole(int roleID, string reportType)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT roleID ");
                sbInsertQuery.Append(" FROM emprolereport ");
                sbInsertQuery.Append(" WHERE roleID = @roleID AND reportType = @reportType  ");

                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    DbUtility.GetParameter("reportType", reportType , MyDbType.String),
                });
                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }

        }


        /// <summary>
        /// Get Report Type List in Role from Table emprolereport
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>True/False</returns>
        public DataTable ReportTypeListExistsInRole(int roleID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT reportType ");
                sbInsertQuery.Append(" FROM emprolereport ");
                sbInsertQuery.Append(" WHERE roleID = @roleID  ");

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                    
                });

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }

        }

        /// <summary>
        /// Get Employee Report Type
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>True/False</returns>
        public DataTable GetEmployeeReportType(int empID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select distinct reportType from emprolereport where roleID IN( ");
                sbInsertQuery.Append(" SELECT  ");
                sbInsertQuery.Append("     roleID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" (SELECT DISTINCT ");
                sbInsertQuery.Append(" eType.empRoles_empRoleID as roleID  ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysroletypeactionitems AS eItem ");
                sbInsertQuery.Append(" INNER JOIN emproletypes AS eType ON eType.empRoles_ActionItem = eItem.ActionItem ");
                sbInsertQuery.Append(" AND eType.empRoles_empRoleID IN (SELECT  ");
                sbInsertQuery.Append(" empRoles_empRoleID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" emprolesincludes ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" empHeader_empHdrID = @empID)  ");

                sbInsertQuery.Append(" UNION  ");

                sbInsertQuery.Append("     SELECT DISTINCT ");
                sbInsertQuery.Append("       syroleSel.emp_roleID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysrolerefsel AS syroleSel ");
                sbInsertQuery.Append(" JOIN empreference eRef ON eRef.sysEmpReferenceCodes_sysRefCode = syroleSel.sys_ref_code ");
                sbInsertQuery.Append(" AND eRef.empHeader_empHdrID = @empID ");
                sbInsertQuery.Append(" AND (syroleSel.sys_cond IS NULL ");
                sbInsertQuery.Append(" OR syroleSel.sys_cond = 'OR') ");
                sbInsertQuery.Append(" AND (((SELECT  ");
                sbInsertQuery.Append(" COUNT(*) ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysrolerefsel AS AND1syroleSel ");
                sbInsertQuery.Append(" JOIN empreference eRef1 ON eRef1.sysEmpReferenceCodes_sysRefCode = AND1syroleSel.sys_ref_code ");
                sbInsertQuery.Append(" AND eRef1.empHeader_empHdrID = @empID ");
                sbInsertQuery.Append(" AND AND1syroleSel.sys_cond = 'AND' ");
                sbInsertQuery.Append(" AND AND1syroleSel.sys_cond IS NOT NULL ");
                sbInsertQuery.Append(" AND AND1syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(AND1syroleSel.sys_ref_code, eRef1.empHeader_empHdrID) ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" AND1syroleSel.emp_roleid = syroleSel.emp_roleid) = (SELECT  ");
                sbInsertQuery.Append(" COUNT(*) ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysrolerefsel AS AND2syroleSel ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbInsertQuery.Append(" AND AND2syroleSel.sys_cond = 'AND') ");
                sbInsertQuery.Append(" AND (SELECT  ");
                sbInsertQuery.Append(" COUNT(*) ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysrolerefsel AS AND2syroleSel ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbInsertQuery.Append(" AND AND2syroleSel.sys_cond = 'AND') > 0) ");
                sbInsertQuery.Append(" OR ((SELECT  ");
                sbInsertQuery.Append(" COUNT(*) ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysrolerefsel AS ORsyroleSel ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" ORsyroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbInsertQuery.Append(" AND ORsyroleSel.sys_cond = 'AND') = 0)) ");
                sbInsertQuery.Append(" AND (syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(syroleSel.sys_ref_code, empHeader_empHdrID)) ");
                sbInsertQuery.Append(" INNER JOIN emproletypes AS eType ON eType.empRoles_empRoleID = syroleSel.emp_roleid ");
                sbInsertQuery.Append(" INNER JOIN sysroletypeactionitems AS eItem ON eType.empRoles_ActionItem = eItem.ActionItem) AS Result ");
                sbInsertQuery.Append(" ) ");
                sbInsertQuery.Append(" union ");
                sbInsertQuery.Append(" select a.sysRefCode as reportType from empmstlistincl a ");
                sbInsertQuery.Append(" inner join sysmasterlisttable b on b.mstListTblID= a.empMstListTblID ");
                sbInsertQuery.Append(" where a.empListInclEmpID =@empID   ");


                return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empID", empID, MyDbType.Int)
                    
                });

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Get Training Event Not Assign to Role From Tabl sysroleassigntraining
        /// </summary>
        /// <param name="roleID">Pass Role ID </param>
        /// <param name="lang">Pass Language Code</param>
        /// <param name="searchText">Pass Role Name</param>
        /// <returns>Datatable</returns>
        public DataTable GetAssignTrainingNotInRole(int roleID, string lang, string searchText)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbFinalQuery = new StringBuilder();
                StringBuilder sbQuery = new StringBuilder();
                StringBuilder sEventQuery = new StringBuilder();
                sEventQuery.Append(string.Format(" SELECT DISTINCT tEvnt.courseHdrID AS eventID, tEvnt.courseTitle{0} AS EventTitle,  evntType.type{0} AS EventType, 0 as Duration ", lang));
                sEventQuery.Append(" FROM traningevent AS tEvnt ");
                sEventQuery.Append(" LEFT OUTER JOIN sysroleassigntraining AS roleEvntInc ON roleEvntInc.courseHdrID = tEvnt.courseHdrID AND roleEvntInc.roleID <> @roleID AND tEvnt.courseActive =1 ");
                sEventQuery.Append(" INNER JOIN coursetype AS evntType ON evntType.type = tEvnt.courseType ");
                sEventQuery.Append(" WHERE tEvnt.courseHdrID Not IN (SELECT courseHdrID FROM sysroleassigntraining where roleID =@roleID ) and tEvnt.isVersionActive = 1  ");
                sEventQuery.Append(" AND tEvnt.testType <> '" + CourseTestType.TestOnly + "' ");
                //if (searchText != "")
                //{
                //    sbQuery.Append(string.Format(" AND tEvnt.courseTitle{0} like '%" + searchText + "%'", lang));
                //}
                //return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                //DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                //});

                if (searchText != "")
                {
                    string[] arrEvent = searchText.Split(' ');
                    int i = 0;
                    foreach (string sEvent in arrEvent)
                    {

                        if (i > 0)
                        {
                            sbQuery.Append(" union all");
                        }

                        sbQuery.Append(sEventQuery);
                        sbQuery.Append(string.Format(" AND tEvnt.courseTitle{0} like '%" + sEvent + "%' ", lang));
                        i += 1;
                    }
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sbQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by EventID   order by count(*) desc ");
                }
                else
                {
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sEventQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by EventID   order by count(*) desc ");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbFinalQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Get Training Event Assign To Role From sysroleassigntraining Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <param name="searchText">Pass Role Name</param>
        /// <returns>Datatable</returns>
        public DataTable GetAssignTrainingInRole(int roleID, string lang, string searchText)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbFinalQuery = new StringBuilder();
                StringBuilder sbQuery = new StringBuilder();
                StringBuilder sEventQuery = new StringBuilder();
                sEventQuery.Append(string.Format(" SELECT DISTINCT tEvnt.courseHdrID AS eventID, tEvnt.courseTitle{0} AS EventTitle,  evntType.type{0} AS EventType, 0 as Duration ", lang));
                sEventQuery.Append(" FROM traningevent AS tEvnt ");
                sEventQuery.Append(" LEFT OUTER JOIN sysroleassigntraining AS roleEvntInc ON roleEvntInc.courseHdrID = tEvnt.courseHdrID AND roleEvntInc.roleID <> @roleID AND tEvnt.courseActive =1 ");
                sEventQuery.Append(" INNER JOIN coursetype AS evntType ON evntType.type = tEvnt.courseType ");
                sEventQuery.Append(" WHERE tEvnt.courseHdrID IN (SELECT courseHdrID FROM sysroleassigntraining where roleID =@roleID ) and  tEvnt.isVersionActive =1 ");
                sEventQuery.Append(" AND tEvnt.testType <> '" + CourseTestType.TestOnly + "' ");
                //if (searchText != "")
                //{
                //    sbQuery.Append(string.Format(" AND tEvnt.courseTitle{0} like '%" + searchText + "%'", lang));
                //}
                //return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                //DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                //});

                if (searchText != "")
                {
                    string[] arrEvent = searchText.Split(' ');
                    int i = 0;
                    foreach (string sEvent in arrEvent)
                    {

                        if (i > 0)
                        {
                            sbQuery.Append(" union all");
                        }

                        sbQuery.Append(sEventQuery);
                        sbQuery.Append(string.Format(" AND tEvnt.courseTitle{0} like '%" + sEvent + "%' ", lang));
                        i += 1;
                    }
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sbQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by EventID   order by count(*) desc ");
                }
                else
                {
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sEventQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by EventID   order by count(*) desc ");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbFinalQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Get Is Role Assign Training Event Or Not From Table sysroleassigntraining
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>True/False</returns>
        public Boolean IsRoleAssignTrainingEvent(int roleID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT roleID ");
                sbInsertQuery.Append(" FROM sysroleassigntraining ");
                sbInsertQuery.Append(" WHERE roleID = @roleID   ");

                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    
                });
                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }

        }

        /// <summary>
        /// Assign Training Event To Role in sysroleassigntraining Table
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="trainingEventID">Pass Training Event IDs (spreated by ",")</param>
        /// <param name="createdBy">Pass Created by User ID</param>
        /// <returns>True/False</returns>
        public Boolean RoleAssignTrainingEvent(int roleID, string trainingEventID, int createdBy)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                string[] searchval = trainingEventID.Split(',');
                foreach (string sValues in searchval)
                {
                    if (BusinessUtility.GetInt(sValues) > 0)
                    {
                        StringBuilder sbInsertQuery = new StringBuilder();
                        StringBuilder sbQuery = new StringBuilder();
                        sbInsertQuery.Append(" Insert into sysroleassigntraining (roleID, courseHdrID) ");
                        sbInsertQuery.Append(" values (@roleID, @courseHdrID) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                            DbUtility.GetParameter("courseHdrID", BusinessUtility.GetInt(sValues) , MyDbType.Int),
                            });


                        sbQuery = new StringBuilder();
                        sbQuery.Append(" select empRoleID from sysrolemaster where isSystemGeneratedRole =1 and courseID = @courseHdrID ");
                        object rValue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("courseHdrID", BusinessUtility.GetInt(sValues) , MyDbType.Int),
                        });

                        if (BusinessUtility.GetInt(rValue) < 0 || rValue == null)
                        {

                            sbInsertQuery = new StringBuilder();
                            sbInsertQuery.Append(" INSERT INTO sysrolemaster (empRoleName, empRoleCreatedBy, empRoleCreatedOn, empRoleLastUpdatedOn, empRoleActive, isSystemGeneratedRole, courseID ) ");
                            sbInsertQuery.Append(" VALUES (@empRoleName, @empRoleCreatedBy, @empRoleCreatedOn, @empRoleLastUpdatedOn, @empRoleActive, @isSystemGeneratedRole, @courseID  ) ");
                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("empRoleName", "CourseID-"+ BusinessUtility.GetInt(sValues), MyDbType.String),    
                                DbUtility.GetParameter("empRoleCreatedBy", createdBy, MyDbType.String),
                                DbUtility.GetParameter("empRoleCreatedOn", DateTime.Now, MyDbType.DateTime),
                                DbUtility.GetParameter("empRoleLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                                DbUtility.GetParameter("empRoleActive", 1, MyDbType.Int),
                                DbUtility.GetParameter("isSystemGeneratedRole", 1, MyDbType.Int),
                                DbUtility.GetParameter("courseID", BusinessUtility.GetInt(sValues), MyDbType.Int),
                            });

                            int createdRoleID = dbTransactionHelper.GetLastInsertID();
                            if (createdRoleID > 0)
                            {
                                sbInsertQuery = new StringBuilder();
                                sbInsertQuery.Append(" Insert into emprolecourses (empRoles_empRoleID, courseHeader_courseHdrID) ");
                                sbInsertQuery.Append(" values (@roleID, @eventID) ");
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("roleID", createdRoleID, MyDbType.Int),    
                                    DbUtility.GetParameter("eventID", BusinessUtility.GetInt(sValues), MyDbType.Int)
                                });
                            }
                        }
                    }
                }

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Remove Assign Training Event From Role
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="trainingEventID">Pass Training Event IDs (spreated by ",")</param>
        /// <returns>True/False</returns>
        public Boolean RemoveRoleAssignTrainingEvent(int roleID, string trainingEventID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                string[] searchval = trainingEventID.Split(',');
                foreach (string sValues in searchval)
                {
                    if (BusinessUtility.GetInt(sValues) > 0)
                    {
                        StringBuilder sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" Delete From sysroleassigntraining where roleID = @roleID and courseHdrID = @courseHdrID ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                            DbUtility.GetParameter("courseHdrID", BusinessUtility.GetInt(sValues) , MyDbType.Int),
                            });
                    }
                }

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Get User List To Assign Training Event
        /// </summary>
        /// <param name="employeeNumber">Pass Employee User ID</param>
        /// <returns>True/False</returns>
        public DataTable GetUsersAssignTrainingEvent(string employeeNumber)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select Distinct empHdr.empHdrID AS EmpID, empHdr.empFirstName, empHdr.empLastName, empHdr.empExtID ");
                sbQuery.Append(" from empheader	as empHdr ");
                sbQuery.Append(" Where 1=1  ");
                sbQuery.Append(" AND empHdr.empExtID = '" + employeeNumber + "' ");
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null
                );
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Assign User To Training Event in emprolesincludes Table
        /// </summary>
        /// <param name="userID">pass employee ID</param>
        /// <param name="trainingEventID">Pass Training Event IDs (spreated by ",")</param>
        /// <param name="createdBy">Pass Created By User ID</param>
        /// <returns>True/False</returns>
        public Boolean AssignUserTrainingEvent(int userID, string trainingEventID, int createdBy)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                string[] searchval = trainingEventID.Split(',');
                foreach (string sValues in searchval)
                {
                    if (BusinessUtility.GetInt(sValues) > 0)
                    {
                        StringBuilder sbInsertQuery = new StringBuilder();
                        StringBuilder sbQuery = new StringBuilder();




                        sbQuery = new StringBuilder();
                        sbQuery.Append(" select empRoleID from sysrolemaster where isSystemGeneratedRole =1 and courseID = @courseHdrID ");
                        object rValue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("courseHdrID", BusinessUtility.GetInt(sValues) , MyDbType.Int),
                            });



                        if (BusinessUtility.GetInt(rValue) < 0 || rValue == null)
                        {

                        }
                        else
                        {
                            int roleID = BusinessUtility.GetInt(rValue);

                            sbQuery = new StringBuilder();
                            sbQuery.Append(" select empRoles_empRoleID from emprolesincludes where empRoles_empRoleID = @roleID and empHeader_empHdrID = @userID ");
                            rValue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("roleID", BusinessUtility.GetInt( roleID), MyDbType.Int),
                            DbUtility.GetParameter("userID", BusinessUtility.GetInt(userID), MyDbType.String),
                            });

                            if (BusinessUtility.GetInt(rValue) < 0 || rValue == null)
                            {

                                sbInsertQuery = new StringBuilder();
                                sbInsertQuery.Append(" Insert into emprolesincludes (empRoles_empRoleID, empHeader_empHdrID, assignedBy, assignedOn) ");
                                sbInsertQuery.Append(" values (@roleID, @userID, @assignedBy, @assignedOn ) ");
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("roleID", BusinessUtility.GetInt( roleID), MyDbType.Int),    
                                DbUtility.GetParameter("userID", BusinessUtility.GetInt(userID), MyDbType.String),
                                DbUtility.GetParameter("assignedBy", BusinessUtility.GetInt( createdBy), MyDbType.Int),    
                                DbUtility.GetParameter("assignedOn", DateTime.Now, MyDbType.DateTime),
                                });
                            }
                        }


                    }
                }

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// Get Employee Assign Training List
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="lang">Pass Language Code</param>
        /// <param name="sCourseName">Pass Course Name</param>
        /// <returns>True/False</returns>
        public DataTable GetEmployeeAssignTraining(int empID, string lang, string sCourseName)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbFinalQuery = new StringBuilder();
                StringBuilder sbQuery = new StringBuilder();
                StringBuilder sEventQuery = new StringBuilder();
                sEventQuery.Append(" SELECT DISTINCT ");
                sEventQuery.Append(" tEvnt.courseHdrID AS eventID, ");
                sEventQuery.Append(" tEvnt.courseTitle" + lang + " AS EventTitle, ");
                sEventQuery.Append(" evntType.type" + lang + " AS EventType, tEvnt.isVersionActive, ");
                sEventQuery.Append(" 0 as Duration ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" traningevent AS tEvnt ");
                sEventQuery.Append(" INNER JOIN ");
                sEventQuery.Append(" coursetype AS evntType ON evntType.type = tEvnt.courseType ");
                sEventQuery.Append(" inner JOIN ");
                sEventQuery.Append(" (    ");
                sEventQuery.Append(" select  ");
                sEventQuery.Append(" roleID, CourseHdrID ");
                sEventQuery.Append(" from ");
                sEventQuery.Append(" sysroleassigntraining ");
                sEventQuery.Append(" where ");
                sEventQuery.Append(" roleID IN ( ");
                sEventQuery.Append(" SELECT DISTINCT ");
                sEventQuery.Append(" eType.empRoles_empRoleID as roleID ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysroletypeactionitems AS eItem ");
                sEventQuery.Append(" INNER JOIN emproletypes AS eType ON eType.empRoles_ActionItem = eItem.ActionItem ");
                sEventQuery.Append(" AND eType.empRoles_empRoleID IN (SELECT  ");
                sEventQuery.Append(" empRoles_empRoleID ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" emprolesincludes ");
                sEventQuery.Append(" WHERE ");
                sEventQuery.Append(" empHeader_empHdrID = @empID) UNION SELECT DISTINCT ");
                sEventQuery.Append(" syroleSel.emp_roleID ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysrolerefsel AS syroleSel ");
                sEventQuery.Append(" JOIN empreference eRef ON eRef.sysEmpReferenceCodes_sysRefCode = syroleSel.sys_ref_code ");
                sEventQuery.Append(" AND eRef.empHeader_empHdrID = @empID ");
                sEventQuery.Append(" AND (syroleSel.sys_cond IS NULL ");
                sEventQuery.Append(" OR syroleSel.sys_cond = 'OR') ");
                sEventQuery.Append(" AND (((SELECT  ");
                sEventQuery.Append(" COUNT(*) ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysrolerefsel AS AND1syroleSel ");
                sEventQuery.Append(" JOIN empreference eRef1 ON eRef1.sysEmpReferenceCodes_sysRefCode = AND1syroleSel.sys_ref_code ");
                sEventQuery.Append(" AND eRef1.empHeader_empHdrID = @empID ");
                sEventQuery.Append(" AND AND1syroleSel.sys_cond = 'AND' ");
                sEventQuery.Append(" AND AND1syroleSel.sys_cond IS NOT NULL ");
                sEventQuery.Append(" AND AND1syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(AND1syroleSel.sys_ref_code, eRef1.empHeader_empHdrID) ");
                sEventQuery.Append(" WHERE ");
                sEventQuery.Append(" AND1syroleSel.emp_roleid = syroleSel.emp_roleid) = (SELECT  ");
                sEventQuery.Append(" COUNT(*) ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysrolerefsel AS AND2syroleSel ");
                sEventQuery.Append(" WHERE ");
                sEventQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sEventQuery.Append(" AND AND2syroleSel.sys_cond = 'AND') ");
                sEventQuery.Append(" AND (SELECT  ");
                sEventQuery.Append(" COUNT(*) ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysrolerefsel AS AND2syroleSel ");
                sEventQuery.Append(" WHERE ");
                sEventQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sEventQuery.Append(" AND AND2syroleSel.sys_cond = 'AND') > 0) ");
                sEventQuery.Append(" OR ((SELECT  ");
                sEventQuery.Append(" COUNT(*) ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysrolerefsel AS ORsyroleSel ");
                sEventQuery.Append(" WHERE ");
                sEventQuery.Append(" ORsyroleSel.emp_roleid = syroleSel.emp_roleid ");
                sEventQuery.Append(" AND ORsyroleSel.sys_cond = 'AND') = 0)) ");
                sEventQuery.Append(" AND (syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(syroleSel.sys_ref_code, empHeader_empHdrID)) ");
                sEventQuery.Append(" INNER JOIN emproletypes AS eType ON eType.empRoles_empRoleID = syroleSel.emp_roleid ");
                sEventQuery.Append(" ) ");
                sEventQuery.Append(" ) AS AssigTrainingEvent ON AssigTrainingEvent.CourseHdrID = tEvnt.courseHdrID and tEvnt.isVersionActive = 1 ");
                sEventQuery.Append(" where 1=1  ");


                //if (sCourseName != "")
                //{
                //    sbInsertQuery.Append(string.Format(" AND tEvnt.courseTitle{0} like '%" + sCourseName + "%'", lang));
                //}
                //return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                //    DbUtility.GetParameter("empID", empID, MyDbType.Int)

                //});

                if (sCourseName != "")
                {
                    string[] arrEvent = sCourseName.Split(' ');
                    int i = 0;
                    foreach (string sEvent in arrEvent)
                    {

                        if (i > 0)
                        {
                            sbQuery.Append(" union all");
                        }

                        sbQuery.Append(sEventQuery);
                        sbQuery.Append(string.Format(" AND tEvnt.courseTitle{0} like '%" + sEvent + "%' ", lang));
                        i += 1;
                    }
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sbQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by EventID   order by count(*) desc ");
                }
                else
                {
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sEventQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    sbFinalQuery.Append(" group by EventID   order by count(*) desc ");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbFinalQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empID", empID, MyDbType.Int)
                });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Count SysRefInReportUserManagment
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <returns>Int</returns>
        public int GetCountSysRefInReportUserManagment(int roleID, int actionID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Select count(*) FROM ");
                if (actionID == (int)RoleAction.Reporting)
                {
                    sbInsertQuery.Append(" sysrolereportrefsel  ");
                }
                else
                {
                    sbInsertQuery.Append(" sysroleusermgmtrefsel  ");
                }
                sbInsertQuery.Append(" Where emp_roleid = @emp_roleid ");

                object obj = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("emp_roleid", roleID, MyDbType.Int),    
                    });
                return BusinessUtility.GetInt(obj);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// To Get Employee ID Belong to the Role
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>Datatable</returns>
        public DataTable GetRoleEmployeeList(int roleID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                StringBuilder sbAndCond = new StringBuilder();
                StringBuilder sbOrCond = new StringBuilder();
                StringBuilder sbFinalQuery = new StringBuilder();
                StringBuilder sbSysRoleRefSelFinalQuery = new StringBuilder();

                string sysCode = "";
                string sysCodeValue = "";
                string sysCondition = "";

                sbSysRoleRefSelFinalQuery.Append(" select distinct emp.empHeader_empHdrID   from empreference as emp join empheader ehdr on ehdr.empHdrID = emp.empHeader_empHdrID and ehdr.isDeleted = 0  ");
                sbQuery.Append(" select sys_ref_code, sys_ref_value,sys_cond  from sysrolerefsel where emp_roleid = @roleID order by sys_cond ");

                DataTable dtRoleConditions = dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    });

                int iCounter = 0;
                foreach (DataRow dRowRoleCondition in dtRoleConditions.Rows)
                {
                    sysCode = BusinessUtility.GetString(dRowRoleCondition["sys_ref_code"]);
                    sysCodeValue = BusinessUtility.GetString(dRowRoleCondition["sys_ref_value"]);
                    sysCondition = BusinessUtility.GetString(dRowRoleCondition["sys_cond"]);


                    if (sysCondition.ToUpper() == SysRoleSelOperator.AND)
                    {
                        sbAndCond.Append("     join (select emp" + iCounter + "  .empHeader_empHdrID as iq" + iCounter + "empID from empreference as emp" + iCounter + " Where  (emp" + iCounter + ".sysEmpReferenceCodes_sysRefCode ='" + sysCode + "' AND emp" + iCounter + ".sysEmpReferenceCodes_sysRefCodeValue ='" + sysCodeValue + "')) as iq" + iCounter + "  on iq" + iCounter + ".iq" + iCounter + "empID = emp.empHeader_empHdrID ");
                    }
                    else
                    {
                        if (sysCondition.ToUpper() == SysRoleSelOperator.OR)
                        {
                            sbOrCond.Append("  OR (emp.sysEmpReferenceCodes_sysRefCode ='" + sysCode + "' AND emp.sysEmpReferenceCodes_sysRefCodeValue ='" + sysCodeValue + "') ");
                        }
                        else
                        {
                            sbOrCond.Append("  OR (emp.sysEmpReferenceCodes_sysRefCode ='" + sysCode + "' AND emp.sysEmpReferenceCodes_sysRefCodeValue ='" + sysCodeValue + "')  ");
                        }
                    }

                    iCounter += 1;
                }


                sbFinalQuery.Append(" select empHeader_empHdrID from emprolesincludes  where empRoles_empRoleID = " + roleID + " ");
                if (iCounter > 0)
                {
                    sbFinalQuery.Append(" union ");
                    sbFinalQuery.Append(BusinessUtility.GetString(sbSysRoleRefSelFinalQuery) + "  " + BusinessUtility.GetString(sbAndCond) + "  " + " where 1=0  " + BusinessUtility.GetString(sbOrCond) + " and emp.empHeader_empHdrID NOT IN (Select empHeader_empHdrID FROM emprolesincludes WHERE empRoles_empRoleID = " + roleID + ")");
                }

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbFinalQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Lock Role ID
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>True/False</returns>
        public bool LockRole(int roleID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" update sysrolemaster set isLock=1 where  empRoleID = @roleID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                        });

                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Unlock Role ID
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>True/False</returns>
        public bool UnLockRole(int roleID)
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" update sysrolemaster set isLock=0 where  empRoleID = @roleID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                        });
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Get Is Role Locked
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>True/False</returns>
        public Boolean IsRoleLocked(int roleID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT empRoleID ");
                sbInsertQuery.Append(" FROM sysrolemaster ");
                sbInsertQuery.Append(" WHERE empRoleID = @roleID and isLock=1  ");

                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                });

                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }

        }


        public Boolean SetRoleUpdateDateTime(int roleID, int updatedBy)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" update sysrolemaster set empRoleLastUpdatedBy =  @empRoleUpdatedBy,  empRoleLastUpdatedOn = @empRoleLastUpdatedOn where empRoleID = @roleID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empRoleUpdatedBy", updatedBy, MyDbType.Int),
                DbUtility.GetParameter("empRoleLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                DbUtility.GetParameter("roleID", roleID, MyDbType.Int)
                });
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        public DataTable GetRoleEmployeeDetailsList(int roleID, string refCode, string refValue)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                StringBuilder sbAndCond = new StringBuilder();
                StringBuilder sbOrCond = new StringBuilder();
                StringBuilder sbFinalQuery = new StringBuilder();
                StringBuilder sbSysRoleRefSelFinalQuery = new StringBuilder();

                string sysCode = "";
                string sysCodeValue = "";
                string sysCondition = "";

                sbSysRoleRefSelFinalQuery.Append(" select distinct emp.empHeader_empHdrID   from empreference as emp join empheader ehdr on ehdr.empHdrID = emp.empHeader_empHdrID and ehdr.isDeleted = 0  ");
                sbQuery.Append(" select sys_ref_code, sys_ref_value,sys_cond  from sysrolerefsel where emp_roleid = @roleID order by sys_cond ");

                DataTable dtRoleConditions = dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    });

                int iCounter = 0;
                foreach (DataRow dRowRoleCondition in dtRoleConditions.Rows)
                {
                    sysCode = BusinessUtility.GetString(dRowRoleCondition["sys_ref_code"]);
                    sysCodeValue = BusinessUtility.GetString(dRowRoleCondition["sys_ref_value"]);
                    sysCondition = BusinessUtility.GetString(dRowRoleCondition["sys_cond"]);


                    if (sysCondition.ToUpper() == SysRoleSelOperator.AND)
                    {
                        sbAndCond.Append("     join (select emp" + iCounter + "  .empHeader_empHdrID as iq" + iCounter + "empID from empreference as emp" + iCounter + " Where  (emp" + iCounter + ".sysEmpReferenceCodes_sysRefCode ='" + sysCode + "' AND emp" + iCounter + ".sysEmpReferenceCodes_sysRefCodeValue ='" + sysCodeValue + "')) as iq" + iCounter + "  on iq" + iCounter + ".iq" + iCounter + "empID = emp.empHeader_empHdrID ");
                    }
                    else
                    {
                        if (sysCondition.ToUpper() == SysRoleSelOperator.OR)
                        {
                            sbOrCond.Append("  OR (emp.sysEmpReferenceCodes_sysRefCode ='" + sysCode + "' AND emp.sysEmpReferenceCodes_sysRefCodeValue ='" + sysCodeValue + "') ");
                        }
                        else
                        {
                            sbOrCond.Append("  OR (emp.sysEmpReferenceCodes_sysRefCode ='" + sysCode + "' AND emp.sysEmpReferenceCodes_sysRefCodeValue ='" + sysCodeValue + "')  ");
                        }
                    }

                    iCounter += 1;
                }


                sbFinalQuery.Append(" select empHeader_empHdrID from emprolesincludes  where empRoles_empRoleID = " + roleID + " ");

                if (refValue == "INDIVIDUAL")
                {
                }
                else
                {
                    if (iCounter > 0)
                    {
                        sbFinalQuery.Append(" union ");
                        sbFinalQuery.Append(BusinessUtility.GetString(sbSysRoleRefSelFinalQuery) + "  " + BusinessUtility.GetString(sbAndCond) + "  " + " where 1=0  " + BusinessUtility.GetString(sbOrCond) + " and emp.empHeader_empHdrID NOT IN (Select empHeader_empHdrID FROM emprolesincludes WHERE empRoles_empRoleID = " + roleID + ")");
                    }
                }


                StringBuilder sbQueryUserDetail = new StringBuilder();
                sbQueryUserDetail.Append(" select empHeader_empHdrID as empHdrID,  ");
                sbQueryUserDetail.Append("    empHdr.empLastName As EmpLastName, empHdr.empFirstName As EmpFirstName,  empHdr.empLoginID As EmpExtID,  ");
                sbQueryUserDetail.Append(" iJobCode.empJobID as JobCode,   "); 
                sbQueryUserDetail.Append(" iqSite.empSiteID AS SiteNumber, ");
                sbQueryUserDetail.Append(" iempSite.empSite, iqLocation.empLocType AS LocationType, iRegion.empRegID AS Region, iDistrict.empDistID AS District, iDivision.empDivID AS Division, iempType.EmployeeType, iqProvince.Province  ");
                sbQueryUserDetail.Append(" from ");
                sbQueryUserDetail.Append(" ( ");
                sbQueryUserDetail.Append(" " + BusinessUtility.GetString(sbFinalQuery) + "  ");
                sbQueryUserDetail.Append(" ) as empList ");
                sbQueryUserDetail.Append(" join empheader empHdr on empHdrID = empList.empHeader_empHdrID  ");

                sbQueryUserDetail.Append(" left join ( ");
                sbQueryUserDetail.Append(" SELECT syrefCode.tilesen as empLocType, empHeader_empHdrID as iqEmpHdrID, sysEmpReferenceCodes_sysRefCodeValue as empLocTypeValue  ");
                sbQueryUserDetail.Append(" from empreference  ");
                sbQueryUserDetail.Append(" join sysempreferencecodes syrefCode on empreference.sysEmpReferenceCodes_sysRefCodeValue =syrefCode.sysRefCodeValue and syrefCode.sysRefCode='DIV' ");
                sbQueryUserDetail.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV' ");
                sbQueryUserDetail.Append(" ) as iqLocation on iqLocation.iqEmpHdrID = empHdr.empHdrID ");
                sbQueryUserDetail.Append(" left join ( ");
                sbQueryUserDetail.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSiteID, empHeader_empHdrID as iEmpHdrID ");
                sbQueryUserDetail.Append(" from empreference  ");
                sbQueryUserDetail.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'STR' ");
                sbQueryUserDetail.Append(" ) as iqSite on iqSite.iEmpHdrID = empHdr.empHdrID ");

                sbQueryUserDetail.Append(" left join ( ");
                sbQueryUserDetail.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSiteID, empHeader_empHdrID as iEmpHdrID ");
                sbQueryUserDetail.Append(" from empreference  ");
                sbQueryUserDetail.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'CSITE' ");
                sbQueryUserDetail.Append(" ) as iqCSite on iqCSite.iEmpHdrID = empHdr.empHdrID ");

                sbQueryUserDetail.Append(" left join ( ");
                sbQueryUserDetail.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID ");
                sbQueryUserDetail.Append(" from empreference ");
                sbQueryUserDetail.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TDIVN' ");
                sbQueryUserDetail.Append(" ) as iDivision ON iDivision.iEmpHdrID = empHdr.empHdrID ");
                sbQueryUserDetail.Append(" left join ( ");
                sbQueryUserDetail.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empRegID, empHeader_empHdrID as iEmpHdrID ");
                sbQueryUserDetail.Append(" from empreference  ");
                sbQueryUserDetail.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TREGN' ");
                sbQueryUserDetail.Append(" ) as iRegion ON iRegion.iEmpHdrID = empHdr.empHdrID ");
                sbQueryUserDetail.Append(" left join (  ");
                sbQueryUserDetail.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDistID, empHeader_empHdrID as iEmpHdrID ");
                sbQueryUserDetail.Append(" from empreference ");
                sbQueryUserDetail.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'TDIST' ");
                sbQueryUserDetail.Append(" ) as iDistrict ON iDistrict.iEmpHdrID = empHdr.empHdrID ");
                sbQueryUserDetail.Append(" left join (  ");
                sbQueryUserDetail.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID ");
                sbQueryUserDetail.Append(" from empreference ");
                sbQueryUserDetail.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'JOB' ");
                sbQueryUserDetail.Append(" ) as iJobCode ON iJobCode.iEmpHdrID = empHdr.empHdrID ");
                sbQueryUserDetail.Append(" left join (  ");

                sbQueryUserDetail.Append(" SELECT  ");
                sbQueryUserDetail.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empUnionType, syrefCode.sysRefCodeText as employeeType, ");
                sbQueryUserDetail.Append(" empHeader_empHdrID AS iEmpHdrID ");
                sbQueryUserDetail.Append(" FROM ");
                sbQueryUserDetail.Append(" empreference ");
                sbQueryUserDetail.Append(" join sysempreferencecodes syrefCode on empreference.sysEmpReferenceCodes_sysRefCodeValue =syrefCode.sysRefCodeValue and syrefCode.sysRefCode='TYP' ");
                sbQueryUserDetail.Append(" WHERE ");
                sbQueryUserDetail.Append(" sysEmpReferenceCodes_sysRefCode = 'TYP' ");

                sbQueryUserDetail.Append(" ) as iempType ON iempType.iEmpHdrID = empHdr.empHdrID ");


                sbQueryUserDetail.Append(" left join (  ");
                sbQueryUserDetail.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empSite, empHeader_empHdrID as iEmpHdrID ");
                sbQueryUserDetail.Append(" from empreference ");
                sbQueryUserDetail.Append(" WHERE sysEmpReferenceCodes_sysRefCode = 'SITE' ");
                sbQueryUserDetail.Append(" ) as iempSite ON iempSite.iEmpHdrID = empHdr.empHdrID ");


                sbQueryUserDetail.Append(" left join ( ");
                sbQueryUserDetail.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as Province, empHeader_empHdrID as iEmpHdrID ");
                sbQueryUserDetail.Append(" from empreference  ");
                sbQueryUserDetail.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'PABBR' ");
                sbQueryUserDetail.Append(" ) as iqProvince on iqProvince.iEmpHdrID = empHdr.empHdrID ");


                sbQueryUserDetail.Append(" where 1 = 1 ");

                StringBuilder sbReportFilter = new StringBuilder();

                if (refCode.ToLower() == EmpSearchBy.Store.ToLower())
                {
                    sbReportFilter.Append(" AND iqSite.empSiteID IN ( ");
                    string[] sSearchValue = refValue.Split(',');
                    int i = 0;

                    string empSiteID = "";

                    foreach (string sValue in sSearchValue)
                    {
                        if (empSiteID == "")
                        {
                            empSiteID += "'" + sValue + "'";
                        }
                        else
                        {
                            empSiteID += "," + "'" + sValue + "'";
                        }
                        i += 1;
                    }
                    sbReportFilter.Append(" " + empSiteID + "  ) ");
                }
                else if (refCode.ToLower() == EmpSearchBy.Region.ToLower())
                {
                    sbReportFilter.Append(" AND iRegion.empRegID IN ( ");
                    string[] sSearchValue = refValue.Split(',');
                    int i = 0;

                    string empRegID = "";

                    foreach (string sValue in sSearchValue)
                    {
                        if (empRegID == "")
                        {
                            empRegID += "'" + sValue + "'";
                        }
                        else
                        {
                            empRegID += "," + "'" + sValue + "'";
                        }
                        i += 1;
                    }
                    sbReportFilter.Append(" " + empRegID + " ) ");
                }
                else if (refCode.ToLower() == EmpSearchBy.Division.ToLower())
                {
                    if (refCode.ToLower() == EmpSearchBy.Division.ToLower() && refValue.ToLower() == "All Users".ToLower())
                    {
                    }
                    else
                    {
                        sbReportFilter.Append(" AND iqLocation.empLocTypeValue IN ( ");
                        string[] sSearchValue = refValue.Split(',');
                        int i = 0;

                        string empLocType = "";

                        foreach (string sValue in sSearchValue)
                        {
                            if (empLocType == "")
                            {
                                empLocType += "'" + sValue + "'";
                            }
                            else
                            {
                                empLocType += "," + "'" + sValue + "'";
                            }
                            i += 1;
                        }
                        sbReportFilter.Append(" " + empLocType + "  ) ");
                    }
                }
                else if (refCode.ToLower() == EmpSearchBy.JobCode.ToLower())
                {
                    sbReportFilter.Append(" AND iJobCode.empJobID IN ( ");
                    string[] sSearchValue = refValue.Split(',');
                    int i = 0;

                    string empJobID = "";

                    foreach (string sValue in sSearchValue)
                    {
                        if (empJobID == "")
                        {
                            empJobID += "'" + sValue + "'";
                        }
                        else
                        {
                            empJobID += "," + "'" + sValue + "'";
                        }
                        i += 1;
                    }
                    sbReportFilter.Append(" " + empJobID + "  ) ");
                }
                else if (refCode.ToLower() == EmpSearchBy.Type.ToLower())
                {
                    sbReportFilter.Append(" AND iempType.empUnionType IN ( ");
                    string[] sSearchValue = refValue.Split(',');
                    int i = 0;

                    string empUnionType = "";

                    foreach (string sValue in sSearchValue)
                    {
                        if (empUnionType == "")
                        {
                            empUnionType += "'" + sValue + "'";
                        }
                        else
                        {
                            empUnionType += "," + "'" + sValue + "'";
                        }
                        i += 1;
                    }
                    sbReportFilter.Append(" " + empUnionType + " ) ");
                }
                else if (refCode.ToLower() == EmpSearchBy.DivisionTBS.ToLower())
                {
                    sbReportFilter.Append(" AND iDivision.empDivID IN ( ");
                    string[] sSearchValue = refValue.Split(',');
                    int i = 0;

                    string empDivID = "";

                    foreach (string sValue in sSearchValue)
                    {
                        if (empDivID == "")
                        {
                            empDivID += "'" + sValue + "'";
                        }
                        else
                        {
                            empDivID += "," + "'" + sValue + "'";
                        }
                        i += 1;
                    }
                    sbReportFilter.Append(" " + empDivID + " ) ");
                }
                else if (refCode.ToLower() == EmpSearchBy.RegionTBS.ToLower())
                {

                    sbReportFilter.Append(" AND iRegion.empRegID IN ( ");
                    string[] sSearchValue = refValue.Split(',');
                    int i = 0;

                    string empRegID = "";

                    foreach (string sValue in sSearchValue)
                    {
                        if (empRegID == "")
                        {
                            empRegID += "'" + sValue + "'";
                        }
                        else
                        {
                            empRegID += "," + "'" + sValue + "'";
                        }
                        i += 1;
                    }
                    sbReportFilter.Append(" " + empRegID + " ) ");
                }
                else if (refCode.ToLower() == EmpSearchBy.District.ToLower())
                {
                    sbReportFilter.Append(" AND iDistrict.empDistID IN ( ");
                    string[] sSearchValue = refValue.Split(',');
                    int i = 0;

                    string empDistID = "";

                    foreach (string sValue in sSearchValue)
                    {
                        if (empDistID == "")
                        {
                            empDistID += "'" + sValue + "'";
                        }
                        else
                        {
                            empDistID += "," + "'" + sValue + "'";
                        }
                        i += 1;
                    }
                    sbReportFilter.Append(" " + empDistID + " ) ");
                }
                else if (refCode.ToLower() == EmpSearchBy.Site.ToLower())
                {
                    sbReportFilter.Append(" AND iempSite.empSite IN ( ");
                    string[] sSearchValue = refValue.Split(',');
                    int i = 0;

                    string empSite = "";

                    foreach (string sValue in sSearchValue)
                    {
                        if (empSite == "")
                        {
                            empSite += "'" + sValue + "'";
                        }
                        else
                        {
                            empSite += "," + "'" + sValue + "'";
                        }
                        i += 1;
                    }
                    sbReportFilter.Append(" " + empSite + " ) ");
                }
                else if (refCode.ToLower() == EmpSearchBy.Province.ToLower())
                {
                    sbReportFilter.Append(" AND iqProvince.Province IN ( ");
                    string[] sSearchValue = refValue.Split(',');
                    int i = 0;

                    string empProvince = "";

                    foreach (string sValue in sSearchValue)
                    {
                        if (empProvince == "")
                        {
                            empProvince += "'" + sValue + "'";
                        }
                        else
                        {
                            empProvince += "," + "'" + sValue + "'";
                        }
                        i += 1;
                    }
                    sbReportFilter.Append(" " + empProvince + " ) ");
                }


                if (refCode != "")
                {
                    sbQueryUserDetail.Append(BusinessUtility.GetString(sbReportFilter));
                }

                
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQueryUserDetail), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("roleID", roleID, MyDbType.Int),    
                    });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public DataTable GetRoleRefCodeList(int roleID, string sCondition)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select distinct sys_ref_code from sysrolerefsel where emp_roleid = " + roleID + " ");
                if (sCondition != "")
                {
                    sbQuery.Append(" and ifnull(sys_cond,'OR')  = '" + sCondition + "'");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null
                );
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }


        public DataTable GetRoleRefCodeValueList(int roleID, string sRefCode, string sCondition)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select emp_roleid, sys_ref_code, sys_ref_value, ifnull(sys_cond,'OR') as sys_cond from sysrolerefsel where emp_roleid = " + roleID + " and sys_ref_code ='" + sRefCode + "' ");
                if (sCondition != "")
                {
                    sbQuery.Append(" and ifnull(sys_cond,'OR')  = '" + sCondition + "'");
                }

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null
                );
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }
    }
}
