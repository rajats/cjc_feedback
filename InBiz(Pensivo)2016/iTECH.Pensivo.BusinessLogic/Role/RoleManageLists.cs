﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Role Manage List Functionality
    /// </summary>
    public class RoleManageLists
    {
        /// <summary>
        /// Assign Manage List to role in table sysmasterlisttable
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="manageListID">Pass Manage List ID</param>
        /// <returns>True/False</returns>
        public Boolean RoleAssignManageList(int roleID, int manageListID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Insert into sysmasterlisttable (mstListBaseRoleID, mstListID) ");
                sbInsertQuery.Append(" values (@mstListBaseRoleID, @mstListID) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("mstListBaseRoleID", roleID, MyDbType.Int),    
                                    DbUtility.GetParameter("mstListID", manageListID, MyDbType.Int)
                                });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch(Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Remove Assign Manage List from role table sysmasterlisttable
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="manageListID">Pass Manage List ID</param>
        /// <returns>True/False</returns>
        public Boolean RemoveRoleAssignManageList(int roleID, int manageListID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" Delete From sysmasterlisttable where mstListBaseRoleID = @mstListBaseRoleID and mstListID = @mstListID ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("mstListBaseRoleID", roleID, MyDbType.Int),    
                            DbUtility.GetParameter("mstListID", manageListID , MyDbType.Int),
                            });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch(Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Manage List Not Associated with Role using table sysmanagelist, sysmasterlisttable
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lang">Pass Language </param>
        /// <param name="searchText">Pass Search Text</param>
        /// <returns>Datatable</returns>
        public DataTable GetManageListNotInRole(int roleID, string lang, string searchText)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(string.Format("  SELECT Distinct mngLst.idsysmanagelist, mngLst.listname ", lang));
                sbQuery.Append(" FROM sysmanagelist AS mngLst ");
                sbQuery.Append(" LEFT OUTER JOIN sysmasterlisttable AS roleMngLst ON  roleMngLst.mstListBaseRoleID <> @mstListBaseRoleID AND mngLst.isActive =1 ");
                sbQuery.Append(" WHERE mngLst.idsysmanagelist Not IN (SELECT mstListID FROM sysmasterlisttable where mstListBaseRoleID =@mstListBaseRoleID )  ");
                if (searchText != "")
                {
                    sbQuery.Append(string.Format(" AND mngLst.listname like '%" + searchText + "%'", lang));
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("mstListBaseRoleID", roleID, MyDbType.Int)
                });

            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Manage List Associated with Role using table sysmanagelist, sysmasterlisttable
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <param name="lang">Pass Language </param>
        /// <param name="searchText">Pass Search Text</param>
        /// <returns>Datatable</returns>
        public DataTable GetManageListInRole(int roleID, string lang, string searchText)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(string.Format(" SELECT Distinct mngLst.idsysmanagelist, mngLst.listname ", lang));
                sbQuery.Append(" FROM sysmanagelist AS mngLst ");
                sbQuery.Append(" LEFT OUTER JOIN sysmasterlisttable AS roleMngLst ON  roleMngLst.mstListBaseRoleID <> @mstListBaseRoleID AND mngLst.isActive =1 ");
                sbQuery.Append(" WHERE mngLst.idsysmanagelist IN (SELECT mstListID FROM sysmasterlisttable where mstListBaseRoleID =@mstListBaseRoleID )  ");
                if (searchText != "")
                {
                    sbQuery.Append(string.Format(" AND mngLst.listname like '%" + searchText + "%'", lang));
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("mstListBaseRoleID", roleID, MyDbType.Int)
                });
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Is Role Assign Manage List
        /// </summary>
        /// <param name="roleID">Pass Role ID</param>
        /// <returns>True/False</returns>
        public Boolean IsRoleAssignManageList(int roleID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT mstListBaseRoleID ");
                sbInsertQuery.Append(" FROM sysmasterlisttable ");
                sbInsertQuery.Append(" WHERE mstListBaseRoleID = @mstListBaseRoleID   ");

                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("mstListBaseRoleID", roleID, MyDbType.Int),    
                    
                });
                if (BusinessUtility.GetInt(rValue) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }

        }

        /// <summary>
        /// To Get Employee Manage List
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="lang">Pass Language</param>
        /// <param name="manageListName">Pass Manage List Name</param>
        /// <returns>DataTable</returns>
        public DataTable GetEmployeeManageList(int empID, string lang, string manageListName)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbFinalQuery = new StringBuilder();
                StringBuilder sbQuery = new StringBuilder();
                StringBuilder sEventQuery = new StringBuilder();

                sEventQuery.Append(" SELECT DISTINCT ");
                sEventQuery.Append(" mstListTblID , manageList.listname, mstListBaseRoleID as roleID, mstListID  ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysmanagelist AS manageList ");
                sEventQuery.Append(" inner JOIN ");
                sEventQuery.Append(" (    ");
                sEventQuery.Append(" select  ");
                sEventQuery.Append(" mstListTblID, mstListBaseRoleID, mstListID ");
                sEventQuery.Append(" from ");
                sEventQuery.Append(" sysmasterlisttable ");
                sEventQuery.Append(" where ");
                sEventQuery.Append(" mstListBaseRoleID IN ( ");
                sEventQuery.Append(" SELECT DISTINCT ");
                sEventQuery.Append(" eType.empRoles_empRoleID as roleID ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysroletypeactionitems AS eItem ");
                sEventQuery.Append(" INNER JOIN emproletypes AS eType ON eType.empRoles_ActionItem = eItem.ActionItem ");
                sEventQuery.Append(" AND eType.empRoles_empRoleID IN (SELECT  ");
                sEventQuery.Append(" empRoles_empRoleID ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" emprolesincludes ");
                sEventQuery.Append(" WHERE ");
                sEventQuery.Append(" empHeader_empHdrID = @empID) UNION SELECT DISTINCT ");
                sEventQuery.Append(" syroleSel.emp_roleID ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysrolerefsel AS syroleSel ");
                sEventQuery.Append(" JOIN empreference eRef ON eRef.sysEmpReferenceCodes_sysRefCode = syroleSel.sys_ref_code ");
                sEventQuery.Append(" AND eRef.empHeader_empHdrID = @empID ");
                sEventQuery.Append(" AND (syroleSel.sys_cond IS NULL ");
                sEventQuery.Append(" OR syroleSel.sys_cond = 'OR') ");
                sEventQuery.Append(" AND (((SELECT  ");
                sEventQuery.Append(" COUNT(*) ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysrolerefsel AS AND1syroleSel ");
                sEventQuery.Append(" JOIN empreference eRef1 ON eRef1.sysEmpReferenceCodes_sysRefCode = AND1syroleSel.sys_ref_code ");
                sEventQuery.Append(" AND eRef1.empHeader_empHdrID = @empID ");
                sEventQuery.Append(" AND AND1syroleSel.sys_cond = 'AND' ");
                sEventQuery.Append(" AND AND1syroleSel.sys_cond IS NOT NULL ");
                sEventQuery.Append(" AND AND1syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(AND1syroleSel.sys_ref_code, eRef1.empHeader_empHdrID) ");
                sEventQuery.Append(" WHERE ");
                sEventQuery.Append(" AND1syroleSel.emp_roleid = syroleSel.emp_roleid) = (SELECT  ");
                sEventQuery.Append(" COUNT(*) ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysrolerefsel AS AND2syroleSel ");
                sEventQuery.Append(" WHERE ");
                sEventQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sEventQuery.Append(" AND AND2syroleSel.sys_cond = 'AND') ");
                sEventQuery.Append(" AND (SELECT  ");
                sEventQuery.Append(" COUNT(*) ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysrolerefsel AS AND2syroleSel ");
                sEventQuery.Append(" WHERE ");
                sEventQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sEventQuery.Append(" AND AND2syroleSel.sys_cond = 'AND') > 0) ");
                sEventQuery.Append(" OR ((SELECT  ");
                sEventQuery.Append(" COUNT(*) ");
                sEventQuery.Append(" FROM ");
                sEventQuery.Append(" sysrolerefsel AS ORsyroleSel ");
                sEventQuery.Append(" WHERE ");
                sEventQuery.Append(" ORsyroleSel.emp_roleid = syroleSel.emp_roleid ");
                sEventQuery.Append(" AND ORsyroleSel.sys_cond = 'AND') = 0)) ");
                sEventQuery.Append(" AND (syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(syroleSel.sys_ref_code, empHeader_empHdrID)) ");
                sEventQuery.Append(" INNER JOIN emproletypes AS eType ON eType.empRoles_empRoleID = syroleSel.emp_roleid ");
                sEventQuery.Append(" ) ");
                sEventQuery.Append(" ) AS AssignManageList ON AssignManageList.mstListID = manageList.idsysmanagelist ");
                sEventQuery.Append(" where 1=1  ");

                //if (manageListName != "")
                //{
                //    sbInsertQuery.Append(string.Format(" AND manageList.listname like '%" + manageListName + "%'", lang));
                //}
                //return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                //    DbUtility.GetParameter("empID", empID, MyDbType.Int)
                //});

                if (manageListName != "")
                {
                    string[] arrEvent = manageListName.Split(' ');
                    int i = 0;
                    foreach (string sEvent in arrEvent)
                    {

                        if (i > 0)
                        {
                            sbQuery.Append(" union all");
                        }

                        sbQuery.Append(sEventQuery);
                        sbQuery.Append(" AND listname like '%" + sEvent + "%' ");
                        i += 1;
                    }
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sbQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    //sbFinalQuery.Append(" group by mstListTblID   order by count(*) desc ");
                    sbFinalQuery.Append(" group by mstListID   order by count(*) desc ,  listName asc ");
                }
                else
                {
                    sbFinalQuery = new StringBuilder();
                    sbFinalQuery.Append(" Select fresult.*, count(*)  ");
                    sbFinalQuery.Append(" from (  ");
                    sbFinalQuery.Append(sEventQuery);
                    sbFinalQuery.Append(" ) as fresult  ");
                    //sbFinalQuery.Append(" group by mstListTblID   order by count(*) desc ");
                    sbFinalQuery.Append(" group by mstListID   order by count(*) desc ,  listName asc ");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbFinalQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empID", empID, MyDbType.Int)
                });
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Site List for Retail or WholeSale Location
        /// </summary>
        /// <param name="siteID">Pass Site ID</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeCanSeeSiteReport(string siteID)
        {
            if (siteID != "")
                siteID = siteID.PadLeft(4, '0');

            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();

                //sbInsertQuery.Append(" SELECT DISTINCT ");
                //sbInsertQuery.Append(" site.sysRefCodeValue, ");
                //sbInsertQuery.Append(" GROUP_CONCAT(CONCAT(CONCAT_WS(' ', eHdr.empFirstName, eHdr.empLastName), ");
                //sbInsertQuery.Append(" ' (', ");
                //sbInsertQuery.Append(" eHdr.empExtID, ");
                //sbInsertQuery.Append(" ')') ");
                //sbInsertQuery.Append(" SEPARATOR ', ') empDetail ");
                //sbInsertQuery.Append(" FROM ");
                //sbInsertQuery.Append(" (SELECT  ");
                //sbInsertQuery.Append(" distinct fun_get_emp_ref_val( 'STR', empHdrID) as sysRefCodeValue from empheader  ");
                //sbInsertQuery.Append(" where fun_get_emp_ref_val( 'DIV', empHdrID) ='RETL' ");
                //sbInsertQuery.Append(" ) AS site ");
                //sbInsertQuery.Append(" LEFT JOIN ");
                //sbInsertQuery.Append(" empotherreportsites empothrsite ON site.sysRefCodeValue = empothrsite.siteno and empothrsite.isActive = 1 ");
                //sbInsertQuery.Append(" LEFT JOIN ");
                //sbInsertQuery.Append(" empheader eHdr ON empothrsite.empHdrID = eHdr.empHdrID ");

                //sbInsertQuery.Append(" where 1=1 and site.sysRefCodeValue is Not null  ");
                //if (siteID != "")
                //{
                //    sbInsertQuery.Append(" AND site.sysRefCodeValue like '%" + siteID + "%'");
                //}
                //sbInsertQuery.Append(" GROUP BY site.sysRefCodeValue ");
                //return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, null);

                ////sbInsertQuery.Append(" select * From ( ");
                ////sbInsertQuery.Append(" SELECT   distinct fun_get_emp_ref_val( 'STR', empHdrID) as sysRefCodeValue, '' as empDetail ");
                ////sbInsertQuery.Append(" from empheader    ");

                ////sbInsertQuery.Append(" join ( ");
                ////sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID  ");
                ////sbInsertQuery.Append(" from empreference  ");
                ////sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'  ");
                ////sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID  ");


                ////sbInsertQuery.Append(" join ( ");
                ////sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID  ");
                ////sbInsertQuery.Append(" from empreference  ");
                ////sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'  ");
                ////sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID   ");

                //////sbInsertQuery.Append(" where ((fun_get_emp_ref_val( 'DIV', empHdrID) ='RETL')   OR  (fun_get_emp_ref_val( 'DIV', empHdrID) ='WR')  )  and (fun_get_emp_ref_val( 'JOB', empHdrID) )  ");
                ////sbInsertQuery.Append(" where ((iqDiv.empDivID ='RETL')   OR  (iqDiv.empDivID ='WR')  )  and (iqJob.empJobID )  ");
                ////sbInsertQuery.Append(" in ( ");
                ////sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824, ");
                ////sbInsertQuery.Append(" 0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631, ");
                ////sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708, ");
                ////sbInsertQuery.Append(" 1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760, ");
                ////sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801, ");
                ////sbInsertQuery.Append(" 1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921, ");
                ////sbInsertQuery.Append(" 1930,1931,1940,1941,1950 ");
                ////sbInsertQuery.Append(" )   ");
                ////sbInsertQuery.Append(" ) as site  ");

                ////sbInsertQuery.Append(" where 1=1 and site.sysRefCodeValue is Not null    ");
                ////if (siteID != "")
                ////{
                ////    sbInsertQuery.Append(" AND site.sysRefCodeValue like '%" + siteID + "%'");
                ////}
                ////sbInsertQuery.Append(" Order By sysRefCodeValue ");

                sbInsertQuery.Append(" select distinct sysSiteNo  as sysRefCodeValue, '' as empDetail from syssites where (sysPensivoLocationType='RETL' or sysPensivoLocationType='WR') and sysSiteActive = 1 ");
                if (siteID != "")
                {
                    sbInsertQuery.Append(" AND sysSiteNo like '%" + siteID + "%'");
                }

                //sbInsertQuery.Append(" SELECT DISTINCT   ");
                //sbInsertQuery.Append(" site.sysRefCodeValue, ");
                //sbInsertQuery.Append(" GROUP_CONCAT(CONCAT(CONCAT_WS(' ', eHdr.empFirstName, eHdr.empLastName),   ");
                //sbInsertQuery.Append(" ' (',   ");
                //sbInsertQuery.Append(" eHdr.empExtID,   ");
                //sbInsertQuery.Append(" ')')   ");
                //sbInsertQuery.Append(" SEPARATOR ', ') empDetail   ");
                //sbInsertQuery.Append(" FROM   ");
                //sbInsertQuery.Append(" ( ");
                //sbInsertQuery.Append(" SELECT   distinct fun_get_emp_ref_val( 'STR', empHdrID) as sysRefCodeValue ");
                //sbInsertQuery.Append(" from empheader    ");
                //sbInsertQuery.Append(" where ((fun_get_emp_ref_val( 'DIV', empHdrID) ='RETL')   OR  (fun_get_emp_ref_val( 'DIV', empHdrID) ='WR')  )  and (fun_get_emp_ref_val( 'JOB', empHdrID) )  ");
                //sbInsertQuery.Append(" in ( ");
                //sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824, ");
                //sbInsertQuery.Append(" 0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631, ");
                //sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708, ");
                //sbInsertQuery.Append(" 1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760, ");
                //sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801, ");
                //sbInsertQuery.Append(" 1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921, ");
                //sbInsertQuery.Append(" 1930,1931,1940,1941,1950 ");
                //sbInsertQuery.Append(" )    ");
                //sbInsertQuery.Append(" ) AS site   ");
                //sbInsertQuery.Append(" LEFT JOIN   ");
                //sbInsertQuery.Append(" ( ");
                ////sbInsertQuery.Append(" select	fun_get_emp_ref_val( 'STR', empHdrID) siteno, empHdrID  from empHeader ");
                //sbInsertQuery.Append(" select empHdrID,  fun_get_emp_ref_val( 'STR', empHdrID) siteno   ");
                //sbInsertQuery.Append(" from empHeader  ");
                //sbInsertQuery.Append("  where empHdrID not IN  ");
                //sbInsertQuery.Append(" ( ");
                //sbInsertQuery.Append(" 	select empListExclEmpID from empmstlistexcl where sysRefCode ='STR'  ");
                //sbInsertQuery.Append(" ) ");
                //sbInsertQuery.Append(" union	  ");
                //sbInsertQuery.Append(" select empListInclEmpID as empHdrID,  sysRefCodeValue as siteno from  empmstlistincl  ");
                //sbInsertQuery.Append(" ) ");
                //sbInsertQuery.Append(" empothrsite ON site.sysRefCodeValue = empothrsite.siteno  ");
                //sbInsertQuery.Append(" left JOIN    employeehasfunctionality empFun on empFun.empHdrID = empothrsite.empHdrID and empFun.reporting >0 ");
                //sbInsertQuery.Append(" LEFT JOIN    empheader eHdr ON  eHdr.empHdrID =empFun.empHdrID  ");

                //sbInsertQuery.Append(" where 1=1 and site.sysRefCodeValue is Not null    ");
                //if (siteID != "")
                //{
                //    sbInsertQuery.Append(" AND site.sysRefCodeValue like '%" + siteID + "%'");
                //}
                //sbInsertQuery.Append(" GROUP BY site.sysRefCodeValue  ");



                return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, null);

            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Site List for Logistics Location
        /// </summary>
        /// <param name="siteID">Pass Site ID</param>
        /// <returns>Datatable</returns>
        public DataTable GetEmployeeCanSeeSiteReportLogistics(string siteID)
        {
            if (siteID != "")
                siteID = siteID.PadLeft(4, '0');

            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();

                // sbInsertQuery.Append(" select * From ( ");
                // sbInsertQuery.Append(" SELECT   distinct fun_get_emp_ref_val( 'STR', empHdrID) as sysRefCodeValue, '' as empDetail ");
                // sbInsertQuery.Append(" from empheader    ");

                // sbInsertQuery.Append(" join (  ");
                // sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID  ");
                // sbInsertQuery.Append(" from empreference  ");
                // sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'  ");
                // sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID  ");


                // sbInsertQuery.Append(" join ( ");
                // sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID  ");
                // sbInsertQuery.Append(" from empreference  ");
                // sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'  ");
                // sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID      ");

                ///* sbInsertQuery.Append(" where (iqDiv.empDivID = 'LOGS' ) ");
                // sbInsertQuery.Append(" and (iqJob.empJobID)   ");
                // sbInsertQuery.Append(" in (   ");
                // sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,  0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,   ");
                // sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,  1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760,  ");
                // sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,  1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921,   ");
                // sbInsertQuery.Append(" 1930,1931,1940,1941,1950   ");
                // sbInsertQuery.Append(" )  ");
                // */

                // //sbInsertQuery.Append(" where ((fun_get_emp_ref_val( 'DIV', empHdrID) ='RETL')   OR  (fun_get_emp_ref_val( 'DIV', empHdrID) ='WR')  )  and (fun_get_emp_ref_val( 'JOB', empHdrID) )  ");
                // //sbInsertQuery.Append(" in ( ");
                // //sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824, ");
                // //sbInsertQuery.Append(" 0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631, ");
                // //sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708, ");
                // //sbInsertQuery.Append(" 1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760, ");
                // //sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801, ");
                // //sbInsertQuery.Append(" 1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921, ");
                // //sbInsertQuery.Append(" 1930,1931,1940,1941,1950 ");
                // //sbInsertQuery.Append(" )   ");
                // sbInsertQuery.Append(" ) as site  ");

                // sbInsertQuery.Append(" where 1=1 and site.sysRefCodeValue is Not null    ");
                // if (siteID != "")
                // {
                //     sbInsertQuery.Append(" AND site.sysRefCodeValue like '%" + siteID + "%'");
                // }
                // sbInsertQuery.Append(" Order By sysRefCodeValue ");

                sbInsertQuery.Append(" select distinct sysSiteNo  as sysRefCodeValue, '' as empDetail from syssites where sysPensivoLocationType='LOGS'  and sysSiteActive = 1  ");
                if (siteID != "")
                {
                    sbInsertQuery.Append(" AND sysSiteNo like '%" + siteID + "%'");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, null);
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Employee List who can see site report list 
        /// </summary>
        /// <param name="sysRefCode">Pass sysRef Code</param>
        /// <param name="siteID">Pass Site ID</param>
        /// <param name="listID">Pass List ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetSiteEmployeeList(string sysRefCode, string siteID, int listID)
        {
            if (siteID != "")
                siteID = siteID.PadLeft(4, '0');

            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select * from   ");
                sbInsertQuery.Append(" (  ");
                sbInsertQuery.Append(" SELECT  empheader.empHdrID, empheader.empFirstName, empheader.empLastName, empheader.empExtID, iqSite.refCodeValue   empSiteID, fun_get_emp_ref_val('STR', empheader.empHdrID) as empSiteNo  ");
                sbInsertQuery.Append(" from empheader    ");
                sbInsertQuery.Append(" join (   ");
                sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as refCodeValue, empHeader_empHdrID as iEmpHdrID   ");
                sbInsertQuery.Append(" from empreference    ");
                sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = @refCode   ");
                sbInsertQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empheader.empHdrID  and iqSite.refCodeValue = @refCodeValue  ");
                sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = empheader.empHdrID   and empFun.reporting >0    ");

                sbInsertQuery.Append("  and (empheader.isDeleted = 0 or empheader.isDeleted is null) and empheader.empActive = 1  and empheader.empHdrID NOT IN ");
                sbInsertQuery.Append(" ( ");
                sbInsertQuery.Append(" select empHdrID    ");
                sbInsertQuery.Append(" from  empmstlistexcl lstexcl    ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID= @listID  ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue   ");
                sbInsertQuery.Append(" ) ");

                sbInsertQuery.Append(" and empheader.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1 ) ");

                if (listID == 1)
                {
                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'   ");
                    sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");
                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                    sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                    sbInsertQuery.Append(" and	((iqDiv.empDivID = 'RETL')   OR  (iqDiv.empDivID ='WR')  )  and ( iqJob.empJobID)  ");
                    sbInsertQuery.Append(" in (   ");
                    sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,   ");
                    sbInsertQuery.Append(" 0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,   ");
                    sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,   ");
                    sbInsertQuery.Append(" 1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760,   ");
                    sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,   ");
                    sbInsertQuery.Append(" 1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921,   ");
                    sbInsertQuery.Append(" 1930,1931,1940,1941,1950   ");
                    sbInsertQuery.Append(" )     ");
                }
                else
                {
                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'   ");
                    sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");

                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                    sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                    /*   sbInsertQuery.Append(" and	((iqDiv.empDivID = 'LOGS')   )  and ( iqJob.empJobID)  ");
                       sbInsertQuery.Append(" in (     ");
                       sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,  0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,    ");
                       sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,  1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760, ");
                       sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,  1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921, ");
                       sbInsertQuery.Append(" 1930,1931,1940,1941,1950     ");
                       sbInsertQuery.Append(" )   ");
                     */
                }



                sbInsertQuery.Append(" union  ");

                sbInsertQuery.Append(" select emphdr.empHdrID, emphdr.empFirstName, emphdr.empLastName, emphdr.empExtID,  sysRefCodeValue as empSiteID , fun_get_emp_ref_val('STR', emphdr.empHdrID) as empSiteNo  ");
                sbInsertQuery.Append(" from  empmstlistincl lstIncl  ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstIncl.empMstListTblID  and  mstLstTbl.mstListID=@listID   ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstIncl.empListInclEmpID and lstIncl.sysRefCode = @refCode and  lstIncl.sysrefCodeValue = @refCodeValue  ");
                sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = emphdr.empHdrID   and empFun.reporting >0   ");

                sbInsertQuery.Append("  and (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1 and emphdr.empHdrID NOT IN  ");
                sbInsertQuery.Append(" (  ");
                sbInsertQuery.Append(" select empHdrID     ");
                sbInsertQuery.Append(" from  empmstlistexcl lstexcl     ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue    ");
                sbInsertQuery.Append(" )  ");

                sbInsertQuery.Append(" and emphdr.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1)  ");
                sbInsertQuery.Append(" )  ");
                sbInsertQuery.Append(" as ordersearchresult order by  empfirstname , empLastName  ");

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("refCode", sysRefCode, MyDbType.String),    
                            DbUtility.GetParameter("refCodeValue", siteID , MyDbType.String),
                            DbUtility.GetParameter("listID", listID , MyDbType.Int),
                            });
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Employee List who can see site report Logistics List
        /// </summary>
        /// <param name="sysRefCode">Pass sysRef Code</param>
        /// <param name="siteID">Pass Site ID</param>
        /// <param name="listID">Pass List ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetSiteEmployeeLogisticsList(string sysRefCode, string siteID, int listID)
        {
            if (siteID != "")
                siteID = siteID.PadLeft(4, '0');

            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select * from   ");
                sbInsertQuery.Append(" (  ");
                /*sbInsertQuery.Append(" SELECT  empheader.empHdrID, empheader.empFirstName, empheader.empLastName, empheader.empExtID, iqSite.refCodeValue   empSiteID   ");
                sbInsertQuery.Append(" from empheader    ");
                sbInsertQuery.Append(" join (   ");
                sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as refCodeValue, empHeader_empHdrID as iEmpHdrID   ");
                sbInsertQuery.Append(" from empreference    ");
                sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = @refCode   ");
                sbInsertQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empheader.empHdrID  and iqSite.refCodeValue = @refCodeValue  ");
                sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = empheader.empHdrID   and empFun.reporting >0    ");
                sbInsertQuery.Append(" and empheader.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1 ) ");

                if (listID == 1)
                {
                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'   ");
                    sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");
                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                    sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                    sbInsertQuery.Append(" and	((iqDiv.empDivID = 'RETL')   OR  (iqDiv.empDivID ='WR')  )  and ( iqJob.empJobID)  ");
                    sbInsertQuery.Append(" in (   ");
                    sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,   ");
                    sbInsertQuery.Append(" 0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,   ");
                    sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,   ");
                    sbInsertQuery.Append(" 1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760,   ");
                    sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,   ");
                    sbInsertQuery.Append(" 1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921,   ");
                    sbInsertQuery.Append(" 1930,1931,1940,1941,1950   ");
                    sbInsertQuery.Append(" )     ");
                }
                else
                {
                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'   ");
                    sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");

                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                    sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                    sbInsertQuery.Append(" and	((iqDiv.empDivID = 'LOGS')   )  and ( iqJob.empJobID)  ");
                    sbInsertQuery.Append(" in (     ");
                    sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,  0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,    ");
                    sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,  1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760, ");
                    sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,  1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921, ");
                    sbInsertQuery.Append(" 1930,1931,1940,1941,1950     ");
                    sbInsertQuery.Append(" )   ");
                }



                sbInsertQuery.Append(" union  ");*/

                sbInsertQuery.Append(" select emphdr.empHdrID, emphdr.empFirstName, emphdr.empLastName, emphdr.empExtID,  sysRefCodeValue as empSiteID , fun_get_emp_ref_val('STR', emphdr.empHdrID) as empSiteNo   ");
                sbInsertQuery.Append(" from  empmstlistincl lstIncl  ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstIncl.empMstListTblID  and  mstLstTbl.mstListID=@listID   ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstIncl.empListInclEmpID and lstIncl.sysRefCode = @refCode and  lstIncl.sysrefCodeValue = @refCodeValue  ");
                sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = emphdr.empHdrID   and empFun.reporting >0   ");

                sbInsertQuery.Append("  and (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1 and emphdr.empHdrID NOT IN  ");
                sbInsertQuery.Append(" (  ");
                sbInsertQuery.Append(" select empHdrID     ");
                sbInsertQuery.Append(" from  empmstlistexcl lstexcl     ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue    ");
                sbInsertQuery.Append(" )  ");

                sbInsertQuery.Append(" and emphdr.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1)  ");
                sbInsertQuery.Append(" )  ");
                sbInsertQuery.Append(" as ordersearchresult order by  empfirstname , empLastName  ");

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("refCode", sysRefCode, MyDbType.String),    
                            DbUtility.GetParameter("refCodeValue", siteID , MyDbType.String),
                            DbUtility.GetParameter("listID", listID , MyDbType.Int),
                            });
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Add Employee Site Report List
        /// </summary>
        /// <param name="userID">Pass User ID</param>
        /// <param name="manageListID">Pass Manage List ID</param>
        /// <param name="siteID">Pass Site ID</param>
        /// <param name="createdBy">Pass Created By User ID</param>
        /// <param name="sysRefCode">Pass Sys Ref Code</param>
        /// <param name="sysRefValue">Pass Sys Ref Code Value</param>
        /// <param name="functionalityID">Pass Functionality ID</param>
        /// <param name="listID">Pass List ID</param>
        /// <returns>True/False</returns>
        public Boolean RemoveSiteEmployeeList(int userID, int manageListID, string siteID, int createdBy, string sysRefCode, string sysRefValue, int functionalityID, int listID)
        {
            if (sysRefValue != "")
                sysRefValue = sysRefValue.PadLeft(4, '0');

            if (siteID != "")
                siteID = siteID.PadLeft(4, '0');

            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                object rvalue = null;

                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select empMstListTblID from empmstlistexcl lstexcl ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                sbInsertQuery.Append(" where  lstexcl.empListExclEmpID = @empListExclEmpID and lstexcl.sysRefCode = @sysRefCode and lstexcl.sysRefCodeValue = @sysRefCodeValue and lstexcl.functionalityID = @functionalityID  ");
                //empMstListTblID = @empMstListTblID and
                rvalue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empMstListTblID", manageListID, MyDbType.Int),    
                            DbUtility.GetParameter("empListExclEmpID", userID , MyDbType.Int),
                            DbUtility.GetParameter("sysRefCode", sysRefCode , MyDbType.String),
                            DbUtility.GetParameter("sysRefCodeValue", sysRefValue , MyDbType.String),
                            DbUtility.GetParameter("functionalityID", functionalityID , MyDbType.Int),
                            DbUtility.GetParameter("listID", listID , MyDbType.Int),
                            });

                if (BusinessUtility.GetInt(rvalue) > 0)
                {

                }
                else
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" select empMstListTblID from empmstlistincl lstIncl ");
                    sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstIncl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                    sbInsertQuery.Append(" where  lstIncl.empListInclEmpID = @empListInclEmpID and lstIncl.sysRefCode = @sysRefCode and lstIncl.sysRefCodeValue = @sysRefCodeValue and lstIncl.functionalityID = @functionalityID   ");
                    //empMstListTblID = @empMstListTblID and
                    rvalue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empMstListTblID", manageListID, MyDbType.Int),    
                            DbUtility.GetParameter("empListInclEmpID", userID , MyDbType.Int),
                            DbUtility.GetParameter("sysRefCode", sysRefCode , MyDbType.String),
                            DbUtility.GetParameter("sysRefCodeValue", sysRefValue , MyDbType.String),
                            DbUtility.GetParameter("functionalityID", functionalityID , MyDbType.Int),
                            DbUtility.GetParameter("listID", listID , MyDbType.Int),
                            });

                    if (BusinessUtility.GetInt(rvalue) > 0)
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" insert INTO empmstlistinclhistroy  ");
                        sbInsertQuery.Append(" (empMstListTblID, empHdrUpdatedBy, empHdrUpdatedOn, empListInclEmpID, createdBy, createdOnDatetime, sysRefCode, sysRefCodeValue, functionalityID) ");
                        sbInsertQuery.Append(" SELECT empMstListTblID, empHdrUpdatedBy, empHdrUpdatedOn, empListInclEmpID, @createdBy, @createdOnDatetime, sysRefCode, sysRefCodeValue, functionalityID  FROM empmstlistincl  ");
                        sbInsertQuery.Append(" WHERE  empListInclEmpID = @empListInclEmpID  and sysRefCode = @sysRefCode and sysRefCodeValue = @sysRefCodeValue and functionalityID = @functionalityID ");
                        //empMstListTblID = @empMstListTblID AND
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empMstListTblID", manageListID , MyDbType.Int),    
                            DbUtility.GetParameter("empListInclEmpID", userID , MyDbType.Int),
                            DbUtility.GetParameter("createdBy", createdBy , MyDbType.String),
                            DbUtility.GetParameter("createdOnDatetime", DateTime.Now , MyDbType.DateTime),
                            DbUtility.GetParameter("sysRefCode", sysRefCode , MyDbType.String),
                            DbUtility.GetParameter("sysRefCodeValue", sysRefValue , MyDbType.String),
                            DbUtility.GetParameter("functionalityID", functionalityID , MyDbType.Int),
                            });


                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append("DELETE lstIncl  FROM empmstlistincl lstIncl  ");
                        sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstIncl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                        sbInsertQuery.Append(" WHERE  lstIncl.empListInclEmpID = @empListInclEmpID  and lstIncl.sysRefCode = @sysRefCode and lstIncl.sysRefCodeValue = @sysRefCodeValue and lstIncl.functionalityID = @functionalityID  ");
                        //empMstListTblID = @empMstListTblID AND
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empMstListTblID", manageListID , MyDbType.Int),    
                            DbUtility.GetParameter("empListInclEmpID", userID , MyDbType.Int),
                            DbUtility.GetParameter("sysRefCode", sysRefCode , MyDbType.String),
                            DbUtility.GetParameter("sysRefCodeValue", sysRefValue , MyDbType.String),
                            DbUtility.GetParameter("functionalityID", functionalityID , MyDbType.Int),
                            DbUtility.GetParameter("listID", listID , MyDbType.Int),
                            });
                    }


                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" insert into empmstlistexcl  ");
                    sbInsertQuery.Append(" (empMstListTblID, empHdrUpdatedBy, empHdrUpdatedOn, empListExclEmpID, sysRefCode, sysRefCodeValue, functionalityID) ");
                    sbInsertQuery.Append(" VALUES (@empMstListTblID, @empHdrUpdatedBy, @empHdrUpdatedOn, @empListExclEmpID, @sysRefCode, @sysRefCodeValue, @functionalityID) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empMstListTblID", manageListID , MyDbType.Int),    
                            DbUtility.GetParameter("empHdrUpdatedBy", createdBy , MyDbType.String),
                            DbUtility.GetParameter("empHdrUpdatedOn", DateTime.Now , MyDbType.DateTime),
                            DbUtility.GetParameter("empListExclEmpID", userID , MyDbType.Int),
                            DbUtility.GetParameter("sysRefCode", sysRefCode , MyDbType.String),
                            DbUtility.GetParameter("sysRefCodeValue", sysRefValue , MyDbType.String),
                            DbUtility.GetParameter("functionalityID", functionalityID , MyDbType.Int),
                            });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Add Employee To Site List
        /// </summary>
        /// <param name="userID">Pass User ID</param>
        /// <param name="manageListID">Pass Manage List ID</param>
        /// <param name="siteID">Pass Site ID</param>
        /// <param name="createdBy">Pass Created By User ID</param>
        /// <param name="sysRefCode">Pass SysRefCode</param>
        /// <param name="sysRefValue">Pass SysRefCodeValue</param>
        /// <param name="functionalityID">Pass Functionality ID</param>
        /// <param name="listID">Pass Lis ID</param>
        /// <returns>True/False</returns>
        public Boolean AddSiteEmployeeList(int userID, int manageListID, string siteID, int createdBy, string sysRefCode, string sysRefValue, int functionalityID, int listID)
        {
            if (sysRefValue != "")
                sysRefValue = sysRefValue.PadLeft(4, '0');

            if (siteID != "")
                siteID = siteID.PadLeft(4, '0');

            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                object rvalue = null;

                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select empMstListTblID from empmstlistincl lstIncl  ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstIncl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                sbInsertQuery.Append(" where  lstIncl.empListInclEmpID = @empListInclEmpID   and lstIncl.sysRefCode = @sysRefCode and lstIncl.sysRefCodeValue = @sysRefCodeValue and lstIncl.functionalityID = @functionalityID ");
                //lstIncl.empMstListTblID = @empMstListTblID and
                rvalue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empMstListTblID", manageListID, MyDbType.Int),    
                            DbUtility.GetParameter("empListInclEmpID", userID , MyDbType.Int),
                            DbUtility.GetParameter("sysRefCode", sysRefCode , MyDbType.String),
                            DbUtility.GetParameter("sysRefCodeValue", sysRefValue , MyDbType.String),
                            DbUtility.GetParameter("functionalityID", functionalityID , MyDbType.Int),
                            DbUtility.GetParameter("listID", listID , MyDbType.Int),
                            
                            });

                if (BusinessUtility.GetInt(rvalue) > 0)
                {

                }
                else
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" select empMstListTblID from empmstlistexcl lstexcl  ");
                    sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                    sbInsertQuery.Append(" where  lstexcl.empListExclEmpID = @empListExclEmpID and lstexcl.sysRefCode = @sysRefCode and lstexcl.sysRefCodeValue = @sysRefCodeValue and lstexcl.functionalityID = @functionalityID  ");
                    //lstexcl.empMstListTblID = @empMstListTblID and
                    rvalue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empMstListTblID", manageListID, MyDbType.Int),    
                            DbUtility.GetParameter("empListExclEmpID", userID , MyDbType.Int),
                            DbUtility.GetParameter("sysRefCode", sysRefCode , MyDbType.String),
                            DbUtility.GetParameter("sysRefCodeValue", sysRefValue , MyDbType.String),
                            DbUtility.GetParameter("functionalityID", functionalityID , MyDbType.Int),
                            DbUtility.GetParameter("listID", listID , MyDbType.Int),
                            });

                    if (BusinessUtility.GetInt(rvalue) > 0)
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" insert INTO empmstlistexclhistory  ");
                        sbInsertQuery.Append(" (empMstListTblID, empHdrUpdatedBy, empHdrUpdatedOn, empListExclEmpID, createdBy, createdOnDatetime, sysRefCode, sysRefCodeValue, functionalityID) ");
                        sbInsertQuery.Append(" SELECT empMstListTblID, empHdrUpdatedBy, empHdrUpdatedOn, empListExclEmpID, @createdBy, @createdOnDatetime , sysRefCode, sysRefCodeValue, functionalityID  FROM empmstlistexcl  ");
                        sbInsertQuery.Append(" WHERE  empListExclEmpID = @empListExclEmpID and sysRefCode = @sysRefCode and sysRefCodeValue = @sysRefCodeValue and functionalityID = @functionalityID  ");
                        //empMstListTblID = @empMstListTblID AND
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empMstListTblID", manageListID , MyDbType.Int),    
                            DbUtility.GetParameter("empListExclEmpID", userID , MyDbType.Int),
                            DbUtility.GetParameter("createdBy", createdBy , MyDbType.String),
                            DbUtility.GetParameter("createdOnDatetime", DateTime.Now , MyDbType.DateTime),
                            DbUtility.GetParameter("sysRefCode", sysRefCode , MyDbType.String),
                            DbUtility.GetParameter("sysRefCodeValue", sysRefValue , MyDbType.String),
                            DbUtility.GetParameter("functionalityID", functionalityID , MyDbType.Int),
                            });


                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append("DELETE lstexcl FROM empmstlistexcl lstexcl  ");
                        sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                        sbInsertQuery.Append(" WHERE  lstexcl.empListExclEmpID = @empListExclEmpID  and lstexcl.sysRefCode = @sysRefCode and lstexcl.sysRefCodeValue = @sysRefCodeValue and lstexcl.functionalityID = @functionalityID   ");
                        //empMstListTblID = @empMstListTblID AND
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empMstListTblID", manageListID , MyDbType.Int),    
                            DbUtility.GetParameter("empListExclEmpID", userID , MyDbType.Int),
                            DbUtility.GetParameter("sysRefCode", sysRefCode , MyDbType.String),
                            DbUtility.GetParameter("sysRefCodeValue", sysRefValue , MyDbType.String),
                            DbUtility.GetParameter("functionalityID", functionalityID , MyDbType.Int),
                            DbUtility.GetParameter("listID", listID , MyDbType.Int),
                            });
                    }


                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" insert into empmstlistincl  ");
                    sbInsertQuery.Append(" (empMstListTblID, empHdrUpdatedBy, empHdrUpdatedOn, empListInclEmpID, sysRefCode, sysRefCodeValue, functionalityID) ");
                    sbInsertQuery.Append(" VALUES (@empMstListTblID, @empHdrUpdatedBy, @empHdrUpdatedOn, @empListInclEmpID, @sysRefCode, @sysRefCodeValue, @functionalityID) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empMstListTblID", manageListID , MyDbType.Int),    
                            DbUtility.GetParameter("empHdrUpdatedBy", createdBy , MyDbType.String),
                            DbUtility.GetParameter("empHdrUpdatedOn", DateTime.Now , MyDbType.DateTime),
                            DbUtility.GetParameter("empListInclEmpID", userID , MyDbType.Int),
                            DbUtility.GetParameter("sysRefCode", sysRefCode , MyDbType.String),
                            DbUtility.GetParameter("sysRefCodeValue", sysRefValue , MyDbType.String),
                            DbUtility.GetParameter("functionalityID", functionalityID , MyDbType.Int),
                            });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get User List Not In Site Manager List
        /// </summary>
        /// <param name="roleManageListID">Pass Manage List ID</param>
        /// <param name="searchBy">Pass Search By</param>
        /// <param name="searchValue">Pass Search Value</param>
        /// <param name="siteID">Pass Site ID</param>
        /// <param name="listID">Pass List ID</param>
        /// <returns></returns>
        public DataTable GetUserListNotInSiteManager(int roleManageListID, string searchBy, string searchValue, string siteID, int listID)
        {
            if (siteID != "")
                siteID = siteID.PadLeft(4, '0');

            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select Distinct emphdr.empHdrID , emphdr.empFirstName, emphdr.empLastName, emphdr.empExtID, iqSite.refCodeValue as empSiteID "); //  fun_get_emp_ref_val( 'STR', emphdr.empHdrID)
                sbQuery.Append(" from empheader	as emphdr ");
                sbQuery.Append(" JOIN (SELECT  ");
                sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
                sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                sbQuery.Append(" FROM ");
                sbQuery.Append(" empreference ");
                sbQuery.Append(" WHERE ");
                sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'STR') AS iqSite ");
                sbQuery.Append(" ON iqSite.iEmpHdrID = emphdr.empHdrID ");
                sbQuery.Append(" Where   (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1 and emphdr.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1 ) ");
                // emphdr.empHdrID Not IN (SELECT empHdrID FROM empotherreportsites where SiteNo =@siteNo and isActive =1 ) and
                //sbQuery.Append("  ");

                if (searchBy == EmpSearchBy.Name)
                {
                    StringBuilder sQuery = new StringBuilder();
                    sbQuery = new StringBuilder();
                    sQuery.Append(" select * from ( select Distinct emphdr.empHdrID , emphdr.empFirstName, emphdr.empLastName, emphdr.empExtID, iqSite.refCodeValue as empSiteID "); //fun_get_emp_ref_val( 'STR', emphdr.empHdrID)
                    sQuery.Append(" from empheader	as emphdr ");
                    sQuery.Append(" JOIN (SELECT  ");
                    sQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
                    sQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                    sQuery.Append(" FROM ");
                    sQuery.Append(" empreference ");
                    sQuery.Append(" WHERE ");
                    sQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'STR') AS iqSite ");
                    sQuery.Append(" ON iqSite.iEmpHdrID = emphdr.empHdrID ");
                    sQuery.Append(" Where  (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1 and  emphdr.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1 ) ) as result where 1=1  ");
                    //emphdr.empHdrID Not IN (SELECT empHdrID FROM empotherreportsites where SiteNo =@siteNo and isActive =1 ) and
                    //sQuery.Append("  ");

                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND ( empFirstName = '" + searchValue + "' OR empLastName = '" + searchValue + "' OR  concat_ws(' ', trim(empFirstName), trim(empLastName)) = '" + searchValue + "'  OR  concat_ws(' ', trim(empFirstName), trim(empLastName), trim(empExtID)) = '" + searchValue + "'  OR empExtID = '" + searchValue + "') ");

                    string[] sEmpName = searchValue.Split(' ');
                    string empClause = "";

                    if (sEmpName.Length > 2)
                    {
                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ) ");


                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' and  empExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                    }
                    else if (sEmpName.Length == 2)
                    {

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "'  ) ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' AND  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                    }
                    else if (sEmpName.Length == 1)
                    {
                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ) ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[0]) + "' ");
                    }

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND (  ");

                    sEmpName = searchValue.Split(' ');
                    empClause = "";
                    foreach (string sName in sEmpName)
                    {
                        if (empClause == "")
                        {
                            empClause += " empFirstName = '" + sName + "' OR empLastName = '" + sName + "' OR  empExtID = '" + sName + "' ";
                        }
                        else
                        {
                            empClause += " OR empFirstName = '" + sName + "' OR empLastName = '" + sName + "' OR  empExtID = '" + sName + "' ";
                        }
                    }

                    sbQuery.Append(empClause);
                    sbQuery.Append(" ) ");

                    sbQuery.Append(" union ");

                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND (  ");

                    sEmpName = searchValue.Split(' ');
                    foreach (string sName in sEmpName)
                    {
                        if (empClause == "")
                        {
                            empClause += " empFirstName like '%" + sName + "%' OR empLastName  like '%" + sName + "%' OR  empExtID like '%" + sName + "%' ";
                        }
                        else
                        {
                            empClause += " OR empFirstName like '%" + sName + "%' OR empLastName  like '%" + sName + "%' OR  empExtID like '%" + sName + "%' ";
                        }
                    }

                    sbQuery.Append(empClause);
                    sbQuery.Append(" ) ");
                }
                else if (searchBy == EmpSearchBy.EmpCode)
                {
                    sbQuery.Append(" AND emphdr.empExtID = '" + searchValue + "' ");
                }
                else if (searchBy == EmpSearchBy.Store || searchBy == EmpSearchBy.Region || searchBy == EmpSearchBy.Division || searchBy == EmpSearchBy.JobCode)
                {
                    sbQuery.Append(" AND fun_get_emp_ref_val('" + searchBy + "', emphdr.empHdrID) = '" + searchValue + "' ");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("siteNo", siteID, MyDbType.String)
                });

            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Update User Has Reporting Functionality
        /// </summary>
        /// <param name="userID">Pass User ID </param>
        /// <param name="siteValue">Pass Site Value</param>
        /// <returns>True/False</returns>
        public Boolean UpdateEmployeeHasFunctionalityStatus(int userID, string siteValue)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                object rvalue = null;


                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select empHdrID from employeehasfunctionality ");
                sbInsertQuery.Append(" where empHdrID = @empHdrID ");
                rvalue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empHdrID", userID, MyDbType.Int),   
                            });
                if (BusinessUtility.GetInt(rvalue) > 0)
                {
                    sbInsertQuery = new StringBuilder();
                    //sbInsertQuery.Append(" update employeehasfunctionality set reporting = fun_is_emp_functionality(empHdrID,4, @siteValue) where empHdrID = @empHdrID  ");
                    //sbInsertQuery.Append(" update employeehasfunctionality set reporting = fun_is_emp_functionality(empHdrID,4, '') where empHdrID = @empHdrID  ");
                    sbInsertQuery.Append(" update employeehasfunctionality set reporting = " + EligibleToReportingModule(userID, siteValue) + " where empHdrID = @empHdrID  ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empHdrID", userID, MyDbType.Int), 
                            DbUtility.GetParameter("siteValue", siteValue, MyDbType.String), 
                            });
                }
                else
                {

                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" insert INTO employeehasfunctionality  ");
                    sbInsertQuery.Append(" (empHdrID) ");
                    sbInsertQuery.Append(" VALUES (@empHdrID) ");


                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empHdrID", userID, MyDbType.Int), 
                            });


                    sbInsertQuery = new StringBuilder();
                    //sbInsertQuery.Append(" update employeehasfunctionality set reporting = fun_is_emp_functionality(empHdrID,4, @siteValue) where empHdrID = @empHdrID  ");
                    //sbInsertQuery.Append(" update employeehasfunctionality set reporting = fun_is_emp_functionality(empHdrID,4, '') where empHdrID = @empHdrID  ");

                    sbInsertQuery.Append(" update employeehasfunctionality set reporting = " + EligibleToReportingModule(userID, siteValue) + " where empHdrID = @empHdrID  ");


                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empHdrID", userID, MyDbType.Int), 
                            DbUtility.GetParameter("siteValue", siteValue, MyDbType.String), 
                            });
                }






                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Employee Name Belong to Functionality
        /// </summary>
        /// <param name="refCode">Pass Ref Code</param>
        /// <param name="refCodeValue">Pass Ref Code Value</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <param name="listID">Pass List ID</param>
        /// <returns>String</returns>
        public string GetUserSiteRetailWRAssgined(string refCode, string refCodeValue, int actionID, int listID)
        {
            if (refCodeValue != "")
                refCodeValue = refCodeValue.PadLeft(4, '0');

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                //sbInsertQuery.Append(" select  ");
                //sbInsertQuery.Append(" GROUP_CONCAT(CONCAT(CONCAT_WS(' ', empFirstName, empLastName),  ' (',  empExtID,  ')')  SEPARATOR ', ') empDetail   ");
                //sbInsertQuery.Append(" from ( ");
                //sbInsertQuery.Append(" SELECT   empExtID, empFirstName, empLastName, fun_get_emp_ref_val( @refCode, empHdrID) refCodeValue ");
                //sbInsertQuery.Append(" from empheader ");
                //sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = empheader.empHdrID and  fun_get_emp_ref_val( @refCode, empHdrID) = @refCodeValue and empFun.reporting >0 ");
                //sbInsertQuery.Append(" ) as result ");
                //sbInsertQuery.Append(" GROUP BY refCodeValue ");
                //object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                //                    DbUtility.GetParameter("refCode", refCode, MyDbType.String),    
                //                    DbUtility.GetParameter("refCodeValue", refCodeValue, MyDbType.String),    
                //                    DbUtility.GetParameter("actionID", actionID, MyDbType.Int)
                //                });

                sbInsertQuery.Append(" set group_concat_max_len = 90000; ");
                sbInsertQuery.Append(" select   GROUP_CONCAT(CONCAT(CONCAT_WS(' ', empFirstName, empLastName),  ' (',  CONCAT_WS('-',  empExtID , refCodeValue),  ')')  SEPARATOR ', ') empDetail  ");
                sbInsertQuery.Append(" from (   ");

                sbInsertQuery.Append(" select * from  ");
                sbInsertQuery.Append(" ( ");
                sbInsertQuery.Append(" SELECT   empExtID, empFirstName, empLastName , iqSite.refCodeValue    ");
                sbInsertQuery.Append(" from empheader   ");
                sbInsertQuery.Append(" join (  ");
                sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as refCodeValue, empHeader_empHdrID as iEmpHdrID  ");
                sbInsertQuery.Append(" from empreference   ");
                sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = @refCode  ");
                sbInsertQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empheader.empHdrID  and iqSite.refCodeValue = @refCodeValue ");
                sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = empheader.empHdrID    ");
                if (actionID == (int)RoleAction.Reporting)
                {
                    sbInsertQuery.Append(" and empFun.reporting >0   ");
                }

                sbInsertQuery.Append(" and empheader.empHdrID NOT IN ");
                sbInsertQuery.Append(" ( ");
                sbInsertQuery.Append(" select empHdrID    ");
                sbInsertQuery.Append(" from  empmstlistexcl lstexcl    ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID= @listID  ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue   ");
                sbInsertQuery.Append(" ) ");

                sbInsertQuery.Append(" and (empheader.isDeleted = 0 or empheader.isDeleted is null) and empheader.empActive = 1 and empheader.empHdrID not in(select empHeaderID from emphdrstatus where empExcludeReports =1)  ");

                if (listID == 1)
                {
                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'   ");
                    sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");

                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                    sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                    sbInsertQuery.Append(" and	((iqDiv.empDivID = 'RETL')   OR  (iqDiv.empDivID ='WR')  )  and ( iqJob.empJobID)  ");
                    //sbInsertQuery.Append(" and	((fun_get_emp_ref_val( 'DIV', empheader.empHdrID) ='RETL')   OR  (fun_get_emp_ref_val( 'DIV', empheader.empHdrID) ='WR')  )  and (fun_get_emp_ref_val( 'JOB', empheader.empHdrID) )   ");
                    sbInsertQuery.Append(" in (   ");
                    sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,   ");
                    sbInsertQuery.Append(" 0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,   ");
                    sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,   ");
                    sbInsertQuery.Append(" 1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760,   ");
                    sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,   ");
                    sbInsertQuery.Append(" 1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921,   ");
                    sbInsertQuery.Append(" 1930,1931,1940,1941,1950   ");
                    sbInsertQuery.Append(" )     ");
                }
                else
                {

                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'   ");
                    sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");

                    sbInsertQuery.Append(" join (  ");
                    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                    sbInsertQuery.Append(" from empreference   ");
                    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                    sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                    /*sbInsertQuery.Append(" and	((iqDiv.empDivID = 'LOGS')   )  and ( iqJob.empJobID)  ");
                    sbInsertQuery.Append(" in (     ");
                    sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,  0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,    ");
                    sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,  1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760, ");
                    sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,  1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921, ");
                    sbInsertQuery.Append(" 1930,1931,1940,1941,1950     ");
                    sbInsertQuery.Append(" )   ");
                     */
                }


                sbInsertQuery.Append(" union  ");
                sbInsertQuery.Append(" select emphdr.empExtID, emphdr.empFirstName, emphdr.empLastName ,  sysRefCodeValue as siteno   ");
                sbInsertQuery.Append(" from  empmstlistincl lstIncl  ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstIncl.empMstListTblID  and  mstLstTbl.mstListID=@listID  ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstIncl.empListInclEmpID and lstIncl.sysRefCode = @refCode and  lstIncl.sysrefCodeValue = @refCodeValue  ");
                sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = emphdr.empHdrID     ");
                if (actionID == (int)RoleAction.Reporting)
                {
                    sbInsertQuery.Append(" and empFun.reporting >0   ");
                }

                sbInsertQuery.Append(" and (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1  and emphdr.empHdrID NOT IN  ");
                sbInsertQuery.Append(" (  ");
                sbInsertQuery.Append(" select empHdrID     ");
                sbInsertQuery.Append(" from  empmstlistexcl lstexcl     ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue    ");
                sbInsertQuery.Append(" )  ");

                sbInsertQuery.Append("  and emphdr.empHdrID not in(select empHeaderID from emphdrstatus where empExcludeReports =1 ) ");

                //if (listID == 1)
                //{
                //    sbInsertQuery.Append("  join ( ");
                //    sbInsertQuery.Append("  SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID  ");
                //    sbInsertQuery.Append("  from empreference  ");
                //    sbInsertQuery.Append("  WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'  ");
                //    sbInsertQuery.Append("  ) as iqDiv on iqDiv.iEmpHdrID = emphdr.empHdrID  ");


                //    sbInsertQuery.Append("  join ( ");
                //    sbInsertQuery.Append("  SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID  ");
                //    sbInsertQuery.Append("  from empreference  ");
                //    sbInsertQuery.Append("  WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'  ");
                //    sbInsertQuery.Append("  ) as iqJob on iqJob.iEmpHdrID = emphdr.empHdrID    ");

                //    sbInsertQuery.Append("  and	((iqDiv.empDivID = 'RETL')   OR  (iqDiv.empDivID ='WR')  )  and (iqJob.empJobID )  ");
                //    sbInsertQuery.Append(" in (   ");
                //    sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,   ");
                //    sbInsertQuery.Append(" 0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,   ");
                //    sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,   ");
                //    sbInsertQuery.Append(" 1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760,   ");
                //    sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,   ");
                //    sbInsertQuery.Append(" 1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921,   ");
                //    sbInsertQuery.Append(" 1930,1931,1940,1941,1950   ");
                //    sbInsertQuery.Append(" )     ");
                //}
                //else
                //{
                //    sbInsertQuery.Append("  join ( ");
                //    sbInsertQuery.Append("  SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID  ");
                //    sbInsertQuery.Append("  from empreference  ");
                //    sbInsertQuery.Append("  WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'  ");
                //    sbInsertQuery.Append("  ) as iqDiv on iqDiv.iEmpHdrID = emphdr.empHdrID  ");


                //    sbInsertQuery.Append("  join ( ");
                //    sbInsertQuery.Append("  SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID  ");
                //    sbInsertQuery.Append("  from empreference  ");
                //    sbInsertQuery.Append("  WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'  ");
                //    sbInsertQuery.Append("  ) as iqJob on iqJob.iEmpHdrID = emphdr.empHdrID    ");

                //    sbInsertQuery.Append("  and	((iqDiv.empDivID = 'LOGS')  )  and (iqJob.empJobID )  ");
                //    sbInsertQuery.Append(" in (     ");
                //    sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,  0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631, ");
                //    sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,  1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760,  ");
                //    sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,  1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921,   ");
                //    sbInsertQuery.Append(" 1930,1931,1940,1941,1950    ");
                //    sbInsertQuery.Append(" ) ");
                //}

                sbInsertQuery.Append(" ) ");
                sbInsertQuery.Append(" as ordersearchresult order by  empfirstname , empLastName ");

                sbInsertQuery.Append(" ) as result  GROUP BY refCodeValue  ");

                //ErrorLog.createLog("Start DateTime "+ DateTime.Now.ToString());
                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("refCode", refCode, MyDbType.String),    
                        DbUtility.GetParameter("refCodeValue", refCodeValue, MyDbType.String),    
                        DbUtility.GetParameter("actionID", actionID, MyDbType.Int),
                        DbUtility.GetParameter("listID", listID, MyDbType.Int)
                    });
                //ErrorLog.createLog("End DateTime "+  DateTime.Now.ToString());
                //ErrorLog.createLog("Query " + BusinessUtility.GetString(sbInsertQuery));
                //ErrorLog.createLog("Param refCode : "+ refCode +" refCodeValue "+ refCodeValue +" actionID = "+ actionID +"  listID = "+ listID + " ");

                return BusinessUtility.GetString(rValue);
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get User List Assigned To Site Logistics
        /// </summary>
        /// <param name="refCode">Pass Ref Code</param>
        /// <param name="refCodeValue">Pass Ref Code Value</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <param name="listID">Pass List ID</param>
        /// <returns>String</returns>
        public string GetUserSiteLogisticsAssgined(string refCode, string refCodeValue, int actionID, int listID)
        {
            if (refCodeValue != "")
                refCodeValue = refCodeValue.PadLeft(4, '0');

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                //sbInsertQuery.Append(" select  ");
                //sbInsertQuery.Append(" GROUP_CONCAT(CONCAT(CONCAT_WS(' ', empFirstName, empLastName),  ' (',  empExtID,  ')')  SEPARATOR ', ') empDetail   ");
                //sbInsertQuery.Append(" from ( ");
                //sbInsertQuery.Append(" SELECT   empExtID, empFirstName, empLastName, fun_get_emp_ref_val( @refCode, empHdrID) refCodeValue ");
                //sbInsertQuery.Append(" from empheader ");
                //sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = empheader.empHdrID and  fun_get_emp_ref_val( @refCode, empHdrID) = @refCodeValue and empFun.reporting >0 ");
                //sbInsertQuery.Append(" ) as result ");
                //sbInsertQuery.Append(" GROUP BY refCodeValue ");
                //object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                //                    DbUtility.GetParameter("refCode", refCode, MyDbType.String),    
                //                    DbUtility.GetParameter("refCodeValue", refCodeValue, MyDbType.String),    
                //                    DbUtility.GetParameter("actionID", actionID, MyDbType.Int)
                //                });

                sbInsertQuery.Append(" set group_concat_max_len = 90000; ");
                sbInsertQuery.Append(" select   GROUP_CONCAT(CONCAT(CONCAT_WS(' ', empFirstName, empLastName),  ' (',  empExtID,  ')')  SEPARATOR ', ') empDetail  ");
                sbInsertQuery.Append(" from (   ");

                sbInsertQuery.Append(" select distinct * from  ");
                sbInsertQuery.Append(" ( ");
                /* sbInsertQuery.Append(" SELECT   empExtID, empFirstName, empLastName , iqSite.refCodeValue    ");
                 sbInsertQuery.Append(" from empheader   ");
                 sbInsertQuery.Append(" join (  ");
                 sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as refCodeValue, empHeader_empHdrID as iEmpHdrID  ");
                 sbInsertQuery.Append(" from empreference   ");
                 sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = @refCode  ");
                 sbInsertQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empheader.empHdrID  and iqSite.refCodeValue = @refCodeValue ");
                 sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = empheader.empHdrID    ");
                 if (actionID == (int)RoleAction.Reporting)
                 {
                     sbInsertQuery.Append(" and empFun.reporting >0   ");
                 }

                 sbInsertQuery.Append(" and empheader.empHdrID NOT IN ");
                 sbInsertQuery.Append(" ( ");
                 sbInsertQuery.Append(" select empHdrID    ");
                 sbInsertQuery.Append(" from  empmstlistexcl lstexcl    ");
                 sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue   ");
                 sbInsertQuery.Append(" ) ");

                 sbInsertQuery.Append(" and empheader.empHdrID not in(select empHeaderID from emphdrstatus where empExcludeReports =1)  ");

                 if (listID == 1)
                 {

                     sbInsertQuery.Append(" join (  ");
                     sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                     sbInsertQuery.Append(" from empreference   ");
                     sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'   ");
                     sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");

                     sbInsertQuery.Append(" join (  ");
                     sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                     sbInsertQuery.Append(" from empreference   ");
                     sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                     sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                     sbInsertQuery.Append(" and	((iqDiv.empDivID = 'RETL')   OR  (iqDiv.empDivID ='WR')  )  and ( iqJob.empJobID)  ");
                     //sbInsertQuery.Append(" and	((fun_get_emp_ref_val( 'DIV', empheader.empHdrID) ='RETL')   OR  (fun_get_emp_ref_val( 'DIV', empheader.empHdrID) ='WR')  )  and (fun_get_emp_ref_val( 'JOB', empheader.empHdrID) )   ");
                     sbInsertQuery.Append(" in (   ");
                     sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,   ");
                     sbInsertQuery.Append(" 0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,   ");
                     sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,   ");
                     sbInsertQuery.Append(" 1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760,   ");
                     sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,   ");
                     sbInsertQuery.Append(" 1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921,   ");
                     sbInsertQuery.Append(" 1930,1931,1940,1941,1950   ");
                     sbInsertQuery.Append(" )     ");
                 }
                 else
                 {

                     sbInsertQuery.Append(" join (  ");
                     sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                     sbInsertQuery.Append(" from empreference   ");
                     sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'   ");
                     sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");

                     sbInsertQuery.Append(" join (  ");
                     sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                     sbInsertQuery.Append(" from empreference   ");
                     sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                     sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                     sbInsertQuery.Append(" and	((iqDiv.empDivID = 'LOGS')   )  and ( iqJob.empJobID)  ");
                     sbInsertQuery.Append(" in (     ");
                     sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,  0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,    ");
                     sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,  1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760, ");
                     sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,  1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921, ");
                     sbInsertQuery.Append(" 1930,1931,1940,1941,1950     ");
                     sbInsertQuery.Append(" )   ");
                 }


                 sbInsertQuery.Append(" union  ");*/
                sbInsertQuery.Append(" select emphdr.empExtID, emphdr.empFirstName, emphdr.empLastName ,  sysRefCodeValue as refCodeValue  ");
                sbInsertQuery.Append(" from  empmstlistincl lstIncl  ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstIncl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstIncl.empListInclEmpID and lstIncl.sysRefCode = @refCode and  lstIncl.sysrefCodeValue = @refCodeValue  ");
                sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = emphdr.empHdrID     ");
                if (actionID == (int)RoleAction.Reporting)
                {
                    sbInsertQuery.Append(" and empFun.reporting >0   ");
                }

                sbInsertQuery.Append(" and emphdr.empHdrID NOT IN  ");
                sbInsertQuery.Append(" (  ");
                sbInsertQuery.Append(" select empHdrID     ");
                sbInsertQuery.Append(" from  empmstlistexcl lstexcl     ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue    ");
                sbInsertQuery.Append(" )  ");

                sbInsertQuery.Append("  and (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1 and emphdr.empHdrID not in(select empHeaderID from emphdrstatus where empExcludeReports =1 ) ");
                sbInsertQuery.Append(" ) ");
                sbInsertQuery.Append(" as ordersearchresult order by  empfirstname , empLastName ");

                sbInsertQuery.Append(" ) as result  GROUP BY refCodeValue  ");
                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("refCode", refCode, MyDbType.String),    
                        DbUtility.GetParameter("refCodeValue", refCodeValue, MyDbType.String),    
                        DbUtility.GetParameter("actionID", actionID, MyDbType.Int),
                        DbUtility.GetParameter("listID", listID, MyDbType.Int),
                    });

                return BusinessUtility.GetString(rValue);
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get List Name
        /// </summary>
        /// <param name="listID">Pass List ID</param>
        /// <returns>String</returns>
        public string GetManageListName(int listID)
        {
            string listName = string.Empty;
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select listname from sysmanagelist where idsysmanagelist= @listID ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("listID", listID , MyDbType.Int),
                });
                listName = BusinessUtility.GetString(rValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return listName;
        }

        /// <summary>
        /// To Get Employee Eligible For Reporting Module
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="siteValue">Pass Site Value</param>
        /// <returns>True/False</returns>
        public int EligibleToReportingModule(int empID, string siteValue)
        {
            int listName = 0;
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();

                sbInsertQuery.Append(" select count(*)  ");
                sbInsertQuery.Append(" FROM( ");

                sbInsertQuery.Append(" SELECT ");
                sbInsertQuery.Append(" eItem.ActionItem ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysroletypeactionitems AS eItem ");
                sbInsertQuery.Append(" INNER JOIN ");
                sbInsertQuery.Append(" emproletypes AS eType ON eType.empRoles_ActionItem = eItem.ActionItem ");
                sbInsertQuery.Append(" AND eType.empRoles_empRoleID IN (SELECT  ");
                sbInsertQuery.Append(" empRoles_empRoleID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" emprolesincludes ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" empHeader_empHdrID = @empID and empHeader_empHdrID NOT IN(select empListExclEmpID from empmstlistexcl where functionalityID = @actionID and empListExclEmpID =@empID and sysRefCodeValue =@siteValue )) ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" eItem.ActionItem = @actionID ");
                sbInsertQuery.Append(" UNION SELECT ");
                sbInsertQuery.Append(" eItem.ActionItem ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysrolerefsel AS syroleSel ");
                sbInsertQuery.Append(" JOIN ");
                sbInsertQuery.Append(" empreference eRef ON eRef.sysEmpReferenceCodes_sysRefCode = syroleSel.sys_ref_code ");
                sbInsertQuery.Append(" AND eRef.empHeader_empHdrID = @empID ");
                sbInsertQuery.Append(" and eRef.empHeader_empHdrID NOT IN(select empListExclEmpID from empmstlistexcl where functionalityID = @actionID and empListExclEmpID =@empID and sysRefCodeValue =@siteValue ) ");
                sbInsertQuery.Append(" AND (syroleSel.sys_cond IS NULL ");
                sbInsertQuery.Append(" OR syroleSel.sys_cond = 'OR') ");
                sbInsertQuery.Append(" AND (((SELECT  ");
                sbInsertQuery.Append(" COUNT(*) ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysrolerefsel AS AND1syroleSel ");
                sbInsertQuery.Append(" JOIN ");
                sbInsertQuery.Append(" empreference eRef1 ON eRef1.sysEmpReferenceCodes_sysRefCode = AND1syroleSel.sys_ref_code ");
                sbInsertQuery.Append(" AND eRef1.empHeader_empHdrID = @empID  ");
                sbInsertQuery.Append(" and eRef1.empHeader_empHdrID NOT IN(select empListExclEmpID from empmstlistexcl where functionalityID = @actionID and empListExclEmpID =@empID and sysRefCodeValue =@siteValue ) ");
                sbInsertQuery.Append(" AND AND1syroleSel.sys_cond = 'AND' ");
                sbInsertQuery.Append(" AND AND1syroleSel.sys_cond IS NOT NULL ");
                sbInsertQuery.Append(" AND AND1syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(AND1syroleSel.sys_ref_code, ");
                sbInsertQuery.Append(" eRef1.empHeader_empHdrID) ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" AND1syroleSel.emp_roleid = syroleSel.emp_roleid) = (SELECT  ");
                sbInsertQuery.Append(" COUNT(*) ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysrolerefsel AS AND2syroleSel ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbInsertQuery.Append(" AND AND2syroleSel.sys_cond = 'AND') ");
                sbInsertQuery.Append(" AND (SELECT ");
                sbInsertQuery.Append(" COUNT(*) ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysrolerefsel AS AND2syroleSel ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbInsertQuery.Append(" AND AND2syroleSel.sys_cond = 'AND') > 0) ");
                sbInsertQuery.Append(" OR ((SELECT ");
                sbInsertQuery.Append(" COUNT(*) ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysrolerefsel AS ORsyroleSel ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" ORsyroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbInsertQuery.Append(" AND ORsyroleSel.sys_cond = 'AND') = 0)) ");
                sbInsertQuery.Append(" AND (syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(syroleSel.sys_ref_code, ");
                sbInsertQuery.Append(" empHeader_empHdrID)) ");
                sbInsertQuery.Append(" INNER JOIN ");
                sbInsertQuery.Append(" emproletypes AS eType ON eType.empRoles_empRoleID = syroleSel.emp_roleid ");
                sbInsertQuery.Append(" INNER JOIN ");
                sbInsertQuery.Append(" sysroletypeactionitems AS eItem ON eType.empRoles_ActionItem = eItem.ActionItem ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" eItem.ActionItem = @actionID ");

                sbInsertQuery.Append(" UNION ");

                sbInsertQuery.Append(" SELECT ");
                sbInsertQuery.Append(" eItem.ActionItem ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" sysroletypeactionitems AS eItem ");
                sbInsertQuery.Append(" INNER JOIN ");
                sbInsertQuery.Append(" empmstlistincl AS eType ON eType.functionalityID = eItem.ActionItem and eItem.ActionItem = @actionID  and empListInclEmpID = @empID  ");

                if (ToCheckDefaultUserHaveReportingModule(empID, siteValue) > 0)
                {
                    sbInsertQuery.Append(" union ");

                    sbInsertQuery.Append(" SELECT ");
                    sbInsertQuery.Append(" eItem.ActionItem ");
                    sbInsertQuery.Append(" FROM ");
                    sbInsertQuery.Append(" sysroletypeactionitems AS eItem where eItem.ActionItem = 4  ");
                }


                sbInsertQuery.Append(" ) as EmpEligableforCourse ");

                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empID", empID , MyDbType.Int),
                DbUtility.GetParameter("actionID", 4 , MyDbType.Int),
                DbUtility.GetParameter("siteValue", siteValue , MyDbType.String),
                });
                return BusinessUtility.GetInt(rValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
            return listName;
        }

        /// <summary>
        /// To Check Default User Have Reporting Module
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <param name="siteValue">Pass Site Value</param>
        /// <returns>Int</returns>
        public int ToCheckDefaultUserHaveReportingModule(int empID, string siteValue)
        {
            int listName = 0;
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();

                sbInsertQuery.Append(" SELECT count(*) from ( ");

                sbInsertQuery.Append(" SELECT  ");
                sbInsertQuery.Append(" empHdrID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empheader ");
                sbInsertQuery.Append(" JOIN (SELECT  ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
                sbInsertQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empreference ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'STR') AS iqSite ON iqSite.iEmpHdrID = @empID ");
                sbInsertQuery.Append(" AND empheader.empHdrID NOT IN (SELECT  ");
                sbInsertQuery.Append(" empHdrID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empmstlistexcl lstexcl ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");

                sbInsertQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = @empID ");
                sbInsertQuery.Append(" AND lstexcl.sysRefCode = 'STR' ");
                sbInsertQuery.Append(" AND lstexcl.sysrefCodeValue = @siteValue) ");
                sbInsertQuery.Append(" AND empheader.empHdrID NOT IN (SELECT  ");
                sbInsertQuery.Append(" empHeaderID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" emphdrstatus ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" empExcludeReports = 1) ");
                sbInsertQuery.Append(" JOIN (SELECT  ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empDivID, ");
                sbInsertQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empreference ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'DIV') AS iqDiv ON iqDiv.iEmpHdrID = @empID ");
                sbInsertQuery.Append(" JOIN (SELECT  ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empJobID, ");
                sbInsertQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empreference ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'JOB') AS iqJob ON iqJob.iEmpHdrID = @empID ");
                sbInsertQuery.Append(" AND ((iqDiv.empDivID = 'RETL') ");
                sbInsertQuery.Append(" OR (iqDiv.empDivID = 'WR')) ");
                sbInsertQuery.Append(" AND (iqJob.empJobID) IN (0030 , 0095, 0401, 0415, 0419, 0425, 0810, 0815, 0819, 0820, 0821, 0824, 0829, 0830, 1002, 1004, 1602, 1609, 1610,  ");
                sbInsertQuery.Append(" 1612, 1616, 1619, 1621, 1631, 1635, 1640, 1653, 1654, 1656, 1660, 1666, 1670, 1681, 1682, 1694, 1708, 1709, 1710, 1711, 1713, 1716, 1724, 1725,  ");
                sbInsertQuery.Append(" 1726, 1730, 1740, 1749, 1760, 1761, 1762, 1767, 1768, 1770, 1772, 1777, 1782, 1789, 1790, 1800, 1801, 1804, 1807, 1813, 1820, 1823, 1826, 1847,  ");
                sbInsertQuery.Append(" 1867, 1910, 1911, 1920, 1921, 1930, 1931, 1940, 1941, 1950)  ");


                sbInsertQuery.Append(" ) as result ");

                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("empID", empID , MyDbType.Int),
                DbUtility.GetParameter("siteValue", siteValue , MyDbType.String),
                });
                listName = BusinessUtility.GetInt(rValue);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }

            return listName;

        }

        /// <summary>
        /// To Upate Employee C Site
        /// </summary>
        /// <param name="userID">Pass User ID</param>
        /// <returns>True?False</returns>
        public Boolean ToUpdateEmployeeCSites(int userID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                object rvalue = null;


                sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select empHdrID from csite ");
                sbInsertQuery.Append(" where empHdrID = @empHdrID ");
                rvalue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empHdrID", userID, MyDbType.Int),   
                            });
                if (BusinessUtility.GetInt(rvalue) > 0)
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" delete from csite ");
                    sbInsertQuery.Append(" where empHdrID = @empHdrID ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empHdrID", userID, MyDbType.Int), 
                            });
                }


                Employee objEmp = new Employee();
                DataTable dt = objEmp.GetEmployeeSites(userID);
                if (dt.Rows.Count > 0)
                {

                    foreach (DataRow dRow in dt.Rows)
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" insert INTO csite  ");
                        sbInsertQuery.Append(" (empHdrID, sysRefCode, sysRefCodeValue ) ");
                        sbInsertQuery.Append(" VALUES (@empHdrID, @sysRefCode, @sysRefCodeValue ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empHdrID", userID, MyDbType.Int), 
                    DbUtility.GetParameter("sysRefCode", EmpSearchBy.Store, MyDbType.String), 
                    DbUtility.GetParameter("sysRefCodeValue", BusinessUtility.GetString(dRow["SiteNo"]), MyDbType.String), 
                            });
                    }
                }
                else
                {
                    string sEmployeeSite = objEmp.GetEmpRefCodeValue(userID, EmpSearchBy.Store);
                    if (sEmployeeSite != "")
                    {
                        sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" insert INTO csite  ");
                        sbInsertQuery.Append(" (empHdrID, sysRefCode, sysRefCodeValue ) ");
                        sbInsertQuery.Append(" VALUES (@empHdrID, @sysRefCode, @sysRefCodeValue ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("empHdrID", userID, MyDbType.Int), 
                    DbUtility.GetParameter("sysRefCode", EmpSearchBy.Store, MyDbType.String), 
                    DbUtility.GetParameter("sysRefCodeValue", sEmployeeSite, MyDbType.String), 
                            });
                    }
                }

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Cross Docks Employee List
        /// </summary>
        /// <param name="searchText">Pass Search Text</param>
        /// <param name="refCode">Pass sysRefCode</param>
        /// <returns>Datatable</returns>
        public DataTable GetCrossDocksEmployeeList(string searchText, string refCode)
        {
            DataTable dtEmployeeList = new DataTable();
            StringBuilder sbQuery = new StringBuilder();
            StringBuilder sQuery = new StringBuilder();

            sbQuery = new StringBuilder();
            sQuery = new StringBuilder();
            //sQuery.Append(" SELECT empheader.empHdrID,  empExtID, empFirstName, empLastName , iqSite.refCodeValue as empSite, csite.sysRefCodeValue as InclSiteReorting     ");
            //sQuery.Append(" from empheader    ");
            //sQuery.Append(" join (   ");
            //sQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as refCodeValue, empHeader_empHdrID as iEmpHdrID   ");
            //sQuery.Append(" from empreference    ");
            //sQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = @refCode   ");
            //sQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empheader.empHdrID   ");
            //sQuery.Append(" JOIN    csite  on csite.empHdrID = empheader.empHdrID    and csite.sysRefCode =@refCode ");
            //sQuery.Append(" WHERE 1=1 ");

            sQuery.Append(" SELECT empheader.empHdrID,  empExtID, empFirstName, empLastName , iqSite.refCodeValue as empSite, iqCSite.refCodeValue as InclSiteReorting    ");
            sQuery.Append(" from empheader     ");
            sQuery.Append(" join (    ");
            sQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as refCodeValue, empHeader_empHdrID as iEmpHdrID    ");
            sQuery.Append(" from empreference     ");
            sQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'STR'    ");
            sQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empheader.empHdrID    ");
            sQuery.Append(" JOIN      ");
            sQuery.Append(" (    ");
            sQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as refCodeValue, empHeader_empHdrID as iEmpHdrID    ");
            sQuery.Append(" from empreference     ");
            sQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'CSITE'    ");
            sQuery.Append(" ) as iqCSite on iqCSite.iEmpHdrID = empheader.empHdrID   ");

            sQuery.Append(" where empExtID  IN(  ");
            sQuery.Append(" 03565, 03576, 03749, 03785, 03937, 03956, 03971, 03986, 03990, 04015, 04110, 04111, 04283, 04316, 04343, 04477, 04516, 04547, 04582, 04624, 04758,   ");
            sQuery.Append(" 04826, 04842, 04868, 04979, 05001, 05722, 05972, 06393, 06585, 06640, 06719, 06761, 06804, 07072, 07073, 07148, 07173, 07256, 07403, 07438, 41994,   ");
            sQuery.Append(" 44156, 44648, 51408, 61402, 61951, 62282, 62380, 63073, 63085, 63453, 63872, 63887, 63888, 64411, 64445, 64455, 64456, 65028, 67464, 69022, 69925,   ");
            sQuery.Append(" 70019, 70471, 75210, 75218, 75219, 75468, 75572, 75616, 75950, 75960, 76070, 76075, 76080, 76082, 76092, 76264, 76284, 76439, 76442, 76443, 76444,   ");
            sQuery.Append(" 03681, 04023, 04074, 04079, 04127, 04137, 04150, 04399, 04568, 04621, 04660, 04707, 04978, 05046, 05048, 05899, 06074, 06109, 06320, 06326, 06530,   ");
            sQuery.Append(" 06721, 06729, 07480, 07546, 07547, 43681, 44462, 44589, 55561, 56164, 56166, 56199, 57170, 57197, 58386, 66457, 66713, 67087, 67517, 03393, 03752,   ");
            sQuery.Append(" 03942, 03944, 03965, 04005, 04092, 04218, 04251, 04295, 04353, 04422, 04425, 04432, 04444, 04512, 04535, 04548, 04665, 04751, 04820, 04919, 04962,   ");
            sQuery.Append(" 05060, 05061, 05305, 05621, 06144, 06501, 06671, 06789, 06805, 06873, 07199, 07518, 07545, 53134, 61641, 62183, 62188, 62761, 62789, 62939, 63000,   ");
            sQuery.Append(" 63475, 63824, 64396, 64463, 64676, 64785, 68869, 75173, 75375, 75644, 75785, 76050, 76366, 76598, 04641, 05268, 05632, 06145, 06519, 06555, 60754,   ");
            sQuery.Append(" 64743, 64759, 64793, 64876, 75542, 75579, 75588, 76021, 76054, 76117, 76514, 76524, 76525, 76669, 03922, 04030, 04059, 04205, 04586, 05030, 06661,   ");
            sQuery.Append(" 63683, 75012, 76170, 76342, 03819, 04259, 04960, 04999, 05297, 06373, 61282, 62594, 62810, 68191, 76670, 03407, 04435, 04457, 04558, 04787, 04863,   ");
            sQuery.Append(" 04956, 05023, 05460, 06359, 06360, 06701, 06780, 06781, 06847, 07059, 07555, 48540, 61631, 63515, 64031, 64102, 64103, 75067, 75451, 75594, 76665,   ");
            sQuery.Append(" 04639, 04822, 04891, 05022, 06093, 06257, 07004, 07364, 60122, 60685, 70035, 75601, 75705, 76151, 76292, 76512, 76513, 76563, 04213, 04288, 07511,   ");
            sQuery.Append(" 07542, 68853, 71993, 72115, 72116, 72321, 07200  ");

            sQuery.Append(" )  ");



            if (BusinessUtility.GetString(searchText) != "")
            {
                sbQuery.Append(sQuery);
                sbQuery.Append("AND ( EmpFirstName = '" + searchText + "' OR EmpLastName = '" + searchText + "' OR  concat_ws(' ', trim(empfirstname), trim(empLastname)) = '" + searchText + "'  OR  concat_ws(' ', trim(empfirstname), trim(empLastname), trim(EmpExtID)) = '" + searchText + "'  OR EmpExtID = '" + searchText + "') ");

                string[] sEmpName = searchText.Split(' ');
                string empClause = "";

                if (sEmpName.Length > 2)
                {
                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and (EmpExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "' OR  EmpExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ) ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' and  EmpExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                }
                else if (sEmpName.Length == 2)
                {

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and (EmpExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "'  ) ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' AND  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                }
                else if (sEmpName.Length == 1)
                {
                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and (EmpExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ) ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ");

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append(" and EmpFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  EmpLastName = '" + BusinessUtility.GetString(sEmpName[0]) + "' ");
                }

                sbQuery.Append(" union ");
                sbQuery.Append(sQuery);
                sbQuery.Append("AND (  ");

                sEmpName = searchText.Split(' ');
                empClause = "";
                foreach (string sName in sEmpName)
                {
                    if (empClause == "")
                    {
                        empClause += " EmpFirstName = '" + sName + "' OR EmpLastName = '" + sName + "' OR  EmpExtID = '" + sName + "' ";
                    }
                    else
                    {
                        empClause += " OR EmpFirstName = '" + sName + "' OR EmpLastName = '" + sName + "' OR  EmpExtID = '" + sName + "' ";
                    }
                }

                sbQuery.Append(empClause);
                sbQuery.Append(" ) ");

                sbQuery.Append(" union ");
                sbQuery.Append(sQuery);
                sbQuery.Append("AND (  ");

                sEmpName = searchText.Split(' ');
                foreach (string sName in sEmpName)
                {
                    if (empClause == "")
                    {
                        empClause += " EmpFirstName like '%" + sName + "%' OR EmpLastName like '%" + sName + "%' OR  EmpExtID like '%" + sName + "%' ";
                    }
                    else
                    {
                        empClause += " OR EmpFirstName like '%" + sName + "%' OR EmpLastName like '%" + sName + "%' OR  EmpExtID like '%" + sName + "%' ";
                    }
                }

                sbQuery.Append(empClause);
                sbQuery.Append(" ) ");
            }
            else
            {
                sbQuery.Append(sQuery);
            }

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dtEmployeeList = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text,
                    new MySqlParameter[]{
                    
                    DbUtility.GetParameter("refCode", refCode, MyDbType.String),
                            });
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dtEmployeeList;
        }

        /// <summary>
        /// To Get User C Site List
        /// </summary>
        /// <returns>DatTable</returns>
        private DataTable GetFixedUserCSiteList()
        {
            DataTable dtFixedUserCSite = new DataTable();
            dtFixedUserCSite.Columns.Add("empExtID", typeof(string));
            dtFixedUserCSite.Columns.Add("empCSite", typeof(string));
            dtFixedUserCSite.AcceptChanges();
            dtFixedUserCSite.Rows.Add("03565", "4190");
            dtFixedUserCSite.Rows.Add("03576", "4190");
            dtFixedUserCSite.Rows.Add("03749", "4190");
            dtFixedUserCSite.Rows.Add("03785", "4190");
            dtFixedUserCSite.Rows.Add("03937", "4190");
            dtFixedUserCSite.Rows.Add("03956", "4190");
            dtFixedUserCSite.Rows.Add("03971", "4190");
            dtFixedUserCSite.Rows.Add("03986", "4190");
            dtFixedUserCSite.Rows.Add("03990", "4190");
            dtFixedUserCSite.Rows.Add("04015", "3440");
            dtFixedUserCSite.Rows.Add("04110", "3440");
            dtFixedUserCSite.Rows.Add("04111", "3440");
            dtFixedUserCSite.Rows.Add("04283", "3440");
            dtFixedUserCSite.Rows.Add("04316", "4190");
            dtFixedUserCSite.Rows.Add("04343", "4190");
            dtFixedUserCSite.Rows.Add("04477", "3440");
            dtFixedUserCSite.Rows.Add("04516", "4190");
            dtFixedUserCSite.Rows.Add("04547", "3440");
            dtFixedUserCSite.Rows.Add("04582", "4190");
            dtFixedUserCSite.Rows.Add("04624", "4190");
            dtFixedUserCSite.Rows.Add("04758", "3440");
            dtFixedUserCSite.Rows.Add("04826", "4190");
            dtFixedUserCSite.Rows.Add("04842", "3440");
            dtFixedUserCSite.Rows.Add("04868", "4190");
            dtFixedUserCSite.Rows.Add("04979", "4190");
            dtFixedUserCSite.Rows.Add("05001", "4190");
            dtFixedUserCSite.Rows.Add("05722", "3440");
            dtFixedUserCSite.Rows.Add("05972", "3440");
            dtFixedUserCSite.Rows.Add("06393", "3440");
            dtFixedUserCSite.Rows.Add("06585", "4190");
            dtFixedUserCSite.Rows.Add("06640", "3440");
            dtFixedUserCSite.Rows.Add("06719", "3440");
            dtFixedUserCSite.Rows.Add("06761", "3440");
            dtFixedUserCSite.Rows.Add("06804", "3440");
            dtFixedUserCSite.Rows.Add("07072", "3440");
            dtFixedUserCSite.Rows.Add("07073", "3440");
            dtFixedUserCSite.Rows.Add("07148", "4190");
            dtFixedUserCSite.Rows.Add("07173", "3440");
            dtFixedUserCSite.Rows.Add("07256", "3440");
            dtFixedUserCSite.Rows.Add("07403", "3440");
            dtFixedUserCSite.Rows.Add("07438", "3440");
            dtFixedUserCSite.Rows.Add("41994", "4190");
            dtFixedUserCSite.Rows.Add("44156", "4190");
            dtFixedUserCSite.Rows.Add("44648", "4190");
            dtFixedUserCSite.Rows.Add("51408", "4190");
            dtFixedUserCSite.Rows.Add("61402", "3440");
            dtFixedUserCSite.Rows.Add("61951", "4190");
            dtFixedUserCSite.Rows.Add("62282", "3440");
            dtFixedUserCSite.Rows.Add("62380", "3440");
            dtFixedUserCSite.Rows.Add("63073", "3440");
            dtFixedUserCSite.Rows.Add("63085", "3440");
            dtFixedUserCSite.Rows.Add("63453", "4190");
            dtFixedUserCSite.Rows.Add("63872", "4190");
            dtFixedUserCSite.Rows.Add("63887", "4190");
            dtFixedUserCSite.Rows.Add("63888", "4190");
            dtFixedUserCSite.Rows.Add("64411", "3440");
            dtFixedUserCSite.Rows.Add("64445", "3440");
            dtFixedUserCSite.Rows.Add("64455", "3440");
            dtFixedUserCSite.Rows.Add("64456", "3440");
            dtFixedUserCSite.Rows.Add("65028", "4190");
            dtFixedUserCSite.Rows.Add("67464", "3440");
            dtFixedUserCSite.Rows.Add("69022", "4190");
            dtFixedUserCSite.Rows.Add("69925", "4190");
            dtFixedUserCSite.Rows.Add("70019", "4190");
            dtFixedUserCSite.Rows.Add("70471", "3440");
            dtFixedUserCSite.Rows.Add("75210", "4190");
            dtFixedUserCSite.Rows.Add("75218", "4190");
            dtFixedUserCSite.Rows.Add("75219", "4190");
            dtFixedUserCSite.Rows.Add("75468", "4190");
            dtFixedUserCSite.Rows.Add("75572", "4190");
            dtFixedUserCSite.Rows.Add("75616", "4190");
            dtFixedUserCSite.Rows.Add("75950", "4190");
            dtFixedUserCSite.Rows.Add("75960", "3440");
            dtFixedUserCSite.Rows.Add("76070", "3440");
            dtFixedUserCSite.Rows.Add("76075", "4190");
            dtFixedUserCSite.Rows.Add("76080", "3440");
            dtFixedUserCSite.Rows.Add("76082", "4190");
            dtFixedUserCSite.Rows.Add("76092", "4190");
            dtFixedUserCSite.Rows.Add("76264", "4190");
            dtFixedUserCSite.Rows.Add("76284", "4190");
            dtFixedUserCSite.Rows.Add("76439", "3440");
            dtFixedUserCSite.Rows.Add("76442", "3440");
            dtFixedUserCSite.Rows.Add("76443", "3440");
            dtFixedUserCSite.Rows.Add("76444", "3440");
            dtFixedUserCSite.Rows.Add("03681", "4740");
            dtFixedUserCSite.Rows.Add("04023", "4740");
            dtFixedUserCSite.Rows.Add("04074", "4340");
            dtFixedUserCSite.Rows.Add("04079", "4540");
            dtFixedUserCSite.Rows.Add("04127", "4740");
            dtFixedUserCSite.Rows.Add("04137", "4540");
            dtFixedUserCSite.Rows.Add("04150", "4740");
            dtFixedUserCSite.Rows.Add("04399", "4340");
            dtFixedUserCSite.Rows.Add("04568", "4540");
            dtFixedUserCSite.Rows.Add("04621", "4340");
            dtFixedUserCSite.Rows.Add("04660", "4540");
            dtFixedUserCSite.Rows.Add("04707", "4540");
            dtFixedUserCSite.Rows.Add("04978", "4340");
            dtFixedUserCSite.Rows.Add("05046", "4340");
            dtFixedUserCSite.Rows.Add("05048", "4540");
            dtFixedUserCSite.Rows.Add("05899", "4340");
            dtFixedUserCSite.Rows.Add("06074", "4340");
            dtFixedUserCSite.Rows.Add("06109", "4340");
            dtFixedUserCSite.Rows.Add("06320", "4340");
            dtFixedUserCSite.Rows.Add("06326", "4340");
            dtFixedUserCSite.Rows.Add("06530", "4340");
            dtFixedUserCSite.Rows.Add("06721", "4340");
            dtFixedUserCSite.Rows.Add("06729", "4340");
            dtFixedUserCSite.Rows.Add("07480", "4540");
            dtFixedUserCSite.Rows.Add("07546", "4340");
            dtFixedUserCSite.Rows.Add("07547", "4340");
            dtFixedUserCSite.Rows.Add("43681", "4340");
            dtFixedUserCSite.Rows.Add("44462", "4540");
            dtFixedUserCSite.Rows.Add("44589", "4540");
            dtFixedUserCSite.Rows.Add("55561", "4340");
            dtFixedUserCSite.Rows.Add("56164", "4340");
            dtFixedUserCSite.Rows.Add("56166", "4540");
            dtFixedUserCSite.Rows.Add("56199", "4340");
            dtFixedUserCSite.Rows.Add("57170", "4340");
            dtFixedUserCSite.Rows.Add("57197", "4540");
            dtFixedUserCSite.Rows.Add("58386", "4540");
            dtFixedUserCSite.Rows.Add("66457", "4340");
            dtFixedUserCSite.Rows.Add("66713", "4340");
            dtFixedUserCSite.Rows.Add("67087", "4540");
            dtFixedUserCSite.Rows.Add("67517", "4340");
            dtFixedUserCSite.Rows.Add("03393", "4140");
            dtFixedUserCSite.Rows.Add("03752", "3290");
            dtFixedUserCSite.Rows.Add("03942", "3290");
            dtFixedUserCSite.Rows.Add("03944", "3290");
            dtFixedUserCSite.Rows.Add("03965", "3290");
            dtFixedUserCSite.Rows.Add("04005", "4140");
            dtFixedUserCSite.Rows.Add("04092", "3290");
            dtFixedUserCSite.Rows.Add("04218", "3090");
            dtFixedUserCSite.Rows.Add("04251", "3290");
            dtFixedUserCSite.Rows.Add("04295", "4140");
            dtFixedUserCSite.Rows.Add("04353", "4140");
            dtFixedUserCSite.Rows.Add("04422", "3290");
            dtFixedUserCSite.Rows.Add("04425", "3290");
            dtFixedUserCSite.Rows.Add("04432", "3290");
            dtFixedUserCSite.Rows.Add("04444", "3090");
            dtFixedUserCSite.Rows.Add("04512", "3290");
            dtFixedUserCSite.Rows.Add("04535", "4140");
            dtFixedUserCSite.Rows.Add("04548", "3290");
            dtFixedUserCSite.Rows.Add("04665", "3290");
            dtFixedUserCSite.Rows.Add("04751", "3290");
            dtFixedUserCSite.Rows.Add("04820", "3290");
            dtFixedUserCSite.Rows.Add("04919", "4140");
            dtFixedUserCSite.Rows.Add("04962", "3290");
            dtFixedUserCSite.Rows.Add("05060", "4140");
            dtFixedUserCSite.Rows.Add("05061", "4140");
            dtFixedUserCSite.Rows.Add("05305", "4140");
            dtFixedUserCSite.Rows.Add("05621", "4140");
            dtFixedUserCSite.Rows.Add("06144", "3290");
            dtFixedUserCSite.Rows.Add("06501", "3290");
            dtFixedUserCSite.Rows.Add("06671", "3290");
            dtFixedUserCSite.Rows.Add("06789", "4140");
            dtFixedUserCSite.Rows.Add("06805", "3290");
            dtFixedUserCSite.Rows.Add("06873", "3290");
            dtFixedUserCSite.Rows.Add("07199", "3290");
            dtFixedUserCSite.Rows.Add("07518", "3290");
            dtFixedUserCSite.Rows.Add("07545", "3290");
            dtFixedUserCSite.Rows.Add("53134", "4140");
            dtFixedUserCSite.Rows.Add("61641", "3290");
            dtFixedUserCSite.Rows.Add("62183", "3290");
            dtFixedUserCSite.Rows.Add("62188", "3290");
            dtFixedUserCSite.Rows.Add("62761", "3290");
            dtFixedUserCSite.Rows.Add("62789", "3290");
            dtFixedUserCSite.Rows.Add("62939", "4140");
            dtFixedUserCSite.Rows.Add("63000", "3090");
            dtFixedUserCSite.Rows.Add("63475", "4140");
            dtFixedUserCSite.Rows.Add("63824", "3290");
            dtFixedUserCSite.Rows.Add("64396", "3290");
            dtFixedUserCSite.Rows.Add("64463", "3290");
            dtFixedUserCSite.Rows.Add("64676", "3090");
            dtFixedUserCSite.Rows.Add("64785", "3290");
            dtFixedUserCSite.Rows.Add("68869", "4140");
            dtFixedUserCSite.Rows.Add("75173", "3290");
            dtFixedUserCSite.Rows.Add("75375", "3290");
            dtFixedUserCSite.Rows.Add("75644", "4140");
            dtFixedUserCSite.Rows.Add("75785", "3290");
            dtFixedUserCSite.Rows.Add("76050", "4140");
            dtFixedUserCSite.Rows.Add("76366", "3290");
            dtFixedUserCSite.Rows.Add("76598", "3290");
            dtFixedUserCSite.Rows.Add("04641", "3543");
            dtFixedUserCSite.Rows.Add("05268", "3543");
            dtFixedUserCSite.Rows.Add("05632", "3543");
            dtFixedUserCSite.Rows.Add("06145", "3543");
            dtFixedUserCSite.Rows.Add("06519", "3543");
            dtFixedUserCSite.Rows.Add("06555", "3543");
            dtFixedUserCSite.Rows.Add("60754", "3543");
            dtFixedUserCSite.Rows.Add("64743", "3543");
            dtFixedUserCSite.Rows.Add("64759", "3543");
            dtFixedUserCSite.Rows.Add("64793", "3543");
            dtFixedUserCSite.Rows.Add("64876", "3543");
            dtFixedUserCSite.Rows.Add("75542", "3543");
            dtFixedUserCSite.Rows.Add("75579", "3543");
            dtFixedUserCSite.Rows.Add("75588", "3543");
            dtFixedUserCSite.Rows.Add("76021", "3543");
            dtFixedUserCSite.Rows.Add("76054", "3543");
            dtFixedUserCSite.Rows.Add("76117", "3543");
            dtFixedUserCSite.Rows.Add("76514", "3543");
            dtFixedUserCSite.Rows.Add("76524", "3543");
            dtFixedUserCSite.Rows.Add("76525", "3543");
            dtFixedUserCSite.Rows.Add("76669", "3543");
            dtFixedUserCSite.Rows.Add("03922", "3590");
            dtFixedUserCSite.Rows.Add("04030", "3590");
            dtFixedUserCSite.Rows.Add("04059", "3590");
            dtFixedUserCSite.Rows.Add("04205", "3590");
            dtFixedUserCSite.Rows.Add("04586", "3590");
            dtFixedUserCSite.Rows.Add("05030", "3590");
            dtFixedUserCSite.Rows.Add("06661", "3590");
            dtFixedUserCSite.Rows.Add("63683", "3590");
            dtFixedUserCSite.Rows.Add("75012", "3590");
            dtFixedUserCSite.Rows.Add("76170", "3590");
            dtFixedUserCSite.Rows.Add("76342", "3590");
            dtFixedUserCSite.Rows.Add("03819", "3540");
            dtFixedUserCSite.Rows.Add("04259", "3540");
            dtFixedUserCSite.Rows.Add("04960", "3540");
            dtFixedUserCSite.Rows.Add("04999", "3540");
            dtFixedUserCSite.Rows.Add("05297", "3540");
            dtFixedUserCSite.Rows.Add("06373", "3540");
            dtFixedUserCSite.Rows.Add("61282", "3540");
            dtFixedUserCSite.Rows.Add("62594", "3540");
            dtFixedUserCSite.Rows.Add("62810", "3540");
            dtFixedUserCSite.Rows.Add("68191", "3540");
            dtFixedUserCSite.Rows.Add("76670", "3540");
            dtFixedUserCSite.Rows.Add("03407", "3690");
            dtFixedUserCSite.Rows.Add("04435", "3690");
            dtFixedUserCSite.Rows.Add("04457", "3690");
            dtFixedUserCSite.Rows.Add("04558", "3690");
            dtFixedUserCSite.Rows.Add("04787", "3690");
            dtFixedUserCSite.Rows.Add("04863", "3690");
            dtFixedUserCSite.Rows.Add("04956", "3690");
            dtFixedUserCSite.Rows.Add("05023", "3690");
            dtFixedUserCSite.Rows.Add("05460", "3690");
            dtFixedUserCSite.Rows.Add("06359", "3690");
            dtFixedUserCSite.Rows.Add("06360", "3690");
            dtFixedUserCSite.Rows.Add("06701", "3690");
            dtFixedUserCSite.Rows.Add("06780", "3690");
            dtFixedUserCSite.Rows.Add("06781", "3690");
            dtFixedUserCSite.Rows.Add("06847", "3690");
            dtFixedUserCSite.Rows.Add("07059", "3690");
            dtFixedUserCSite.Rows.Add("07555", "3690");
            dtFixedUserCSite.Rows.Add("48540", "3690");
            dtFixedUserCSite.Rows.Add("61631", "3690");
            dtFixedUserCSite.Rows.Add("63515", "3690");
            dtFixedUserCSite.Rows.Add("64031", "3690");
            dtFixedUserCSite.Rows.Add("64102", "3690");
            dtFixedUserCSite.Rows.Add("64103", "3690");
            dtFixedUserCSite.Rows.Add("75067", "3690");
            dtFixedUserCSite.Rows.Add("75451", "3690");
            dtFixedUserCSite.Rows.Add("75594", "3690");
            dtFixedUserCSite.Rows.Add("76665", "3690");
            dtFixedUserCSite.Rows.Add("04639", "3740");
            dtFixedUserCSite.Rows.Add("04822", "3740");
            dtFixedUserCSite.Rows.Add("04891", "3740");
            dtFixedUserCSite.Rows.Add("05022", "3740");
            dtFixedUserCSite.Rows.Add("06093", "3740");
            dtFixedUserCSite.Rows.Add("06257", "3740");
            dtFixedUserCSite.Rows.Add("07004", "3740");
            dtFixedUserCSite.Rows.Add("07364", "3740");
            dtFixedUserCSite.Rows.Add("60122", "3740");
            dtFixedUserCSite.Rows.Add("60685", "3740");
            dtFixedUserCSite.Rows.Add("70035", "3740");
            dtFixedUserCSite.Rows.Add("75601", "3740");
            dtFixedUserCSite.Rows.Add("75705", "3740");
            dtFixedUserCSite.Rows.Add("76151", "3740");
            dtFixedUserCSite.Rows.Add("76292", "3740");
            dtFixedUserCSite.Rows.Add("76512", "3740");
            dtFixedUserCSite.Rows.Add("76513", "3740");
            dtFixedUserCSite.Rows.Add("76563", "3740");
            dtFixedUserCSite.Rows.Add("04213", "4690");
            dtFixedUserCSite.Rows.Add("04288", "4690");
            dtFixedUserCSite.Rows.Add("07511", "4690");
            dtFixedUserCSite.Rows.Add("07542", "4690");
            dtFixedUserCSite.Rows.Add("68853", "4690");
            dtFixedUserCSite.Rows.Add("71993", "4690");
            dtFixedUserCSite.Rows.Add("72115", "4690");
            dtFixedUserCSite.Rows.Add("72116", "4690");
            dtFixedUserCSite.Rows.Add("72321", "4690");
            dtFixedUserCSite.Rows.Add("07200", "3290");
            dtFixedUserCSite.AcceptChanges();
            return dtFixedUserCSite;
        }

        /// <summary>
        /// To Get User C Site Value From C Site List
        /// </summary>
        /// <param name="empExtID">Pass Employee Ext ID</param>
        /// <returns>String</returns>
        public string GetFixedUserCSite(string empExtID)
        {
            string sCSite = string.Empty;

            DataTable dt = this.GetFixedUserCSiteList();
            DataRow[] foundRows;
            foundRows = dt.Select("empExtID='" + empExtID + "'");
            if (foundRows.Length > 0)
            {
                sCSite = BusinessUtility.GetString(foundRows[0][1]);
            }

            return sCSite;
        }

        /// <summary>
        /// To Get Employee District Can See Site Report
        /// </summary>
        /// <param name="districtID">Pass District ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetEmployeeCanSeeSiteDistrictReport(string districtID)
        {
            if (districtID != "")
                districtID = districtID.PadLeft(4, '0');

            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append("  select distinct sysRefCode, sysRefCodeValue, '' as empDetail from sysempreferencecodes where sysRefCode ='TDIST' and sysRefCodeActive = 1 ");
                if (districtID != "")
                {
                    sbInsertQuery.Append(" AND sysRefCodeValue like '%" + districtID + "%'");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, null);

            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get User Site District List
        /// </summary>
        /// <param name="refCode">Pass SysRefCode</param>
        /// <param name="refCodeValue">Pass SysRefCode Value</param>
        /// <param name="actionID">Pass Action ID</param>
        /// <param name="listID">Pass List ID</param>
        /// <returns>String</returns>
        public string GetUserSiteDestrictReporting(string refCode, string refCodeValue, int actionID, int listID)
        {
            if (refCodeValue != "")
                refCodeValue = refCodeValue.PadLeft(4, '0');

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                //sbInsertQuery.Append(" set group_concat_max_len = 90000; ");
                //sbInsertQuery.Append(" select   GROUP_CONCAT(CONCAT(CONCAT_WS(' ', empFirstName, empLastName),  ' (',  empExtID,  ')')  SEPARATOR ', ') empDetail  ");
                //sbInsertQuery.Append(" from (   ");

                //sbInsertQuery.Append(" select * from  ");
                //sbInsertQuery.Append(" ( ");
                //sbInsertQuery.Append(" SELECT   empExtID, empFirstName, empLastName , iqSite.refCodeValue    ");
                //sbInsertQuery.Append(" from empheader   ");
                //sbInsertQuery.Append(" join (  ");
                //sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as refCodeValue, empHeader_empHdrID as iEmpHdrID  ");
                //sbInsertQuery.Append(" from empreference   ");
                //sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = @refCode  ");
                //sbInsertQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empheader.empHdrID  and iqSite.refCodeValue = @refCodeValue ");
                //sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = empheader.empHdrID    ");
                //if (actionID == (int)RoleAction.Reporting)
                //{
                //    sbInsertQuery.Append(" and empFun.reporting >0   ");
                //}

                //sbInsertQuery.Append(" and empheader.empHdrID NOT IN ");
                //sbInsertQuery.Append(" ( ");
                //sbInsertQuery.Append(" select empHdrID    ");
                //sbInsertQuery.Append(" from  empmstlistexcl lstexcl    ");
                //sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID= @listID  ");
                //sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue   ");
                //sbInsertQuery.Append(" ) ");

                //sbInsertQuery.Append(" and (empheader.isDeleted = 0 or empheader.isDeleted is null) and empheader.empActive = 1 and empheader.empHdrID not in(select empHeaderID from emphdrstatus where empExcludeReports =1)  ");

                //sbInsertQuery.Append(" join (  ");
                //sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                //sbInsertQuery.Append(" from empreference   ");
                //sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'   ");
                //sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");

                //sbInsertQuery.Append(" join (  ");
                //sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                //sbInsertQuery.Append(" from empreference   ");
                //sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                //sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                //sbInsertQuery.Append(" and	((iqDiv.empDivID = 'RETL')   OR  (iqDiv.empDivID ='WR')  )  and ( iqJob.empJobID)  ");
                //sbInsertQuery.Append(" in (   ");
                //sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,   ");
                //sbInsertQuery.Append(" 0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,   ");
                //sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,   ");
                //sbInsertQuery.Append(" 1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760,   ");
                //sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,   ");
                //sbInsertQuery.Append(" 1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921,   ");
                //sbInsertQuery.Append(" 1930,1931,1940,1941,1950   ");
                //sbInsertQuery.Append(" )     ");
                //sbInsertQuery.Append(" union  ");
                //sbInsertQuery.Append(" select emphdr.empExtID, emphdr.empFirstName, emphdr.empLastName ,  sysRefCodeValue as siteno   ");
                //sbInsertQuery.Append(" from  empmstlistincl lstIncl  ");
                //sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstIncl.empMstListTblID  and  mstLstTbl.mstListID=@listID  ");
                //sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstIncl.empListInclEmpID and lstIncl.sysRefCode = @refCode and  lstIncl.sysrefCodeValue = @refCodeValue  ");
                //sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = emphdr.empHdrID     ");

                //if (actionID == (int)RoleAction.Reporting)
                //{
                //    sbInsertQuery.Append(" and empFun.reporting >0   ");
                //}

                //sbInsertQuery.Append(" and (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1  and emphdr.empHdrID NOT IN  ");
                //sbInsertQuery.Append(" (  ");
                //sbInsertQuery.Append(" select empHdrID     ");
                //sbInsertQuery.Append(" from  empmstlistexcl lstexcl     ");
                //sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                //sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue    ");
                //sbInsertQuery.Append(" )  ");

                //sbInsertQuery.Append("  and emphdr.empHdrID not in(select empHeaderID from emphdrstatus where empExcludeReports =1 ) ");
                //sbInsertQuery.Append(" ) ");
                //sbInsertQuery.Append(" as ordersearchresult order by  empfirstname , empLastName ");

                //sbInsertQuery.Append(" ) as result  GROUP BY refCodeValue  ");



                sbInsertQuery.Append(" set group_concat_max_len = 90000;  SELECT  ");
                sbInsertQuery.Append(" GROUP_CONCAT(CONCAT(CONCAT_WS(' ', empFirstName, empLastName), ");
                sbInsertQuery.Append(" ' (', ");
                sbInsertQuery.Append(" empExtID, ");
                sbInsertQuery.Append(" ')') ");
                sbInsertQuery.Append(" SEPARATOR ', ') empDetail ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" (SELECT  ");
                sbInsertQuery.Append(" * ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" (SELECT  ");
                sbInsertQuery.Append(" empExtID, empFirstName, empLastName, iqSite.refCodeValue ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empheader ");
                sbInsertQuery.Append(" JOIN (SELECT  ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
                sbInsertQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empreference ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCode = @refCode) AS iqSite ON iqSite.iEmpHdrID = empheader.empHdrID ");
                sbInsertQuery.Append(" AND iqSite.refCodeValue = @refCodeValue ");
                sbInsertQuery.Append(" JOIN employeehasfunctionality empFun ON empFun.empHdrID = empheader.empHdrID ");
                if (actionID == (int)RoleAction.Reporting)
                {
                    //sbInsertQuery.Append(" AND empFun.reporting > 0 ");
                }
                sbInsertQuery.Append(" AND empheader.empHdrID NOT IN (SELECT  ");
                sbInsertQuery.Append(" empHdrID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empmstlistexcl lstexcl ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
                sbInsertQuery.Append(" AND mstLstTbl.mstListID = @listID ");
                sbInsertQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
                sbInsertQuery.Append(" AND lstexcl.sysRefCode = @refCode ");
                sbInsertQuery.Append(" AND lstexcl.sysrefCodeValue = @refCodeValue) ");
                sbInsertQuery.Append(" AND (empheader.isDeleted = 0 ");
                sbInsertQuery.Append(" OR empheader.isDeleted IS NULL) ");
                sbInsertQuery.Append(" AND empheader.empActive = 1 ");
                sbInsertQuery.Append(" AND empheader.empHdrID NOT IN (SELECT  ");
                sbInsertQuery.Append(" empHeaderID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" emphdrstatus ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" empExcludeReports = 1) ");
                sbInsertQuery.Append(" JOIN (SELECT  ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empDivID, ");
                sbInsertQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empreference ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'TDIST') AS iqDiv ON iqDiv.iEmpHdrID = empheader.empHdrID ");
                sbInsertQuery.Append(" JOIN (SELECT  ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS empJobID, ");
                sbInsertQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empreference ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'JOB') AS iqJob ON iqJob.iEmpHdrID = empheader.empHdrID ");

                sbInsertQuery.Append(" AND (iqJob.empJobID) IN (1602)  ");

                sbInsertQuery.Append(" UNION SELECT  ");
                sbInsertQuery.Append(" emphdr.empExtID, ");
                sbInsertQuery.Append(" emphdr.empFirstName, ");
                sbInsertQuery.Append(" emphdr.empLastName, ");
                sbInsertQuery.Append(" sysRefCodeValue AS siteno ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empmstlistincl lstIncl ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstIncl.empMstListTblID ");
                sbInsertQuery.Append(" AND mstLstTbl.mstListID = @listID ");
                sbInsertQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstIncl.empListInclEmpID ");
                sbInsertQuery.Append(" AND lstIncl.sysRefCode = @refCode ");
                sbInsertQuery.Append(" AND lstIncl.sysrefCodeValue = @refCodeValue ");
                sbInsertQuery.Append(" JOIN employeehasfunctionality empFun ON empFun.empHdrID = emphdr.empHdrID ");
                if (actionID == (int)RoleAction.Reporting)
                {
                    //sbInsertQuery.Append(" AND empFun.reporting > 0 ");
                }
                sbInsertQuery.Append(" AND (emphdr.isDeleted = 0 ");
                sbInsertQuery.Append(" OR emphdr.isDeleted IS NULL) ");
                sbInsertQuery.Append(" AND emphdr.empActive = 1 ");
                sbInsertQuery.Append(" AND emphdr.empHdrID NOT IN (SELECT  ");
                sbInsertQuery.Append(" empHdrID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empmstlistexcl lstexcl ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
                sbInsertQuery.Append(" AND mstLstTbl.mstListID = @listID ");
                sbInsertQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
                sbInsertQuery.Append(" AND lstexcl.sysRefCode = @refCode ");
                sbInsertQuery.Append(" AND lstexcl.sysrefCodeValue = @refCodeValue) ");
                sbInsertQuery.Append(" AND emphdr.empHdrID NOT IN (SELECT  ");
                sbInsertQuery.Append(" empHeaderID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" emphdrstatus ");
                sbInsertQuery.Append(" WHERE ");
                sbInsertQuery.Append(" empExcludeReports = 1)) AS ordersearchresult ");
                sbInsertQuery.Append(" ORDER BY empfirstname , empLastName) AS result ");
                sbInsertQuery.Append(" GROUP BY refCodeValue ");

                object rValue = dbHelp.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("refCode", refCode, MyDbType.String),    
                        DbUtility.GetParameter("refCodeValue", refCodeValue, MyDbType.String),    
                        DbUtility.GetParameter("actionID", actionID, MyDbType.Int),
                        DbUtility.GetParameter("listID", listID, MyDbType.Int)
                    });

                return BusinessUtility.GetString(rValue);
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get User List Not Associated with District Manager
        /// </summary>
        /// <param name="roleManageListID">Pass Role Manage List ID</param>
        /// <param name="searchBy">Pass Search By</param>
        /// <param name="searchValue">Pass Search Value</param>
        /// <param name="siteID">Pass Site ID</param>
        /// <param name="listID">Pass List ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetUserListNotInDistrictManager(int roleManageListID, string searchBy, string searchValue, string siteID, int listID)
        {
            if (siteID != "")
                siteID = siteID.PadLeft(4, '0');

            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select Distinct emphdr.empHdrID , emphdr.empFirstName, emphdr.empLastName, emphdr.empExtID, iqSite.refCodeValue as empSiteID "); //  fun_get_emp_ref_val( 'STR', emphdr.empHdrID)
                sbQuery.Append(" from empheader	as emphdr ");
                sbQuery.Append(" JOIN (SELECT  ");
                sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
                sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                sbQuery.Append(" FROM ");
                sbQuery.Append(" empreference ");
                sbQuery.Append(" WHERE ");
                sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'TDIST') AS iqSite ");
                sbQuery.Append(" ON iqSite.iEmpHdrID = emphdr.empHdrID ");
                sbQuery.Append(" Where   (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1 and emphdr.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1 ) ");
                // emphdr.empHdrID Not IN (SELECT empHdrID FROM empotherreportsites where SiteNo =@siteNo and isActive =1 ) and
                //sbQuery.Append("  ");

                if (searchBy == EmpSearchBy.Name)
                {
                    StringBuilder sQuery = new StringBuilder();
                    sbQuery = new StringBuilder();
                    sQuery.Append(" select * from ( select Distinct emphdr.empHdrID , emphdr.empFirstName, emphdr.empLastName, emphdr.empExtID, iqSite.refCodeValue as empSiteID "); //fun_get_emp_ref_val( 'STR', emphdr.empHdrID)
                    sQuery.Append(" from empheader	as emphdr ");
                    sQuery.Append(" JOIN (SELECT  ");
                    sQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
                    sQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                    sQuery.Append(" FROM ");
                    sQuery.Append(" empreference ");
                    sQuery.Append(" WHERE ");
                    sQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'TDIST') AS iqSite ");
                    sQuery.Append(" ON iqSite.iEmpHdrID = emphdr.empHdrID ");
                    sQuery.Append(" Where  (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1 and  emphdr.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1 ) ) as result where 1=1  ");
                    //emphdr.empHdrID Not IN (SELECT empHdrID FROM empotherreportsites where SiteNo =@siteNo and isActive =1 ) and
                    //sQuery.Append("  ");

                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND ( empFirstName = '" + searchValue + "' OR empLastName = '" + searchValue + "' OR  concat_ws(' ', trim(empFirstName), trim(empLastName)) = '" + searchValue + "'  OR  concat_ws(' ', trim(empFirstName), trim(empLastName), trim(empExtID)) = '" + searchValue + "'  OR empExtID = '" + searchValue + "') ");

                    string[] sEmpName = searchValue.Split(' ');
                    string empClause = "";

                    if (sEmpName.Length > 2)
                    {
                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ) ");


                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' and  empExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                    }
                    else if (sEmpName.Length == 2)
                    {

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "'  ) ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' AND  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                    }
                    else if (sEmpName.Length == 1)
                    {
                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ) ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[0]) + "' ");
                    }

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND (  ");

                    sEmpName = searchValue.Split(' ');
                    empClause = "";
                    foreach (string sName in sEmpName)
                    {
                        if (empClause == "")
                        {
                            empClause += " empFirstName = '" + sName + "' OR empLastName = '" + sName + "' OR  empExtID = '" + sName + "' ";
                        }
                        else
                        {
                            empClause += " OR empFirstName = '" + sName + "' OR empLastName = '" + sName + "' OR  empExtID = '" + sName + "' ";
                        }
                    }

                    sbQuery.Append(empClause);
                    sbQuery.Append(" ) ");

                    sbQuery.Append(" union ");

                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND (  ");

                    sEmpName = searchValue.Split(' ');
                    foreach (string sName in sEmpName)
                    {
                        if (empClause == "")
                        {
                            empClause += " empFirstName like '%" + sName + "%' OR empLastName  like '%" + sName + "%' OR  empExtID like '%" + sName + "%' ";
                        }
                        else
                        {
                            empClause += " OR empFirstName like '%" + sName + "%' OR empLastName  like '%" + sName + "%' OR  empExtID like '%" + sName + "%' ";
                        }
                    }

                    sbQuery.Append(empClause);
                    sbQuery.Append(" ) ");
                }
                else if (searchBy == EmpSearchBy.EmpCode)
                {
                    sbQuery.Append(" AND emphdr.empExtID = '" + searchValue + "' ");
                }
                else if (searchBy == EmpSearchBy.Store || searchBy == EmpSearchBy.Region || searchBy == EmpSearchBy.Division || searchBy == EmpSearchBy.JobCode)
                {
                    sbQuery.Append(" AND fun_get_emp_ref_val('" + searchBy + "', emphdr.empHdrID) = '" + searchValue + "' ");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("siteNo", siteID, MyDbType.String)
                });

            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Site  District Employee List
        /// </summary>
        /// <param name="sysRefCode">Pass SysRefCode</param>
        /// <param name="siteID">Pass Site ID</param>
        /// <param name="listID">Pass List ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetSiteDistrictEmployeeList(string sysRefCode, string siteID, int listID)
        {
            if (siteID != "")
                siteID = siteID.PadLeft(4, '0');

            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select * from   ");
                sbInsertQuery.Append(" (  ");
                sbInsertQuery.Append(" SELECT  empheader.empHdrID, empheader.empFirstName, empheader.empLastName, empheader.empExtID, iqSite.refCodeValue   empSiteID, fun_get_emp_ref_val(@refCode, empheader.empHdrID) as empSiteNo  ");
                sbInsertQuery.Append(" from empheader    ");
                sbInsertQuery.Append(" join (   ");
                sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as refCodeValue, empHeader_empHdrID as iEmpHdrID   ");
                sbInsertQuery.Append(" from empreference    ");
                sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = @refCode   ");
                sbInsertQuery.Append(" ) as iqSite on iqSite.iEmpHdrID = empheader.empHdrID  and iqSite.refCodeValue = @refCodeValue  ");
                sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = empheader.empHdrID   and empFun.reporting >0    ");

                sbInsertQuery.Append("  and (empheader.isDeleted = 0 or empheader.isDeleted is null) and empheader.empActive = 1  and empheader.empHdrID NOT IN ");
                sbInsertQuery.Append(" ( ");
                sbInsertQuery.Append(" select empHdrID    ");
                sbInsertQuery.Append(" from  empmstlistexcl lstexcl    ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID= @listID  ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue   ");
                sbInsertQuery.Append(" ) ");

                sbInsertQuery.Append(" and empheader.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1 ) ");

                //if (listID == 1)
                //{
                sbInsertQuery.Append(" join (  ");
                sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                sbInsertQuery.Append(" from empreference   ");
                sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = @refCode   ");
                sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");
                sbInsertQuery.Append(" join (  ");
                sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                sbInsertQuery.Append(" from empreference   ");
                sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                sbInsertQuery.Append(" and	 ( iqJob.empJobID)  ");
                sbInsertQuery.Append(" in (   ");
                sbInsertQuery.Append(" 1602   ");
                sbInsertQuery.Append(" )     ");
                //}
                //else
                //{
                //    sbInsertQuery.Append(" join (  ");
                //    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empDivID, empHeader_empHdrID as iEmpHdrID   ");
                //    sbInsertQuery.Append(" from empreference   ");
                //    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'DIV'   ");
                //    sbInsertQuery.Append(" ) as iqDiv on iqDiv.iEmpHdrID = empheader.empHdrID   ");

                //    sbInsertQuery.Append(" join (  ");
                //    sbInsertQuery.Append(" SELECT sysEmpReferenceCodes_sysRefCodeValue as empJobID, empHeader_empHdrID as iEmpHdrID   ");
                //    sbInsertQuery.Append(" from empreference   ");
                //    sbInsertQuery.Append(" WHERE  sysEmpReferenceCodes_sysRefCode = 'JOB'   ");
                //    sbInsertQuery.Append(" ) as iqJob on iqJob.iEmpHdrID = empheader.empHdrID    ");
                //    /*   sbInsertQuery.Append(" and	((iqDiv.empDivID = 'LOGS')   )  and ( iqJob.empJobID)  ");
                //       sbInsertQuery.Append(" in (     ");
                //       sbInsertQuery.Append(" 0030,0095,0401,0415,0419,0425,0810,0815,0819,0820,0821,0824,  0829,0830,1002,1004,1602,1609,1610,1612,1616,1619,1621,1631,    ");
                //       sbInsertQuery.Append(" 1635,1640,1653,1654,1656,1660,1666,1670,1681,1682,1694,1708,  1709,1710,1711,1713,1716,1724,1725,1726,1730,1740,1749,1760, ");
                //       sbInsertQuery.Append(" 1761,1762,1767,1768,1770,1772,1777,1782,1789,1790,1800,1801,  1804,1807,1813,1820,1823,1826,1847,1867,1910,1911,1920,1921, ");
                //       sbInsertQuery.Append(" 1930,1931,1940,1941,1950     ");
                //       sbInsertQuery.Append(" )   ");
                //     */
                //}



                sbInsertQuery.Append(" union  ");

                sbInsertQuery.Append(" select emphdr.empHdrID, emphdr.empFirstName, emphdr.empLastName, emphdr.empExtID,  sysRefCodeValue as empSiteID , fun_get_emp_ref_val(@refCode, emphdr.empHdrID) as empSiteNo  ");
                sbInsertQuery.Append(" from  empmstlistincl lstIncl  ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstIncl.empMstListTblID  and  mstLstTbl.mstListID=@listID   ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstIncl.empListInclEmpID and lstIncl.sysRefCode = @refCode and  lstIncl.sysrefCodeValue = @refCodeValue  ");
                sbInsertQuery.Append(" JOIN    employeehasfunctionality empFun on empFun.empHdrID = emphdr.empHdrID   and empFun.reporting >0   ");

                sbInsertQuery.Append("  and (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1 and emphdr.empHdrID NOT IN  ");
                sbInsertQuery.Append(" (  ");
                sbInsertQuery.Append(" select empHdrID     ");
                sbInsertQuery.Append(" from  empmstlistexcl lstexcl     ");
                sbInsertQuery.Append(" join sysmasterlisttable mstLstTbl on mstLstTbl.mstListTblID =  lstexcl.empMstListTblID  and  mstLstTbl.mstListID=@listID ");
                sbInsertQuery.Append(" JOin empheader emphdr on emphdr.empHdrID = lstexcl.empListExclEmpID and lstexcl.sysRefCode = @refCode and  lstexcl.sysrefCodeValue = @refCodeValue    ");
                sbInsertQuery.Append(" )  ");

                sbInsertQuery.Append(" and emphdr.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1)  ");
                sbInsertQuery.Append(" )  ");
                sbInsertQuery.Append(" as ordersearchresult order by  empfirstname , empLastName  ");

                return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("refCode", sysRefCode, MyDbType.String),    
                            DbUtility.GetParameter("refCodeValue", siteID , MyDbType.String),
                            DbUtility.GetParameter("listID", listID , MyDbType.Int),
                            });
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Regional Diractor User List
        /// </summary>
        /// <param name="searchBy">Pass Search By</param>
        /// <param name="searchValue">Pass Search Value</param>
        /// <returns>DataTable</returns>
        public DataTable GetUserListRegionalDiractor(string searchBy, string searchValue)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" select Distinct emphdr.empHdrID ,emphdr.EmpFirstName, emphdr.EmpLastName, CONCAT_WS(' ',  emphdr.EmpFirstName, emphdr.EmpLastName) AS EmpName, emphdr.empExtID, '' as empSiteID "); //  fun_get_emp_ref_val( 'STR', emphdr.empHdrID)
                sbQuery.Append(" from empheader	as emphdr ");
                sbQuery.Append(" JOIN (SELECT  ");
                sbQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
                sbQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                sbQuery.Append(" FROM ");
                sbQuery.Append(" empreference ");
                sbQuery.Append(" WHERE ");
                sbQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'JOB') AS iqSite ");
                sbQuery.Append(" ON iqSite.iEmpHdrID = emphdr.empHdrID ");
                sbQuery.Append(" Where iqSite.refCodeValue =1609 and   (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1 and emphdr.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1 ) ");


                if (searchBy == EmpSearchBy.Name)
                {
                    StringBuilder sQuery = new StringBuilder();
                    sbQuery = new StringBuilder();
                    sQuery.Append(" select * from ( select Distinct emphdr.empHdrID , emphdr.EmpFirstName, emphdr.EmpLastName, CONCAT_WS(' ',  emphdr.EmpFirstName, emphdr.EmpLastName) AS EmpName, emphdr.empExtID, '' as empSiteID "); //fun_get_emp_ref_val( 'STR', emphdr.empHdrID)
                    sQuery.Append(" from empheader	as emphdr ");
                    sQuery.Append(" JOIN (SELECT  ");
                    sQuery.Append(" sysEmpReferenceCodes_sysRefCodeValue AS refCodeValue, ");
                    sQuery.Append(" empHeader_empHdrID AS iEmpHdrID ");
                    sQuery.Append(" FROM ");
                    sQuery.Append(" empreference ");
                    sQuery.Append(" WHERE ");
                    sQuery.Append(" sysEmpReferenceCodes_sysRefCode = 'JOB') AS iqSite ");
                    sQuery.Append(" ON iqSite.iEmpHdrID = emphdr.empHdrID ");
                    sQuery.Append(" Where iqSite.refCodeValue =1609 and  (emphdr.isDeleted = 0 or emphdr.isDeleted is null) and emphdr.empActive = 1 and  emphdr.empHdrID Not IN (select empHeaderID from emphdrstatus where empExcludeReports =1 ) ) as result where 1=1  ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND ( empFirstName = '" + searchValue + "' OR empLastName = '" + searchValue + "' OR  concat_ws(' ', trim(empFirstName), trim(empLastName)) = '" + searchValue + "'  OR  concat_ws(' ', trim(empFirstName), trim(empLastName), trim(empExtID)) = '" + searchValue + "'  OR empExtID = '" + searchValue + "') ");

                    string[] sEmpName = searchValue.Split(' ');
                    string empClause = "";

                    if (sEmpName.Length > 2)
                    {
                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ) ");


                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' and  empExtID = '" + BusinessUtility.GetString(sEmpName[2]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' and  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                    }
                    else if (sEmpName.Length == 2)
                    {

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empExtID = '" + BusinessUtility.GetString(sEmpName[1]) + "'  ) ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' AND  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[1]) + "' ");
                    }
                    else if (sEmpName.Length == 1)
                    {
                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and (empExtID = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ) ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "'  ");

                        sbQuery.Append(" union ");
                        sbQuery.Append(sQuery);
                        sbQuery.Append(" and empFirstName = '" + BusinessUtility.GetString(sEmpName[0]) + "' OR  empLastName = '" + BusinessUtility.GetString(sEmpName[0]) + "' ");
                    }

                    sbQuery.Append(" union ");
                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND (  ");

                    sEmpName = searchValue.Split(' ');
                    empClause = "";
                    foreach (string sName in sEmpName)
                    {
                        if (empClause == "")
                        {
                            empClause += " empFirstName = '" + sName + "' OR empLastName = '" + sName + "' OR  empExtID = '" + sName + "' ";
                        }
                        else
                        {
                            empClause += " OR empFirstName = '" + sName + "' OR empLastName = '" + sName + "' OR  empExtID = '" + sName + "' ";
                        }
                    }

                    sbQuery.Append(empClause);
                    sbQuery.Append(" ) ");

                    sbQuery.Append(" union ");

                    sbQuery.Append(sQuery);
                    sbQuery.Append("AND (  ");

                    sEmpName = searchValue.Split(' ');
                    foreach (string sName in sEmpName)
                    {
                        if (empClause == "")
                        {
                            empClause += " empFirstName like '%" + sName + "%' OR empLastName  like '%" + sName + "%' OR  empExtID like '%" + sName + "%' ";
                        }
                        else
                        {
                            empClause += " OR empFirstName like '%" + sName + "%' OR empLastName  like '%" + sName + "%' OR  empExtID like '%" + sName + "%' ";
                        }
                    }

                    sbQuery.Append(empClause);
                    sbQuery.Append(" ) ");
                }
                else if (searchBy == EmpSearchBy.EmpCode)
                {
                    sbQuery.Append(" AND emphdr.empExtID = '" + searchValue + "' ");
                }
                else if (searchBy == EmpSearchBy.Store || searchBy == EmpSearchBy.Region || searchBy == EmpSearchBy.Division || searchBy == EmpSearchBy.JobCode)
                {
                    sbQuery.Append(" AND fun_get_emp_ref_val('" + searchBy + "', emphdr.empHdrID) = '" + searchValue + "' ");
                }
                return dbHelper.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null);
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Regional District List
        /// </summary>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>String</returns>
        public string GetRegionalDistrictList(int empID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select   ");
                sbInsertQuery.Append(" GROUP_CONCAT(CONCAT(DistrictID)   ");
                sbInsertQuery.Append(" SEPARATOR ', ') empDetail    ");
                sbInsertQuery.Append(" from   ");
                sbInsertQuery.Append(" (  ");
                sbInsertQuery.Append(" SELECT   ");
                sbInsertQuery.Append(" empLstIncl.sysRefCodeValue AS DistrictID  ");
                sbInsertQuery.Append(" FROM  ");
                sbInsertQuery.Append(" empmstlistincl empLstIncl  ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = empLstIncl.empMstListTblID  ");
                sbInsertQuery.Append(" AND empLstIncl.empListInclEmpID = @empID  ");
                sbInsertQuery.Append(" AND mstLstTbl.mstListID = 5  ");
                sbInsertQuery.Append(" AND empLstIncl.sysRefCode = 'TDIST'  ");
                sbInsertQuery.Append(" AND empLstIncl.sysRefCodeValue NOT IN (SELECT   ");
                sbInsertQuery.Append(" sysrefcodeValue  ");
                sbInsertQuery.Append(" FROM  ");
                sbInsertQuery.Append(" empmstlistexcl lstexcl  ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID  ");
                sbInsertQuery.Append(" AND mstLstTbl.mstListID = 5  ");
                sbInsertQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID  ");
                sbInsertQuery.Append(" AND lstexcl.empListExclEmpID = @empID  ");
                sbInsertQuery.Append(" AND lstexcl.sysRefCode = 'TDIST')  ");
                sbInsertQuery.Append(" order by empLstIncl.sysRefCodeValue ");
                sbInsertQuery.Append(" ) as result   ");
                object rValue = dbHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empID", empID , MyDbType.Int),
                            });

                return BusinessUtility.GetString(rValue);
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get District Manager List Not Associated with District
        /// </summary>
        /// <param name="searchDistrictID">Pass Search District ID</param>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetDestrictListNotAssociateToManager(string searchDistrictID, int empID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" select distinct sysRefCodeValue,  sysRefCodeValue as DistrictID ");
                sbInsertQuery.Append(" from sysempreferencecodes  ");
                sbInsertQuery.Append(" where sysRefCode ='TDIST' and sysRefCodeActive = 1   ");
                sbInsertQuery.Append(" and sysRefCodeValue NOT IN  ");
                sbInsertQuery.Append(" (SELECT  ");
                sbInsertQuery.Append(" sysrefcodeValue ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empmstlistexcl lstexcl ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
                sbInsertQuery.Append(" AND mstLstTbl.mstListID = 5 ");
                sbInsertQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
                sbInsertQuery.Append(" AND lstexcl.empListExclEmpID = @empID ");
                sbInsertQuery.Append(" AND lstexcl.sysRefCode = 'TDIST') ");
                sbInsertQuery.Append(" AND  ");
                sbInsertQuery.Append(" sysRefCodeValue Not IN ( ");
                sbInsertQuery.Append(" SELECT  ");
                sbInsertQuery.Append(" empLstIncl.sysRefCodeValue AS DistrictID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empmstlistincl empLstIncl ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = empLstIncl.empMstListTblID ");
                sbInsertQuery.Append(" AND empLstIncl.empListInclEmpID = @empID ");
                sbInsertQuery.Append(" AND mstLstTbl.mstListID = 5 ");
                sbInsertQuery.Append(" AND empLstIncl.sysRefCode = 'TDIST' ");
                sbInsertQuery.Append(" AND empLstIncl.sysRefCodeValue NOT IN (SELECT  ");
                sbInsertQuery.Append(" sysrefcodeValue ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empmstlistexcl lstexcl ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
                sbInsertQuery.Append(" AND mstLstTbl.mstListID = 5 ");
                sbInsertQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
                sbInsertQuery.Append(" AND lstexcl.empListExclEmpID = @empID ");
                sbInsertQuery.Append(" AND lstexcl.sysRefCode = 'TDIST') ");
                sbInsertQuery.Append(" ) ");

                if (searchDistrictID != "")
                {
                    sbInsertQuery.Append(" and sysRefCodeValue like '%" + searchDistrictID + "%'  ");
                }

                sbInsertQuery.Append(" order by sysRefCodeValue ");


                return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empID", empID , MyDbType.Int),
                            });
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// To Get Districts Associate Manage List
        /// </summary>
        /// <param name="searchDistrictID">Pass Search DistrictID</param>
        /// <param name="empID">Pass Employee ID</param>
        /// <returns>DataTable</returns>
        public DataTable GetDestrictListAssociateToManager(string searchDistrictID, int empID)
        {
            DbHelper dbHelper = new DbHelper(false);
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                sbInsertQuery.Append(" SELECT  ");
                sbInsertQuery.Append(" empLstIncl.sysRefCodeValue, empLstIncl.sysRefCodeValue AS DistrictID ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empmstlistincl empLstIncl ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = empLstIncl.empMstListTblID ");
                sbInsertQuery.Append(" AND empLstIncl.empListInclEmpID = @empID ");
                sbInsertQuery.Append(" AND mstLstTbl.mstListID = 5 ");
                sbInsertQuery.Append(" AND empLstIncl.sysRefCode = 'TDIST' ");
                sbInsertQuery.Append(" AND empLstIncl.sysRefCodeValue NOT IN (SELECT  ");
                sbInsertQuery.Append(" sysrefcodeValue ");
                sbInsertQuery.Append(" FROM ");
                sbInsertQuery.Append(" empmstlistexcl lstexcl ");
                sbInsertQuery.Append(" JOIN sysmasterlisttable mstLstTbl ON mstLstTbl.mstListTblID = lstexcl.empMstListTblID ");
                sbInsertQuery.Append(" AND mstLstTbl.mstListID = 5 ");
                sbInsertQuery.Append(" JOIN empheader emphdr ON emphdr.empHdrID = lstexcl.empListExclEmpID ");
                sbInsertQuery.Append(" AND lstexcl.empListExclEmpID = @empID ");
                sbInsertQuery.Append(" AND lstexcl.sysRefCode = 'TDIST') ");
                if (searchDistrictID != "")
                {
                    sbInsertQuery.Append(" and sysRefCodeValue like '%" + searchDistrictID + "%'  ");
                }
                sbInsertQuery.Append(" order by sysRefCodeValue ");


                return dbHelper.GetDataTable(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empID", empID , MyDbType.Int),
                            });
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

    }
}
