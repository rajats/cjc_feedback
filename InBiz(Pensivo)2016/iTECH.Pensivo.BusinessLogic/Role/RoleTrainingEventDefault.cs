﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;


namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Role Training Event Default Functionality
    /// </summary>
    public class RoleTrainingEventDefault
    {
        /// <summary>
        /// To Get/Set EmpRoleID
        /// </summary>
        public Int64 EmpRoleID { get; set; }

        /// <summary>
        /// To Get/Set Training Event ID
        /// </summary>
        public Int64 TrainingEventID { get; set; }

        /// <summary>
        /// To Get/Set Training Event Ver
        /// </summary>
        public int TrainingEventVer { get; set; }

        /// <summary>
        /// To Get/Set Training Reporting Keyword
        /// </summary>
        public string ReportingKeywords { get; set; }

        /// <summary>
        /// To Get/Set Minimum Pass Score
        /// </summary>
        public int MinimumPassScore { get; set; }

        /// <summary>
        /// To Get/Set Maximum Pass Score
        /// </summary>
        public int MaximumPassScore { get; set; }

        /// <summary>
        /// To Get/Set Attempt Reset Type
        /// </summary>
        public string AttemptResetType { get; set; }

        /// <summary>
        /// To Get/Set Attempt Grant Waiting Time
        /// </summary>
        public int AttemptGrantWaitingTime { get; set; }

        /// <summary>
        /// To Get/Set Order Seq In Display
        /// </summary>
        public int OrderSeqInDisplay { get; set; }

        /// <summary>
        /// To Get/Set User Display Priority
        /// </summary>
        public int UserDisplayPriority { get; set; }

        /// <summary>
        /// To Get/Set Repeat Required
        /// </summary>
        public bool RepeatRequired { get; set; }

        /// <summary>
        /// To Get/Set Repeat In Days
        /// </summary>
        public int RepeatInDays { get; set; }

        /// <summary>
        /// To Get/Set Repeat Trigger Type
        /// </summary>
        public string RepeatTriggerType { get; set; }

        /// <summary>
        /// To Get/Set Alert Required
        /// </summary>
        public bool AlertRequired { get; set; }

        /// <summary>
        /// To Get/Set Alert Type
        /// </summary>
        public string AlertType { get; set; }

        /// <summary>
        /// To Get/Set Alert In Days
        /// </summary>
        public int AlertInDays { get; set; }

        /// <summary>
        /// To Get/Set Alert Before Launch
        /// </summary>
        public bool AlertBeforeLaunch { get; set; }

        /// <summary>
        /// To Get/Set Alert Not Completed
        /// </summary>
        public bool AlertNotCompleted { get; set; }

        /// <summary>
        /// To Get/Set Alert Manager Not Completed
        /// </summary>
        public bool AlertManagerNotCompleted { get; set; }

        /// <summary>
        /// To Get/Set Alert Repeat In Days
        /// </summary>
        public int AlertRepeatInDays { get; set; }

        /// <summary>
        /// To Get/Set Alert Format
        /// </summary>
        public string AlertFormat { get; set; }

        /// <summary>
        /// To Get/Set Alert Portal Message
        /// </summary>
        public string AlertPortalMsg { get; set; }

        /// <summary>
        /// To Get/Set Alert Email Message
        /// </summary>
        public string AlertEmailMsg { get; set; }

        /// <summary>
        /// To Get/Set Alert Escalation Email Message
        /// </summary>
        public string AlertEscalationEmailMsg { get; set; }

        /// <summary>
        /// To Get/Set Alert Email Subject
        /// </summary>
        public string AlertEmailSubject { get; set; }

        /// <summary>
        /// To Get/Set Alert Escalation Email Subject
        /// </summary>
        public string AlertEscalationEmailSubject { get; set; }

        /// <summary>
        /// To Get/Set Employee ROle Training Evt Vals Column
        /// </summary>
        public string EmpRoleTrainingEvtValsCol { get; set; }

        /// <summary>
        /// To Get/Set Repeat Fixed Date Time
        /// </summary>
        public DateTime RepeatFixedDateTime { get; set; }

        /// <summary>
        /// To Get/Set Course Start DateTime
        /// </summary>
        public DateTime CourseStartDateTime { get; set; }

        /// <summary>
        /// To Get/Set Fixed Day
        /// </summary>
        public int FixedDay { get; set; }

        /// <summary>
        /// To Get/Set Fixed Month
        /// </summary>
        public int FixedMonth { get; set; }

        /// <summary>
        /// To Save/Update Training Event Default 
        /// </summary>
        /// <returns>True/False</returns>
        public Boolean SaveUpdateRoleTrainingEventDefault()
        {
            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsertQuery = new StringBuilder();
                StringBuilder sbQuery = new StringBuilder();
                sbInsertQuery = new StringBuilder();

                sbInsertQuery.Append(" SELECT trainingEventID FROM emproletrainingevtvals where empRoleID = @empRoleID and trainingEventID = @eventID ");
                object rValue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("eventID", this.TrainingEventID, MyDbType.Int),
                DbUtility.GetParameter("empRoleID", this.EmpRoleID, MyDbType.Int),
                });

                if (BusinessUtility.GetInt(rValue) <= 0)
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" insert into emproletrainingevtvals ");
                    sbInsertQuery.Append(" ( ");
                    sbInsertQuery.Append(" empRoleID, trainingEventID,  reportingKeywords, minimumPassScore, maximumPassScore, attemptResetType, attemptGrantWaitingTime, orderSeqInDisplay, userDisplayPriority,  "); //trainingEventVer,
                    sbInsertQuery.Append(" repeatRequired, repeatInDays, repeatTriggerType, alertRequired, alertType, alertBeforeLaunch, alertNotCompleted, alertManagerNotCompleted, alertRepeatInDays, alertInDays, alertFormat, alertPortalMsg, alertEmailMsg, alertEscalationEmailMsg,  ");
                    sbInsertQuery.Append(" alertEmailSubject, alertEscalationEmailSubject, emproletrainingevtvalscol, repeatFixedDate, fixedDay, fixedMonth, startdatetime    ");
                    sbInsertQuery.Append(" ) ");
                    sbInsertQuery.Append(" values ");
                    sbInsertQuery.Append(" ( ");
                    sbInsertQuery.Append(" @empRoleID, @trainingEventID, @reportingKeywords, @minimumPassScore, @maximumPassScore, @attemptResetType, @attemptGrantWaitingTime, @orderSeqInDisplay, @userDisplayPriority,  "); //@trainingEventVer, 
                    sbInsertQuery.Append(" @repeatRequired, @repeatInDays, @repeatTriggerType, @alertRequired, @alertType, @alertBeforeLaunch, @alertNotCompleted, @alertManagerNotCompleted, @alertRepeatInDays,  @alertInDays, @alertFormat, @alertPortalMsg, @alertEmailMsg, @alertEscalationEmailMsg,  ");
                    sbInsertQuery.Append(" @alertEmailSubject, @alertEscalationEmailSubject, @emproletrainingevtvalscol, @repeatFixedDate, @fixedDay, @fixedMonth, @startdatetime   ");
                    sbInsertQuery.Append(" ) ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("@empRoleID", this.EmpRoleID , MyDbType.Int),
                    DbUtility.GetParameter("@trainingEventID", this.TrainingEventID , MyDbType.Int),
                    //DbUtility.GetParameter("@trainingEventVer", this.TrainingEventVer , MyDbType.Int),
                    DbUtility.GetParameter("@reportingKeywords", this.ReportingKeywords , MyDbType.String),
                    DbUtility.GetParameter("@minimumPassScore", this.MinimumPassScore , MyDbType.Int),
                    DbUtility.GetParameter("@maximumPassScore", this.MaximumPassScore , MyDbType.Int),
                    DbUtility.GetParameter("@attemptResetType", this.AttemptResetType , MyDbType.String),
                    DbUtility.GetParameter("@attemptGrantWaitingTime", this.AttemptGrantWaitingTime , MyDbType.Int),
                    DbUtility.GetParameter("@orderSeqInDisplay", this.OrderSeqInDisplay , MyDbType.Int),
                    DbUtility.GetParameter("@userDisplayPriority", this.UserDisplayPriority , MyDbType.Int),
                    DbUtility.GetParameter("@repeatRequired", this.RepeatRequired , MyDbType.Boolean),
                    DbUtility.GetParameter("@repeatInDays", this.RepeatInDays , MyDbType.Int),
                    DbUtility.GetParameter("@repeatTriggerType", this.RepeatTriggerType , MyDbType.String),
                    DbUtility.GetParameter("@alertRequired", this.AlertRequired , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertType", this.AlertType , MyDbType.String),
                    DbUtility.GetParameter("@alertBeforeLaunch", this.AlertBeforeLaunch , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertNotCompleted", this.AlertNotCompleted , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertManagerNotCompleted", this.AlertManagerNotCompleted , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertRepeatInDays", this.AlertRepeatInDays , MyDbType.Int),
                    DbUtility.GetParameter("@alertInDays", this.AlertInDays , MyDbType.Int),
                    DbUtility.GetParameter("@alertFormat", this.AlertFormat , MyDbType.String),
                    DbUtility.GetParameter("@alertPortalMsg", this.AlertPortalMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEmailMsg", this.AlertEmailMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEscalationEmailMsg", this.AlertEscalationEmailMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEmailSubject", this.AlertEmailSubject , MyDbType.String),
                    DbUtility.GetParameter("@alertEscalationEmailSubject", this.AlertEscalationEmailSubject , MyDbType.String),
                    DbUtility.GetParameter("@emproletrainingevtvalscol", this.EmpRoleTrainingEvtValsCol , MyDbType.String),
                    DbUtility.GetParameter("@repeatFixedDate", this.RepeatFixedDateTime , MyDbType.DateTime),
                    DbUtility.GetParameter("@fixedDay", this.FixedDay , MyDbType.Int),
                    DbUtility.GetParameter("@fixedMonth", this.FixedMonth , MyDbType.Int),
                    DbUtility.GetParameter("@startdatetime", this.CourseStartDateTime , MyDbType.DateTime),
                    });
                }
                else
                {
                    sbInsertQuery = new StringBuilder();
                    sbInsertQuery.Append(" update emproletrainingevtvals set  ");
                    sbInsertQuery.Append("  reportingKeywords = @reportingKeywords, minimumPassScore = @minimumPassScore, maximumPassScore = @maximumPassScore,   "); //trainingEventVer = @trainingEventVer,
                    sbInsertQuery.Append(" attemptResetType = @attemptResetType, attemptGrantWaitingTime = @attemptGrantWaitingTime, orderSeqInDisplay = @orderSeqInDisplay,   ");
                    sbInsertQuery.Append(" userDisplayPriority = @userDisplayPriority, repeatRequired = @repeatRequired,  repeatInDays = @repeatInDays, repeatTriggerType = @repeatTriggerType,  ");
                    sbInsertQuery.Append(" alertRequired = @alertRequired, alertType = @alertType, alertBeforeLaunch = @alertBeforeLaunch, alertNotCompleted = @alertNotCompleted, alertManagerNotCompleted = @alertManagerNotCompleted, alertRepeatInDays = @alertRepeatInDays,   ");
                    sbInsertQuery.Append(" alertInDays = @alertInDays, alertFormat = @alertFormat, alertPortalMsg = @alertPortalMsg, alertEmailMsg = @alertEmailMsg, alertEscalationEmailMsg = @alertEscalationEmailMsg, alertEmailSubject =@alertEmailSubject, alertEscalationEmailSubject = @alertEscalationEmailSubject,   ");
                    sbInsertQuery.Append(" emproletrainingevtvalscol = @emproletrainingevtvalscol, repeatFixedDate = @repeatFixedDate,    ");
                    sbInsertQuery.Append(" fixedDay = @fixedDay, fixedMonth = @fixedMonth, startdatetime = @startdatetime ");
                    sbInsertQuery.Append(" where trainingEventID = @trainingEventID and empRoleID = @empRoleID ");

                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("@empRoleID", this.EmpRoleID , MyDbType.Int),
                    DbUtility.GetParameter("@trainingEventID", this.TrainingEventID , MyDbType.Int),
                    //DbUtility.GetParameter("@trainingEventVer", this.TrainingEventVer , MyDbType.Int),
                    DbUtility.GetParameter("@reportingKeywords", this.ReportingKeywords , MyDbType.String),
                    DbUtility.GetParameter("@minimumPassScore", this.MinimumPassScore , MyDbType.Int),
                    DbUtility.GetParameter("@maximumPassScore", this.MaximumPassScore , MyDbType.Int),
                    DbUtility.GetParameter("@attemptResetType", this.AttemptResetType , MyDbType.String),
                    DbUtility.GetParameter("@attemptGrantWaitingTime", this.AttemptGrantWaitingTime , MyDbType.Int),
                    DbUtility.GetParameter("@orderSeqInDisplay", this.OrderSeqInDisplay , MyDbType.Int),
                    DbUtility.GetParameter("@userDisplayPriority", this.UserDisplayPriority , MyDbType.Int),
                    DbUtility.GetParameter("@repeatRequired", this.RepeatRequired , MyDbType.Boolean),
                    DbUtility.GetParameter("@repeatInDays", this.RepeatInDays , MyDbType.Int),
                    DbUtility.GetParameter("@repeatTriggerType", this.RepeatTriggerType , MyDbType.String),
                    DbUtility.GetParameter("@alertRequired", this.AlertRequired , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertType", this.AlertType , MyDbType.String),
                    DbUtility.GetParameter("@alertBeforeLaunch", this.AlertBeforeLaunch , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertNotCompleted", this.AlertNotCompleted , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertManagerNotCompleted", this.AlertManagerNotCompleted , MyDbType.Boolean),
                    DbUtility.GetParameter("@alertRepeatInDays", this.AlertRepeatInDays , MyDbType.Int),
                    DbUtility.GetParameter("@alertInDays", this.AlertInDays , MyDbType.Int),
                    DbUtility.GetParameter("@alertFormat", this.AlertFormat , MyDbType.String),
                    DbUtility.GetParameter("@alertPortalMsg", this.AlertPortalMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEmailMsg", this.AlertEmailMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEscalationEmailMsg", this.AlertEscalationEmailMsg , MyDbType.String),
                    DbUtility.GetParameter("@alertEmailSubject", this.AlertEmailSubject , MyDbType.String),
                    DbUtility.GetParameter("@alertEscalationEmailSubject", this.AlertEscalationEmailSubject , MyDbType.String),
                    DbUtility.GetParameter("@emproletrainingevtvalscol", this.EmpRoleTrainingEvtValsCol , MyDbType.String),
                    DbUtility.GetParameter("@repeatFixedDate", this.RepeatFixedDateTime , MyDbType.DateTime),
                    DbUtility.GetParameter("@fixedDay", this.FixedDay , MyDbType.Int),
                    DbUtility.GetParameter("@fixedMonth", this.FixedMonth , MyDbType.Int),
                    DbUtility.GetParameter("@startdatetime", this.CourseStartDateTime , MyDbType.DateTime),
                    });
                }
                dbTransactionHelper.CommitTransaction();
                bReturn = true;
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }

        /// <summary>
        /// To Get Training Event Default
        /// </summary>
        /// <param name="empRoleID">Pass Employee Role ID</param>
        /// <param name="eventID">Pass Event ID</param>
        /// <param name="lang">Pass Language</param>
        public void GetRoleTrainingEventDefault(int empRoleID, int eventID, string lang)
        {
            StringBuilder sbQuery = new StringBuilder();

            sbQuery.Append(" select ");
            sbQuery.Append(" empRoleID, trainingEventID, trainingEventVer, reportingKeywords, minimumPassScore, maximumPassScore, attemptResetType, attemptGrantWaitingTime, orderSeqInDisplay, ");
            sbQuery.Append(" userDisplayPriority, repeatRequired, repeatInDays, repeatTriggerType, alertRequired, alertType, alertBeforeLaunch, alertNotCompleted, alertManagerNotCompleted, alertRepeatInDays, alertInDays, alertFormat, alertPortalMsg, alertEmailMsg, ");
            sbQuery.Append(" alertEscalationEmailMsg, alertEmailSubject, alertEscalationEmailSubject, emproletrainingevtvalscol, repeatFixedDate, ");
            sbQuery.Append(" fixedDay, fixedMonth, startdatetime ");
            sbQuery.Append(" from ");
            sbQuery.Append(" emproletrainingevtvals");
            sbQuery.Append(" where trainingEventID = @trainingEventID and empRoleID = @empRoleID ");

            MySqlDataReader objDr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                objDr = dbHelp.GetDataReader(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("trainingEventID", eventID, MyDbType.Int),
                DbUtility.GetParameter("empRoleID", empRoleID, MyDbType.Int),
                });
                while (objDr.Read())
                {
                    this.EmpRoleID = BusinessUtility.GetInt(objDr["empRoleID"]);
                    this.TrainingEventID = BusinessUtility.GetInt(objDr["trainingEventID"]);
                    this.TrainingEventVer = BusinessUtility.GetInt(objDr["trainingEventVer"]);
                    this.ReportingKeywords = BusinessUtility.GetString(objDr["reportingKeywords"]);
                    this.MinimumPassScore = BusinessUtility.GetInt(objDr["minimumPassScore"]);
                    this.MaximumPassScore = BusinessUtility.GetInt(objDr["maximumPassScore"]);
                    this.AttemptResetType = BusinessUtility.GetString(objDr["attemptResetType"]);
                    this.AttemptGrantWaitingTime = BusinessUtility.GetInt(objDr["attemptGrantWaitingTime"]);
                    this.OrderSeqInDisplay = BusinessUtility.GetInt(objDr["orderSeqInDisplay"]);
                    this.UserDisplayPriority = BusinessUtility.GetInt(objDr["userDisplayPriority"]);
                    this.RepeatRequired = BusinessUtility.GetBool(objDr["repeatRequired"]);
                    this.RepeatInDays = BusinessUtility.GetInt(objDr["repeatInDays"]);
                    this.RepeatTriggerType = BusinessUtility.GetString(objDr["repeatTriggerType"]);
                    this.AlertRequired = BusinessUtility.GetBool(objDr["alertRequired"]);
                    this.AlertType = BusinessUtility.GetString(objDr["alertType"]);
                    this.AlertBeforeLaunch = BusinessUtility.GetBool(objDr["alertBeforeLaunch"]);
                    this.AlertNotCompleted = BusinessUtility.GetBool(objDr["alertNotCompleted"]);
                    this.AlertManagerNotCompleted = BusinessUtility.GetBool(objDr["alertManagerNotCompleted"]);
                    this.AlertRepeatInDays = BusinessUtility.GetInt(objDr["alertRepeatInDays"]);
                    this.AlertInDays = BusinessUtility.GetInt(objDr["alertInDays"]);
                    this.AlertFormat = BusinessUtility.GetString(objDr["alertFormat"]);
                    this.AlertPortalMsg = BusinessUtility.GetString(objDr["alertPortalMsg"]);
                    this.AlertEmailMsg = BusinessUtility.GetString(objDr["alertEmailMsg"]);
                    this.AlertEscalationEmailMsg = BusinessUtility.GetString(objDr["alertEscalationEmailMsg"]);
                    this.AlertEmailSubject = BusinessUtility.GetString(objDr["alertEmailSubject"]);
                    this.AlertEscalationEmailSubject = BusinessUtility.GetString(objDr["alertEscalationEmailSubject"]);
                    this.EmpRoleTrainingEvtValsCol = BusinessUtility.GetString(objDr["emproletrainingevtvalscol"]);
                    this.RepeatFixedDateTime = BusinessUtility.GetDateTime(objDr["repeatFixedDate"]);
                    this.FixedDay = BusinessUtility.GetInt(objDr["fixedDay"]);
                    this.FixedMonth = BusinessUtility.GetInt(objDr["fixedMonth"]);
                    this.CourseStartDateTime = BusinessUtility.GetDateTime(objDr["startdatetime"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }


        public void GetRoleCourseDetail(int roleID, int courseID)
        {
            StringBuilder sbQuery = new StringBuilder();
            //sbQuery.Append(" SELECT empHdrID, EmpExtID, EmpEmail, empLogInID, CONCAT_WS(' ',  EmpFirstName, EmpLastName) AS EmpName, EmpFirstName, EmpLastName, EmpPassword, ");
            //sbQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.JobCode + "', @EmpID) AS JobCode, fun_get_emp_ref_val('" + EmpRefCode.Department + "', @EmpID) AS Department,  ");
            //sbQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.District + "', @EmpID) AS District, fun_get_emp_ref_val('" + EmpRefCode.Region + "', @EmpID) AS Region,  ");
            //sbQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.AccountType + "', @EmpID) AS AccountType,   fun_get_emp_ref_val('" + EmpRefCode.Store + "', @EmpID) AS Store,  ");
            //sbQuery.Append(" fun_get_emp_ref_val('" + EmpRefCode.Division + "', @EmpID) AS Division,   EmpType, fun_get_emp_ref_val('" + EmpRefCode.Type + "', @EmpID) emp_type , fun_get_emp_ref_val('" + EmpRefCode.TypeParsed + "', @EmpID) emp_type_parse, PensivoUUID ");
            //sbQuery.Append(" FROM empheader WHERE  empHdrID = @EmpID ");


            sbQuery.Append(" select  ");
            sbQuery.Append(" distinct @courseID as courseID,   @roleID as roleID,  ");
            sbQuery.Append(" ifnull( roletraning.repeatRequired, teventdefault.repeatRequired) as repeatRequired ,  ");
            sbQuery.Append(" ifnull(roletraning.minimumPassScore, teventdefault.minimumPassScore) as minimumPassScore,  ");
            sbQuery.Append(" ifnull(roletraning.orderSeqInDisplay, teventdefault.orderSeqInDisplay) as orderSeqInDisplay,  ");
            sbQuery.Append(" ifnull(roletraning.repeatInDays, teventdefault.repeatInDays) as repeatInDays,  ");
            sbQuery.Append(" ifnull(roletraning.repeatTriggerType, teventdefault.repeatTriggerType) as repeatTriggerType,  ");
            sbQuery.Append(" ifnull(roletraning.startdatetime, teventdefault.startdatetime) as startdatetime, rolemaster.emprolecreatedon, ");
            sbQuery.Append(" case ifnull(roletraning.repeatTriggerType, teventdefault.repeatTriggerType) when 'F' then 365 else ifnull(roletraning.repeatInDays, teventdefault.repeatInDays)  end  as RepeatRate, ");
            sbQuery.Append(" ifnull(roletraning.fixedDay, teventdefault.fixedDay) as fixedDay, ");
            sbQuery.Append(" ifnull(roletraning.fixedMonth, teventdefault.fixedMonth) as fixedMonth ");
            sbQuery.Append(" from  sysrolemaster as rolemaster  ");
            sbQuery.Append(" left join emproletrainingevtvals as roletraning on roletraning.empRoleID = rolemaster.empRoleID and roletraning.trainingEventID = @courseID   ");
            sbQuery.Append(" left join trainingeventdefaults as teventdefault on teventdefault.trainingEventID = @courseID ");
            sbQuery.Append(" where  rolemaster.empRoleID = @roleID ");

            List<MySqlParameter> pList = new List<MySqlParameter>();
            pList.Add(DbUtility.GetParameter("courseID", courseID, MyDbType.String));
            pList.Add(DbUtility.GetParameter("roleID", roleID, MyDbType.Int));

            MySqlDataReader objDr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                objDr = dbHelp.GetDataReader(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, pList.ToArray());
                while (objDr.Read())
                {
                    this.RepeatRequired = BusinessUtility.GetBool(objDr["repeatRequired"]);
                    this.RepeatTriggerType = BusinessUtility.GetString(objDr["repeatTriggerType"]);
                    this.RepeatInDays = BusinessUtility.GetInt(objDr["RepeatRate"]);
                    this.OrderSeqInDisplay = BusinessUtility.GetInt(objDr["orderSeqInDisplay"]);
                    this.FixedMonth = BusinessUtility.GetInt(objDr["fixedMonth"]);
                    this.FixedDay = BusinessUtility.GetInt(objDr["fixedDay"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
