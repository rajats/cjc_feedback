﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define Functionality Regarding Employee/users 
    /// </summary>
    public class Feedback
    {
        /// <summary>
        /// To Get/Set Employee ID
        /// </summary>
        public string EmpIDList { get; set; }

        /// <summary>
        /// To Get/Set Employee Password 
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// To Get/Set Employee First Name
        /// </summary>
        public string EventDate { get; set; }

        /// <summary>
        /// To Get/Set Employee Last Name
        /// </summary>
        public string TrainingEventID { get; set; }

        /// <summary>
        /// To Get/Set Questionnaire Name
        /// </summary>
        public string TrainingEventName { get; set; }


        /// <summary>
        /// To insert feedback details using sp_InsertFeedbackDetails Stored Procedure and return roleID
        /// </summary>
        /// <param name="prmEventName">Pass Event Name</param>
        /// <param name="prmEventDate">Pass Event Date</param>
        /// <param name="prmCourseHdrID">Pass Feedback Training ID</param>
        /// <param name="prmEmpList">Pass Employee List</param>
        /// <param name="prmEmpCreator">Pass Creator Employee ID</param>
        /// <returns>int</returns>
        public int InsertFeedbackDetails(string prmEventName, string prmEventDate, int prmCourseHdrID, string prmEmpList, int prmEmpCreator)
        {
            int rValue = 0;
            DataTable ResultDataTable = new DataTable();
            StringBuilder sbQuery = new StringBuilder();

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                sbQuery = new StringBuilder();
                sbQuery.Append(" call sp_InsertFeedbackDetails('" + prmEventName + "','" + prmEventDate + "'," + prmCourseHdrID + ",'" + prmEmpList + "'," + prmEmpCreator + ");");
                object objrValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, null);

                rValue= BusinessUtility.GetInt(objrValue);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error While executing procedure sp_InsertFeedbackDetails: Procedure Call : call sp_InsertFeedbackDetails('" + prmEventName + "','" + prmEventDate + "'," + prmCourseHdrID + ",'" + prmEmpList + "'," + prmEmpCreator + "); Error Details Below :");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return rValue;
        }

    }
}
