﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;

using iTECH.Library.Utilities;
using System.Data;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using System.Xml;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define User Additional Information Functionality 
    /// </summary>
    public class EmphdrInfocl
    {

        /// <summary>
        /// To Define Query
        /// </summary>
        string m_sSQL = string.Empty;

        /// <summary>
        /// To  Define Employee Header ID
        /// </summary>
        private string m_sEmpHeaderId = "0";

        /// <summary>
        /// To Get Employee Header ID
        /// </summary>
        public string EmpHeaderId
        {
            get { return m_sEmpHeaderId; }
        }

        /// <summary>
        /// To Define Employee Phone
        /// </summary>
        private string m_sEmpPhone = string.Empty;

        /// <summary>
        /// To Get Employee Phone
        /// </summary>
        public string EmpPhone
        {
            get { return m_sEmpPhone; }
        }

        /// <summary>
        /// To Define Employee Address1
        /// </summary>
        private string m_sEmpAddress1 = string.Empty;

        /// <summary>
        /// To Get Employee Address 1
        /// </summary>
        public string EmpAddress1
        {
            get { return m_sEmpAddress1; }
        }

        /// <summary>
        /// To Define Employee Address 2
        /// </summary>
        private string m_sEmpAddress2 = string.Empty;

        /// <summary>
        /// To Get Employee Address 2
        /// </summary>
        public string EmpAddress2
        {
            get { return m_sEmpAddress2; }
        }

        /// <summary>
        /// To Define Employee Address City
        /// </summary>
        private string m_sEmpAddressCity = string.Empty;

        /// <summary>
        /// To Get Employee Address City
        /// </summary>
        public string EmpAddressCity
        {
            get { return m_sEmpAddressCity; }
        }

        /// <summary>
        /// To Define Employee State
        /// </summary>
        private string m_sEmpState = string.Empty;

        /// <summary>
        /// To Get Employee State
        /// </summary>
        public string EmpState
        {
            get { return m_sEmpState; }
        }

        /// <summary>
        /// To Define Employee City
        /// </summary>
        private string m_sEmpCity = string.Empty;

        /// <summary>
        /// To Get Employee City
        /// </summary>
        public string EmpCity
        {
            get { return m_sEmpCity; }
        }

        /// <summary>
        /// To Define Employee Zip Code
        /// </summary>
        private string m_sEmpZipCode = string.Empty;

        /// <summary>
        /// To Get Employee Zip Code
        /// </summary>
        public string EmpZipCode
        {
            get { return m_sEmpZipCode; }
        }

        /// <summary>
        /// To Define Employee Date of Birth
        /// </summary>
        private DateTime m_dtEmpDOB;

        /// <summary>
        /// To Get Employee Date of Birth
        /// </summary>
        public DateTime EmpDOB
        {
            get { return m_dtEmpDOB; }
        }

        /// <summary>
        /// To Get User Additional Information From emphdrinfo and emphdr Table
        /// </summary>
        /// <param name="sUserID">Pass Employee/User ID</param>
        public EmphdrInfocl(string sUserID)
        {
            m_sEmpHeaderId = sUserID;
            DbHelper oDbHelper = new DbHelper(true);
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" SELECT emphdr.emphdrId, emphdr.empextid, IF(empinfo.empPhone IS NULL, '', empinfo.empPhone) AS empinfo.empPhone,");
                sbQuery.Append("IF(empinfo.empAddress1 IS NULL, '', empinfo.empAddress1) AS empinfo.empAddress1, IF(empinfo.empAddress2 IS NULL, '', empinfo.empAddress2) AS empAddress2,");
                sbQuery.Append("IF(empinfo.empCity IS NULL, '', empinfo.empCity) AS empinfo.empCity, IF(empinfo.empState IS NULL, '', empinfo.empState) AS empinfo.empState,");
                sbQuery.Append("IF(empinfo.empZipCode IS NULL, '', empinfo.empZipCode) AS empinfo.empZipCode, IF(empinfo.empDOB IS NULL, '', empinfo.empDOB) AS empinfo.empDOB ");
                sbQuery.Append(" FROM empheader emphdr ");
                sbQuery.Append(" INNER JOIN emphdrinfo empinfo ON emphdr.emphdrId = empinfo.emphdrId  and emphdr.empExtID=@empExtID ");
                MySqlParameter[] oMySqlParameter = { DbUtility.GetParameter("empExtID", m_sEmpHeaderId, MyDbType.String) };

                MySqlDataReader oMySqlDataReader = oDbHelper.GetDataReader(Convert.ToString(sbQuery), CommandType.Text, oMySqlParameter);
                if (oMySqlDataReader.Read())
                {
                    m_sEmpPhone = BusinessUtility.GetString(oMySqlDataReader.GetString("empPhone"));
                    m_sEmpAddress1 = BusinessUtility.GetString(oMySqlDataReader.GetString("empAddress1"));
                    m_sEmpAddress2 = BusinessUtility.GetString(oMySqlDataReader.GetString("empAddress2"));
                    m_sEmpCity = BusinessUtility.GetString(oMySqlDataReader.GetString("empCity"));
                    m_sEmpState = BusinessUtility.GetString(oMySqlDataReader.GetString("empState"));
                    m_sEmpZipCode = BusinessUtility.GetString(oMySqlDataReader.GetString("empZipCode"));
                    m_dtEmpDOB = BusinessUtility.GetDateTime(oMySqlDataReader.GetString("empDOB"));
                }
                oMySqlDataReader.Close();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

    }
}