﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.Pensivo.BusinessLogic
{

    /// <summary>
    /// To Define AI Reporting Functionality  
    /// </summary>
    public class AIReportCls
    {
        /// <summary>
        /// To Get Question Details As Page or Section From aiquestionheader Table
        /// </summary>
        /// <param name="pageQuestion">Pass AI Section ID</param>
        /// <returns>True/False</returns>
        public DataTable getQuestionDetails(string pageQuestion)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append("select * from aiquestionheader where aisection=@pageQuestion and aiisactive = 1  order by aisequence asc");
            MySqlParameter[] p = { new MySqlParameter("@pageQuestion", pageQuestion) };
            DbHelper dbHelp = new DbHelper(true);
            DataTable dt = new DataTable();
            try
            {
                dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, p);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dt;
        }

        /// <summary>
        /// To Get The Max AI Form ID From aiformheader Table
        /// </summary>
        /// <returns>int</returns>
        public int getMaxFormID()
        {
            int maxID = 0;
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append("select max(aiFormID) from aiformheader;");
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object o = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, null);
                maxID = BusinessUtility.GetInt(o);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return maxID;
        }

        /// <summary>
        /// To Save AI Form Details 
        /// </summary>
        /// <param name="objAIQuestion">Pass AI Question List</param>
        /// <param name="objEmpDetails">Pass AI Employee Detail</param>
        /// <param name="reportStatus">Pass Reporting Status</param>
        /// <returns>True/False</returns>
        public Boolean insertAIDetails(List<AIQuestionAnswer> objAIQuestion, List<Employee> objEmpDetails, string reportStatus)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            StringBuilder sbInsertQuery = new StringBuilder();
            List<AIQuestionAnswer> objAnswer = new List<AIQuestionAnswer>();
            string aiLocationType = string.Empty;
            int lastinsertedFormID = 0;
            int employeeID = 0;
            int aiISLicense = 0;
            bool bREturn = false;
            string IncidetORAccident = string.Empty;
            StringBuilder IncidetORAccidentDesc = new StringBuilder();
            DateTime IncidentORAcccidentDate = DateTime.Now;

            try
            {
                if (objAIQuestion.Count > 0 && objAIQuestion.Count > 0)
                {
                    var IsRetailLogistic = objAIQuestion.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic);
                    if (IsRetailLogistic != null)
                    {
                        aiLocationType = Convert.ToString(IsRetailLogistic.AIQuestionAnswerText);
                    }
                    var IsLicensee = objAIQuestion.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                    if (IsLicensee != null)
                    {
                        aiISLicense = Convert.ToInt32(1);
                    }
                    else
                    {
                        aiISLicense = Convert.ToInt32(0);
                    }

                    var incidentoraccident = objAIQuestion.FirstOrDefault(x => x.AIPageName == PageName.Incident);
                    if (incidentoraccident != null)
                    {
                        if (Convert.ToString(incidentoraccident.AIQuestionAnswerText) == "1")
                        {
                            IncidetORAccident = "Incident";
                            var incidentResult = objAIQuestion.Where(x => x.AIPageName == PageName.IncidentDetail).ToList();
                            if (incidentResult != null && incidentResult.Count > 0)
                            {
                                foreach (var result in incidentResult)
                                {
                                    if (Convert.ToInt32(result.AIQuestionID) == 32)
                                    {
                                        IncidentORAcccidentDate = Convert.ToDateTime(result.AIQuestionAnswerText);
                                    }
                                    if (Convert.ToInt32(result.AIQuestionText) == 49)
                                    {
                                        IncidetORAccidentDesc.Append(Convert.ToString(result.AIQuestionAnswerText));
                                    }
                                }
                            }
                        }

                        else
                        {
                            var AccidentDetails = objAIQuestion.ToList();
                            var incidentResult1 = objAIQuestion.Where(x => x.AIPageName == PageName.IncidentDetail).ToList();
                            if (incidentResult1 != null && incidentResult1.Count > 0)
                            {
                                foreach (var result in incidentResult1)
                                {
                                    if (Convert.ToInt32(result.AIQuestionID) == 32)
                                    {
                                        IncidentORAcccidentDate = Convert.ToDateTime(result.AIQuestionAnswerText);
                                    }
                                }
                            }
                            StringBuilder QuestionYesIDss = new StringBuilder();
                            var QuestionIDYesResult = objAIQuestion.Where(x => x.AIQuestionAnswerText == "1").ToList();
                            if (QuestionIDYesResult != null)
                            {
                                foreach (var getYesIDs in QuestionIDYesResult)
                                {
                                    QuestionYesIDss.Append(BusinessUtility.GetString(getYesIDs.AIQuestionID) + ",");
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(QuestionYesIDss)))
                                {
                                    QuestionYesIDss.Length--;
                                }
                            }

                            StringBuilder sqlInsertCommand = new StringBuilder();
                            DataTable dt1 = new DataTable();
                            sqlInsertCommand.Clear();
                            sqlInsertCommand.Append("select * from aiscenario where  aiScenarioQuestionIDYes = @aiScenarioQuestionIDYes ");
                            dt1 = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sqlInsertCommand), CommandType.Text, new MySqlParameter[]{
                                                 DbUtility.GetParameter("aiScenarioQuestionIDYes", QuestionYesIDss, MyDbType.String)
                                    });

                            if (dt1.Rows.Count > 0)
                            {
                                DataRow dr = dt1.Rows[0];
                                IncidetORAccident = Convert.ToString(dr["aiScenarioName"]);
                            }
                            foreach (var result1 in AccidentDetails)
                            {
                                if (Convert.ToString(result1.AIPageName) == PageName.PersonalInjury)
                                {
                                    IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                                }
                                if (Convert.ToString(result1.AIPageName) == PageName.LostTime)
                                {
                                    IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                                }
                                if (Convert.ToString(result1.AIPageName) == PageName.WVH)
                                {
                                    IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                                }
                                if (Convert.ToString(result1.AIPageName) == PageName.HealthCareConfirmation)
                                {
                                    IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                                }
                                if (Convert.ToString(result1.AIPageName) == PageName.PropertyDamageQuestion)
                                {
                                    IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                                }
                                if (Convert.ToString(result1.AIPageName) == PageName.FirstAidBox)
                                {
                                    IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                                }
                            }
                        }
                    }
                    else
                    {

                        var AccidentDetails = objAIQuestion.ToList();
                        var incidentResult1 = objAIQuestion.Where(x => x.AIPageName == PageName.IncidentDetail).ToList();
                        if (incidentResult1 != null && incidentResult1.Count > 0)
                        {
                            foreach (var result in incidentResult1)
                            {
                                if (Convert.ToInt32(result.AIQuestionID) == 32)
                                {
                                    IncidentORAcccidentDate = Convert.ToDateTime(result.AIQuestionAnswerText);
                                }

                            }
                        }
                        StringBuilder QuestionYesIDss = new StringBuilder();
                        var QuestionIDYesResult = objAIQuestion.Where(x => x.AIQuestionAnswerText == "1").ToList();
                        if (QuestionIDYesResult != null)
                        {
                            foreach (var getYesIDs in QuestionIDYesResult)
                            {
                                QuestionYesIDss.Append(BusinessUtility.GetString(getYesIDs.AIQuestionID) + ",");
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(QuestionYesIDss)))
                            {
                                QuestionYesIDss.Length--;
                            }
                        }
                        StringBuilder sqlInsertCommand = new StringBuilder();
                        DataTable dt1 = new DataTable();
                        sqlInsertCommand.Clear();
                        sqlInsertCommand.Append("select * from aiscenario where  aiScenarioQuestionIDYes = @aiScenarioQuestionIDYes ");
                        dt1 = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sqlInsertCommand), CommandType.Text, new MySqlParameter[]{
                                                 DbUtility.GetParameter("aiScenarioQuestionIDYes", QuestionYesIDss, MyDbType.String)
                                    });

                        if (dt1.Rows.Count > 0)
                        {
                            DataRow dr = dt1.Rows[0];
                            IncidetORAccident = Convert.ToString(dr["aiScenarioName"]);
                        }
                        foreach (var result1 in AccidentDetails)
                        {
                            if (Convert.ToString(result1.AIPageName) == PageName.PersonalInjury)
                            {
                                IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                            }
                            if (Convert.ToString(result1.AIPageName) == PageName.LostTime)
                            {
                                IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                            }
                            if (Convert.ToString(result1.AIPageName) == PageName.WVH)
                            {
                                IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                            }
                            if (Convert.ToString(result1.AIPageName) == PageName.HealthCareConfirmation)
                            {
                                IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                            }
                            if (Convert.ToString(result1.AIPageName) == PageName.PropertyDamageQuestion)
                            {
                                IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                            }
                            if (Convert.ToString(result1.AIPageName) == PageName.FirstAidBox)
                            {
                                IncidetORAccidentDesc.Append(Convert.ToString(result1.AIQuestionDescription));
                            }
                        }
                    }

                    foreach (var employeeDetail in objEmpDetails)
                    {
                        sbInsertQuery.Append(" insert into aiformheader(aiempheaderID, aiemailID, aiaffectedEmpID, aisiteNo,aiLocationType, aiISLicense, createdOn, createdBy, aiISCompleted, aiISCompletedDateTime,  aiISCompletedBy, aiEmpName, aiEmpSiteName, aiReportType, aiReportTypeDescription, aiIncidentAccidentDateTime) ");
                        sbInsertQuery.Append(" values(@aiempheaderID, @aiemailID, @aiaffectedEmpID, @aisiteNo,@aiLocationType, @aiISLicense, @createdOn, @createdBy, @aiISCompleted, @aiISCompletedDateTime,  @aiISCompletedBy, @aiEmpName, @aiEmpSiteName, @aiReportType, @aiReportTypeDescription, @aiIncidentAccidentDateTime) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("aiempheaderID", employeeDetail.EmpID, MyDbType.Int),    
                                    DbUtility.GetParameter("aiemailID", employeeDetail.EmpEmail, MyDbType.String),
                                    DbUtility.GetParameter("aiaffectedEmpID", employeeDetail.EmpExtID, MyDbType.String),
                                    DbUtility.GetParameter("aisiteNo", employeeDetail.Location, MyDbType.String),
                                    DbUtility.GetParameter("aiLocationType", aiLocationType, MyDbType.String),
                                    DbUtility.GetParameter("aiISLicense", aiISLicense, MyDbType.Int),
                                    DbUtility.GetParameter("createdOn", DateTime.Now  , MyDbType.DateTime),
                                    DbUtility.GetParameter("createdBy", employeeDetail.EmpID  , MyDbType.Boolean),
                                    DbUtility.GetParameter("aiISCompleted",  Convert.ToInt32(reportStatus)  , MyDbType.Int),
                                    DbUtility.GetParameter("aiISCompletedDateTime", DateTime.Now  , MyDbType.DateTime),
                                    DbUtility.GetParameter("aiISCompletedBy", employeeDetail.EmpID, MyDbType.Int),
                                    DbUtility.GetParameter("aiEmpName", employeeDetail.EmpName, MyDbType.String),
                                    DbUtility.GetParameter("aiEmpSiteName", employeeDetail.Division, MyDbType.String),


                                    DbUtility.GetParameter("aiReportType", IncidetORAccident , MyDbType.String),
                                    DbUtility.GetParameter("aiReportTypeDescription",  Convert.ToString(IncidetORAccidentDesc), MyDbType.String),
                                    DbUtility.GetParameter("aiIncidentAccidentDateTime", IncidentORAcccidentDate, MyDbType.DateTime)                                         
                               });
                    }

                    lastinsertedFormID = dbTransactionHelper.GetLastInsertID();
                    foreach (var answerList in objAIQuestion)
                    {
                        sbInsertQuery.Clear();
                        if (Convert.ToString(answerList.AIQuestionType) == QuestionType.TextType)
                        {
                            sbInsertQuery.Append(" insert into aitextquestionanswer(aiQuestionTextansID, aiFormTextansID, aiQuestionTextAnswer, aiCreatedOn, aiCreatedBy ) ");
                            sbInsertQuery.Append(" values(@aiQuestionTextansID, @aiFormTextansID, @aiQuestionTextAnswer, @aiCreatedOn, @aiCreatedBy) ");
                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("aiQuestionTextansID", answerList.AIQuestionID, MyDbType.Int),    
                                    DbUtility.GetParameter("aiFormTextansID", lastinsertedFormID, MyDbType.Int),
                                    DbUtility.GetParameter("aiQuestionTextAnswer", answerList.AIQuestionAnswerText, MyDbType.String),
                                    DbUtility.GetParameter("aiCreatedOn", DateTime.Now,  MyDbType.DateTime),
                                    DbUtility.GetParameter("aiCreatedBy", employeeID, MyDbType.Int),                                                                 
                                });
                        }
                        if (Convert.ToString(answerList.AIQuestionType) == QuestionType.IntegerType)
                        {
                            sbInsertQuery.Append(" insert into aitextquestionanswer(aiQuestionIntansID, aiFormIntansID, aiQuestionIntergerAnswer, aiCreatedOn, aiCreatedBy ) ");
                            sbInsertQuery.Append(" values(@aiQuestionIntansID, @aiFormIntansID, @aiQuestionIntergerAnswer, @aiCreatedOn, @aiCreatedBy) ");
                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("aiQuestionIntansID", answerList.AIQuestionID, MyDbType.Int),    
                                    DbUtility.GetParameter("aiFormIntansID", lastinsertedFormID, MyDbType.Int),
                                    DbUtility.GetParameter("aiQuestionIntergerAnswer", answerList.AIQuestionAnswerText, MyDbType.Int),
                                    DbUtility.GetParameter("aiCreatedOn", DateTime.Now,  MyDbType.DateTime),
                                    DbUtility.GetParameter("aiCreatedBy", employeeID, MyDbType.Int),                                                                 
                                });
                        }
                        if (Convert.ToString(answerList.AIQuestionType) == QuestionType.BooleanType)
                        {
                            sbInsertQuery.Append(" insert into aibooleanquestionanswer (aiQuestionboolansID, aiFormboolansID,  aiQuestionboolAnswer, aiCreatedOn, aiCreatedBy)  ");
                            sbInsertQuery.Append(" values(@aiQuestionboolansID, @aiFormboolansID, @aiQuestionboolAnswer, @aiCreatedOn, @aiCreatedBy) ");
                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("aiQuestionboolansID", answerList.AIQuestionID, MyDbType.Int),    
                                    DbUtility.GetParameter("aiFormboolansID", lastinsertedFormID, MyDbType.Int),
                                    DbUtility.GetParameter("aiQuestionboolAnswer",  Convert.ToInt32(answerList.AIQuestionAnswerText), MyDbType.Int),
                                    DbUtility.GetParameter("aiCreatedOn", DateTime.Now,  MyDbType.DateTime),
                                    DbUtility.GetParameter("aiCreatedBy", employeeID, MyDbType.Int),                                                                 
                            });
                        }
                    }

                    #region insertscaneriodetails
                    insertScenarioDetails(objAIQuestion, objEmpDetails, dbTransactionHelper, lastinsertedFormID, aiLocationType);
                    #endregion

                    dbTransactionHelper.CommitTransaction();
                    bREturn = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally { dbTransactionHelper.CloseDatabaseConnection(); }
            return bREturn;
        }

        /// <summary>
        /// To Save AI Scenario Details
        /// </summary>
        /// <param name="objAIQuestion">Pass AI Question List </param>
        /// <param name="objEmpDetails">Pass Employee Detail List</param>
        /// <param name="dbTransactionHelper">Pass DB Transaction Connection</param>
        /// <param name="aiFormHeaderID">Pass AI Form Header ID</param>
        /// <param name="locationType">Pass Location Type</param>
        public void insertScenarioDetails(List<AIQuestionAnswer> objAIQuestion, List<Employee> objEmpDetails, DbTransactionHelper dbTransactionHelper, int aiFormHeaderID, string locationType)
        {
            StringBuilder sqlInsertCommand = new StringBuilder();
            StringBuilder QuestionYesIDs = new StringBuilder();
            StringBuilder QuestionNoIDs = new StringBuilder();
            string scenarioName = string.Empty;
            DataTable dt = new DataTable();
            DataTable scenarioTable = new DataTable();
            int lastInsertedScenarioID = 0;
            int scenarioID = 0;
            List<aiScenarioMaster> objScenario = new List<aiScenarioMaster>();
            string GroupCCIDs = string.Empty;
            string GroupBCCIDs = string.Empty;
            DataTable groupTable = new DataTable();
            try
            {
                if (objAIQuestion != null && objEmpDetails != null)
                {
                    var QuestionIDYesResult = objAIQuestion.Where(x => x.AIQuestionAnswerText == "1").ToList();
                    if (QuestionIDYesResult != null)
                    {
                        foreach (var getYesIDs in QuestionIDYesResult)
                        {
                            QuestionYesIDs.Append(BusinessUtility.GetString(getYesIDs.AIQuestionID) + ",");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(QuestionYesIDs)))
                        {
                            QuestionYesIDs.Length--;
                        }
                    }
                    var QuestionIDNoResult = objAIQuestion.Where(x => x.AIQuestionAnswerText == "0").ToList();
                    if (QuestionIDNoResult != null)
                    {
                        foreach (var getNoIDs in QuestionIDNoResult)
                        {
                            QuestionNoIDs.Append(BusinessUtility.GetString(getNoIDs.AIQuestionID) + ",");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(QuestionNoIDs)))
                        {
                            QuestionNoIDs.Length--;
                        }
                    }
                }
                sqlInsertCommand.Clear();
                sqlInsertCommand.Append("select * from aiscenario where  aiScenarioQuestionIDYes = @aiScenarioQuestionIDYes ");
                scenarioTable = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sqlInsertCommand), CommandType.Text, new MySqlParameter[]{
                                                 DbUtility.GetParameter("aiScenarioQuestionIDYes", QuestionYesIDs, MyDbType.String)
                                    });

                if (scenarioTable.Rows.Count > 0)
                {
                    DataRow dr = scenarioTable.Rows[0];
                    scenarioID = Convert.ToInt32(dr["aiScenarioID"]);
                    sqlInsertCommand.Clear();
                    sqlInsertCommand.Append("select group_concat(aiGroupEmailCCID) as CCEmailID,  group_concat(aiGroupEmailBCCID) as BCCEmailID from  aiemailscenariogrouprelation where aiScenarioID = @ScenarioID;");
                    groupTable = dbTransactionHelper.GetDataTable(Convert.ToString(sqlInsertCommand), CommandType.Text, new MySqlParameter[]{
                                                          DbUtility.GetParameter("ScenarioID", scenarioID, MyDbType.Int)                              
                                                         });

                    if (groupTable.Rows.Count > 0)
                    {
                        DataRow dr1 = groupTable.Rows[0];
                        GroupCCIDs = Convert.ToString(dr1["CCEmailID"]);
                        GroupBCCIDs = Convert.ToString(dr1["BCCEmailID"]);
                    }

                    lastInsertedScenarioID = dbTransactionHelper.GetLastInsertID();
                    sqlInsertCommand.Clear();

                    var EmpDetails = objEmpDetails.FirstOrDefault();
                    string locationGroupName = string.Empty;
                    if (locationType == "R")
                    {
                        locationGroupName = "retail" + "_" + EmpDetails.Location;
                    }
                    else
                    {
                        locationGroupName = "logis" + "_" + EmpDetails.Location;
                    }

                    DataTable dtTable = new DataTable();
                    sqlInsertCommand.Append("select * from  aiemailgroupmaster   where  aiEmailGroupName = @aiEmailGroupName");

                    dtTable = dbTransactionHelper.GetDataTable(Convert.ToString(sqlInsertCommand), CommandType.Text,
                                               new MySqlParameter[]{
                                                   DbUtility.GetParameter("aiEmailGroupName", locationGroupName, MyDbType.String)
                                                 });
                    if (dtTable.Rows.Count > 0)
                    {
                        DataRow dataReader = dtTable.Rows[0];
                        GroupCCIDs = GroupCCIDs + "," + Convert.ToString(dataReader["ID"]);
                    }
                    sqlInsertCommand.Clear();
                    sqlInsertCommand.Append("insert into  aiemailsender(aiEmailScenarioID, aiEmailJobIDs, aiEmailSiteIDs, aiEmailCreatedOn,aiEmailCreatedBy, aiEmailGroupToID, aiEmailGroupCCID, aiEmailGroupBCCID, aiEmailFormID)");
                    sqlInsertCommand.Append("values(@aiEmailScenarioID, @aiEmailJobIDs, @aiEmailSiteIDs, @aiEmailCreatedOn,@aiEmailCreatedBy, @aiEmailGroupToID, @aiEmailGroupCCID, @aiEmailGroupBCCID, @aiEmailFormID)");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sqlInsertCommand), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("aiEmailScenarioID", scenarioID, MyDbType.Int),  
                                DbUtility.GetParameter("aiEmailJobIDs", EmpDetails.EmpExtID, MyDbType.String),    
                                DbUtility.GetParameter("aiEmailSiteIDs", EmpDetails.Location, MyDbType.String),
                                DbUtility.GetParameter("aiEmailCreatedOn", DateTime.Now, MyDbType.DateTime),
                                DbUtility.GetParameter("aiEmailCreatedBy", 1, MyDbType.Int),
                                DbUtility.GetParameter("aiEmailGroupToID", EmpDetails.AIReportInitiatorEmailID,  MyDbType.String),    
                                DbUtility.GetParameter("aiEmailGroupCCID", GroupCCIDs, MyDbType.String),
                                DbUtility.GetParameter("aiEmailGroupBCCID", GroupBCCIDs, MyDbType.String),
                                DbUtility.GetParameter("aiEmailFormID", aiFormHeaderID , MyDbType.Int)                                
                        });
                }



            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                //throw ;
            }
            finally { }
        }
    }

    /// <summary>
    /// To Define AI Question Answer Object
    /// </summary>
    public class AIQuestionAnswer
    {
        /// <summary>
        /// Get/Set AI Question ID
        /// </summary>
        public int AIQuestionID { get; set; }

        /// <summary>
        /// Get/Set AI Question Type
        /// </summary>
        public string AIQuestionType { get; set; }

        /// <summary>
        /// Get/Set AI Question Text
        /// </summary>
        public string AIQuestionText { get; set; }

        /// <summary>
        /// Get/Set AI Question Answer Text
        /// </summary>
        public string AIQuestionAnswerText { get; set; }

        /// <summary>
        /// Get/Set AI Question Description
        /// </summary>
        public string AIQuestionDescription { get; set; }

        /// <summary>
        /// Get/Set AI Is Active
        /// </summary>
        public int AIIsActive { get; set; }

        /// <summary>
        /// Get/Set AI Is Mandatory
        /// </summary>
        public int AIIsMandatory { get; set; }

        /// <summary>
        /// Get/Set AI Sequence
        /// </summary>
        public int AISequence { get; set; }

        /// <summary>
        /// Get/Set AI Section
        /// </summary>
        public string AISection { get; set; }

        /// <summary>
        /// Get/Set AI Created On DateTime
        /// </summary>
        public DateTime AICreatedOn { get; set; }

        /// <summary>
        /// Get/Set AI Created By
        /// </summary>
        public int AICreatedBy { get; set; }

        /// <summary>
        /// Get/Set AI Form ID
        /// </summary>
        public int AIFormID { get; set; }

        /// <summary>
        /// Get/Set AI Page Name
        /// </summary>
        public string AIPageName { get; set; }

        /// <summary>
        /// Get/Set Is Licensee
        /// </summary>
        public string IsLicensee { get; set; }
    }

    /// <summary>
    /// To Define AI Scenario Master Object
    /// </summary>
    public class aiScenarioMaster
    {
        /// <summary>
        /// Get/Set Scenario ID
        /// </summary>
        public int ScenarioID { get; set; }

        /// <summary>
        /// Get/Set Scenarion Name
        /// </summary>
        public string ScenarioName { get; set; }
    }

    /// <summary>
    /// To Define AI Report Questions Object
    /// </summary>
    public class AIQuestions
    {
        /// <summary>
        /// Get/Set Question ID
        /// </summary>
        public int QuestionID { get; set; }

        /// <summary>
        /// Get/Set Question Text
        /// </summary>
        public string QuestionText { get; set; }
    }

    /// <summary>
    /// To Define Previous Next Page Object
    /// </summary>
    public class ManagePagePreviousNext
    {
        /// <summary>
        /// Get/Set Page Name
        /// </summary>
        public string pageName { get; set; }

        /// <summary>
        /// Get/Set Current Page URl
        /// </summary>
        public string currentPageUrl { get; set; }

        /// <summary>
        /// Get/Set Previour Page URL
        /// </summary>
        public string PreviousPageUrl { get; set; }

        /// <summary>
        /// Get/Set Next Page URL
        /// </summary>
        public string NextPageUrl { get; set; }
    }

}
