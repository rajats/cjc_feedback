﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTECH.Library.Utilities;
using iTECH.Library.Web;
using System.Configuration;
using System.Data.Odbc;
using System.Data;
using System.IO;


namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Define AI Reports Email Functionality  
    /// </summary>
    public class AIEmailCls
    {
        /// <summary>
        /// To Email AI Report Notification Without Attachment 
        /// </summary>
        /// <param name="objAIQuestion">Pass AI Question List</param>
        /// <param name="objEmpDetails">Pass Employee Detail List</param>
        /// <returns>True/False</returns>
        public Boolean SendAINotificationEmail(List<AIQuestionAnswer> objAIQuestion, List<Employee> objEmpDetails)
        {
            StringBuilder bodyString = new StringBuilder();
            StringBuilder natureofReport = new StringBuilder();
            string dateofaccidentincident = string.Empty;
            string reportType = string.Empty;
            string PDFlocationpath = string.Empty;

            try
            {
                var affectedEmployeeDetails = objEmpDetails.FirstOrDefault();
                var aireportDetails = objAIQuestion.ToList();

                if (aireportDetails != null && aireportDetails.Count > 0)
                {
                    var dateoFIncidentAccident = aireportDetails.FirstOrDefault(x => x.AIPageName == PageName.IncidentAccidentDetail && x.AISequence == 1);
                    if (dateoFIncidentAccident != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dateoFIncidentAccident.AIQuestionAnswerText)))
                        {
                            dateofaccidentincident = Convert.ToDateTime(dateoFIncidentAccident.AIQuestionAnswerText).ToString("yyyy-MM-dd");
                        }
                    }
                    var result1 = aireportDetails.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                    if (result1 != null)
                    {
                        natureofReport.Append("Incident" + ",");
                        var aireportType = aireportDetails.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic);
                        if (aireportType != null)
                        {
                            if (Convert.ToString(aireportType.AIQuestionAnswerText) == "R")
                            {
                                reportType = "RETAIL A/I";
                            }
                            else
                            {
                                reportType = "LOGISTICS A/I";
                            }
                        }
                    }
                    else
                    {
                        var aireportType = aireportDetails.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic);
                        if (aireportType != null)
                        {
                            if (Convert.ToString(aireportType.AIQuestionAnswerText) == "R")
                            {
                                reportType = "RETAIL A/I";
                            }
                            else
                            {
                                reportType = "LOGISTICS A/I";
                            }
                        }

                        var case2Result = aireportDetails.Where(x => x.AIQuestionAnswerText == "1").ToList();
                        if (case2Result != null && case2Result.Count > 0)
                        {

                            foreach (var items in case2Result)
                            {
                                if (Convert.ToString(items.AIPageName) == PageName.PersonalInjury)
                                {
                                    natureofReport.Append("  Personal  Injury" + " " + ",");
                                }
                                if (Convert.ToString(items.AIPageName) == PageName.FirstAidBox)
                                {
                                    natureofReport.Append("  First Aid" + " " + ",");
                                }
                                if (Convert.ToString(items.AIPageName) == PageName.HealthCareConfirmation)
                                {
                                    natureofReport.Append("  Health  Care" + " " + ",");
                                }
                                if (Convert.ToString(items.AIPageName) == PageName.LostTime)
                                {
                                    natureofReport.Append("  Lost Time" + " " + ",");
                                }
                                if (Convert.ToString(items.AIPageName) == PageName.PropertyDamageQuestion)
                                {
                                    natureofReport.Append("  Property  Damage" + " " + ",");
                                }

                                if (Convert.ToString(items.AIPageName) == PageName.WVH)
                                {
                                    natureofReport.Append("  WVH" + " " + ",");
                                }
                            }
                        }
                    }


                }
                if (!string.IsNullOrEmpty(Convert.ToString(natureofReport)))
                {
                    natureofReport.Length--;
                }

                bodyString.Clear();

                #region mailbodysection
                bodyString.Append("<div>" + reportType + "</div>");
                bodyString.Append("<div>TBS - Site #" + Convert.ToString(affectedEmployeeDetails.Location) + " - <div>");
                bodyString.Append("<div>Affected employee: " + Convert.ToString(affectedEmployeeDetails.EmpName) + " (#" + Convert.ToString(affectedEmployeeDetails.EmpExtID) + ")<div>");
                bodyString.Append("<div>Nature of report:    " + Convert.ToString(natureofReport) + " <div>");
                bodyString.Append("<div>Brief description:  </div>");
                bodyString.Append("<div>Body part(s) affected (if applicable):  </div>");
                #endregion

                EmailHelper.SendEmail(EmailFrom, Convert.ToString(affectedEmployeeDetails.AIReportInitiatorEmailID), Convert.ToString(bodyString), reportType, true);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally { }
            return true;
        }

        /// <summary>
        /// To Email AI Report Notification With Attachment 
        /// </summary>
        /// <param name="objAIQuestion">Pass AI Question List</param>
        /// <param name="objEmpDetails">Pass AI Employee Details</param>
        /// <param name="attachmentFilePath">Pass Attached File Path</param>
        /// <returns>True/False</returns>
        public Boolean SendAINotificationEmail(List<AIQuestionAnswer> objAIQuestion, List<Employee> objEmpDetails, string attachmentFilePath)
        {
            StringBuilder bodyString = new StringBuilder();
            StringBuilder natureofReport = new StringBuilder();
            string dateofaccidentincident = string.Empty;
            string reportType = string.Empty;
            string PDFlocationpath = string.Empty;

            try
            {
                var affectedEmployeeDetails = objEmpDetails.FirstOrDefault();
                var aireportDetails = objAIQuestion.ToList();

                if (aireportDetails != null && aireportDetails.Count > 0)
                {
                    var dateoFIncidentAccident = aireportDetails.FirstOrDefault(x => x.AIPageName == PageName.IncidentAccidentDetail && x.AISequence == 1);
                    if (dateoFIncidentAccident != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dateoFIncidentAccident.AIQuestionAnswerText)))
                        {
                            dateofaccidentincident = Convert.ToDateTime(dateoFIncidentAccident.AIQuestionAnswerText).ToString("yyyy-MM-dd");
                        }
                    }
                    var result1 = aireportDetails.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                    if (result1 != null)
                    {
                        natureofReport.Append("Incident" + ",");
                        var aireportType = aireportDetails.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic);
                        if (aireportType != null)
                        {
                            if (Convert.ToString(aireportType.AIQuestionAnswerText) == "R")
                            {
                                reportType = "RETAIL A/I";
                            }
                            else
                            {
                                reportType = "LOGISTICS A/I";
                            }
                        }
                    }
                    else
                    {
                        var aireportType = aireportDetails.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic);
                        if (aireportType != null)
                        {
                            if (Convert.ToString(aireportType.AIQuestionAnswerText) == "R")
                            {
                                reportType = "RETAIL A/I";
                            }
                            else
                            {
                                reportType = "LOGISTICS A/I";
                            }
                        }

                        var case2Result = aireportDetails.Where(x => x.AIQuestionAnswerText == "1").ToList();
                        if (case2Result != null && case2Result.Count > 0)
                        {

                            foreach (var items in case2Result)
                            {
                                if (Convert.ToString(items.AIPageName) == PageName.PersonalInjury)
                                {
                                    natureofReport.Append("  Personal  Injury" + " " + ",");
                                }
                                if (Convert.ToString(items.AIPageName) == PageName.FirstAidBox)
                                {
                                    natureofReport.Append("  First Aid" + " " + ",");
                                }
                                if (Convert.ToString(items.AIPageName) == PageName.HealthCareConfirmation)
                                {
                                    natureofReport.Append("  Health  Care" + " " + ",");
                                }
                                if (Convert.ToString(items.AIPageName) == PageName.LostTime)
                                {
                                    natureofReport.Append("  Lost Time" + " " + ",");
                                }
                                if (Convert.ToString(items.AIPageName) == PageName.PropertyDamageQuestion)
                                {
                                    natureofReport.Append("  Property  Damage" + " " + ",");
                                }

                                if (Convert.ToString(items.AIPageName) == PageName.WVH)
                                {
                                    natureofReport.Append("  WVH" + " " + ",");
                                }
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(natureofReport)))
                {
                    natureofReport.Length--;
                }

                bodyString.Clear();

                #region mailbodysection
                bodyString.Append("<div>" + reportType + "</div>");
                bodyString.Append("<div>TBS - Site #" + Convert.ToString(affectedEmployeeDetails.Location) + " - <div>");
                bodyString.Append("<div>Affected employee: " + Convert.ToString(affectedEmployeeDetails.EmpName) + " (#" + Convert.ToString(affectedEmployeeDetails.EmpExtID) + ")<div>");
                bodyString.Append("<div>Nature of report:    " + Convert.ToString(natureofReport) + " <div>");
                bodyString.Append("<div>Brief description:  </div>");
                bodyString.Append("<div>Body part(s) affected (if applicable):  </div>");
                #endregion

                EmailHelper.SendEmail(EmailFrom, Convert.ToString(affectedEmployeeDetails.AIReportInitiatorEmailID), Convert.ToString(bodyString), reportType, attachmentFilePath);

            }
            catch { }
            finally { }
            return true;
        }


        /// <summary>
        /// To Get Support Email ID
        /// </summary>
        public static string EmailFrom
        {
            get
            {
                //return BusinessUtility.GetString(ConfigurationManager.AppSettings["SupportEmailFrom"]);
                return BusinessUtility.GetString(AppConfig.GetAppConfigValue("SupportEmailFrom"));
            }
        }

    }
}
