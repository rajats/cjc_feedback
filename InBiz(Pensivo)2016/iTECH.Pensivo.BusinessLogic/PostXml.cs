﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;
using System.Net;
using System.Xml;
using System.IO;

namespace iTECH.Pensivo.BusinessLogic
{
    /// <summary>
    /// To Include XML Feature Posted By Scorm Player
    /// </summary>
    public class PostXml
    {
        /// <summary>
        /// Get/Set Post XML ID
        /// </summary>
        public int PostXmlID { get; set; }

        /// <summary>
        /// To Get/Set Posted DateTime
        /// </summary>
        public DateTime PostedDate { get; set; }

        /// <summary>
        /// To Get/Set Posted XML Data
        /// </summary>
        public string PostedXmlData { get; set; }

        /// <summary>
        /// To Get/Set Post Type
        /// </summary>
        public PostType PostType { get; set; }

        /// <summary>
        /// To Get/Set Post Direction
        /// </summary>
        public PostDirection PostDirection { get; set; }

        /// <summary>
        /// To Save Posted XML Data
        /// </summary>
        /// <param name="dbHelp">Pass DB Connection</param>
        /// <param name="xmlData">Pass XML Data</param>
        /// <param name="postType">Pass Post Type</param>
        /// <param name="postDirection">Pass Post Direction</param>
        /// <returns>True/False</returns>
        public int SetPostedData(DbHelper dbHelp, string xmlData, PostType postType, PostDirection postDirection, int regID, string sXMLFilePath)
        {
            xmlData = "";

            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "INSERT INTO z_post_xml_file(PostedDate,PostedXmlData,PostType,PostDirection, regID, xmlFilePath) VALUES(@PostedDate,@PostedXmlData,@PostType,@PostDirection, @regID, @xmlFilePath)";
            if (postType == PostType.C)
            {
                sql = "INSERT INTO z_post_xml_file(PostedDate,PostedXmlData,PostType,PostDirection, regID, xmlFilePath) VALUES(@PostedDate, Compress(@PostedXmlData), @PostType,@PostDirection, @regID, @xmlFilePath)";
            }
            else
            {
                sql = "INSERT INTO z_post_xml_file(PostedDate,PostedXmlData,PostType,PostDirection, regID, xmlFilePath) VALUES(@PostedDate,@PostedXmlData,@PostType,@PostDirection, @regID, @xmlFilePath)";
            }

            try
            {
                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("PostedDate", DateTime.Now, MyDbType.DateTime),
                DbUtility.GetParameter("PostedXmlData", xmlData, MyDbType.String),
                DbUtility.GetParameter("PostType", postType.ToString(), MyDbType.String),
                DbUtility.GetParameter("PostDirection", postDirection.ToString(), MyDbType.String),
                DbUtility.GetParameter("regID", regID, MyDbType.Int),
                DbUtility.GetParameter("xmlFilePath", sXMLFilePath, MyDbType.String),
                });

                return dbHelp.GetLastInsertID();
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }

    /// <summary>
    /// To Define Posted XML Type
    /// </summary>
    public enum PostType
    {
        C, // Callback
        G   // GetRegistration
    }

    /// <summary>
    /// To Define Posted XML Direction
    /// </summary>
    public enum PostDirection
    {
        INT,
        EXT
    }
}
