﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmpSearchDashBoard.aspx.cs" Inherits="EmpSearchDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box">
                    <a id="hrfSearchEmpName" runat="server" href="#" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSearchName%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfSearchEmpCode" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title" id="hEmpCode" runat="server"></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" id="dvSite" runat="server">
                    <a id="hrfSearchEmpSite" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title" id="hSite" runat="server"></h4>
                        </div>
                    </a>
                </div>
            </div>
            <div class="boxset" id="dvTypeDivJobCode" runat="server">
                <div class="boxset-box">
                    <a id="hrfSearchEmpRegion" runat="server" href="#" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblEmpType%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" id="dvSearchEmpDivision" runat="server">
                    <a id="hrfSearchEmpDivision" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSearchLocation%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" id="dvSearchEmpJobCode" runat="server">
                    <a id="hrfSearchEmpJobCode" runat="server" href="EmployeeCourses.aspx?CourseStatus=5" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSearchJobCode%></h4>
                        </div>
                    </a>
                </div>
            </div>
            <div class="boxset" id="dvTypeRegDistDiv" runat="server">
                <div class="boxset-box">
                    <a id="hrfSearchEmpTBSDIST" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSearchDistrict%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" id="dvSearchEmpTBSREG" runat="server">
                    <a id="hrfSearchEmpTBSREG" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSearchRegion%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" id="dvSearchEmpTBSDIV" runat="server">
                    <a id="hrfSearchEmpTBSDIV" runat="server" href="#" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSearchDivision%></h4>
                        </div>
                    </a>
                </div>
            </div>
            <div class="boxset" id="dvBDLSITE" runat="server" visible="false">
                <div class="boxset-box">
                    <a id="hrfSearchBDLSite" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSearchSite%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfSearchBDLProvince" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblProvince%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

