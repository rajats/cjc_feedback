﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ReportDateDashBoard.aspx.cs" Inherits="ReportDateDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box">
                    <a id="hrfCurrentMonth" runat="server" href="#" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblCurrentMonth%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfCurrentQuarter" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblCurrentQuarter%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfCalendarYearToDate" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblCalendarYearToDate%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfSpecificDates" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSpecificDates%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
