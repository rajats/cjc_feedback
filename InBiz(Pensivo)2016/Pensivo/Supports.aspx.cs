﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Library.Utilities;
using System.Text;
using iTECH.Pensivo.BusinessLogic;

public partial class Supports : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Utils.TrainingInst == (int)Institute.bdl)
        {
            hdForgetOptionDescMessage.Value = Resources.Resource.lblForgetOptionDescBDL;
        }
        else
        {
            hdForgetOptionDescMessage.Value = Resources.Resource.lblForgetOptionDesc;
        }

        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
        {
            lblEmpID.InnerHtml = Resources.Resource.lblEmail;
            txtemployeenumber.Attributes.Add("placeholder", Resources.Resource.lblEmail);
            pEmpID.InnerHtml = Resources.Resource.lblEmail;
            rfEmpID.Text = Resources.Resource.reqEmpEmailID;
            dvEmail.Visible = false;
            rfMail.Enabled=false;
            regexEmailValid.Enabled = false;
           
            if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)
            {
                hdInstToCompare.Value = BusinessUtility.GetString((int)Institute.AlMurrayDentistry);
            }
            else if (Utils.TrainingInst == (int)Institute.tdc)
            {
                hdInstToCompare.Value = BusinessUtility.GetString((int)Institute.tdc);
            }
            else if (Utils.TrainingInst == (int)Institute.navcanada)
            {
                hdInstToCompare.Value = BusinessUtility.GetString((int)Institute.navcanada);
            }
        }
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            lblEmpID.InnerHtml = Resources.Resource.lblEDE2UserName;
            txtemployeenumber.Attributes.Add("placeholder", Resources.Resource.lblEDE2UserName);
            pEmpID.InnerHtml = Resources.Resource.lblEDE2UserName;
            rfEmpID.Text = Resources.Resource.reqEDE2EmpNumber;
            hdInstToCompare.Value = BusinessUtility.GetString((int)Institute.EDE2);
        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            lblEmpID.InnerHtml = Resources.Resource.lblBDLUserName;
            txtemployeenumber.Attributes.Add("placeholder", Resources.Resource.lblBDLUserName);
            pEmpID.InnerHtml = Resources.Resource.lblBDLUserName;
            rfEmpID.Text = Resources.Resource.reqBDLEmpNumber;
            hdInstToCompare.Value = BusinessUtility.GetString((int)Institute.bdl);
        }
        else
        {
            lblEmpID.InnerHtml = Resources.Resource.lblUserName;
            txtemployeenumber.Attributes.Add("placeholder", Resources.Resource.lblUserName);
            pEmpID.InnerHtml = Resources.Resource.lblUserName;
            rfEmpID.Text = Resources.Resource.reqEmpNumber;
            hdInstToCompare.Value = BusinessUtility.GetString((int)Institute.beercollege);
        }

        hdnCurrentInst.Value = BusinessUtility.GetString(Utils.TrainingInst);
        hdInstToCompare.Value = BusinessUtility.GetString((int)Institute.navcanada);
    }


    /// <summary>
    /// To Define Event To Send a Mail To Support
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event</param>
    protected void btnsend_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            StringBuilder sbMessage = new StringBuilder();

            string clientIp = (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();
            HttpBrowserCapabilities browse = Request.Browser;
            string sUserAgents = Request.UserAgent;
            string platform = browse.Platform;
            string sBrowserDetail = browse.Browser + " ver " + browse.Version;

            sbMessage.Append(Resources.Resource.lblEmpFirstName + ": " + txtfirstname.Value + "<br />");
            sbMessage.Append(Resources.Resource.lblEmpLastName + ": " + txtlastname.Value + "<br />");
            sbMessage.Append(Resources.Resource.lblSpuportFormEmployeeNo + ": " + txtemployeenumber.Value + "<br /><br />");
            sbMessage.Append(Resources.Resource.lblSupportBrowserDetail + ": " + BusinessUtility.GetString(sBrowserDetail) + "<br />");
            sbMessage.Append(Resources.Resource.lblSupportFormIPAddress + ": " + BusinessUtility.GetString(clientIp) + "<br />");
            sbMessage.Append(Resources.Resource.lblUserAgent + ": " + BusinessUtility.GetString(sUserAgents) + "<br /><br />");
            if ((Utils.TrainingInst != (int)Institute.tdc) || (Utils.TrainingInst != (int)Institute.navcanada) || (Utils.TrainingInst != (int)Institute.AlMurrayDentistry) || (Utils.TrainingInst != (int)Institute.EDE2))
            {
                sbMessage.Append(Resources.Resource.lblEmailID + ": " + txtemail.Value + "<br />");
            }
            sbMessage.Append(Resources.Resource.lblQuestion + ": " + ddlQuestion.SelectedItem.Text + "<br />");
            sbMessage.Append("<div style='float:left;'>" + Resources.Resource.lblContent + ": </div><div style='float:left;'>" + txtArea.InnerHtml.Replace("\r\n", "<br/>") + "</div><br />");

            string sDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string EmailFrom = Utils.SupportEmailFrom;
            string EmailSubject = Convert.ToString(txtemail.Value) + " on " + sDateTime + "  " + ddlQuestion.SelectedValue;
            string[] EmailTo = Utils.SupportEmailTo.Split(',');
            foreach (string sMailTo in EmailTo)
            {
                EmailHelper.SendEmail(EmailFrom, sMailTo, BusinessUtility.GetString(sbMessage), EmailSubject, true);
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowPensivoMessage('" + Resources.Resource.lblSupportMessageSent + "', '" + "Faqs.aspx" + "');", true);
        }

    }
}