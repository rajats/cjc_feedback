﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;


public partial class AssignTrainingEventEmployee : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Assign_Training);
        if (!IsPostBack && !IsPagePostBack(gvUser))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSelectTheUser, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            ltrTitle.Text = Resources.Resource.lblSelectTheUser;
            hdnAssignByUserID.Value = BusinessUtility.GetString(CurrentEmployee.EmpID);
            hdnTrainingEvent.Value = this.TrainingEventID;
            Employee objEmp = new Employee();
            Event objEvent = new Event();
            int empID = objEmp.GetEmpID(this.EmpCode);
            objEmp.GetEmployeeDetail(empID);

            string sMessage = "";
            sMessage = Resources.Resource.lblMsgAssignTrainingEventAddUserConfirmation.Replace("#Name#", objEmp.EmpName + " (" + objEmp.EmpLogInID + ")");

            string sTrainingEventName = "";
            string[] searchval = this.TrainingEventID.Split(',');
            foreach (string sValues in searchval)
            {
                objEvent = new Event();
                objEvent.GetEventDetail(BusinessUtility.GetInt(sValues), Globals.CurrentAppLanguageCode);
                if (sTrainingEventName == "")
                {
                    sTrainingEventName = objEvent.EventName;
                }
                else
                {
                    sTrainingEventName += "<br/>" + objEvent.EventName;
                }
            }
            HdnMessage.Value = sMessage.Replace("#EVENTNAME#", sTrainingEventName);
        }


        if (Utils.TrainingInst == (int)Institute.bdl)
        {
            gvUser.Columns[3].HeaderText = Resources.Resource.lblEmpoyeeID;
        }
        else if ((Utils.TrainingInst == (int)Institute.EDE2))
        {
            gvUser.Columns[3].HeaderText = Resources.Resource.lblEmpoyeeID;
        }
        else if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry))
        {
            gvUser.Columns[3].HeaderText = Resources.Resource.lblEmpEmailID;
        }

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "assignUserTrainingEvent")
        {
            try
            {
                int userID = BusinessUtility.GetInt(Request.Form["UserID"]);
                string trainingEnventID = BusinessUtility.GetString(Request.Form["TrainingEnventID"]);
                int assignByUserID = BusinessUtility.GetInt(Request.Form["AssignByUserID"]);
                int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                if (trainingEnventID != "" && userID > 0)
                {
                    objRole = new Role();
                    if (objRole.AssignUserTrainingEvent(userID, trainingEnventID, assignByUserID) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }
            Response.End();
            Response.SuppressContent = true;
        }
    }

    /// <summary>
    /// To Define JQ Grid Cell Binding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event Args</param>
    protected void gvUser_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 4)
        {
            e.CellHtml = string.Format(@"<a href=""javascript:;""  onclick=""AddUsers({0})"">" + Resources.Resource.lblAdd + "</a>", e.CellHtml);
        }
    }

    /// <summary>
    /// To Bind JQ Grid with User List
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvUser_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Role objRole = new Role();
        gvUser.DataSource = objRole.GetUsersAssignTrainingEvent(Utils.ReplaceDBSpecialCharacter(this.EmpCode));
        gvUser.DataBind();
    }

    /// <summary>
    /// To Get Training Event ID
    /// </summary>
    public string TrainingEventID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["trainingeventid"]);
        }
    }

    /// <summary>
    /// To Get Employee Code
    /// </summary>
    public string EmpCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["empcode"]);
        }
    }

}