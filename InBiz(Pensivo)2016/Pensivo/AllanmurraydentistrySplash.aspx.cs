﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;

public partial class AllanmurraydentistrySplash : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnEnglish_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Session["Language"] = AppCulture.ENGLISH_CANADA;
        Response.Redirect("public.aspx");
    }
    protected void btnFrench_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Session["Language"] = AppCulture.FRENCH_CANADA;
        Response.Redirect("public.aspx");
    }
}