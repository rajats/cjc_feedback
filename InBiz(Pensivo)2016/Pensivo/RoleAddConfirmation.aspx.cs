﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class RoleAddConfirmation : BasePage
{
    /// <summary>
    /// Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string roleName = "";
            string empName = "";

            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblConfirmation, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            HdnFlag.Value = this.Flag;
            if (this.RoleID > 0)
            {
                 objRole = new Role();
                roleName = objRole.GetRoleName(this.RoleID);
            }

            if (this.UserID > 0)
            {
                Employee objRole = new Employee();
                objRole.GetEmployeeDetail(this.UserID);
                empName = objRole.EmpName;
            }

            ltrTitle.Text = Resources.Resource.lblAddedUserToTheSystemRole+  " '" + roleName + "' : " + empName;
            hdnUserID.Value = BusinessUtility.GetString( this.UserID);
            hdnRoleID.Value = BusinessUtility.GetString(this.RoleID);
            hdnSearchBy.Value = this.SearchBy;
            hdnSearchValue.Value = this.SearchValue;
            hrefNo.HRef = "RoleAddUser.aspx?roleID=" + this.RoleID + "&searchby=" + this.SearchBy + "&searchval=" + this.SearchValue + "";

            if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addUser")
            {
                try
                {
                    int userID = BusinessUtility.GetInt(Request.Form["UserID"]);
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);

                    if (userID > 0 && roleID > 0)
                    {
                        List<Role> lRoleUser = new List<Role>();
                        lRoleUser.Add(new Role { UserID = userID });
                        objRole = new Role();
                        if (objRole.RoleAddUsers(roleID, lRoleUser) == true)
                        {
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }
                Response.End();
                Response.SuppressContent = true;
            }
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get UserID
    /// </summary>
    public int UserID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["userID"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Flag for Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }
}