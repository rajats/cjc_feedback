﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using RusticiSoftware.HostedEngine.Client;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Net;

public partial class EmployeeViewCourses : System.Web.UI.Page
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.GetPostedXML();
    }

    /// <summary>
    /// To Get XML Content Posted By SCROM
    /// </summary>
    private void GetPostedXML()
    {
        Boolean result = false;
        try
        {
            string strxml = string.Empty;
            Response.Clear();
            Response.ContentType = "text/xml";
            string xmlData = string.Empty;
            string payLoadID = string.Empty;
            string orderID = string.Empty;

            PostXml postXmlTrack = new PostXml();
            int postXmlTrackID = -1;
            DbHelper dbHelp = new DbHelper(true);

            //Read Requested Xml Data
            using (System.IO.StreamReader reader = new System.IO.StreamReader(Page.Request.InputStream))
            {
                xmlData = HttpUtility.UrlDecode(reader.ReadToEnd());
            }
            if (!string.IsNullOrEmpty(xmlData))
            {
                //First step to save Raw xml posted data to database for future reference
                int iRegID = Import.GetRegIDFromXML(HttpUtility.UrlDecode(xmlData.Replace("data=", "")), BusinessUtility.GetString(PostType.C));
                string sXMLFilePath = Import.SaveXMLFile(HttpUtility.UrlDecode(xmlData.Replace("data=", "")), BusinessUtility.GetString(PostType.C), iRegID);


                postXmlTrackID = postXmlTrack.SetPostedData(dbHelp, xmlData.Replace("data=", ""), PostType.C, PostDirection.INT, iRegID, sXMLFilePath);
                result = Import.SaveRegistrationXML(HttpUtility.UrlDecode(xmlData.Replace("data=", "")), BusinessUtility.GetString(PostType.C));
            }
            else
            {
                ErrorLog.createLog("XML Data Not Found");
            }
        }
        catch (Exception e)
        {
            ErrorLog.createLog(e.ToString() + "  " + result);
        }
    }
}