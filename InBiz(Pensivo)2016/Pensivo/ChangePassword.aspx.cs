﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class ChangePassword : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        dvLogInErrMsg.Visible = false;
    }

    /// <summary>
    /// To Change Pass and move to next page
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void BtnChangePass_OnClick(object sender, EventArgs e)
    {
        Employee objEmpGetDetail = new Employee();
        objEmpGetDetail.GetEmployeeDetail(this.EmpID);
        if (objEmpGetDetail.EmpExtID == txtNewPassword.Text)
        {
            GlobalMessage.showAlertMessageWithFocus(Resources.Resource.lblCannotUserUserIDasPassword, txtNewPassword.ClientID);
            txtNewPassword.Focus();
            return;
        }

        Employee objEmp = new Employee();
        if (objEmp.UpdateEmployeePassword(this.EmpID, txtNewPassword.Text) == true)
        {
            if ((BusinessUtility.GetInt(Request.QueryString["QID"]) > 0) && (BusinessUtility.GetInt(Request["QOtherID"]) > 0))
            {
                objEmp.InsertEmployeeAnswer(this.EmpID, BusinessUtility.GetInt(Request.QueryString["QID"]), BusinessUtility.GetString(Request.QueryString["QAns"]));
                objEmp.InsertEmployeeAnswer(this.EmpID, BusinessUtility.GetInt(Request["QOtherID"]), BusinessUtility.GetString(Request.QueryString["QotherAns"]));
            }

            objEmp.GetEmployeeDetail(this.EmpID);
            if (Utils.TrainingInst == (int)Institute.bdl || Utils.TrainingInst == (int)Institute.beercollege)
            {
                PensivoExecutions.PensivoPostData("", "", txtNewPassword.Text, "", BusinessUtility.GetInt(objEmp.PensivoUUID));
            }
            EmployeeValidation objEmpValidate = new EmployeeValidation();
            if (objEmpValidate.ValidateEmployee(objEmp.EmpExtID, txtNewPassword.Text) == true)
            {
                Response.Redirect("employeedashboard.aspx");
            }
        }
        else
        {
            showMessage(Resources.Resource.lblMsgNotAbleToChangePassword);
        }
    }

    /// <summary>
    /// To Get Employee ID
    /// </summary>
    private int EmpID { get { return BusinessUtility.GetInt(Request.QueryString["uid"]); } }

    /// <summary>
    /// To Show Message
    /// </summary>
    /// <param name="message">Pass Message</param>
    private void showMessage(string message)
    {
        lblErrMsg.Text = message;
        dvLogInErrMsg.Visible = true;
    }
}