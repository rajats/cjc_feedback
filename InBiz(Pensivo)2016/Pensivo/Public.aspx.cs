﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text.RegularExpressions;

public partial class Public : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Utils.TrainingInst == (int)Institute.beercollege)
        {
            pBdl.Visible = false;
            ptbs.Visible = true;
        }
        if (Utils.TrainingInst == (int)Institute.bdl)
        {
            pBdl.Visible = true;
            ptbs.Visible = false;

            ulHtmlContal.Visible = false;
            //pHtmlContal.Visible = false;
        }
        if ((Utils.TrainingInst == (int)Institute.tdc) )
        {
            dvtbs.Visible = false;
            dvtdc.Visible = true;
        }
        if ((Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.EDE2))
        {
            dvtbs.Visible = false;
            dvtdc.Visible = false;
            dvNavacanada.Visible = true;
        }
        if ((Utils.TrainingInst == (int)Institute.AlMurrayDentistry))
        {
            dvtbs.Visible = false;
            dvtdc.Visible = false;
            dvNavacanada.Visible = false;
            dvAlMurrayDentistry.Visible = true;
        }
        CreateNewUser();
    }

    /// <summary>
    /// To Create New User
    /// </summary>
    private void CreateNewUser()
    {
        if (this.Source.ToUpper() == UserCreatedSource.New.ToUpper())
        {
            if (this.UUID != "" && this.ExtID != "")
            {
                Regex rgx = new Regex(Utils.EmpIdValidatorExpression);
                if (!rgx.IsMatch(this.ExtID))
                {
                    if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
                    {
                        GlobalMessage.showAlertMessage(Resources.Resource.lblTDCInvalidLogInIDFormat);
                    }
                    else if (Utils.TrainingInst == (int)Institute.EDE2)
                    {
                        GlobalMessage.showAlertMessage(Resources.Resource.lblEDE2InvalidLogInIDFormat);
                    }
                    else if (Utils.TrainingInst == (int)Institute.bdl)
                    {
                        GlobalMessage.showAlertMessage(Resources.Resource.lblBdlInvalidLogInIDFormat);
                    }
                    else
                    {
                        GlobalMessage.showAlertMessage(Resources.Resource.lblTBSInvalidLogInIDFormat);
                    }
                    return;
                }

                Employee objEmp = new Employee();
                if (objEmp.IsLoginIDExists(this.ExtID) == false)
                {
                    showAlertMessage();
                    return;
                }

                if (objEmp.IsUUIDExists(this.UUID) == false)
                {
                    showAlertMessage();
                    return;
                }
                Response.Redirect("EmployeeShortDetail.aspx?src=" + UserCreatedSource.New + "&EMPID=" + this.ExtID + "&UUID=" + this.UUID + "");
            }
            else
            {
                showAlertMessage();
            }
        }
    }

    /// <summary>
    /// To Get User Creating Source
    /// </summary>
    private string Source
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["src"]);
        }
    }

    /// <summary>
    /// To Get Pensivo UUID
    /// </summary>
    private string UUID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["uuid"]);
        }
    }

    /// <summary>
    /// To Get User ID
    /// </summary>
    private string ExtID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["EmpID"]);
        }
    }

    /// <summary>
    /// To Show Message
    /// </summary>
    private void showAlertMessage()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowPensivoMessage('" + Resources.Resource.lblPleaseContactAdministrator + "');", true);
    }

}