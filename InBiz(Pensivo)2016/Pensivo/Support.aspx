﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="Support.aspx.cs" Inherits="Support" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function disableButton(sender, group) {
            Page_ClientValidate(group);
            if (Page_IsValid) {
                sender.disabled = "disabled";
                __doPostBack(sender.name, '');
            }
        }
</script> 
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper width-med">
            <h1><%=Resources.Resource.lblSupportContact %></h1>
            <div class="boxed-content">
                <header class="form-section-header">
                    <h6><%=Resources.Resource.lblSupportNeedHelp %></h6>
                    <p><%=Resources.Resource.lblSupportQuestionComplaints %>
                    </p>
                </header>
                <asp:Panel ID="pnlContent" runat="server" DefaultButton="btnsend">
                    <div class="form-body">

                        <div class="plms-fieldset is-first">
                            <label for="email-address" class="plms-label is-hidden"><%=Resources.Resource.EmailAdd %></label>
                            <div class="plms-tooltip-parent">
                                <input type="text" name="email-address" id="txtemail" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, EmailAdd %>">

                                <asp:RequiredFieldValidator ID="rfMail" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                    ControlToValidate="txtemail" Text="<%$ Resources:Resource, reqMail %>" />
                                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" class="formels-feedback invalid" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail" Display="Dynamic" ValidationGroup="vg1" ErrorMessage="<%$ Resources:Resource, errMsgInvalidMailFormat %>" SetFocusOnError="true"></asp:RegularExpressionValidator>

                                <div class="plms-tooltip plms-tooltip-micro left-top outside autow label-replacement" style="display: none;">
                                    <div class="plms-tooltip-body">
                                        <p><%=Resources.Resource.EmailAdd %></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="plms-fieldset">
                            <label for="account-type" class="plms-label is-hidden"><%=Resources.Resource.lblSupportFAQQuestion1 %></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlQuestion" runat="server" CssClass="plms-select skin2 is-placeholder">
                                    <asp:ListItem class="option-placeholder" Text="<%$ Resources:Resource, lblSupportFAQQuestion1 %>" Value="<%$ Resources:Resource, lblSupportFAQQuestion1 %>" />
                                    <asp:ListItem Text="<%$ Resources:Resource, lblSupportFAQQuestion3 %>" Value="<%$ Resources:Resource, lblSupportFAQQuestion3 %>" />
                                    <asp:ListItem Text="<%$ Resources:Resource, lblSupportFAQQuestion4 %>" Value="<%$ Resources:Resource, lblSupportFAQQuestion4 %>" />
                                    <asp:ListItem Text="<%$ Resources:Resource, lblSupportFAQQuestion5 %>" Value="<%$ Resources:Resource, lblSupportFAQQuestion5 %>" />
                                    <asp:ListItem Text="<%$ Resources:Resource, lblSupportFAQQuestion6 %>" Value="<%$ Resources:Resource, lblSupportFAQQuestion6 %>" />
                                    <asp:ListItem Text="<%$ Resources:Resource, lblSupportFAQQuestion7 %>" Value="<%$ Resources:Resource, lblSupportFAQQuestion7 %>" />
                                    <asp:ListItem Text="<%$ Resources:Resource, lblSupportFAQQuestion8 %>" Value="<%$ Resources:Resource, lblSupportFAQQuestion8 %>" />
                                    <asp:ListItem Text="<%$ Resources:Resource, lblSupportFAQQuestion9 %>" Value="<%$ Resources:Resource, lblSupportFAQQuestion9 %>" />
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" InitialValue="<%$ Resources:Resource, lblSupportFAQQuestion1 %>" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                    ControlToValidate="ddlQuestion" Text="<%$ Resources:Resource, lblPleaseSelectHowIcanHelp %>" />


                                <div class="plms-tooltip plms-tooltip-micro left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p>Account Type</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="plms-fieldset">
                            <label for="email-address" class="plms-label is-hidden"><%=Resources.Resource.textArea %></label>
                            <div class="plms-tooltip-parent">
                                <textarea placeholder="<%$ Resources:Resource, textArea %>" name="help-topic-other" id="txtArea" runat="server" class="plms-textarea skin2" cols="4" rows="4"></textarea>
                                <asp:RequiredFieldValidator ID="rdtextarea" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                    ControlToValidate="txtArea" Text="<%$ Resources:Resource, reqText %>" />
                                <div class="plms-tooltip plms-tooltip-micro left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%=Resources.Resource.textArea %></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <footer class="form-footer">
                            <asp:Button ID="btnsend" runat="server" CssClass="btn round" Text="<%$ Resources:Resource, btnsend%>"  ValidationGroup="vg1" OnClick="btnsend_Click"  OnClientClick="disableButton(this,'')" UseSubmitBehavior="false"/>
                        </footer>
                    </div>
                </asp:Panel>
            </div>
            <%--Include Required Js File in Page--%>
            <script src="_js/respond.min.js"></script>
            <script src="_js/modernizr.custom.159181214.js"></script>
            <script src="_js/jquery-1.11.1.min.js"></script>
            <script src="_js/main.min.js"></script>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#<%=txtemail.ClientID%>").focus();
                            });
        </script>

    </section>
</asp:Content>

