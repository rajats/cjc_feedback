﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections;

public partial class UserProfileCreate : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        showMessge("");
        valEmpIDExp.ValidationExpression = Utils.EmpIdValidatorExpression;
        if (!IsPostBack)
        {
            GetEmpDetail();
            if (this.UserID > 0)
            {
                hdnUserID.Value = BusinessUtility.GetString(this.UserID);
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblEditUser, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
                ltrTitle.Text = Resources.Resource.lblEditUser;
                btnCreateEmployee.Text = Resources.Resource.lblUpdateEmployee;
                rfPassword.Enabled = false;

                if (CurrentEmployee.EmpType == EmpType.SuperAdmin)
                {
                    dvChangePassword.Visible = true;
                }
                //if (AuthorizedToViewPage.GetAuthorizedToView(CurrentEmployee.EmpID, (int)RoleAction.Reset_User) && (CurrentEmployee.EmpType != EmpType.SuperAdmin))
                //{
                //    dvChangePassword.Visible = true;
                //}
                //else if (CurrentEmployee.EmpType == EmpType.SuperAdmin)
                //{
                //    dvChangePassword.Visible = true;
                //}
            }
            else
            {
                ltrTitle.Text = Resources.Resource.lblCreateUser;
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblCreateUser, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            }
        }

        Employee objEmp = new Employee();
        objEmp.GetEmployeeDetail(this.UserID);
        hdnConfirmationMsg.Value = Resources.Resource.msgConfirmationResetPassQuestion.Replace("#USERDETAIL#", objEmp.EmpName + " (" + objEmp.EmpExtID + ")");

        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) || (Utils.TrainingInst == (int)Institute.EDE2))
        {
            dvEmail.Visible = false;
            rfMail.Enabled = false;
            regexEmailValid.Enabled = false;
            dvjobcode.Visible = false;
            rfJobCode.Enabled = false;
            dvStore.Visible = false;
            rfddlStore.Enabled = false;
            dvDivission.Visible = false;
            rfddlDivision.Enabled = false;
            dvType.Visible = false;
            rfddlType.Enabled = false;
            dvTypeParsed.Visible = false;
            rfddlTypeParsed.Enabled = false;

            if (Utils.TrainingInst == (int)Institute.EDE2)
            {
                lblEmpID.InnerHtml = Resources.Resource.lblEDE2UserName;
                txtEmpID.Attributes.Add("placeholder", Resources.Resource.lblEDE2UserName);
                pEmpID.InnerHtml = Resources.Resource.lblEDE2UserName;
                rfEmpID.Text = Resources.Resource.reqEDE2EmpNumber;
                valEmpIDExp.Text = Resources.Resource.lblEDE2InvalidLogInIDFormat;
            }
            else
            {
                lblEmpID.InnerHtml = Resources.Resource.lblEmail;
                txtEmpID.Attributes.Add("placeholder", Resources.Resource.lblEmail);
                pEmpID.InnerHtml = Resources.Resource.lblEmail;
                rfEmpID.Text = Resources.Resource.reqEmpEmailID;
                valEmpIDExp.Text = Resources.Resource.lblTDCInvalidLogInIDFormat;
            }

            hdnInformationMsg.Value = Resources.Resource.msgInformationResetPassQuestionTDC.Replace("#EMPEXTID#", objEmp.EmpExtID);
        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            lblEmpID.InnerHtml = Resources.Resource.lblBDLUserName;
            txtEmpID.Attributes.Add("placeholder", Resources.Resource.lblBDLUserName);
            pEmpID.InnerHtml = Resources.Resource.lblBDLUserName;
            rfEmpID.Text = Resources.Resource.reqBDLEmpNumber;
            valEmpIDExp.Text = Resources.Resource.lblBdlInvalidLogInIDFormat;

            hdnInformationMsg.Value = Resources.Resource.msgInformationResetPassQuestionBDL.Replace("#EMPEXTID#", objEmp.EmpExtID);
            if (this.UserID > 0)
            {
                dvProvince.Visible = true;
            }
        }

        else
        {
            lblEmpID.InnerHtml = Resources.Resource.lblUserName;
            txtEmpID.Attributes.Add("placeholder", Resources.Resource.lblUserName);
            pEmpID.InnerHtml = Resources.Resource.lblUserName;
            rfEmpID.Text = Resources.Resource.reqEmpNumber;
            valEmpIDExp.Text = Resources.Resource.lblTBSInvalidLogInIDFormat;

            hdnInformationMsg.Value = Resources.Resource.msgInformationResetPassQuestionTBS.Replace("#EMPEXTID#", objEmp.EmpExtID);
        }

        if ((Utils.TrainingInst == (int)Institute.beercollege) || Utils.TrainingInst == (int)Institute.bdl)
        {
            rfMail.Enabled = false;
        }
    }

    /// <summary>
    /// To Create User Profile
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnCreateEmployee_OnClick(object sender, EventArgs e)
    {
        Employee obj = new Employee();
        RoleManageLists objRoleManageList = new RoleManageLists();
        obj.EmpFirstName = txtFirstName.Text;
        obj.EmpLastName = txtlastName.Text;
        obj.EmpPassword = txtPassword.Text;
        obj.EmpNewPassword = txtPassword.Text;
        obj.EmpEmail = txtEmail.Text;
        obj.EmpLogInID = txtEmpID.Text;
        obj.EmpStateCode = "ON";
        obj.EmpCountryCode = "CAN";
        obj.Division = ddlDivision.SelectedItem.Value;
        obj.Region = "";
        obj.District = "";
        obj.Department = "";
        obj.JobCode = ddlJobCode.SelectedItem.Value;
        obj.Location = ddlStore.SelectedItem.Value;
        obj.Type = ddlType.SelectedItem.Value;
        obj.TypeParsed = ddlTypeParsed.SelectedItem.Value;

        if (this.UserID == 0)
        {
            if (obj.EmployeeIDExists(obj.EmpLogInID, this.UserID) == true)
            {
                if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry))
                {
                    showMessge(Resources.Resource.errMsgEmailIDAllreadyAssigned);
                }
                else
                {
                    showMessge(Resources.Resource.errMsgEmpIDAllreadyAssigned);
                }
                return;
            }
            obj.CreatedSource = UserCreatedSource.Pensivo;
            //// Get Pensivo UUID from PostGress Server
            if (Utils.TrainingInst == (int)Institute.bdl || Utils.TrainingInst == (int)Institute.beercollege)
            {
                obj.PensivoUUID = PensivoExecutions.PensivoPostUserData(Convert.ToString(txtEmpID.Text), Convert.ToString(txtPassword.Text), Convert.ToString(txtFirstName.Text), Convert.ToString(txtlastName.Text), Convert.ToString(ddlType.SelectedItem.Value), Convert.ToString(ddlDivision.SelectedItem.Value), BusinessUtility.GetString(ddlStore.SelectedValue), BusinessUtility.GetString(ddlJobCode.SelectedItem.Value));
                if (string.IsNullOrEmpty(obj.PensivoUUID))
                {
                    ErrorLog.createLog("Postgresql retrun null for user  " + txtEmpID.Text);
                    obj.SaveSysErrorLog(Convert.ToString(txtEmpID.Text));
                }
            }
            if (obj.InsertEmployee() == true)
            {
                objRoleManageList.UpdateEmployeeHasFunctionalityStatus(BusinessUtility.GetInt(obj.EmpID), "");

                //EmpAssignedCourses objAssignedCourse = new EmpAssignedCourses();
                //objAssignedCourse.ResetEmployeeAssignedCourses(BusinessUtility.GetInt(obj.EmpID));
                ExecutingThread.ThreadAssignedCoursesByEmployee(BusinessUtility.GetInt(obj.EmpID), 0, (int)AssignedCourseInActiveReason.ByUserProfileChange, (int)AssignedCourseUpdatedSource.ByUserProfileChange);

                Response.Redirect("AdministrationDashBoard.aspx");
            }
        }
        else
        {
            if (obj.EmployeeIDExists(obj.EmpLogInID, this.UserID) == true)
            {
                if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry))
                {
                    showMessge(Resources.Resource.errMsgEmailIDAllreadyAssignedtoother);
                }
                else
                {
                    showMessge(Resources.Resource.errMsgEmpIDAllreadyAssignedtoother);
                }
                return;
            }

            if (obj.UpdateEmployeeDetail(this.UserID) == true)
            {
                objRoleManageList.UpdateEmployeeHasFunctionalityStatus(BusinessUtility.GetInt(this.UserID), "");

                //EmpAssignedCourses objAssignedCourse = new EmpAssignedCourses();
                //objAssignedCourse.ResetEmployeeAssignedCourses(BusinessUtility.GetInt(this.UserID));

                ExecutingThread.ThreadAssignedCoursesByEmployee(BusinessUtility.GetInt(this.UserID), 0, (int)AssignedCourseInActiveReason.ByUserProfileChange, (int)AssignedCourseUpdatedSource.ByUserProfileChange);

                if (BusinessUtility.GetInt(hdnUUID.Value) > 0)
                {
                    if (Utils.TrainingInst == (int)Institute.bdl || Utils.TrainingInst == (int)Institute.beercollege)
                    {
                        PensivoExecutions.PensivoUpdateUserData(BusinessUtility.GetInt(hdnUUID.Value), Convert.ToString(txtEmpID.Text), Convert.ToString(txtPassword.Text), Convert.ToString(txtFirstName.Text), Convert.ToString(txtlastName.Text), Convert.ToString(ddlType.SelectedItem.Value), Convert.ToString(ddlDivision.SelectedItem.Value), Convert.ToString(ddlStore.SelectedItem.Value), BusinessUtility.GetString(ddlJobCode.SelectedItem.Value));
                    }
                }
                Response.Redirect("AdministrationDashBoard.aspx");
            }
        }
    }

    /// <summary>
    /// To Get User Details
    /// </summary>
    private void GetEmpDetail()
    {
        Employee objEmp = new Employee();
        ddlJobCode.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.JobCode);
        ddlJobCode.DataTextField = "sysRefCodeText";
        ddlJobCode.DataValueField = "sysRefCodeValue";
        ddlJobCode.DataBind();
        ddlJobCode.Items.Insert(0, new ListItem(Resources.Resource.lblJobCode, ""));

        ddlStore.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.Store);
        ddlStore.DataTextField = "sysRefCodeText";
        ddlStore.DataValueField = "sysRefCodeValue";
        ddlStore.DataBind();
        ddlStore.Items.Insert(0, new ListItem(Resources.Resource.lblStore, ""));

        ddlDivision.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.Division);
        ddlDivision.DataTextField = "sysRefCodeText";
        ddlDivision.DataValueField = "sysRefCodeValue";
        ddlDivision.DataBind();
        ddlDivision.Items.Insert(0, new ListItem(Resources.Resource.lblDivision, ""));

        ddlType.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.Type);
        ddlType.DataTextField = "sysRefCodeText";
        ddlType.DataValueField = "sysRefCodeValue";
        ddlType.DataBind();
        ddlType.Items.Insert(0, new ListItem(Resources.Resource.lblType, ""));

        ddlTypeParsed.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.TypeParsed);
        ddlTypeParsed.DataTextField = "sysRefCodeText";
        ddlTypeParsed.DataValueField = "sysRefCodeValue";
        ddlTypeParsed.DataBind();
        ddlTypeParsed.Items.Insert(0, new ListItem(Resources.Resource.lblTypeParsed, ""));

        ddlProvince.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.Province);
        ddlProvince.DataTextField = "sysRefCodeText";
        ddlProvince.DataValueField = "sysRefCodeValue";
        ddlProvince.DataBind();
        ddlProvince.Items.Insert(0, new ListItem(Resources.Resource.lblProvince, ""));


        objEmp = new Employee();
        objEmp.GetEmployeeDetail(this.UserID);
        txtFirstName.Text = BusinessUtility.GetString(objEmp.EmpFirstName);
        txtlastName.Text = BusinessUtility.GetString(objEmp.EmpLastName);
        txtPassword.Text = BusinessUtility.GetString(objEmp.EmpPassword);
        txtEmail.Text = BusinessUtility.GetString(objEmp.EmpEmail);
        txtEmpID.Text = BusinessUtility.GetString(objEmp.EmpExtID);
        ddlJobCode.SelectedValue = BusinessUtility.GetString(objEmp.JobCode);
        ddlStore.SelectedValue = BusinessUtility.GetString(objEmp.Location);
        ddlDivision.SelectedValue = BusinessUtility.GetString(objEmp.Division);
        ddlType.SelectedValue = BusinessUtility.GetString(objEmp.Type);
        ddlTypeParsed.SelectedValue = BusinessUtility.GetString(objEmp.TypeParsed);
        ddlProvince.SelectedValue = BusinessUtility.GetString(objEmp.Province);
        hdnUUID.Value = BusinessUtility.GetString(objEmp.PensivoUUID);
    }

    /// <summary>
    /// To Get User ID
    /// </summary>
    public int UserID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["userID"]);

        }
    }

    /// <summary>
    /// To Show Message
    /// </summary>
    /// <param name="message">Pass String Message</param>
    public void showMessge(string message)
    {
        if (message != "")
        {
            ltrErrMsg.Text = message;
            ltrErrMsg.Visible = true;
        }
        else
        {
            ltrErrMsg.Visible = false;
        }
    }


    [System.Web.Services.WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string ResetUserPasswordQuestion(int userID)
    {
        string rValue = string.Empty;
        try
        {
            Employee objemp = new Employee();
            objemp.GetEmployeeDetail(userID);
            if (objemp.ResetEmployeePasswordQuestion(userID, objemp.EmpExtID) == true)
            {
                rValue = "1";
            }
        }
        catch (Exception ex)
        {
            ErrorLog.CreateLog(ex);
        }
        return rValue;
    }
}