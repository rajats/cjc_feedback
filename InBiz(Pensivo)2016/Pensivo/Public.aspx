﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true" CodeFile="Public.aspx.cs" Inherits="Public" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="dvtbs" runat="server">
        <h1>Welcome to <%=Utils.TrainingInstTitle %> Training Solution</h1>
        <img class="left" id="img-7580114" src="_images/side-brick-building-tbs-branding.jpg" />
        <p>At <%=Utils.TrainingInstTitle %> we want to provide the necessary tools for you to conduct your role safely and effectively. E-learning is something relatively new to our business, but it is well established as one of the most powerful means to facilitate training within an organization the size of ours.</p>
        <p>Training is a necessary piece in providing the required knowledge for you to ensure a safe and efficient working environment for everyone in your location. The courses offered here have been designed to make this experience informative, interesting and more importantly... enjoyable. You will be exposed to critical information as well as activities and quick knowledge checks that are meant to be fun and educational.</p>
        <p>You are the pioneers of this new way of training at <%=Utils.TrainingInstTitle %>... congratulations!</p>
        <p id="ptbs" runat="server">If you already have a username and password, enter them in the login form to the left. Otherwise, use your Employee ID as both your username and password and you will be guided through the next steps.</p>
        <p id="pBdl" runat="server" visible="false">If you already have a username and password, enter them in the login form by clicking the "Log In" button on the top right of the page. Otherwise, use your Employee ID as both your username and password and you will be guided through the next steps.</p>
        <p>Some courses need special permission to be accessed. If you are interested in the following, please contact the person responsible:</p>
        <ul id="ulHtmlContal" runat="server">
            <li>For the Worker Health &amp; Safety Rep course, please contact Jeff Wilcox</li>
        </ul>
        <p id="pHtmlContal" runat="server">Please contact Suzanne Makins or Cayman Persichini on any concerns regarding your course.</p>
    </div>

    <div id="dvtdc" runat="server" visible="false">
        <h1 class="h2">Welcome to The Directors College Training Solution</h1>
        <img src="_images/DC-ACC_E-Module_Image.jpg" id="img-7580114" class="left">
        <p class="hero">Congratulations! You are about to take your first step towards completing one of the specialized programs offered by The Directors College. The Audit Committee Certified program (A.C.C) is Canada’s only university accredited program, making it truly one of a kind. Throughout the program, you will not only learn the audit and financial aspects of the audit committee’s responsibilities, you will also gain a deep understanding of the management components and relationship tactics involved in committee effectiveness.</p>
        <p class="hero">Sincere best wishes in carrying out your first module - we look forward to meeting you in the onsite Audit Committee Board Simulation!</p>
        <p class="hero">Find dates to register for your onsite Board Simulation Module <a href="http://thedirectorscollege.com/programs/acc">here</a>.</p>
    </div>

    <div id="dvNavacanada" runat="server" visible="false" style="margin-top:100px;">
        <h1><%=Resources.Resource.lblPublicPageContent1 %></h1>
        <%--        <img id="img-7580114" class="left" src="_themes/nav-canada/_images/nav-canada-1148300715.jpg">
        <p><%=Resources.Resource.lblPublicPageContent2 %></p>
        <p><%=Resources.Resource.lblPublicPageContent3 %></p>
        <p><%=Resources.Resource.lblPublicPageContent4 %></p>--%>
    </div>
    <div id="dvAlMurrayDentistry" runat="server" visible="false">
        <h1><%=Resources.Resource.lblAlMurrayDentistryPublicPageContent1 %></h1>
        <img id="img-7580114" class="left" src="_themes/nav-canada/_images/nav-canada-1148300715.jpg">
        <p><%=Resources.Resource.lblAlMurrayDentistryPublicPageContent2 %></p>
        <p><%=Resources.Resource.lblAlMurrayDentistryPublicPageContent3 %></p>
        <p><%=Resources.Resource.lblAlMurrayDentistryPublicPageContent4 %></p>
    </div>
</asp:Content>
