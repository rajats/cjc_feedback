﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmpOtherSearchDashBoardGrid.aspx.cs" Inherits="EmpOtherSearchDashBoardGrid" %>
<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada )  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.EDE2 )  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>

     <%--Define Inline Css to Wrap Grid Header and Row Data--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th div {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
        }

        .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
            vertical-align: text-top;
        }
    </style>
    <section id="main-content" class="pg-advanced-list">
        <div class="wrapper">
            <h1>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h1>
            <aside class="column span-4 fixed-onscroll">
                <div class="search-box" style="width: 384px; left: 216px;">
                    <header class="search-box-header">
                        <h2 class="h6">
                            <asp:Literal ID="ltrSearchTitle" runat="server"></asp:Literal></h2>
                        <p class="last-child">
                            <asp:Literal ID="ltrSearchMessage" runat="server"></asp:Literal>
                        </p>
                    </header>
                    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
                        <div class="search-box-body">
                            <div class="plms-input-group">
                                <div class="plms-fieldset">
                                    <asp:Label ID="lblRoleName" AssociatedControlID="txtRoleName" class="filter-key plms-label is-hidden no-height" runat="server"
                                        Text="<%$ Resources:Resource, lblRoleName %>" for="name"></asp:Label>
                                    <asp:TextBox ID="txtRoleName" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblRoleName %>">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <input id="btnSearch" class="btn fluid clearfix" type="button" style="width: 100%;" value="<%=Resources.Resource.lbSearch%>" />
                        </div>
                    </asp:Panel>
                </div>
            </aside>
            <div class="main-content-body column span-8" onkeypress="return disableEnterKey(event)">
                <div id="grid_wrapper" style="width: 100%;">
                    <trirand:JQGrid runat="server" ID="gvRoles" Height="300px" MultiSelect="true" multiboxonly="true"
                        AutoWidth="True" OnDataRequesting="gvRoles_DataRequesting">
                        <Columns>
                            <trirand:JQGridColumn DataField="sysRefCodeValue" Visible="false" PrimaryKey="True" />
                            <trirand:JQGridColumn DataField="sysRefCodeText" HeaderText="<%$ Resources:Resource, lblName %>"
                                Editable="false" Width="200" />
                        </Columns>
                        <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn=""></SortSettings>
                        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                        <ClientSideEvents LoadComplete="loadComplete" RowSelect="updateIdsOfSelectedRows" BeforeRowSelect="beforeSelectRow" />
                    </trirand:JQGrid>
                </div>

                <footer class="form-footer">
                    <div class="btngrp align-r">
                        <a id="hrfNext" runat="server" href="#nogo" class="btn round" onclick="getSelectedRole();">Next</a>
                    </div>
                </footer>


                <asp:HiddenField ID="HdnRoleID" runat="server" />
                <asp:HiddenField ID="HdnSearchBy" runat="server" />
                <asp:HiddenField ID="HdnFlag" runat="server" />
                <asp:HiddenField ID="HdnReportOption" runat="server" />
                <asp:HiddenField ID="HdnActionID" runat="server" />
                <asp:HiddenField ID="HdnSysRoleOperator" runat="server" />
                <asp:HiddenField ID="HdnIsAllowMultipleSelection" Value="1" runat="server" />

                <asp:HiddenField ID="HdnMessage" runat="server" />

            </div>
        </div>
    </section>
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvRoles.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        // To Hold Selected Grid Value on Grid Page Changes
        $grid = $("#<%=gvRoles.ClientID%>"),
                idsOfSelectedRows = [],
                updateIdsOfSelectedRows = function (id, isSelected) {
                    var index = $.inArray(id, idsOfSelectedRows);
                    if (!isSelected && index >= 0) {
                        idsOfSelectedRows.splice(index, 1);
                    } else if (index < 0) {
                        idsOfSelectedRows.push(id);
                    }
                };

        // To Hold Selected Grid Value on Grid Page Changes
        $grid = $("#<%=gvRoles.ClientID%>"),
                idsOfSelectedRows = [],
                updateIdsOfSelectedRows = function (id, isSelected) {
                    if ($("#<%=HdnIsAllowMultipleSelection.ClientID%>").val() == "0") {
                        idsOfSelectedRows = [];
                    }

                    var index = $.inArray(id, idsOfSelectedRows);
                    if (!isSelected && index >= 0) {
                        idsOfSelectedRows.splice(index, 1);
                    } else if (index < 0) {
                        idsOfSelectedRows.push(id);
                    }
                };


                // Resized JQ Grid
                function jqGridResize() {
                    $("#<%=gvRoles.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
                }

                // Define JQ Grid Load Complete and Load Saved JQ Grid Selected value on Page Changes
                function loadComplete(data) {
                    $("#cb_ContentPlaceHolder1_gvRoles").hide();
                    jqGridResize();
                    for (i = 0, count = idsOfSelectedRows.length; i < count; i++) {
                        $("#<%=gvRoles.ClientID%>").jqGrid('setSelection', idsOfSelectedRows[i], false);
                    }
                }


                // Allow JQ Grid To Single Row Selection
                function beforeSelectRow(id) {
                    if ($("#<%=HdnIsAllowMultipleSelection.ClientID%>").val() == "0") {
                        $('#<%=gvRoles.ClientID%>').jqGrid('resetSelection');
                        return (true);
                    }
                    else {
                        return (true);
                    }
                }

                // To Get Selected Grid Row Value and Move to Next Page
                function getSelectedRole() {
                    var arrRooms = $grid.getGridParam('selarrrow');
                    if (idsOfSelectedRows.length <= 0) {
                        ShowPensivoMessage("<%=Resources.Resource.reqPleaseSelectAvalue%>");
                        return false;
                    }
                    else {
                        if ($("#<%=HdnReportOption.ClientID%>").val() != "") {
                            window.location.href = "ReportFilterSummary.aspx?searchval=" + idsOfSelectedRows.join(",") + "&roption=" + $("#<%=HdnReportOption.ClientID%>").val() + "&searchby=" + $("#<%=HdnSearchBy.ClientID%>").val() + "&flag=" + $("#<%=HdnFlag.ClientID%>").val() + "";
                        }
                        else {
                            if ($("#<%=HdnActionID.ClientID%>").val() > 0) {
                                //window.location.href = "RoleSysReportUserRefAssociationConfirmation.aspx?searchval=" + idsOfSelectedRows.join(",") + "&roleID=" + $("#<%=HdnRoleID.ClientID%>").val() + "&searchby=" + $("#<%=HdnSearchBy.ClientID%>").val() + "&flag=" + $("#<%=HdnFlag.ClientID%>").val() + "&actionID=" + $("#<%=HdnActionID.ClientID%>").val();
                                var title = "<%=Resources.Resource.lblConfirmation%>";
                                var messageText = ($("#<%=HdnMessage.ClientID%>").val()).replace("#SEARCHVALUE#", idsOfSelectedRows.join("</br>"));
                                okButtonText = "OK";
                                LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
                                okButtonRedirectlink = "TriggerRoleSysReportUserRefAssociationOkButton('" + idsOfSelectedRows.join(",") + "', '" + $("#<%=HdnRoleID.ClientID%>").val() + "');";
                                LaterCnclButtonRedirectLink = " HideConfirmationDialog();";
                                PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
                            }
                            else {
                                var title = "<%=Resources.Resource.lblConfirmation%>";
                                var messageText = ($("#<%=HdnMessage.ClientID%>").val()).replace("#SEARCHVALUE#", idsOfSelectedRows.join("</br>"));
                                okButtonText = "OK";
                                LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
                                okButtonRedirectlink = "TriggerRoleSysRefAssociationOkButton('" + idsOfSelectedRows.join(",") + "', '" + $("#<%=HdnRoleID.ClientID%>").val() + "');";
                                LaterCnclButtonRedirectLink = " HideConfirmationDialog();";
                                PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
                            }
                        }
                    }
                }

                // To Trigger Role SysReport User Ref AssociationOkButton
        function TriggerRoleSysReportUserRefAssociationOkButton(searchValue, roleID) {
            ShowPensivoWaitingMessage("<%=Resources.Resource.lblRoleModifyPleaseWait%>");
                    var datatoPost = {};
                    datatoPost.isAjaxCall = 1;

                    if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                datatoPost.callBack = "addActionSysRefInReportUserManagment";
            }
            else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                datatoPost.callBack = "removeActionSysRefInReportUserManagment";
            }
            datatoPost.SearchBy = $("#<%=HdnSearchBy.ClientID%>").val();
            datatoPost.SearchValue = searchValue;
            datatoPost.RoleID = roleID;
            datatoPost.ActionID = $("#<%=HdnActionID.ClientID%>").val();
            $.post("EmpOtherSearchDashBoardGrid.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                        //ShowPensivoMessage("<%=Resources.Resource.lblFunctionnalityAddedtoRole%>", "roleActivity.aspx?roleID=" + roleID + "");
                        window.location.href = "roleActivity.aspx?roleID=" + roleID;
                    }
                    else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                        //ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueRemovedToRole%>", "roleActivity.aspx?roleID=" + roleID + "");
                        window.location.href = "roleActivity.aspx?roleID=" + roleID;
                    }
                    ClosePensivoWaitingMessage();
            }
            else {
                if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                        ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueNotAddedToRole%>")
                    }
                    else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                        ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueNoRemovedToRole%>")
                    }
                    ClosePensivoWaitingMessage();
            }
            });
    }

    // To Trigger Role Sys Ref Association Ok Button
        function TriggerRoleSysRefAssociationOkButton(searchValue, roleID) {
            ShowPensivoWaitingMessage("<%=Resources.Resource.lblRoleModifyPleaseWait%>");
        var datatoPost = {};
        datatoPost.isAjaxCall = 1;
        if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
            datatoPost.callBack = "addActionRoleSysRefAssociation";
        }
        else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
            datatoPost.callBack = "removeActionRoleSysRefAssociation";
        }
        datatoPost.SearchBy = $("#<%=HdnSearchBy.ClientID%>").val();
        datatoPost.SearchValue = searchValue;
        datatoPost.RoleID = roleID;
        datatoPost.SysRoleOperator = $("#<%=HdnSysRoleOperator.ClientID%>").val();

        $.post("EmpOtherSearchDashBoardGrid.aspx", datatoPost, function (data) {
            if (data == "ok") {
                if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                    window.location.href = "EmpSearchDashBoard.aspx?roleID=" + roleID + "&flag=" + $("#<%=HdnFlag.ClientID%>").val();
                }
                else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                    window.location.href = "EmpSearchDashBoard.aspx?roleID=" + roleID + "&flag=" + $("#<%=HdnFlag.ClientID%>").val();
                }
                ClosePensivoWaitingMessage();
        }
        else {
            if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                    ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueNotAddedToRole%>")
                }
                else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                    ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueNoRemovedToRole%>")
                }
                ClosePensivoWaitingMessage();
        }
        });



    }


        $(document).ready(function () {
            $("#<%=txtRoleName.ClientID%>").focus();
                });

    </script>
</asp:Content>
