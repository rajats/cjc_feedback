﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;


public partial class RoleRemoveFunctionality : BasePage
{
    /// <summary>
    /// To Hold HTML String
    /// </summary>
    protected string htmlText = "";

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Role objRole = new Role();
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblModifiyExistingFunctionalityToTheSystemRole, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            htmlText = loadEmpSearchValue();
        }

        
        string roleName = "";
        if (this.RoleID > 0)
        {
            objRole = new Role();
            roleName = objRole.GetRoleName(this.RoleID);
        }
        hdnMsgFunctionalityConfirmation.Value = Resources.Resource.lblRoleFunctionalityRemove.Replace("#ROLENAME#", roleName);

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeAction")
        {
            try
            {
                int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                string actionType = BusinessUtility.GetString(Request.Form["ActionType"]);
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                if (actionID > 0 && roleID > 0)
                {
                     objRole = new Role();
                    if (objRole.RoleRemoveFunctionality(roleID, actionID, actionType) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
    }

    /// <summary>
    /// To Get Functionality HTML Content List
    /// </summary>
    /// <returns>String</returns>
    private string loadEmpSearchValue()
    {
        string sbHtml = "";
        Role objRole = new Role();
        DataTable dt = objRole.GetFunctionalityInRole(this.RoleID, Globals.CurrentAppLanguageCode);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sbHtml += " <div class='boxset' id='dvAdminDashBoard"+ BusinessUtility.GetString(i) +"'> ";
            for (int j = 0; j <= 2; j++)
            {
                if (i < dt.Rows.Count)
                {
                    string sText = BusinessUtility.GetString(dt.Rows[i]["ItemName"]);
                    string sValue = BusinessUtility.GetString(dt.Rows[i]["ActionItem"]);
                    if (BusinessUtility.GetInt(sValue) == (int)RoleAction.AI_2015)
                    {
                        sText = Resources.Resource.lblAI2015;
                    }

                    if (sText != "")
                    {
                        string sFunction = "";
                        string sRoleType = BusinessUtility.GetString(dt.Rows[i]["RoleType"]);
                        int isAllowAllAccessData=0;

                        if (sRoleType == RoleType.Menu)
                        {
                            objRole = new Role();                            
                            if(objRole.IsAllowAllAccessData(this.RoleID, BusinessUtility.GetInt(sValue), sRoleType) ==true)
                            {
                                isAllowAllAccessData = 1;
                            }

                            if (objRole.IsAllowAllAccessData(this.RoleID, BusinessUtility.GetInt(sValue), sRoleType) == true || objRole.IsAllowAllSpecificAccessData(this.RoleID, BusinessUtility.GetInt(sValue), sRoleType) == true)
                            {
                                sFunction = string.Format("onclick='showsubmenu({0},{1},\"" + sRoleType + "\",\"" + this.Flag + "\",\"" + this.Flag + "\");'", this.RoleID, sValue, sRoleType, BusinessUtility.GetString(isAllowAllAccessData));
                            }
                            if (objRole.ActionExistsInRole(this.RoleID, BusinessUtility.GetInt(sValue), sRoleType) == true)
                            {
                                sFunction = string.Format("onclick='showsubmenu({0},{1},\"" + sRoleType + "\",\"" + this.Flag + "\",\"" + this.Flag + "\");'", this.RoleID, sValue, sRoleType, BusinessUtility.GetString(isAllowAllAccessData));
                            }
                            else
                            {
                                sFunction = string.Format("onclick='removeInRole({0},{1},\"" + sRoleType + "\",\"" + sText + "\");'", this.RoleID, sValue, sRoleType);
                            }
                        }
                        else if (sRoleType == RoleType.System)
                        {
                            sFunction = string.Format("onclick='removeInRole({0},{1},\"" + sRoleType + "\",\"" + sText + "\");'", this.RoleID, sValue, sRoleType);
                        }

                        sbHtml += " <div class='boxset-box'> ";
                        sbHtml += " <a id='hrfSearchEmpName' runat='server' href='#' " + sFunction + " class='boxset-box-wrapper'> ";
                        sbHtml += " <div class='boxset-box-inner'> ";
                        sbHtml += " <h4 class='boxset-title'> " + sText + " </h4> ";
                        sbHtml += " </div> ";
                        sbHtml += " </a> ";
                        sbHtml += " </div> ";
                        if (j <= 1)
                        {
                            i += 1;
                        }
                    }
                }
            }
            sbHtml += " </div> ";
        }
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

}