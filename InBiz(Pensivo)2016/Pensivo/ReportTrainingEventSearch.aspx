﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ReportTrainingEventSearch.aspx.cs" Inherits="ReportTrainingEventSearch" %>
<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
   <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.EDE2)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>

     <%--Define Inline Css to Wrap Grid Header and Row Data--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th div {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
        }

        .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
            vertical-align: text-top;
        }
    </style>
    <section id="main-content" class="pg-advanced-list">
        <div class="wrapper">
            <h1><%=Resources.Resource.lblEvents %></h1>
            <aside class="column span-4 fixed-onscroll" id="dvSearch" runat="server">
                <div class="search-box" style="width: 384px; left: 216px;">
                    <header class="search-box-header">
                        <h2 class="h6"><%=Resources.Resource.lblFindTrainingEvents %></h2>
                        <p class="last-child"><%=Resources.Resource.lblFindEventMessage %></p>
                    </header>
                    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
                        <div class="search-box-body">
                            <div class="plms-input-group">
                                <div class="plms-fieldset">
                                    <asp:Label ID="Label2" AssociatedControlID="txtRoleName" class="filter-key plms-label is-hidden no-height" runat="server"
                                        Text="<%$ Resources:Resource, lblFindEvent %>" for="name"></asp:Label>
                                    <asp:TextBox ID="txtRoleName" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblFindEvent %>">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <input id="btnSearch" class="btn fluid clearfix" type="button" style="width: 100%;" value="<%=Resources.Resource.lbSearch%>" />
                        </div>
                    </asp:Panel>
                </div>
            </aside>
            <div class="main-content-body column span-8" onkeypress="return disableEnterKey(event)">
                <div id="grid_wrapper" style="width: 100%;">
                    <trirand:JQGrid runat="server" ID="gvRoles" Height="300px" MultiSelect="true" multiboxonly="true"
                        AutoWidth="True" OnDataRequesting="gvRoles_DataRequesting">
                        <Columns>
                            <trirand:JQGridColumn DataField="eventID" Visible="false" PrimaryKey="True" />
                            <trirand:JQGridColumn DataField="eventID" HeaderText="<%$ Resources:Resource, lblTrainingEventID %>"
                                Editable="false" Width="85" />
                            <trirand:JQGridColumn DataField="EventTitle" HeaderText="<%$ Resources:Resource, lblTrainingEventName %>"
                                Editable="false" Width="300" />
                            <trirand:JQGridColumn DataField="EventType" HeaderText="<%$ Resources:Resource, lblTrainingEventType %>"
                                Editable="false" Width="60" />
                            <trirand:JQGridColumn DataField="Duration" HeaderText="<%$ Resources:Resource, lblTrainingEventDuration %>"
                                Editable="false" Width="50" />
                        </Columns>
                        <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn=""></SortSettings>
                        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                        <ClientSideEvents LoadComplete="loadComplete" RowSelect="updateIdsOfSelectedRows" />
                    </trirand:JQGrid>
                </div>
                <footer class="form-footer">
                    <div class="btngrp align-r">
                        <a href="#nogo" class="btn round" onclick="getSelectedRole();"><%=Resources.Resource.lblNext %></a>
                    </div>
                </footer>
                <asp:HiddenField ID="hdnROption" runat="server" />
                <asp:HiddenField ID="hdnSearchBy" runat="server" />
                <asp:HiddenField ID="hdnIsAllEvent" runat="server" />
            </div>
        </div>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvRoles.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        // To Hold Grid Selected Value on Page Changes
        $grid = $("#<%=gvRoles.ClientID%>"),
                idsOfSelectedRows = [],
                updateIdsOfSelectedRows = function (id, isSelected) {
                    var index = $.inArray(id, idsOfSelectedRows);
                    if (!isSelected && index >= 0) {
                        idsOfSelectedRows.splice(index, 1);
                    } else if (index < 0) {
                        idsOfSelectedRows.push(id);
                    }
                };


        // Resize JQGrid
        function jqGridResize() {
            $("#<%=gvRoles.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // Define JQGrid Load Complete Method
        function loadComplete(data) {
            $("#cb_ContentPlaceHolder1_gvRoles").hide();
            jqGridResize();
            for (i = 0, count = idsOfSelectedRows.length; i < count; i++) {
                $("#<%=gvRoles.ClientID%>").jqGrid('setSelection', idsOfSelectedRows[i], false);
            }
        }

        // Get JQGrid Selected Value and Move to Next Page
        function getSelectedRole() {
            if (idsOfSelectedRows.length <= 0) {
                ShowPensivoMessage("<%=Resources.Resource.errMsgSelectEvent%>");
                return false;
            }
            else {
                window.location.href = "ReportFilterSummary.aspx?roption=" + $('#<%=hdnROption.ClientID%>').val() + "&isAllEvent=" + $('#<%=hdnIsAllEvent.ClientID%>').val() + "&searchby=" + $('#<%=hdnSearchBy.ClientID%>').val() + "&searchval=" + idsOfSelectedRows.join(",");
            }
        }

        $(document).ready(function () {
            $("#<%=txtRoleName.ClientID%>").focus();
               });
    </script>
</asp:Content>
