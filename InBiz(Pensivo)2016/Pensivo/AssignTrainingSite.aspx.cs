﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;


public partial class AssignTrainingSite : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Role objRole = new Role();

        AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Assign_Training);

        //if (!IsPostBack && !IsPagePostBack(gvRoles))
        {
            HdnSearchBy.Value = BusinessUtility.GetString(this.SearchBy);
            HdnTrainingEventID.Value = BusinessUtility.GetString(this.TrainingEventIDs);

            hrfNext.InnerText = Resources.Resource.lblNext;


            if (Utils.TrainingInst == (int)Institute.bdl)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblDeptID, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                gvRoles.Columns[1].HeaderText = Resources.Resource.lblDeptID;
                ltrTitle.Text = Resources.Resource.lblDeptID;
                ltrSearchTitle.Text = Resources.Resource.lblFindDeptID;
                ltrSearchMessage.Text = Resources.Resource.lblFindAssignTrainingDeptIDMessage;
                lblRoleName.Text = Resources.Resource.lblDeptID;
                txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblDeptID);
            }
            else
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchSite, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                gvRoles.Columns[1].HeaderText = Resources.Resource.lblSearchSite;
                ltrTitle.Text = Resources.Resource.lblSearchSite;
                ltrSearchTitle.Text = Resources.Resource.lblFindAssignSite;
                ltrSearchMessage.Text = Resources.Resource.lblFindAssignTrainingSiteMessage;
                lblRoleName.Text = Resources.Resource.lblSearchAssignSite;
                txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblSearchAssignSite);
            }

            HdnIsAllowMultipleSelection.Value = "0";
        }
    }

    /// <summary>
    /// To Bind JQ Grid With Data Source Differtent System Refference value Associated with role or not 
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtRole = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        Role objRole = new Role();

        DataTable dt = objRole.GetSysRef(Utils.ReplaceDBSpecialCharacter(this.SearchBy), Utils.ReplaceDBSpecialCharacter(txtRole), Globals.CurrentAppLanguageCode);
        gvRoles.DataSource = dt;
        gvRoles.DataBind();

    }



    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }


    /// <summary>
    /// To Get Training Event ID(s) Spreated by ","
    /// </summary>
    public string TrainingEventIDs
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["trainingeventid"]);
        }
    }

}