﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Library.Utilities;
using System.IO;
using iTECH.Pensivo.BusinessLogic;

public partial class roleActivity : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblRoleEditDashBoard, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            hrfModifyUserAsscociationWithSystem.HRef = "RoleUserDashBoard.aspx?roleID=" + this.RoleID;
            hrfModifyTraningEventUserAccess.HRef = "TraningEventRoleDashBoard.aspx?roleID=" + this.RoleID;
            hrfModifyFunctionalitySystemRoleCanAccess.HRef = "FunctionalityRoleDashBoard.aspx?roleID=" + this.RoleID;
        }
        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry))//|| (Utils.TrainingInst == (int)Institute.navcanada)
        {
            dvRoleActivity.Visible = false;
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }
}