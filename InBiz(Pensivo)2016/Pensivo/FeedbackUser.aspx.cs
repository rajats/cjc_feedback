﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class FeedbackUser : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvEmployee))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblFeedbackUser, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery));
        }
    }

    /// <summary>
    /// To Bind JQ Grid with Employee List Data Source
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvEmployee_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Employee objEmp = new Employee();
        string txtName = Utils.ReplaceDBSpecialCharacter(BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]));
        objEmp.EmpName = txtName;
        objEmp.ExcludeReportUser = true;
        gvEmployee.DataSource = objEmp.GetEmployeeList();
        gvEmployee.DataBind();
    }



    protected void btnFeedbackUser_Click(object sender, EventArgs e)
    {
        string sFeedbackUser = BusinessUtility.GetString(hdnFeedbackUser.Value);

        Feedback objFeedback = (Feedback)Session["FeedbackDetails"];
        objFeedback.EmpIDList = BusinessUtility.GetString(sFeedbackUser);
        Session["FeedbackDetails"] = objFeedback;


        Response.Redirect("FeedbackMessagePreview.aspx");
    }
}