﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class TrainingEventView : BasePage
{
    /// <summary>
    /// Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvEvent))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblModifyExistingTrainingEvent, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            ltrTitle.Text = Resources.Resource.lblModifyTraining;
        }
    }

    /// <summary>
    /// To Define JQ Grid  CellBinding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event Args</param>
    protected void gvEvent_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            e.CellHtml = string.Format(@"<a href=""javascript:;""  onclick=""EditEvent({0})"">" + Resources.Resource.lblEdit + "</a>", e.CellHtml);
        }
    }

    /// <summary>
    /// To Bind JQ Grid With Training Event List
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvEvent_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtRole = Utils.ReplaceDBSpecialCharacter( BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]));
        Event objRole = new Event();
        objRole.EventName = txtRole;
        gvEvent.DataSource = objRole.GetEventList(Globals.CurrentAppLanguageCode);
        gvEvent.DataBind();
    }
}