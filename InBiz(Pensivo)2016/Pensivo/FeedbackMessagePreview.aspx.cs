﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;


using System.Security.Cryptography;
using System.Globalization;

public partial class FeedbackMessagePreview : BasePage
{
    Employee objEmp = new Employee();
    protected string feedbackMessage = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblFeedbackMessage, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery));


            string sUserName = "";
            string sMessage = "";


            Feedback objFeedback = (Feedback)Session["FeedbackDetails"];
            DataTable dt = objEmp.GetEmployeeListEmpIDIn(objFeedback.EmpIDList);

            foreach (DataRow dRow in dt.Rows)
            {
                if (sUserName == "")
                {
                    
                       sUserName = BusinessUtility.GetString(dRow["EmpFirstName"]) + " "  + BusinessUtility.GetString(dRow["EmpLastName"]) + " " + "(" + BusinessUtility.GetString(dRow["EmpExtID"]) + "<mailto:"+ BusinessUtility.GetString(dRow["EmpExtID"])+">" + ")";
                }
                else
                {
                    sUserName += "<br/>" + BusinessUtility.GetString(dRow["EmpFirstName"]) + " " + BusinessUtility.GetString(dRow["EmpLastName"]) + " " + "(" + BusinessUtility.GetString(dRow["EmpExtID"]) + "<mailto:" + BusinessUtility.GetString(dRow["EmpExtID"]) + ">" + ")";
                }
            }
            sMessage = Resources.Resource.lblMsgFeedbackConfirmationMultipleUsers.Replace("#QUESIONNAME#", BusinessUtility.GetString(objFeedback.EventName))
            .Replace("#USERNAME#", sUserName);
            HdnMessage.Value = sMessage;

            HdnMessageSent.Value = Resources.Resource.lblMsgFeedbackSent.Replace("#USERFIRSTNAME#", BusinessUtility.GetString(CurrentEmployee.EmpFirstName));

            string[] sUserID = objFeedback.EmpIDList.Split(',');

            string sToUserName = "";

            if (sUserID.Length > 0)
            {
                objEmp = new Employee();
                objEmp.GetEmployeeDetail(BusinessUtility.GetInt(sUserID[0]));
                sToUserName = objEmp.EmpFirstName;
            }

            string[] strDate = objFeedback.EventDate.Split('-');
            string strFormattedEventdate = string.Empty;
            DateTime dtEvnt;

            try
            {
                dtEvnt = new DateTime(Convert.ToInt32(strDate[2]), Convert.ToInt32(strDate[0]), Convert.ToInt32(strDate[1]));
                strFormattedEventdate = dtEvnt.ToString("MMMM dd, yyyy");
                HdnFormattedEventDate.Value = strFormattedEventdate;
            }
            catch(Exception error)
            {
                ErrorLog.createLog("Error While Formatting Feedback Event Date : "+ objFeedback.EventDate.ToString() + " to MMMM dd, yyyy for Feedback Event Name"+ objFeedback.EventName);
                ErrorLog.createLog(error.Message);
                HdnFormattedEventDate.Value = objFeedback.EventDate.ToString();
            }

            feedbackMessage = Utils.FeedbackMessage.Replace("#EVENTNAME#", objFeedback.EventName).Replace("#EVENTDATE#", HdnFormattedEventDate.Value).Replace("#FROMUSERNAME#", CurrentEmployee.EmpName).Replace("#TOUSER#", sToUserName).Replace("<a href='#LINK#'>#LINK1#</a>", "[Questionnaire link will be inserted here]").Replace("#QUESTIONNAIRENAME#", objFeedback.TrainingEventName);

        }


    }

    protected void btnSendMessage_Click(object sender, EventArgs e)
    {
        
        Feedback objFeedback = (Feedback)Session["FeedbackDetails"];
        string[] sUserID = objFeedback.EmpIDList.Split(',');
        

        int iRoleID =  objFeedback.InsertFeedbackDetails(objFeedback.EventName, objFeedback.EventDate, BusinessUtility.GetInt( objFeedback.TrainingEventID) , objFeedback.EmpIDList, BusinessUtility.GetInt( CurrentEmployee.EmpID));
        if (iRoleID > 0)
        {
            foreach (string strUserID in sUserID)
            {
                //Response.Redirect("decryptionPage.aspx?id1=" + HttpUtility.UrlEncode(Encrypt(id1)));
                objEmp = new Employee();
                objEmp.GetEmployeeDetail(BusinessUtility.GetInt(strUserID));
                string sToUserName = objEmp.EmpFirstName;
                string sToEmailID = objEmp.EmpExtID;
                string sLink = BusinessUtility.GetString(Utils.GetSiteName()) + "" + "EmployeeCourses.aspx" + "?CourseStatus=3&roleid=" + iRoleID + "&empid=" + strUserID + "&isFeedback=1";
                string sLink1 = HttpUtility.UrlEncode(EncryptionUtility.Encrypt(BusinessUtility.GetString(Utils.GetSiteName()) + "" + "EmployeeCourses.aspx" + "?CourseStatus=3&roleid=" + iRoleID + "&empid=" + strUserID + "&isFeedback=1" + ""));
                string sMessage = Utils.FeedbackMessage.Replace("#EVENTNAME#", objFeedback.EventName).Replace("#EVENTDATE#", HdnFormattedEventDate.Value).Replace("#FROMUSERNAME#", CurrentEmployee.EmpName).Replace("[First Name of each Respondent will be inserted here]", sToUserName).Replace("#LINK#", sLink).Replace("#LINK1#", sLink1).Replace("#QUESTIONNAIRENAME#", objFeedback.TrainingEventName);
                string EmailFrom = Utils.SupportEmailFrom;
                string EmailSubject = Resources.Resource.lblFeedbackMailSubject;

                EmailHelper.SendEmail(EmailFrom, sToEmailID, BusinessUtility.GetString(sMessage), EmailSubject, true);
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "AttemptRequestDialog", "MessageSendConfirmation();", true);
        }
        else
        {
            showAlertMessage();
        }
    }

    /// <summary>
    /// To Show Alert Message
    /// </summary>
    private void showAlertMessage()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowPensivoMessage('"+Resources.Resource.lblFeedbackErrorInsrtFdbkDtls + "','"+ "FeedbackHome.aspx" + "');", true);
    }
}