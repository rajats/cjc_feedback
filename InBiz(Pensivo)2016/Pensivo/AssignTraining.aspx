﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AssignTraining.aspx.cs" Inherits="AssignTraining" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div id="dvAccessAllData" class="boxset-box" runat="server">
                    <a id="hrfAccessAllData" runat="server" href="AssignTrainingEvent.aspx" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblAssignAUserToATrainingEvent%></h4>
                        </div>
                    </a>
                </div>
                <div id="dvAccessAllspecificData" class="boxset-box" runat="server">
                    <a id="hrfAccessSpecificData" runat="server" href="#" class="boxset-box-wrapper is-disabled"><%--is-disabled AccessDataDashBoard.aspx--%>
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblRemoveAUserFromATrainingEvent%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>



