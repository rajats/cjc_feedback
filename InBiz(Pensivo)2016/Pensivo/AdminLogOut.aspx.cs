﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTECH.Library.Utilities;
using iTECH.Pensivo.BusinessLogic;

public partial class AdminLogOut : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            Breadcrumbs.BreadcrumbsSetRootMenu();
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAdminLogout, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
        }
    }

    /// <summary>
    /// To  Return Super Adming Account
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnReturnToSuperAdminAccount_Click(object sender, EventArgs e)
    {
        Session["Logout"] = null;
        if (CurrentEmployee.IsSuperAdminLoginAsAnotherUser)
        {
            Employee objEmp = new Employee();
            int iUserID = BusinessUtility.GetInt(CurrentEmployee.SuperAdminEmpID);
            CurrentEmployee.RemoveEmployeeFromSession();
            CurrentEmployee.RemoveSuperAdminFromSession();

            objEmp.GetEmployeeDetail(iUserID);
            CurrentEmployee.SetCurrentEmployeeSession(BusinessUtility.GetInt(objEmp.EmpID), objEmp.EmpLogInID, objEmp.EmpExtID, objEmp.EmpName, objEmp.EmpEmail, objEmp.EmpType);

            Role objRole = new Role();
            if ((objRole.GetUserHasFunctionality(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)RoleAction.Administration, "") == true) || (CurrentEmployee.EmpType == EmpType.SuperAdmin))
            {
                Response.Redirect("AdministrationDashBoard.aspx");
            }
            else
            {
                Response.Redirect("employeedashboard.aspx");
            }
        }
    }

    /// <summary>
    /// To Login with Another Account
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnLoginAsAnotherAccount_Click(object sender, EventArgs e)
    {
        Session["Logout"] = null;

        Employee objEmp = new Employee();
        int iUserID = BusinessUtility.GetInt(CurrentEmployee.SuperAdminEmpID);
        CurrentEmployee.RemoveEmployeeFromSession();
        CurrentEmployee.RemoveSuperAdminFromSession();

        objEmp.GetEmployeeDetail(iUserID);
        CurrentEmployee.SetCurrentEmployeeSession(BusinessUtility.GetInt(objEmp.EmpID), objEmp.EmpLogInID, objEmp.EmpExtID, objEmp.EmpName, objEmp.EmpEmail, objEmp.EmpType);


        Response.Redirect("Alias.aspx");
    }

    /// <summary>
    /// To Logout of the system
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnLogOutofTheSystem_Click(object sender, EventArgs e)
    {
        Session["Logout"] = null;
        EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.SignedOutofPIB,
            BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), "", 0, (int)RusticRegStatusCode.None, "");

        CurrentEmployee.RemoveEmployeeFromSession();
        CurrentEmployee.RemoveSuperAdminFromSession();
        Response.Redirect("~/Public.aspx");
    }
}