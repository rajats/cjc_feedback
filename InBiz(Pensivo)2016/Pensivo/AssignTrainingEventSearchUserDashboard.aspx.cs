﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;

public partial class AssignTrainingEventSearchUserDashboard : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Assign_Training);

            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAssignTrainingUserSerachBy, Path.GetFileName(Request.Url.AbsolutePath), Path.GetFileName(Request.Url.AbsolutePath));


            hrfUserToAccess.HRef = "AssignTrainingEventByEmpCode.aspx?trainingeventid="+ this.TrainingEventIDs;
            hrfSiteToAccess.HRef = "AssignTrainingSite.aspx?trainingeventid=" + this.TrainingEventIDs + "&searchby=" + EmpSearchBy.Store ;
            if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) || (Utils.TrainingInst == (int)Institute.EDE2))
            {
                dvSiteToAccess.Visible = false;
            }
        }
    }

    /// <summary>
    /// To Get Training Event ID(s) Spreated by ","
    /// </summary>
    public string TrainingEventIDs
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["trainingeventid"]);
        }
    }
}