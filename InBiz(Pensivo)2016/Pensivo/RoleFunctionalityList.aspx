﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleFunctionalityList.aspx.cs" Inherits="RoleFunctionalityList" %>

<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <%=htmlText %>
        </div>
        <asp:HiddenField ID="hdnMsgFunctionalityConfirmation" runat="server" />
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Show Functionality Sub Menu
        function showsubmenu(roleID, functionalityID, functionalityType, flag, isAllowAccessAllData) {
            if (functionalityID == "<%=(int)RoleAction.Reporting%>" && functionalityType == "M") {
                window.location.href = "RoleRemoveReportType.aspx?roleID=" + roleID + "&actionID=" + functionalityID + "&flag=" + flag + "&functionalityType=" + functionalityType + " ";
            }
            else if (functionalityID == "<%=(int)RoleAction.Manage_List%>") {
                window.location.href = "RoleManageList.aspx?roleID=" + roleID + "&actionID=" + functionalityID + "&flag=" + flag + "&functionalityType=" + functionalityType + " ";
            }
            else if (functionalityID == "<%=(int)RoleAction.Assign_Training%>" && functionalityType == "M") {
                window.location.href = "RoleTrainingEvent.aspx?roleID=" + roleID + "&actionID=" + functionalityID + "&flag=" + flag + "&functionalityType=" + functionalityType + " ";
            }
            else {
                window.location.href = "AddReportingDashBoard.aspx?roleID=" + roleID + "&actionID=" + functionalityID + "&flag=" + flag + "&functionalityType=" + functionalityType + " ";
            }
    }

    // To Role Remove Functionality COnfirmation
    function removeInRole(roleID, functionalityID, functionalityType, actionName) {
        var title = "<%=Resources.Resource.lblConfirmation%>";
            //var messageText = "<%=Resources.Resource.msgRuWantToRemoveFunctionality%>";
            var messageText = ($("#<%=hdnMsgFunctionalityConfirmation.ClientID%>").val()).replace("#FUNCTIONALITYNAME#", actionName);
            okButtonText = "OK";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "ConfirmRoleRemovefuncOk(" + roleID + ", " + functionalityID + ", '" + functionalityType + "')";
            LaterCnclButtonRedirectLink = "ConfirmRoleRemovefuncCancel();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Trigger Role Remove Functionality OK Button
        function ConfirmRoleRemovefuncOk(roleID, functionalityID, functionalityType) {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "removeAction";
            datatoPost.RoleID = roleID;
            datatoPost.ActionID = functionalityID;
            datatoPost.ActionType = functionalityType;
            $.post("RoleRemoveFunctionality.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    window.location.href = "roleActivity.aspx?roleID=" + roleID + "";
                }
                else {
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotRemoveFunctionality%>")
                }
            });
        }

        // To Trigger Role Remove Functionality Cancel Button
        function ConfirmRoleRemovefuncCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
        }
    </script>
</asp:Content>
