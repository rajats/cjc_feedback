﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <asp:Panel ID="cntPannel" runat="server" DefaultButton="BtnChangePass">
            <div class="wrapper width-med">
                <h1><%= Resources.Resource.lblResetPassword%></h1>
                <div class="boxed-content">
                    <div class="boxed-content-body" onclick="HideMessage()">
                        <div id="dvLogInErrMsg" class="plms-alert invalid" runat="server" visible="false">
                            <p class="last-child">
                                <asp:Label ID="lblErrMsg" runat="server"></asp:Label></p>
                        </div>
                        <header class="form-section-header">
                            <p></p>
                        </header>
                        <div class="plms-fieldset">
                            <label class="plms-label" for="new-password"><%= Resources.Resource.lblEnterNewPassword%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtNewPassword" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblEnterNewPassword %>" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfNewPassword" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="txtNewPassword" Text="<%$ Resources:Resource, reqMsgNewPassword %>" />
                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtNewPassword" ID="RegularExpressionValidator1" class="formels-feedback invalid"
                                    ValidationExpression="^[\s\S]{4,}$" runat="server" ErrorMessage="<%$ Resources:Resource, lblPasswordLength %>"></asp:RegularExpressionValidator>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEnterNewPassword%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label" for="confirm-new-password"><%= Resources.Resource.lblEnterConfirmPassword%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtConfirmPassword" runat="server" class="plms-input skin2 " placeholder="<%$ Resources:Resource, lblEnterConfirmPassword %>" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfConfirmPassword" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="txtConfirmPassword" Text="<%$ Resources:Resource, reqMsgConfirmPassword %>" />
                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtConfirmPassword" ID="RegularExpressionValidator2" class="formels-feedback invalid"
                                    ValidationExpression="^[\s\S]{4,}$" runat="server" ErrorMessage="<%$ Resources:Resource, lblPasswordLength %>"></asp:RegularExpressionValidator>
                                <asp:CompareValidator ID="cmpValidator" ControlToValidate="txtConfirmPassword" ControlToCompare="txtNewPassword" ValidationGroup="grpRole" Text="<%$ Resources:Resource, reqMsgPasswordNotMatch %>" runat="Server"
                                    class="formels-feedback invalid" Display="Dynamic" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEnterConfirmPassword%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="form-footer">
                            <div class="btngrp">
                                <asp:Button ID="BtnChangePass" class="btn round" runat="server" ValidationGroup="grpRole" OnClientClick="return BtnClick();" Text="<%$ Resources:Resource, lblupdatePassword %>" OnClick="BtnChangePass_OnClick" />
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Define To Show Contorls Custom Error message
        function BtnClick() {
            var val = Page_ClientValidate();
            if (!val) {
                var i = 0;
                for (; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $("#" + Page_Validators[i].controltovalidate).addClass("has-error");
                        break;
                    }
                    else {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            else {
                var i = 0;
                for (; i < Page_Validators.length; i++) {

                    {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            return val;
        }

        // To Hide Error Message
        function HideMessage() {
            $("#<%=dvLogInErrMsg.ClientID %>").hide();
        }

        $(document).ready(function () {
            $("#<%=txtNewPassword.ClientID%>").focus();
                });
    </script>
</asp:Content>

