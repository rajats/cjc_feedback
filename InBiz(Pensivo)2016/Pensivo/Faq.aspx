﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="Faq.aspx.cs" Inherits="Faq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div id="dvMainWrapper" class="wrapper">
            <div class="main-content-header">
                <h1><%=Resources.Resource.lblFaq %></h1>
                <a class="btn" id="contact-support" href="support.aspx"><%=Resources.Resource.lblContactSupport  %></a>
            </div>
            <div class="plms-accordion is-faq soft" data-toggle-handle="header">
                <header class="plms-accordion-header">
                    <span href="#nogo" class="openclose-icon closed"></span>
                    <h2 class="h6">1. <%=Resources.Resource.lblFaqQuestion2 %></h2>
                </header>
                <div class="plms-accordion-body">
                    <div class="plms-accordion-body-content">
                        <p><%=Resources.Resource.lblFaqQuestion2Answer1 %></p>
                        <ol>
                            <li>
                                <%=Resources.Resource.lblFaqQuestion2Answer2 %>
                            </li>
                            <li><%=Resources.Resource.lblFaqQuestion2Answer3 %></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="plms-accordion is-faq soft" data-toggle-handle="header">
                <header class="plms-accordion-header">
                    <span href="#nogo" class="openclose-icon closed"></span>
                    <h2 class="h6">2. <%=Resources.Resource.lblFaqQuestion3 %></h2>
                </header>
                <div class="plms-accordion-body">
                    <div class="plms-accordion-body-content">
                        <p> <%=Resources.Resource.lblFaqQuestion3Answer1 %> 
                        </p>
                        <p> <%=Resources.Resource.lblFaqQuestion3Answer2 %> 
                        </p>
                    </div>
                </div>
            </div>
            <div class="plms-accordion is-faq soft" data-toggle-handle="header">
                <header class="plms-accordion-header">
                    <span href="#nogo" class="openclose-icon closed"></span>
                    <h2 class="h6">3. <%=Resources.Resource.lblFaqQuestion4 %></h2>
                </header>
                <div class="plms-accordion-body">
                    <div class="plms-accordion-body-content">
                        <p><%=Resources.Resource.lblFaqQuestion4Answer1 %>
                        </p>
                    </div>
                </div>
            </div>
            <div class="plms-accordion is-faq soft" data-toggle-handle="header">
                <header class="plms-accordion-header">
                    <span href="#nogo" class="openclose-icon closed"></span>
                    <h2 class="h6">4. <%=Resources.Resource.lblFaqQuestion5 %></h2>
                </header>
                <div class="plms-accordion-body">
                    <div class="plms-accordion-body-content">
                        <p><%=Resources.Resource.lblFaqQuestion5Answer1 %>
                        </p>
                        <ol>
                            <li><%=Resources.Resource.lblFaqQuestion5Answer2 %></li>
                            <li><%=Resources.Resource.lblFaqQuestion5Answer3 %></li>
                            <li><%=Resources.Resource.lblFaqQuestion5Answer4 %></li>
                            <li><%=Resources.Resource.lblFaqQuestion5Answer5 %></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="plms-accordion is-faq soft" data-toggle-handle="header">
                <header class="plms-accordion-header">
                    <span href="#nogo" class="openclose-icon closed"></span>
                    <h2 class="h6">5. <%=Resources.Resource.lblFaqQuestion6 %></h2>
                </header>
                <div class="plms-accordion-body">
                    <div class="plms-accordion-body-content">
                        <p><%=Resources.Resource.lblFaqQuestion6Answer1 %>
                        </p>
                        <ul>
                            <li><%=Resources.Resource.lblFaqQuestion6Answer2 %></li>
                            <li id="liIETBS" runat="server"><%=Resources.Resource.lblFaqQuestion6Answer3 %></li>
                            <li id="liIENNAV" runat="server" visible="false"><%=Resources.Resource.lblFaqQuestion6Answer4 %></li>
                            <li><%=Resources.Resource.lblFaqQuestion6Answer5 %></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="plms-accordion is-faq soft" data-toggle-handle="header">
                <header class="plms-accordion-header">
                    <span href="#nogo" class="openclose-icon closed"></span>
                    <h2 class="h6">6. <%=Resources.Resource.lblFaqQuestion7 %></h2>
                </header>
                <div class="plms-accordion-body">
                    <div class="plms-accordion-body-content">
                        <p><a href="Support.aspx"><%=Resources.Resource.lblFaqQuestion7Answer1 %></a> <%=Resources.Resource.lblFaqQuestion7Answer2 %></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

