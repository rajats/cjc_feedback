﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleActivity.aspx.cs" Inherits="roleActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box">
                    <a id="hrfModifyUserAsscociationWithSystem" runat="server" href="#" class="boxset-box-wrapper "><%--is-selected--%>
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblModifyUserAssocationWithSystem%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfModifyTraningEventUserAccess" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblModifyTraningEventUserAccess%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" id="dvRoleActivity" runat="server">
                    <a id="hrfModifyFunctionalitySystemRoleCanAccess" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblModifyFunctionalitySystemRoleCanAccess%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

