﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIWVHManagerSection.aspx.cs" Inherits="AIReports_AIWVHManagerSection" %>

<%@ Register Src="~/AIReports/UserControl/IncidentMenu.ascx" TagName="UC1" TagPrefix="UC1Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
     <script type="text/javascript">
         function GetWVHManagerSectionReport()
         {
             $.ajax( {
                 type: "POST",
                 url: "CommonInterface.aspx/getQuestions",
                 data: "{pageType:'" + "WVHManagerSection" + "'}",
                 dataType: "json",
                 contentType: "application/json; charset=utf-8",
                 success: function ( data )
                 {
                     var index = 0;
                     $.each( data.d, function ( index, value )
                     {


                         
                         
                         
                         

                         if ( index == 0 )
                         {
                             $( "#<%=hdManagerQID.ClientID%>" ).val( value.QuestionID );
                             $( "#lblManagerNameText" ).html( value.QuestionText );
                              $( "#lblManagerNameTextToolTip" ).html( value.QuestionText );
                              $( "#<%=txtmanagersupervisorname.ClientID%>" ).attr( "placeholder", value.QuestionText );
                          }

                         if ( index == 1 )
                         {
                             $( "#<%=hdWVHManagerQuestionID1.ClientID%>" ).val( value.QuestionID );
                             $( "#WVHManagerQuestionIDText" ).html( value.QuestionText );
                         }
                        if ( index == 2 )
                        {
                            $( "#<%=hdWVHManagerQuestionID2.ClientID%>" ).val( value.QuestionID );
                              $( "#WVHManagerQuestionID2Text" ).html( value.QuestionText );
                              $( "#WVHManagerQuestionID2TextTip" ).html( value.QuestionText );
                              $( "#<%=txtdelayreasons.ClientID%>" ).attr( "placeholder", value.QuestionText );
                          }
                        index = parseInt( index ) + 1;

                    } );
                },
                error: function ( XMLHttpRequest, textStatus, errorThrown )
                {
                    //alert( errorThrown );
                }
            } );
          }
          $( document ).ready( function ()
          {
              GetWVHManagerSectionReport();


          } );
          function ClickChange( ID, Value )
          {
              if ( Value == "Yes" )
              {
                  $( "#<%=hdWVHManagerQuestionID1Ans.ClientID%>" ).val( "1" );
              }
              else
              {
                  $( "#<%=hdWVHManagerQuestionID1Ans.ClientID%>" ).val( "0" );
              }

              if ( ID == "ancAiQuestion1Yes" )
              {
                  $( "#<%=ancAiQuestion1Yes.ClientID%>" ).removeClass( 'btn' );
               $( "#<%=ancAiQuestion1Yes.ClientID%>" ).addClass( 'btnChageColor' );
               if ( $( "#<%=ancAiQuestion1No.ClientID%>" ).hasClass( "btnChageColor" ) )
               {
                   $( "#<%=ancAiQuestion1No.ClientID%>" ).removeClass( 'btnChageColor' );
                   $( "#<%=ancAiQuestion1No.ClientID%>" ).addClass( 'btn' );
               }
               $( "#<%=dvResaon.ClientID%>" ).show();

           }
           if ( ID == "ancAiQuestion1No" )
           {
               $( "#<%=ancAiQuestion1No.ClientID%>" ).removeClass( 'btn' );
               $( "#<%=ancAiQuestion1No.ClientID%>" ).addClass( 'btnChageColor' );
               if ( $( "#<%=ancAiQuestion1Yes.ClientID%>" ).hasClass( "btnChageColor" ) )
               {
                   $( "#<%=ancAiQuestion1Yes.ClientID%>" ).removeClass( 'btnChageColor' );
                   $( "#<%=ancAiQuestion1Yes.ClientID%>" ).addClass( 'btn' );
               }
               $( "#<%=dvResaon.ClientID%>" ).hide();
               $( "#<%=txtdelayreasons.ClientID%>" ).val( "" );
           }
       }
    </script>


<section id="main-content" class="pg-ca-preventions">
<span class="mask"></span>
<div class="wrapper">
	<div class="plms-alert secondary-clr">
	   <p>
                    <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
      </p>
	</div><!--End .plms-alert-->
	
	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		    <%=Resources.Resource.lblMandatoryField %></p>
	</div><!--End .plms-alert-->
	
	

	<div class="layout-sidebar-right">
		<div class="boxed-content">			
			<div class="boxed-content-body">			
			<header class="boxed-content-header">
			<h2><%=Resources.Resource.lblManagerText %> </h2>
			 
			</header>
		
			<div class="plms-fieldset push-down pull-up-10">
            <asp:HiddenField ID="hdManagerQID" runat="server" />
			<label class="plms-label is-hidden" for="manager-supervisor-name" id ="lblManagerNameText"> </label>
			<div class="plms-tooltip-parent">
			<input type="text"  class="plms-input skin2" id="txtmanagersupervisorname" name="manager-supervisor-name"  runat="server" />
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id ="lblManagerNameTextToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			

			<div class="plms-fieldset-wrapper">
				<div class="column span-8">
				  <asp:HiddenField ID="hdWVHManagerQuestionID1" runat="server" />
                                <asp:HiddenField ID="hdWVHManagerQuestionID1Ans" runat="server" />
                                <p class="bold" id="WVHManagerQuestionIDText"></p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp align-r">
				                 <a id="ancAiQuestion1Yes" runat="server" class="btn" title="Yes" onclick="ClickChange('ancAiQuestion1Yes', this.text);">Yes</a>
                                    <a id="ancAiQuestion1No" runat="server" class="btn" title="No" onclick="ClickChange('ancAiQuestion1No', this.text);">No</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
			   <div class="divHide" id="dvResaon"  runat="server" >

			<div class="plms-fieldset pull-up-10">
                 <asp:HiddenField  ID="hdWVHManagerQuestionID2"  runat="server"/>
		          <label class="plms-label is-hidden" for="delay-reasons"    id = "WVHManagerQuestionID2Text"> </label>
			<div class="plms-tooltip-parent">
			   <asp:TextBox ID ="txtdelayreasons"  runat="server"   CssClass="plms-textarea skin2"  TextMode="MultiLine"></asp:TextBox>
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="WVHManagerQuestionID2TextTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
					
			</div><!--End .boxed-content-body-->
            </div>
			
			<nav class="pagination-nav">
			 <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />
                        <asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevious  %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large" OnClick="btnPrevious_Click" />

			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span><%=Resources.Resource.lblHideMenu %></span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
			  <UC1Menu:UC1 ID="lblmenu" runat="server" />
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
	                <asp:Button ID="btnSaveAndExit" runat="server" title="<%$ Resources:Resource, lblSaveAndExitLater  %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn large" OnClick="btnSaveAndExit_Click" />
                    <asp:Button ID="btnSubmitmyReport" runat="server" title="<%$ Resources:Resource, lblSubmitMyReport  %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn large align-r" OnClick="btnSubmitmyReport_Click" />
	</div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
</asp:Content>

