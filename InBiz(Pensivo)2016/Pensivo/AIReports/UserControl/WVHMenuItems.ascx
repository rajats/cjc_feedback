﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WVHMenuItems.ascx.cs" Inherits="AIReports_UserControl_WVHMenuItems" %>

<ul> 
<% if (IsRetailWVH) %>
    <%{ %>
  <li><a id="A1" href="~/AIReports/AiDetails.aspx" title=""  runat="server"><%= Resources.Resource.lblDetailPage %> </a></li>
  <li><a id="A2" href="~/AIReports/AIWVHDescription.aspx" title=""  runat="server">  <%= Resources.Resource.lblAccidentIncidentdetailPage %></a></li>
  <li><a id="A3" href="~/AIReports/AIActionAndPrevention.aspx" title=""  runat="server">   <%= Resources.Resource.lblAIActionandPreventionPage %> </a></li>
  <li><a id="A4" href="~/AIReports/AIWVHEmergencyResponse.aspx" title=""  runat="server">   <%= Resources.Resource.lblWVHEmergencyResponsePage %> </a></li>
  <li><a id="A5" href="~/AIReports/AIWitnessesCoWorker.aspx" title=""  runat="server">  <%= Resources.Resource.lblCoworkerPage %></a></li>
  <li><a id="A6" href="~/AIReports/AIWVHComments.aspx" title="" runat="server">  <%= Resources.Resource.lblWVHComments %> </a></li>
   <li><a id="A7" href="~/AIReports/AIWVHManagerSection.aspx" title="" runat="server">  <%= Resources.Resource.lblManagerSupervisorPage %> </a></li>
<li><a id="A8" href="~/AIReports/AIWVHFollowUp.aspx" title=""  runat="server">  <%= Resources.Resource.lblWVHFollowup %>   </a></li>
 <%} %> 
</ul>