﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities; 

public partial class AIReports_UserControl_WVHMenuItems : System.Web.UI.UserControl
{
    List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
    protected  Boolean IsRetailWVH = false;
    protected  Boolean IsLogisticWVHLicensee = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        BindMenu();
    }
    private void BindMenu()
    {
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];        
        // Case 1:  Retail:Yes, WVH:Yes      
        var IsRetailAndLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic || x.AIQuestionAnswerText == "R");
        var IsWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH || x.AIQuestionAnswerText == "1");
        if ((IsRetailAndLogistic != null) && IsWVH != null)
        {
            IsRetailWVH = true;

        }
        // Case : 2 logistic:Yes Licensee:Yes WVH : Yes
        var IsLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "L");
        var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1"  &&  x.AISequence==1);
        var IsWVHCase2 = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
        if (IsLogistic != null && IsLicensee != null && IsWVHCase2 != null)
        {
            IsLogisticWVHLicensee = true;
        }

    }
     
}