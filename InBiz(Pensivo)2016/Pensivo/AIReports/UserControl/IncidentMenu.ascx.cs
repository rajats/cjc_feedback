﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Text;

public partial class AIReports_UserControl_IncidentMenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ltrl.Text = BindMenu();
    }
    private string  BindMenu()
    {
        StringBuilder strData = new StringBuilder();
        string currentPageUrl = string.Empty;
        currentPageUrl = Convert.ToString(Request.Url.Segments.Last());
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        { 
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                strData.Append("<ul>");
                var IsWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
                if (IsWVH != null)
                {  
                    if ( currentPageUrl.Contains("AIWVHEventReportedBy"))
                    {
                        strData.Append("<li class='is-active' ><a id='A1' href='AIWVHEventReportedBy.aspx' title=''  runat='server'>" + Resources.Resource.WVHReportedEventPage + "</a></li>");
                    }
                    else
                    {
                        strData.Append("<li ><a id='A1' href='AIWVHEventReportedBy.aspx' title=''  runat='server'>" + Resources.Resource.WVHReportedEventPage + "</a></li>");
                    }
                    if (currentPageUrl.Contains("AIWVHOffenders"))
                    {
                        strData.Append("<li class='is-active' ><a id='A1' href='AIWVHOffenders.aspx' title=''  runat='server'>" + Resources.Resource.WVHOffendersPage + "</a></li>");
                    }
                    else
                    {
                        strData.Append("<li ><a id='A1' href='AIWVHOffenders.aspx' title=''  runat='server'>" + Resources.Resource.WVHOffendersPage + "</a></li>");
                    }

                  
                    if (currentPageUrl.Contains("AIWVHDescription"))
                    {
                        strData.Append("<li class='is-active' ><a id='A1' href='AIWVHDescription.aspx' title=''  runat='server'>" + Resources.Resource.WVHDescriptionPage + "</a></li>");
                    }
                    else
                    {
                        strData.Append("<li ><a id='A1' href='AIWVHDescription.aspx' title=''  runat='server'>" + Resources.Resource.WVHDescriptionPage + "</a></li>");
                    }

                    if (currentPageUrl.Contains("AIWVHEmergencyResponse"))
                    {
                        strData.Append("<li class='is-active' ><a id='A1' href='AIWVHEmergencyResponse.aspx' title=''  runat='server'>" + Resources.Resource.WVHEmergencyResponsePage + "</a></li>");
                    }
                    else
                    {
                        strData.Append("<li ><a id='A1' href='AIWVHEmergencyResponse.aspx' title=''  runat='server'>" + Resources.Resource.WVHEmergencyResponsePage + "</a></li>");
                    }
                    if (currentPageUrl.Contains("AIWVHEmployeesPresent"))
                    {
                        strData.Append("<li class='is-active' ><a id='A1' href='AIWVHEmployeesPresent.aspx' title=''  runat='server'>" + Resources.Resource.WVHEmployeePresentPage + "</a></li>");
                    }
                    else
                    {
                        strData.Append("<li ><a id='A1' href='AIWVHEmployeesPresent.aspx' title=''  runat='server'>" + Resources.Resource.WVHEmployeePresentPage + "</a></li>");
                    }                   
                    if (currentPageUrl.Contains("AIWVHComments"))
                    {
                        strData.Append("<li class='is-active' ><a id='A1' href='AIWVHComments.aspx' title=''  runat='server'>" + Resources.Resource.WVHCommentPage + "</a></li>");
                    }
                    else
                    {
                        strData.Append("<li ><a id='A1' href='AIWVHComments.aspx' title=''  runat='server'>" + Resources.Resource.WVHCommentPage + "</a></li>");
                    }
                    if (currentPageUrl.Contains("AIWVHManagerSection") || currentPageUrl.Contains("AIWVHSupervisorSection"))
                    {
                        strData.Append("<li class='is-active' ><a id='A1' href='AIWVHManagerSection.aspx' title=''  runat='server'>" + Resources.Resource.WVHManagerSupervisorPage + "</a></li>");
                    }
                    else
                    {
                        strData.Append("<li ><a id='A1' href='AIWVHManagerSection.aspx' title=''  runat='server'>" + Resources.Resource.WVHManagerSupervisorPage + "</a></li>");
                    }
                    if (currentPageUrl.Contains("AIWVHFollowUp"))
                    {
                        strData.Append("<li class='is-active' ><a id='A1' href='AIWVHFollowUp.aspx' title=''  runat='server'>" + Resources.Resource.WVHFollowUpPage + "</a></li>");
                    }
                    else
                    {
                        strData.Append("<li ><a id='A1' href='AIWVHFollowUp.aspx' title=''  runat='server'>" + Resources.Resource.WVHFollowUpPage + "</a></li>");
                    }
                }

                // Commaon Menu

                if (IsWVH == null)
                {

                if (currentPageUrl.Contains("AIGeneralInformation") || currentPageUrl.Contains("AIGetGeneralInformation") || currentPageUrl.Contains("AIEmpGeneralInformation"))
                {
                    strData.Append("<li class='is-active' ><a id='A1' href='AIGeneralInformation.aspx' title=''  runat='server'>" + Resources.Resource.lblGenInfoPage + "</a></li>");
                }
                else
                {
                    strData.Append("<li ><a id='A1' href='AIGeneralInformation.aspx' title=''  runat='server'>" + Resources.Resource.lblGenInfoPage + "</a></li>");
                }
                // End  Section
                }

                var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (ISLicensee != null)
                {
                    if (currentPageUrl.Contains("AiDetails") || currentPageUrl.Contains("AIDetail2") || currentPageUrl.Contains("AIdetails3") || currentPageUrl.Contains("AIDetails4") || currentPageUrl.Contains("AIDetails5") || currentPageUrl.Contains("AIDetails6") || currentPageUrl.Contains("AILicenseeDetails"))
                    {
                        strData.Append("<li  class='is-active'><a id='A2' href='AiDetails.aspx' title=''  runat='server'> " + Resources.Resource.lblDetailPage + "</a></li>");
                    }
                    else
                    {
                        strData.Append("<li><a id='A2' href='AiDetails.aspx' title=''  runat='server'> " + Resources.Resource.lblDetailPage + "</a></li>");
                    }


                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");
                    

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    else if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                     else if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    else if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    else if  (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        } 

                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    else if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    else if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href=AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }

                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    else if  (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        } 

                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    else if  (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        } 
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    else if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {

                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        } 
                    }
                    // Case 11: Personal Injury AND Health Care
                    else if (IsPerosnalInjury != null && IsHealthCare != null)
                    {

                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        } 
                    }
                    // Case 12: Property Damage AND Lost Time
                    else if (IsLostTime != null && IsPropertyDamage != null)
                    {

                    }
                    // Case 13: Personal Injury AND First Aid
                    else if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }


                    }

                    // Case 17: Property Damage
                    else if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {

                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }

                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }

                    // Case 14: Property Damage
                    else if (IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }

                    }
                    // Case 15: Lost Time
                    else if (IsLostTime != null)
                    {

                    }
                    // Case 16: Personal Injury
                    else if (IsPerosnalInjury != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }

                    }

                    var IsIncidentYes = objList.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                    if (IsIncidentYes != null)
                    {
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }  
                    }

                }
                else
                {
                    if (currentPageUrl.Contains("AiDetails") || currentPageUrl.Contains("AIDetail2") || currentPageUrl.Contains("AIdetails3") || currentPageUrl.Contains("AIDetails4") || currentPageUrl.Contains("AIDetails5") || currentPageUrl.Contains("AIDetails6"))
                    {
                        strData.Append("<li  class='is-active'><a id='A2' href='AiDetails.aspx' title=''  runat='server'> " + Resources.Resource.lblDetailPage + "</a></li>");
                    }
                    else
                    {
                        strData.Append("<li><a id='A2' href='AiDetails.aspx' title=''  runat='server'> " + Resources.Resource.lblDetailPage + "</a></li>");
                    }

                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    else if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    else if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    else if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    else if  (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        } 
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    else if  (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    else if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {

                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href=AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }


                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    else if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        } 

                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    else if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {

                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        } 

                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    else if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {

                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        } 

                    }
                    // Case 11: Personal Injury AND Health Care
                    else if (IsPerosnalInjury != null && IsHealthCare != null)
                    {

                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        } 

                    }
                    // Case 12: Property Damage AND Lost Time
                    else if (IsLostTime != null && IsPropertyDamage != null)
                    {

                    }
                    // Case 13: Personal Injury AND First Aid
                    else if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }

                    // Case 17: Property Damage
                    else if ( IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }

                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }

                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }

                    }  

                    // Case 14: Property Damage
                    else if (IsPropertyDamage != null)
                    {

                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }

                    }
                    // Case 15: Lost Time
                    else if (IsLostTime != null)
                    {

                    }
                    // Case 16: Personal Injury
                    else if (IsPerosnalInjury != null)
                    {
                        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
                        {
                            strData.Append("<li class='is-active'><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A2' href='AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

                        }
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    }

                    var IsIncidentYes = objList.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                    if (IsIncidentYes != null)
                    {
                        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7") || currentPageUrl.Contains("AIDetails8"))
                        {
                            strData.Append("<li class='is-active'><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A3' href='AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
                        }
                        if (currentPageUrl.Contains("AIContributingFactor"))
                        {
                            strData.Append("<li class='is-active'><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A4' href='AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
                        }
                        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm"))
                        {
                            strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                        else
                        {
                            strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                        }
                    } 
                } 

                if(IsWVH != null)
                { 

                   if (currentPageUrl.Contains("AIActionAndPrevention"))
                   {
                      strData.Append("<li class='is-active'><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                   }
                   else
                   {
                    strData.Append("<li><a id='A5' href='AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
                  }
                }
                // Common section 
                if (currentPageUrl.Contains("AIWitnessesCoWorker"))
                {
                    strData.Append("<li class='is-active'><a id='A6' href='AIWitnessesCoWorker.aspx' title=''  runat='server'>   " + Resources.Resource.lblCoworkerPage + "  </a></li>");
                }
                else
                {
                    strData.Append("<li><a id='A6' href='AIWitnessesCoWorker.aspx' title=''  runat='server'>" + Resources.Resource.lblCoworkerPage + "  </a></li>");
                }
                if (currentPageUrl.Contains("AIWorkerComment"))
                {
                    strData.Append("<li class='is-active'><a id='A7' href='AIWorkerComment.aspx' title='' runat='server'>" + Resources.Resource.lblWorkerCommentPage + " </a></li>");
                }
                else
                {
                    strData.Append("<li><a id='A7' href='AIWorkerComment.aspx' title='' runat='server'>" + Resources.Resource.lblWorkerCommentPage + " </a></li>");
                }
                if (currentPageUrl.Contains("AIPeopleInvolved"))
                {
                    strData.Append("<li class='is-active'><a id='A8' href='AIPeopleInvolved.aspx' title=''  runat='server'>" + Resources.Resource.lblPeopleInvolvedPage + "</a></li>");
                }
                else
                {
                    strData.Append("<li><a id='A8' href='AIPeopleInvolved.aspx' title=''  runat='server'>" + Resources.Resource.lblPeopleInvolvedPage + "</a></li>");
                }
                // Common section End



                strData.Append("</ul>");
            }
        }
        catch { }
        finally { }
        return Convert.ToString(strData);
    }
    //private string  CreateMenu()
    //{
    //    StringBuilder strData = new StringBuilder();
    //    string currentPageUrl = string.Empty;        
    //    currentPageUrl = Convert.ToString(Request.Url.Segments.Last());
    //    try
    //    {     

    //        if (currentPageUrl.Contains("AIDescriptionInjuryIllness") || currentPageUrl.Contains("AICauseofInjury") || currentPageUrl.Contains("AINatureofInjury"))
    //        {
    //            strData.Append("<li class='is-active'><a id='A2' href='~/AIReports/AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");
    //        }
    //        else
    //        {
    //            strData.Append("<li><a id='A2' href='~/AIReports/AIDescriptionInjuryIllness.aspx' title=''  runat='server'> " + Resources.Resource.lblDescriptionofinjuryIllnessPage + "</a></li>");

    //        }
    //        if (currentPageUrl.Contains("AIReturntowork") || currentPageUrl.Contains("AIReturntowork1") || currentPageUrl.Contains("HealthCareDetails"))
    //        {
    //            strData.Append("<li class='is-active'><a id='A2' href='~/AIReports/AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
    //        }
    //        else
    //        {
    //            strData.Append("<li><a id='A2' href='~/AIReports/AIReturntowork.aspx' title=''  runat='server'> " + Resources.Resource.lblReturntoworkPage + "</a></li>");
    //        }
    //        if (currentPageUrl.Contains("AIAccidentIncidentDetail") || currentPageUrl.Contains("AIIncidentReason") || currentPageUrl.Contains("AIDetails7"))
    //        {
    //            strData.Append("<li class='is-active'><a id='A3' href='~/AIReports/AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
    //        }
    //        else
    //        {
    //            strData.Append("<li><a id='A3' href='~/AIReports/AIAccidentIncidentDetail.aspx' title=''  runat='server'>  " + Resources.Resource.lblAccidentIncidentdetailPage + "</a></li>");
    //        }
    //        if (currentPageUrl.Contains("AIContributingFactor"))
    //        {
    //            strData.Append("<li class='is-active'><a id='A4' href='~/AIReports/AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
    //        }
    //        else
    //        {
    //            strData.Append("<li><a id='A4' href='~/AIReports/AIContributingFactor.aspx' title=''  runat='server'> " + Resources.Resource.lblcontributingFactorPage + " </a></li>");
    //        }
    //        if (currentPageUrl.Contains("AIPersonalProtectiveEquipment"))
    //        {
    //            strData.Append("<li class='is-active'><a id='A5' href='~/AIReports/AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
    //        }
    //        else
    //        {
    //            strData.Append("<li><a id='A5' href='~/AIReports/AIPersonalProtectiveEquipment.aspx' title='' runat='server'> " + Resources.Resource.lblAiPersonProtectivePage + "  </a></li>");
    //        }
    //        if (currentPageUrl.Contains("AIActionAndPrevention") || currentPageUrl.Contains("AIActionAndPreventionConfirm") || currentPageUrl.Contains("AIActionAndPreventionConfirm")) 
    //        {
    //            strData.Append("<li class='is-active'><a id='A5' href='~/AIReports/AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
    //        }   
    //        else 
    //        {
    //            strData.Append("<li><a id='A5' href='~/AIReports/AIActionAndPrevention.aspx' title='' runat='server'> " + Resources.Resource.lblAiCorrectionActionPage + "  </a></li>");
    //        }
    //        if (currentPageUrl.Contains("AIGetPropertyDamageDetail"))
    //        {
    //            strData.Append("<li class='is-active'><a id='A2' href='~/AIReports/AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
    //        }
    //        else
    //        {
    //            strData.Append("<li><a id='A2' href='~/AIReports/AIGetPropertyDamageDetail.aspx' title=''  runat='server'> " + Resources.Resource.lblPropertyDamagePage + "</a></li>");
    //        }


            
    //    }
    //    catch { }
    //    finally { }

    //    return Convert.ToString(strData);
    //}

}