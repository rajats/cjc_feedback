﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIReturntowork : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();

        }
    }   
    protected void btnPrevious_Click(object sender, EventArgs e)
    { 
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {            
            if(objList != null)
            {

            var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
            if (IsLicensee != null)
            {
                // Get result from all scenarios
                var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 6 : Personal Injury AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 7 : Personal Injury AND First Aid AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AILicenseeDetails.aspx", false);
                    return;
                }
                // Case 8 : Personal Injury AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 9 : Personal Injury AND First Aid AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 10: Personal Injury AND First Aid AND Health Care
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 11: Personal Injury AND Health Care
                if (IsPerosnalInjury != null && IsHealthCare != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 12: Property Damage AND Lost Time
                if (IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AILicenseeDetails.aspx", false);
                    return;
                }
                // Case 13: Personal Injury AND First Aid
                if (IsPerosnalInjury != null && IsFirstAidBox != null)
                {
                    Response.Redirect("AILicenseeDetails.aspx", false);
                    return;
                }
                // Case 14: Property Damage
                if (IsPropertyDamage != null)
                {
                    Response.Redirect("AILicenseeDetails.aspx", false);
                    return;
                }
                // Case 15: Lost Time
                if (IsLostTime != null)
                {
                    Response.Redirect("AILicenseeDetails.aspx", false);
                    return;
                }
                // Case 16: Personal Injury
                if (IsPerosnalInjury != null)
                {
                    Response.Redirect("AILicenseeDetails.aspx", false);
                    return;
                }

            }
            else
            {

                // Get result from all scenarios
                var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 6 : Personal Injury AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 7 : Personal Injury AND First Aid AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails6.aspx", false);
                    return;
                }
                // Case 8 : Personal Injury AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 9 : Personal Injury AND First Aid AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 10: Personal Injury AND First Aid AND Health Care
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 11: Personal Injury AND Health Care
                if (IsPerosnalInjury != null && IsHealthCare != null)
                {
                    Response.Redirect("AINatureofInjury.aspx", false);
                    return;
                }
                // Case 12: Property Damage AND Lost Time
                if (IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails6.aspx", false);
                    return;
                }
                // Case 13: Personal Injury AND First Aid
                if (IsPerosnalInjury != null && IsFirstAidBox != null)
                {
                    Response.Redirect("AIDetails6.aspx", false);
                    return;
                }
                // Case 14: Property Damage
                if (IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails6.aspx", false);
                    return;
                }
                // Case 15: Lost Time
                if (IsLostTime != null)
                {
                    Response.Redirect("AIDetails6.aspx", false);
                    return;
                }
                // Case 16: Personal Injury
                if (IsPerosnalInjury != null)
                {
                    Response.Redirect("AIDetails6.aspx", false);
                    return;
                } 
            } 
            } 
        }
        catch { }
        finally { }        
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.ReturnToWork).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }
                  
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestion1ID.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdQuestion1ID.Value),
                             AIQuestionType = QuestionType.BooleanType,
                             AIQuestionAnswerText = Convert.ToString(hdQuestion1IDAns.Value) == "" ? ConfirmationYesNo.NotAnswered : Convert.ToString(hdQuestion1IDAns.Value),
                             AIFormID = aiFormID,
                             AIPageName = PageName.ReturnToWork,
                             AISequence = 1
                         });                
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestion1ID.Value)) && Convert.ToString(hdQuestion1IDAns.Value) == "1" &&  !string.IsNullOrEmpty(Convert.ToString(hdQuestion2ID.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdQuestion2ID.Value),
                             AIQuestionType = QuestionType.TextType,
                             AIQuestionAnswerText = Convert.ToString(txtlicenseeownermanager.Text),
                             AIFormID = aiFormID,
                             AIPageName = PageName.ReturnToWork,
                             AISequence = 2
                         }); 
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestion1ID.Value)) && Convert.ToString(hdQuestion1IDAns.Value) == "1" && !string.IsNullOrEmpty(Convert.ToString(hdQuestion3ID.Value)))
                {
                    objList.Add(
                            new AIQuestionAnswer
                            {
                                AIQuestionID = Convert.ToInt32(hdQuestion3ID.Value),
                                AIQuestionType = QuestionType.TextType,
                                AIQuestionAnswerText = Convert.ToString(txtdateofmodified.Text),
                                AIFormID = aiFormID,
                                AIPageName = PageName.ReturnToWork,
                                AISequence = 3
                            });

                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestion4ID.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdQuestion4ID.Value),
                             AIQuestionType = QuestionType.BooleanType,
                             AIQuestionAnswerText = Convert.ToString(hdQuestion4IDAns.Value) == "" ? ConfirmationYesNo.NotAnswered : Convert.ToString(hdQuestion4IDAns.Value),
                             AIFormID = aiFormID,
                             AIPageName = PageName.ReturnToWork,
                             AISequence = 4
                         });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestion4ID.Value)) && Convert.ToString(hdQuestion1IDAns.Value) == "0" && !string.IsNullOrEmpty(Convert.ToString(hdQuestion5ID.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdQuestion5ID.Value),
                             AIQuestionType = QuestionType.TextType,
                             AIQuestionAnswerText = Convert.ToString(txtBox1.Text),
                             AIFormID = aiFormID,
                             AIPageName = PageName.ReturnToWork,
                             AISequence = 5
                         });  
                } 
             
                Session["AIReportQuestions"] = objList;

                 //managePreviousNext("AIDetails4.aspx");

                 // RedirectionLink();
                 //Response.Redirect("AIDetails3.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { } 

        Response.Redirect("AIReturntowork1.aspx", false);
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBoard.aspx", false);
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.ReturnToWork).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 0;
                    foreach (var result in resultItem)
                    {
                        if (count == 0 && result.AISequence == 1)
                        {
                            if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                            {
                                hdQuestion1IDAns.Value = "1";
                                ancHaWorkYes.Attributes.Remove("Class");
                                ancHaWorkYes.Attributes.Add("Class", "btnChageColor");
                                
                                dvLicenceWorkerManager.Attributes.Remove("Class");
                                dvLicenceWorkerManager.Attributes.Add("Class", "divShow");

                                dvDateOfModified.Attributes.Remove("Class");
                                dvDateOfModified.Attributes.Add("Class", "divShow");
                            }
                            else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                            {
                                hdQuestion1IDAns.Value = "1";
                                ancHaWorkNo.Attributes.Remove("Class");
                                ancHaWorkNo.Attributes.Add("Class", "btnChageColor"); 
                            }
                        }
                        else if (count == 1 && result.AISequence == 2)
                        {
                             txtlicenseeownermanager.Text = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        else if (count == 2 && result.AISequence == 3)
                        {
                            txtdateofmodified.Text = Convert.ToDateTime(result.AIQuestionAnswerText).ToString("yyyy-MM-dd");
                        }
                        else if (count == 3 && result.AISequence == 4)
                        {
                            if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                            {
                                hdQuestion4IDAns.Value = "1";
                                ancDateYes.Attributes.Remove("Class");
                                ancDateYes.Attributes.Add("Class", "btnChageColor");                                
                            }
                            else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                            {
                                hdQuestion4IDAns.Value = "0";

                                ancDateNo.Attributes.Remove("Class");
                                ancDateNo.Attributes.Add("Class", "btnChageColor");

                                dvQuestion5.Attributes.Remove("Class");
                                dvQuestion5.Attributes.Add("Class", "divShow");
                            } 
                        }
                        else if (count == 4 && result.AISequence == 5)
                        {
                            txtBox1.Text = Convert.ToString(result.AIQuestionAnswerText);

                        }
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }
    }


}