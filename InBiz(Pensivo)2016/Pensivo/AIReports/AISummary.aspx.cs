﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AISummary : System.Web.UI.Page
{
    List<Employee> empObj = new List<Employee>();
    string typeofIncident = string.Empty;
    string licenseeDetails = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                 
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                        lblEmpNameD.Text = BusinessUtility.GetString(result.EmpName);
                        lblSiteNo.Text = BusinessUtility.GetString(result.Location);
                        lblEmpIDs.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSiteno1.Text = BusinessUtility.GetString(result.Location);
                        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
                        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
                        if (objList != null)
                        {
                            var IsretailLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic);
                            if (IsretailLogistic != null)
                            {
                                if (Convert.ToString(IsretailLogistic.AIQuestionAnswerText) == "R")
                                {
                                    lblType.Text = "Retail";
                                }
                                else
                                {
                                    lblType.Text = "Logistics";
                                } 
                            }
                            var IsLicensee = objList.Where(x => x.AIPageName == PageName.IsLogistic).ToList();
                            if (IsLicensee != null)
                            {
                                int count = 0;
                                foreach (var IsLicenseed in IsLicensee)
                                {
                                    if (count ==0)
                                    {
                                        if (IsLicenseed.AIQuestionAnswerText == "1")
                                        {
                                            licenseeDetails = "Yes";
                                        }
                                        else if (IsLicenseed.AIQuestionAnswerText == "0")
                                        {
                                            licenseeDetails = "No";
                                        }
                                        else if (IsLicenseed.AIQuestionAnswerText == "2")
                                        {
                                            licenseeDetails = "NA";
                                        }                                     
                                    }
                                    if (count == 1)
                                    {
                                        if (IsLicenseed.AIQuestionAnswerText == "Check off in Inside ?")
                                        {
                                            licenseeDetails = licenseeDetails + "," + "Fleet";
                                        } 
                                    }
                                    count++; 
                                }                                  
                            }                            
                            if (IsLicensee != null &&  IsLicensee.Count>0)
                            {
                                lblIsLicensee.Text = licenseeDetails;                                 
                            }
                            else
                            {
                                lblIsLicensee.Text = "N/A";
                            }



                            var result1 = objList.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                            if (result1 != null)
                            {
                                lblTypeOfincident.Text = "Incident";
                            }
                            else
                            {
                                var GetResult = objList.Where(x => x.AIQuestionAnswerText == "1").ToList();
                                if (GetResult != null)
                                {
                                    foreach (var items in GetResult)
                                    {
                                        if (Convert.ToString(items.AIPageName) == PageName.PersonalInjury)
                                        {
                                            typeofIncident = typeofIncident + "Personal  Injury"   + ","  + " " ;
                                        }
                                        if (Convert.ToString(items.AIPageName) == PageName.FirstAidBox)
                                        {
                                            typeofIncident = typeofIncident + "First Aid" + "," + " ";
                                        }
                                        if (Convert.ToString(items.AIPageName) == PageName.HealthCareConfirmation)
                                        {
                                            typeofIncident = typeofIncident + "Health  Care" + "," + " ";
                                        }
                                        if (Convert.ToString(items.AIPageName) == PageName.LostTime)
                                        {
                                            typeofIncident = typeofIncident + "Lost Time" + "," + " ";
                                        }
                                        if (Convert.ToString(items.AIPageName) == PageName.PropertyDamageQuestion)
                                        {
                                            typeofIncident = typeofIncident + "Property  Damage" + "," + " ";
                                        }
                                        if (Convert.ToString(items.AIPageName) == PageName.WVH)
                                        {
                                            typeofIncident = typeofIncident + "WVH" + "," + " ";
                                        }
                                    }
                                }
                            }
 
                            if (!string.IsNullOrEmpty(typeofIncident))
                            {
                                lblTypeOfincident.Text = typeofIncident.Remove(typeofIncident.Length - 2);
                            }

                           

                        }
                       
                      
                    }
                }
            } 
        }
    } 
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBoard.aspx", false);
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            //Response.Redirect("AIGeneralInformation.aspx", false);             
        } 
        catch { }
        finally { }
        //Response.Redirect("AIDashBoard.aspx", false);
    }
    protected void btnPrevoius_Click(object sender, EventArgs e)    
    {
        List<ManagePagePreviousNext> objPageDetail = new List<ManagePagePreviousNext>();
        objPageDetail = (List<ManagePagePreviousNext>)Session["AIPagesList"];
        if (objPageDetail != null)
        {
            var pageUrlResult = objPageDetail.FirstOrDefault(x=>x.pageName == PageName.Incident);
            if (pageUrlResult != null)
            {
                Response.Redirect(Convert.ToString(pageUrlResult.currentPageUrl));
            }
            var pageUrlResult1 = objPageDetail.FirstOrDefault(x => x.pageName == PageName.PropertyDamageQuestion);

            if (pageUrlResult1 != null)
            {
                Response.Redirect(Convert.ToString(pageUrlResult1.currentPageUrl));
            } 
        }   
    }
    protected void btnYes_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];

        try
        {
           // Response.Redirect("AIGeneralInformation.aspx", false);

            // Check  for WVH Yes

            if (objList != null)
            {
                var ISWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
                if (ISWVH != null)
                {
                    Response.Redirect("AIWVHEventReportedBy.aspx", false);
                    return;
                }
                else
                {
                    Response.Redirect("AIGeneralInformation.aspx", false);
                    return;
                }
            }
        }
        catch { }
        finally { }
        
                  
    }
}