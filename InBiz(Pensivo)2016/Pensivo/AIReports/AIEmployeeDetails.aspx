﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIEmployeeDetails.aspx.cs" Inherits="AIReports_AIEmployeeDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="../lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
        <%if (Utils.TrainingInst == 3)  %>
    <%{ %>
                    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
            <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="../lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="../lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="../lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="../lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>


    <link rel="stylesheet" href="../_styles/ai-reports.css" />
    <link href="../_css/global.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            DisableNext();
        });

        function DisableNext() {
            $("#<%=hrfNext.ClientID %>").attr("disabled", "disabled");
          $("#<%=hrfNext.ClientID %>").removeClass("btn").removeClass("large").removeClass("align-r");
          $("#<%=hrfNext.ClientID %>").addClass("btngrayout").addClass("large").addClass("align-r");
      }

      function EnableNext() {
          $("#<%=hrfNext.ClientID %>").removeAttr("disabled");
          $("#<%=hrfNext.ClientID %>").removeClass("btngrayout").removeClass("large").removeClass("align-r");
          $("#<%=hrfNext.ClientID %>").addClass("btn").addClass("large").addClass("align-r");
      }
    </script>

    <section id="main-content" class="pg-affected-employee">
        <span class="mask"></span>
        <div class="wrapper">

            <div class="boxed-content">
                <h2><%= Resources.Resource.lblAffectedEmployeeText %></h2>

                <div class="boxed-content-body">
                    <%--<asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">--%>
                    <div id="SearchPanel" class="column span-5 form-fields">
                        <div class="plms-fieldset first-child">
                            <%--<label class="plms-label is-hidden" for="txtEmpName">Employee Name</label>--%>
                            <asp:Label ID="Label2" AssociatedControlID="txtEmpName" class="filter-key plms-label is-hidden" runat="server"
                                Text="<%$ Resources:Resource, lblAIEmployeeName %>" for="txtEmpName"></asp:Label>
                            <div class="plms-tooltip-parent">
                                <%--<input type="text" placeholder="Employee Name" class="plms-input skin2" id="employee-name" name="employee-name" />--%>
                                <asp:TextBox ID="txtEmpName" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblAIEmployeeName %>">
                                </asp:TextBox>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%=Resources.Resource.lblAIEmployeeName %></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                        <div class="plms-fieldset">
                            <%--<label class="plms-label is-hidden" for="employee-id">5 digit Employee ID (E.g. 58930)</label>--%>
                            <asp:Label ID="Label1" AssociatedControlID="txtEmpID" class="filter-key plms-label is-hidden" runat="server"
                                Text="<%$ Resources:Resource, lblAIEmpIDeg %>" for="txtEmpID"></asp:Label>
                            <div class="plms-tooltip-parent">
                                <%--<input type="text" maxlength="5" placeholder="5 digit Employee ID (E.g. 58930)" class="plms-input skin2" id="employee-id" name="employee-id" />--%>
                                <asp:TextBox ID="txtEmpID" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblAIEmpIDeg %>">
                                </asp:TextBox>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%=Resources.Resource.lblAIEmpIDeg %></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                        <div class="plms-fieldset">
                            <%--<label class="plms-label is-hidden" for="site-number">Site #</label>--%>
                            <asp:Label ID="Label3" AssociatedControlID="txtSite" class="filter-key plms-label is-hidden" runat="server"
                                Text="<%$ Resources:Resource, lblAISite %>" for="txtSite"></asp:Label>
                            <div class="plms-tooltip-parent">
                                <%--<input type="text" maxlength="10" placeholder="Site #" class="plms-input skin2" id="site-number" name="site-number" />--%>
                                <asp:TextBox ID="txtSite" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblAISite %>">
                                </asp:TextBox>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%=Resources.Resource.lblAISite %></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                        <%--<button class="btn fluid large search-button">Search</button>--%>
                        <input id="btnSearch" class="btn fluid large search-button" type="button" value="<%=Resources.Resource.lbSearch%>" />

                    </div>
                    <!--End .column-->

                    <div class="column span-7">
                        <div id="grid_wrapper" class="plms-table" onkeypress="return disableEnterKey(event)"  style="width: 100%;">
                            <%--<div id="grid_wrapper" style="width: 100%;">--%>
                            <trirand:JQGrid runat="server" ID="gvRoles" MultiSelect="true" multiboxonly="true"
                                AutoWidth="True" OnCellBinding="gvRoles_CellBinding" OnDataRequesting="gvRoles_DataRequesting">
                                <Columns>
                                    <trirand:JQGridColumn DataField="empHdrID" Visible="false" PrimaryKey="True" />
                                    <trirand:JQGridColumn DataField="EmpFirstName" HeaderText="<%$ Resources:Resource, lblEmpFirstName %>"
                                        Editable="false" Width="200" />
                                    <trirand:JQGridColumn DataField="EmpLastName" HeaderText="<%$ Resources:Resource, lblEmpLastName %>"
                                        Editable="false" Width="200" />
                                    <trirand:JQGridColumn DataField="Store" HeaderText="<%$ Resources:Resource, lblAIEmployeeSite %>"
                                        Editable="false" Width="200" />
                                    <trirand:JQGridColumn DataField="EmpExtID" HeaderText="<%$ Resources:Resource, lblAIEmployeeID %>"
                                        Editable="false" Width="200" />
                                    <%--<trirand:JQGridColumn DataField="empEmail" HeaderText="<%$ Resources:Resource, lblEmail %>"
                                        Editable="false" Width="200" />--%>

                                    <%--<trirand:JQGridColumn DataField="empHdrID" HeaderText="<%$ Resources:Resource, lblEdit %>"
                                        Sortable="false" TextAlign="Center" Width="50" />--%>
                                </Columns>
                                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                                    ShowDeleteButton="false" ShowSearchButton="false" />
                                <SortSettings InitialSortColumn=""></SortSettings>
                                <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                                <ClientSideEvents LoadComplete="loadComplete" BeforeRowSelect="beforeSelectRow" />
                            </trirand:JQGrid>
                            <%--</div>--%>
                        </div>
                         
                        <%--<div class="plms-table">--%>



                        <%--<table>
				<thead>
				<tr>
				<th>&nbsp;</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Employee Site #</th>
				<th>Employee ID</th>
				</tr>
				</thead>
				
				<tbody>
				<tr>
				<td><input type="radio" name="affected-employee" /></td>
				<td>Joshua</td>
				<td>Johnston</td>
				<td>1308</td>
				<td>06690</td>
				</tr>
				<tr>
				<td><input type="radio" name="affected-employee" /></td>
				<td>John</td>
				<td>Speece</td>
				<td>1321</td>
				<td>71144</td>
				</tr>
				<tr>
				<td><input type="radio" name="affected-employee" /></td>	
				<td>Derek</td>
				<td>Johnston</td>
				<td>2012</td>
				<td>57723</td>
				</tr>
				</tbody>
			</table>--%>
                        <%--</div>--%><!--End .plms-table-->
                    </div>
                    <!--End .column-->
                    <%--</asp:Panel>--%>
                </div>
                <!--End .boxed-content-body-->

                <nav class="pagination-nav">
                    <a href="AIEmailValidation.aspx" runat="server" title="<%$ Resources:Resource, lblPrevious %>" class="btn large"><%=Resources.Resource.lblPrevious %></a>
                    <a href="#" onclick="getSelectedRole();" id="hrfNext" title="<%$ Resources:Resource, lblNext %>" class="btn large align-r" disabled="disabled" runat="server"><%=Resources.Resource.lblNext %></a>
                    <%--<asp:Button ID="btnNext" runat="server" class="btn large align-r" Text="<%$ Resources:Resource, lblAINext %>" OnClientClick="getSelectedRole();"   />--%>
                </nav>
                <!--End .pagination-nav-->

            </div>
            <!--End .boxed-content-->

            <footer>

                <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn" OnClick="btnSubmitwithoutSaving_Click" />

                <!--   <a href="#nogo" class="btn" title="Exit Without Saving">Exit Without Saving</a>-->
            </footer>

        </div>
        <!--End .wrapper-->
    </section>
    <script type="text/javascript">
        //$(document).ready(function () {
        //    $("#main-content").removeClass("pg-user-new");
        //    $("#main-content").addClass("pg-advanced-list");
        //});
        $grid = $( "#<%=gvRoles.ClientID%>" );
        $grid.initGridHelper( {
            searchPanelID: "SearchPanel",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        } );

        function jqGridResize()
        {
            $( "#<%=gvRoles.ClientID%>" ).jqResizeAfterLoad( "grid_wrapper", 0 );
        }

        function loadComplete( data )
        {
            $( "#cb_ContentPlaceHolder1_gvRoles" ).hide();
            if ( data.rows.length == 0 && data.total == 0 )
            {
                if ( $( '#<%=txtEmpName.ClientID%>' ).val() != "" || $( '#<%=txtEmpID.ClientID%>' ).val() != "" || $( '#<%=txtSite.ClientID%>' ).val() != "" )
                {
                    ShowPensivoMessage("<p style='text-align:justify;'><%=Resources.Resource.lblemprecordnotfound%><p>");
                }
            }
            else
            {
                jqGridResize();
            }
        }
        $("#btnSearch").click(function () {
            if ($('#<%=txtEmpName.ClientID%>').val() == "" && $('#<%=txtEmpID.ClientID%>').val() == "" && $('#<%=txtSite.ClientID%>').val() == "") {
                ShowPensivoMessage("<p style='text-align:justify;'><%=Resources.Resource.lblBlankSearch%><p>");
            }
            DisableNext();
        });


        function reloadGrid() {
            $('#<%=gvRoles.ClientID%>').trigger("reloadGrid");
        }

        function beforeSelectRow(id) {
            $('#<%=gvRoles.ClientID%>').jqGrid('resetSelection');
            EnableNext();
            return (true);
        }


        function getSelectedRole()
        {
            var arrRooms = $grid.getGridParam( 'selarrrow' );
            if ( arrRooms.length <= 0 )
            {
                ShowPensivoMessage( "<%=Resources.Resource.lblAIPleaseSelectEmployee%>" );
                return false;
            }
            else
            {
                window.location.href = "AILocationInfo.aspx?empID=" + arrRooms.join( "," );
            }
        }

    </script>
</asp:Content>

