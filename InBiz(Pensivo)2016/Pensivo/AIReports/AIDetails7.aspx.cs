﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIDetails7 : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();

            //if (!string.IsNullOrEmpty(Convert.ToString(Request.UrlReferrer)))
            //{
            //    ViewState["RefUrl"] = Request.UrlReferrer.ToString();

            //}
            //else
            //{
            //    Response.Redirect("AILocationInfo.aspx", false);
            //} 
        }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.IncidentAccidentDetail6).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 0;
                    foreach (var result in resultItem)
                    {
                        if (count == 0)
                        {
                            txtaddress.Value = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {
                // Check Is Licensee
                var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (ISLicensee != null)
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIReturntowork1.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIReturntowork1.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIReturntowork1.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIReturntowork1.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIReturntowork1.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIAccidentIncidentDetail.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    //Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }

                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AINatureofInjury.aspx", false);
                        return;
                    }

                    var ISIncident = objList.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                    if (ISIncident != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }

                }
                else
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AINatureofInjury.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AINatureofInjury.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails4.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AINatureofInjury.aspx", false);
                        return;
                    }

                    var ISIncident = objList.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                    if (ISIncident != null)
                    {
                        Response.Redirect("AIDetails4.aspx", false);
                        return;
                    }
                }
            }

        }
        catch { }
        finally { }


       
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.IncidentAccidentDetail6).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID1.Value)))
                {
                    objList.Add(
                     new AIQuestionAnswer
                     {
                         AIQuestionID = Convert.ToInt32(hdAIQuestionID1.Value),
                         AIQuestionType = QuestionType.TextType,
                         AIQuestionAnswerText = Convert.ToString(txtaddress.Value),
                         AIFormID = aiFormID,
                         AIPageName = PageName.IncidentAccidentDetail6
                     });
                } 

                Session["AIReportQuestions"] = objList;

                managePreviousNext("AIAccidentIncidentDetail.aspx");

                RedirectionLink();
                // Response.Redirect("AIAccidentIncidentDetail.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }

    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }
    private void managePreviousNext(string NextPageUrl)
    {
        List<ManagePagePreviousNext> objPage = new List<ManagePagePreviousNext>();
        try
        {
            objPage = (List<ManagePagePreviousNext>)Session["AIPagesList"];
            if (objPage != null)
            {
                var resultItem = objPage.Where(x => x.pageName == PageName.IncidentAccidentDetail3).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objPage.Remove(removeItem);
                    }
                }
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIDetails7.aspx", NextPageUrl = NextPageUrl, pageName = PageName.IncidentAccidentDetail6, PreviousPageUrl = "" });
            }
            else
            {
                objPage = new List<ManagePagePreviousNext>();
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIDetails7.aspx", NextPageUrl = NextPageUrl, pageName = PageName.IncidentAccidentDetail6, PreviousPageUrl = "" });
            }
            Session["AIPagesList"] = objPage;
        }
        catch { }
        finally { }
    }
    private void RedirectionLink()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {

                // Check Is Licensee
                var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.Location && x.IsLicensee == "1");
                if (ISLicensee != null)
                {
                    var IsIncidentYes = objList.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                    if (IsIncidentYes != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }

                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                }
                else
                {
                    var IsIncidentYes = objList.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                    if (IsIncidentYes != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }

                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIDetails8.aspx", false);
                        return;
                    }
                }
            }
        }
        catch { }
        finally { }
    }

}