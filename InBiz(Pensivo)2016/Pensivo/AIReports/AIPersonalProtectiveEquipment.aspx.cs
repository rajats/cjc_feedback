﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AIPersonalProtectiveEquipment : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();
        }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.PersonalProtectiveEquipment).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 0;
                    foreach (var result in resultItem)
                    {
                        if (count == 0 &&  Convert.ToInt32(result.AISequence)==1)
                        {
                           if( Convert.ToString(result.AIQuestionAnswerText) == "1")
                           {                            
                               ancAiQuestionD1Yes.Attributes.Remove("Class");
                               ancAiQuestionD1Yes.Attributes.Add("Class", "btnChageColor");
                               divChkSection.Attributes.Remove("Class");
                               divChkSection.Attributes.Add("Class", "divShow");
                           }
                           else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                           {
                               ancAiQuestionD1No.Attributes.Remove("Class");
                               ancAiQuestionD1No.Attributes.Add("Class", "btnChageColor");
                           } 
                        }
                        else if (count == 1 && Convert.ToInt32(result.AISequence) == 2 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {                           
                             chkbox2.Checked = true;                          
                        }
                        else if (count == 2 && Convert.ToInt32(result.AISequence) == 3 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox3.Checked = true;
                        }
                        else if (count == 3 && Convert.ToInt32(result.AISequence) == 4 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox4.Checked = true;
                        }
                        else  if (count == 4 && Convert.ToInt32(result.AISequence) == 5 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox5.Checked = true;
                        }
                        else if (count == 5 && Convert.ToInt32(result.AISequence) == 6 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox6.Checked = true;
                        }
                        else if (count == 6 && Convert.ToInt32(result.AISequence) == 7 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox7.Checked = true;
                            dvOthers.Attributes.Remove("Class");
                            dvOthers.Attributes.Add("Class", "divShow");                            
                        }
                        else if (count == 7 && Convert.ToInt32(result.AISequence) == 8 )
                        {
                            txtOtherID.Value = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        else if (count == 7 && Convert.ToInt32(result.AISequence) == 8)
                        {
                            txtdelayreasoning.Value = Convert.ToString(result.AIQuestionAnswerText);
                        } 
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.PersonalProtectiveEquipment).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }
                



                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID1.Value)))
                {
                    objList.Add(
                           new AIQuestionAnswer
                           {
                               AIQuestionID = Convert.ToInt32(hdQuestionID2.Value),
                               AIQuestionType = QuestionType.BooleanType,
                               AIQuestionAnswerText = Convert.ToString(hdQuestionAnsID1.Value) == "" ? ConfirmationYesNo.NotAnswered : Convert.ToString(hdQuestionAnsID1.Value),
                               AIFormID = aiFormID,
                               AIPageName = PageName.PersonalProtectiveEquipment,
                               AISequence = 1
                           });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID1.Value)) && Convert.ToString(hdQuestionAnsID1.Value) == "1")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID2.Value)))
                    {
                        objList.Add(
                             new AIQuestionAnswer
                             {
                                 AIQuestionID = Convert.ToInt32(hdQuestionID2.Value),
                                 AIQuestionType = QuestionType.BooleanType,
                                 AIQuestionAnswerText = Convert.ToString(chkbox1.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                                 AIFormID = aiFormID,
                                 AIPageName = PageName.PersonalProtectiveEquipment,
                                 AISequence =2
                             });
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID3.Value)))
                    {
                        objList.Add(
                            new AIQuestionAnswer
                            {
                                AIQuestionID = Convert.ToInt32(hdQuestionID3.Value),
                                AIQuestionType = QuestionType.BooleanType,
                                AIQuestionAnswerText = Convert.ToString(chkbox2.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                                AIFormID = aiFormID,
                                AIPageName = PageName.PersonalProtectiveEquipment,
                                AISequence = 3
                            });
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID4.Value)))
                    {
                        objList.Add(
                            new AIQuestionAnswer
                            {
                                AIQuestionID = Convert.ToInt32(hdQuestionID4.Value),
                                AIQuestionType = QuestionType.BooleanType,
                                AIQuestionAnswerText = Convert.ToString(chkbox3.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                                AIFormID = aiFormID,
                                AIPageName = PageName.PersonalProtectiveEquipment,
                                AISequence = 4
                            });
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID5.Value)))
                    {
                        objList.Add(
                            new AIQuestionAnswer
                            {
                                AIQuestionID = Convert.ToInt32(hdQuestionID5.Value),
                                AIQuestionType = QuestionType.BooleanType,
                                AIQuestionAnswerText = Convert.ToString(chkbox4.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                                AIFormID = aiFormID,
                                AIPageName = PageName.PersonalProtectiveEquipment,
                               AISequence = 5
                            });
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID6.Value)))
                    {
                           objList.Add(
                           new AIQuestionAnswer
                           {
                               AIQuestionID = Convert.ToInt32(hdQuestionID6.Value),
                               AIQuestionType = QuestionType.BooleanType,
                               AIQuestionAnswerText = Convert.ToString(chkbox5.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                               AIFormID = aiFormID,
                               AIPageName = PageName.PersonalProtectiveEquipment,
                               AISequence = 6

                           });
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID7.Value)))
                    {
                        objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdQuestionID7.Value),
                             AIQuestionType = QuestionType.BooleanType,
                             AIQuestionAnswerText = Convert.ToString(chkbox6.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
                             AIFormID = aiFormID,
                             AIPageName = PageName.PersonalProtectiveEquipment,
                             AISequence = 7
                         });
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID8.Value)))
                    {
                        objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdQuestionID8.Value),
                             AIQuestionType = QuestionType.BooleanType,
                             AIQuestionAnswerText = Convert.ToString(chkbox7.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
                             AIFormID = aiFormID,
                             AIPageName = PageName.PersonalProtectiveEquipment,
                             AISequence= 8
                         });

                        if (chkbox7.Checked == true)
                        {

                            //other-text-input
                            if (!string.IsNullOrEmpty(hdQuestionID9.Value))
                            {
                                objList.Add(
                                  new AIQuestionAnswer
                                  {
                                      AIQuestionID = Convert.ToInt32(hdQuestionID9.Value),
                                      AIQuestionType = QuestionType.TextType,
                                      AIQuestionAnswerText = Convert.ToString(txtOtherID.Value),
                                      AIFormID = aiFormID,
                                      AIPageName = PageName.PersonalProtectiveEquipment,
                                      AISequence = 9
                                       
                                  });
                            }
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID1.Value)) && Convert.ToString(hdQuestionAnsID1.Value) == "0")
                {
                    if (!string.IsNullOrEmpty(hdQuestionID10.Value))
                    {
                        objList.Add(
                          new AIQuestionAnswer
                          {
                              AIQuestionID = Convert.ToInt32(hdQuestionID10.Value),
                              AIQuestionType = QuestionType.TextType,
                              AIQuestionAnswerText = Convert.ToString(txtdelayreasoning.Value),
                              AIFormID = aiFormID,
                              AIPageName = PageName.PersonalProtectiveEquipment,
                              AISequence= 10
                          });
                    }
                }  

                Session["AIReportQuestions"] = objList;

                managePreviousNext("");

                RedirectionLink();
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {                
                var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (ISLicensee != null)
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    //Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }

                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }

                }
                else
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    //Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIContributingFactor.aspx", false);
                        return;
                    }
                }
            }

        }
        catch { }
        finally { }
        //List<ManagePagePreviousNext> objPageDetail = new List<ManagePagePreviousNext>();
        //objPageDetail = (List<ManagePagePreviousNext>)Session["AIPagesList"];
        //if (objPageDetail != null)
        //{
        //    var pageUrlResult1 = objPageDetail.FirstOrDefault(x => x.pageName == PageName.ContributingFactor);

        //    if (pageUrlResult1 != null)
        //    {
        //        //Response.Write(pageUrlResult1.currentPageUrl);
        //        Response.Redirect(Convert.ToString(pageUrlResult1.currentPageUrl));
        //    }
        //}

    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }
    private void managePreviousNext(string NextPageUrl)
    {
        List<ManagePagePreviousNext> objPage = new List<ManagePagePreviousNext>();
        try
        {
            objPage = (List<ManagePagePreviousNext>)Session["AIPagesList"];
            if (objPage != null)
            {
                var resultItem = objPage.Where(x => x.pageName == PageName.PersonalProtectiveEquipment).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objPage.Remove(removeItem);
                    }
                }
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIPersonalProtectiveEquipment.aspx", NextPageUrl = NextPageUrl, pageName = PageName.PersonalProtectiveEquipment, PreviousPageUrl = "" });
            }
            else
            {
                objPage = new List<ManagePagePreviousNext>();
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIPersonalProtectiveEquipment.aspx", NextPageUrl = NextPageUrl, pageName = PageName.PersonalProtectiveEquipment, PreviousPageUrl = "" });
            }
            Session["AIPagesList"] = objPage;
        }
        catch { }
        finally { }
    }
    private void RedirectionLink()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {

            if (objList != null)
            {

                var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (ISLicensee != null)
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }

                    //Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }

                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }



                }
                else
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }

                    //Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }

                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                }
            }
        }
        catch { }
        finally { }
    }

}