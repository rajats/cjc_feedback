﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
 
public partial class AIReports_AIIncidentConfirmation : System.Web.UI.Page
{

    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];

                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                        hdFirstName.Value = BusinessUtility.GetString(result.EmpFirstName);
                    }
                }
            }
        }
    }
    protected void btnPrevoius_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIIncidentExp.aspx");
    }  
    protected void btnYes_Click(object sender, EventArgs e)
    {
        // Redirect to exit or done(As per working Flow)            
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.Incident).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }
                 
                objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdIncidentConfirmQuestionID.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(ConfirmationYesNo.Yes),
                        AIFormID = aiFormID,
                        AIPageName = PageName.Incident
                    });
                Session["AIReportQuestions"] = objList;
                

                managePreviousNext("AISummary.aspx");
                Response.Redirect("AISummary.aspx");
            }
        }
        catch  {  }
        finally { }
        //Response.Redirect("AISummary.aspx", false);
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();  
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList !=  null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.Incident).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                } 
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }     
                objList.Add(
                    new AIQuestionAnswer 
                    {
                        AIQuestionID = Convert.ToInt32(hdIncidentConfirmQuestionID.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText =   Convert.ToString(ConfirmationYesNo.No),
                        AIFormID = aiFormID,
                        AIPageName = PageName.Incident
                    });
                Session["AIReportQuestions"] = objList;
                Response.Redirect("AIWVHDesc.aspx");
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBoard.aspx", false);
    }
    private void managePreviousNext(string NextPageUrl)
    {
        List<ManagePagePreviousNext> objPage = new List<ManagePagePreviousNext>();
        try
        {
            objPage = (List<ManagePagePreviousNext>)Session["AIPagesList"];
            if (objPage != null)
            {
                var resultItem = objPage.Where(x => x.pageName == PageName.Incident).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objPage.Remove(removeItem);
                    }
                }
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIIncidentConfirmation.aspx", NextPageUrl = NextPageUrl, pageName = PageName.Incident, PreviousPageUrl = "AIIncidentExp.aspx" });
            }
            else
            {
                objPage = new List<ManagePagePreviousNext>();
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIIncidentConfirmation.aspx", NextPageUrl = NextPageUrl, pageName = PageName.Incident, PreviousPageUrl = "AIIncidentExp.aspx" });
            }

            Session["AIPagesList"] = null;
            Session["AIPagesList"] = objPage;
        }
        catch( Exception ex) { }
        finally { }
    }
}