﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AIWVHManagerSection : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
                getFormData();
            }
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHManagerSection).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdManagerQID.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdManagerQID.Value),
                            AIQuestionType = QuestionType.TextType,
                            AIQuestionAnswerText = Convert.ToString(txtmanagersupervisorname.Value),
                            AIFormID = aiFormID,
                            AIPageName = PageName.WVHManagerSection,
                            AISequence = 1
                        });
                } 

                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHManagerQuestionID1.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdWVHManagerQuestionID1.Value),
                             AIQuestionType = QuestionType.BooleanType,
                             AIQuestionAnswerText = Convert.ToString(hdWVHManagerQuestionID1Ans.Value == "" ? ConfirmationYesNo.NotAnswered : Convert.ToString(hdWVHManagerQuestionID1Ans.Value)),
                             AIFormID = aiFormID,
                             AIPageName = PageName.WVHManagerSection,
                             AISequence = 2
                         });
                }
                if (Convert.ToString(hdWVHManagerQuestionID1Ans.Value) == "1")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(hdWVHManagerQuestionID2.Value)))
                    {
                        objList.Add(
                             new AIQuestionAnswer
                             {
                                 AIQuestionID = Convert.ToInt32(hdWVHManagerQuestionID2.Value),
                                 AIQuestionType = QuestionType.TextType,
                                 AIQuestionAnswerText = Convert.ToString(txtdelayreasons.Text),
                                 AIFormID = aiFormID,
                                 AIPageName = PageName.WVHManagerSection,
                                 AISequence = 3
                             });
                    }
                }

                Session["AIReportQuestions"] = objList;

                //managePreviousNext("");

                RedirectionLink();

            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        //List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        //objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            Response.Redirect("AIWVHComments.aspx", false);
            //var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
            //if (IsLicensee != null)
            //{
            //    Response.Redirect("AIWVHComments.aspx", false);
            //    return;
            //}
            //else
            //{
            //    Response.Redirect("AIWVHComments.aspx", false);
            //    return;
            //}
        }
        catch { }
        finally { }
    }
    protected void btnSaveAndExit_Click(object sender, EventArgs e)
    {
        
    }
    protected void btnSubmitmyReport_Click(object sender, EventArgs e)
    {

    }
    private void RedirectionLink()
    {
        //List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        //objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            Response.Redirect("AIWVHSupervisorSection.aspx", false); 
            //var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
            //if (IsLicensee != null)
            //{ 
            //    Response.Redirect("AIWVHSupervisorSection.aspx", false); 
            //    return;
            //}
            //else
            //{
            //    // Case 1:  Retail:Yes, WVH:Yes
            //    var IsRetailAndLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "R");
            //    var IsWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
            //    if ((IsRetailAndLogistic != null) && IsWVH != null)
            //    {
            //        Response.Redirect("AIWVHSupervisorSection.aspx", false); 
            //        return;
            //    }
            //    else
            //    {
            //        Response.Redirect("AIWVHSupervisorSection.aspx", false);
            //        return;
            //    }
            //} 
        }
        catch { }
        finally { }        
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHManagerSection).OrderBy(x => x.AISequence).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 1;
                    foreach (var result in resultItem)
                    {
                        if (count == 1)
                        {
                            txtmanagersupervisorname.Value = Convert.ToString(result.AIQuestionAnswerText);
                        } 
                        else if (count == 2 && Convert.ToString(result.AIQuestionAnswerText) == "1" && result.AISequence == 2)
                        {
                            ancAiQuestion1Yes.Attributes.Remove("Class");
                            ancAiQuestion1Yes.Attributes.Add("Class", "btnChageColor");
                            dvResaon.Attributes.Remove("Class");
                            dvResaon.Attributes.Add("Class", "divShow");                             
                        }
                        else if (count == 2 && Convert.ToString(result.AIQuestionAnswerText) == "0" && result.AISequence == 2)
                        {
                            ancAiQuestion1No.Attributes.Remove("Class");
                            ancAiQuestion1No.Attributes.Add("Class", "btnChageColor");
                        }
                        else if (count == 3 && result.AISequence == 3)
                        {
                            txtdelayreasons.Text = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }
    }

}