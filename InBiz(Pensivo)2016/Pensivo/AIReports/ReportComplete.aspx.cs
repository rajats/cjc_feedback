﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_ReportComplete : System.Web.UI.Page
{
    List<Employee> empObj = new List<Employee>();
    AIReportCls objReport = new AIReportCls();
    List<AIQuestionAnswer> objQuestionAns = new List<AIQuestionAnswer> ();
    AIEmailCls emailCls = new AIEmailCls();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            // insert data into database
            try
            {
                if (objReport.insertAIDetails((List<AIQuestionAnswer>)Session["AIReportQuestions"],  (List<Employee>)Session["EmployeeDetails"],  AIReportStatus.AIISCompletedYes))
                {

                    empObj = (List<Employee>)Session["EmployeeDetails"];
                    var initiatorDetail = empObj.FirstOrDefault();
                    lblInitiatorEmailID.Text = Convert.ToString(initiatorDetail.AIReportInitiatorEmailID);
                    CommonPrint objCommonPrint = new CommonPrint();

                    // Need to write the code to check Report Type

                    string aiReportFilePath = objCommonPrint.GetAIReportPrint(AIReportType.WVHOnly, (List<AIQuestionAnswer>)Session["AIReportQuestions"], (List<Employee>)Session["EmployeeDetails"]);

                    emailCls.SendAINotificationEmail((List<AIQuestionAnswer>)Session["AIReportQuestions"], (List<Employee>)Session["EmployeeDetails"],aiReportFilePath);


                }
                else
                {
                    //  Fail 
                }
            }
            catch( Exception ex) 
            {
                Response.Write(ex.ToString());            
            }
            finally{}

            // End Block
        }
    }
}