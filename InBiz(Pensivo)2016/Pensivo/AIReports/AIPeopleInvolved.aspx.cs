﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIPeopleInvolved : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();
        }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {           
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.PeopleInvolved).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 0;
                    foreach (var result in resultItem)
                    {
                        if (count == 0)
                        {
                            txtmanagersupervisor.Value = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        if (count == 1)
                        {
                            txthealthsafetyrepjhscmember.Value = Convert.ToString(result.AIQuestionAnswerText);
                        }

                        if (count == 2)
                        {
                            txtdmrodcmanagerco.Value = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        if (count == 3)
                        {
                            txtdirector.Value = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIWorkerComment.aspx", false);
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();         
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.PeopleInvolved).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID1.Value)))
                {

                objList.Add(
                     new AIQuestionAnswer
                     {
                         AIQuestionID = Convert.ToInt32(hdAIQuestionID1.Value),
                         AIQuestionType = QuestionType.TextType,
                         AIQuestionAnswerText = Convert.ToString(txtmanagersupervisor.Value),
                         AIFormID = aiFormID,
                         AIPageName = PageName.PeopleInvolved
                     });

                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID2.Value)))
                {
                    objList.Add(
                    new AIQuestionAnswer
                    {

                        AIQuestionID = Convert.ToInt32(hdAIQuestionID2.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = Convert.ToString(txthealthsafetyrepjhscmember.Value),
                        AIFormID = aiFormID,
                        AIPageName = PageName.PeopleInvolved
                    });

                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID3.Value)))
                {
                    objList.Add(
                    new AIQuestionAnswer
                    {

                        AIQuestionID = Convert.ToInt32(hdAIQuestionID3.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = Convert.ToString(txtdmrodcmanagerco.Value),
                        AIFormID = aiFormID,
                        AIPageName = PageName.PeopleInvolved
                    });

                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID4.Value)))
                {
                    objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdAIQuestionID4.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = Convert.ToString(txtdirector.Value),
                        AIFormID = aiFormID,
                        AIPageName = PageName.PeopleInvolved
                    });

                }

                Session["AIReportQuestions"] = objList;
                Response.Redirect("ReportComplete.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }

    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }
}