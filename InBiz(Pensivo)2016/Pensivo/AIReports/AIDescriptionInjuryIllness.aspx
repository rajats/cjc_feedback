﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIDescriptionInjuryIllness.aspx.cs" Inherits="AIReports_AIDescriptionInjuryIllness" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
   
     <script  type="text/javascript">
         function GetInjuryIllnessQuestions() {
             $.ajax({
                 type: "POST",
                 url: "CommonInterface.aspx/getQuestions",
                 data: "{pageType:'" + "InjuryIllness" + "'}",
                 dataType: "json",
                 contentType: "application/json; charset=utf-8",
                 success: function (data) {
                     var index = 0;
                     $.each(data.d, function (index, value) {
                         if (index == 0) {
                             $("#<%=hdInjuryIllnessQuest1.ClientID%>").val(value.QuestionID);
                             $("#InjuryIllnessQuest1").html(value.QuestionText);
                         }
                         if (index == 1) {
                             $("#<%=hdInjuryIllnessQuest2.ClientID%>").val(value.QuestionID);
                             $("#InjuryIllnessQuest2").html(value.QuestionText);
                         }
                         if (index == 2) {
                             $("#<%=hdInjuryIllnessQuest3.ClientID%>").val(value.QuestionID);
                             $("#InjuryIllnessQuest3").html(value.QuestionText);
                         }
                         if (index == 3) {
                             $("#<%=hdInjuryIllnessQuest4.ClientID%>").val(value.QuestionID);
                             $("#InjuryIllnessQuest4").html(value.QuestionText);
                         }
                         if (index == 4) {
                             $("#<%=hdInjuryIllnessQuest5.ClientID%>").val(value.QuestionID);
                             $("#InjuryIllnessQuest5").html(value.QuestionText);
                         }
                         if (index == 5) {
                             $("#<%=hdInjuryIllnessQuest6.ClientID%>").val(value.QuestionID);
                             $("#InjuryIllnessQuest6").html(value.QuestionText);
                         }
                         if (index == 6) {
                             $("#<%=hdInjuryIllnessQuest7.ClientID%>").val(value.QuestionID);
                             $("#InjuryIllnessQuest7").html(value.QuestionText);
                         }
                         if (index == 7) {
                             $("#<%=hdInjuryIllnessQuest8.ClientID%>").val(value.QuestionID);
                             $("#InjuryIllnessQuest8").html(value.QuestionText);
                         }
                         if (index == 8) {
                             $("#<%=hdInjuryIllnessQuest9.ClientID%>").val(value.QuestionID);
                             $("#InjuryIllnessQuest9").html(value.QuestionText);
                         }

               if (index == 9) {
                 $("#<%=hdInjuryIllnessQuest10.ClientID%>").val(value.QuestionID);
                 $("#InjuryIllnessQuest10").html(value.QuestionText);
               }
               if (index == 10) {
                 $("#<%=hdInjuryIllnessQuest11.ClientID%>").val(value.QuestionID);
               }
               else if (index == 11) {
                 $("#<%=hdInjuryIllnessQuest12.ClientID%>").val(value.QuestionID);
               }
               else if (index == 12) {
                 $("#<%=hdInjuryIllnessQuest13.ClientID%>").val(value.QuestionID);
               }

               index = parseInt(index) + 1;
             });
           },
           error: function (XMLHttpRequest, textStatus, errorThrown) {
             //alert( errorThrown );
           }
         });
       }
         $(document).ready(function () {
           GetInjuryIllnessQuestions();
           document.getElementById("ContentPlaceHolder1_dvOthers").style.display = "none";
         });

         function RemoveSelection(activeControl)
         {
           var sPrefix = 'ContentPlaceHolder1_';
           var sActiveId = '#' + sPrefix + activeControl;
           var sCtrlId = sPrefix + activeControl;

           if (sCtrlId == "ContentPlaceHolder1_chkOthers")
           {
             var bSelected = document.getElementById("ContentPlaceHolder1_chkOthers").checked;

             if (bSelected)
             {
               //document.getElementById("ContentPlaceHolder1_dvOthers").style.display = "block";
               $("#<%=dvOthers.ClientID %>").show();
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns11').val(1);
               document.getElementById("ContentPlaceHolder1_chkNone").checked = false;
               //alert($('#ContentPlaceHolder1_hdInjuryIllnessQuestAns11').val());
             }
             else
             {
               //document.getElementById("ContentPlaceHolder1_dvOthers").style.display = "none";
               $("#<%=dvOthers.ClientID %>").hide();
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns11').val(0);
             }
           }

           else if (sCtrlId == "ContentPlaceHolder1_chkNone")
           {
             var bSelected = document.getElementById("ContentPlaceHolder1_chkNone").checked;
             var sBlank = '';
             if (bSelected) {
               $('#dvBoxContent a').filter(function (index) {
                 $(this).removeClass("btnChageColor");
                 $(this).removeClass("selected");
                 $(this).addClass("btn grey");
               })
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns1').val(sBlank);
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns2').val(sBlank);
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns3').val(sBlank);
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns4').val(sBlank);
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns5').val(sBlank);
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns6').val(sBlank);
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns7').val(sBlank);
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns8').val(sBlank);
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns9').val(sBlank);
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns10').val(sBlank);
               document.getElementById("ContentPlaceHolder1_chkOthers").checked = false;
               $('#ContentPlaceHolder1_txtOthers').val(sBlank);
               //document.getElementById("ContentPlaceHolder1_dvOthers").style.display = "none";
               $("#<%=dvOthers.ClientID %>").hide();
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns13').val(1);
             }
             else
             {
               $('#ContentPlaceHolder1_hdInjuryIllnessQuestAns13').val(0);
             }
           }

         }


       function ChangeColor(control, activeControl, ctrlQuestion, ctrlAnswer)
       {
         var sPrefix = 'ContentPlaceHolder1_';
         var sActiveId = '#' + sPrefix + activeControl;
         var sQuestion = '#' + sPrefix + ctrlQuestion;
         var sAnswer = '#' + sPrefix + ctrlAnswer;
         var sText = control.text.toLowerCase();

         var sAnsLeft = '0';
         var sAnsRight = '1';

         if ($(sActiveId).hasClass("btnChageColor"))
         {
           $(sActiveId).removeClass('btnChageColor');
           $(sActiveId).addClass('btn grey');
           if (sText == "left")
           {
             $(sAnswer).val($(sAnswer).val().replace(sAnsLeft, ''));
             $(sAnswer).val($(sAnswer).val().replace(',', ''));
           }
           else if (sText == "right")
           {
             $(sAnswer).val($(sAnswer).val().replace(sAnsRight, ''));
             $(sAnswer).val($(sAnswer).val().replace(',', ''));
           }
           //alert($(sAnswer).val());
         }

         else if ($(sActiveId).hasClass("btn"))
         {
           $(sActiveId).removeClass('btn');
           $(sActiveId).removeClass('grey');
           $(sActiveId).addClass('btnChageColor');
           var bSelected = document.getElementById("ContentPlaceHolder1_chkNone").checked;
           if (bSelected)
           {
             document.getElementById("ContentPlaceHolder1_chkNone").checked = false;
           }
           if ($(sAnswer).val() == '')
           {
             if (sText == "left")
             {
               $(sAnswer).val(sAnsLeft);
             }
             else if (sText == "right")
             {
               $(sAnswer).val(sAnsRight);
             }
           }
           else
           {
             if (sText == "left")
             {
               $(sAnswer).val($(sAnswer).val() + ',' + sAnsLeft);
             }
             else if (sText == "right")
             {
               $(sAnswer).val($(sAnswer).val() + ',' + sAnsRight);
             }
           }
           //alert($(sAnswer).val());
         }

       }
     </script>
 
    <section id="main-content" class="pg-ca-preventions">
<span class="mask"></span>
<div class="wrapper">

	<div class="plms-alert secondary-clr">
		<p> 
             <%=Resources.Resource.lblAccidentIncident %>
             <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
             <asp:Label ID="lblEmpID" runat="server" /></span>)
        </p> 
	</div><!--End .plms-alert-->
	
 <div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		  <%=Resources.Resource.lblMandatoryField %></p>
	</div><!--End .plms-alert-->
 
	<div class="layout-sidebar-right">

		<div class="boxed-content" id="dvBoxContent">
			
			<div class="boxed-content-body">
				
			<h2>     <%=Resources.Resource.lblDescriptionIllnessTitle %></h2>
			
			<p class="small-text pull-up-10">     <%=Resources.Resource.lblDescriptionIllnessText %> </p>
			
			<div class="plms-fieldset-wrapper">
                 <asp:HiddenField  ID="hdInjuryIllnessQuest1"  runat="server"/> 
                 <asp:HiddenField  ID="hdInjuryIllnessQuestAns1"  runat="server"/> 
				<div class="column span-8">
					<p class="bold" id="InjuryIllnessQuest1"></p>
				</div><!--End .column-->				
				<div class="column span-4">                       
					<div class="btngrp radio-group align-r">
					<a id="ancAncleLeft" runat="server" class="btn grey" title="Yes" onclick="ChangeColor(this, 'ancAncleLeft', 'hdInjuryIllnessQuest1', 'hdInjuryIllnessQuestAns1');">Left</a>
					<a id="ancAncleRight" runat="server" class="btn grey" title="No" onclick="ChangeColor(this, 'ancAncleRight', 'hdInjuryIllnessQuest1', 'hdInjuryIllnessQuestAns1');">Right</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
			
			<div class="plms-fieldset-wrapper">
                <asp:HiddenField  ID="hdInjuryIllnessQuest2"  runat="server"/> 
                <asp:HiddenField  ID="hdInjuryIllnessQuestAns2"  runat="server"/> 
				<div class="column span-8">
					<p class="bold" id="InjuryIllnessQuest2"></p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp radio-group align-r">
					<a id="ancArmLeft" runat="server" href="#nogo" class="btn grey" title="Yes" onclick="ChangeColor(this, 'ancArmLeft', 'hdInjuryIllnessQuest2', 'hdInjuryIllnessQuestAns2');">Left</a>
					<a id="ancArmRight" runat="server" href="#nogo" class="btn grey" title="No" onclick="ChangeColor(this, 'ancArmRight', 'hdInjuryIllnessQuest2', 'hdInjuryIllnessQuestAns2');">Right</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
			
			<div class="plms-fieldset-wrapper">
                <asp:HiddenField  ID="hdInjuryIllnessQuest3"  runat="server"/> 
                <asp:HiddenField  ID="hdInjuryIllnessQuestAns3"  runat="server"/> 
				<div class="column span-8">
					<p class="bold" id="InjuryIllnessQuest3"> </p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp radio-group align-r">
					<a id="ancEarLeft" runat="server" href="#nogo" class="btn grey" title="Yes" onclick="ChangeColor(this, 'ancEarLeft', 'hdInjuryIllnessQuest3', 'hdInjuryIllnessQuestAns3');">Left</a>
					<a id="ancEarRight" runat="server" href="#nogo" class="btn grey" title="No" onclick="ChangeColor(this, 'ancEarRight', 'hdInjuryIllnessQuest3', 'hdInjuryIllnessQuestAns3');">Right</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
			
			<div class="plms-fieldset-wrapper">
                     <asp:HiddenField  ID="hdInjuryIllnessQuest4"  runat="server"/> 
                <asp:HiddenField  ID="hdInjuryIllnessQuestAns4"  runat="server"/> 
				<div class="column span-8">
					<p class="bold" id="InjuryIllnessQuest4">Eye</p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp radio-group align-r">
					<a id="ancEyeLeft" runat="server" href="#nogo" class="btn grey" title="Yes" onclick="ChangeColor(this, 'ancEyeLeft', 'hdInjuryIllnessQuest4', 'hdInjuryIllnessQuestAns4');">Left</a>
					<a id="ancEyeRight" runat="server" href="#nogo" class="btn grey" title="No" onclick="ChangeColor(this, 'ancEyeRight', 'hdInjuryIllnessQuest4', 'hdInjuryIllnessQuestAns4');">Right</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
			
			<div class="plms-fieldset-wrapper">
                 <asp:HiddenField  ID="hdInjuryIllnessQuest5"  runat="server"/> 
                <asp:HiddenField  ID="hdInjuryIllnessQuestAns5"  runat="server"/> 

				<div class="column span-8">
					<p class="bold" id="InjuryIllnessQuest5"> </p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp radio-group align-r">
					<a id="ancFaceLeft" runat="server" href="#nogo" class="btn grey" title="Yes" onclick="ChangeColor(this, 'ancFaceLeft', 'hdInjuryIllnessQuest5', 'hdInjuryIllnessQuestAns5');">Left</a>
					<a id="ancFaceRight" runat="server" href="#nogo" class="btn grey" title="No" onclick="ChangeColor(this, 'ancFaceRight', 'hdInjuryIllnessQuest5', 'hdInjuryIllnessQuestAns5');">Right</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
			

                
			<div class="plms-fieldset-wrapper">
                 <asp:HiddenField  ID="hdInjuryIllnessQuest6"  runat="server"/> 
                <asp:HiddenField  ID="hdInjuryIllnessQuestAns6"  runat="server"/> 

				<div class="column span-8">
					<p class="bold" id="InjuryIllnessQuest6"> </p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp radio-group align-r">
					<a id="ancFootLeft" runat="server" href="#nogo" class="btn grey" title="Yes" onclick="ChangeColor(this, 'ancFootLeft', 'hdInjuryIllnessQuest6', 'hdInjuryIllnessQuestAns6');">Left</a>
					<a id="ancFootRight" runat="server" href="#nogo" class="btn grey" title="No" onclick="ChangeColor(this, 'ancFootRight', 'hdInjuryIllnessQuest6', 'hdInjuryIllnessQuestAns6');">Right</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
			
                 <div class="plms-fieldset-wrapper">
                 <asp:HiddenField  ID="hdInjuryIllnessQuest7"  runat="server"/> 
                <asp:HiddenField  ID="hdInjuryIllnessQuestAns7"  runat="server"/> 

				<div class="column span-8">
					<p class="bold" id="InjuryIllnessQuest7"> </p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp radio-group align-r">
					<a id="ancHandLeft" runat="server" href="#nogo" class="btn grey" title="Yes" onclick="ChangeColor(this, 'ancHandLeft', 'InjuryIllnessQuest7', 'hdInjuryIllnessQuestAns7');">Left</a>
					<a id="ancHandRight" runat="server" href="#nogo" class="btn grey" title="No" onclick="ChangeColor(this, 'ancHandRight', 'InjuryIllnessQuest7', 'hdInjuryIllnessQuestAns7');">Right</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->


                <div class="plms-fieldset-wrapper">
                 <asp:HiddenField  ID="hdInjuryIllnessQuest8"  runat="server"/> 
                <asp:HiddenField  ID="hdInjuryIllnessQuestAns8"  runat="server"/> 

				<div class="column span-8">
					<p class="bold" id="InjuryIllnessQuest8"> </p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp radio-group align-r">
					<a id="ancHeadLeft" runat="server" href="#nogo" class="btn grey" title="Yes" onclick="ChangeColor(this, 'ancHeadLeft', 'hdInjuryIllnessQuest8', 'hdInjuryIllnessQuestAns8');">Left</a>
					<a id="ancHeadRight" runat="server" href="#nogo" class="btn grey" title="No" onclick="ChangeColor(this, 'ancHeadRight', 'hdInjuryIllnessQuest8', 'hdInjuryIllnessQuestAns8');">Right</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->




                      <div class="plms-fieldset-wrapper">
                 <asp:HiddenField  ID="hdInjuryIllnessQuest9"  runat="server"/> 
                <asp:HiddenField  ID="hdInjuryIllnessQuestAns9"  runat="server"/> 

				<div class="column span-8">
					<p class="bold" id="InjuryIllnessQuest9"> </p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp radio-group align-r">
					<a id="ancKneeLeft" runat="server" href="#nogo" class="btn grey" title="Yes" onclick="ChangeColor(this, 'ancKneeLeft', 'hdInjuryIllnessQuest9', 'hdInjuryIllnessQuestAns9');">Left</a>
					<a id="ancKneeRight" runat="server" href="#nogo" class="btn grey" title="No" onclick="ChangeColor(this, 'ancKneeRight', 'hdInjuryIllnessQuest9', 'hdInjuryIllnessQuestAns9');">Right</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->


       <div class="plms-fieldset-wrapper">
           <asp:HiddenField  ID="hdInjuryIllnessQuest10"  runat="server"/> 
            <asp:HiddenField  ID="hdInjuryIllnessQuestAns10"  runat="server"/> 

				<div class="column span-8">
					<p class="bold" id="InjuryIllnessQuest10"> </p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp radio-group align-r">
					<a id="ancShoulderLeft" runat="server" href="#nogo" class="btn grey" title="Yes" onclick="ChangeColor(this, 'ancShoulderLeft', 'hdInjuryIllnessQuest10', 'hdInjuryIllnessQuestAns10');">Left</a>
					<a id="ancShoulderRight" runat="server" href="#nogo" class="btn grey" title="No" onclick="ChangeColor(this, 'ancShoulderRight', 'hdInjuryIllnessQuest10', 'hdInjuryIllnessQuestAns10');">Right</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->



      <div class="plms-fieldset push-up label-checkbox-group">
        <asp:HiddenField  ID="hdInjuryIllnessQuest11"  runat="server"/> 
            <asp:HiddenField  ID="hdInjuryIllnessQuestAns11"  runat="server"/> 
			<label for="ContentPlaceHolder1_chkOthers" class="checkbox-input-label">
        <input type="checkbox" name="chkOthers" id="chkOthers" runat="server" onchange="RemoveSelection('chkOthers');">
			Others
			</label>
			</div>
       <div class="plms-fieldset push-up">
       </div>

        <div class="divHide" id="dvOthers" runat="server">
			<div class="plms-fieldset pull-up " >
        <asp:HiddenField  ID="hdInjuryIllnessQuest12"  runat="server"/> 
         <asp:HiddenField  ID="hdInjuryIllnessQuestAns12"  runat="server"/> 
			<label class="plms-label is-hidden" for="other">Other</label>
			<div class="plms-tooltip-parent">
			<input type="text" placeholder="Other" class="plms-input skin2" id="txtOthers" name="txtOthers" runat="server" />
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p>Other</p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
          </div>
			
			<div class="plms-fieldset push-up label-checkbox-group">
        <asp:HiddenField  ID="hdInjuryIllnessQuest13"  runat="server"/> 
         <asp:HiddenField  ID="hdInjuryIllnessQuestAns13"  runat="server"/> 
			<label for="ContentPlaceHolder1_chkNone" class="checkbox-input-label">
			<input type="checkbox" name="chkNone" id="chkNone" runat="server" onchange="RemoveSelection('chkNone');">
			None
			</label>
			</div><!--End .plms-fieldset-->
					
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">
		               <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"    />
                   <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"  />
			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span>Hide Menu</span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->			
			<nav class="aside-main-nav">
			 <ul>

                 	 <UC1Menu:UC1  ID="lblmenu"   runat="server"/>
			</ul>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">

	 <asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click"  /> 
     <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click"  />
         
    </div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->

</asp:Content>

