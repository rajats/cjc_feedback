﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AILicenseeDetails.aspx.cs" Inherits="AIReports_AILicenseeDetails" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../_styles/ai-reports.css" />     
    <script  type="text/javascript">
       function GetLicenseeQuestions() {
           $.ajax({
               type: "POST",
               url: "CommonInterface.aspx/getQuestions",
               data: "{pageType:'" + "LicenseeDetail" + "'}",
               dataType: "json",
               contentType: "application/json; charset=utf-8",
               success: function (data) {
                   var index = 0;
                   $.each(data.d, function (index, value)
                   {
                       if (index == 0)
                       {   
                           $("#<%=hdLicenseeQuestionID1.ClientID%>").val(value.QuestionID);                            
                           $("#lblLicenseeQuestionText1").html(value.QuestionText);
                           $("#lblLicenseeQuestionText1ToolTip").html(value.QuestionText);
                           $("#<%=txtLicenseeQuestion1Ans.ClientID%>").attr("placeholder", value.QuestionText);     
                            
                       }                      
                       if (index == 1)
                       {                          
                           $("#<%=hdLicenseeQuestionID2.ClientID%>").val(value.QuestionID);
                           $("#lblLicenseeQuestionText2").html(value.QuestionText);
                           $("#lblLicenseeQuestionTextTooltip").html(value.QuestionText);
                           $("#<%=txtLicenseeQuestion2Ans.ClientID%>").attr("placeholder", value.QuestionText);    
                       }

                       if (index == 2) {
                           $("#<%=hdLicenseeQuestionID3.ClientID%>").val(value.QuestionID);
                            $("#lblLicenseeQuestionText3").html(value.QuestionText);
                            $("#lblLicenseeQuestionText3Tooltip").html(value.QuestionText);
                            $("#<%=txtLicenseeQuestion3Ans.ClientID%>").attr("placeholder", value.QuestionText);
                       }


                       if (index == 3)
                       {
                           $("#<%=hdLicenseeQuestionID4.ClientID%>").val(value.QuestionID);
                           $("#lblLicenseeQuestionText4").html(value.QuestionText);                           
                       }
                       if (index == 4)
                       {                         
                           $("#<%=hdLicenseeQuestionID5.ClientID%>").val(value.QuestionID);
                           $("#lblLicenseeQuestionText5").html(value.QuestionText);
                           $("#lblLicenseeQuestionText5ToolTip").html(value.QuestionText);
                           $("#<%=txtLicenseeQuestion5Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                       }
                       index = parseInt(index) + 1; 
                    });
                 },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert( errorThrown );
                }
            });
        }
        $(document).ready(function ()
        {
          GetLicenseeQuestions();
          //document.getElementById('dvLicenseeQuestion').style.display = "none";
        });
        function getAnswer(controlID, value) 
        { 
            //$("#<%=hdLicenseeQuestion4Ans.ClientID%>").val(anchorText);
        }
     function ClickChange(ID, Value) {
         if ( Value == "Yes" )
         {
             $( "#<%=hdLicenseeQuestion4Ans.ClientID%>" ).val( "1" );                    
             $( "#<%=dvLicenseeQuestion.ClientID%>" ).hide();
         }
         else
         {
         $("#<%=hdLicenseeQuestion4Ans.ClientID%>").val("0");
           $( "#<%=dvLicenseeQuestion.ClientID%>" ).show();
            }
       if (ID == "anchYes") {
             $("#<%=anchYes.ClientID%>").removeClass('btn');
             $("#<%=anchYes.ClientID%>").addClass('btnChageColor');
             if ($("#<%=anchNo.ClientID%>").hasClass("btnChageColor")) {
               $("#<%=anchNo.ClientID%>").removeClass('btnChageColor');
               $("#<%=anchNo.ClientID%>").addClass('btn');
             }
           }
       if (ID == "anchNo") {
             $("#<%=anchNo.ClientID%>").removeClass('btn');
               $("#<%=anchNo.ClientID%>").addClass('btnChageColor');
               if ($("#<%=anchYes.ClientID%>").hasClass("btnChageColor")) {
                 $("#<%=anchYes.ClientID%>").removeClass('btnChageColor');
               $("#<%=anchYes.ClientID%>").addClass('btn');
             }
           }
         }
     </script>
<section id="main-content">
<span class="mask"></span>
<div class="wrapper">
	<div class="plms-alert secondary-clr"> 
        <p> 
               <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
        </p> 
	</div><!--End .plms-alert-->
	
<%--	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		  <%=Resources.Resource.lblMandatoryField %>

		</p>
	</div><!--End .plms-alert-->--%>

	<div class="layout-sidebar-right">

		<div class="boxed-content">
			
			<div class="boxed-content-body">
				
			<h2><%=Resources.Resource.lblDetails %></h2>
			
			<div class="plms-fieldset pull-up-10">
            <asp:HiddenField  ID="hdLicenseeQuestionID1"  runat="server"/>   
			<label class="plms-label is-hidden" for="licensee-name" id="lblLicenseeQuestionText1"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtLicenseeQuestion1Ans" runat="server" CssClass="plms-input skin2" >
            </asp:TextBox>  
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="lblLicenseeQuestionText1ToolTip">  </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			
                  
			<div class="plms-fieldset">
            <asp:HiddenField  ID="hdLicenseeQuestionID2"  runat="server"/> 
			<label class="plms-label is-hidden" for="licensee-address" id="lblLicenseeQuestionText2"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtLicenseeQuestion2Ans" runat="server" CssClass="plms-input skin2" >
            </asp:TextBox> 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="lblLicenseeQuestionTextTooltip"></p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
                 

                	
			<div class="plms-fieldset pull-up-10"> 
            <asp:HiddenField  ID="hdLicenseeQuestionID3"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblLicenseeQuestionText3"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtLicenseeQuestion3Ans" runat="server" CssClass="filter-key plms-input skin2" >
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblLicenseeQuestionText3ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->


			<div style="clear:both; height:20px;">

			</div>
			<div class="plms-fieldset-wrapper">
				<i class="req-icon" title="Mandatory Field">*</i>
				<div class="column span-8">
                      <asp:HiddenField  ID="hdLicenseeQuestionID4"  runat="server"/> 
					<p class="bold" id="lblLicenseeQuestionText4"></p>
				</div><!--End .column-->
				
				<div class="column span-4">
                    <asp:HiddenField id="hdLicenseeQuestion4Ans"  runat="server"/>
					<div class="btngrp align-r">
					<a id = "anchYes"  class="btn" title="Yes"  onclick="getAnswer('anchYes',  1);ClickChange('anchYes', this.text);"  runat="server">Yes</a>
					<a id ="anchNo"   class="btn" title="No" onclick="getAnswer('anchNo', 0);ClickChange('anchNo', this.text);"  runat="server">No</a>

					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->  
			
            <div class="divHide"  id="dvLicenseeQuestion"  runat="server">
            <div class="plms-fieldset pull-up-10" > 
            <asp:HiddenField  ID="hdLicenseeQuestionID5"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblLicenseeQuestionText5"></label>
			<div class="plms-tooltip-parent">
            <i class="req-icon" title="Mandatory Field">*</i>
            <asp:TextBox ID="txtLicenseeQuestion5Ans" runat="server" CssClass="filter-key plms-input skin2" TextMode="MultiLine">
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblLicenseeQuestionText5ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->					
			</div><!--End .boxed-content-body-->  
            </div>

            
			



			<nav class="pagination-nav">
                <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"    />
                        <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"  />

 			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span><%= Resources.Resource.lblHideMenu %></span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">		 
			      <UC1Menu:UC1  ID="lblmenu"   runat="server"/>   
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
  
        <asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click" />
         <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click" />
         
	</div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
     
</asp:Content>

