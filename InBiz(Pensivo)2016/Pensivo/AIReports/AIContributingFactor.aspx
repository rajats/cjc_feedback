﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIContributingFactor.aspx.cs" Inherits="AIReports_AIContributingFactor" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../_styles/ai-reports.css" />
       <script   type="text/javascript">
      function GetAIContributingFactor()
      {
          $.ajax( {
              type: "POST",
              url: "CommonInterface.aspx/getQuestions",
              data: "{pageType:'" + "ContributingFactor" + "'}",
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function ( data )
              {
                  var index = 0;
                  $.each( data.d, function ( index, value )
                  {
                      if ( index == 0 )
                      {
                          $( "#<%=hdQuestionID1.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText1" ).html( value.QuestionText );
                      }
                      if ( index == 1 )
                      {
                          $( "#<%=hdQuestionID2.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText2" ).html( value.QuestionText );
                      }

                      if ( index == 2 )
                      {
                          $( "#<%=hdQuestionID3.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText3" ).html( value.QuestionText );
                      }
                      if ( index == 3 )
                      {
                          $( "#<%=hdQuestionID4.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText4" ).html( value.QuestionText );
                      }

                      if ( index == 4 )
                      {
                          $( "#<%=hdQuestionID5.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText5" ).html( value.QuestionText );
                      }
                      if ( index == 5 )
                      {
                          $( "#<%=hdQuestionID6.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText6" ).html( value.QuestionText );
                      }
                      if ( index == 6 )
                      {
                          $( "#<%=hdQuestionID7.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText7" ).html( value.QuestionText );
                      }
                      if ( index == 7 )
                      {
                          $( "#<%=hdQuestionID8.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText8" ).html( value.QuestionText );
                      }

                      if ( index == 8 )
                      {
                          $( "#<%=hdQuestionID9.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText9" ).html( value.QuestionText );
                      }

                      if ( index == 9 )
                      {
                          $( "#<%=hdQuestionID10.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText10" ).html( value.QuestionText );
                      }
                      if ( index == 10 )
                      {
                          $( "#<%=hdQuestionID11.ClientID%>" ).val( value.QuestionID );
                           $( "#QuestionIDText11" ).html( value.QuestionText );
                      }


                      if ( index == 11 )
                      {
                          $( "#<%=hdQuestionID12.ClientID%>" ).val( value.QuestionID );
                             $( "#QuestionIDText12" ).html( value.QuestionText );
                      }
                      if ( index == 12 )
                      {
                          $( "#<%=hdQuestionID13.ClientID%>" ).val( value.QuestionID );
                             $( "#QuestionIDText13" ).html( value.QuestionText );
                      }

                      if ( index == 13 )
                      {
                          $( "#<%=hdQuestionID14.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText14" ).html( value.QuestionText );
                      }

                      if ( index == 14 )
                      {
                          $( "#<%=hdQuestionID15.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText15" ).html( value.QuestionText );
                      }
                      if ( index == 15 )
                      {
                          $( "#<%=hdQuestionID16.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText16" ).html( value.QuestionText );
                      }
                      if ( index == 16 )
                      {
                          $( "#<%=hdQuestionID17.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText17" ).html( value.QuestionText );
                      }

                      if ( index == 17 )
                      {
                          $( "#<%=hdQuestionID18.ClientID%>" ).val( value.QuestionID );
                           $( "#QuestionIDText18" ).html( value.QuestionText );
                      }


                      if ( index == 18 )
                      {
                          $( "#<%=hdQuestionID19.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText19" ).html( value.QuestionText );
                      }
                       
                      if ( index == 19 )
                      {
                          $( "#<%=hdQuestionID20.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText20" ).html( value.QuestionText );
                      }
                      if( index  == 20)
                      {
                          $( "#<%=hdQuestionID21.ClientID%>" ).val( value.QuestionID );
                          $( "#hdOtherQuestionText" ).html( value.QuestionText );
                          $( "#hdOtherQuestionTextToolTip" ).html( value.QuestionText );
                          $( "#<%=txtother.ClientID%>" ).attr( "placeholder", value.QuestionText );   
                      }
                        


                      index = parseInt( index ) + 1;
                  } );
              },
              error: function ( XMLHttpRequest, textStatus, errorThrown )
              {
                   //alert( errorThrown );
              }
            });
         }
         $( document ).ready( function ()
         {
           GetAIContributingFactor();           
         });

         function validate() {
           var bSelected = document.getElementById("ContentPlaceHolder1_chkbox20").checked;

           if (bSelected) {
             //document.getElementById('dvOthers').style.display = "block";
             $("#<%=dvOthers.ClientID %>").show();
           }
           else {
             //document.getElementById('dvOthers').style.display = "none";
             $("#<%=dvOthers.ClientID %>").hide();
           }
         }
          </script> 
    
  <section id="main-content">
<span class="mask"></span>
<div class="wrapper">
	<div class="plms-alert secondary-clr">
	     <p> 
             <%=Resources.Resource.lblAccidentIncident %>
               <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
         </p>
	</div><!--End .plms-alert-->

    
	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		  <%=Resources.Resource.lblMandatoryField %></p>
	</div>  

    <!--End .plms-alert--> 
	<div class="layout-sidebar-right">
		<div class="boxed-content">			
			<div class="boxed-content-body">				
				<h2> <%=Resources.Resource.lblContributingFactor %></h2>				
				<div class="fieldset-group">
				<p class="bold">   <%=Resources.Resource.lblContributingFactor1 %> </p>				
				<div class="label-checkbox-group">
				  <asp:HiddenField   ID="hdQuestionID1"  runat="server"/>
				<label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox1" runat="server"><input type="checkbox" id="chkbox1" name="location-type"  runat="server" /><span id ="QuestionIDText1"></span></label>		
                      <asp:HiddenField   ID="hdQuestionID2"  runat="server"/>
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox2"><input type="checkbox" id="chkbox2" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText2"></span></label>
				      <asp:HiddenField   ID="hdQuestionID3"  runat="server"/>
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox3"><input type="checkbox" id="chkbox3" name="arrangement-congestion"  runat="server" /><span id ="QuestionIDText3"></span></label>
		              <asp:HiddenField   ID="hdQuestionID4"  runat="server"/>
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox4"><input type="checkbox" id="chkbox4" name="arrangement-congestion"  runat="server"  /><span id ="QuestionIDText4"></span></label>
		               <asp:HiddenField   ID="hdQuestionID5"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox5"><input type="checkbox" id="chkbox5" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText5"></span></label>
		               <asp:HiddenField   ID="hdQuestionID6"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox6"><input type="checkbox" id="chkbox6" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText6"></span></label>
		                 <asp:HiddenField   ID="hdQuestionID7"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox7"><input type="checkbox" id="chkbox7" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText7"></span></label>
		                 <asp:HiddenField   ID="hdQuestionID8"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox8"><input type="checkbox" id="chkbox8" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText8"></span></label>
		             <asp:HiddenField   ID="hdQuestionID9"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox9"><input type="checkbox" id="chkbox9" name="arrangement-congestion"  runat="server"  /><span id ="QuestionIDText9"></span></label>
                     <asp:HiddenField   ID="hdQuestionID10"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox10"><input type="checkbox" id="chkbox10" name="arrangement-congestion"  runat="server"  /><span id ="QuestionIDText10"></span></label>
		            <asp:HiddenField   ID="hdQuestionID11"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox11"><input type="checkbox" id="chkbox11" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText11"></span></label>
		            <asp:HiddenField   ID="hdQuestionID12"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox12"><input type="checkbox" id="chkbox12" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText12"></span></label>
		            <asp:HiddenField   ID="hdQuestionID13"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox13"><input type="checkbox" id="chkbox13" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText13"></span></label>
		                 <asp:HiddenField   ID="hdQuestionID14"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox14"><input type="checkbox" id="chkbox14" name="arrangement-congestion"  runat="server"  /><span id ="QuestionIDText14"></span></label>
		            <asp:HiddenField   ID="hdQuestionID15"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox15"><input type="checkbox" id="chkbox15" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText15"></span></label>
		            <asp:HiddenField   ID="hdQuestionID16"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox16"><input type="checkbox" id="chkbox16" name="arrangement-congestion"  runat="server"    /><span id ="QuestionIDText16"></span></label>
		             <asp:HiddenField    ID="hdQuestionID17"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox17"><input type="checkbox" id="chkbox17" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText17"></span></label>
		                <asp:HiddenField    ID="hdQuestionID18"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox18"><input type="checkbox" id="chkbox18" name="arrangement-congestion"  runat="server" /><span id ="QuestionIDText18"></span></label>
		                 <asp:HiddenField    ID="hdQuestionID19"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox19"><input type="checkbox" id="chkbox19" name="arrangement-congestion"  runat="server"   /><span id ="QuestionIDText19"></span></label>
		                        <asp:HiddenField    ID="hdQuestionID20"  runat="server" />
                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox20"><input type="checkbox" id="chkbox20" name="arrangement-congestion"  runat="server" onchange="validate();" /><span id ="QuestionIDText20"></span></label>
		                 
                </div><!--End .label-checkbox-group-->
				</div><!--End .fieldset-group-->
					
                <div   id="dvOthers" runat="server"  class="divHide">

			    <div class="plms-fieldset"  >
                    <asp:HiddenField    ID="hdQuestionID21"  runat="server" />
				<label class="plms-label is-hidden" for="other-text-input"  id="hdOtherQuestionText"> </label>
				<div class="plms-tooltip-parent">
				<i class="req-icon" title="Mandatory Field">*</i>
				<input type="text"  class="plms-input skin2" id="txtother" name="other-text-input"   runat="server"/>
				<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
				<div class="plms-tooltip-body">
				<p id="hdOtherQuestionTextToolTip"> </p>
				</div><!--End .plms-tooltip-body-->
				</div><!--End .plms-tooltip-->
				</div><!--End .plms-tooltip-parent-->
				</div><!--End .plms-fieldset-->
           

                </div>
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">		
                    <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"    />
                    <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"   />
                 
        <%--        <a href="ai-reports-22.php" title="Previous" class="btn large">Previous</a>
			  <a href="ai-reports-24-1.php" title="Next" class="btn large align-r">Next</a>--%>
			</nav><!--End .pagination-nav-->		
		</div><!--End .boxed-content-->
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span><%=Resources.Resource.lblHideMenu %></span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
			 	        	 <UC1Menu:UC1  ID="lblmenu"   runat="server"/>

			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
           <asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click"    /> 
           <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click"   />
          
	</div>
	</footer>
</div><!--End .wrapper-->
</section><!--End #main-content-->
</asp:Content>

