﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIGetGeneralInformation.aspx.cs" Inherits="AIReports_AIGetGeneralInformation" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%> 
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"> 
     <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	 <link rel="stylesheet" href="../_styles/ai-reports.css" />
     
      <script   type="text/javascript">
          function GetGenralInfoQuestions()
          {
              $.ajax( {
                  type: "POST",
                  url: "CommonInterface.aspx/getQuestions",
                  data: "{pageType:'" + "GetGeneralInformation" + "'}",
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  success: function ( data )
                  {
                      var index = 0;
                      $.each( data.d, function ( index, value )
                      {
                          if ( index == 0 )
                          {
                              $( "#<%=hdQuestionID1.ClientID%>" ).val( value.QuestionID );
                              $( "#QuestionText1ToolTip" ).html( value.QuestionText );
                              $( "#<%=txtLastName.ClientID%>" ).attr( "placeholder", value.QuestionText );
                              $( "#QuestionText1" ).html( value.QuestionText );
                         }
                         if ( index == 1 )
                         {
                             $( "#<%=hdQuestionID2.ClientID%>" ).val( value.QuestionID );
                             $( "#QuestionText2ToolTip" ).html( value.QuestionText );
                             $( "#<%=txtFirstName.ClientID%>" ).attr( "placeholder", value.QuestionText );
                             $( "#QuestionText2" ).html( value.QuestionText );
                         }


                          if ( index == 2)
                          {
                              $( "#<%=hdQuestionID3.ClientID%>" ).val( value.QuestionID );
                              $( "#QuestionText3ToolTip" ).html( value.QuestionText );
                              $( "#QuestionText3" ).html( value.QuestionText );
                                $( "#<%=txtPhoneNo.ClientID%>" ).attr( "placeholder", value.QuestionText );

                         }
                          if ( index == 3 )
                          {
                              $( "#<%=hdQuestionID4.ClientID%>" ).val( value.QuestionID );
                              $( "#QuestionText4ToolTip" ).html( value.QuestionText );
                              $( "#QuestionText4" ).html( value.QuestionText );
                                $( "#<%=txtAddress.ClientID%>" ).attr( "placeholder", value.QuestionText );

                            }
                           
                           

                         index = parseInt( index ) + 1;
                     } );
                 },
                 error: function ( XMLHttpRequest, textStatus, errorThrown )
                 {
                     //alert( errorThrown );
                 }
             } );
         }
         $( document ).ready( function ()
         {
             GetGenralInfoQuestions();
         } );
          </script>

    <section id="main-content">
<span class="mask"></span>
<div class="wrapper">

	<div class="plms-alert secondary-clr">
		   <p> 
               <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
            </p>
	</div><!--End .plms-alert-->	
 	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
	  <%=Resources.Resource.lblMandatoryField %></p>
	</div><!--End .plms-alert--> 

	<div class="layout-sidebar-right">
		<div class="boxed-content">			
			<div class="boxed-content-body">
			<h2><%=Resources.Resource.lblGeneralInfo %></h2>	
                		
            <asp:HiddenField  ID="hdQuestionID1"  runat="server"/>
			<div class="plms-fieldset pull-up-10">
			<label class="plms-label is-hidden" for="last-name" id="QuestionText1"></label>
			<div class="plms-tooltip-parent">
        <i class="req-icon" title="Mandatory Field">*</i>
			<input id ="txtLastName" type="text" placeholder="Last Name" class="plms-input skin2"   name="last-name"  runat="server" />

			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="QuestionText1ToolTip"></p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			
			<div class="plms-fieldset">
                 <asp:HiddenField  ID="hdQuestionID2"  runat="server"/>                
			<label class="plms-label is-hidden" for="first-name"  id ="QuestionText2"></label>
			<div class="plms-tooltip-parent">
        <i class="req-icon" title="Mandatory Field">*</i>
			<input id ="txtFirstName" type="text" placeholder="First Name" class="plms-input skin2"  runat="server" name="first-name" />
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="QuestionText2ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			
			<div class="plms-fieldset">
                   <asp:HiddenField  ID="hdQuestionID3"  runat="server"/>  
			<label class="plms-label is-hidden" for="phone-number"  id ="QuestionText3"> </label>
			<div class="plms-tooltip-parent">
        <i class="req-icon" title="Mandatory Field">*</i>
			<input type="text" id="txtPhoneNo"   runat="server"   class="plms-input skin2"  name="phone-number" />
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="QuestionText3ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			
			<div class="plms-fieldset">
                 <asp:HiddenField  ID="hdQuestionID4"  runat="server"/>  
			<label class="plms-label is-hidden" for="address"  id = "QuestionText4"></label>
			<div class="plms-tooltip-parent">
        <i class="req-icon" title="Mandatory Field">*</i>
			<textarea  id="txtAddress" rows="4" cols="4" class="plms-textarea height-small skin2"  runat="server" name="address" placeholder="Address"></textarea>
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="QuestionText4ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
					
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">
                <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />
                <asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevious  %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large" OnClick="btnPrevious_Click"/>
                 
            </nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span><%=Resources.Resource.lblHideMenu %></span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
		  <ul>
<%--			<li class="is-active"><a id="A1" href="AIGeneralInformation.aspx" title=""  runat="server"><%= Resources.Resource.lblGenInfoPage %></a></li>
			<li><a id="A2" href="AiDetails.aspx" title=""  runat="server"><%= Resources.Resource.lblDetailPage %> </a></li>
			<li><a id="A3" href="AIAccidentIncidentDetail.aspx" title=""  runat="server">  <%= Resources.Resource.lblAccidentIncidentdetailPage %>   </a></li>
			<li><a id="A4" href="AIContributingFactor.aspx" title=""  runat="server">   <%= Resources.Resource.lblcontributingFactorPage %> </a></li>
			<li><a id="A5" href="AIActionAndPrevention.aspx" title="" runat="server">  <%= Resources.Resource.lblAiCorrectionActionPage %>   </a></li>
			<li><a id="A6" href="AIWitnessesCoWorker.aspx" title=""  runat="server">  <%= Resources.Resource.lblCoworkerPage %></a></li>
			<li><a id="A7" href="AIWorkerComment.aspx" title="" runat="server">  <%= Resources.Resource.lblWorkerCommentPage %> </a></li>
			<li><a id="A8" href="AIPeopleInvolved.aspx" title=""  runat="server">  <%= Resources.Resource.lblPeopleInvolvedPage %>   </a></li>
			</ul>--%>
			      <UC1Menu:UC1  ID="lblmenu"   runat="server"/> 

			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
         
        <asp:Button  ID="btnSaveAndExit"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater  %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn large" OnClick="btnSaveAndExit_Click"     />

        <asp:Button  ID="btnSubmitmyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport  %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn large align-r" OnClick="btnSubmitmyReport_Click"     />
         
	</div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
     <script  type ="text/javascript">
         ( function ()
         {
             $( ".datepicker" ).datepicker( {
                 dateFormat: "yy-mm-dd",
                 changeMonth: true,
                 changeYear: true,
                 yearRange: GetDateRange(),
                 maxDate: '+0D'
             });
         })();

         function GetDateRange()
         {
             var currentDate = new Date();
             var currentYear = currentDate.getFullYear();
             var previousYear = currentDate.getFullYear() - parseInt(<%= Resources.Resource.lblYearRange %> );
             return previousYear + ':' + currentYear;
         }
</script>
  
</asp:Content>

