﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIGeneralPersonalInfo.aspx.cs" Inherits="AIReports_AIGeneralPersonalInfo" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../_styles/ai-reports.css" />
  <script  type="text/javascript">
      function GetGeneralInfoQuestions() {
          $.ajax({
              type: "POST",
              url: "CommonInterface.aspx/getQuestions",
              data: "{pageType:'" + "WVHGeneralPersonalInfo" + "'}",
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  var index = 0;
                  $.each(data.d, function (index, value) {
                      if (index == 0) {
                          $("#<%=hdGeneralInfoQuestionID1.ClientID%>").val(value.QuestionID);
                          $("#lblGeneralInfoQuestionText1").html(value.QuestionText);
                          $("#lblGeneralInfoQuestionText1ToolTip").html(value.QuestionText);
                          $("#<%=txtGeneralInfoQuestion1Ans.ClientID%>").attr("placeholder", value.QuestionText);                          
                       }
                       if (index == 1) {

                           $("#<%=hdGeneralInfoQuestionID2.ClientID%>").val(value.QuestionID);
                           $("#lblGeneralInfoQuestionText2").html(value.QuestionText);
                           $("#lblGeneralInfoQuestionText2ToolTip").html(value.QuestionText);
                           $("#<%=txtGeneralInfoQuestion2Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                       }
                       if (index == 2) {
                           $("#<%=hdGeneralInfoQuestionID3.ClientID%>").val(value.QuestionID);
                           $("#lblGeneralInfoQuestionText3").html(value.QuestionText);
                           $("#lblGeneralInfoQuestionText3ToolTip").html(value.QuestionText);
                           $("#<%=txtGeneralInfoQuestion3Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                       }
                       if (index == 3) {
                           $("#<%=hdGeneralInfoQuestionID4.ClientID%>").val(value.QuestionID);
                           $("#lblGeneralInfoQuestionText4").html(value.QuestionText);
                           $("#lblGeneralInfoQuestionText4ToolTip").html(value.QuestionText);
                           $("#<%=txtGeneralInfoQuestion4Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                       }
                      if (index == 4) {
                          $("#<%=hdGeneralInfoQuestionID5.ClientID%>").val(value.QuestionID);
                          $("#lblGeneralInfoQuestionText5").html(value.QuestionText);
                          $("#lblGeneralInfoQuestionText5ToolTip").html(value.QuestionText);
                          $("#<%=txtGeneralInfoQuestion5Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                      }
                      if (index == 5) {
                          $("#<%=hdGeneralInfoQuestionID6.ClientID%>").val(value.QuestionID);
                          $("#lblGeneralInfoQuestionText6").html(value.QuestionText);
                          $("#lblGeneralInfoQuestionText6ToolTip").html(value.QuestionText);
                          $("#<%=txtGeneralInfoQuestion6Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                      }
                       index = parseInt(index) + 1;
                   });
               },
               error: function (XMLHttpRequest, textStatus, errorThrown) {
                   //alert( errorThrown );
               }
           });
       }
       $(document).ready(function () {
           GetGeneralInfoQuestions();
       });      
     </script>  
    <section id="main-content">
<span class="mask"></span>
<div class="wrapper">
	<div class="plms-alert secondary-clr"> 
        <p> 
               <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
        </p> 
	</div><!--End .plms-alert-->
	
 <div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		  <%=Resources.Resource.lblMandatoryField %>
		</p>
	</div><!--End .plms-alert--> 

	<div class="layout-sidebar-right">

		<div class="boxed-content">
			
			<div class="boxed-content-body">
				
			<h2><%=Resources.Resource.lblReportingEmployee %></h2>
			
			<div class="plms-fieldset pull-up-10">
            <asp:HiddenField  ID="hdGeneralInfoQuestionID1"  runat="server"/>   
			<label class="plms-label is-hidden" for="licensee-name" id="lblGeneralInfoQuestionText1"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtGeneralInfoQuestion1Ans" runat="server" CssClass="plms-input skin2" >
            </asp:TextBox>  
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="lblGeneralInfoQuestionText1ToolTip">  </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			
                  
			<div class="plms-fieldset">
            <asp:HiddenField  ID="hdGeneralInfoQuestionID2"  runat="server"/> 
			<label class="plms-label is-hidden" for="licensee-address" id="lblGeneralInfoQuestionText2"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtGeneralInfoQuestion2Ans" runat="server" CssClass="plms-input skin2" >
            </asp:TextBox> 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="lblGeneralInfoQuestionText2ToolTip"></p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
                 

                	
			<div class="plms-fieldset pull-up-10"> 
            <asp:HiddenField  ID="hdGeneralInfoQuestionID3"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblGeneralInfoQuestionText3"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtGeneralInfoQuestion3Ans" runat="server" CssClass="filter-key plms-input skin2" >
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblGeneralInfoQuestionText3ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->


			 
			<div class="plms-fieldset pull-up-10"> 
            <asp:HiddenField  ID="hdGeneralInfoQuestionID4"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblGeneralInfoQuestionText4"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtGeneralInfoQuestion4Ans" runat="server" CssClass="filter-key plms-input skin2">
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblGeneralInfoQuestionText4ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->



            
            <div class="plms-fieldset pull-up-10"> 
            <asp:HiddenField  ID="hdGeneralInfoQuestionID5"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblGeneralInfoQuestionText5"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtGeneralInfoQuestion5Ans" runat="server" CssClass="filter-key plms-input skin2">
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblGeneralInfoQuestionText5ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->



            
            <div class="plms-fieldset pull-up-10"> 
            <asp:HiddenField  ID="hdGeneralInfoQuestionID6"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblGeneralInfoQuestionText6"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtGeneralInfoQuestion6Ans" runat="server" CssClass="filter-key plms-input skin2">
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblGeneralInfoQuestionText6ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
					
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">

                <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"    />
                        <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"  />

		<%--	<a href="ai-reports-21-6.php" title="Previous" class="btn large">Previous</a>
			<a href="ai-reports-22-1.php" title="Next" class="btn large align-r">Next</a>--%>
			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span><%=Resources.Resource.lblHideMenu %></span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">			
    <%--       <ul>
			<li class="is-active"><a id="A1" href="AIGeneralInformation.aspx" title=""  runat="server"><%= Resources.Resource.lblGenInfoPage %></a></li>
			<li><a id="A2" href="AiDetails.aspx" title=""  runat="server"><%= Resources.Resource.lblDetailPage %> </a></li>
			<li><a id="A3" href="AIAccidentIncidentDetail.aspx" title=""  runat="server">  <%= Resources.Resource.lblAccidentIncidentdetailPage %>   </a></li>
			<li><a id="A4" href="AIContributingFactor.aspx" title=""  runat="server">   <%= Resources.Resource.lblcontributingFactorPage %> </a></li>
			<li><a id="A5" href="AIActionAndPrevention.aspx" title="" runat="server">  <%= Resources.Resource.lblAiCorrectionActionPage %>   </a></li>
			<li><a id="A6" href="AIWitnessesCoWorker.aspx" title=""  runat="server">  <%= Resources.Resource.lblCoworkerPage %></a></li>
			<li><a id="A7" href="AIWorkerComment.aspx" title="" runat="server">  <%= Resources.Resource.lblWorkerCommentPage %> </a></li>
			<li><a id="A8" href="AIPeopleInvolved.aspx" title=""  runat="server">  <%= Resources.Resource.lblPeopleInvolvedPage %>   </a></li>
			</ul>--%>

			      <UC1Menu:UC1  ID="lblmenu"   runat="server"/>

			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
  
        <%--<asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click" />--%>
         <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click" />
         
	</div>
	</footer>

</div><!--End .wrapper-->
</section>
</asp:Content>

