﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIPeopleInvolved.aspx.cs" Inherits="AIReports_AIPeopleInvolved" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="../_styles/ai-reports.css" />

    <script type="text/javascript">
        function GetAIPeopleInvolved()
        {
            $.ajax( {
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "PeopleInvolved" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function ( data )
                {
                    var index = 0;
                    $.each( data.d, function ( index, value )
                    {
                        if ( index == 0 )
                        {
                            $( "#<%=hdAIQuestionID1.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionIDText1ToolTip" ).html( value.QuestionText );
                             $( "#<%=txtmanagersupervisor.ClientID%>" ).attr( "placeholder", value.QuestionText );
                             $( "#AIQuestionIDText1" ).html( value.QuestionText );
                         }
                         if ( index == 1 )
                         {
                             $( "#<%=hdAIQuestionID2.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionIDText2ToolTip" ).html( value.QuestionText );
                             $( "#<%=txthealthsafetyrepjhscmember.ClientID%>" ).attr( "placeholder", value.QuestionText );
                              $( "#AIQuestionIDText2" ).html( value.QuestionText );
                          }


                         if ( index == 2 )
                         {
                             $( "#<%=hdAIQuestionID3.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionIDText1ToolTip" ).html( value.QuestionText );
                             $( "#<%=txtdmrodcmanagerco.ClientID%>" ).attr( "placeholder", value.QuestionText );
                                $( "#AIQuestionIDText3" ).html( value.QuestionText );
                         } 

                         if ( index == 3 )
                         {
                             $( "#<%=hdAIQuestionID4.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionIDText4ToolTip" ).html( value.QuestionText );
                             $( "#<%=txtdirector.ClientID%>" ).attr( "placeholder", value.QuestionText );
                             $( "#AIQuestionIDText4" ).html( value.QuestionText );
                         }

                         index = parseInt( index ) + 1;
                     } );
                 },
                 error: function ( XMLHttpRequest, textStatus, errorThrown )
                 {
                     //alert( errorThrown );
                 }
             } );
         }
         $( document ).ready( function ()
         {
             GetAIPeopleInvolved();
         } );
    </script>
    <section id="main-content" class="pg-ca-preventions">
        <span class="mask"></span>
        <div class="wrapper">

            <div class="plms-alert secondary-clr">

                <p>
                    <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->

       <%--   <div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
	  <%=Resources.Resource.lblMandatoryField %></p>
	</div><!--End .plms-alert-->--%>
	 

            <div class="layout-sidebar-right">

                <div class="boxed-content">

                    <div class="boxed-content-body">
                        <h2><%=Resources.Resource.lblPeopleInvolvedTitle %>   </h2>

                        <div class="plms-fieldset pull-up-10">
                            <asp:HiddenField ID="hdAIQuestionID1" runat="server" />
                            <label class="plms-label is-hidden" for="manager-supervisor" id="AIQuestionIDText1"></label>
                            <div class="plms-tooltip-parent">
                                <input type="text" class="plms-input skin2" id="txtmanagersupervisor" name="manager-supervisor" runat="server" />

                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="AIQuestionIDText1ToolTip"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                        <div class="plms-fieldset">
                            <asp:HiddenField ID="hdAIQuestionID2" runat="server" />
                            <label class="plms-label is-hidden" for="health-safety-rep-jhsc-member" id="AIQuestionIDText2"></label>

                            <div class="plms-tooltip-parent">
                                <input type="text" class="plms-input skin2" id="txthealthsafetyrepjhscmember" name="health-safety-rep-jhsc-member" runat="server" />

                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="AIQuestionIDText2ToolTip"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                        <div class="plms-fieldset">
                            <asp:HiddenField ID="hdAIQuestionID3" runat="server" />
                            <label class="plms-label is-hidden" for="dm-ro-dc-manager-co" id="AIQuestionIDText3"></label>

                            <div class="plms-tooltip-parent">
                                <input type="text" class="plms-input skin2" id="txtdmrodcmanagerco" name="dm-ro-dc-manager-co" runat="server" />

                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="AIQuestionIDText3ToolTip"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                        <div class="plms-fieldset">
                            <asp:HiddenField ID="hdAIQuestionID4" runat="server" />
                            <label class="plms-label is-hidden" for="director" id="AIQuestionIDText4"></label>

                            <div class="plms-tooltip-parent">
                                <input type="text" class="plms-input skin2" id="txtdirector" name="director" runat="server" />

                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="AIQuestionIDText4ToolTip"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                    </div>
                    <!--End .boxed-content-body-->

                    <nav class="pagination-nav">

                         <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"   />
                         <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"    />
                  
                    </nav>
                    <!--End .pagination-nav-->
                </div>
                <!--End .boxed-content-->

                <aside id="main-aside">
                    <div class="aside-toggle-handle">
                        <div class="flyout-message-wrapper">
                            <a href="#toggle-aside" class="btn">
                                <i class="icon"></i>
                                <span>Hide Menu</span>
                            </a>
                        </div>
                        <!--End .flyout-message-wrapper-->
                    </div>
                    <!--End .aside-toggle-handle-->

                    <nav class="aside-main-nav">
            <%-- <ul>
			<li><a id="A1" href="AIGeneralInformation.aspx" title=""  runat="server"><%= Resources.Resource.lblGenInfoPage %></a></li>
			<li><a id="A2" href="AiDetails.aspx" title=""  runat="server"><%= Resources.Resource.lblDetailPage %> </a></li>
			<li><a id="A3" href="AIAccidentIncidentDetail.aspx" title=""  runat="server">  <%= Resources.Resource.lblAccidentIncidentdetailPage %>   </a></li>
			<li><a id="A4" href="AIContributingFactor.aspx" title=""  runat="server">   <%= Resources.Resource.lblcontributingFactorPage %> </a></li>
			<li><a id="A5" href="AIActionAndPrevention.aspx" title="" runat="server">  <%= Resources.Resource.lblAiCorrectionActionPage %>   </a></li>
			<li><a id="A6" href="AIWitnessesCoWorker.aspx" title=""  runat="server">  <%= Resources.Resource.lblCoworkerPage %></a></li>
			<li><a id="A7" href="AIWorkerComment.aspx" title="" runat="server">  <%= Resources.Resource.lblWorkerCommentPage %> </a></li>
			<li  class="is-active"><a id="A8" href="AIPeopleInvolved.aspx" title=""  runat="server">  <%= Resources.Resource.lblPeopleInvolvedPage %>   </a></li>
			</ul>--%> 
			       <UC1Menu:UC1  ID="lblmenu"   runat="server"/>  
                    </nav>
                    <!--End .aside-main-nav-->
                </aside>
                <!--End #main-aside-->

            </div>
            <!--End .layout-sidebar-right-->

            <footer>
                <div class="btngrp">                        
                <asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click"     /> 
                <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click"      />
         
                </div>
            </footer>
        </div>
        <!--End .wrapper-->
    </section>
    <!--End #main-content-->

</asp:Content>

