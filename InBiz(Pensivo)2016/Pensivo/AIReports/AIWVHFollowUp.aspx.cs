﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AIWVHFollowUp : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }

                getFormData();
            }
        }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {
                var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (IsLicensee != null)
                {
                    Response.Redirect("AIWVHSupervisorSection.aspx", false);
                    return;
                }
                else
                {
                    Response.Redirect("AIWVHSupervisorSection.aspx", false);
                    return;
                }
            }

        }
        catch { }
        finally { }
    }
    protected void btnSaveAndExit_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitmyReport_Click(object sender, EventArgs e)
    {
        //List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();       
        //try
        //{
        //    objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        //    if (objList != null)
        //    {
        //        // Remove item if Item is already Exists(Based on PageName)
        //        var resultItem = objList.Where(x => x.AIPageName == PageName.WVHFollowUp).ToList();
        //        if (resultItem != null && resultItem.Count > 0)
        //        {
        //            foreach (var removeItem in resultItem)
        //            {
        //                objList.Remove(removeItem);
        //            }
        //        }
        //        // Get the AI Form ID
        //        var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
        //        if (fromIDResult != null)
        //        {
        //            aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
        //        }

        //        objList.Add(
        //             new AIQuestionAnswer
        //             {
        //                 AIQuestionID = Convert.ToInt32(hdWVHFollowUPQuestionID1.Value),
        //                 AIQuestionType = QuestionType.TextType,
        //                 AIQuestionAnswerText = Convert.ToString(txtcommentsadditionalactions.Text),
        //                 AIFormID = aiFormID,
        //                 AIPageName = PageName.WVHFollowUp,
        //                 AISequence = 1
        //             });

        //        Session["AIReportQuestions"] = objList;

        //        //managePreviousNext("");
        //        //RedirectionLink();
        //        Response.Redirect("ReportComplete.aspx", false);
        //    }
        //}
        //catch (Exception ex) { throw ex; }
        //finally { }

    }
    protected void btnNext_Click(object sender, System.EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHFollowUp).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }


                if (!string.IsNullOrEmpty(Convert.ToString(  hdWVHFollowUPQuestionID1.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdWVHFollowUPQuestionID1.Value),
                             AIQuestionType = QuestionType.TextType,
                             AIQuestionAnswerText = Convert.ToString(txtcommentsadditionalactions.Text),
                             AIFormID = aiFormID,
                             AIPageName = PageName.WVHFollowUp,
                             AISequence = 1
                         });
                }
                Session["AIReportQuestions"] = objList;
                 

                // Get result from all scenarios
                var ISWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
                var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");
                if (ISWVH != null && IsPerosnalInjury == null && IsFirstAidBox == null && IsHealthCare == null && IsLostTime == null && IsPropertyDamage == null)
                {
                    Response.Redirect("AiDetails.aspx", false);
                    return;
                }
                else
                {
                    Response.Redirect("AIGeneralInformation.aspx", false);
                    return;
                }
            }        
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                var resultItem = objList.FirstOrDefault(x => x.AIPageName == PageName.WVHFollowUp);
                if (resultItem != null)
                {
                    txtcommentsadditionalactions.Text = Convert.ToString(resultItem.AIQuestionAnswerText);
                }
            }
        }
        catch { }
        finally { }
    }

}