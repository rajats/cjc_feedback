﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIPersonalInjury.aspx.cs" Inherits="AIReports_AIPersonalInjury" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link rel="stylesheet" href="../_styles/ai-reports.css" />
 <section id="main-content">
<span class="mask"></span>
<div class="wrapper width-med">
 	<div class="plms-alert secondary-clr">
       <p>
                   <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %> #<span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
       </p>
    </div><!--End .plms-alert-->
	<div class="boxed-content">		
		<h2> <%=Resources.Resource.lblPersonalInjury %>   </h2>		
		<div class="boxed-content-body">
		
        <h3 class="h4">  <%=Resources.Resource.lblDescription %></h3>	
            	
		<p> <%=Resources.Resource.lblPersonalInjuyDescription %> </p>
		
		</div><!--End .boxed-content-body-->		
		<nav class="pagination-nav">
            <asp:Button ID="btnPrevoius"  runat="server" title= "<%$ Resources:Resource, lblPrevoius  %>" Text="<%$ Resources:Resource, lblPrevoius %>" class="btn large" OnClick="btnPrevoius_Click"  />
            
            <asp:Button ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"  />

        </nav><!--End .pagination-nav-->
	
	</div><!--End .boxed-content-->
	

	<footer>
	 <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn" OnClick="btnSubmitwithoutSaving_Click" />
    </footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
 

</asp:Content>

