﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIWitnessesCoWorker.aspx.cs" Inherits="AIReports_AIWitnessesCoWorker" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../_styles/ai-reports.css" />

 <script   type="text/javascript">
     function GetAIWitnessesCoworker()
     {
         $.ajax( {
             type: "POST",
             url: "CommonInterface.aspx/getQuestions",
             data: "{pageType:'" + "WitnessCoworkersForm" + "'}",
             dataType: "json",
             contentType: "application/json; charset=utf-8",
             success: function ( data )
             {
                 var index = 0;
                 $.each( data.d, function ( index, value )
                 {
                     if ( index == 0 )
                     {
                         $( "#<%=hdAIQuestionID1.ClientID%>" ).val( value.QuestionID );                            
                            $( "#AIQuestionIDText1" ).html( value.QuestionText );
                     }
                         if ( index == 1 )
                         {
                             $( "#<%=hdAIQuestionID2.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionIDText2ToolTip" ).html( value.QuestionText );
                             $( "#<%=txtname.ClientID%>" ).attr( "placeholder", value.QuestionText );
                             $( "#AIQuestionIDText2" ).html( value.QuestionText );
                          }
                         if ( index == 2 )
                         {
                             $( "#<%=hdAIQuestionID3.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionIDText3ToolTip" ).html( value.QuestionText );
                             $( "#<%=txtphonenumber.ClientID%>" ).attr( "placeholder", value.QuestionText );
                             $( "#AIQuestionIDText2" ).html( value.QuestionText );
                         }
                     if ( index == 3)
                     {
                         $( "#<%=hdAIQuestionID4.ClientID%>" ).val( value.QuestionID );
                          $( "#AIQuestionIDText4" ).html( value.QuestionText );
                      }

                         index = parseInt( index ) + 1;
                     } );
                 },
                 error: function ( XMLHttpRequest, textStatus, errorThrown )
                 {
                     //alert( errorThrown );
                 }
             } );
         }
         $( document ).ready( function ()
         {
           GetAIWitnessesCoworker();
           
           $( "#<%=dvWitnessInfo.ClientID%>" ).hide();

         });

         function ClickChange(ID, Value) {
           if (Value == "Yes") {
             
           }
           else
           {
             
           }
           if (ID == "ancAiQuestion1Yes") {
             $("#<%=ancAiQuestion1Yes.ClientID%>").removeClass('btn');
             $("#<%=ancAiQuestion1Yes.ClientID%>").addClass('btnChageColor');
             if ($("#<%=ancAiQuestion1No.ClientID%>").hasClass("btnChageColor")) {
               $("#<%=ancAiQuestion1No.ClientID%>").removeClass('btnChageColor');
               $("#<%=ancAiQuestion1No.ClientID%>").addClass('btn');
             }
               
               
               $( "#<%=txtphonenumber.ClientID%>" ).val( "" );
               $( "#<%=txtname.ClientID%>" ).val( "" );
               $( "#<%=dvWitnessInfo.ClientID%>" ).show( );
             
           }

           if (ID == "ancAiQuestion1No") {
             $("#<%=ancAiQuestion1No.ClientID%>").removeClass('btn');
             $("#<%=ancAiQuestion1No.ClientID%>").addClass('btnChageColor');
             if ($("#<%=ancAiQuestion1Yes.ClientID%>").hasClass("btnChageColor")) {
               $("#<%=ancAiQuestion1Yes.ClientID%>").removeClass('btnChageColor');
                 $("#<%=ancAiQuestion1Yes.ClientID%>").addClass('btn');
             }
               $( "#<%=dvWitnessInfo.ClientID%>" ).hide();
             
             }

             if (ID == "ancAiQuestion2Yes") {
               $("#<%=ancAiQuestion2Yes.ClientID%>").removeClass('btn');
             $("#<%=ancAiQuestion2Yes.ClientID%>").addClass('btnChageColor');
             if ($("#<%=ancAiQuestion2No.ClientID%>").hasClass("btnChageColor")) {
               $("#<%=ancAiQuestion2No.ClientID%>").removeClass('btnChageColor');
               $("#<%=ancAiQuestion2No.ClientID%>").addClass('btn');
             }
           }

           if (ID == "ancAiQuestion2No") {
             $("#<%=ancAiQuestion2No.ClientID%>").removeClass('btn');
             $("#<%=ancAiQuestion2No.ClientID%>").addClass('btnChageColor');
             if ($("#<%=ancAiQuestion2Yes.ClientID%>").hasClass("btnChageColor")) {
               $("#<%=ancAiQuestion2Yes.ClientID%>").removeClass('btnChageColor');
               $("#<%=ancAiQuestion2Yes.ClientID%>").addClass('btn');
             }
           }


         }

  
          </script> 


    
<section id="main-content" class="pg-ca-preventions">
<span class="mask"></span>
<div class="wrapper">
	<div class="plms-alert secondary-clr">
		
         <p> 
             <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
         </p>	 
	</div><!--End .plms-alert--> 

<%--	  	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		  <%=Resources.Resource.lblMandatoryField %></p>
	</div><!--End .plms-alert-->--%>
 

	<div class="layout-sidebar-right">

		<div class="boxed-content">
			
			<div class="boxed-content-body">
				
			<h2>  <%=Resources.Resource.lblWinnesssesCoworkerTitle %> </h2>
			
			<div class="plms-fieldset-wrapper push-up-10">
				<i class="req-icon" title="Mandatory Field">*</i>
				<div class="column span-8">                    
                    <asp:HiddenField ID="hdAIQuestionID1"   runat="server"/>
                    <asp:HiddenField ID="hdAIQuestionIDAns1"   runat="server"/>
					<p class="bold"  id="AIQuestionIDText1"></p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp align-r">
					<a id="ancAiQuestion1Yes" runat="server"   class="btn" title="Yes" onclick="ClickChange('ancAiQuestion1Yes', this.text);">Yes</a>
					<a id="ancAiQuestion1No" runat="server" class="btn" title="No" onclick="ClickChange('ancAiQuestion1No', this.text);">No</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
			
      <div id="dvWitnessInfo"    runat="server">
			<p>Provide the names and phone numbers of all persons who witnessed or have information concerning the accident / incident. All co-workers in the immediate area must be interviewed concerning their knowledge of the accident / incident, symptoms, complaints, or lack thereof.</p>
			
      
			  <div class="plms-fieldset pull-up-10">
                   <asp:HiddenField ID="hdAIQuestionID2"   runat="server"/>
				  <label class="plms-label is-hidden" for="name" id="AIQuestionIDText2"></label>
				
				  <div class="plms-tooltip-parent">
					  <input type="text"   class="plms-input skin2" id="txtname" name="name"  runat="server" />
					
                    
					  <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
						  <div class="plms-tooltip-body">
							  <p  id="AIQuestionIDText2ToolTip"></p>
						  </div><!--End .plms-tooltip-body-->
					  </div><!--End .plms-tooltip-->
				  </div><!--End .plms-tooltip-parent-->
			  </div><!--End .plms-fieldset-->
			
			  <div class="plms-fieldset push-down">
                   <asp:HiddenField ID="hdAIQuestionID3"   runat="server"/>
				  <label class="plms-label is-hidden" for="phone-number"  id="AIQuestionIDText3" > </label>
				  <div class="plms-tooltip-parent">
					  <input type="text"  class="plms-input skin2" id="txtphonenumber" name="phone-number"  runat="server"/>
					  <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
						  <div class="plms-tooltip-body">
							  <p id="AIQuestionIDText3ToolTip">  </p>
						  </div><!--End .plms-tooltip-body-->
					  </div><!--End .plms-tooltip-->
				  </div><!--End .plms-tooltip-parent-->
			  </div><!--End .plms-fieldset-->
      
			
			<div class="plms-fieldset-wrapper push-up-10">
                  <asp:HiddenField ID="hdAIQuestionID4"   runat="server"/>
                  <asp:HiddenField ID="hdAIQuestionIDAns4"   runat="server"/>              
				<i class="req-icon" title="Mandatory Field">*</i>
				<div class="column span-8">
					<p class="bold"  id="AIQuestionIDText4"> </p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp align-r">
					<a id="ancAiQuestion2Yes" runat="server" href="#nogo" class="btn" title="Yes" onclick="ClickChange('ancAiQuestion2Yes', this.text);">Yes</a>
					<a id="ancAiQuestion2No" runat="server" href="#nogo" class="btn" title="No" onclick="ClickChange('ancAiQuestion2No', this.text);">No</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
			
			<a href="#nogo" class="btn large fluid clearfix" title="Click here to add another">Click here to add another</a>
					
			  </div><!--End .boxed-content-body-->
	  </div>
			<nav class="pagination-nav">
                 
              <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"    />
              <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"  />
                
<%--			<a href="AIActionAndPreventionFromDevice.aspx" title="Previous" class="btn large"  runat="server">Previous</a>
			<a href="AIWorkerComment.aspx" title="Next" class="btn large align-r"  runat="server">Next</a>--%>			
            
            </nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span><%=  Resources.Resource.lblHideMenu %></span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->			
			<nav class="aside-main-nav">
			       <UC1Menu:UC1  ID="lblmenu"   runat="server"/>  

<!--		    <ul>
			<li ><a id="A1" href="AIGeneralInformation.aspx" title=""  runat="server"><%= Resources.Resource.lblGenInfoPage %></a></li>
			<li><a id="A2" href="AiDetails.aspx" title=""  runat="server"><%= Resources.Resource.lblDetailPage %> </a></li>
			<li><a id="A3" href="AIAccidentIncidentDetail.aspx" title=""  runat="server">  <%= Resources.Resource.lblAccidentIncidentdetailPage %>   </a></li>
			<li><a id="A4" href="AIContributingFactor.aspx" title=""  runat="server">   <%= Resources.Resource.lblcontributingFactorPage %> </a></li>
			<li><a id="A5" href="AIActionAndPrevention.aspx" title="" runat="server">  <%= Resources.Resource.lblAiCorrectionActionPage %>   </a></li>
			<li class="is-active"><a id="A6" href="AIWitnessesCoWorker.aspx" title=""  runat="server">  <%= Resources.Resource.lblCoworkerPage %></a></li>
			<li><a id="A7" href="AIWorkerComment.aspx" title="" runat="server">  <%= Resources.Resource.lblWorkerCommentPage %> </a></li>
			<li><a id="A8" href="AIPeopleInvolved.aspx" title=""  runat="server">  <%= Resources.Resource.lblPeopleInvolvedPage %>   </a></li>
			</ul>-->
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
	<%--<a href="#nogo" class="btn" title="Save & Exit to Continue Later">Save &amp; Exit to Continue Later</a>
	<a href="#nogo" class="btn" title="Submit My Report">Submit My Report</a>--%>
	
         <asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click" /> 
         <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" />
 
    </div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
  
</asp:Content>

