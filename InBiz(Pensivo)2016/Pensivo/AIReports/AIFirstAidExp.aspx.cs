﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIFirstAidExp : System.Web.UI.Page
{
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
           //ViewState["RefUrl"] = Request.UrlReferrer.ToString();
        }
    }
    //protected void btnPrevious_Click(object sender, EventArgs e)
    //{
    //    //object refUrl = ViewState["RefUrl"];
    //    //if (refUrl != null)
    //    //    Response.Redirect((string)refUrl);
    //    ////Response.Redirect(Request.UrlReferrer.ToString(), false);
    //}
    //protected void btnNext_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("AILostTimeExp.aspx", false);
    //}
    //protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    //{
    //    // Redirect to AI dashboard
    //}   
    protected void btnPrevoius_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIFirstAidDesc.aspx", false);
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIFirstAidBox.aspx", false);
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBoard.aspx", false);

        // exit  to save
    }
}