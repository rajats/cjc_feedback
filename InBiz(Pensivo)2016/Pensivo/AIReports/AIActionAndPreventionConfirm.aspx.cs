﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIActionAndPreventionConfirm : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();
        }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            var resultItem = objList.Where(x => x.AIPageName == PageName.ActionPreventionConfirmation).OrderBy(x => x.AIQuestionID).ToList();
            if (resultItem != null && resultItem.Count > 0)
            {
                int count = 0;
                foreach (var result in resultItem)
                { 
                    if (count == 0)
                    {
                        hdAIQuestionAnsID1.Value = Convert.ToString(result.AIQuestionAnswerText);
                        if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            ancAiQuestionD1Yes.Attributes.Remove("Class");
                            ancAiQuestionD1Yes.Attributes.Add("Class", "btnChageColor"); 
                        }
                        else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                        {
                            ancAiQuestionD1No.Attributes.Remove("Class");
                            ancAiQuestionD1No.Attributes.Add("Class", "btnChageColor");
                            dvLicenseeQuestion.Attributes.Remove("Class");
                            dvLicenseeQuestion.Attributes.Add("Class", "divShow");
                        }                        
                    }
                    else if (count == 1)
                    {
                        if (result.AISequence == 2)
                        {
                            txtlicenseeownermanager.Value = Convert.ToString(result.AISequence);
                        }
                        else if (result.AISequence == 3)
                        {
                            hdAIQuestionAnsID3.Value = Convert.ToString(result.AIQuestionAnswerText);
                            if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                            {
                                ancAiQuestion2Yes.Attributes.Remove("Class");
                                ancAiQuestion2Yes.Attributes.Add("Class", "btnChageColor"); 
                                
                            }
                            else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                            {
                                ancAiQuestion2No.Attributes.Remove("Class");
                                ancAiQuestion2No.Attributes.Add("Class", "btnChageColor"); 
                            }
                        }
                    }
                    else if  (count == 2)
                    {
                        if (result.AISequence == 3)
                        {
                            hdAIQuestionAnsID3.Value = Convert.ToString(result.AIQuestionAnswerText);
                            if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                            {
                                ancAiQuestion2Yes.Attributes.Remove("Class");
                                ancAiQuestion2Yes.Attributes.Add("Class", "btnChageColor");
                            }
                            else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                            {
                                ancAiQuestion2No.Attributes.Remove("Class");
                                ancAiQuestion2No.Attributes.Add("Class", "btnChageColor");
                            }
                        }
                        else if(result.AISequence == 4)
                        {
                            hdAIQuestionAnsID4.Value = Convert.ToString(result.AIQuestionAnswerText);
                            if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                            {
                                ancAiQuestion3Yes.Attributes.Remove("Class");
                                ancAiQuestion3Yes.Attributes.Add("Class", "btnChageColor");
                            }
                            else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                            {
                                ancAiQuestion3No.Attributes.Remove("Class");
                                ancAiQuestion3No.Attributes.Add("Class", "btnChageColor");
                            }
                        }                        
                    }
                    else if (count == 3)
                    {
                        hdAIQuestionAnsID4.Value = Convert.ToString(result.AIQuestionAnswerText);
                        if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            ancAiQuestion3Yes.Attributes.Remove("Class");
                            ancAiQuestion3Yes.Attributes.Add("Class", "btnChageColor");
                        }
                        else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                        {
                            ancAiQuestion3No.Attributes.Remove("Class");
                            ancAiQuestion3No.Attributes.Add("Class", "btnChageColor");
                        }
                    } 
                    count++; 
                }
            }
        }
        catch { }
        finally { }
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIActionAndPrevention.aspx", false);
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();        
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.ActionPreventionConfirmation).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }

                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }

                
                if (!string.IsNullOrEmpty(hdAIQuestionID1.Value))
                {
                objList.Add(
                     new AIQuestionAnswer
                     {
                         AIQuestionID = Convert.ToInt32(hdAIQuestionID1.Value),
                         AIQuestionType = QuestionType.BooleanType,
                         AIQuestionAnswerText = Convert.ToString(hdAIQuestionAnsID1.Value) == "" ? ConfirmationYesNo.NotAnswered : Convert.ToString(hdAIQuestionAnsID1.Value),
                         AIFormID = aiFormID,
                         AIPageName = PageName.ActionPreventionConfirmation,
                         AISequence = 1
                     });
                }

                if (!string.IsNullOrEmpty(hdAIQuestionID2.Value))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdAIQuestionID2.Value),
                            AIQuestionType = QuestionType.TextType,
                            AIQuestionAnswerText = Convert.ToString(txtlicenseeownermanager.Value),
                            AIFormID = aiFormID,
                            AIPageName = PageName.ActionPreventionConfirmation,
                            AISequence = 2
                        });
                }

                if (!string.IsNullOrEmpty(hdAIQuestionID3.Value))
                {
                    objList.Add(
                       new AIQuestionAnswer
                       {
                           AIQuestionID = Convert.ToInt32(hdAIQuestionID3.Value),
                           AIQuestionType = QuestionType.TextType,
                           AIQuestionAnswerText = Convert.ToString(hdAIQuestionAnsID3.Value),
                           AIFormID = aiFormID,
                           AIPageName = PageName.ActionPreventionConfirmation,
                           AISequence = 3
                       });

                }


                if (!string.IsNullOrEmpty(hdAIQuestionID4.Value))
                {
                    objList.Add(
                      new AIQuestionAnswer
                      {
                          AIQuestionID = Convert.ToInt32(hdAIQuestionID4.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = Convert.ToString(hdAIQuestionAnsID4.Value),
                          AIFormID = aiFormID,
                          AIPageName = PageName.ActionPreventionConfirmation, 
                          AISequence = 4
                      });
                }

                Session["AIReportQuestions"] = objList;
                Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
             
            
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }


    }
}