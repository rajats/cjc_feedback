﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIPropertyDamageConfirm : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                        hdFirstName.Value = BusinessUtility.GetString(result.EmpFirstName);
                    }
                }
            }

            if (!string.IsNullOrEmpty(Convert.ToString(Request.UrlReferrer)))
            {
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
            }
            else
            {
                Response.Redirect("AILocationInfo.aspx", false);
            }
        }
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        // Redirect to AI dashboard

    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.PropertyDamageQuestion).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }

                objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdPropertyDamageConfirmQuestionID.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(ConfirmationYesNo.No),
                        AIFormID = aiFormID,
                        AIPageName = PageName.PropertyDamageQuestion
                    });
                Session["AIReportQuestions"] = objList;



                List<AIQuestionAnswer> objList1 = new List<AIQuestionAnswer>();
                objList1 = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
                if (objList1 != null)
                {
                    var result = objList.Where(x => x.AIQuestionAnswerText == "1").ToList();
                    if (result != null && result.Count > 0)
                    {
                        managePreviousNext("AISummary.aspx");
                        Response.Redirect("AISummary.aspx", false);

                    }
                    else
                    {
                        managePreviousNext("AIValidationSummary.aspx");
                        Response.Redirect("AIValidationSummary.aspx", false);
                    }
                }
                //Response.Redirect("AIValidationSummary.aspx", false);

            }
        }
        catch (Exception ex) { throw ex; }
        finally { }

    }
    protected void btnYes_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            // Remove item if Item is already Exists(Based on PageName)
            var resultItem = objList.Where(x => x.AIPageName == PageName.PropertyDamageQuestion).ToList();
            if (resultItem != null && resultItem.Count > 0)
            {
                foreach (var removeItem in resultItem)
                {
                    objList.Remove(removeItem);
                }
            }
            // Get the AI Form ID
            var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
            if (fromIDResult != null)
            {
                aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
            }


            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdPropertyDamageConfirmQuestionID.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(ConfirmationYesNo.Yes),
                        AIFormID = aiFormID,
                        AIPageName = PageName.PropertyDamageQuestion
                    });

                Session["AIReportQuestions"] = objList;


                List<AIQuestionAnswer> objList1 = new List<AIQuestionAnswer>();
                objList1 = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
                if (objList1 != null)
                {
                    var result = objList.Where(x => x.AIQuestionAnswerText == "1").ToList();
                    if (result != null && result.Count > 0)
                    {
                        managePreviousNext("AISummary.aspx");
                        Response.Redirect("AISummary.aspx", false);
                        
                    }
                    else
                    {
                        managePreviousNext("AIValidationSummary.aspx");
                        Response.Redirect("AIValidationSummary.aspx", false);
                    }
                }
               
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnPrevoius_Click(object sender, EventArgs e)
    {    
       Response.Redirect("AIPropertyDamageDesc.aspx", false );
    }
    private void managePreviousNext(string NextPageUrl)
    { 
        List<ManagePagePreviousNext> objPage = new List<ManagePagePreviousNext>();
        try
        {
            objPage = (List<ManagePagePreviousNext>)Session["AIPagesList"];
            if (objPage != null)
            {
                var resultItem = objPage.Where(x => x.pageName == PageName.PropertyDamageQuestion).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objPage.Remove(removeItem);
                    }
                }
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIPropertyDamageConfirm.aspx", NextPageUrl = NextPageUrl, pageName = PageName.PropertyDamageQuestion, PreviousPageUrl = "AIPropertyDamageDesc.aspx" });
            }
            else
            {
                objPage = new List<ManagePagePreviousNext>();
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIPropertyDamageConfirm.aspx", NextPageUrl = NextPageUrl, pageName = PageName.PropertyDamageQuestion, PreviousPageUrl = "AIPropertyDamageDesc.aspx" });
            }
            

            Session["AIPagesList"] = objPage;
        }
        catch { }
        finally { }
    }

}