﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ReportComplete.aspx.cs" Inherits="AIReports_ReportComplete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
     <section id="main-content">
<span class="mask"></span>
<div class="wrapper width-med">

	<div class="plms-alert secondary-clr">
	 
	  <p><%=Resources.Resource.lblAccidentIncident %>
           <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> ( <%=Resources.Resource.lblAISite %> <span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>,  <%=Resources.Resource.lblEmployeeID %>  <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
       </p> 
    </div><!--End .plms-alert-->
	<div class="boxed-content">		
		<h2> </h2>		
		<div class="boxed-content-body">
		
		<div class="column span-9">
			<div class="alert secondary-clr">               
			<p>  
                   <%=Resources.Resource.lblReportCompleteMsg %> &nbsp; <asp:Label  ID="lblInitiatorEmailID"  runat="server"></asp:Label>                 
               
                <br />

                 Once you Click <a  href="#">OK</a>, you'll be redirected to the Completed A/Is page.
			</p>
			<%--<p>Does what happened to Joshua match the "lost time" description 
			and closely relate to one of the examples seen on the previous screens?</p>--%>
			</div><!--End .alert-->
		</div><!--End .column-->
		
		<div class="column span-3">
			<div class="btngrp align-r">
	<%--		<asp:Button ID="btnNo" runat="server" title="<%$ Resources:Resource, lblNo  %>" Text="<%$ Resources:Resource, lblNo %>" class="btn large"     />
             <asp:Button ID="btnYes" runat="server" title="<%$ Resources:Resource, lblYes %>" Text="<%$ Resources:Resource, lblYes %>" class="btn large align-r"    />--%>
			</div><!--End .btngrp-->
		</div><!--End .column-->
		
		</div><!--End .boxed-content-body-->
		
		<nav class="pagination-nav">         
            
        <%-- <asp:Button ID="btnPrevoius"  runat="server" title= "<%$ Resources:Resource, lblPrevoius  %>" Text="<%$ Resources:Resource, lblPrevoius %>" class="btn large"    />--%>
		<%--<a href="AILostTimeExp.aspx" title="Previous" class="btn large">Previous</a>
		<a href="AIPropertyDamageDesc.aspx" title="Next" class="btn large align-r">Next</a>--%>
		</nav><!--End .pagination-nav-->
	
	</div><!--End .boxed-content-->
	
	<footer>
	<%--<asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn"   />--%>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->

</asp:Content>

