﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIGeneralInformation.aspx.cs" Inherits="AIReports_AIGeneralInformation" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../_styles/ai-reports.css" />     
    <script   type="text/javascript">
         function GetGenralInfoQuestion()
         {
             $.ajax( {
                 type: "POST",
                 url: "CommonInterface.aspx/getQuestions",
                 data: "{pageType:'" + "GeneralInformation" + "'}",
                 dataType: "json",
                 contentType: "application/json; charset=utf-8",
                 success: function ( data )
                 {
                     var index = 0;
                     $.each( data.d, function ( index, value )
                     {
                         if ( index == 0 )
                         {
                             $( "#<%=hdroutineQuestionID1.ClientID%>" ).val( value.QuestionID );
                             $( "#PRoutine" ).html( value.QuestionText );     
                         }
                         if ( index == 1 )
                         {
                             $( "#<%=hdReoccurrenceQuestionID1.ClientID%>" ).val( value.QuestionID );
                             $( "#PReoccurrence" ).html( value.QuestionText );
                         
                          }                      

                       index = parseInt( index ) + 1;
                   } );
               },
               error: function ( XMLHttpRequest, textStatus, errorThrown )
               {
                   //alert( errorThrown );
               }
           } );
       }
       $( document ).ready( function ()
       {
           GetGenralInfoQuestion();
       });

      function ChangeColor(control, activeControl, ctrl2, ctrlAnswer) {
        var sPrefix = 'ContentPlaceHolder1_';
        var sActiveId = '#' + sPrefix + activeControl;
        var sControlDeactivate1 = '#' + sPrefix + ctrl2;
        var sAnswer = '#' + sPrefix + ctrlAnswer;
        var sText = control.text.toLowerCase();
        var sAnsYes = '1';
        var sAnsNo = '0';

        if (sText == "yes") {
          $(sAnswer).val(sAnsYes);
        }
        else if (sText == "no") {
          $(sAnswer).val(sAnsNo);
        }

        $(sActiveId).removeClass('btn');
        $(sActiveId).addClass('btnChageColor');
        if ($(sControlDeactivate1).hasClass("btnChageColor")) {
          $(sControlDeactivate1).removeClass('btnChageColor');
          $(sControlDeactivate1).addClass('btn');
        }
      }


    </script> 
    <section id="main-content">
<span class="mask"></span>
<div class="wrapper">

	<div class="plms-alert secondary-clr">
            <p> 
               <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
            </p>
	</div><!--End .plms-alert-->

	 
    <div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		  <%=Resources.Resource.lblMandatoryField %></p>
	   </div><!--End .plms-alert--> 

	<div class="layout-sidebar-right">
		<div class="boxed-content">			
			<div class="boxed-content-body">				
			<h2><%=Resources.Resource.lblGeneralInfo %></h2>			
			<div class="plms-fieldset-wrapper">
                <asp:HiddenField ID ="hdroutineQuestionID1"  runat="server"/>
                 <asp:HiddenField ID ="hdroutineQuestionAns1"  runat="server"/>
				<i class="req-icon" title="Mandatory Field">*</i>
				<div class="column span-8">
					<p class="bold" id="PRoutine"></p>
				</div><!--End .column-->
				
			<div class="column span-4">
					<div class="btngrp align-r">
					<a  id="aRoutineYes" class="btn" title="Yes" onclick="ChangeColor(this, 'aRoutineYes', 'aRoutineNo', 'hdroutineQuestionAns1');" runat="server">Yes</a>
					<a  id="aRoutineNo" class="btn" title="No" onclick="ChangeColor(this, 'aRoutineNo', 'aRoutineYes', 'hdroutineQuestionAns1');"  runat="server">No</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
		    </div><!--End .plms-fieldset-wrapper-->
			
			<div class="plms-fieldset-wrapper">
				<div class="column span-8">
                    <asp:HiddenField ID ="hdReoccurrenceQuestionID1"  runat="server"/>
                     <asp:HiddenField ID ="hdReoccurrenceQuestionAns1"  runat="server"/>
					<i class="req-icon" title="Mandatory Field">*</i>
					<p class="bold" id="PReoccurrence"> </p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp align-r">
					<a  id="aReoccurrenceYes" class="btn" title="Yes"  onclick="ChangeColor(this, 'aReoccurrenceYes', 'aReoccurrenceNo', 'hdReoccurrenceQuestionAns1');"  runat="server" >Yes</a>
					<a  id="aReoccurrenceNo" class="btn" title="No" onclick="ChangeColor(this, 'aReoccurrenceNo', 'aReoccurrenceYes', 'hdReoccurrenceQuestionAns1');"   runat="server">No</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
					
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">
		  
            <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"    />

			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span>Hide Menu</span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">

			      <UC1Menu:UC1  ID="lblmenu"   runat="server"/>

			 <%--<ul>
			<li class="is-active"><a id="A1" href="AIGeneralInformation.aspx" title=""  runat="server"><%= Resources.Resource.lblGenInfoPage %></a></li>
			<li><a id="A2" href="AiDetails.aspx" title=""  runat="server"><%= Resources.Resource.lblDetailPage %> </a></li>
			<li><a id="A3" href="AIAccidentIncidentDetail.aspx" title=""  runat="server">  <%= Resources.Resource.lblAccidentIncidentdetailPage %>   </a></li>
			<li><a id="A4" href="AIContributingFactor.aspx" title=""  runat="server">   <%= Resources.Resource.lblcontributingFactorPage %> </a></li>
			<li><a id="A5" href="AIActionAndPrevention.aspx" title="" runat="server">  <%= Resources.Resource.lblAiCorrectionActionPage %>   </a></li>
			<li><a id="A6" href="AIWitnessesCoWorker.aspx" title=""  runat="server">  <%= Resources.Resource.lblCoworkerPage %></a></li>
			<li><a id="A7" href="AIWorkerComment.aspx" title="" runat="server">  <%= Resources.Resource.lblWorkerCommentPage %> </a></li>
			<li><a id="A8" href="AIPeopleInvolved.aspx" title=""  runat="server">  <%= Resources.Resource.lblPeopleInvolvedPage %>   </a></li>
			</ul>--%>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
        <asp:Button  ID="btnSaveAndExit"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater  %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn large" OnClick="btnSaveAndExit_Click"    />

        <asp:Button  ID="btnSubmitmyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport  %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn large align-r" OnClick="btnSubmitmyReport_Click"    />

	</div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
     
  
</asp:Content>

