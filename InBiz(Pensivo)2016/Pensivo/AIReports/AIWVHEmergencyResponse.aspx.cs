﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIWVHEmergencyResponse : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
                getFormData();
            }
        }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        //List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        //objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {

            Response.Redirect("AIWVHEventOccurrence.aspx", false);
            //var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
            //if (IsLicensee != null)
            //{
            //    var IsPI = objList.FirstOrDefault(x=>x.AIPageName ==  PageName.PersonalInjury &&  x.AIQuestionAnswerText=="1");
            //    if (IsPI != null)
            //    {

            //        Response.Redirect("AIWVHEventOccurrence.aspx", false);
            //        return; 
            //    }
            //    else
            //    {
            //        Response.Redirect("AIActionAndPrevention.aspx", false);
            //        return; 
            //    }                 
            //}
            //else
            //{
            //    Response.Redirect("AIActionAndPrevention.aspx", false);
            //    return; 
            //}
        }
        catch { }
        finally { }

    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHEmergencyResponse).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }

                if (!string.IsNullOrEmpty(Convert.ToString( hdWVHResponseQuestionID1.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdWVHResponseQuestionID1.Value),
                             AIQuestionType = QuestionType.BooleanType,
                             AIQuestionAnswerText = Convert.ToString(Chkcoworker.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                             AIFormID = aiFormID,
                             AIPageName = PageName.WVHEmergencyResponse,
                             AISequence = 1
                         });

                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHResponseQuestionID2.Value)))
                {
                    objList.Add(
                  new AIQuestionAnswer
                  {
                      AIQuestionID = Convert.ToInt32(hdWVHResponseQuestionID2.Value),
                      AIQuestionType = QuestionType.BooleanType,
                      AIQuestionAnswerText = Convert.ToString(Chksupervisor.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                      AIFormID = aiFormID,
                      AIPageName = PageName.WVHEmergencyResponse,
                      AISequence = 2
                  });
                }
                 
                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHResponseQuestionID3.Value)))
                {
                    objList.Add(
                 new AIQuestionAnswer
                 {
                     AIQuestionID = Convert.ToInt32(hdWVHResponseQuestionID3.Value),
                     AIQuestionType = QuestionType.BooleanType,
                     AIQuestionAnswerText = Convert.ToString(Chkformeremployee.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                     AIFormID = aiFormID,
                     AIPageName = PageName.WVHEmergencyResponse,
                     AISequence = 3
                 });
                }
                if (Chkformeremployee.Checked)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(hdWVHResponseQuestionID4.Value)))
                    {
                        objList.Add(
                            new AIQuestionAnswer
                            {
                                AIQuestionID = Convert.ToInt32(hdWVHResponseQuestionID4.Value),
                                AIQuestionType = QuestionType.TextType,
                                AIQuestionAnswerText = Convert.ToString(txtOfficerName.Text),
                                AIFormID = aiFormID,
                                AIPageName = PageName.WVHEmergencyResponse,
                                AISequence = 4
                            });
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(hdWVHResponseQuestionID5.Value)))
                    {
                        objList.Add(
                             new AIQuestionAnswer
                             {
                                 AIQuestionID = Convert.ToInt32(hdWVHResponseQuestionID5.Value),
                                 AIQuestionType = QuestionType.TextType,
                                 AIQuestionAnswerText = Convert.ToString(txtofficerbadgenumber.Text),
                                 AIFormID = aiFormID,
                                 AIPageName = PageName.WVHEmergencyResponse,
                                 AISequence = 5
                             });
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(hdWVHResponseQuestionID6.Value)))
                    {
                        objList.Add(
                          new AIQuestionAnswer
                          {
                              AIQuestionID = Convert.ToInt32(hdWVHResponseQuestionID6.Value),
                              AIQuestionType = QuestionType.TextType,
                              AIQuestionAnswerText = Convert.ToString(txtpoliceoccurrencenumber.Text),
                              AIFormID = aiFormID,
                              AIPageName = PageName.WVHEmergencyResponse,
                              AISequence = 6
                          });
                    }
                }
                Session["AIReportQuestions"] = objList;

                //managePreviousNext("");

                RedirectionLink();

            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnSaveAndExit_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitmyReport_Click(object sender, EventArgs e)
    {

    }
    private void RedirectionLink()
    {
        //List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {

            //var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
            //if (IsLicensee !=  null)
            //{  
            //    // Case : 2 logistic:Yes Licensee:Yes WVH : Yes
            //    var IsLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "L");
            //    var IsWVHCase2 = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
            //    if (IsLogistic != null  && IsWVHCase2 != null)
            //    {
            //        var ISPI = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
            //        if (ISPI != null)
            //        {
            //            // AIWitnessesCoWorker.aspx
            //            Response.Redirect("AIWVHEmployeesPresent.aspx", false);
            //            return;
            //        }
            //        else
            //        {
            //            Response.Redirect("AIWitnessesCoWorker.aspx", false);
            //            return;
            //        }
            //    } 
            //}
            //else
            //{
            //    // Case 1:  Retail:Yes, WVH:Yes
            //    var IsRetailAndLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "R");
            //    var IsWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
            //    if ((IsRetailAndLogistic != null) && IsWVH != null)
            //    {
            //        Response.Redirect("AIWitnessesCoWorker.aspx", false);
            //        return;
            //    }
            //}

            Response.Redirect("AIWVHEmployeesPresent.aspx", false);

        }
        catch { }
        finally { }        
    }    
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHEmergencyResponse).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 1;
                    foreach (var result in resultItem)
                    {
                        if (count == 1 &&   Convert.ToString(result.AIQuestionAnswerText) == "1" &&   result.AISequence == 1)
                        {  
                           Chkcoworker.Checked = true;                                                      
                        }
                        else if (count == 2 && Convert.ToString(result.AIQuestionAnswerText) == "1" && result.AISequence == 2)
                        {
                            Chksupervisor.Checked = true;
                        }
                        else  if (count == 3 && Convert.ToString(result.AIQuestionAnswerText) == "1" && result.AISequence == 3)
                        {
                            Chkformeremployee.Checked = true;
                            // get the associated field regarding it 
                            divQuestionID.Visible = true;
                        }
                        else  if (count == 4 && result.AISequence == 4)
                        {
                            txtOfficerName.Text = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        else if (count == 5 && result.AISequence == 5)
                        {
                            txtofficerbadgenumber.Text = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        else if (count == 6 && result.AISequence == 6)
                        {
                            txtpoliceoccurrencenumber.Text = Convert.ToString(result.AIQuestionAnswerText);
                        }                 
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }
    }


}