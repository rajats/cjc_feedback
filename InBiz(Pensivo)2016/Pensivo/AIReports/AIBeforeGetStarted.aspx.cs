﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIBeforeGetStarted : System.Web.UI.Page
{
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];

                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                        hdFirstName.Value = BusinessUtility.GetString(result.EmpFirstName);
                    }
                }
            }
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        Session["AIPagesList"] = null;
        Response.Redirect("AIIncidentdesc.aspx");        
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    { 
      string redirectURL = string.Empty;
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            // Check Is Licensee
            var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "R");
            if (ISLicensee != null)
            {
                Response.Redirect("AIIsReatilLogistic.aspx");   
            }
            else
            {
                Response.Redirect("AIIsLogistic.aspx");  
            } 
        }
        catch { }
        finally { } 
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashboard.aspx", false);
    }
}