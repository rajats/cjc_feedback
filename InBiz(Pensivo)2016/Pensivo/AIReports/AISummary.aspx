﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AISummary.aspx.cs" Inherits="AIReports_AISummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link rel="stylesheet" href="../_styles/ai-reports.css" />
    <style  type="text/css">
      

    </style>

<script  type="text/javascript">
    function confirmData()
    {
        $( "#<%=btnYes.ClientID %>" ).removeClass( 'btnChageColor' );
        $( "#<%=btnYes.ClientID %>" ).addClass( 'btn' );
        ShowPensivoMessage( "<p style='text-align:justify;'><%=Resources.Resource.lblAIsummaryAlert%><p>" );
        return false;
    }
    function confirmDataYes()
    {  
        $( "#<%=btnYes.ClientID %>" ).removeClass( 'btn' );
        $( "#<%=btnYes.ClientID %>" ).addClass( 'btnChageColor' );    
        return false;
    }
     

</script>

<section id="main-content">
<span class="mask"></span>
<div class="wrapper">
	<div class="plms-alert secondary-clr">
	<p>         
        <%=Resources.Resource.lblAccidentIncident %>
        <strong class="employee-name">
                         <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
    </p>

    </div><!--End .plms-alert-->

	<div class="boxed-content"  >		
		<h2><%=Resources.Resource.lblSummary %></h2>		
		<div class="boxed-content-body">			
		<h3 class="h4"> <%=Resources.Resource.lblYourAns %> </h3>
		<div class="plms-table">
		<table>
		<tr>
		<th><%=Resources.Resource.lblAffectedEmployee %>    </th>
		<td><asp:label  ID="lblEmpNameD"  runat="server"></asp:label> (<%=Resources.Resource.lblAISite %> <asp:label  ID="lblSiteNo"  runat="server"></asp:label> , <%=Resources.Resource.lblEmployeeID %>  <asp:label  ID="lblEmpIDs"  runat="server"></asp:label>)</td>
		</tr>		
		<tr>
		<th><%=Resources.Resource.lblSummarySite %>               
             </th>
		<td><asp:label  ID="lblSiteno1"  runat="server"></asp:label></td>
		</tr>
		
		<tr>
		<th>  <%=Resources.Resource.lblSummaryQuestionText1 %>   </th>
		<td><asp:Label  ID="lblType"  runat="server"></asp:Label></td>
		</tr>
		
		<tr>
		<th><%=Resources.Resource.lblSummaryQuestionText2 %> </th>
		<td><asp:Label  ID="lblIsLicensee"  runat="server"></asp:Label></td>
		</tr>
		
		<tr>
		<th><%=Resources.Resource.lblSummaryQuestionText3 %> </th>
		<td><asp:Label  ID="lblTypeOfincident"  runat="server"></asp:Label></td>
		</tr>
		</table>
		</div><!--End .plms-table-->

		<div class="boxed-content">
		<div class="column span-9">
			<div class="alert secondary-clr">
			<p>
                <%=Resources.Resource.lblSummaryQuestionText4 %>                 
            </p>
			</div><!--End .alert-->
		</div><!--End .column-->

     		
		<div class="column span-3">
			<div class="btngrp align-r">  
                <asp:Button ID="btnYes"  runat="server"  title= "<%$ Resources:Resource, lblYes %>" Text="<%$ Resources:Resource, lblYes %>" class="btn" OnClick="btnYes_Click"  />
                <asp:Button ID="btnNo"  runat="server"  title= "<%$ Resources:Resource, lblNo %>" Text="<%$ Resources:Resource, lblNo %>" class="btn"  OnClientClick="return confirmData();" />
           

			</div><!--End .btngrp-->
		</div><!--End .column-->
		</div><!--End .boxed-content-->
				
		</div><!--End .boxed-content-body-->
		
		<nav class="pagination-nav"> 

		    <asp:Button ID="btnPrevoius"  runat="server" title= "<%$ Resources:Resource, lblPrevoius  %>" Text="<%$ Resources:Resource, lblPrevoius %>" class="btn large" OnClick="btnPrevoius_Click"  />
		
       <%--     <asp:Button ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />--%>

                   
		</nav><!--End .pagination-nav-->
	
	</div><!--End .boxed-content-->
	
	<footer>  
	<asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn" OnClick="btnSubmitwithoutSaving_Click" />
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
        
      
     
</asp:Content>

