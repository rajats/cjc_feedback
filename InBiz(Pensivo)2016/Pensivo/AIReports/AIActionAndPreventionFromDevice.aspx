﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIActionAndPreventionFromDevice.aspx.cs" Inherits="AIReports_AIActionAndPreventionFromDevice" %>

<%@ Register Src="~/AIReports/UserControl/IncidentMenu.ascx" TagName="UC1" TagPrefix="UC1Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="../_styles/ai-reports.css" />

    <script type="text/javascript">
        function GetAIAction()
        {
            $.ajax( {
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "ActionPreventionCorrection" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function ( data )
                {
                    var index = 0;
                    $.each( data.d, function ( index, value )
                    {
                        if ( index == 0 )
                        {
                            $( "#<%=hdQuestionID1.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText1" ).html( value.QuestionText );
                      }
                      if ( index == 1 )
                      {
                          $( "#<%=hdQuestionID2.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText2" ).html( value.QuestionText );
                      }

                      if ( index == 2 )
                      {
                          $( "#<%=hdQuestionID3.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText3" ).html( value.QuestionText );
                      }
                      if ( index == 3 )
                      {
                          $( "#<%=hdQuestionID4.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText4" ).html( value.QuestionText );
                      }

                      if ( index == 4 )
                      {
                          $( "#<%=hdQuestionID5.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText5" ).html( value.QuestionText );
                      }
                      if ( index == 5 )
                      {
                          $( "#<%=hdQuestionID6.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText6" ).html( value.QuestionText );
                      }
                      if ( index == 6 )
                      {
                          $( "#<%=hdQuestionID7.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText7" ).html( value.QuestionText );
                      }
                      if ( index == 7 )
                      {
                          $( "#<%=hdQuestionID8.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText8" ).html( value.QuestionText );
                      }

                      if ( index == 8 )
                      {
                          $( "#<%=hdQuestionID9.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText9" ).html( value.QuestionText );
                      }

                      if ( index == 9 )
                      {
                          $( "#<%=hdQuestionID10.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText10" ).html( value.QuestionText );
                          $( "#<%=txtothertextinput.ClientID%>" ).attr( "placeholder", value.QuestionText );
                          $( "#QuestionIDText10" ).html( value.QuestionText );
                      }
                      if ( index == 10 )
                      {
                          $( "#<%=hdQuestionID11.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText11" ).html( value.QuestionText );
                          $( "#<%=txtdatecompleted.ClientID%>" ).attr( "placeholder", value.QuestionText );
                          $( "#hdQuestionIDText2ToolTip" ).html( value.QuestionText );

                      }
                      index = parseInt( index ) + 1;
                  } );
              },
              error: function ( XMLHttpRequest, textStatus, errorThrown )
              {
                  //alert( errorThrown );
              }
          } );
      }
      function validate()
      {
          var bSelected = document.getElementById( "ContentPlaceHolder1_chkbox9" ).checked;
          if ( bSelected )
          {               
              $( "#<%=dvOthers.ClientID %>" ).show();
          }
          else
          {        
              $( "#<%=dvOthers.ClientID %>" ).hide();
          }
       }

      $( document ).ready( function ()
      {
         GetAIAction();
      });
    </script>


    <section id="main-content" class="pg-ca-preventions">
        <span class="mask"></span>
        <div class="wrapper">
            <div class="plms-alert secondary-clr">
                <p>
                    <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->


            <div class="plms-alert neutral">
                <p class="mandatory-fields-message">
                    <i class="req-icon" title="Mandatory Field">*</i>
                    <%=Resources.Resource.lblMandatoryField %>
                </p>
            </div>
            <!--End .plms-alert-->


            <div class="layout-sidebar-right">

                <div class="boxed-content">

                    <div class="boxed-content-body">

                        <h2><%=Resources.Resource.lblAactionAndPreventionText %></h2>

                        <p><%=Resources.Resource.lblPreventionCorrectionText1 %> </p>

                        <p class="small-text pull-up"><%=Resources.Resource.lblPreventionCorrectionText2 %>  </p>

                        <div class="label-checkbox-group">

                            <asp:HiddenField ID="hdQuestionID1" runat="server" />
                            <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox1">
                                <input type="checkbox" id="chkbox1" name="location-type" runat="server" /><span id="QuestionIDText1"></span></label>

                            <asp:HiddenField ID="hdQuestionID2" runat="server" />
                            <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox2">
                                <input type="checkbox" id="chkbox2" name="arrangement-congestion" runat="server" /><span id="QuestionIDText2"></span></label>

                            <asp:HiddenField ID="hdQuestionID3" runat="server" />
                            <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox3">
                                <input type="checkbox" id="chkbox3" name="arrangement-congestion" runat="server" /><span id="QuestionIDText3"></span></label>

                            <asp:HiddenField ID="hdQuestionID4" runat="server" />
                            <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox4">
                                <input type="checkbox" id="chkbox4" name="arrangement-congestion" runat="server" /><span id="QuestionIDText4"></span></label>

                            <asp:HiddenField ID="hdQuestionID5" runat="server" />
                            <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox5">
                                <input type="checkbox" id="chkbox5" name="arrangement-congestion" runat="server" /><span id="QuestionIDText5"></span></label>

                            <asp:HiddenField ID="hdQuestionID6" runat="server" />
                            <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox6">
                                <input type="checkbox" id="chkbox6" name="arrangement-congestion" runat="server" /><span id="QuestionIDText6"></span></label>

                            <asp:HiddenField ID="hdQuestionID7" runat="server" />
                            <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox7">
                                <input type="checkbox" id="chkbox7" name="arrangement-congestion" runat="server" /><span id="QuestionIDText7"></span></label>

                            <asp:HiddenField ID="hdQuestionID8" runat="server" />
                            <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox8">
                                <input type="checkbox" id="chkbox8" name="arrangement-congestion" runat="server" /><span id="QuestionIDText8"></span></label>

                            <asp:HiddenField ID="hdQuestionID9" runat="server" />
                            <label class="checkbox-input-label" for="ContentPlaceHolder1_chkbox9">
                                <input type="checkbox" id="chkbox9" name="arrangement-congestion" runat="server" onchange="validate();" /><span id="QuestionIDText9"></span></label>

                            <!--ActionPreventionCorrection>--> 
                        </div>
                        <!--End .label-checkbox-group-->
                        
                        <div id="dvOthers" runat="server" class="divHide">
                            <div class="plms-fieldset">
                                <asp:HiddenField ID="hdQuestionID10" runat="server" />
                                <label class="plms-label is-hidden" for="other-text-input" id="QuestionIDText10"></label>
                                <div class="plms-tooltip-parent">
                                    <i class="req-icon" title="Mandatory Field">*</i>
                                    <input type="text" class="plms-input skin2" id="txtothertextinput" name="other-text-input" runat="server" />
                                    <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                        <div class="plms-tooltip-body">
                                            <p id="hdQuestionIDText10ToolTip"></p>
                                        </div>
                                        <!--End .plms-tooltip-body-->
                                    </div>
                                    <!--End .plms-tooltip-->
                                </div>
                                <!--End .plms-tooltip-parent-->
                            </div>
                            <!--End .plms-fieldset-->
                        </div>


                        <div class="plms-fieldset">
                            <asp:HiddenField ID="hdQuestionID11" runat="server" />
                            <label class="plms-label is-hidden" for="date-completed" id="QuestionIDText11"></label>
                            <div class="plms-tooltip-parent">
                                <input type="text" class="plms-input datepicker skin2" id="txtdatecompleted" name="date-completed" runat="server" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="hdQuestionIDText2ToolTip"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                    </div>
                    <!--End .boxed-content-body-->

                    <nav class="pagination-nav">

                        <%--<a href="AIActionAndPreventionConfirm.aspx" title="Previous" class="btn large"  runat="server">Previous</a>
			<a href="AIWitnessesCoWorker.aspx" title="Next" class="btn large align-r"  runat="server" >Next</a>--%>


                        <asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevoius  %>" Text="<%$ Resources:Resource, lblPrevoius %>" class="btn large" OnClick="btnPrevious_Click" />
                        <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />


                    </nav>
                    <!--End .pagination-nav-->

                </div>
                <!--End .boxed-content-->

                <aside id="main-aside">
                    <div class="aside-toggle-handle">
                        <div class="flyout-message-wrapper">
                            <a href="#toggle-aside" class="btn">
                                <i class="icon"></i>
                                <span><%= Resources.Resource.lblHideMenu %></span>
                            </a>
                        </div>
                        <!--End .flyout-message-wrapper-->
                    </div>
                    <!--End .aside-toggle-handle-->

                    <nav class="aside-main-nav">
                        <UC1Menu:UC1 ID="lblmenu" runat="server" />

                    </nav>
                    <!--End .aside-main-nav-->
                </aside>
                <!--End #main-aside-->

            </div>
            <!--End .layout-sidebar-right-->

            <footer>
                <div class="btngrp">

                    <asp:Button ID="btnSubmitandExitLater" runat="server" title="<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click" />
                    <asp:Button ID="btnSubmitMyReport" runat="server" title="<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click" />


                    <%--	<a href="#nogo" class="btn" title="Save & Exit to Continue Later">Save &amp; Exit to Continue Later</a>
	<a href="#nogo" class="btn" title="Submit My Report">Submit My Report</a>--%>
                </div>
            </footer>

        </div>
        <!--End .wrapper-->
    </section>
    <!--End #main-content-->
    <script>
        var minYear = '-' +  <%= Resources.Resource.minyearinmonth %> + 'M';
        ( function ()
        {
            $( ".datepicker" ).datepicker( {
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                yearRange: GetDateRange(),
                maxDate: '+0D',
            } );
        } )();

        function GetDateRange()
        {
            var currentDate = new Date();
            var currentYear = currentDate.getFullYear();
            var previousYear = currentDate.getFullYear() - 10;
            return previousYear + ':' + currentYear;
        }
    </script>

</asp:Content>

