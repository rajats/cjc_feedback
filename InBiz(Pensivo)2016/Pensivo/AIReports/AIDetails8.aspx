﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIDetails8.aspx.cs" Inherits="AIReports_AIDetails8" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	  <link rel="stylesheet" href="../_styles/ai-reports.css" />
     <script   type="text/javascript">
         function GetAIDetails5()
         {
             $.ajax( {
                 type: "POST",
                 url: "CommonInterface.aspx/getQuestions",
                 data: "{pageType:'" + "IncidentAccidentDetail7" + "'}",
                 dataType: "json",
                 contentType: "application/json; charset=utf-8",
                 success: function ( data )
                 {
                     var index = 0;
                     $.each( data.d, function ( index, value )
                     {
                         if ( index == 0 )
                         {
                             $( "#<%=hdAIQuestionID1.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionText1" ).html( value.QuestionText );                             
                             $( "#<%=txtaddress.ClientID%>" ).attr( "placeholder", value.QuestionText );
                             $( "#AIQuestionText1ToolTip" ).html( value.QuestionText ); 
                         } 
                     });
                 },
                 error: function ( XMLHttpRequest, textStatus, errorThrown )
                 {
                     //alert( errorThrown );
                 }
             } );
         }
         $( document ).ready( function ()
         {
             GetAIDetails5();
         } );
          </script>   

 <section id="main-content">
<span class="mask"></span>
<div class="wrapper">

	<div class="plms-alert secondary-clr">
		 <p> 
             <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
         </p>	
	</div><!--End .plms-alert-->
	
	 <div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		  <%=Resources.Resource.lblMandatoryField %></p>
	</div><!--End .plms-alert -->
	

	<div class="layout-sidebar-right">

		<div class="boxed-content">
			
			<div class="boxed-content-body">				
			<h2>
                <%= Resources.Resource.lblAccidentIncident %>
			</h2>
			
			<div class="plms-fieldset pull-up-10">
                <asp:HiddenField  ID="hdAIQuestionID1"   runat="server"/>               
				<label class="plms-label is-hidden" for="address" id="AIQuestionText1"></label>	 
				<div class="plms-tooltip-parent">
					<i class="req-icon" title="Mandatory Field">*</i>
					<textarea rows="4" cols="4" class="plms-textarea skin2" 
					id="txtaddress" name="txtaddress"     runat="server"></textarea>					
					<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
						<div class="plms-tooltip-body">
							<p  id="AIQuestionText1ToolTip"> </p>
						</div><!--End .plms-tooltip-body-->
					</div><!--End .plms-tooltip-->
				</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->								
			</div><!--End .boxed-content-body-->
             
			
			<nav class="pagination-nav">
                    <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"      />
                    <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"     />
    		</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span><%=Resources.Resource.lblHideMenu %></span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
				 <UC1Menu:UC1  ID="lblmenu"   runat="server"/>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
    <%-- <a href="#nogo" class="btn" title="Save & Exit to Continue Later">Save &amp; Exit to Continue Later</a>
	<a href="#nogo" class="btn" title="Submit My Report">Submit My Report</a>--%>

         <asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click"  /> 
         <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click"    />
         
	</div>
	</footer> 
</div><!--End .wrapper-->
</section><!--End #main-content-->
</asp:Content>

