﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;



public partial class AIReports_AIValidationSummary : System.Web.UI.Page
{
    List<Employee> empObj = new List<Employee>();
    int aiFormID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }

            if (!string.IsNullOrEmpty(Convert.ToString(Request.UrlReferrer)))
            {
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
            }
            else
            {
                Response.Redirect("AILocationInfo.aspx", false);
            }

            CheckAnswers();
        }
    }   
    private void CheckAnswers()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
             objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
             if (objList != null)
             {
                 var result = objList.Where(x => x.AIQuestionAnswerText == "1").ToList();
                 if (result != null && result.Count > 0)
                 {
                     Response.Redirect("AISummary.aspx"); 
                 }                
             } 
        }
        catch { }
        finally { } 
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {

    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        object refUrl = ViewState["RefUrl"];
        if (refUrl != null)
            Response.Redirect((string)refUrl);
    }    
    protected void btnBackToIncident_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIIncidentdesc.aspx", false);
    }
}