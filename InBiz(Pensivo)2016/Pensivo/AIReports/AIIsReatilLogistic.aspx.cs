﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIIsReatilLogistic : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        // Redirect to exit or done(As per working Flow)            
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.IsReatilLogistic).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdlocationQuestion1.Value)))
                {
                    objList.Add(
                 new AIQuestionAnswer
                 {
                     AIQuestionID = Convert.ToInt32(hdlocationQuestion1.Value),
                     AIQuestionType = QuestionType.TextType,
                     AIQuestionAnswerText = Convert.ToString(Request.Form["locationtype"]),
                     AIFormID = aiFormID,
                     AIPageName = PageName.IsReatilLogistic
                 });
                }


                Session["AIReportQuestions"] = objList;

                if (Convert.ToString(Request.Form["locationtype"]) == "R")
                {
                    Response.Redirect("AIBeforeGetStarted.aspx", false);
                }
                else
                {
                    Response.Redirect("AIIsLogistic.aspx", false);
                }
            }
        }
        catch { }
        finally { }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AILocationInfo.aspx", false);
    }
}