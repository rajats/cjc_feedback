﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIDashboard.aspx.cs" Inherits="AIReports_AIDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
 <section id="main-content" class="pg-dashboard"> 
<span class="mask"></span>
<div class="wrapper">
<h1>Accident / Incident Reports</h1>
<div class="boxset">
	<div class="boxset-box">
		<a href="AIEmailValidation.aspx" class="boxset-box-wrapper"  runat="server">
			<div class="boxset-box-inner">
			    <h4 class="boxset-title">Start A New AI</h4>
			</div><!--End .boxset-box-inner-->
		</a><!--End .boxset-box-wrapper-->
	</div><!--End .boxset-box-->
	
	<div class="boxset-box">
		<a href="#nogo" class="boxset-box-wrapper">
			<div class="boxset-box-inner">
			    <h4 class="boxset-title">Continue A Previously Started AI</h4>
			</div><!--End .boxset-box-inner-->
		</a><!--End .boxset-box-wrapper-->
	</div><!--End .boxset-box-->
	
	<div class="boxset-box">
		<a href="#nogo" class="boxset-box-wrapper">
			<div class="boxset-box-inner">
			    <h4 class="boxset-title">Re-Open an AI</h4>
			</div><!--End .boxset-box-inner-->
		</a><!--End .boxset-box-wrapper-->
	</div><!--End .boxset-box-->
</div><!--End .boxset-->

</div><!--End .wrapper-->
</section> 
</asp:Content>

