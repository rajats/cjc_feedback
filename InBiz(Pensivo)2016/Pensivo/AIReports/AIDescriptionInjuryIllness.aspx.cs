﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIDescriptionInjuryIllness : System.Web.UI.Page
{
    List<Employee> empObj = new List<Employee>();
    int aiFormID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            this.getFormData();
        }
    }

    private void getFormData()
    {
      List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
      try
      {
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        if (objList != null)
        {
          var resultItem = objList.Where(x => x.AIPageName == PageName.InjuryIllness).OrderBy(x => x.AIQuestionID).ToList();
          if (resultItem != null && resultItem.Count > 0)
          {
            int count = 0;
            foreach (var result in resultItem)
            {
              if (count == 0 && result.AISequence == 1)
              {
                if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                {
                  hdInjuryIllnessQuestAns1.Value = "0";
                  ancAncleLeft.Attributes.Remove("Class");
                  ancAncleLeft.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                {
                  hdInjuryIllnessQuestAns1.Value = "1";
                  ancAncleRight.Attributes.Remove("Class");
                  ancAncleRight.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1,0" || Convert.ToString(result.AIQuestionAnswerText) == "0,1")
                {
                  hdInjuryIllnessQuestAns1.Value = "1,0";
                  ancAncleLeft.Attributes.Remove("Class");
                  ancAncleLeft.Attributes.Add("Class", "btnChageColor");
                  ancAncleRight.Attributes.Remove("Class");
                  ancAncleRight.Attributes.Add("Class", "btnChageColor");
                }
              }

              else if (count == 1 && result.AISequence == 2)
              {
                if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                {
                  hdInjuryIllnessQuestAns2.Value = "0";
                  ancArmLeft.Attributes.Remove("Class");
                  ancArmLeft.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                {
                  hdInjuryIllnessQuestAns2.Value = "1";
                  ancArmRight.Attributes.Remove("Class");
                  ancArmRight.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1,0" || Convert.ToString(result.AIQuestionAnswerText) == "0,1")
                {
                  hdInjuryIllnessQuestAns2.Value = "1,0";
                  ancArmLeft.Attributes.Remove("Class");
                  ancArmLeft.Attributes.Add("Class", "btnChageColor");
                  ancArmRight.Attributes.Remove("Class");
                  ancArmRight.Attributes.Add("Class", "btnChageColor");
                }
              }

              else if (result.AISequence == 3)
              {
                if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                {
                  hdInjuryIllnessQuestAns3.Value = "0";
                  ancEarLeft.Attributes.Remove("Class");
                  ancEarLeft.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                {
                  hdInjuryIllnessQuestAns3.Value = "1";
                  ancEarRight.Attributes.Remove("Class");
                  ancEarRight.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1,0" || Convert.ToString(result.AIQuestionAnswerText) == "0,1")
                {
                  hdInjuryIllnessQuestAns3.Value = "1,0";
                  ancEarLeft.Attributes.Remove("Class");
                  ancEarLeft.Attributes.Add("Class", "btnChageColor");
                  ancEarRight.Attributes.Remove("Class");
                  ancEarRight.Attributes.Add("Class", "btnChageColor");
                }
              }

              else if (result.AISequence == 4)
              {
                if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                {
                  hdInjuryIllnessQuestAns4.Value = "0";
                  ancEyeLeft.Attributes.Remove("Class");
                  ancEyeLeft.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                {
                  hdInjuryIllnessQuestAns4.Value = "1";
                  ancEyeRight.Attributes.Remove("Class");
                  ancEyeRight.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1,0" || Convert.ToString(result.AIQuestionAnswerText) == "0,1")
                {
                  hdInjuryIllnessQuestAns4.Value = "1,0";
                  ancEyeLeft.Attributes.Remove("Class");
                  ancEyeLeft.Attributes.Add("Class", "btnChageColor");
                  ancEyeRight.Attributes.Remove("Class");
                  ancEyeRight.Attributes.Add("Class", "btnChageColor");
                }
              }

              else if (result.AISequence == 5)
              {
                if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                {
                  hdInjuryIllnessQuestAns5.Value = "0";
                  ancFaceLeft.Attributes.Remove("Class");
                  ancFaceLeft.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                {
                  hdInjuryIllnessQuestAns5.Value = "1";
                  ancFaceRight.Attributes.Remove("Class");
                  ancFaceRight.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1,0" || Convert.ToString(result.AIQuestionAnswerText) == "0,1")
                {
                  hdInjuryIllnessQuestAns5.Value = "1,0";
                  ancFaceLeft.Attributes.Remove("Class");
                  ancFaceLeft.Attributes.Add("Class", "btnChageColor");
                  ancFaceRight.Attributes.Remove("Class");
                  ancFaceRight.Attributes.Add("Class", "btnChageColor");
                }
              }

              else if (result.AISequence == 6)
              {
                if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                {
                  hdInjuryIllnessQuestAns6.Value = "0";
                  ancFootLeft.Attributes.Remove("Class");
                  ancFootLeft.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                {
                  hdInjuryIllnessQuestAns6.Value = "1";
                  ancFootRight.Attributes.Remove("Class");
                  ancFootRight.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1,0" || Convert.ToString(result.AIQuestionAnswerText) == "0,1")
                {
                  hdInjuryIllnessQuestAns6.Value = "1,0";
                  ancFootLeft.Attributes.Remove("Class");
                  ancFootLeft.Attributes.Add("Class", "btnChageColor");
                  ancFootRight.Attributes.Remove("Class");
                  ancFootRight.Attributes.Add("Class", "btnChageColor");
                }
              }

              else if (result.AISequence == 7)
              {
                if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                {
                  hdInjuryIllnessQuestAns7.Value = "0";
                  ancHandLeft.Attributes.Remove("Class");
                  ancHandLeft.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                {
                  hdInjuryIllnessQuestAns7.Value = "1";
                  ancHandRight.Attributes.Remove("Class");
                  ancHandRight.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1,0" || Convert.ToString(result.AIQuestionAnswerText) == "0,1")
                {
                  hdInjuryIllnessQuestAns7.Value = "1,0";
                  ancHandLeft.Attributes.Remove("Class");
                  ancHandLeft.Attributes.Add("Class", "btnChageColor");
                  ancHandRight.Attributes.Remove("Class");
                  ancHandRight.Attributes.Add("Class", "btnChageColor");
                }
              }

              else if (result.AISequence == 8)
              {
                if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                {
                  hdInjuryIllnessQuestAns8.Value = "0";
                  ancHeadLeft.Attributes.Remove("Class");
                  ancHeadLeft.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                {
                  hdInjuryIllnessQuestAns8.Value = "1";
                  ancHeadRight.Attributes.Remove("Class");
                  ancHeadRight.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1,0" || Convert.ToString(result.AIQuestionAnswerText) == "0,1")
                {
                  hdInjuryIllnessQuestAns8.Value = "1,0";
                  ancHeadLeft.Attributes.Remove("Class");
                  ancHeadLeft.Attributes.Add("Class", "btnChageColor");
                  ancHeadRight.Attributes.Remove("Class");
                  ancHeadRight.Attributes.Add("Class", "btnChageColor");
                }
              }

              else if (result.AISequence == 9)
              {
                if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                {
                  hdInjuryIllnessQuestAns9.Value = "0";
                  ancKneeLeft.Attributes.Remove("Class");
                  ancKneeLeft.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                {
                  hdInjuryIllnessQuestAns9.Value = "1";
                  ancKneeRight.Attributes.Remove("Class");
                  ancKneeRight.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1,0" || Convert.ToString(result.AIQuestionAnswerText) == "0,1")
                {
                  hdInjuryIllnessQuestAns9.Value = "1,0";
                  ancKneeLeft.Attributes.Remove("Class");
                  ancKneeLeft.Attributes.Add("Class", "btnChageColor");
                  ancKneeRight.Attributes.Remove("Class");
                  ancKneeRight.Attributes.Add("Class", "btnChageColor");
                }
              }

              else if (result.AISequence == 10)
              {
                if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                {
                  hdInjuryIllnessQuestAns10.Value = "0";
                  ancShoulderLeft.Attributes.Remove("Class");
                  ancShoulderLeft.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                {
                  hdInjuryIllnessQuestAns10.Value = "1";
                  ancShoulderRight.Attributes.Remove("Class");
                  ancShoulderRight.Attributes.Add("Class", "btnChageColor");
                }
                else if (Convert.ToString(result.AIQuestionAnswerText) == "1,0" || Convert.ToString(result.AIQuestionAnswerText) == "0,1")
                {
                  hdInjuryIllnessQuestAns10.Value = "1,0";
                  ancShoulderLeft.Attributes.Remove("Class");
                  ancShoulderLeft.Attributes.Add("Class", "btnChageColor");
                  ancShoulderRight.Attributes.Remove("Class");
                  ancShoulderRight.Attributes.Add("Class", "btnChageColor");
                }
              }

              else if (result.AISequence == 11 && result.AIQuestionAnswerText == "1")
              {
                chkOthers.Checked = true;
                dvOthers.Attributes.Remove("Class");
                dvOthers.Attributes.Add("Class", "divShow");
              }
              else if (result.AISequence == 12)
              {
                txtOthers.Value = result.AIQuestionAnswerText;

              }

              else if (count == 0 && result.AISequence == 13)
              {
                chkNone.Checked = true;
              }

              count++;
            }
          }
        }
      }
      catch { }
      finally { }
    }

    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {

                // Check Is Licensee
                var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (ISLicensee != null)
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }

                    // Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null &&  IsPropertyDamage != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AILicenseeDetails.aspx", false);
                        return;
                    }

                }
                else
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null &&  IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }

                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIDetails6.aspx", false);
                        return;
                    }
                }
            }
        }
        catch { }
        finally { }

        //if (objList != null)
        //{

        //    var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
        //    if (ISLicensee != null)
        //    {
        //        // Redirect Case: WVH:No, PI:Yes, FirstAidBox:Yes, PropertDamage:Yes
        //        var getresult = objList.Where(x => x.AIPageName == PageName.PersonalInjury || x.AIPageName == PageName.FirstAidBox || x.AIPageName == PageName.PropertyDamageQuestion).ToList();
        //        if (getresult != null && getresult.Count == 3)
        //        {
        //            var result = getresult.Where(x => x.AIQuestionAnswerText == "1").ToList();
        //            if (result != null && result.Count == 3)
        //            {
        //                Response.Redirect("AILicenseeDetails.aspx", false);
        //                return;
        //            }
        //        }

        //        // Case : WVH :No, PI:Yes, Lost Time  :  Yes, Property Damage:Yes
        //        var Case2result = objList.Where(x => x.AIPageName == PageName.PersonalInjury || x.AIPageName == PageName.LostTime || x.AIPageName == PageName.PropertyDamageQuestion).ToList();
        //        if (Case2result != null && Case2result.Count == 3)
        //        {
        //            var fresult2 = Case2result.Where(x => x.AIQuestionAnswerText == "1").ToList();
        //            if (fresult2 != null && fresult2.Count == 3)
        //            {
        //                Response.Redirect("HealthCareDetails.aspx", false);
        //                return;
        //            }
        //        }

        //        // Case  :  WVH :No,  Personal Injury:Yes,  Logistic:Yes,  Licensee:Yes
        //        var caseIsPI = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
        //        if (caseIsPI != null)
        //        {
        //            Response.Redirect("AILicenseeDetails.aspx", false);
        //            return;
        //        }
        //    }
        //    else
        //    {
        //        // Case : WVH :No, PI:Yes, Lost Time  :  Yes, Property Damage:Yes
        //        var getresult = objList.Where(x => x.AIPageName == PageName.PersonalInjury || x.AIPageName == PageName.LostTime || x.AIPageName == PageName.PropertyDamageQuestion).ToList();
        //        if (getresult != null && getresult.Count == 3)
        //        {
        //            var fresult = getresult.Where(x => x.AIQuestionAnswerText == "1").ToList();
        //            if (fresult != null && fresult.Count == 3)
        //            {
        //                Response.Redirect("HealthCareDetails.aspx", false);
        //                return;
        //            }
        //        }
        //        // Case : WVH :No, PI:Yes, FirstAId:Yes, Property Damage:Yes
        //        var Case3Result = objList.Where(x => x.AIPageName == PageName.PersonalInjury || x.AIPageName == PageName.FirstAidBox || x.AIPageName == PageName.PropertyDamageQuestion).ToList();
        //        if (Case3Result != null && Case3Result.Count == 3)
        //        {
        //            var Result = Case3Result.Where(x => x.AIQuestionAnswerText == "1").ToList();
        //            if (Result != null && Result.Count == 3)
        //            {
        //                Response.Redirect("AIDetails6.aspx", false);
        //                return;
        //            }
        //        }
        //        // Case : WVH:No, PI:Yes,FirstAid:Yes,Location:Retail
        //        var Case2Result = objList.Where(x => x.AIPageName == PageName.PersonalInjury || x.AIPageName == PageName.FirstAidBox).ToList();
        //        if (Case2Result != null && Case2Result.Count == 2)
        //        {
        //            var Result = Case2Result.Where(x => x.AIQuestionAnswerText == "1").ToList();
        //            if (Result != null && Result.Count == 2)
        //            {
        //                Response.Redirect("AIDetails6.aspx", false);
        //                return;
        //            }
        //        }
        //        // Case : WVH:No, PI:Yes,FirstAid:Yes,Location:Retail
        //        var Case4Result = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
        //        if (Case4Result != null)
        //        {
        //            Response.Redirect("AIDetails6.aspx", false);
        //            return;
        //        }
        //        else
        //        {
        //            Response.Redirect("AIReturntowork1.aspx", false);
        //            return;
        //        }
        //    } 
 
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.InjuryIllness).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }

                if (hdInjuryIllnessQuestAns13.Value != "1")
                {

                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest1.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns1.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest1.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns1.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 1
                        });
                  }

                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest2.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns2.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest2.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns2.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 2
                        });
                  }

                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest3.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns3.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest3.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns3.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 3
                        });
                  }

                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest4.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns4.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest4.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns4.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 4
                        });
                  }

                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest5.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns5.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest5.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns5.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 5
                        });
                  }

                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest6.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns6.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest6.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns6.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 6
                        });
                  }

                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest7.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns7.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest7.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns7.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 7
                        });
                  }

                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest8.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns8.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest8.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns8.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 8
                        });
                  }

                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest9.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns9.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest9.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns9.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 9
                        });
                  }

                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest10.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns10.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest10.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns10.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 10
                        });
                  }
                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest11.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns11.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest11.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns11.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 11
                        });
                  }
                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest12.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns11.Value) && hdInjuryIllnessQuestAns11.Value == "1")
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest11.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = txtOthers.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 12
                        });
                  }
                }
                else
                {
                  if (!string.IsNullOrEmpty(hdInjuryIllnessQuest13.Value) && !string.IsNullOrEmpty(hdInjuryIllnessQuestAns13.Value))
                  {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                          AIQuestionID = Convert.ToInt32(hdInjuryIllnessQuest11.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = hdInjuryIllnessQuestAns11.Value,
                          AIFormID = aiFormID,
                          AIPageName = PageName.InjuryIllness,
                          AISequence = 13
                        });
                  }
                }

                Session["AIReportQuestions"] = objList;
            }

        }
        catch (Exception ex) { throw ex; }
        finally { }

        managePreviousNext("");
        RedirectionLink();

        // Response.Redirect("AIDetails7.aspx", false);
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBoard.aspx", false);
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {
    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {
    }
    private void managePreviousNext(string NextPageUrl)
    {
        List<ManagePagePreviousNext> objPage = new List<ManagePagePreviousNext>();
        try
        {
            objPage = (List<ManagePagePreviousNext>)Session["AIPagesList"];
            if (objPage != null)
            {
                var resultItem = objPage.Where(x => x.pageName == PageName.InjuryIllness).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objPage.Remove(removeItem);
                    }
                }
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIDescriptionInjuryIllness.aspx", NextPageUrl = NextPageUrl, pageName = PageName.InjuryIllness, PreviousPageUrl = "" });
            }
            else
            {
                objPage = new List<ManagePagePreviousNext>();
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIDescriptionInjuryIllness.aspx", NextPageUrl = NextPageUrl, pageName = PageName.InjuryIllness, PreviousPageUrl = "" });
            }
            Session["AIPagesList"] = objPage;

        }
        catch { }
        finally { }
    }
    private void RedirectionLink()
    {        
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];

            if (objList != null)
            {

                // Check Is Licensee
                var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (ISLicensee != null)
                {

                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }

                    // Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null &&  IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }

                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                     

                }
                else
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AICauseofInjury.aspx", false);
                        return;
                    }

                     
                }
            }
        }
        catch { }
        finally { }
    }

}