﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIPersonalProtectiveEquipment.aspx.cs" Inherits="AIReports_AIPersonalProtectiveEquipment" %>

<%@ Register Src="~/AIReports/UserControl/IncidentMenu.ascx" TagName="UC1" TagPrefix="UC1Menu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
    <script type="text/javascript">
        var sPrefix = "ContentPlaceHolder1_";
        function GetAIContributingFactor()
        {
            $.ajax( {
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "PersonalProtectiveEquipment" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function ( data )
                {
                    var index = 0;
                    $.each( data.d, function ( index, value )
                    {
                        if ( index == 0 )
                        {
                            $( "#<%=hdQuestionID1.ClientID%>" ).val( value.QuestionID );
                            $( "#QuestionIDText1" ).html( value.QuestionText );
                        }
                        if ( index == 1 )
                        {
                            $( "#<%=hdQuestionID2.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText2" ).html( value.QuestionText );
                      }

                        if ( index == 2 )
                        {
                            $( "#<%=hdQuestionID3.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText3" ).html( value.QuestionText );
                      }
                        if ( index == 3 )
                        {
                            $( "#<%=hdQuestionID4.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText4" ).html( value.QuestionText );
                      }

                        if ( index == 4 )
                        {
                            $( "#<%=hdQuestionID5.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText5" ).html( value.QuestionText );
                      }
                        if ( index == 5 )
                        {
                            $( "#<%=hdQuestionID6.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText6" ).html( value.QuestionText );
                      }
                        if ( index == 6 )
                        {
                            $( "#<%=hdQuestionID7.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText7" ).html( value.QuestionText );
                      }
                        if ( index == 7 )
                        {
                            $( "#<%=hdQuestionID8.ClientID%>" ).val( value.QuestionID );
                          $( "#QuestionIDText8" ).html( value.QuestionText );
                      }
                        if ( index == 8 )
                        {
                            $( "#<%=hdQuestionID10.ClientID%>" ).val( value.QuestionID );
                          $( "#AIQuestionIDText10ToolTip" ).html( value.QuestionText );
                          $( "#<%=txtdelayreasoning.ClientID%>" ).attr( "placeholder", value.QuestionText );
                          $( "#QuestionIDText10" ).html( value.QuestionText );
                      }
                        if ( index == 9 )
                        {
                            $( "#<%=hdQuestionID9.ClientID%>" ).val( value.QuestionID );
                          $( "#txtOtherQuestionToolTip" ).html( value.QuestionText );
                          $( "#<%=txtOtherID.ClientID%>" ).attr( "placeholder", value.QuestionText );
                          $( "#txtOtherQuestion" ).html( value.QuestionText );
                      }

                        index = parseInt( index ) + 1;
                    } );
                },
                error: function ( XMLHttpRequest, textStatus, errorThrown )
                {
                    //alert( errorThrown );
                }
            } );
      }
      $( document ).ready( function ()
      {
          GetAIContributingFactor();

          
          document.getElementById( sPrefix + 'dvDelayreasoning' ).style.display = "none";
          document.getElementById( sPrefix + 'divChkSection' ).style.display = "none";
      } );

      function validate()
      {
          var bSelected = document.getElementById( "ContentPlaceHolder1_chkbox7" ).checked;

          if ( bSelected )
          {
              $( "#<%=dvOthers.ClientID %>" ).show();
          }
          else
          {
              $( "#<%=dvOthers.ClientID %>" ).hide();
          }
      }

      function ClickChange( ID, Value )
      {

          if ( ID == "ancAiQuestionD1Yes" )
          {
              $( "#<%=hdQuestionAnsID1.ClientID%>" ).val( "1" );

                 document.getElementById( sPrefix + 'dvDelayreasoning' ).style.display = "none";
                 document.getElementById( sPrefix + 'divChkSection' ).style.display = "block";
                 validate();
                 $( "#<%=ancAiQuestionD1Yes.ClientID%>" ).removeClass( 'btn' );
                 $( "#<%=ancAiQuestionD1Yes.ClientID%>" ).addClass( 'btnChageColor' );
                 if ( $( "#<%=ancAiQuestionD1No.ClientID%>" ).hasClass( "btnChageColor" ) )
                 {
                     $( "#<%=ancAiQuestionD1No.ClientID%>" ).removeClass( 'btnChageColor' );
                     $( "#<%=ancAiQuestionD1No.ClientID%>" ).addClass( 'btn' );
                 }


                 $( "#<%=txtdelayreasoning.ClientID%>" ).val( " " );
             }

             if ( ID == "ancAiQuestionD1No" )
             {
                 $( "#<%=hdQuestionAnsID1.ClientID%>" ).val( "0" );

                 document.getElementById( sPrefix + 'dvDelayreasoning' ).style.display = "block";
                 document.getElementById( sPrefix + 'divChkSection' ).style.display = "none";
                 document.getElementById( sPrefix + 'dvOthers' ).style.display = "none";

                 $( "#<%=ancAiQuestionD1No.ClientID%>" ).removeClass( 'btn' );
                 $( "#<%=ancAiQuestionD1No.ClientID%>" ).addClass( 'btnChageColor' );
                 if ( $( "#<%=ancAiQuestionD1Yes.ClientID%>" ).hasClass( "btnChageColor" ) )
                 {
                     $( "#<%=ancAiQuestionD1Yes.ClientID%>" ).removeClass( 'btnChageColor' );
                     $( "#<%=ancAiQuestionD1Yes.ClientID%>" ).addClass( 'btn' );
                 }
             }

         }

    </script>

    <section id="main-content">
        <span class="mask"></span>
        <div class="wrapper">

            <div class="plms-alert secondary-clr">
                <p>
                    <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->


            <div class="plms-alert neutral">
                <p class="mandatory-fields-message">
                    <i class="req-icon" title="Mandatory Field">*</i>
                    <%=Resources.Resource.lblMandatoryField %>
                </p>

            </div>

            <!--End .plms-alert-->
            <div class="layout-sidebar-right">
                <div class="boxed-content">
                    <div class="boxed-content-body">
                        <h2><%=Resources.Resource.lblPersonalProEquipmentTitle %></h2>

                        <div class="fieldset-group">
                            <%--<p class="bold">   <%=Resources.Resource.lbPersoanlEquipmentTitle %> </p>--%>

                            <div class="plms-fieldset-wrapper push-up">
                                <asp:HiddenField ID="hdQuestionID1" runat="server" />
                                <asp:HiddenField ID="hdQuestionAnsID1" runat="server" />
                                <div class="column span-8">

                                    <p class="bold" id="QuestionIDText1"></p>
                                </div>
                                <!--End .column-->

                                <div class="column span-4">
                                    <div class="btngrp align-r">
                                       
                                        <a id="ancAiQuestionD1Yes" runat="server" class="btn" title="Yes" onclick="ClickChange('ancAiQuestionD1Yes', this.text);">Yes</a>
                                        <a id="ancAiQuestionD1No" runat="server" class="btn" title="No" onclick="ClickChange('ancAiQuestionD1No', this.text);">No</a>
                                    </div>
                                    <!--End .btngrp-->
                                </div>
                                <!--End .column-->
                            </div>

                            <div  id="divChkSection" runat="server"  class="divHide">
                                <div class="label-checkbox-group">
                                <p class="bold"><%=Resources.Resource.lbPersoanlEquipmentTitle %> </p>
                                <asp:HiddenField ID="hdQuestionID2" runat="server" />
                                <label id="Label1" class="checkbox-input-label" for="ContentPlaceHolder1_chkbox1" runat="server">
                                    <input type="checkbox" id="chkbox1" name="location-type" runat="server" /><span id="QuestionIDText2"></span></label>
                                <asp:HiddenField ID="hdQuestionID3" runat="server" />
                                <label id="Label2" class="checkbox-input-label" for="ContentPlaceHolder1_chkbox2" runat="server">
                                    <input type="checkbox" id="chkbox2" name="arrangement-congestion" runat="server" /><span id="QuestionIDText3"></span></label>
                                <asp:HiddenField ID="hdQuestionID4" runat="server" />
                                <label id="Label3" class="checkbox-input-label" for="ContentPlaceHolder1_chkbox3" runat="server">
                                    <input type="checkbox" id="chkbox3" name="arrangement-congestion" runat="server" /><span id="QuestionIDText4"></span></label>
                                <asp:HiddenField ID="hdQuestionID5" runat="server" />
                                <label id="Label4" class="checkbox-input-label" for="ContentPlaceHolder1_chkbox4" runat="server">
                                    <input type="checkbox" id="chkbox4" name="arrangement-congestion" runat="server" /><span id="QuestionIDText5"></span></label>
                                <asp:HiddenField ID="hdQuestionID6" runat="server" />
                                <label id="Label5" class="checkbox-input-label" for="ContentPlaceHolder1_chkbox5" runat="server">
                                    <input type="checkbox" id="chkbox5" name="arrangement-congestion" runat="server" /><span id="QuestionIDText6"></span></label>
                                <asp:HiddenField ID="hdQuestionID7" runat="server" />
                                <label id="Label6" class="checkbox-input-label" for="ContentPlaceHolder1_chkbox6" runat="server">
                                    <input type="checkbox" id="chkbox6" name="arrangement-congestion" runat="server" /><span id="QuestionIDText7"></span></label>
                                <asp:HiddenField ID="hdQuestionID8" runat="server" />
                                <label id="Label7" class="checkbox-input-label" for="ContentPlaceHolder1_chkbox7" runat="server">
                                    <input type="checkbox" id="chkbox7" name="arrangement-congestion" runat="server" onchange="validate();" /><span id="QuestionIDText8"></span></label>

                            </div>
                            </div>
                            
                            <!--End .label-checkbox-group-->

                        </div>
                        <!--End .fieldset-group-->
                        <div id="dvOthers" runat="server" class="divHide">
                            <div class="plms-fieldset">
                                <asp:HiddenField ID="hdQuestionID9" runat="server" />
                                <label class="plms-label is-hidden" for="other-text-input" id="txtOtherQuestion"></label>
                                <div class="plms-tooltip-parent">
                                    <i class="req-icon" title="Mandatory Field">*</i>
                                    <input type="text" placeholder="Other (please specify)" class="plms-input skin2" id="txtOtherID" name="other-text-input" runat="server" />
                                    <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                        <div class="plms-tooltip-body">
                                            <p id="txtOtherQuestionToolTip"></p>
                                        </div>
                                        <!--End .plms-tooltip-body-->
                                    </div>
                                    <!--End .plms-tooltip-->
                                </div>
                                <!--End .plms-tooltip-parent-->
                            </div>
                            <!--End .plms-fieldset-->
                        </div>

                        <div class="plms-fieldset pull-up" id="dvDelayreasoning" runat="server">
                            <asp:HiddenField ID="hdQuestionID10" runat="server" />
                            <label class="plms-label is-hidden" for="delay-reasoning" id="QuestionIDText10"></label>
                            <div class="plms-tooltip-parent">
                                <textarea rows="4" cols="4" class="plms-textarea skin2" id="txtdelayreasoning" name="delay-reasoning" runat="server"></textarea>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="AIQuestionIDText10ToolTip"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->


                    </div>
                    <!--End .boxed-content-body-->

                    <nav class="pagination-nav">
                        <asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click" />
                        <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />

                        <%--        <a href="ai-reports-22.php" title="Previous" class="btn large">Previous</a>
			  <a href="ai-reports-24-1.php" title="Next" class="btn large align-r">Next</a>--%>
                    </nav>
                    <!--End .pagination-nav-->
                </div>
                <!--End .boxed-content-->
                <aside id="main-aside">
                    <div class="aside-toggle-handle">
                        <div class="flyout-message-wrapper">
                            <a href="#toggle-aside" class="btn">
                                <i class="icon"></i>
                                <span><%=Resources.Resource.lblHideMenu %></span>
                            </a>
                        </div>
                        <!--End .flyout-message-wrapper-->
                    </div>
                    <!--End .aside-toggle-handle-->

                    <nav class="aside-main-nav">
                        <%--<ul>
			<li><a id="A1" href="AIGeneralInformation.aspx" title=""  runat="server"><%= Resources.Resource.lblGenInfoPage %></a></li>
			<li><a id="A2" href="AiDetails.aspx" title=""  runat="server"><%= Resources.Resource.lblDetailPage %> </a></li>
			<li ><a id="A3" href="AIAccidentIncidentDetail.aspx" title=""  runat="server">  <%= Resources.Resource.lblAccidentIncidentdetailPage %>   </a></li>
			<li><a id="A4" href="AIContributingFactor.aspx" title=""  runat="server">   <%= Resources.Resource.lblcontributingFactorPage %> </a></li>
            <li class="is-active"><a id="A9" href="AIPersonalProtectiveEquipment.aspx" title="" runat="server">  <%= Resources.Resource.lblAiPersonProactivePage %>   </a></li>
			<li><a id="A5" href="AIActionAndPrevention.aspx" title="" runat="server">  <%= Resources.Resource.lblAiCorrectionActionPage %>   </a></li>
            <li><a id="A6" href="AIWitnessesCoWorker.aspx" title=""  runat="server">  <%= Resources.Resource.lblCoworkerPage %></a></li>
			<li><a id="A7" href="AIWorkerComment.aspx" title="" runat="server">  <%= Resources.Resource.lblWorkerCommentPage %> </a></li>
			<li><a id="A8" href="AIPeopleInvolved.aspx" title=""  runat="server">  <%= Resources.Resource.lblPeopleInvolvedPage %>   </a></li>
			</ul>--%>
                        <UC1Menu:UC1 ID="lblmenu" runat="server" />


                    </nav>
                    <!--End .aside-main-nav-->
                </aside>
                <!--End #main-aside-->

            </div>
            <!--End .layout-sidebar-right-->

            <footer>
                <div class="btngrp">
                    <asp:Button ID="btnSubmitandExitLater" runat="server" title="<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click" />
                    <asp:Button ID="btnSubmitMyReport" runat="server" title="<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click" />

                </div>
            </footer>
        </div>
        <!--End .wrapper-->
    </section>
    <!--End #main-content-->
</asp:Content>

