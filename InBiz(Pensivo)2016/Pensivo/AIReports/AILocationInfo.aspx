﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AILocationInfo.aspx.cs" Inherits="AIReports_AILocationInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
    
    <script  type="text/javascript">
        function GetLocationQuestions() {            
            $.ajax({
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "Location" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data)
                {
                    var index = 0;
                    $.each(data.d, function (index, value)
                    {
                        if (index == 0)
                        {
                            $('#<%=hdlocationQuestion1.ClientID%>').val(value.QuestionID);
                            $('#lblSiteQuestionText1' ).html( value.QuestionText );
                        } 
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown)
                {
                    //alert( errorThrown );
                }
            });
        }

        $(document).ready(function () {
            GetLocationQuestions();
           
        });


        function BlankData()
        {
            if ( $( '#<%=txtSite.ClientID%>' ).val() == "" )
            { 
                ShowPensivoMessage( "<%=Resources.Resource.lblAiInvalidSite%>" );
                return false;
             }
            else
            {
                return true;
            } 
        } 
    </script> 
    <section id="main-content" class="pg-location">
        <span class="mask"></span>
        <div class="wrapper width-med">           
            
            <div class="plms-alert secondary-clr">
                <p>
                     <%=Resources.Resource.lblAccidentIncident %><strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %>  <span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>

            <!--End .plms-alert-->
            <div class="boxed-content">
                <h2> <%=Resources.Resource.lblLocation %></h2>
                <p> 
                      <%=Resources.Resource.lblLocationText %>        
                </p>
                <div class="boxed-content-body">
                    <asp:Panel ID="pnlContent" runat="server" DefaultButton="btnSearch">
                        <div class="input-button-group">
                            <asp:HiddenField  ID="hdlocationQuestion1"  runat="server"/>    
                            <div class="plms-fieldset ws-per-70">
                                <%--<label class="plms-label is-hidden" for="site-number">Site #</label>--%>
                                <asp:Label ID="Label3" AssociatedControlID="txtSite" class="filter-key plms-label is-hidden" runat="server"
                                    Text="<%$ Resources:Resource, lblAISite %>" for="txtSite"></asp:Label>
                                <div class="plms-tooltip-parent">
                                    <%--<input type="text" maxlength="10" placeholder="Site #" class="plms-input skin2" id="site-number" name="site-number" />--%>
                                    <asp:TextBox ID="txtSite" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblAISite %>">
                                    </asp:TextBox>
                                    <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                        <div class="plms-tooltip-body">
                                            <p id="lblSiteQuestionText1"> </p>
                                        </div>
                                        <!--End .plms-tooltip-body-->
                                    </div>
                                    <!--End .plms-tooltip-->
                                </div>
                                <!--End .plms-tooltip-parent-->
                            </div>
                            <!--End .plms-fieldset-->
                            <%--<button class="btn fluid large ws-per-30 search-button">Search</button>--%>
                           
                            <!--
                             <asp:Button ID="btnSearch" runat="server" class="btn fluid large ws-per-30 search-button" Text="<%$ Resources:Resource, lbSearch %>" OnClick="btnSearch_Click" ValidationGroup="vg1" />

                            -->
                        </div>
                    </asp:Panel>
                    <!--End .input-button-group-->

                 <%--   <div class="fieldset-group location-type" onkeypress="return disableEnterKey(event)">
                        <p>                               
                             <asp:Label ID="locationQuestion1"  runat="server" Text="Mukesh"></asp:Label>
                        </p>
                        <div class="label-radio-group">
                            <label class="radio-input-label" for="location-retail">
                                <input type="radio" id="locationretail" name="locationtype" value="R" checked="checked" onclick="onLogisticChange(1)" />Retail Location</label>
                            <label class="radio-input-label" for="location-logistics">
                                <input type="radio" id="locationlogistics" name="locationtype" value="L"   onclick="onLogisticChange(2)"  />Logistics Location</label>
                        </div>
                        <!--End .label-radio-group-->
                    </div>--%>
                    <!--End .fieldset-group-->

                    <%--<div class="fieldset-group location-type" onkeypress="return disableEnterKey(event)" id="dvIsLogistic">
                        <p>
                            <asp:HiddenField  ID="hdlocationQuestion2"  runat="server"/>
                            <asp:Label ID="locationQuestion2"  runat="server"></asp:Label>
                            </p>
                        <div class="label-radio-group">
                            <label class="radio-input-label" for="licensee-yes">
                                <input type="radio" id="licensee-yes" name="licensee" value="1" checked="checked"/>Yes</label>
                            <label class="radio-input-label" for="licensee-no">
                                <input type="radio" id="licensee-no" name="licensee" value="0"/>No</label>                            
                            <!--
                            <label class="radio-input-label" for="licensee-na">
                                <input type="radio" id="licensee-na" name="licensee" value="2"/>N/A</label>-->
                        </div>
                        <!--End .label-radio-group-->
                    </div>--%>
                    <!--End .fieldset-group-->
                </div>
                <!--End .boxed-content-body-->
                <nav class="pagination-nav">
                    <a href="AIEmployeeDetails.aspx" runat="server" title="<%$ Resources:Resource, lblPrevious %>" class="btn large"><%=Resources.Resource.lblPrevious %></a>
                     
                    <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"  OnClientClick="return BlankData();" />
                </nav>
                <!--End .pagination-nav-->

            </div>
            <!--End .boxed-content-->
            <footer>
              <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn"  />
            </footer>

        </div>
        <!--End .wrapper-->
    </section>    
</asp:Content>

