﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AIPersonalInjuryConfirm : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];

                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                        hdFirstName.Value = BusinessUtility.GetString(result.EmpFirstName);
                    }
                }
            }
        }
    }
    protected void btnPrevoius_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIPersonalInjuryExp.aspx", false);
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {   
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.PersonalInjury).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                } 


                objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdPersonalInjuryConfirmQuestionQuestionID.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(ConfirmationYesNo.No),
                        AIFormID = aiFormID,
                        AIPageName = PageName.PersonalInjury
                    });

                Session["AIReportQuestions"] = objList;
                Response.Redirect("AIPropertyDamageDesc.aspx");
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnYes_Click(object sender, EventArgs e)
    {      
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {   
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.PersonalInjury).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                } 

                objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdPersonalInjuryConfirmQuestionQuestionID.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(ConfirmationYesNo.Yes),
                        AIFormID = aiFormID,
                        AIPageName = PageName.PersonalInjury
                    });

                Session["AIReportQuestions"] = objList;
                Response.Redirect("AIFirstAidDesc.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBorad.aspx", false);
    }
}