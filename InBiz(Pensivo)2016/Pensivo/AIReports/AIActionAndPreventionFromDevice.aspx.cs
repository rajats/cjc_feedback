﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AIActionAndPreventionFromDevice : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();
        }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.ActionPreventionCorrection).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 0;
                    foreach (var result in resultItem)
                    {
                        if (count == 0 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox1.Checked = true;
                        }
                        else if (count == 1 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox2.Checked = true;
                        }
                        else if (count == 2 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox3.Checked = true;
                        }
                        else if (count == 3 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox4.Checked = true;
                        }
                        else if (count == 4 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox5.Checked = true;
                        }
                        else if (count == 5 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox6.Checked = true;
                        }
                        else if (count == 6 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox7.Checked = true;
                        }
                        else if (count == 7 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox8.Checked = true;
                        }
                        else if (count == 8 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkbox9.Checked = true;
                            dvOthers.Attributes.Remove("Class");
                            dvOthers.Attributes.Add("Class", "divShow");
                        }
                        else if (count == 9)
                        {
                            txtothertextinput.Value = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        else if (count == 10)
                        {
                            if (!string.IsNullOrEmpty(result.AIQuestionAnswerText))
                            {
                                txtdatecompleted.Value = Convert.ToDateTime(result.AIQuestionAnswerText).ToString("yyyy-MM-dd");
                            }
                        }
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }
    }

    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIActionAndPreventionConfirm.aspx", false);
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.ActionPreventionCorrection).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID1.Value)))
                {
                    objList.Add(
                   new AIQuestionAnswer
                   {
                       AIQuestionID = Convert.ToInt32(hdQuestionID1.Value),
                       AIQuestionType = QuestionType.BooleanType,
                       AIQuestionAnswerText = Convert.ToString(chkbox1.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                       AIFormID = aiFormID,
                       AIPageName = PageName.ActionPreventionCorrection,
                       AISequence = 1
                   });
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID2.Value)))
                {
                    objList.Add(
                  new AIQuestionAnswer
                  {
                      AIQuestionID = Convert.ToInt32(hdQuestionID2.Value),
                      AIQuestionType = QuestionType.BooleanType,
                      AIQuestionAnswerText = Convert.ToString(chkbox2.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                      AIFormID = aiFormID,
                      AIPageName = PageName.ActionPreventionCorrection,
                      AISequence = 2
                  });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID3.Value)))
                {
                    objList.Add(
                      new AIQuestionAnswer
                      {
                          AIQuestionID = Convert.ToInt32(hdQuestionID3.Value),
                          AIQuestionType = QuestionType.BooleanType,
                          AIQuestionAnswerText = Convert.ToString(chkbox3.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                          AIFormID = aiFormID,
                          AIPageName = PageName.ActionPreventionCorrection,
                          AISequence = 3
                      });

                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID4.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdQuestionID4.Value),
                            AIQuestionType = QuestionType.BooleanType,
                            AIQuestionAnswerText = Convert.ToString(chkbox4.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                            AIFormID = aiFormID,
                            AIPageName = PageName.ActionPreventionCorrection,
                            AISequence = 4
                        });

                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID5.Value)))
                {

                    objList.Add(
                   new AIQuestionAnswer
                   {
                       AIQuestionID = Convert.ToInt32(hdQuestionID5.Value),
                       AIQuestionType = QuestionType.BooleanType,
                       AIQuestionAnswerText = Convert.ToString(chkbox5.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                       AIFormID = aiFormID,
                       AIPageName = PageName.ActionPreventionCorrection,
                       AISequence = 5
                   });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID6.Value)))
                {

                    objList.Add(
                new AIQuestionAnswer
                {
                    AIQuestionID = Convert.ToInt32(hdQuestionID6.Value),
                    AIQuestionType = QuestionType.BooleanType,
                    AIQuestionAnswerText = Convert.ToString(chkbox6.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                    AIFormID = aiFormID,
                    AIPageName = PageName.ActionPreventionCorrection,
                    AISequence = 6
                });
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID7.Value)))
                {

                    objList.Add(
                new AIQuestionAnswer
                {
                    AIQuestionID = Convert.ToInt32(hdQuestionID7.Value),
                    AIQuestionType = QuestionType.BooleanType,
                    AIQuestionAnswerText = Convert.ToString(chkbox7.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                    AIFormID = aiFormID,
                    AIPageName = PageName.ActionPreventionCorrection,
                    AISequence = 7
                });

                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID8.Value)))
                {

                    objList.Add(
                new AIQuestionAnswer
                {
                    AIQuestionID = Convert.ToInt32(hdQuestionID8.Value),
                    AIQuestionType = QuestionType.BooleanType,
                    AIQuestionAnswerText = Convert.ToString(chkbox8.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                    AIFormID = aiFormID,
                    AIPageName = PageName.ActionPreventionCorrection,
                    AISequence = 8
                });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID9.Value)))
                {

                    objList.Add(
          new AIQuestionAnswer
          {
              AIQuestionID = Convert.ToInt32(hdQuestionID9.Value),
              AIQuestionType = QuestionType.BooleanType,
              AIQuestionAnswerText = Convert.ToString(chkbox9.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
              AIFormID = aiFormID,
              AIPageName = PageName.ActionPreventionCorrection,
              AISequence = 9

          });

                }

                if (chkbox9.Checked)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID10.Value)))
                    {
                        objList.Add(
                            new AIQuestionAnswer
                            {
                                AIQuestionID = Convert.ToInt32(hdQuestionID10.Value),
                                AIQuestionType = QuestionType.TextType,
                                AIQuestionAnswerText = Convert.ToString(txtothertextinput.Value),
                                AIFormID = aiFormID,
                                AIPageName = PageName.ActionPreventionCorrection,
                                AISequence = 10
                            });
                    }

                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID11.Value)))
                {

                    objList.Add(
    new AIQuestionAnswer
    {
        AIQuestionID = Convert.ToInt32(hdQuestionID11.Value),
        AIQuestionType = QuestionType.TextType,
        AIQuestionAnswerText = Convert.ToString(txtdatecompleted.Value),
        AIFormID = aiFormID,
        AIPageName = PageName.ActionPreventionCorrection,
        AISequence = 11
    });

                }



                Session["AIReportQuestions"] = objList;
                Response.Redirect("AIWitnessesCoWorker.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }
}