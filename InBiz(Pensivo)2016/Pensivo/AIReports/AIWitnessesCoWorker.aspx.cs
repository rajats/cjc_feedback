﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AIWitnessesCoWorker : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();
        }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.WitnessCoworkersForm).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 0;
                    foreach (var result in resultItem)
                    {
                        if (count == 0)
                        {

                        }
                        if (count == 1)
                        {
                            txtname.Value = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        if (count == 2)
                        {
                            txtphonenumber.Value = Convert.ToDateTime(result.AIQuestionAnswerText).ToString("yyyy-MM-dd");
                        }
                        if (count == 3)
                        {

                        }
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {

                var ISIncident = objList.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                if (ISIncident != null)
                {
                    Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                    return;
                }
                var ISWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
                var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");
                if (ISWVH != null && IsPerosnalInjury == null && IsFirstAidBox == null && IsHealthCare == null && IsLostTime == null && IsPropertyDamage == null)
                {
                    Response.Redirect("AIActionAndPrevention.aspx", false);
                    return;
                } 

                var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (IsLicensee != null)
                {
                    // Get result from all scenarios
                    //var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    //var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    //var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    //var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    //var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }

                }
                else
                {
                    // Get result from all scenarios
                    //var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    //var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    //var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    //var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    //var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIActionAndPreventionFromDevice.aspx", false);
                        return;
                    }

                }
            }

        }
        catch { }
        finally { }

    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.WitnessCoworkersForm).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID1.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdAIQuestionID1.Value),
                             AIQuestionType = QuestionType.BooleanType,
                             AIQuestionAnswerText = Convert.ToString(hdAIQuestionIDAns1.Value) == "" ? ConfirmationYesNo.No : ConfirmationYesNo.Yes,
                             AIFormID = aiFormID,
                             AIPageName = PageName.WitnessCoworkersForm
                         });

                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID2.Value)))
                {
                    objList.Add(
                      new AIQuestionAnswer
                      {
                          AIQuestionID = Convert.ToInt32(hdAIQuestionID2.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = Convert.ToString(txtname.Value),
                          AIFormID = aiFormID,
                          AIPageName = PageName.WitnessCoworkersForm
                      });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID3.Value)))
                {
                    objList.Add(
                      new AIQuestionAnswer
                      {
                          AIQuestionID = Convert.ToInt32(hdAIQuestionID3.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = Convert.ToString(txtphonenumber.Value),
                          AIFormID = aiFormID,
                          AIPageName = PageName.WitnessCoworkersForm
                      });

                }



                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID4.Value)))
                {
                    objList.Add(
                     new AIQuestionAnswer
                     {
                         AIQuestionID = Convert.ToInt32(hdAIQuestionID4.Value),
                         AIQuestionType = QuestionType.TextType,
                         AIQuestionAnswerText = Convert.ToString(hdAIQuestionIDAns4.Value),
                         AIFormID = aiFormID,
                         AIPageName = PageName.WitnessCoworkersForm
                     });
                }

                Session["AIReportQuestions"] = objList;
                RedirectionLink();
                //Response.Redirect("AIWorkerComment.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    private void RedirectionLink()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {
                var ISIncident = objList.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                if (ISIncident != null)
                {
                    Response.Redirect("AIWorkerComment.aspx", false);
                    return;
                }

                var ISWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
                var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");
                if (ISWVH != null && IsPerosnalInjury == null && IsFirstAidBox == null && IsHealthCare == null && IsLostTime == null && IsPropertyDamage == null)
                {
                    Response.Redirect("AIWorkerComment.aspx", false);
                    return;
                } 


                var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (IsLicensee != null)
                {
                    // Get result from all scenarios
                    //var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    //var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    //var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    //var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    //var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }

                }
                else
                {
                    // Get result from all scenarios
                    //var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    //var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    //var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    //var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    //var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIWorkerComment.aspx", false);
                        return;
                    }
                }

            }

        }
        catch { }
        finally { }
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
}