﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIWVHOffenders : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
                getFormData();
            }
        }
    }
    protected void btnSaveAndExit_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitmyReport_Click(object sender, EventArgs e)
    {

    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {
                var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (IsLicensee != null)
                {

                    Response.Redirect("AIWVHEventReportedBy.aspx", false);
                    return;
                }
                else
                {
                    Response.Redirect("AIWVHEventReportedBy.aspx", false);
                    return;
                }
            }
        }
        catch { }
        finally { }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {

        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHOffenders).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHOffendersQuestionID1.Value)))
                {

                    objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdWVHOffendersQuestionID1.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = Convert.ToString(txtallegedoffendername.Text),
                        AIFormID = aiFormID,
                        AIPageName = PageName.WVHOffenders,
                        AISequence = 1
                    });
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHOffendersQuestionID2.Value)))
                {
                    objList.Add(
                  new AIQuestionAnswer
                  {
                      AIQuestionID = Convert.ToInt32(hdWVHOffendersQuestionID2.Value),
                      AIQuestionType = QuestionType.TextType,
                      AIQuestionAnswerText = Convert.ToString(txtidentifyinginformation.Text),
                      AIFormID = aiFormID,
                      AIPageName = PageName.WVHOffenders,
                      AISequence = 2
                  });

                }



                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHOffendersQuestionID3.Value)))
                {
                    objList.Add(
                 new AIQuestionAnswer
                 {

                     AIQuestionID = Convert.ToInt32(hdWVHOffendersQuestionID3.Value),
                     AIQuestionType = QuestionType.BooleanType,
                     AIQuestionAnswerText = Convert.ToString(Chkcoworker.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
                     AIFormID = aiFormID,
                     AIPageName = PageName.WVHOffenders,
                     AISequence = 3
                 });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHOffendersQuestionID4.Value)))
                {
                    objList.Add(
               new AIQuestionAnswer
               {

                   AIQuestionID = Convert.ToInt32(hdWVHOffendersQuestionID4.Value),
                   AIQuestionType = QuestionType.BooleanType,
                   AIQuestionAnswerText = Convert.ToString(Chksupervisor.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
                   AIFormID = aiFormID,
                   AIPageName = PageName.WVHOffenders,
                   AISequence = 4
               });

                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHOffendersQuestionID5.Value)))
                {

                    objList.Add(
         new AIQuestionAnswer
         {

             AIQuestionID = Convert.ToInt32(hdWVHOffendersQuestionID5.Value),
             AIQuestionType = QuestionType.BooleanType,
             AIQuestionAnswerText = Convert.ToString(Chkformeremployee.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
             AIFormID = aiFormID,
             AIPageName = PageName.WVHOffenders,
             AISequence = 5
         });

                }



                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHOffendersQuestionID6.Value)))
                {
                    objList.Add(
         new AIQuestionAnswer
         {

             AIQuestionID = Convert.ToInt32(hdWVHOffendersQuestionID6.Value),
             AIQuestionType = QuestionType.BooleanType,
             AIQuestionAnswerText = Convert.ToString(chkspousepartner.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
             AIFormID = aiFormID,
             AIPageName = PageName.WVHOffenders,
             AISequence = 6
         });

                }



                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHOffendersQuestionID7.Value)))
                {
                    objList.Add(
         new AIQuestionAnswer
         {

             AIQuestionID = Convert.ToInt32(hdWVHOffendersQuestionID7.Value),
             AIQuestionType = QuestionType.BooleanType,
             AIQuestionAnswerText = Convert.ToString(Chkfamily.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
             AIFormID = aiFormID,
             AIPageName = PageName.WVHOffenders,
             AISequence = 7
         });
                }



                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHOffendersQuestionID8.Value)))
                {
                    objList.Add(
    new AIQuestionAnswer
    {

        AIQuestionID = Convert.ToInt32(hdWVHOffendersQuestionID8.Value),
        AIQuestionType = QuestionType.BooleanType,
        AIQuestionAnswerText = Convert.ToString(Chkother.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
        AIFormID = aiFormID,
        AIPageName = PageName.WVHOffenders,
        AISequence = 8
    });
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdOthers.Value)))
                {

                    objList.Add(
    new AIQuestionAnswer
    {

        AIQuestionID = Convert.ToInt32(hdOthers.Value),
        AIQuestionType = QuestionType.TextType,
        AIQuestionAnswerText = Convert.ToString(txtOther.Text),
        AIFormID = aiFormID,
        AIPageName = PageName.WVHOffenders,
        AISequence = 9
    });
                }


                Session["AIReportQuestions"] = objList;

                //managePreviousNext("");

                RedirectionLink();

            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
        RedirectionLink();
    }
    private void RedirectionLink()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            //var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
            //if (IsLicensee != null)
            //{
            //    ////   Case 2: 
            //    //var IsLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "L");
            //    //var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
            //    //if (IsLogistic != null && IsLicensee != null)
            //    //{
            //    //    Response.Redirect("AILicenseeDetails.aspx", false);
            //    //} 
            //    Response.Redirect("AIWVHDescription.aspx", false);
            //}
            //else
            //{
            //    // For Retail & logistic both in  case of  Lincensee no
            //    // Case 1:  Retail:Yes, WVH:Yes
            //    var IsRetailAndLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "R");
            //    var IsWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
            //    if ((IsRetailAndLogistic != null) && IsWVH != null)
            //    {
            //        Response.Redirect("AIWVHDescription.aspx", false);
            //    }
            //}
            Response.Redirect("AIWVHDescription.aspx", false);
        }
        catch { }
        finally { }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        int count = 1;
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHOffenders).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var result in resultItem)
                    {
                        if (count == 1 && result.AISequence == 1)
                        {
                            txtallegedoffendername.Text = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        else if (count == 2 && result.AISequence == 2)
                        {
                            txtidentifyinginformation.Text = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        else if (count == 3 && result.AISequence == 3 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            Chkcoworker.Checked = true;
                        }
                        else if (count == 4 && result.AISequence == 4 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            Chksupervisor.Checked = true;
                        }
                        else if (count == 5 && result.AISequence == 5 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            Chkformeremployee.Checked = true;
                        }
                        else if (count == 6 && result.AISequence == 6 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            chkspousepartner.Checked = true;
                        }
                        else if (count == 6 && result.AISequence == 6 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            Chkfamily.Checked = true;
                        }
                        else if (count == 6 && result.AISequence == 6 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                        {
                            Chkother.Checked = true;                        
                            dvOthers.Attributes.Remove("Class");
                            dvOthers.Attributes.Add("Class", "divShow");                       
                        }
                        else if (count == 7 && result.AISequence == 7)
                        {
                            txtOther.Text = Convert.ToString(result.AIQuestionAnswerText);
                        }
                        count++;
                    }                
                } 
            }
        }
        catch { }
        finally { }
    }


}