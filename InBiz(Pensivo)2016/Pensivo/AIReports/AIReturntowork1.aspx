﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIReturntowork1.aspx.cs" Inherits="AIReports_AIReturntowork1" %>

<%@ Register Src="~/AIReports/UserControl/IncidentMenu.ascx" TagName="UC1" TagPrefix="UC1Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
    <script type="text/javascript">
        function GetAIDetails()
        {
            $.ajax( {
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "ReturntoWork1" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function ( data )
                {
                    var index = 0;
                    $.each( data.d, function ( index, value )
                    {
                        if ( index == 0 )
                        {
                            $( "#<%=hdAIQuestionID1.ClientID%>" ).val( value.QuestionID );
                                 $( "#AIQuestionIDText1" ).html( value.QuestionText );
                             }
                             if ( index == 1 )
                             {
                                 $( "#<%=hdAIQuestionID2.ClientID%>" ).val( value.QuestionID );
                                 $( "#AIQuestionIDText2ToolTip" ).html( value.QuestionText );
                                 $( "#AIQuestionIDText2" ).attr( "placeholder", value.QuestionText );
                                 $( "#AIQuestionIDText2" ).html( value.QuestionText );
                             }
                             if ( index == 2 )
                             {
                                 $( "#<%=hdAIQuestionID3.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionIDText3" ).html( value.QuestionText );
                         }
                             if ( index == 3 )
                             {
                                 $( "#<%=hdAIQuestionID4.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionIDText4" ).html( value.QuestionText );
                         }

                             index = parseInt( index ) + 1;
                         } );
                     },
                     error: function ( XMLHttpRequest, textStatus, errorThrown )
                     {
                         //alert( errorThrown );
                     }
                 } );
         }
         $( document ).ready( function ()
         {
             GetAIDetails();
         } );

         function ClickChange( ID, Value )
         {

             if ( ID == "ancAiQuestionD1Yes" )
             {
                 $( "#<%=hdAIQuestionAnsID1.ClientID%>" ).val( "1" );

                 $( "#<%=divQuestionID2.ClientID%>" ).hide();
                 

                 $( "#<%=ancAiQuestionD1Yes.ClientID%>" ).removeClass( 'btn' );
                 $( "#<%=ancAiQuestionD1Yes.ClientID%>" ).addClass( 'btnChageColor' );
                 if ( $( "#<%=ancAiQuestionD1No.ClientID%>" ).hasClass( "btnChageColor" ) )
                 {
                     $( "#<%=ancAiQuestionD1No.ClientID%>" ).removeClass( 'btnChageColor' );
                     $( "#<%=ancAiQuestionD1No.ClientID%>" ).addClass( 'btn' );
                 }
             }

             if ( ID == "ancAiQuestionD1No" )
             {
                 $( "#<%=hdAIQuestionAnsID1.ClientID%>" ).val( "0" );

                 $( "#<%=divQuestionID2.ClientID%>" ).show();

                 //document.getElementById( 'divQuestionID2' ).style.display = "";

                 $( "#<%=ancAiQuestionD1No.ClientID%>" ).removeClass( 'btn' );
                 $( "#<%=ancAiQuestionD1No.ClientID%>" ).addClass( 'btnChageColor' );
                 if ($( "#<%=ancAiQuestionD1Yes.ClientID%>" ).hasClass( "btnChageColor" ) )
                 {
                     $( "#<%=ancAiQuestionD1Yes.ClientID%>" ).removeClass( 'btnChageColor' );
                     $( "#<%=ancAiQuestionD1Yes.ClientID%>" ).addClass( 'btn' );
               }
           }

           if ( ID == "ancAiQuestion2Yes" )
           {
               $( "#<%=hdAIQuestionAnsID2.ClientID%>" ).val( "1" );

                 $( "#<%=ancAiQuestionD2Yes.ClientID%>" ).removeClass( 'btn' );
                 $( "#<%=ancAiQuestionD2Yes.ClientID%>" ).addClass( 'btnChageColor' );
                 if ( $( "#<%=ancAiQuestionD2No.ClientID%>" ).hasClass( "btnChageColor" ) )
                 {
                     $( "#<%=ancAiQuestionD2No.ClientID%>" ).removeClass( 'btnChageColor' );
                   $( "#<%=ancAiQuestionD2No.ClientID%>" ).addClass( 'btn' );
               }
           }

           if ( ID == "ancAiQuestion2No" )
           {
               $( "#<%=hdAIQuestionAnsID2.ClientID%>" ).val( "0" );
                 $( "#<%=ancAiQuestionD2No.ClientID%>" ).removeClass( 'btn' );
                 $( "#<%=ancAiQuestionD2No.ClientID%>" ).addClass( 'btnChageColor' );
                 if ( $( "#<%=ancAiQuestionD2Yes.ClientID%>" ).hasClass( "btnChageColor" ) )
                 {
                     $( "#<%=ancAiQuestionD2Yes.ClientID%>" ).removeClass( 'btnChageColor' );
                   $( "#<%=ancAiQuestionD2Yes.ClientID%>" ).addClass( 'btn' );
               }
           }

           if ( ID == "ancAiQuestion3Yes" )
           {
               $( "#<%=hdAIQuestionAnsID3.ClientID%>" ).val( "1" );
              //s document.getElementById( 'divQuestionID4' ).style.display = "";


               $( "#<%=divQuestionID4.ClientID%>" ).show();
               
         

                 $( "#<%=ancAiQuestion3Yes.ClientID%>" ).removeClass( 'btn' );
                 $( "#<%=ancAiQuestion3Yes.ClientID%>" ).addClass( 'btnChageColor' );
                 if ( $( "#<%=ancAiQuestion3No.ClientID%>" ).hasClass( "btnChageColor" ) )
                 {
                     $( "#<%=ancAiQuestion3No.ClientID%>" ).removeClass( 'btnChageColor' );
                   $( "#<%=ancAiQuestion3No.ClientID%>" ).addClass( 'btn' );
               }
           }

           if ( ID == "ancAiQuestion3No" )
           {
               $( "#<%=hdAIQuestionAnsID3.ClientID%>" ).val( "0" );
              // document.getElementById( 'divQuestionID4' ).style.display = "none";
                  
               ShowPensivoMessage( "<p style='text-align:justify;'><%=Resources.Resource.lblReturnToWork1NOText%><p>" );
                

               $( "#<%=divQuestionID4.ClientID%>" ).hide();

                 $( "#<%=ancAiQuestion3No.ClientID%>" ).removeClass( 'btn' );
                 $( "#<%=ancAiQuestion3No.ClientID%>" ).addClass( 'btnChageColor' );
                 if ( $( "#<%=ancAiQuestion3Yes.ClientID%>" ).hasClass( "btnChageColor" ) )
                 {
                     $( "#<%=ancAiQuestion3Yes.ClientID%>" ).removeClass( 'btnChageColor' );
                   $( "#<%=ancAiQuestion3Yes.ClientID%>" ).addClass( 'btn' );
                 }
           }
       }
    </script>

    <section id="main-content" class="pg-ca-preventions">
        <span class="mask"></span>
        <div class="wrapper">
            <div class="plms-alert secondary-clr">
                <p>
                    <%=Resources.Resource.lblAccidentIncident %>  <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> ( <%=Resources.Resource.lblAISite %>  #<span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %><span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->

            <div class="plms-alert neutral">
                <p class="mandatory-fields-message">
                    <i class="req-icon" title="Mandatory Field">*</i>
                    <%=Resources.Resource.lblMandatoryField %>
                </p>
            </div>
            <!--End .plms-alert-->

            <div class="layout-sidebar-right">
                <div class="boxed-content">
                    <div class="boxed-content-body">

                        <h2><%=Resources.Resource.lblReturntoworkTitle %></h2>
                        <div class="plms-fieldset-wrapper push-up">
                            <asp:HiddenField ID="hdAIQuestionID1" runat="server" />
                            <asp:HiddenField ID="hdAIQuestionAnsID1" runat="server" />
                            <div class="column span-8">
                                <i class="req-icon" title="Mandatory Field">*</i>
                                <p class="bold" id="AIQuestionIDText1"></p>
                            </div>
                            <!--End .column-->  
                            <div class="column span-4">
                                <div class="btngrp align-r">
                                    <a id="ancAiQuestionD1Yes" runat="server" class="btn" title="Yes" onclick="ClickChange('ancAiQuestionD1Yes', this.text);">Yes</a>
                                    <a id="ancAiQuestionD1No" runat="server" class="btn" title="No" onclick="ClickChange('ancAiQuestionD1No', this.text);">No</a>
                                </div>
                                <!--End .btngrp-->
                            </div>
                            <!--End .column-->
                        </div>
                        <!--End .plms-fieldset-wrapper-->



                        <div id="divQuestionID2"  runat="server"  class="divHide">
                        <div class="plms-fieldset-wrapper push-up-10" >
                            <div class="column span-8">
                                <asp:HiddenField ID="hdAIQuestionID2" runat="server" />
                                <asp:HiddenField ID="hdAIQuestionAnsID2" runat="server" />
                                <i class="req-icon" title="Mandatory Field">*</i>
                                <p class="bold" id="AIQuestionIDText2"></p>
                            </div>
                            <!--End .column-->
                            <div class="column span-4">
                                <div class="btngrp align-r">
                                    <a runat="server" id="ancAiQuestionD2Yes" class="btn" title="Yes" onclick="ClickChange('ancAiQuestion2Yes', this.text);">Yes</a>
                                    <a runat="server" id="ancAiQuestionD2No" class="btn" title="No" onclick="ClickChange('ancAiQuestion2No', this.text);">No</a>
                                </div>
                                <!--End .btngrp-->
                            </div>
                            <!--End .column-->
                        </div>
                        <!--End .plms-fieldset-wrapper-->
                        </div>
                        
                        

                        <div class="plms-fieldset-wrapper push-up-10">
                            <div class="column span-8">
                                <asp:HiddenField ID="hdAIQuestionID3" runat="server" />
                                <asp:HiddenField ID="hdAIQuestionAnsID3" runat="server" />
                                <i class="req-icon" title="Mandatory Field">*</i>
                                <p class="bold" id="AIQuestionIDText3"></p>
                            </div>
                            <!--End .column-->

                            <div class="column span-4">
                                <div class="btngrp align-r">
                                    <a id="ancAiQuestion3Yes" runat="server" class="btn" title="Yes" onclick="ClickChange('ancAiQuestion3Yes', this.text);">Yes</a>
                                    <a id="ancAiQuestion3No" runat="server" class="btn" title="No" onclick="ClickChange('ancAiQuestion3No', this.text);">No</a>
                                </div>
                                <!--End .btngrp-->
                            </div>
                            <!--End .column-->
                        </div>
                        <!--End .plms-fieldset-wrapper-->


                        <div id="divQuestionID4"  class="divHide"  runat="server">
                    
                        <div class="plms-fieldset-wrapper push-up-10" >
                            <div class="column span-8">
                                <asp:HiddenField ID="hdAIQuestionID4" runat="server" />
                                <asp:HiddenField ID="hdAIQuestionAnsID4" runat="server" />
                                <i class="req-icon" title="Mandatory Field">*</i>
                                <p class="bold" id="AIQuestionIDText4"></p>
                            </div>
                            <!--End .column-->

                            <div class="column span-4">
                                <div class="btngrp align-r">
                                    <input type="text" class="plms-input datepicker skin2" id="txtscheduledcompletiondate" name="scheduled-completion-date" runat="server" />
                                </div>
                                <!--End .btngrp-->
                            </div>
                            <!--End .column-->
                        </div>
                            </div>
                        
                        <!--End .plms-fieldset-wrapper-->
                     <%--   
                        <div id="dvNOQuestions" runat="server" style="display: none;">
                            <div class="plms-fieldset-wrapper push-up-10" id="div1" >
                                <div class="column span-8">                                   
                                    <i class="req-icon" title="Mandatory Field">*</i>
                                    <p class="bold" id="P1">  <%= Resources.Resource.lblReturnToWork1NOText%></p>
                                </div>
                                <!--End .column-->  
                            </div>
                            <!--End .plms-fieldset-wrapper-->  
                        </div>--%>

                        
                    </div>
                    <!--End .boxed-content-body-->



                    <nav class="pagination-nav">
                        <asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevious  %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large" OnClick="btnPrevious_Click" />
                        <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, lblNext %>" class="btn large align-r" OnClick="btnNext_Click" />
                    </nav>
                    <!--End .pagination-nav-->
                </div>
                <!--End .boxed-content-->


                <aside id="main-aside">
                    <div class="aside-toggle-handle">
                        <div class="flyout-message-wrapper">
                            <a href="#toggle-aside" class="btn">
                                <i class="icon"></i>
                                <span><%=Resources.Resource.lblHideMenu %></span>
                            </a>
                        </div>
                        <!--End .flyout-message-wrapper-->
                    </div>
                    <!--End .aside-toggle-handle-->
                    <nav class="aside-main-nav">
                        <UC1Menu:UC1 ID="lblmenu" runat="server" />
                    </nav>
                    <!--End .aside-main-nav-->
                </aside>
                <!--End #main-aside-->

            </div>
            <!--End .layout-sidebar-right-->

            <footer>
                <div class="btngrp">

                    <asp:Button ID="btnSubmitandExitLater" runat="server" title="<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click" />
                    <asp:Button ID="btnSubmitMyReport" runat="server" title="<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click" />

                </div>
            </footer>

        </div>
        <!--End .wrapper-->
    </section>
    <!--End #main-content-->

    <script type="text/javascript">
        $( document ).ready( function ()
        {
            if ( $( "#<%=hdAIQuestionAnsID1.ClientID%>" ).val() == "0" )
            {
                document.getElementById( 'divQuestionID2' ).style.display = "";
            }
        } );
  
        var minYear = '-' +  <%= Resources.Resource.minyearinmonth %> + 'M';
          ( function ()
          {
              $( ".datepicker" ).datepicker( {
                  dateFormat: "yy-mm-dd",
                  changeMonth: true,
                  changeYear: true,
                  yearRange: GetDateRange(),
                  maxDate: '+0D' 
              } );
          })();

          function GetDateRange() {
              var currentDate = new Date();
              var currentYear = currentDate.getFullYear();
              var previousYear = currentDate.getFullYear() - parseInt(<%= Resources.Resource.lblYearRange %> );
              return previousYear + ':' + currentYear;
          }
 
    </script>
</asp:Content>

