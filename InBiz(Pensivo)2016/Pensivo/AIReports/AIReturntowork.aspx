﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIReturntowork.aspx.cs" Inherits="AIReports_AIReturntowork" %>

<%@ Register Src="~/AIReports/UserControl/IncidentMenu.ascx" TagName="UC1" TagPrefix="UC1Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
    <script type="text/javascript">
        function GetAIReturntowork()
        {
            $.ajax( {
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "ReturnToWork" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function ( data )
                {
                    var index = 0;
                    $.each( data.d, function ( index, value )
                    {
                        if ( index == 0 )
                        {
                            $( "#<%=hdQuestion1ID.ClientID%>" ).val( value.QuestionID );
                             $( "#lblQuestion1Text" ).html( value.QuestionText );
                         }
                        if ( index == 1 )
                        {
                            $( "#<%=hdQuestion2ID.ClientID%>" ).val( value.QuestionID );
                             $( "#spQuestion2Text" ).html( value.QuestionText );
                             $( "#spQuestion2TextToolTip" ).html( value.QuestionText );

                         }
                        if ( index == 2 )
                        {
                            $( "#<%=hdQuestion3ID.ClientID%>" ).val( value.QuestionID );
                             $( "#lblQuestion3Text" ).html( value.QuestionText );
                         }
                        if ( index == 3 )
                        {
                            $( "#<%=hdQuestion4ID.ClientID%>" ).val( value.QuestionID );
                            $( "#lblQuestion4Text" ).html( value.QuestionText );
                        }
                        if ( index == 3 )
                        {
                            $( "#<%=hdQuestion5ID.ClientID%>" ).val( value.QuestionID );
                            $( "#lblQuestion5Text" ).html( value.QuestionText );
                            $( "#lblQuestion5TextToolTip" ).html( value.QuestionText );
                        }

                        index = parseInt( index ) + 1;
                    } );
                },
                error: function ( XMLHttpRequest, textStatus, errorThrown )
                {
                    //alert( errorThrown );
                }
            } );
        }
        $( document ).ready( function ()
        {
            GetAIReturntowork();

            $( "#<%=dvLicenceWorkerManager.ClientID %>" ).hide();
       $( "#<%=dvDateOfModified.ClientID %>" ).hide();
         $( "#<%=dvQuestion5.ClientID %>" ).hide();
     } );


     function ChangeColor( control, activeControl, ctrl2, ctrl3 )
     {
         var sPrefix = 'ContentPlaceHolder1_';
         var sActiveId = '#' + sPrefix + activeControl;
         var sControlDeactivate1 = '#' + sPrefix + ctrl2;
         var sControlDeactivate2 = '#' + sPrefix + ctrl3;
         var sText = control.text;
         if ( sText == "Yes" )
         {
         }
         else
         {
         }


         $( sActiveId ).removeClass( 'btn' );
         $( sActiveId ).addClass( 'btnChageColor' );
         if ( $( sControlDeactivate1 ).hasClass( "btnChageColor" ) )
         {
             $( sControlDeactivate1 ).removeClass( 'btnChageColor' );
             $( sControlDeactivate1 ).addClass( 'btn' );
         }
         if ( $( sControlDeactivate2 ).hasClass( "btnChageColor" ) )
         {
             $( sControlDeactivate2 ).removeClass( 'btnChageColor' );
             $( sControlDeactivate2 ).addClass( 'btn' );
         }


         var id = sPrefix + activeControl;
         if ( id == "ContentPlaceHolder1_ancHaWorkYes" )
         {
             $( "#<%=dvLicenceWorkerManager.ClientID %>" ).show();
           $( "#<%=dvDateOfModified.ClientID %>" ).show();
           $( "#<%=txtdateofmodified.ClientID %>" ).val( "" );
           $( "#<%=txtlicenseeownermanager.ClientID %>" ).val( "" );
       }
       else if ( id == "ContentPlaceHolder1_ancHaWorkNo" )
       {
           $( "#<%=dvLicenceWorkerManager.ClientID %>" ).hide();
           $( "#<%=dvDateOfModified.ClientID %>" ).hide();

           $( "#<%=txtlicenseeownermanager.ClientID %>" ).val( "" );
       }
       else if ( id == "ContentPlaceHolder1_ancDateYes" )
       {
           $( "#<%=dvQuestion5.ClientID %>" ).hide();
       }
       else if ( id == "ContentPlaceHolder1_ancDateNo" )
       {
           $( "#<%=txtBox1.ClientID %>" ).val( "" );

           $( "#<%=dvQuestion5.ClientID %>" ).show();
       }
}
    </script>
    <section id="main-content" class="pg-ca-preventions">
        <span class="mask"></span>
        <div class="wrapper">
            <div class="plms-alert secondary-clr">
                <p>
                    <%=Resources.Resource.lblAccidentIncident %>  <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> ( <%=Resources.Resource.lblAISite %>  #<span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %><span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->

            <div class="plms-alert neutral">
                <p class="mandatory-fields-message">
                    <i class="req-icon" title="Mandatory Field">*</i>
                    <%=Resources.Resource.lblMandatoryField %>
                </p>
            </div>
            <!--End .plms-alert-->

            <div class="layout-sidebar-right">
                <div class="boxed-content">
                    <div class="boxed-content-body">
                        <h2><%=Resources.Resource.lblReturntoworkTitle %></h2>
                        <div class="plms-fieldset-wrapper push-up">
                            <div class="column span-8">
                                <asp:HiddenField ID="hdQuestion1ID" runat="server" />
                                <asp:HiddenField ID="hdQuestion1IDAns" runat="server" />
                                <p class="bold" id="lblQuestion1Text"></p>
                            </div>
                            <!--End .column-->
                            <div class="column span-4">
                                <div class="btngrp align-r">
                                    <a id="ancHaWorkYes" runat="server" class="btn" title="Yes" onclick="ChangeColor(this, 'ancHaWorkYes', 'ancHaWorkNo', '');">Yes</a>
                                    <a id="ancHaWorkNo" runat="server" class="btn" title="No" onclick="ChangeColor(this, 'ancHaWorkNo', 'ancHaWorkYes', '');">No</a>
                                </div>
                                <!--End .btngrp-->

                                
                            </div>
                            <!--End .column-->
                        </div>
                        <!--End .plms-fieldset-wrapper-->
                     
                        <div id="dvLicenceWorkerManager" runat="server" class="divHide">

                            <div class="plms-fieldset push-down pull-up-10">
                                <asp:HiddenField ID="hdQuestion2ID" runat="server" />
                                <label class="plms-label" for="licensee-owner-manager" id="spQuestion2Text"></label>
                                <div class="plms-tooltip-parent">
                                    <asp:TextBox ID="txtlicenseeownermanager" runat="server" TextMode="MultiLine" CssClass="plms-input skin2"></asp:TextBox>
                                    <div class="plms-tooltip plms-tooltip-micro active left-top outside autow">
                                        <div class="plms-tooltip-body">
                                            <p id="spQuestion2TextToolTip"></p>
                                        </div>
                                        <!--End .plms-tooltip-body-->
                                    </div>
                                    <!--End .plms-tooltip-->
                                </div>
                                <!--End .plms-tooltip-parent-->
                            </div>
                            <!--End .plms-fieldset-->
                        </div>


                        <div id="dvDateOfModified" runat="server" class="divHide">
                            <div class="plms-fieldset-wrapper push-up-10">
                                <asp:HiddenField ID="hdQuestion3ID" runat="server" />
                                <div class="column span-8">
                                    <p class="bold" id="lblQuestion3Text"></p>
                                </div>
                                <!--End .column-->
                                <div class="column span-4">
                                    <asp:TextBox ID="txtdateofmodified" runat="server" CssClass="plms-input datepicker skin2"></asp:TextBox>
                                    <div class="btngrp align-r">
                                    </div>
                                    <!--End .btngrp-->
                                </div>
                                <!--End .column-->
                            </div>
                            <!--End .plms-fieldset-wrapper-->
                        </div>


                        <div class="plms-fieldset-wrapper push-up">
                            <asp:HiddenField ID="hdQuestion4ID" runat="server" />
                            <asp:HiddenField ID="hdQuestion4IDAns" runat="server" />
                            <div class="column span-8">
                                <p class="bold" id="lblQuestion4Text"></p>
                            </div>
                            <!--End .column-->
                            <div class="column span-4">
                                <div class="btngrp align-r">

                                    
                                    <a id="ancDateYes" runat="server" class="btn" title="Yes" onclick="ChangeColor(this, 'ancDateYes', 'ancDateNo', '');">Yes</a>
                                    <a id="ancDateNo" runat="server" class="btn" title="No" onclick="ChangeColor(this, 'ancDateNo', 'ancDateYes', '');">No</a>
                                </div>
                                <!--End .btngrp-->
                            </div>
                            <!--End .column-->
                         </div>
                        <!--End .plms-fieldset-wrapper-->


                        <div id="dvQuestion5" runat="server" class="divHide">

                            <div class="plms-fieldset push-down pull-up-10">
                                <asp:HiddenField ID="hdQuestion5ID" runat="server" />
                                <label class="plms-label" for="licensee-owner-manager" id="lblQuestion5Text"></label>
                                <div class="plms-tooltip-parent">
                                    <asp:TextBox ID="txtBox1" runat="server" TextMode="MultiLine" CssClass="plms-input skin2"></asp:TextBox>
                                    <div class="plms-tooltip plms-tooltip-micro active left-top outside autow">
                                        <div>
                                            <p id="lblQuestion5TextToolTip"></p>
                                        </div>
                                        <!--End .plms-tooltip-body-->
                                    </div>
                                    <!--End .plms-tooltip-->
                                </div>
                                <!--End .plms-tooltip-parent-->
                            </div>

                            </div>
                            <!--End .plms-fieldset-->
                            <div>
                            </div>
                            <!--End .boxed-content-body-->
                            <nav class="pagination-nav">
                                <asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevious  %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large" OnClick="btnPrevious_Click" />
                                <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, lblNext %>" class="btn large align-r" OnClick="btnNext_Click" />
                            </nav>
                            <!--End .pagination-nav-->

                        
                        <!--End .boxed-content-->

                        <aside id="main-aside">
                            <div class="aside-toggle-handle">
                                <div class="flyout-message-wrapper">
                                    <a href="#toggle-aside" class="btn">
                                        <i class="icon"></i>
                                        <span><%=Resources.Resource.lblHideMenu %></span>
                                    </a>
                                </div>
                                <!--End .flyout-message-wrapper-->
                            </div>
                            <!--End .aside-toggle-handle-->


                            <nav class="aside-main-nav">

                                <UC1Menu:UC1 ID="lblmenu" runat="server" />

                            </nav>
                            <!--End .aside-main-nav-->
                        </aside>
                        <!--End #main-aside-->

                    </div>
                    <!--End .layout-sidebar-right-->
                </div>
                <footer>
                    <div class="btngrp">

                        <asp:Button ID="btnSubmitandExitLater" runat="server" title="<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click" />
                        <asp:Button ID="btnSubmitMyReport" runat="server" title="<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click" />


                    </div>
                </footer>

            </div>
            <!--End .wrapper-->
        </div>
    </section>
    <!--End #main-content-->

    <script>
        ( function ()
        {
            $( ".datepicker" ).datepicker( {
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                yearRange: GetDateRange(),
                maxDate: '+0D'
            } );
        } )();
        function GetDateRange()
        {
            var currentDate = new Date();
            var currentYear = currentDate.getFullYear();
            var previousYear = currentDate.getFullYear() - parseInt(<%= Resources.Resource.lblYearRange %> );
             return previousYear + ':' + currentYear;
         }
    </script>
</asp:Content>

