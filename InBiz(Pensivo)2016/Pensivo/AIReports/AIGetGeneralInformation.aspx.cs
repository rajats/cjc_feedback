﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;




public partial class AIReports_AIGetGeneralInformation : System.Web.UI.Page
{
  int aiFormID = 0;
  string sEmpHdrId = "0";
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    //int iEmpHeaderId = 0;
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);

                        txtFirstName.Value = BusinessUtility.GetString(result.EmpFirstName);
                        txtLastName.Value = BusinessUtility.GetString(result.EmpLastName);
                        //if (iEmpHeaderId == 0)
                        //{
                        //  iEmpHeaderId = BusinessUtility.GetInt(result.EmpExtID);
                        //}
                        //this.PopulateEmployeeDetails(result.EmpExtID);
                        sEmpHdrId = result.EmpExtID;
                    }

                }
                getFormData();
            }
        }
    }

    private void PopulateEmployeeDetails(string iEmpHeaderID)
    {
        EmphdrInfocl oEmphdrInfocl = new EmphdrInfocl(iEmpHeaderID);
        StringBuilder sbAddress = new StringBuilder();
        txtPhoneNo.Value = oEmphdrInfocl.EmpPhone;
        if (!string.IsNullOrEmpty(oEmphdrInfocl.EmpAddress1))
        {
          sbAddress.Append(oEmphdrInfocl.EmpAddress1);
          sbAddress.AppendLine();
        }
        if (!string.IsNullOrEmpty(oEmphdrInfocl.EmpAddress2))
        {
          sbAddress.Append(oEmphdrInfocl.EmpAddress2);
          sbAddress.AppendLine();
        }
        if (!string.IsNullOrEmpty(oEmphdrInfocl.EmpCity))
        {
          sbAddress.Append(oEmphdrInfocl.EmpCity);
          sbAddress.Append(" ");
        }
        if (!string.IsNullOrEmpty(oEmphdrInfocl.EmpState))
        {
          sbAddress.Append(oEmphdrInfocl.EmpState);
          sbAddress.AppendLine();
        }
        if (!string.IsNullOrEmpty(oEmphdrInfocl.EmpZipCode))
        {
          sbAddress.Append(oEmphdrInfocl.EmpZipCode);
        }
        txtAddress.Value = sbAddress.ToString();
    }



    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];

            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.GetGeneralInformation).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                  int count = 0;
                  foreach (var result in resultItem)
                  {
                    if (count == 0)
                    {
                      txtLastName.Value = Convert.ToString(result.AIQuestionAnswerText);
                    }
                    else if (count == 1)
                    {
                      txtFirstName.Value = Convert.ToString(result.AIQuestionAnswerText);
                    }
                    else if (count == 2)
                    {
                      txtPhoneNo.Value = Convert.ToString(result.AIQuestionAnswerText);
                    }
                    else if (count == 3)
                    {
                      txtAddress.Value = Convert.ToString(result.AIQuestionAnswerText);
                    }
                    count++;

                  }
                }
                else
                {
                  this.PopulateEmployeeDetails(sEmpHdrId);
                }

            }
        }
        catch { }
        finally { }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {


        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.GetGeneralInformation).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }

                if (!string.IsNullOrEmpty(hdQuestionID1.Value))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {

                             AIQuestionID = Convert.ToInt32(hdQuestionID1.Value),
                             AIQuestionType = QuestionType.TextType,
                             AIQuestionAnswerText = Convert.ToString(txtLastName.Value),
                             AIFormID = aiFormID,
                             AIPageName = PageName.GetGeneralInformation
                         });
                }
                if (!string.IsNullOrEmpty(hdQuestionID2.Value))
                {
                    objList.Add(
                      new AIQuestionAnswer
                      {

                          AIQuestionID = Convert.ToInt32(hdQuestionID2.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = Convert.ToString(txtFirstName.Value),
                          AIFormID = aiFormID,
                          AIPageName = PageName.GetGeneralInformation
                      });
                }

                if (!string.IsNullOrEmpty(hdQuestionID3.Value))
                {
                    objList.Add(
                      new AIQuestionAnswer
                      {

                          AIQuestionID = Convert.ToInt32(hdQuestionID3.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = Convert.ToString(txtPhoneNo.Value),
                          AIFormID = aiFormID,
                          AIPageName = PageName.GetGeneralInformation
                      });
                }

                if (!string.IsNullOrEmpty(hdQuestionID4.Value))
                {
                    objList.Add(
                      new AIQuestionAnswer
                      {

                          AIQuestionID = Convert.ToInt32(hdQuestionID4.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = Convert.ToString(txtAddress.Value),
                          AIFormID = aiFormID,
                          AIPageName = PageName.GetGeneralInformation
                      });
                }

                Session["AIReportQuestions"] = objList;
                Response.Redirect("AIEmpGeneralInformation.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }

    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIGeneralInformation.aspx", false);
    }
    protected void btnSaveAndExit_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitmyReport_Click(object sender, EventArgs e)
    {

    }
}