﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIIncidentReason : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();
        }
    }

    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            var resultItem = objList.Where(x => x.AIPageName == PageName.WVHGeneralPersonalInfo).OrderBy(x => x.AIQuestionID).ToList();
            if (resultItem != null && resultItem.Count > 0)
            {
                int count = 0;
                foreach (var result in resultItem)
                {
                    if (count == 0)
                    {
                        txtAIIncidentReason.Value = Convert.ToString(result.AIQuestionAnswerText);
                    }
                    count++;
                }
            }
        }
        catch { }
        finally { }
    } 
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIAccidentIncidentDetail.aspx", false);
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        // get the Personal information and insert all info in database and then redirect it to 
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHGeneralPersonalInfo).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }



                objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdAIDescriptionofAccidentQuestionID.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = Convert.ToString(txtAIIncidentReason.Value),
                        AIFormID = aiFormID,
                        AIPageName = PageName.WVHGeneralPersonalInfo
                    });

                

                Session["AIReportQuestions"] = objList;
            }
            Response.Redirect("AIContributingFactor.aspx", false);
        }
        catch (Exception ex) { throw ex; }
        finally { }

    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        // Redirect to AI DashBoard 
    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBoard.aspx", false);
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click1(object sender, EventArgs e)
    {

    }
}