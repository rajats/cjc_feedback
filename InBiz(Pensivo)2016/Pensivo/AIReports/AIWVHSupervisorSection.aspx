﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIWVHSupervisorSection.aspx.cs" Inherits="AIReports_AIWVHSupervisorSection" %>

<%@ Register Src="~/AIReports/UserControl/IncidentMenu.ascx" TagName="UC1" TagPrefix="UC1Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="../_styles/ai-reports.css" />

    <script type="text/javascript">
        function GetWVHSuperVisor()
        {
            $.ajax( {
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "WVHSupervisorSection" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function ( data )
                {
                    var index = 0;
                    $.each( data.d, function ( index, value )
                    {                         
                        if ( index == 0 )
                        {
                            $( "#<%=hdWVHSupervisorQuestionID1.ClientID%>" ).val( value.QuestionID );
                            $( "#spappropriatedebriefing" ).html( value.QuestionText );
                        }
                        if ( index == 1 )
                        {
                            $( "#<%=hdWVHSupervisorQuestionID2.ClientID%>" ).val( value.QuestionID );
                                $( "#lblWVHWVHSupervisorQuestiontext1" ).html( value.QuestionText );
                                $( "#<%=txtdebriefingperson.ClientID%>" ).attr( "placeholder", value.QuestionText );
                                $( "#lblWVHWVHSupervisorQuestiontext1ToolTip" ).html( value.QuestionText );
                              
                            }
                            if ( index == 2 )
                            {
                                $( "#<%=hdWVHSupervisorQuestionID3.ClientID%>" ).val( value.QuestionID );
                                $( "#lblWVHWVHSupervisorQuestiontext2" ).html( value.QuestionText );
                                $( "#<%=txtdebriefingdate.ClientID%>" ).attr( "placeholder", value.QuestionText );
                                $( "#lblWVHWVHSupervisorQuestiontext2ToolTip" ).html( value.QuestionText );
                            }
                            if ( index == 3 )
                            {
                                $( "#<%=hdWVHSupervisorQuestionID4.ClientID%>" ).val( value.QuestionID );
                                $( "#spemployeenotified" ).html( value.QuestionText );
                            }

                            if ( index == 4)
                            {
                                $( "#<%=hdWVHSupervisorQuestionID5.ClientID%>" ).val( value.QuestionID );
                                $( "#spformeremployee" ).html( value.QuestionText );
                            } 
                            
                            if ( index == 5)
                            {
                                $( "#<%=hdWVHSupervisorQuestionID6.ClientID%>" ).val( value.QuestionID );
                                $( "#spspousepartner" ).html( value.QuestionText );
                            }
                            if ( index == 6)
                            {
                                $( "#<%=hdWVHSupervisorQuestionID7.ClientID%>" ).val( value.QuestionID );
                                $( "#spfamily" ).html( value.QuestionText );
                            } 

                        } );
                    },
                    error: function ( XMLHttpRequest, textStatus, errorThrown )
                    {
                        //alert( errorThrown );
                    }
                } );
            }
            $( document ).ready( function ()
            {
              GetWVHSuperVisor();              

            });

            function validate() {
              var chk = document.getElementById("ContentPlaceHolder1_Chkappropriatedebriefing");
              if ( chk.checked )
              {                  
                  $("#<%=txtdebriefingperson.ClientID%>").val(" ");
                  $( "#<%=txtdebriefingdate.ClientID%>" ).val( " " );     
                  $( "#<%=dvdBrief.ClientID%>" ).show();                  
              }
              else
              {
                  $( "#<%=dvdBrief.ClientID%>" ).hide();                
              }
            }
    </script>

    <section id="main-content" class="pg-ca-preventions">
        <span class="mask"></span>
        <div class="wrapper">
            <div class="plms-alert secondary-clr">

                <p>
                    <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>

            </div>
            <!--End .plms-alert-->

            <div class="plms-alert neutral">
                <p class="mandatory-fields-message">
                    <i class="req-icon" title="Mandatory Field">*</i>
                    <%=Resources.Resource.lblMandatoryField %>
                </p>
            </div>
            <!--End .plms-alert--> 

            <div class="layout-sidebar-right">

                <div class="boxed-content">

                    <div class="boxed-content-body">

                        <header class="boxed-content-header">
                            <h2><%=Resources.Resource.lblManagerText %> </h2>
                                            <h6>  <%=Resources.Resource.lblClicklAll %></h6>
                        </header>

                        <div class="fieldset-group">
                            <asp:HiddenField ID="hdWVHSupervisorQuestionID1" runat="server" />
                            <div class="label-checkbox-group">
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_Chkappropriatedebriefing">
                                    <input type="checkbox" id="Chkappropriatedebriefing" name="appropriate-debriefing"   runat="server" onchange="validate();"/>                                    
                                    <span id="spappropriatedebriefing"></span>
                                </label>
                            </div>
                            <!--End .label-checkbox-group-->
                        </div>
                        <!--End .fieldset-group-->

                      <div id="dvdBrief"  runat="server"  class="divHide">
                        <div class="plms-fieldset pull-up-10">
                            <asp:HiddenField ID="hdWVHSupervisorQuestionID2" runat="server" />
                            <label class="plms-label is-hidden" for="debriefing-person" id="lblWVHWVHSupervisorQuestiontext1"></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtdebriefingperson" runat="server" CssClass="plms-input skin2"></asp:TextBox>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="lblWVHWVHSupervisorQuestiontext1ToolTip"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                        <div class="plms-fieldset push-down">
                            <asp:HiddenField ID="hdWVHSupervisorQuestionID3" runat="server" />
                            <label class="plms-label is-hidden" for="debriefing-date" id="lblWVHWVHSupervisorQuestiontext2"></label>
                            <div class="plms-tooltip-parent">
                                <i class="req-icon" title="Mandatory Field">*</i>
                                <asp:TextBox ID="txtdebriefingdate" runat="server" CssClass="plms-input datepicker skin2"></asp:TextBox>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="lblWVHWVHSupervisorQuestiontext2ToolTip"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->
                      </div>


                      <div class="fieldset-group">
                            <div class="label-checkbox-group">
                                <asp:HiddenField ID="hdWVHSupervisorQuestionID4" runat="server" />
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_Chkemployeenotified"  runat="server">
                                    <input type="checkbox" id="Chkemployeenotified" name="employee-notified" runat="server" />
                                    <span id="spemployeenotified"></span>
                                </label>
                                <asp:HiddenField ID="hdWVHSupervisorQuestionID5" runat="server" />
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_Chkformeremployee"  runat="server">
                                    <input type="checkbox" id="Chkformeremployee" name="former-employee" runat="server" />
                                    <span id="spformeremployee"></span>
                                </label>
                                <asp:HiddenField ID="hdWVHSupervisorQuestionID6" runat="server" />
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_Chkspousepartner"  runat="server">
                                    <input type="checkbox" id="Chkspousepartner" name="spouse-partner" runat="server" />
                                    <span id="spspousepartner"></span>
                                </label>
                                <asp:HiddenField ID="hdWVHSupervisorQuestionID7" runat="server" />
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_family"  runat="server">
                                    <input type="checkbox" id="family" name="family" runat="server" />
                                    <span id="spfamily"></span>
                                </label>
                            </div>
                            <!--End .label-checkbox-group-->
                        </div>
                        <!--End .fieldset-group-->


                    </div>
                    <!--End .boxed-content-body-->
                    <nav class="pagination-nav">
                        <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />
                        <asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevious  %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large" OnClick="btnPrevious_Click" />

                    </nav>
                    <!--End .pagination-nav-->

                </div>
                <!--End .boxed-content-->

                <aside id="main-aside">
                    <div class="aside-toggle-handle">
                        <div class="flyout-message-wrapper">
                            <a href="#toggle-aside" class="btn">
                                <i class="icon"></i>
                                <span><%=Resources.Resource.lblHideMenu %></span>
                            </a>
                        </div>
                        <!--End .flyout-message-wrapper-->
                    </div>
                    <!--End .aside-toggle-handle-->

                    <nav class="aside-main-nav">
                        <UC1Menu:UC1 ID="lblmenu" runat="server" />
                    </nav>
                    <!--End .aside-main-nav-->
                </aside>
                <!--End #main-aside-->

            </div>
            <!--End .layout-sidebar-right-->

            <footer>
                <div class="btngrp">
                    <asp:Button ID="btnSaveAndExit" runat="server" title="<%$ Resources:Resource, lblSaveAndExitLater  %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn large" OnClick="btnSaveAndExit_Click" />
                    <asp:Button ID="btnSubmitmyReport" runat="server" title="<%$ Resources:Resource, lblSubmitMyReport  %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn large align-r" OnClick="btnSubmitmyReport_Click" />

                </div>
            </footer>

        </div>
        <!--End .wrapper-->
    </section>
    <!--End #main-content-->

   
  <script> 
    (function () {
      $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        yearRange: GetDateRange(),
        maxDate: '+0D'
      });
    })();

    function GetDateRange() {
      var currentDate = new Date();
      var currentYear = currentDate.getFullYear();
      var previousYear = currentDate.getFullYear() - parseInt(<%= Resources.Resource.lblYearRange %> );
      return previousYear + ':' + currentYear;
    }
</script>

</asp:Content>

