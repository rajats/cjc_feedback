﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AICauseofInjury : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();
        }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            var resultItem = objList.Where(x => x.AIPageName == PageName.CauseofInjury).OrderBy(x => x.AIQuestionID).ToList();
            if (resultItem != null && resultItem.Count > 0)
            {
                int count = 0;
                foreach (var result in resultItem)
                {
                    if (count == 0 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox1.Checked = true;
                    }
                    else if (count == 1 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox2.Checked = true;
                    }
                    else if (count == 2 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox3.Checked = true;
                    }
                    else if (count == 3 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox4.Checked = true;
                    }
                    else if (count == 4 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox5.Checked = true;
                    }
                    else if (count == 5 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox6.Checked = true;
                    }
                    else if (count == 6 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox7.Checked = true;
                    }
                    else if (count == 7 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox8.Checked = true;
                    }
                    else if (count == 8 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox9.Checked = true;
                    }
                    else if (count == 9 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox10.Checked = true;
                        dvOthers.Attributes.Remove("Class");
                        dvOthers.Attributes.Add("Class", "divShow");
                    }
                    else if (count == 10 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        txtQuestion11.Value = result.AIQuestionAnswerText;
                    }
                    count++;
                }
            }
        }
        catch { }
        finally { }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.CauseofInjury).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }
                

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID1.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdQuestionID1.Value),
                             AIQuestionType = QuestionType.BooleanType,
                             AIQuestionAnswerText = Convert.ToString(chkbox1.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                             AIFormID = aiFormID,
                             AIPageName = PageName.CauseofInjury,
                             AISequence = 1
                         });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID2.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdQuestionID2.Value),
                            AIQuestionType = QuestionType.BooleanType,
                            AIQuestionAnswerText = Convert.ToString(chkbox2.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                            AIFormID = aiFormID,
                            AIPageName = PageName.CauseofInjury,
                            AISequence = 2
                        });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID3.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdQuestionID3.Value),
                            AIQuestionType = QuestionType.BooleanType,
                            AIQuestionAnswerText = Convert.ToString(chkbox3.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                            AIFormID = aiFormID,
                            AIPageName = PageName.CauseofInjury,
                            AISequence = 3
                        });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID4.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdQuestionID4.Value),
                            AIQuestionType = QuestionType.BooleanType,
                            AIQuestionAnswerText = Convert.ToString(chkbox4.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                            AIFormID = aiFormID,
                            AIPageName = PageName.CauseofInjury,
                            AISequence = 4
                        });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID5.Value)))
                {
                    objList.Add(
                       new AIQuestionAnswer
                       {
                           AIQuestionID = Convert.ToInt32(hdQuestionID5.Value),
                           AIQuestionType = QuestionType.BooleanType,
                           AIQuestionAnswerText = Convert.ToString(chkbox5.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                           AIFormID = aiFormID,
                           AIPageName = PageName.CauseofInjury,
                           AISequence = 5
                       });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID6.Value)))
                {
                    objList.Add(
                     new AIQuestionAnswer
                     {
                         AIQuestionID = Convert.ToInt32(hdQuestionID6.Value),
                         AIQuestionType = QuestionType.BooleanType,
                         AIQuestionAnswerText = Convert.ToString(chkbox6.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                         AIFormID = aiFormID,
                         AIPageName = PageName.CauseofInjury,
                         AISequence = 6
                     });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID7.Value)))
                {
                    objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdQuestionID7.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(chkbox7.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                        AIFormID = aiFormID,
                        AIPageName = PageName.CauseofInjury,
                        AISequence = 7
                    });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID8.Value)))
                {
                    objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdQuestionID8.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(chkbox8.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                        AIFormID = aiFormID,
                        AIPageName = PageName.CauseofInjury,
                        AISequence = 8
                    });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID9.Value)))
                {
                    objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdQuestionID9.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(chkbox9.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                        AIFormID = aiFormID,
                        AIPageName = PageName.CauseofInjury,
                        AISequence = 9
                    });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID10.Value)))
                {
                    objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdQuestionID10.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(chkbox10.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                        AIFormID = aiFormID,
                        AIPageName = PageName.CauseofInjury,
                        AISequence = 10
                    });
                }
                if (chkbox10.Checked)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID11.Value)))
                    {
                        objList.Add(
                  new AIQuestionAnswer
                  {
                      AIQuestionID = Convert.ToInt32(hdQuestionID11.Value),
                      AIQuestionType = QuestionType.BooleanType,
                      AIQuestionAnswerText = txtQuestion11.Value,
                      AIFormID = aiFormID,
                      AIPageName = PageName.CauseofInjury,
                      AISequence = 11
                  });
                    }
                }
                Session["AIReportQuestions"] = objList;

                managePreviousNext("");

                //RedirectionLink();

                Response.Redirect("AINatureofInjury.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    //private void RedirectionLink()
    //{
    //    string redirectURL = string.Empty;
    //    List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
    //    try
    //    {
    //        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];

    //        var ISPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == "PropertyDamageQuestion");
    //        if (ISPropertyDamage != null)
    //        {
    //            if (Convert.ToString(ISPropertyDamage.AIQuestionAnswerText) == "1")
    //            {
    //                Response.Redirect("AIGetPropertyDamageDetail.aspx");
    //            }
    //            else
    //            {
    //                Response.Redirect("AIActionAndPrevention.aspx");
    //            }
    //        }
    //        else
    //        {
    //            Response.Redirect("AIActionAndPrevention.aspx");
    //        }                         
    //    }
    //    catch { }
    //    finally { }
    //} 
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }
    private void managePreviousNext(string NextPageUrl)
    {
        List<ManagePagePreviousNext> objPage = new List<ManagePagePreviousNext>();
        try
        {
            objPage = (List<ManagePagePreviousNext>)Session["AIPagesList"];
            if (objPage != null)
            {
                var resultItem = objPage.Where(x => x.pageName == PageName.ContributingFactor).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objPage.Remove(removeItem);
                    }
                }
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AICauseofInjury.aspx", NextPageUrl = NextPageUrl, pageName = PageName.CauseofInjury, PreviousPageUrl = "" });
            }
            else
            {
                objPage = new List<ManagePagePreviousNext>();
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AICauseofInjury.aspx", NextPageUrl = NextPageUrl, pageName = PageName.CauseofInjury, PreviousPageUrl = "" });
            }
            Session["AIPagesList"] = objPage;
        }
        catch { }
        finally { }
    }
}