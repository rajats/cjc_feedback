﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Services;
using iTECH.Pensivo.BusinessLogic;
using System.Data;

public partial class AIReports_CommonInterface : System.Web.UI.Page
{   
    protected void Page_Load(object sender, EventArgs e)
    {


    }
    [WebMethod]
    public static List<AIQuestions>  getQuestions(string pageType)
    {
        DataTable dt = new DataTable();
        AIReportCls objAIReports = new AIReportCls();
        List<AIQuestions> objAIQuestion = new List<AIQuestions>();
        try 
        {
            dt = objAIReports.getQuestionDetails(pageType);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    objAIQuestion.Add(new AIQuestions { QuestionID =  Convert.ToInt32(dr["aiquestionID"]), QuestionText =  Convert.ToString( dr["aiquestionText"]) });
                }
            } 
        }
        catch { }
        finally { }
        return objAIQuestion;
    } 

}