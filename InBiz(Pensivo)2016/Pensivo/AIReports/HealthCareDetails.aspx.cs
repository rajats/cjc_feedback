﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_HealthCareDetails : System.Web.UI.Page
{
    List<Employee> empObj = new List<Employee>();
    int aiFormID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
        }
    }     
    protected void btnNext_Click(object sender, EventArgs e)
    {
        // Redirect to Property Damage Description
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.HealthCareDetail).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                } 

                objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32( hdHealthCareDetailQuestionID1.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = Convert.ToString( txtHealthCareDetailQuestion1Ans.Text),
                        AIFormID = aiFormID,
                        AIPageName = PageName.HealthCareDetail
                    });

                objList.Add(
                   new AIQuestionAnswer
                   {
                       AIQuestionID = Convert.ToInt32(hdHealthCareDetailQuestionID2.Value),
                       AIQuestionType = QuestionType.TextType,
                       AIQuestionAnswerText = Convert.ToString(txtHealthCareDetailQuestion2Ans.Text),
                       AIFormID = aiFormID,
                       AIPageName = PageName.HealthCareDetail
                   });

                objList.Add(
                 new AIQuestionAnswer
                 {
                     AIQuestionID = Convert.ToInt32(hdHealthCareDetailQuestionID3.Value),
                     AIQuestionType = QuestionType.TextType,
                     AIQuestionAnswerText = Convert.ToString(txtHealthCareDetailQuestion3Ans.Text),
                     AIFormID = aiFormID,
                     AIPageName = PageName.HealthCareDetail
                 });
                


                objList.Add(
                 new AIQuestionAnswer
                 {
                     AIQuestionID = Convert.ToInt32(hdHealthCareDetailQuestionID4.Value),
                     AIQuestionType = QuestionType.TextType,
                     AIQuestionAnswerText = Convert.ToString(txtHealthCareDetailQuestion4Ans.Text),
                     AIFormID = aiFormID,
                     AIPageName = PageName.HealthCareDetail
                 });


                objList.Add(
               new AIQuestionAnswer
               {
                   AIQuestionID = Convert.ToInt32(hdHealthCareDetailQuestionID5.Value),
                   AIQuestionType = QuestionType.TextType,
                   AIQuestionAnswerText = Convert.ToString(txtHealthCareDetailQuestion5Ans.Text),
                   AIFormID = aiFormID,
                   AIPageName = PageName.HealthCareDetail
               });


            objList.Add(
             new AIQuestionAnswer
             {
                 AIQuestionID = Convert.ToInt32(hdHealthCareDetailQuestionID6.Value),
                 AIQuestionType = QuestionType.TextType,
                 AIQuestionAnswerText = Convert.ToString(txtHealthCareDetailQuestion6Ans.Text),
                 AIFormID = aiFormID,
                 AIPageName = PageName.HealthCareDetail
             });

            objList.Add(
            new AIQuestionAnswer
            {
               AIQuestionID = Convert.ToInt32(hdHealthCareDetailQuestionID7.Value),
               AIQuestionType = QuestionType.TextType,
               AIQuestionAnswerText = Convert.ToString(txtHealthCareDetailQuestion7Ans.Text),
               AIFormID = aiFormID,
               AIPageName = PageName.HealthCareDetail
            });  

            Session["AIReportQuestions"] = objList;
            }
            managePreviousNext("");


            RedirectionLink();

            //Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
        }
        catch (Exception ex) { throw ex; }
        finally { }  
    }



    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    { 
        // redirect to AI Dashboard
        Response.Redirect("AIdashBoard.aspx", false);
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIReturntowork1.aspx", false);     
    }     
    protected void btnSubmitwithoutSaving_Click1(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBoard.aspx", false);
    }

    private void managePreviousNext(string NextPageUrl)
    {
        List<ManagePagePreviousNext> objPage = new List<ManagePagePreviousNext>();
        try
        {
            objPage = (List<ManagePagePreviousNext>)Session["AIPagesList"];
            if (objPage != null)
            {
                var resultItem = objPage.Where(x => x.pageName == PageName.ReturnToWork1).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objPage.Remove(removeItem);
                    }
                }
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "HealthCareDetails.aspx", NextPageUrl = NextPageUrl, pageName = PageName.HealthCareDetail, PreviousPageUrl = "" });
            }
            else
            {
                objPage = new List<ManagePagePreviousNext>();
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "HealthCareDetails.aspx", NextPageUrl = NextPageUrl, pageName = PageName.HealthCareDetail, PreviousPageUrl = "" });
            }
            Session["AIPagesList"] = objPage;

        }
        catch { }
        finally { }
    }

    private void RedirectionLink()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {

            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            // Check Is Licensee
            var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.Location && x.IsLicensee == "1");
            if (ISLicensee != null)
            {
                // Get result from all scenarios
                var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails7.aspx", false);
                    return;
                }
                // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails7.aspx", false);
                    return;
                }
                // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails7.aspx", false);
                    return;
                }
                // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails7.aspx", false);
                    return;
                }
                // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 6 : Personal Injury AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails7.aspx", false);
                    return;
                }
                // Case 7 : Personal Injury AND First Aid AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 8 : Personal Injury AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 9 : Personal Injury AND First Aid AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 10: Personal Injury AND First Aid AND Health Care
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 11: Personal Injury AND Health Care
                if (IsPerosnalInjury != null && IsHealthCare != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 12: Property Damage AND Lost Time
                if (IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 13: Personal Injury AND First Aid
                if (IsPerosnalInjury != null && IsFirstAidBox != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 14: Property Damage
                if (IsPropertyDamage != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 15: Lost Time
                if (IsLostTime != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 16: Personal Injury
                if (IsPerosnalInjury != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
            }
            else
            {
                // Get result from all scenarios
                var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails7.aspx", false);
                    return;
                }
                // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails7.aspx", false);
                    return;
                }
                // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails7.aspx", false);
                    return;
                }
                // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails7.aspx", false);
                    return;
                }
                // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 6 : Personal Injury AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDetails7.aspx", false);
                    return;
                }
                // Case 7 : Personal Injury AND First Aid AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 8 : Personal Injury AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 9 : Personal Injury AND First Aid AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 10: Personal Injury AND First Aid AND Health Care
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 11: Personal Injury AND Health Care
                if (IsPerosnalInjury != null && IsHealthCare != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 12: Property Damage AND Lost Time
                if (IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 13: Personal Injury AND First Aid
                if (IsPerosnalInjury != null && IsFirstAidBox != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 14: Property Damage
                if (IsPropertyDamage != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 15: Lost Time
                if (IsLostTime != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
                // Case 16: Personal Injury
                if (IsPerosnalInjury != null)
                {
                    Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                    return;
                }
 
            }

        }
        catch { }
        finally { }
    }

}