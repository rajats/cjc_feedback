﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="HealthCareDetails.aspx.cs" Inherits="AIReports_HealthCareDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <link rel="stylesheet" href="../_styles/ai-reports.css" />
 <link rel="stylesheet" href="../_styles/ai-reports.css" />

   <script  type="text/javascript">
       function GetHealthCareDetailQuestions() {
           $.ajax({
               type: "POST",
               url: "CommonInterface.aspx/getQuestions",
               data: "{pageType:'" + "HealthCareDetail" + "'}",
               dataType: "json",
               contentType: "application/json; charset=utf-8",
               success: function (data) {
                   var index = 0;
                   $.each(data.d, function (index, value) {
                       if (index == 0)
                       {
                           $("#<%=hdHealthCareDetailQuestionID1.ClientID%>").val(value.QuestionID);
                           $("#lblHealthCareDetailQuestionText1").html(value.QuestionText);
                           $("#lblHealthCareDetailQuestionText1ToolTip").html(value.QuestionText);
                           $("#<%=txtHealthCareDetailQuestion1Ans.ClientID%>").attr("placeholder", value.QuestionText);      
                           
                       }
                       if (index == 1)
                       {
                           $("#<%=hdHealthCareDetailQuestionID2.ClientID%>").val(value.QuestionID);
                           $("#lblHealthCareDetailQuestionText2").html(value.QuestionText);
                           $("#lblHealthCareDetailQuestionText2ToolTip").html(value.QuestionText);
                           $("#<%=txtHealthCareDetailQuestion2Ans.ClientID%>").attr("placeholder", value.QuestionText);      
                       }
                       if (index == 2)
                       {
                           $("#<%=hdHealthCareDetailQuestionID3.ClientID%>").val(value.QuestionID);
                           $("#lblHealthCareDetailQuestionText3").html(value.QuestionText);
                           $("#lblHealthCareDetailQuestionText3ToolTip").html(value.QuestionText);
                           $("#<%=txtHealthCareDetailQuestion3Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                       }
                       if (index == 3)
                       {
                           $("#<%=hdHealthCareDetailQuestionID4.ClientID%>").val(value.QuestionID);
                           $("#lblHealthCareDetailQuestionText4").html(value.QuestionText);
                           $("#lblHealthCareDetailQuestionText4ToolTip").html(value.QuestionText);
                           $("#<%=txtHealthCareDetailQuestion4Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                       }
                       if (index == 4)
                       {
                           $("#<%=hdHealthCareDetailQuestionID5.ClientID%>").val(value.QuestionID);
                           $("#lblHealthCareDetailQuestionText5").html(value.QuestionText);
                           $("#lblHealthCareDetailQuestionText5ToolTip").html(value.QuestionText);
                           $("#<%=txtHealthCareDetailQuestion5Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                       }

                       if (index == 5)
                       {
                           $("#<%=hdHealthCareDetailQuestionID6.ClientID%>").val(value.QuestionID);
                           $("#lblHealthCareDetailQuestionText6").html(value.QuestionText);
                           $("#lblHealthCareDetailQuestionText6ToolTip").html(value.QuestionText);
                           $("#<%=txtHealthCareDetailQuestion6Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                       }

                       if (index == 6) {
                           $("#<%=hdHealthCareDetailQuestionID7.ClientID%>").val(value.QuestionID);
                           $("#lblHealthCareDetailQuestionText7").html(value.QuestionText);
                           $("#lblHealthCareDetailQuestionText7ToolTip").html(value.QuestionText);
                           $("#<%=txtHealthCareDetailQuestion7Ans.ClientID%>").attr("placeholder", value.QuestionText); 
                         }

                       index = parseInt(index) + 1;
                   });
               },
               error: function (XMLHttpRequest, textStatus, errorThrown) {
                   //alert( errorThrown );
               }
           });
       }
       $(document).ready(function () {
           GetHealthCareDetailQuestions();
       });
     </script>

<section id="main-content">
<span class="mask"></span>
<div class="wrapper">
	<div class="plms-alert secondary-clr">     
            <p> 
               <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
        </p> 
	</div><!--End .plms-alert-->	
 <div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		 <%=Resources.Resource.lblMandatoryField %>
		</p>
	</div><!--End .plms-alert-->

	<div class="layout-sidebar-right">
		<div class="boxed-content">			
			<div class="boxed-content-body">				
			<h2><%=Resources.Resource.lblDetails %></h2>
                			
			<div class="plms-fieldset pull-up-10">
            <asp:HiddenField  ID="hdHealthCareDetailQuestionID1"  runat="server"/>   
			<label class="plms-label is-hidden" for="licensee-name" id="lblHealthCareDetailQuestionText1"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtHealthCareDetailQuestion1Ans" runat="server" CssClass="plms-input skin2" >
            </asp:TextBox>  
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="lblHealthCareDetailQuestionText1ToolTip">  </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			
                  
			<div class="plms-fieldset">
            <asp:HiddenField  ID="hdHealthCareDetailQuestionID2"  runat="server"/> 
			<label class="plms-label is-hidden" for="licensee-address" id="lblHealthCareDetailQuestionText2"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtHealthCareDetailQuestion2Ans" runat="server" CssClass="plms-input skin2" >
            </asp:TextBox> 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="lblHealthCareDetailQuestionText2ToolTip"></p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
                 

                	
			<div class="plms-fieldset pull-up-10"> 
            <asp:HiddenField  ID="hdHealthCareDetailQuestionID3"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblHealthCareDetailQuestionText3"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtHealthCareDetailQuestion3Ans" runat="server" CssClass="filter-key plms-input skin2" >
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblHealthCareDetailQuestionText3ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->

                 
			<div class="plms-fieldset pull-up-10"> 
            <asp:HiddenField  ID="hdHealthCareDetailQuestionID4"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblHealthCareDetailQuestionText4"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtHealthCareDetailQuestion4Ans" runat="server" CssClass="filter-key plms-input skin2"  >
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblHealthCareDetailQuestionText4ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->



                	<div class="plms-fieldset pull-up-10"> 
            <asp:HiddenField  ID="hdHealthCareDetailQuestionID5"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblHealthCareDetailQuestionText5"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtHealthCareDetailQuestion5Ans" runat="server" CssClass="filter-key plms-input skin2"  >
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblHealthCareDetailQuestionText5ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->

 
            <div class="plms-fieldset pull-up-10"> 
            <asp:HiddenField  ID="hdHealthCareDetailQuestionID6"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblHealthCareDetailQuestionText6"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtHealthCareDetailQuestion6Ans" runat="server" CssClass="filter-key plms-input skin2"  >
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblHealthCareDetailQuestionText6ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->


                 

             <div class="plms-fieldset pull-up-10"> 
            <asp:HiddenField  ID="hdHealthCareDetailQuestionID7"  runat="server"/>                         
			<label class="plms-label is-hidden" for="reasons-not-advising-supervisor" id="lblHealthCareDetailQuestionText7"></label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtHealthCareDetailQuestion7Ans" runat="server" CssClass="filter-key plms-input skin2">
            </asp:TextBox>   			 
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblHealthCareDetailQuestionText7ToolTip"> </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->

                 
					
			</div><!--End .boxed-content-body-->


			
			<nav class="pagination-nav">

                <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"    />
                        <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"  />
                 
			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span>Hide Menu</span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
			<ul>
			<li><a href="#nogo" title="">General Information</a></li>
			<li class="is-active"><a href="#nogo" title="">Details</a></li>
			<li><a href="#nogo" title="">Description of Accident / Incident</a></li>
			<li><a href="#nogo" title="">Contributing Factors / Conditions</a></li>
			<li><a href="#nogo" title="">Corrective Actions &amp; Prevention</a></li>
			<li><a href="#nogo" title="">Witnesses / Co-Workers</a></li>
			<li><a href="#nogo" title="">Worker's Comments</a></li>
			<li><a href="#nogo" title="">People Involved</a></li>
			</ul>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
  <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn" OnClick="btnSubmitwithoutSaving_Click1" />
	</div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
     
</asp:Content>

