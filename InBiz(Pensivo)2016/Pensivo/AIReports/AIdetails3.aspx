﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIdetails3.aspx.cs" Inherits="AIReports_AIdetails3" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">     
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../_styles/ai-reports.css" />
     <script   type="text/javascript">
         function GetAIDetails2()
         {
             $.ajax( {
                 type: "POST",
                 url: "CommonInterface.aspx/getQuestions",
                 data: "{pageType:'" + "IncidentAccidentDetail2" + "'}",
                 dataType: "json",
                 contentType: "application/json; charset=utf-8",
                 success: function ( data )
                 {
                     var index = 0;
                     $.each( data.d, function ( index, value )
                     {
                         if ( index == 0 )
                         {
                             $( "#<%=hdAIQuestionID1.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionIDText1ToolTip" ).html( value.QuestionText );
                             $( "#<%=txtdatelastworked.ClientID%>" ).attr( "placeholder", value.QuestionText );
                              $( "#AIQuestionIDText1" ).html( value.QuestionText );
                         }
                         if ( index == 1 )
                         {
                             $( "#<%=hdAIQuestionID2.ClientID%>" ).val( value.QuestionID );
                              $( "#AIQuestionIDText2ToolTip" ).html( value.QuestionText );
                              $( "#<%=txtshiftstart.ClientID%>" ).attr( "placeholder", value.QuestionText );
                              $( "#AIQuestionIDText2" ).html( value.QuestionText );
                         }
                         if ( index == 2 )
                         {
                             $( "#<%=hdAIQuestionID3.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionIDText3ToolTip" ).html( value.QuestionText );
                             $( "#<%=txtshiftend.ClientID%>" ).attr( "placeholder", value.QuestionText );
                              $( "#AIQuestionIDText3" ).html( value.QuestionText );
                          }
                         index = parseInt( index ) + 1;
                     });
                 },
                 error: function ( XMLHttpRequest, textStatus, errorThrown )
                 {
                     //alert( errorThrown );
                 }
             });
          }
          $( document ).ready( function ()
          {
              GetAIDetails2();
          } );
          </script> 
     
    
    
<section id="main-content">
<span class="mask"></span>
<div class="wrapper">
	<div class="plms-alert secondary-clr">	
        <p> 
             <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
         </p>	 
    
    </div><!--End .plms-alert-->
    
    	 
 <div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
	  <%=Resources.Resource.lblMandatoryField %></p>
	</div><!--End .plms-alert--> 

	<div class="layout-sidebar-right">
		<div class="boxed-content">			
			<div class="boxed-content-body">				
			<h2><%=Resources.Resource.lblDetails %></h2>			
			<div class="plms-fieldset pull-up-10 push-down">
                <asp:HiddenField  ID="hdAIQuestionID1"  runat="server" />                
				<label class="plms-label is-hidden" for="date-last-worked"  id="AIQuestionIDText1"></label>				
				<div class="plms-tooltip-parent">
					<i class="req-icon" title="Mandatory Field">*</i>
					<input type="text"  class="plms-input datepicker skin2" id="txtdatelastworked" name="date-last-worked"  runat="server" />
					
					<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
						<div class="plms-tooltip-body">
							<p  id="AIQuestionIDText1ToolTip"></p>
						</div><!--End .plms-tooltip-body-->
					</div><!--End .plms-tooltip-->
				</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			
			<p class="bold clearfix">Scheduled Shift on Day of Incident</p>
			
			<div class="plms-fieldset pull-up">
                <asp:HiddenField  ID="hdAIQuestionID2"  runat="server" />     
				<label class="plms-label is-hidden" for="shift-start"  id ="AIQuestionIDText2"> </label>
				
				<div class="plms-tooltip-parent">
					<i class="req-icon" title="Mandatory Field">*</i>
					<input type="text" maxlength="5" class="plms-input skin2" id="txtshiftstart" name="shift-start" runat="server"/>
					
					<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
						<div class="plms-tooltip-body">
							<p  id="AIQuestionIDText2ToolTip"></p>
						</div><!--End .plms-tooltip-body-->
					</div><!--End .plms-tooltip-->
				</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			
			<div class="plms-fieldset">
                <asp:HiddenField  ID="hdAIQuestionID3"  runat="server" />     
				<label class="plms-label is-hidden" for="shift-end"   id ="AIQuestionIDText3"> </label>
				
				<div class="plms-tooltip-parent">
					<i class="req-icon" title="Mandatory Field">*</i>
					<input type="text"   maxlength="5" class="plms-input skin2" id="txtshiftend" name="shift-end"   runat="server" />
					
					<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
						<div class="plms-tooltip-body">
							<p  id="AIQuestionIDText3ToolTip"></p>
						</div><!--End .plms-tooltip-body-->
					</div><!--End .plms-tooltip-->
				</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
					
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">

                
                        <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"   />
                        <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"/>



<%--			<a href="ai-reports-21-2.php" title="Previous" class="btn large">Previous</a>
			<a href="ai-reports-21-4.php" title="Next" class="btn large align-r">Next</a>--%>




			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
							<span><%=Resources.Resource.lblHideMenu %></span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
			 <UC1Menu:UC1  ID="lblmenu"   runat="server"/>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">

 
                    <asp:Button ID="btnSubmitandExitLater" runat="server" title="<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click" />
                    <asp:Button ID="btnSubmitMyReport" runat="server" title="<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click" />

                </div>
            </footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
<script>   
  (function () {
    $(".datepicker").datepicker({
      dateFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true,
      yearRange: GetDateRange(),
      maxDate: '+0D'
    });
  })();

  function GetDateRange() {
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    var previousYear = currentDate.getFullYear() - parseInt(<%= Resources.Resource.lblYearRange %>);
    return previousYear + ':' + currentYear;
  }
</script>
</asp:Content>

