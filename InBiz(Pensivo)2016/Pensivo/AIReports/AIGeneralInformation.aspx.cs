﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIGeneralInformation : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);                         
                    }
                }
            }
            getFormData();
        }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {


                var resultItem = objList.Where(x => x.AIPageName == PageName.GeneralInformation).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 0;
                    foreach (var result in resultItem)
                    {
                      if (count == 0 && result.AISequence == 1)
                        {

                          if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                          {
                            hdroutineQuestionAns1.Value = "1";
                            aRoutineYes.Attributes.Remove("class");
                            aRoutineYes.Attributes.Add("class", "btnChageColor");
                          }
                          else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                          {
                            hdroutineQuestionAns1.Value = "0";
                            aRoutineNo.Attributes.Remove("class");
                            aRoutineNo.Attributes.Add("class", "btnChageColor");
                          }
                        }
                      else if (count == 1  && result.AISequence == 2)
                        {
                          if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                          {
                            hdReoccurrenceQuestionAns1.Value = "1";
                            aReoccurrenceYes.Attributes.Remove("class");
                            aReoccurrenceYes.Attributes.Add("class", "btnChageColor");
                          }
                          else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                          {
                            hdReoccurrenceQuestionAns1.Value = "0";
                            aReoccurrenceNo.Attributes.Remove("class");
                            aReoccurrenceNo.Attributes.Add("class", "btnChageColor");
                          }
                            //else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                            //{
                            //    aReoccurrenceNo.Attributes.Remove("class");
                            //    aReoccurrenceNo.Attributes.Add("class", "btnChageColor");
                            //}
                        }
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }
    } 
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.GeneralInformation).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }


                if (!string.IsNullOrEmpty(hdroutineQuestionAns1.Value))
                {
                  objList.Add(
                       new AIQuestionAnswer
                       {
                         AIQuestionID = Convert.ToInt32(hdroutineQuestionID1.Value),
                         AIQuestionType = QuestionType.BooleanType,
                         AIQuestionAnswerText = Convert.ToString(hdroutineQuestionAns1.Value) == "" ? ConfirmationYesNo.NotAnswered : Convert.ToString(hdroutineQuestionAns1.Value),
                         AIFormID = aiFormID,
                         AIPageName = PageName.GeneralInformation,
                         AISequence = 1
                       });
                }

                if (!string.IsNullOrEmpty(hdReoccurrenceQuestionID1.Value))
                {
                  objList.Add(
                     new AIQuestionAnswer
                     {
                       AIQuestionID = Convert.ToInt32(hdReoccurrenceQuestionID1.Value),
                       AIQuestionType = QuestionType.BooleanType,
                       AIQuestionAnswerText = Convert.ToString(hdReoccurrenceQuestionAns1.Value) == "" ? ConfirmationYesNo.NotAnswered : Convert.ToString(hdReoccurrenceQuestionAns1.Value),
                       AIFormID = aiFormID,
                       AIPageName = PageName.GeneralInformation,
                       AISequence = 2
                     });
                }
                Session["AIReportQuestions"] = objList;
                Response.Redirect("AIGetGeneralInformation.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnSaveAndExit_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitmyReport_Click(object sender, EventArgs e)
    {

    }
}