﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIIsLogistic : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
        }          
    }           
    protected void btnNext_Click(object sender, EventArgs e)
    {               
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.IsLogistic).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdIsLogisticQuestion1.Value)))
                { 
                
                objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdIsLogisticQuestion1.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = Convert.ToString(Request.Form["Islicensee"]),
                        AIFormID = aiFormID,
                        AIPageName = PageName.IsLogistic,
                        AISequence = 1
                    });
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdIsLogisticQuestion2.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdIsLogisticQuestion2.Value),
                            AIQuestionType = QuestionType.TextType,
                            AIQuestionAnswerText = (rdcheckOfinside.Checked == true ? Convert.ToString(rdcheckOfinside.Value) : Convert.ToString(rdCheckoffFleet.Value)),
                            AIFormID = aiFormID,
                            AIPageName = PageName.IsLogistic,
                            AISequence = 2
                        });
                }

                Session["AIReportQuestions"] = objList;
                
                Response.Redirect("AIBeforeGetStarted.aspx", false);                
            }
        }
        catch { }
        finally { }  
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIIsReatilLogistic.aspx", false);
    }
}