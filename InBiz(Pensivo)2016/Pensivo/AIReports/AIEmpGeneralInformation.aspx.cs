﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
 
public partial class AIReports_AIEmpGeneralInformation : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    int iEmpHeaderId = 0;
    string sEmpHdrId = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
             
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                        if (iEmpHeaderId == 0)
                        {
                          iEmpHeaderId = BusinessUtility.GetInt(result.EmpExtID);
                        }
                        sEmpHdrId = result.EmpExtID;
                    }
                }
               
            }
            getFormData();
        }
    }

    private void PopulateEmployeeDetails(string sEmpHeaderID)
    {
      EmphdrInfocl oEmphdrInfocl = new EmphdrInfocl(sEmpHeaderID);
      if (!string.IsNullOrEmpty(Convert.ToString(oEmphdrInfocl.EmpDOB)) && Convert.ToDateTime(oEmphdrInfocl.EmpDOB).ToString("yyyy-MM-dd") != "1900-01-01")
      {
        txtDOB.Value = Convert.ToDateTime(oEmphdrInfocl.EmpDOB).ToString("yyyy-MM-dd");
      }
      txtEmployeeNo.Value = sEmpHeaderID;
    }

    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            var resultItem = objList.Where(x => x.AIPageName == PageName.GetEmpGeneralInformation).OrderBy(x => x.AIQuestionID).ToList();
            if (resultItem != null && resultItem.Count > 0)
            {
              int count = 0;
              foreach (var result in resultItem)
              {
                if (count == 0)
                {
                  txtEmployeeNo.Value = Convert.ToString(result.AIQuestionAnswerText);
                }
                if (count == 1)
                {
                  txtClassification.Value = Convert.ToString(result.AIQuestionAnswerText);
                }
                if (count == 2)
                {
                  txtjobtitle.Value = Convert.ToString(result.AIQuestionAnswerText);
                }
                if (count == 3)
                {
                  txtDOB.Value = Convert.ToDateTime(result.AIQuestionAnswerText).ToString("yyyy-MM-dd");
                }
                count++;
              }
            }
            else
            {
              this.PopulateEmployeeDetails(sEmpHdrId);
            }
        }
        catch { }
        finally { }
    } 
     
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIGetGeneralInformation.aspx", false);
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.GetEmpGeneralInformation).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }

               if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID1.Value)))
               {
                 objList.Add(
                      new AIQuestionAnswer
                      {
                        AIQuestionID = Convert.ToInt32(hdAIQuestionID1.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = Convert.ToString(txtEmployeeNo.Value),
                        AIFormID = aiFormID,
                        AIPageName = PageName.GetEmpGeneralInformation
                      });

               }

               if (!string.IsNullOrEmpty(hdAIQuestionID2.Value))
               {
                 objList.Add(
                   new AIQuestionAnswer
                   {

                     AIQuestionID = Convert.ToInt32(hdAIQuestionID2.Value),
                     AIQuestionType = QuestionType.TextType,
                     AIQuestionAnswerText = Convert.ToString(txtClassification.Value),
                     AIFormID = aiFormID,
                     AIPageName = PageName.GetEmpGeneralInformation
                   });
               }

               if (!string.IsNullOrEmpty(hdAIQuestionID3.Value))
               {
                 objList.Add(
                   new AIQuestionAnswer
                   {

                     AIQuestionID = Convert.ToInt32(hdAIQuestionID3.Value),
                     AIQuestionType = QuestionType.TextType,
                     AIQuestionAnswerText = Convert.ToString(txtjobtitle.Value),
                     AIFormID = aiFormID,
                     AIPageName = PageName.GetEmpGeneralInformation
                   });
               }

               if (!string.IsNullOrEmpty(hdAIQuestionID4.Value))
               {
                 objList.Add(
                   new AIQuestionAnswer
                   {
                     AIQuestionID = Convert.ToInt32(hdAIQuestionID4.Value),
                     AIQuestionType = QuestionType.TextType,
                     AIQuestionAnswerText = Convert.ToString(txtDOB.Value),
                     AIFormID = aiFormID,
                     AIPageName = PageName.GetEmpGeneralInformation
                   });

               }

                Session["AIReportQuestions"] = objList;
                Response.Redirect("AiDetails.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }

    }
}