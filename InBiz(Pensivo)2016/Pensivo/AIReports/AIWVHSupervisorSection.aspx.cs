﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIWVHSupervisorSection : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
                getFormData();
            }
        }
    }
    protected void btnSaveAndExit_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitmyReport_Click(object sender, EventArgs e)
    {

    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        //List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        //objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            Response.Redirect("AIWVHManagerSection.aspx", false);

            //var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
            //if (IsLicensee != null)
            //{
            //    Response.Redirect("AIWVHManagerSection.aspx", false);
            //    return;
            //}
            //else
            //{
            //    Response.Redirect("AIWVHManagerSection.aspx", false);
            //    return;
            //}

        }
        catch { }
        finally { }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHSupervisorSection).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }



                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHSupervisorQuestionID1.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdWVHSupervisorQuestionID1.Value),
                             AIQuestionType = QuestionType.BooleanType,
                             AIQuestionAnswerText = Convert.ToString(Chkappropriatedebriefing.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                             AIFormID = aiFormID,
                             AIPageName = PageName.WVHSupervisorSection,
                             AISequence = 1
                         }); 
                }
                if (Chkappropriatedebriefing.Checked)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(hdWVHSupervisorQuestionID2.Value)))
                    {
                        objList.Add(
                      new AIQuestionAnswer
                      {
                          AIQuestionID = Convert.ToInt32(hdWVHSupervisorQuestionID2.Value),
                          AIQuestionType = QuestionType.TextType,
                          AIQuestionAnswerText = Convert.ToString(txtdebriefingperson.Text),
                          AIFormID = aiFormID,
                          AIPageName = PageName.WVHSupervisorSection,
                          AISequence = 2
                      });

                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(hdWVHSupervisorQuestionID3.Value)))
                    {
                        objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdWVHSupervisorQuestionID3.Value),
                            AIQuestionType = QuestionType.TextType,
                            AIQuestionAnswerText = Convert.ToString(txtdebriefingdate.Text),
                            AIFormID = aiFormID,
                            AIPageName = PageName.WVHSupervisorSection,
                            AISequence = 3
                        });
                    }
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHSupervisorQuestionID4.Value)))
                {
                    objList.Add(
              new AIQuestionAnswer
              {
                  AIQuestionID = Convert.ToInt32(hdWVHSupervisorQuestionID4.Value),
                  AIQuestionType = QuestionType.BooleanType,
                  AIQuestionAnswerText = Convert.ToString(Chkemployeenotified.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
                  AIFormID = aiFormID,
                  AIPageName = PageName.WVHSupervisorSection,
                  AISequence = 4
              });
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHSupervisorQuestionID5.Value)))
                {
                    objList.Add(
              new AIQuestionAnswer
              {
                  AIQuestionID = Convert.ToInt32(hdWVHSupervisorQuestionID5.Value),
                  AIQuestionType = QuestionType.BooleanType,
                  AIQuestionAnswerText = Convert.ToString(Chkformeremployee.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
                  AIFormID = aiFormID,
                  AIPageName = PageName.WVHSupervisorSection,
                  AISequence = 5
              });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHSupervisorQuestionID6.Value)))
                {
                    objList.Add(
                              new AIQuestionAnswer
                               {
                                      AIQuestionID = Convert.ToInt32(hdWVHSupervisorQuestionID6.Value),
                                      AIQuestionType = QuestionType.BooleanType,
                                      AIQuestionAnswerText = Convert.ToString(Chkspousepartner.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
        AIFormID = aiFormID,
        AIPageName = PageName.WVHSupervisorSection,
        AISequence = 6
    });
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHSupervisorQuestionID7.Value)))
                {
                    objList.Add(
    new AIQuestionAnswer
    {
        AIQuestionID = Convert.ToInt32(hdWVHSupervisorQuestionID7.Value),
        AIQuestionType = QuestionType.BooleanType,
        AIQuestionAnswerText = Convert.ToString(family.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.No),
        AIFormID = aiFormID,
        AIPageName = PageName.WVHSupervisorSection,
        AISequence = 7
    });
                }
                Session["AIReportQuestions"] = objList;
                //managePreviousNext("");

                RedirectionLink();
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    private void RedirectionLink()
    {
        //List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        //objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            Response.Redirect("AIWVHFollowUp.aspx", false);
            //var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
            //if (IsLicensee != null)
            //{ 
            //    Response.Redirect("AIWVHFollowUp.aspx", false);
            //    return;
            //} 
            //else
            //{
            //    // Case 1:  Retail:Yes, WVH:Yes 
            //    var IsRetailAndLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "R");
            //    var IsWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
            //    if ((IsRetailAndLogistic != null) && IsWVH != null)
            //    {
            //        Response.Redirect("AIWVHFollowUp.aspx", false);
            //        return;
            //    }
            //    else
            //    {
            //        Response.Redirect("AIWVHFollowUp.aspx", false);
            //        return;
            //    }
            //}
        }
        catch
        { }
        finally { }

    }

    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHSupervisorSection).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 1;
                    foreach (var result in resultItem)
                    {
                        if (count == 1 && Convert.ToString(result.AIQuestionAnswerText) == "1" && result.AISequence == 1)
                        {
                            Chkappropriatedebriefing.Checked = true;
                            dvdBrief.Attributes.Remove("Class");
                            dvdBrief.Attributes.Add("Class", "divShow");
                        }
                        else if (count == 2)
                        {                            
                            if (result.AISequence == 2)
                            {
                                txtdebriefingperson.Text = Convert.ToString(result.AIQuestionAnswerText);
                            }
                            else
                            {
                                Chkemployeenotified.Checked = true;
                            } 
                        }
                        else if (count == 3)
                        {
                            if (result.AISequence == 3)
                            {
                                txtdebriefingdate.Text = Convert.ToDateTime(result.AIQuestionAnswerText).ToString("yyyy-MM-dd");
                            }
                            else
                            {
                                Chkformeremployee.Checked = true;
                            } 
                        }
                        else if (count == 4)
                        {
                            if (result.AISequence == 4)
                            {
                                Chkemployeenotified.Checked = true;
                            }
                            else if (result.AISequence == 5)
                            {
                                Chkspousepartner.Checked = true;
                            } 
                        }
                        else if (count == 5)
                        {
                            if (result.AISequence == 5)
                            {
                                Chkformeremployee.Checked = true;
                            }
                            else if (result.AISequence == 6)
                            {
                                family.Checked = true;
                            }                            
                        }
                        else if (count == 6)
                        {
                            Chkspousepartner.Checked = true;
                        }
                        else if (count == 7)
                        {
                            family.Checked = true;
                        } 
                      
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }

    }

}