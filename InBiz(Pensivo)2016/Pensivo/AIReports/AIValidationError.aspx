﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIValidationError.aspx.cs" Inherits="AIReports_AIValidationError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../_styles/ai-reports.css" />


 
    <section id="main-content" class="pg-ca-preventions">
<span class="mask"></span>
<div class="wrapper">

	<div class="plms-alert secondary-clr">
		<p>Accident / Incident report for <strong class="employee-name">Joshua Johnston</strong> 
		(Site #<span class="site-num">1308</span>, Employee ID <span class="employee-id">06690</span>)</p>
	</div><!--End .plms-alert-->
	
	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		Symbol signifies mandatory field which must be 
		completed prior to submitting the AI form.</p>
	</div><!--End .plms-alert-->
	
	<div class="layout-sidebar-right">

		<div class="boxed-content">
			
			<div class="boxed-content-body">
				
			<div class="plms-alert invalid">
			<p>Hang on now &mdash; Looks like you forgot to provide us with some 
			essential answers / information! Please look over the highlighted 
			item(s) below and answer each question.</p>
			</div>	
				
			<h2>Validation Errors</h2>
			
			<p>This is not a page and is created solely for example purposes.</p>
			
			<div class="plms-fieldset-wrapper has-error">
				<i class="req-icon" title="Mandatory Field">*</i>
				<div class="column span-8">
					<p class="bold">Routine Work?</p>
				</div><!--End .column-->
				
				<div class="column span-4">
					<div class="btngrp align-r">
					<a href="#nogo" class="btn" title="Yes">Yes</a>
					<a href="#nogo" class="btn" title="No">No</a>
					</div><!--End .btngrp-->
				</div><!--End .column-->
			</div><!--End .plms-fieldset-wrapper-->
			
			<div class="plms-fieldset pull-up-10 push-down">
				<label class="plms-label is-hidden" for="required-text-input-field">Required Text Input Field</label>
				
				<div class="plms-tooltip-parent">
					<i class="req-icon" title="Mandatory Field">*</i>
					<input type="text" placeholder="Required Text Input Field" class="plms-input has-error skin2" id="required-text-input-field" name="required-text-input-field" />
					
					<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
						<div class="plms-tooltip-body">
							<p>Required Text Input Field</p>
						</div><!--End .plms-tooltip-body-->
					</div><!--End .plms-tooltip-->
				</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			
			<div class="label-checkbox-group clearfix has-error">
			<label class="checkbox-input-label" for="battery-charge"><input type="checkbox" id="battery-charge" name="battery-charge" />Battery Charge</label>
			<label class="checkbox-input-label" for="dock-safety-wheel-chocking"><input type="checkbox" id="dock-safety-wheel-chocking" name="dock-safety-wheel-chocking" />Dock Safety / Wheel Chocking</label>
			<label class="checkbox-input-label" for="eye-wash"><input type="checkbox" id="eye-wash" name="eye-wash" />Eye Wash</label>
			<label class="checkbox-input-label" for="fall-prevention"><input type="checkbox" id="fall-prevention" name="fall-prevention" />Fall Prevention</label>
			<label class="checkbox-input-label" for="housekeeping"><input type="checkbox" id="housekeeping" name="housekeeping" />Housekeeping</label>
			<label class="checkbox-input-label" for="knife-safety"><input type="checkbox" id="knife-safety" name="knife-safety" />Knife Safety</label>
			<label class="checkbox-input-label" for="ladder-safety-rolling-ladder"><input type="checkbox" id="ladder-safety-rolling-ladder" name="ladder-safety-rolling-ladder" />Ladder Safety / Rolling Ladder</label>
			<label class="checkbox-input-label" for="pedestrian-safety"><input type="checkbox" id="pedestrian-safety" name="pedestrian-safety" />Pedestrian Safety</label>
			<label class="checkbox-input-label" for="power-equipment"><input type="checkbox" id="power-equipment" name="power-equipment" />Power Equipment</label>
			<label class="checkbox-input-label" for="ppe"><input type="checkbox" id="ppe" name="ppe" />PPE</label>
			<label class="checkbox-input-label" for="safe-lifting-techniques-material-handling"><input type="checkbox" id="safe-lifting-techniques-material-handling" name="safe-lifting-techniques-material-handling" />Safe Lifting Techniques / Material Handling</label>
			<label class="checkbox-input-label" for="spotter-signaller"><input type="checkbox" id="spotter-signaller" name="spotter-signaller" />Spotter / Signaller</label>
			<label class="checkbox-input-label" for="whmis"><input type="checkbox" id="whmis" name="whmis" />WHMIS</label>
			<label class="checkbox-input-label" for="other"><input type="checkbox" id="other" name="other" />Other</label>
			</div><!--End .label-checkbox-group-->
								
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">
		         <a href="AIPeopleInvolved.aspx" title="Previous" class="btn large"  runat="server">Previous</a>
			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span>Hide Menu</span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
			<ul>
			<li><a href="#nogo" title="">General Information</a></li>
			<li><a href="#nogo" title="">Details</a></li>
			<li><a href="#nogo" title="">Description of Accident / Incident</a></li>
			<li><a href="#nogo" title="">Contributing Factors / Conditions</a></li>
			<li><a href="#nogo" title="">Corrective Actions &amp; Prevention</a></li>
			<li><a href="#nogo" title="">Witnesses / Co-Workers</a></li>
			<li><a href="#nogo" title="">Worker's Comments</a></li>
			<li class="is-active"><a href="#nogo" title="">People Involved</a></li>
			</ul>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
	<a href="#nogo" class="btn" title="Save & Exit to Continue Later">Save &amp; Exit to Continue Later</a>
	<a href="#nogo" class="btn" title="Submit My Report">Submit My Report</a>
	</div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
</asp:Content>

