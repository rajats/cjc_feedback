﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AILostTimeExp.aspx.cs" Inherits="AIReports_AILostTimeExp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
     <script>
         function showMore()
         {
             if ( $( "#dvSecond" ).hasClass( "divHide" ) )
             {
                 $( "#dvSecond" ).removeClass( 'divHide' );
                 $( "#dvSecond" ).addClass( 'divShow' );
                 $( "#<%= ancShowMore.ClientID%>" ).hide();
               }
           }
           </script>
<section id="main-content">
<span class="mask"></span>
<div class="wrapper width-med">

	<div class="plms-alert secondary-clr">
	<p>
                    <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %> #<span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>    </div><!--End .plms-alert-->

	<div class="boxed-content">
		
		<h2><%=Resources.Resource.lblLostTime %></h2>
		
		<div class="boxed-content-body">
		
		<h3 class="h4"><%=Resources.Resource.lblExamples %></h3>
			
         <div  id="dvFirst">
                  <%=Resources.Resource.lblLostTimeExpText %>
            </div>
          
              <div id="dvSecond" class="divHide">
                    <%=Resources.Resource.lblLostTimeExpText1 %>
                  </div>
                  <br />


            <a id="ancShowMore" class="btn fluid large clearfix" title="Click to read other examples" onclick="showMore();"  runat="server"><%= Resources.Resource.lblLoadMore %></a>
           

		</div><!--End .boxed-content-body-->
		
		<nav class="pagination-nav">
		<asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevious  %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large" OnClick="btnPrevious_Click"  />
                    <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, lblNext %>" class="btn large align-r" OnClick="btnNext_Click"   />
		</nav><!--End .pagination-nav-->
	
	</div><!--End .boxed-content-->
	
	<footer>
	 <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn" OnClick="btnSubmitwithoutSaving_Click" />
	</footer>
</div><!--End .wrapper-->
</section><!--End #main-content-->
</asp:Content>

