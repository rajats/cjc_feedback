﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIWeekShiftSchedule1.aspx.cs" Inherits="AIReports_AIWeekShiftSchedule1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <link rel="stylesheet" href="../_styles/ai-reports.css" />
    <section id="main-content">
<span class="mask"></span>
<div class="wrapper">

	<div class="plms-alert secondary-clr">
		<p>Accident / Incident report for <strong class="employee-name">Joshua Johnston</strong> 
		(Site #<span class="site-num">1308</span>, Employee ID <span class="employee-id">06690</span>)</p>
	</div><!--End .plms-alert-->
	
	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		Symbol signifies mandatory field which must be 
		completed prior to submitting the AI form.</p>
	</div><!--End .plms-alert-->

	<div class="layout-sidebar-right">

		<div class="boxed-content">
			
			<div class="boxed-content-body">
				
			<h2>Details</h2>
			
			<p>Worker's scheduled shifts week <em>following</em> Accident / Incident.</p>
			
			<p class="small-text pull-up-10"><strong>hh</strong> represents the number of complete hours that 
			have passed since midnight (00-24), <strong>mm</strong> represents the 
			number of complete minutes that have passed since the start of 
			the hour (00-59).</p>
			
			<div class="plms-table scheduled-shifts">
			<table>
			<caption>Worker's scheduled shifts following the week of accident / incident.</caption>
			<tr>
				<th>Monday</th>
				<td><label for="mon-start-time">Start Time</label><input name="mon-start-time" id="mon-start-time" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text" /></td>
				<td><label for="mon-end-time">End Time</label><input class="plms-input skin2" name="mon-end-time" id="mon-end-time" placeholder="End Time (hh:mm)" type="text" /></td>
			</tr>
			<tr>
				<th>Tuesday</th>
				<td><label for="tue-start-time">Start Time</label><input name="tue-start-time" id="tue-start-time" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text" /></td>
				<td><label for="tue-end-time">End Time</label><input class="plms-input skin2" name="tue-end-time" id="tue-end-time" placeholder="End Time (hh:mm)" type="text" /></td>
			</tr>
			<tr>
				<th>Wednesday</th>
				<td><label for="wed-start-time">Start Time</label><input name="wed-start-time" id="wed-start-time" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text" /></td>
				<td><label for="wed-end-time">End Time</label><input class="plms-input skin2" name="wed-end-time" id="wed-end-time" placeholder="End Time (hh:mm)" type="text" /></td>
			</tr>
			<tr>
				<th>Thursday</th>
				<td><label for="thur-start-time">Start Time</label><input name="thur-start-time" id="thur-start-time" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text" /></td>
				<td><label for="thur-end-time">End Time</label><input class="plms-input skin2" name="thur-end-time" id="thur-end-time" placeholder="End Time (hh:mm)" type="text" /></td>
			</tr>
			<tr>
				<th>Friday</th>
				<td><label for="fri-start-time">Start Time</label><input name="fri-start-time" id="fri-start-time" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text" /></td>
				<td><label for="fri-end-time">End Time</label><input class="plms-input skin2" name="fri-end-time" id="fri-end-time" placeholder="End Time (hh:mm)" type="text" /></td>
			</tr>
			<tr>
				<th>Saturday</th>
				<td><label for="sat-start-time">Start Time</label><input name="sat-start-time" id="sat-start-time" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text" /></td>
				<td><label for="sat-end-time">End Time</label><input class="plms-input skin2" name="sat-end-time" id="sat-end-time" placeholder="End Time (hh:mm)" type="text" /></td>
			</tr>
			<tr>
				<th>Sunday</th>
				<td><label for="sun-start-time">Start Time</label><input name="sun-start-time" id="sun-start-time" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text" /></td>
				<td><label for="sun-end-time">End Time</label><input class="plms-input skin2" name="sun-end-time" id="sun-end-time" placeholder="End Time (hh:mm)" type="text" /></td>
			</tr>
			</table>
			</div>
					
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">
			<a href="AIWeekShiftSchedule.aspx" title="Previous" class="btn large"  runat="server">Previous</a>
			<a href="AILicenseeDetails.aspx" title="Next" class="btn large align-r"  runat="server">Next</a>
			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span>Hide Menu</span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
			<ul>
			<li><a href="#nogo" title="">General Information</a></li>
			<li class="is-active"><a href="#nogo" title="">Details</a></li>
			<li><a href="#nogo" title="">Description of Accident / Incident</a></li>
			<li><a href="#nogo" title="">Contributing Factors / Conditions</a></li>
			<li><a href="#nogo" title="">Corrective Actions &amp; Prevention</a></li>
			<li><a href="#nogo" title="">Witnesses / Co-Workers</a></li>
			<li><a href="#nogo" title="">Worker's Comments</a></li>
			<li><a href="#nogo" title="">People Involved</a></li>
			</ul>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
	<a href="#nogo" class="btn" title="Save & Exit to Continue Later">Save &amp; Exit to Continue Later</a>
	<a href="#nogo" class="btn" title="Submit My Report">Submit My Report</a>
	</div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->



</asp:Content>

