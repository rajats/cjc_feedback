﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIEmailValidation.aspx.cs" Inherits="AIReports_AIEmailValidation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
  <link href="../_css/global.css" rel="stylesheet" />
  <script>
    $(document).ready(function () {
      $("#<%=btnValidateEmail.ClientID %>").attr("disabled", "disabled");
      $("#<%=btnValidateEmail.ClientID %>").removeClass("btn").removeClass("large").removeClass("align-r");
      $("#<%=btnValidateEmail.ClientID %>").addClass("btngrayout").addClass("large").addClass("align-r");
    });

    function ValidateEmail(email)
    {
      var btnValidateEmail = document.getElementById('<%=btnValidateEmail.ClientID %>');
      var sExpression = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      var bValid = sExpression.test(email.value);
      if (!bValid) {
        $("#<%=btnValidateEmail.ClientID %>").attr("disabled", "disabled");
        $("#<%=btnValidateEmail.ClientID %>").removeClass("btn").removeClass("large").removeClass("align-r");
        $("#<%=btnValidateEmail.ClientID %>").addClass("btngrayout").addClass("large").addClass("align-r");
      }
      else {
        $("#<%=btnValidateEmail.ClientID %>").removeAttr("disabled");
        $("#<%=btnValidateEmail.ClientID %>").removeClass("btngrayout").removeClass("large").removeClass("align-r");
        $("#<%=btnValidateEmail.ClientID %>").addClass("btn").addClass("large").addClass("align-r");
      }
    }
  </script>

    <section id="main-content">
        <span class="mask"></span>
        <div class="wrapper width-med">
            <asp:Panel ID="pnlcontent" runat="server" DefaultButton="btnValidateEmail">
                <div class="boxed-content">
                     <h2>  <%= Resources.Resource.lblYourEmail %></h2>
                     <p>

                          <%= Resources.Resource.lblYourEmailText %>
                       
                     </p>
                    <div class="boxed-content-body">

                        <div class="plms-fieldset email-address">
                            <label class="plms-label is-hidden" for="txtEmail"><%= Resources.Resource.lblYourEmail %></label>
                            <div class="plms-tooltip-parent">
                                <%--<input type="text" placeholder="Your Email Address (Ex.: john.smith@thebeerstore.ca)" class="plms-input skin2" id="email-address" name="email-address" />--%>
                                <asp:TextBox ID="txtEmail" runat="server" placeholder="Your Email Address (Ex.: john.smith@thebeerstore.ca)" class="plms-input skin2" onkeyup="return ValidateEmail(this);" onBlur="return ValidateEmail(this);"></asp:TextBox>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p>  <%= Resources.Resource.EmailAdd %> </p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                            <asp:RequiredFieldValidator ID="rfMail" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="txtEmail" Text="<%$ Resources:Resource, reqMail %>" />
                            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" class="formels-feedback invalid" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="vg1" ErrorMessage="<%$ Resources:Resource, lblAIEmailNotvalid %>" SetFocusOnError="true"></asp:RegularExpressionValidator>
                            <%--<p class="formels-feedback invalid">This email address is not valid.</p>--%>
                        </div>
                        <!--End .plms-fieldset-->
                    </div>
                    <!--End .boxed-content-body-->

                    <nav class="pagination-nav">           
                        <asp:Button ID="btnValidateEmail" runat="server" class="btn large align-r" Text="<%$ Resources:Resource, lblAINext %>" OnClick="btnValidateEmail_Click" ValidationGroup="vg1" />
                    </nav>
                    <!--End .pagination-nav-->

                </div>
                <!--End .boxed-content-->
            </asp:Panel>
            <footer>
                <div class="btngrp">
                     <asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click"   /> 
                    <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click"  />
                    <!--
                    <a href="#nogo" class="btn" title="Save & Exit To Continue Later">Save &amp; Exit To Continue Later</a>
                    <a href="#nogo" class="btn" title="Submit My Report">Submit My Report</a>-->
                </div>
                <!--End .btngrp-->
            </footer>
        </div>
        <!--End .wrapper-->
    </section>
</asp:Content>

