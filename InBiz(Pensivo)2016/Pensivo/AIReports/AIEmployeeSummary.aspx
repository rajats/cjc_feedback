﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIEmployeeSummary.aspx.cs" Inherits="AIReports_AIEmployeeSummary" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
<section id="main-content">
<span class="mask"></span>
<div class="wrapper">

	<div class="plms-alert secondary-clr">
	<p>Accident / Incident report for <strong class="employee-name">Joshua Johnston</strong> (Site #<span class="site-num">1308</span>, Employee ID <span class="employee-id">06690</span>)</p>
	</div><!--End .plms-alert-->

	<div class="boxed-content">
		
		<h2>Summary</h2>
		
		<div class="boxed-content-body">
			
		<h3 class="h4">Here's what you answered</h3>

		<div class="plms-table">
		<table>
		<tr>
		<th>Affected employee</th>
		<td>Joshua Johnston (Site #1308, Employee ID 06690)</td>
		</tr>
		
		<tr>
		<th>Site # where accident / incident took place</th>
		<td>1308</td>
		</tr>
		
		<tr>
		<th>Where did this happen?</th>
		<td>Logistics</td>
		</tr>
		
		<tr>
		<th>Did it happen at a licensee?</th>
		<td>Yes</td>
		</tr>
		
		<tr>
		<th>Type of accident</th>
		<td>Incident</td>
		</tr>
		</table>
		</div><!--End .plms-table-->

		<div class="boxed-content">
		<div class="column span-9">
			<div class="alert secondary-clr">
			<p>Does what happened to Joshua match the "property damage" description 
			and closely relate to one of the examples seen on the 
			previous screens?</p>
			</div><!--End .alert-->
		</div><!--End .column-->
		
		<div class="column span-3">
			<div class="btngrp align-r">
			<a href="#nogo" class="btn" title="Yes">Yes</a>
			<a href="#nogo" class="btn" title="No">No</a>
			</div><!--End .btngrp-->
		</div><!--End .column-->
		</div><!--End .boxed-content-->
				
		</div><!--End .boxed-content-body-->
		
		<nav class="pagination-nav">
		<a href="AISummary.aspx.aspx" title="Previous" class="btn large"  runat="server">Previous</a>
		<a href="AIGeneralInformation.aspx" title="Next" class="btn large align-r">Next</a>
		</nav><!--End .pagination-nav-->
	
	</div><!--End .boxed-content-->
	
	<footer>
	<a href="#nogo" class="btn" title="Exit Without Saving">Exit Without Saving</a>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
</asp:Content>

