﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIReturntowork1 : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();
        }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIReturntowork.aspx", false);
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.ReturnToWork1).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }

                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }



                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID1.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdAIQuestionID1.Value),
                            AIQuestionType = QuestionType.TextType,
                            AIQuestionAnswerText = Convert.ToString(hdAIQuestionAnsID1.Value),
                            AIFormID = aiFormID,
                            AIPageName = PageName.ReturnToWork1,
                            AISequence = 1

                        });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionAnsID1.Value)) && BusinessUtility.GetString(Convert.ToString(hdAIQuestionAnsID1.Value)) == "0" && !string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID2.Value)))
                {

                    objList.Add(
                                       new AIQuestionAnswer
                                       {
                                           AIQuestionID = Convert.ToInt32(hdAIQuestionID2.Value),
                                           AIQuestionType = QuestionType.TextType,
                                           AIQuestionAnswerText = Convert.ToString(hdAIQuestionAnsID2.Value),
                                           AIFormID = aiFormID,
                                           AIPageName = PageName.ReturnToWork1,
                                           AISequence = 2
                                       });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID3.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdAIQuestionID3.Value),
                             AIQuestionType = QuestionType.TextType,
                             AIQuestionAnswerText = Convert.ToString(hdAIQuestionAnsID3.Value),
                             AIFormID = aiFormID,
                             AIPageName = PageName.ReturnToWork1,
                             AISequence = 3
                         });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID3.Value)) && BusinessUtility.GetString(Convert.ToString(hdAIQuestionAnsID3.Value)) == "1" && !string.IsNullOrEmpty(Convert.ToString(hdAIQuestionID4.Value)))
                {
                    objList.Add(
                                   new AIQuestionAnswer
                                   {
                                       AIQuestionID = Convert.ToInt32(hdAIQuestionID4.Value),
                                       AIQuestionType = QuestionType.TextType,
                                       AIQuestionAnswerText = Convert.ToString(txtscheduledcompletiondate.Value),
                                       AIFormID = aiFormID,
                                       AIPageName = PageName.ReturnToWork1,
                                       AISequence = 4
                                   });
                }

            }
            Session["AIReportQuestions"] = objList;

            managePreviousNext("");

            RedirectionLink();
        }
        catch (Exception ex) { throw ex; }
        finally { }

        managePreviousNext("");
        RedirectionLink();
        //Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
    }

    private void managePreviousNext(string NextPageUrl)
    {
        List<ManagePagePreviousNext> objPage = new List<ManagePagePreviousNext>();
        try
        {
            objPage = (List<ManagePagePreviousNext>)Session["AIPagesList"];
            if (objPage != null)
            {
                var resultItem = objPage.Where(x => x.pageName == PageName.ReturnToWork1).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objPage.Remove(removeItem);
                    }
                }
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIReturntowork1.aspx", NextPageUrl = NextPageUrl, pageName = PageName.ReturnToWork1, PreviousPageUrl = "" });
            }
            else
            {
                objPage = new List<ManagePagePreviousNext>();
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIReturntowork1.aspx", NextPageUrl = NextPageUrl, pageName = PageName.ReturnToWork1, PreviousPageUrl = "" });
            }
            Session["AIPagesList"] = objPage;

        }
        catch { }
        finally { }
    }
    private void RedirectionLink()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {

                var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.Location && x.IsLicensee == "1");
                if (ISLicensee != null)
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIDetails7.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }


                }
                else
                {
                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIAccidentIncidentDetail.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("HealthCareDetails.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIAccidentIncidentDetail.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIAccidentIncidentDetail.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIAccidentIncidentDetail.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIAccidentIncidentDetail.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIDescriptionInjuryIllness.aspx", false);
                        return;
                    }

                }
            }

        }
        catch { }
        finally { }
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }

    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                var resultItem = objList.Where(x => x.AIPageName == PageName.ReturnToWork).OrderBy(x => x.AIQuestionID).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    int count = 0;
                    foreach (var result in resultItem)
                    {
                        if (count == 0 && result.AISequence == 1)
                        {

                            if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                            {
                                hdAIQuestionAnsID1.Value = "1";
                                ancAiQuestionD1Yes.Attributes.Remove("Class");
                                ancAiQuestionD1Yes.Attributes.Add("Class", "btnChageColor");
                            }
                            else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                            {
                                hdAIQuestionAnsID1.Value = "1";
                                ancAiQuestionD1No.Attributes.Remove("Class");
                                ancAiQuestionD1No.Attributes.Add("Class", "btnChageColor");
                                divQuestionID2.Attributes.Remove("Class");
                                divQuestionID2.Attributes.Add("Class", "divShow");
                            }
                        }
                        else if (count == 1)
                        {
                            if (result.AISequence == 2)
                            {
                                if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                                {
                                    hdAIQuestionAnsID2.Value = "1";
                                    ancAiQuestionD2Yes.Attributes.Remove("Class");
                                    ancAiQuestionD2Yes.Attributes.Add("Class", "btnChageColor");
                                }
                                else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                                {
                                    hdAIQuestionAnsID2.Value = "1";
                                    ancAiQuestionD2No.Attributes.Remove("Class");
                                    ancAiQuestionD2No.Attributes.Add("Class", "btnChageColor");
                                }
                            }
                            else if (result.AISequence == 3)
                            {
                                if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                                {
                                    hdAIQuestionAnsID3.Value = "1";
                                    ancAiQuestion3Yes.Attributes.Remove("Class");
                                    ancAiQuestion3Yes.Attributes.Add("Class", "btnChageColor");
                                }
                                else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                                {
                                    hdAIQuestionAnsID3.Value = "1";
                                    ancAiQuestion3No.Attributes.Remove("Class");
                                    ancAiQuestion3No.Attributes.Add("Class", "btnChageColor");
                                } 
                            }                           
                        }
                        else if (count == 2 && result.AISequence == 3)
                        {
                            if (Convert.ToString(result.AIQuestionAnswerText) == "1")
                            {
                                hdAIQuestionAnsID3.Value = "1";
                                ancAiQuestion3Yes.Attributes.Remove("Class");
                                ancAiQuestion3Yes.Attributes.Add("Class", "btnChageColor");
                                divQuestionID4.Attributes.Remove("Class");
                                divQuestionID4.Attributes.Add("Class", "divShow");
                                   
                            }
                            else if (Convert.ToString(result.AIQuestionAnswerText) == "0")
                            {
                                hdAIQuestionAnsID3.Value = "1";
                                ancAiQuestion3No.Attributes.Remove("Class");
                                ancAiQuestion3No.Attributes.Add("Class", "btnChageColor");
                            } 
                        } 
                        else if (count == 3 && result.AISequence == 4)
                        {
                         txtscheduledcompletiondate.Value= Convert.ToDateTime(result.AIQuestionAnswerText).ToString("yyyy-MM-dd");    
                        }
                        
                        count++;
                    }
                }
            }
        }
        catch { }
        finally { }
    }

}