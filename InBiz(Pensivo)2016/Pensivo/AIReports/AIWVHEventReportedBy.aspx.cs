﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AIWVHEventReportedBy : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
                getFormData();
            }
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.WVHEventReportedBy).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }




                if (!string.IsNullOrEmpty(Convert.ToString(hdWVHEventReportQuestionID1.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdWVHEventReportQuestionID1.Value),
                             AIQuestionType = QuestionType.TextType,
                             AIQuestionAnswerText = Convert.ToString(txtreportername.Text),
                             AIFormID = aiFormID,
                             AIPageName = PageName.WVHEventReportedBy,
                             AISequence = 1
                         });

                }

                Session["AIReportQuestions"] = objList;

                //managePreviousNext("");

                RedirectionLink();

            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    private void RedirectionLink()
    {
        //// Case 1:  Retail:Yes, WVH:Yes
        //List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        //objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        //var IsRetailAndLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "R");
        //var IsWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
        //if ((IsRetailAndLogistic != null) && IsWVH != null)
        //{
        //    Response.Redirect("AIWVHOffenders.aspx", false);
        //} 
        //// Case : 2 Licensee:Yes, WVH:Yes
        //var IsLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "L");
        //var IsLicensee =  objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence==1);
        //if (IsLogistic != null  &&  IsLicensee != null)
        //{
        //    Response.Redirect("AIWVHOffenders.aspx", false);

        //} 
        Response.Redirect("AIWVHOffenders.aspx", false);
    }
    protected void btnSaveAndExit_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitmyReport_Click(object sender, EventArgs e)
    {

    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            var resultItem = objList.FirstOrDefault(x => x.AIPageName == PageName.WVHEventReportedBy);
            if (resultItem != null)
            {
                txtreportername.Text = Convert.ToString(resultItem.AIQuestionAnswerText);
            }
        }
        catch { }
        finally { }
    }
    protected void btnPrevious_Click(object sender, System.EventArgs e)
    {
        //List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        //objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
        //    if (objList != null)
        //    {
        //        var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
        //        if (IsLicensee != null)
        //        {
        //            var ISPI = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
        //            if (ISPI != null)
        //            {
        //                // AIWitnessesCoWorker.aspx
        //                Response.Redirect("AIWitnessesCoWorker.aspx", false);
        //                return;
        //            }
        //            else
        //            {
        //                Response.Redirect("AILicenseeDetails.aspx", false);
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            Response.Redirect("AiDetails.aspx", false);
        //            return;

        //        }
        // }  
            Response.Redirect("AISummary.aspx", false);
        }
        catch { }
        finally { }
        //// Case 1:  Retail:Yes, WVH:Yes
        //List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        //objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        //var IsRetailAndLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "R");
        //var IsWVH = objList.FirstOrDefault(x => x.AIPageName == PageName.WVH && x.AIQuestionAnswerText == "1");
        //if ((IsRetailAndLogistic != null) && IsWVH != null)
        //{
        //    Response.Redirect("AiDetails.aspx", false);
        //}
        //// Case : 2 Licensee:Yes, WVH:Yes
        //var IsLogistic = objList.FirstOrDefault(x => x.AIPageName == PageName.IsReatilLogistic && x.AIQuestionAnswerText == "L");
        //var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
        //if (IsLogistic != null && IsLicensee != null)
        //{
        //    Response.Redirect("AILicenseeDetails.aspx", false);

        //} 
    }
}