﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIIncidentdetail.aspx.cs" Inherits="AIReports_AIIncidentdetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
    <script  type="text/javascript">
        function GetIncidentDetailQuestions() {
               $.ajax({
                   type: "POST",
                   url: "CommonInterface.aspx/getQuestions",
                   data: "{pageType:'" + "IncidentDetail" + "'}",
                   dataType: "json",
                   contentType: "application/json; charset=utf-8",
                   success: function (data) {
                       var index = 0;
                       $.each(data.d, function (index, value) {
                           if (index == 0) {
                               $("#<%=hdIncidentDetailQuestionID1.ClientID%>").val(value.QuestionID);
                               $("#lblIncidentDetailQuestionText1").html(value.QuestionText);
                       }
                       if (index == 1)
                       {
                           $("#<%=hdIncidentDetailQuestionID2.ClientID%>").val(value.QuestionID);
                           $("#lblIncidentDetailQuestionText2").html(value.QuestionText);
                       }
                       if (index == 2)
                       {
                           $("#<%=hdIncidentDetailQuestionID3.ClientID%>").val(value.QuestionID);
                           $("#lblIncidentDetailQuestionText3").html(value.QuestionText);
                       }
                       if (index == 3)
                       {
                           $("#<%=hdIncidentDetailQuestionID4.ClientID%>").val(value.QuestionID);
                           $("#lblIncidentDetailQuestionText4").html(value.QuestionText);
                       }
                       if (index == 4)
                       {
                           $("#<%=hdIncidentDetailQuestionID5.ClientID%>").val(value.QuestionID);
                           $("#lblIncidentDetailQuestionText5").html(value.QuestionText);
                       }
                
                       index = parseInt(index) + 1;
                   });
               },
               error: function (XMLHttpRequest, textStatus, errorThrown) {
                   //alert( errorThrown );
               }
           });
       }
       $(document).ready(function () {
           GetIncidentDetailQuestions();
       });
    </script> 
 <section id="main-content" class="pg-location">
        <span class="mask"></span>
        <div class="wrapper width-med">
            <div class="plms-alert secondary-clr">
                <p>
                    <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->
            <div class="boxed-content">
                <h2> <%=Resources.Resource.lblIncidentDetail%> </h2>
                <p>  
                </p>
                <div class="boxed-content-body">
                     
                    <div class="fieldset-group location-type" onkeypress="return disableEnterKey(event)">
                        <p>
                             <asp:HiddenField  ID="hdIncidentDetailQuestionID1"  runat="server"/>   
                            <span id="lblIncidentDetailQuestionText1"></span>                          
                        </p>
                        <div class="label-radio-group">
                             <asp:TextBox ID="txtIncidentDetailQuestion1Ans" runat="server" CssClass="filter-key plms-input skin2" >
                                   </asp:TextBox> 
                        </div>
                        <!--End .label-radio-group-->
                    </div>
                    <!--End .fieldset-group-->
                     
                    <div class="fieldset-group location-type" onkeypress="return disableEnterKey(event)">
                        <p>
                             <asp:HiddenField  ID="hdIncidentDetailQuestionID2"  runat="server"/>   
                            <span id="lblIncidentDetailQuestionText2"></span>                          
                        </p>
                        <div class="label-radio-group">
                             <asp:TextBox ID="txtIncidentDetailQuestion2Ans" runat="server" CssClass="filter-key plms-input skin2" >
                                   </asp:TextBox> 
                        </div>
                        <!--End .label-radio-group-->
                    </div>
                    <!--End .fieldset-group-->
                    

                         <div class="fieldset-group location-type" onkeypress="return disableEnterKey(event)">
                        <p>
                             <asp:HiddenField  ID="hdIncidentDetailQuestionID3"  runat="server"/>   
                            <span id="lblIncidentDetailQuestionText3"></span>                          
                        </p>
                        <div class="label-radio-group">
                             <asp:TextBox ID="txtIncidentDetailQuestion3Ans" runat="server" CssClass="filter-key plms-input skin2" >
                                   </asp:TextBox> 
                        </div>
                        <!--End .label-radio-group-->
                    </div>
                    <!--End .fieldset-group-->
                    
                     

                         <div class="fieldset-group location-type" onkeypress="return disableEnterKey(event)">
                        <p>
                             <asp:HiddenField  ID="hdIncidentDetailQuestionID4"  runat="server"/>   
                            <span id="lblIncidentDetailQuestionText4"></span>                          
                        </p>
                        <div class="label-radio-group">
                             <asp:TextBox ID="txtIncidentDetailQuestion4Ans" runat="server" CssClass="filter-key plms-input skin2" >
                                   </asp:TextBox> 
                        </div>
                        <!--End .label-radio-group-->
                    </div>
                    <!--End .fieldset-group-->
                     


                        <div class="fieldset-group location-type" onkeypress="return disableEnterKey(event)">
                        <p>
                             <asp:HiddenField  ID="hdIncidentDetailQuestionID5"  runat="server"/>   
                            <span id="lblIncidentDetailQuestionText5"></span>                          
                        </p>
                        <div class="label-radio-group">
                             <asp:TextBox ID="txtIncidentDetailQuestion5Ans" runat="server" CssClass="filter-key plms-input skin2" >
                                   </asp:TextBox> 
                        </div>
                        <!--End .label-radio-group-->
                    </div>
                    <!--End .fieldset-group-->
                     

                     

                </div>
                <!--End .boxed-content-body-->


                <nav class="pagination-nav">
                        <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"      />
                        <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"   />
                </nav>
                <!--End .pagination-nav-->
            </div>
            <!--End .boxed-content-->
            <footer>
               <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn" OnClick="btnSubmitwithoutSaving_Click"   />
            </footer>
             
        </div>
        <!--End .wrapper-->
    </section>
</asp:Content>

