﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIHealthCare.aspx.cs" Inherits="AIReports_AIHealthCare" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <link rel="stylesheet" href="../_styles/ai-reports.css" />
        <script  type="text/javascript">
            function GethealthCareQuestions() {
                $.ajax({
                    type: "POST",
                    url: "CommonInterface.aspx/getQuestions",
                    data: "{pageType:'" + "HealthCareConfirmation" + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var index = 0;
                        $.each(data.d, function (index, value) {
                            if (index == 0) {
                                $("#<%=hdHealthCareConfirmQuestionID.ClientID%>").val(value.QuestionID);
                                $("#HealthCareConfirmQuestion").html(value.QuestionText.replace("-EmpName-", $("#<%=hdFirstName.ClientID%>").val()));
                            }
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //alert( errorThrown );
                    }
                });
            }
            $(document).ready(function () {
                GethealthCareQuestions();
            });
     </script>
      
    <section id="main-content">
<span class="mask"></span>
<div class="wrapper width-med">
	<div class="plms-alert secondary-clr"> 
              <p><%=Resources.Resource.lblAccidentIncident %>
                   <asp:HiddenField  ID="hdFirstName"  runat="server" />
           <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> ( <%=Resources.Resource.lblAISite %> <span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>,  <%=Resources.Resource.lblEmployeeID %>  <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
          </p> 
    </div><!--End .plms-alert-->
	<div class="boxed-content">		
		 <h2><%=Resources.Resource.lblHealthCare %>  </h2>
		 <div class="boxed-content-body">
	   <div class="column span-9">
			<div class="alert secondary-clr">
            <asp:HiddenField  runat="server" ID="hdHealthCareConfirmQuestionID" />
			<p id="HealthCareConfirmQuestion">
            </p>
			</div><!--End .alert-->
		</div><!--End .column-->		
		<div class="column span-3">
			<div class="btngrp align-r"> 
                <asp:Button ID="btnYes"  runat="server"  title= "<%$ Resources:Resource, lblYes %>" Text="<%$ Resources:Resource, lblYes %>" class="btn" OnClick="btnYes_Click"/>
                <asp:Button ID="btnNo"  runat="server"  title= "<%$ Resources:Resource, lblNo %>" Text="<%$ Resources:Resource, lblNo %>" class="btn" OnClick="btnNo_Click"  />
           	</div><!--End .btngrp-->
		</div><!--End .column-->		
		</div><!--End .boxed-content-body-->
		
		<nav class="pagination-nav">	 
              <asp:Button ID="btnPrevoius"  runat="server" title= "<%$ Resources:Resource, lblPrevoius  %>" Text="<%$ Resources:Resource, lblPrevoius %>" class="btn large" OnClick="btnPrevoius_Click"/>
            	 
		</nav><!--End .pagination-nav-->
	</div><!--End .boxed-content-->	
	<footer> 
           <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn" OnClick="btnSubmitwithoutSaving_Click" />
	</footer>     
</div><!--End .wrapper-->
</section><!--End #main-content-->        
</asp:Content>

