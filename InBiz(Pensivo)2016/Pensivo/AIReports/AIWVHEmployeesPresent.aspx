﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIWVHEmployeesPresent.aspx.cs" Inherits="AIReports_AIWVHEmployeesPresent" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../_styles/ai-reports.css" />
      <script   type="text/javascript">
          function GetWVHDescription()
          {
              $.ajax( {
                  type: "POST",
                  url: "CommonInterface.aspx/getQuestions",
                  data: "{pageType:'" + "WVHEmployeesPresent" + "'}",
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  success: function ( data )
                  {
                      var index = 0;
                      $.each( data.d, function ( index, value )
                      {
                          if ( index == 0 )
                          {
                              $( "#<%=hdWVHEmpPresentQuestionID1.ClientID%>" ).val( value.QuestionID );
                              $( "#lblWVHEmpPresentQuestiontext" ).html( value.QuestionText );
                              $( "#<%=txtName.ClientID%>" ).attr( "placeholder", value.QuestionText );
                              $( "#lblWVHEmpPresentQuestiontextToolTip" ).html( value.QuestionText );
                          }
                      } );
                  },
                  error: function ( XMLHttpRequest, textStatus, errorThrown )
                  {
                      //alert( errorThrown );
                  }
              });
          }
          $( document ).ready( function ()
          {
              GetWVHDescription();
          });
          </script>

    <section id="main-content" class="pg-ca-preventions">
<span class="mask"></span>
<div class="wrapper">

	<div class="plms-alert secondary-clr">
		   <p> 
               <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
           </p>
	</div><!--End .plms-alert-->
	
	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		 <%=Resources.Resource.lblMandatoryField %></p>
	</div><!--End .plms-alert-->
	
	

	<div class="layout-sidebar-right">

		<div class="boxed-content">
			
			<div class="boxed-content-body">
			
			<header class="boxed-content-header">
			<h2> <%=Resources.Resource.lblEmployeePresentTitle %> </h2>

			
			</header>
			
			<div class="plms-fieldset pull-up-10 push-down">
                <asp:HiddenField  ID="hdWVHEmpPresentQuestionID1"  runat="server" />
			<label class="plms-label is-hidden" for="witness-name" id="lblWVHEmpPresentQuestiontext"> </label>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtName"  runat="server"  CssClass="plms-input skin2"></asp:TextBox>
         	<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p  id="lblWVHEmpPresentQuestiontextToolTip"></p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			
			<a href="#nogo" class="btn fluid large clearfix" title="Click here to add another">Click here to add another</a>
					
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">
                 <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"   />
                <asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevious  %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large" OnClick="btnPrevious_Click" />
            
		 


			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span><%=Resources.Resource.lblHideMenu %></span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
			  <uc1menu:uc1  ID="lblmenu"   runat="server"/>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp"> 

        <asp:Button  ID="btnSaveAndExit"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater  %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn large" OnClick="btnSaveAndExit_Click"   />

        <asp:Button  ID="btnSubmitmyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport  %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn large align-r" OnClick="btnSubmitmyReport_Click"  />
          
	
    </div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
</asp:Content>

