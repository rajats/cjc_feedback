﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIPropertyDamageExp.aspx.cs" Inherits="AIReports_AIPropertyDamageExp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_styles/ai-reports.css" />    
       <script>           
               function showMore()
               {
                   if ( $( "#dvSecond" ).hasClass( "divHide" ) )
                   {
                       $( "#dvSecond" ).removeClass( 'divHide' );
                       $( "#dvSecond" ).addClass( 'divShow' );
                       $( "#<%= ancShowMore.ClientID%>" ).hide();
                   } 
              }
            </script>

    <section id="main-content">
        <span class="mask"></span>
        <div class="wrapper width-med">

            <div class="plms-alert secondary-clr">
               <p>
                    <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %> #<span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->

            <div class="boxed-content">

                <h2><%=Resources.Resource.lblPropertyDamage %>  </h2>

                <div class="boxed-content-body">

                    <h3 class="h4"><%=Resources.Resource.lblExamples %></h3>

                    <div id="dvFirst">
                              <%=Resources.Resource.lblPropertyDamageExpText %>

                    </div>              
                      <div id="dvSecond" class="divHide">
                          <%=Resources.Resource.lblPropertyDamageExpText1 %>
                    </div>

                      <br />
               <a id="ancShowMore" class="btn fluid large clearfix" title="Click to read other examples" onclick="showMore();"  runat="server"><%= Resources.Resource.lblLoadMore %></a>
                      

                </div>
                <!--End .boxed-content-body-->

                <nav class="pagination-nav">

                    <asp:Button ID="btnPrevoius" runat="server" title="<%$ Resources:Resource, lblPrevoius  %>" Text="<%$ Resources:Resource, lblPrevoius %>" class="btn large" OnClick="btnPrevoius_Click" />
                    <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />

                </nav>
                <!--End .pagination-nav-->

            </div>
            <!--End .boxed-content--> 
            <footer> 

      <%--          <a href="#nogo" class="btn" title="Exit Without Saving">Exit Without Saving</a>--%>
                <asp:Button ID="btnExitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn" OnClick="btnExitwithoutSaving_Click" />
            </footer>

        </div>
        <!--End .wrapper-->
    </section>
    <!--End #main-content-->
</asp:Content>

