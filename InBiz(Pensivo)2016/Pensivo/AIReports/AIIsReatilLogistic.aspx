﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIIsReatilLogistic.aspx.cs" Inherits="AIReports_AIIsReatilLogistic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
    <link href="../_css/global.css" rel="stylesheet" />

    <script  type="text/javascript">
        function GetIsRetailLogisticQst()
        {
            $.ajax( {
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "IsReatilLogistic" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function ( data )
                {
                    var index = 0;
                    $.each(data.d, function ( index, value )
                    {
                        if ( index == 0 )
                        {
                            $('#<%=hdlocationQuestion1.ClientID%>').val(value.QuestionID);
                            $('#<%=locationQuestion1.ClientID%>').html( value.QuestionText);
                        }

                    } );
                },
                error: function ( XMLHttpRequest, textStatus, errorThrown )
                {
                    //alert( errorThrown );
                }
            } );
        }
        
        
        $(document).ready(function()
        {
            GetIsRetailLogisticQst();
        });
        

        $( document ).ready( function ()
        {
            if ( document.getElementById( "locationretail" ).checked == true || document.getElementById( "locationlogistics" ).checked == true )
            {

            }
            else
            {
                $( "#<%=btnNext.ClientID %>" ).attr( "disabled", "disabled" );
                $( "#<%=btnNext.ClientID %>" ).removeClass( "btn" ).removeClass( "large" ).removeClass( "align-r" );
                $( "#<%=btnNext.ClientID %>" ).addClass( "btngrayout" ).addClass( "large" ).addClass( "align-r" );
            }
        });
        function activeButton()
        {
            $( "#<%=btnNext.ClientID %>" ).removeAttr( "disabled" );
            $( "#<%=btnNext.ClientID %>" ).removeClass( "btngrayout" ).removeClass( "large" ).removeClass( "align-r" );
            $( "#<%=btnNext.ClientID %>" ).addClass( "btn").addClass("large").addClass( "align-r" );            
        } 

    </script> 
    <section id="main-content" class="pg-location">
        <span class="mask"></span>
        <div class="wrapper width-med">           
            
            <div class="plms-alert secondary-clr">
                <p>
                     <%=Resources.Resource.lblAccidentIncident %><strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %>  <span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>

            <!--End .plms-alert-->
            <div class="boxed-content">
                <h2> <%=Resources.Resource.lblLocation %></h2>
                <p>  
                         
                </p>
                <div class="boxed-content-body">
                    
                   

                 <div class="fieldset-group location-type" onkeypress="return disableEnterKey(event)">                     
                     <asp:HiddenField   ID="hdlocationQuestion1"  runat="server"/>
                        <p>    
                             <asp:Label ID="locationQuestion1"  runat="server" Text=""></asp:Label>
                        </p>
                       	<div class="label-checkbox-group">
                            <label class="radio-input-label" for="locationretail">
                                <input type="radio" id="locationretail" name="locationtype" value="R" onclick="activeButton();"/>    Retail Location</label>
                            <label class="radio-input-label" for="locationlogistics">
                                <input type="radio" id="locationlogistics" name="locationtype" value="L"   onclick="activeButton();"/>   Logistics Location</label>
                        </div>
                        <!--End .label-radio-group-->
                    </div> 
                    <!--End .fieldset-group-->
                   
                    <!--End .fieldset-group-->
                </div>
                <!--End .boxed-content-body-->
                <nav class="pagination-nav">
                  <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click" />
                     
                    <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />
                      </nav>
                <!--End .pagination-nav-->

            </div>
            <!--End .boxed-content-->
            <footer>
              <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn"  />
            </footer>

        </div>
        <!--End .wrapper-->
    </section>    
</asp:Content>

