﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIWVHOffenders.aspx.cs" Inherits="AIReports_AIWVHOffenders" %>
<%@ Register Src="~/AIReports/UserControl/IncidentMenu.ascx" TagName="UC1" TagPrefix="UC1Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
    <script type="text/javascript">
        function GetWVHFollowUpReport()
        {
            $.ajax( {
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "WVHOffenders" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function ( data )
                {
                    var index = 0;
                    $.each( data.d, function (index, value)
                    {
                        if ( index == 0 )
                        {
                            $( "#<%=hdWVHOffendersQuestionID1.ClientID%>" ).val( value.QuestionID );
                                $( "#lblWVHOffendersQuestiontext" ).html( value.QuestionText );
                                $( "#<%=txtallegedoffendername.ClientID%>" ).attr( "placeholder", value.QuestionText );
                                $( "#lblWVHOffendersQuestiontextTip1" ).html( value.QuestionText );
                            }
                            if ( index == 1 )
                            {
                                $( "#<%=hdWVHOffendersQuestionID2.ClientID%>" ).val( value.QuestionID );
                                $( "#lblWVHOffendersQuestiontext2" ).html( value.QuestionText );
                                $( "#<%=txtidentifyinginformation.ClientID%>" ).attr( "placeholder", value.QuestionText );
                                 $( "#lblWVHOffendersQuestiontextTip2" ).html( value.QuestionText );
                             }

                            if ( index == 2 )
                            {
                                $( "#<%=hdWVHOffendersQuestionID3.ClientID%>" ).val( value.QuestionID );
                                $( "#spCoworker" ).html( value.QuestionText );
                            }
                            if ( index == 3 )
                            {
                                $( "#<%=hdWVHOffendersQuestionID4.ClientID%>" ).val( value.QuestionID );
                                $( "#spsupervisor" ).html( value.QuestionText );
                            }

                            if ( index == 4 )
                            {
                                $( "#<%=hdWVHOffendersQuestionID5.ClientID%>" ).val( value.QuestionID );
                                $( "#spformeremployee" ).html( value.QuestionText );
                            }
                            if ( index == 5 )
                            {
                                $( "#<%=hdWVHOffendersQuestionID6.ClientID%>" ).val( value.QuestionID );
                                $( "#spSpousePartner" ).html( value.QuestionText );
                            }
                            if ( index == 6 )
                            {
                                $( "#<%=hdWVHOffendersQuestionID7.ClientID%>" ).val( value.QuestionID );
                                $( "#spFamily" ).html( value.QuestionText );

                            }
                            if ( index == 7 )
                            {
                                $( "#<%=hdWVHOffendersQuestionID8.ClientID%>" ).val( value.QuestionID );
                                $( "#spother" ).html( value.QuestionText );
                            }

                            if ( index == 8 )
                            {
                                $( "#<%=hdOthers.ClientID%>" ).val( value.QuestionID );
                                $( "#txtOtherQuestion" ).html( value.QuestionText );
                                $( "#<%=txtOther.ClientID%>" ).attr( "placeholder", value.QuestionText );
                                $( "#txtOtherQuestionToolTip" ).html( value.QuestionText );
                            }

                        } );
                    },
                    error: function ( XMLHttpRequest, textStatus, errorThrown )
                    {
                        //alert( errorThrown );
                    }
                } );
            }
            $( document ).ready( function ()
            {
              GetWVHFollowUpReport();              
            });
         
         
            function validate() {
              var bSelected = document.getElementById("ContentPlaceHolder1_Chkother").checked;
              if (bSelected)
              {
                //document.getElementById('dvOthers').style.display = "block";
                $("#<%=dvOthers.ClientID %>").show();
              }
              else
              {
                //document.getElementById('dvOthers').style.display = "none";
                $("#<%=dvOthers.ClientID %>").hide();
              }
            }
    </script>
    

    <section id="main-content" class="pg-ca-preventions">
        <span class="mask"></span>
        <div class="wrapper">

            <div class="plms-alert secondary-clr">
                <p>
                    <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->

            <div class="plms-alert neutral">
                <p class="mandatory-fields-message">
                    <i class="req-icon" title="Mandatory Field">*</i>
                    <%=Resources.Resource.lblMandatoryField %>
                </p>
            </div>
            <!--End .plms-alert-->

            <div class="layout-sidebar-right">
                <div class="boxed-content">
                    <div class="boxed-content-body">

                        <header class="boxed-content-header">
                            <h2><%=Resources.Resource.WVHOffendersText1 %> </h2> 
                            <p class="pull-up-10"><%=Resources.Resource.WVHOffendersText2 %>  </p>
                        </header>
                        <div class="plms-fieldset pull-up-10">
                            <asp:HiddenField ID="hdWVHOffendersQuestionID1" runat="server" />
                            <label class="plms-label is-hidden" for="alleged-offender-name" id="lblWVHOffendersQuestiontext"></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtallegedoffendername" runat="server" CssClass="plms-input skin2"></asp:TextBox>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="lblWVHOffendersQuestiontextTip1"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                        <div class="plms-fieldset push-down">
                            <asp:HiddenField ID="hdWVHOffendersQuestionID2" runat="server" />
                            <label class="plms-label is-hidden" for="identifying-information" id="lblWVHOffendersQuestiontext2"></label>
                            <div class="plms-tooltip-parent">

                                <asp:TextBox ID="txtidentifyinginformation" runat="server" CssClass="plms-textarea skin2" TextMode="MultiLine"></asp:TextBox>


                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="lblWVHOffendersQuestiontextTip2"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                        <div class="fieldset-group">
                            <p class="bold">Relationship to Affected Employee</p>

                            <div class="label-checkbox-group">
                                <asp:HiddenField ID="hdWVHOffendersQuestionID3" runat="server" />
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_Chkcoworker">
                                         <input type="checkbox" id="Chkcoworker" name="co-worker" runat="server" /><span id="spCoworker"></span></label>
                                <asp:HiddenField ID="hdWVHOffendersQuestionID4" runat="server" />
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_Chksupervisor">
                                    <input type="checkbox" id="Chksupervisor" name="supervisor" runat="server" /><span id="spsupervisor"></span> </label>
                                <asp:HiddenField ID="hdWVHOffendersQuestionID5" runat="server" />
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_Chkformeremployee">
                                    <input type="checkbox" id="Chkformeremployee" name="former-employee" runat="server" /><span id="spformeremployee"></span> </label>
                                <asp:HiddenField ID="hdWVHOffendersQuestionID6" runat="server" />
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_chkspousepartner">
                                    <input type="checkbox" id="chkspousepartner" name="spouse-partner" runat="server" /><span id="spSpousePartner"></span> </label>
                                <asp:HiddenField ID="hdWVHOffendersQuestionID7" runat="server" />
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_Chkfamily">
                                    <input type="checkbox" id="Chkfamily" name="family"  runat="server" /><span id="spFamily"></span> </label>
                                <asp:HiddenField ID="hdWVHOffendersQuestionID8" runat="server" />
                                <label class="checkbox-input-label" for="ContentPlaceHolder1_Chkother">
                                    <input type="checkbox" id="Chkother" name="other"  runat="server" onchange="validate();" /><span id="spother"></span></label>
                            </div>
                            <!--End .label-checkbox-group-->
                        </div>
                        <!--End .fieldset-group-->


                        <div id="dvOthers" runat="server"  class="divHide">
                            <div class="plms-fieldset push-down">
                            <asp:HiddenField ID="hdOthers" runat="server" />
                            <label class="plms-label is-hidden" for="other-text-input" id="txtOtherQuestion"></label>
                            <div class="plms-tooltip-parent" >
                                <i class="req-icon" title="Mandatory Field">*</i>
                                <asp:TextBox ID="txtOther" runat="server" CssClass="plms-input skin2"> </asp:TextBox>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="txtOtherQuestionToolTip"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        </div>

                        
                        <!--End .plms-fieldset-->

                        <a  class="btn fluid large clearfix" title="Click here to add another">Click here to add another</a>

                    </div>
                    <!--End .boxed-content-body-->

                    <nav class="pagination-nav">

                        <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />
                        <asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevious  %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large" OnClick="btnPrevious_Click" />

                    </nav>
                    <!--End .pagination-nav-->

                </div>
                <!--End .boxed-content-->

                <aside id="main-aside">
                    <div class="aside-toggle-handle">
                        <div class="flyout-message-wrapper">
                            <a href="#toggle-aside" class="btn">
                                <i class="icon"></i>
                                <span><%=Resources.Resource.lblHideMenu %></span>
                            </a>
                        </div>
                        <!--End .flyout-message-wrapper-->
                    </div>
                    <!--End .aside-toggle-handle-->

                    <nav class="aside-main-nav">
                        <UC1Menu:UC1 ID="lblmenu" runat="server" />
                    </nav>
                    <!--End .aside-main-nav-->

                </aside>
                <!--End #main-aside-->

            </div>
            <!--End .layout-sidebar-right-->

            <footer>
                <div class="btngrp">
                    <asp:Button ID="btnSaveAndExit" runat="server" title="<%$ Resources:Resource, lblSaveAndExitLater  %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn large" OnClick="btnSaveAndExit_Click" />
                    <asp:Button ID="btnSubmitmyReport" runat="server" title="<%$ Resources:Resource, lblSubmitMyReport  %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn large align-r" OnClick="btnSubmitmyReport_Click" />

                </div>
            </footer>
        </div>
        <!--End .wrapper-->
    </section>
    <!--End #main-content-->

</asp:Content>

