﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIGetPropertyDamageDetail.aspx.cs" Inherits="AIReports_AIGetPropertyDamageDetail" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%> 
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <script  type="text/javascript">
        function GetPropertyDamageQuestions() {
            $.ajax({
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "PropertyDamagedDetail" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var index = 0;
                    $.each(data.d, function (index, value) {
                        if (index == 0) {
                            $('#<%=hdPropertyDamageDetailQuestion1.ClientID%>').val(value.QuestionID); 
                            $("#hdPropertyDamageDetailQuestionText1ToolTip").html(value.QuestionText);
                            $("#<%=txtdPropertyDamageDetailQuestionAns1.ClientID%>").attr("placeholder", value.QuestionText); 
                        }
                        if (index == 1) {
                            $('#<%=hdPropertyDamageDetailQuestion2.ClientID%>').val(value.QuestionID);
                            $("#hdPropertyDamageDetailQuestionText2ToolTip").html(value.QuestionText);
                            $("#<%=txtdPropertyDamageDetailQuestionAns2.ClientID%>").attr("placeholder", value.QuestionText); 
                        }
                        if (index == 2) {
                            $('#<%=hdPropertyDamageDetailQuestion3.ClientID%>').val(value.QuestionID);
                            $("#hdPropertyDamageDetailQuestionText3ToolTip").html(value.QuestionText);
                            $("#<%=txtdPropertyDamageDetailQuestionAns3.ClientID%>").attr("placeholder", value.QuestionText); 
                        }
                        if (index == 3) {
                            $('#<%=hdPropertyDamageDetailQuestion4.ClientID%>').val(value.QuestionID);
                            $("#hdPropertyDamageDetailQuestionText4ToolTip").html(value.QuestionText);
                            $("#<%=txtdPropertyDamageDetailQuestionAns4.ClientID%>").attr("placeholder", value.QuestionText); 
                        }
                        if (index == 4) {
                            $('#<%=hdPropertyDamageDetailQuestion5.ClientID%>').val(value.QuestionID);
                            $("#hdPropertyDamageDetailQuestionText5ToolTip").html(value.QuestionText);
                            $("#<%=txtdPropertyDamageDetailQuestionAns5.ClientID%>").attr("placeholder", value.QuestionText); 
                        }
                        if (index == 5) {
                            $('#<%=hdPropertyDamageDetailQuestion6.ClientID%>').val(value.QuestionID);
                            $("#hdPropertyDamageDetailQuestionText6ToolTip").html(value.QuestionText);
                            $("#<%=txtdPropertyDamageDetailQuestionAns6.ClientID%>").attr("placeholder", value.QuestionText); 
                        }
                        index =  parseInt(index) + 1;                        
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert( errorThrown );
                }
            });
        }
        $(document).ready(function () {
            GetPropertyDamageQuestions();           
        });
     
    </script> 
       <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
        <link rel="stylesheet" href="../_styles/ai-reports.css" />

    <section id="main-content">
<span class="mask"></span>
<div class="wrapper">
	<div class="plms-alert secondary-clr"> 
        <p> 
               <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
        </p> 
	</div><!--End .plms-alert-->	
 	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>	 
      <%=Resources.Resource.lblMandatoryField %>
		</p>
	</div><!--End .plms-alert-->

	<div class="layout-sidebar-right">

		<div class="boxed-content">
			
			<div class="boxed-content-body">
				
			<h2><%=Resources.Resource.lblPropertyDamage %></h2>
			
			<div class="plms-fieldset pull-up-10">
            <asp:HiddenField  ID="hdPropertyDamageDetailQuestion1"  runat="server"/>   
			<label class="plms-label is-hidden" for="licensee-name" id="hdPropertyDamageDetailQuestionText1"></label>
        <i class="req-icon" title="Mandatory Field">*</i>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtdPropertyDamageDetailQuestionAns1" runat="server" CssClass="plms-input skin2" >
            </asp:TextBox>  
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="hdPropertyDamageDetailQuestionText1ToolTip">  </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
			                  
            <div style ="clear:both; height:10px;">

            </div>
		 


 	        <div class="plms-fieldset">
            <asp:HiddenField  ID="hdPropertyDamageDetailQuestion2"  runat="server"/>   
			<label class="plms-label is-hidden" for="licensee-name" id="hdPropertyDamageDetailQuestionText2"></label>
              <i class="req-icon" title="Mandatory Field">*</i>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtdPropertyDamageDetailQuestionAns2" runat="server" CssClass="plms-input skin2"  >
            </asp:TextBox>  
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="hdPropertyDamageDetailQuestionText2ToolTip">  </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
 
		    <div style ="clear:both; height:10px;">
            </div>

            <div class="plms-fieldset">
            <asp:HiddenField  ID="hdPropertyDamageDetailQuestion3"  runat="server"/>   
			<label class="plms-label is-hidden" for="licensee-name" id="hdPropertyDamageDetailQuestionText3"></label>
              <i class="req-icon" title="Mandatory Field">*</i>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtdPropertyDamageDetailQuestionAns3" runat="server" CssClass="plms-input datepicker skin2"  name="date">
            </asp:TextBox>  
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="hdPropertyDamageDetailQuestionText3ToolTip">  </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->

			 <div style ="clear:both; height:10px;">

                </div>
            <div class="plms-fieldset">
            <asp:HiddenField  ID="hdPropertyDamageDetailQuestion4"  runat="server"/>   
			<label class="plms-label is-hidden" for="licensee-name" id="hdPropertyDamageDetailQuestionText4"></label>
              <i class="req-icon" title="Mandatory Field">*</i>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtdPropertyDamageDetailQuestionAns4" runat="server" CssClass="plms-input skin2 datepicker" >
            </asp:TextBox>  
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="hdPropertyDamageDetailQuestionText4ToolTip">  </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->	
         	
                 <div style ="clear:both; height:10px;">

                </div>

           <div class="plms-fieldset">
            <asp:HiddenField  ID="hdPropertyDamageDetailQuestion5"  runat="server"/>   
			<label class="plms-label is-hidden" for="licensee-name" id="hdPropertyDamageDetailQuestionText5"></label>
             <i class="req-icon" title="Mandatory Field">*</i>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtdPropertyDamageDetailQuestionAns5" runat="server" CssClass="plms-input skin2" >
            </asp:TextBox>  
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="hdPropertyDamageDetailQuestionText5ToolTip">  </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->	
            <div style ="clear:both; height:10px;">
            </div>
            

            <div class="plms-fieldset">
            <asp:HiddenField  ID="hdPropertyDamageDetailQuestion6"  runat="server"/>   
			<label class="plms-label is-hidden" for="licensee-name" id="hdPropertyDamageDetailQuestionText6"></label>
              <i class="req-icon" title="Mandatory Field">*</i>
			<div class="plms-tooltip-parent">
            <asp:TextBox ID="txtdPropertyDamageDetailQuestionAns6" runat="server" CssClass="plms-input skin2" >
            </asp:TextBox>  
			<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
			<div class="plms-tooltip-body">
			<p id="hdPropertyDamageDetailQuestionText6ToolTip">  </p>
			</div><!--End .plms-tooltip-body-->
			</div><!--End .plms-tooltip-->
			</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->	

			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">

                <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"    />
                        <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"  />

		<%--	<a href="ai-reports-21-6.php" title="Previous" class="btn large">Previous</a>
			<a href="ai-reports-22-1.php" title="Next" class="btn large align-r">Next</a>--%>
			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span><%=Resources.Resource.lblHideMenu %></span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			 
			<nav class="aside-main-nav">
			      <UC1Menu:UC1  ID="lblmenu"   runat="server"/> 
	                		<%--<ul> 
                  <li ><a id="A1" href="AIGeneralInformation.aspx" title=""  runat="server"><%= Resources.Resource.lblGenInfoPage %></a></li>
			      <li ><a id="A2" href="AiDetails.aspx" title=""  runat="server"><%= Resources.Resource.lblDetailPage %> </a></li>
                  <li class="is-active"><a id="A9" href="AIGetPropertyDamageDetail.aspx" title=""  runat="server"><%= Resources.Resource.lblPropertyDamagePage %> </a></li>
			      <li><a id="A3" href="AIAccidentIncidentDetail.aspx" title=""  runat="server">  <%= Resources.Resource.lblAccidentIncidentdetailPage %>   </a></li>
			      <li><a id="A4" href="AIContributingFactor.aspx" title=""  runat="server">   <%= Resources.Resource.lblcontributingFactorPage %> </a></li>
			      <li><a id="A5" href="AIActionAndPrevention.aspx" title="" runat="server">  <%= Resources.Resource.lblAiCorrectionActionPage %>   </a></li>
		       	<li><a id="A6" href="AIWitnessesCoWorker.aspx" title=""  runat="server">  <%= Resources.Resource.lblCoworkerPage %></a></li>
			     <li><a id="A7" href="AIWorkerComment.aspx" title="" runat="server">  <%= Resources.Resource.lblWorkerCommentPage %> </a></li>
			<li><a id="A8" href="AIPeopleInvolved.aspx" title=""  runat="server">  <%= Resources.Resource.lblPeopleInvolvedPage %>   </a></li>
 
			</ul>--%>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
  
        <asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click"   />
         <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click"   />
         
	</div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->



 <script  type ="text/javascript">         
        (function () {
          $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            yearRange: GetDateRange(),
            maxDate: '+0D'
          });
        })();

        function GetDateRange() {
          var currentDate = new Date();
          var currentYear = currentDate.getFullYear();
          var previousYear = currentDate.getFullYear() - parseInt(<%= Resources.Resource.lblYearRange %>);
          return previousYear + ':' + currentYear;
        }
</script>
  

</asp:Content>

