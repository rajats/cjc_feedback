﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIValidationSummary.aspx.cs" Inherits="AIReports_AIValidationSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link rel="stylesheet" href="../_styles/ai-reports.css" />
<section id="main-content">
<span class="mask"></span>
<div class="wrapper width-med">
	<div class="plms-alert secondary-clr">
         <p> 
               <%=Resources.Resource.lblAccidentIncident %>
                     <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
        </p> 
    </div><!--End .plms-alert--> 

	<div class="boxed-content">
		
		<h2><%=Resources.Resource.lblSummary %></h2>
		
		<div class="boxed-content-body">
			
		<h3 class="h4"> <%=Resources.Resource.lbloops %>  </h3>
		
		<p>
            

            It seems you haven't answered yes to any of the accident/incident types. 
		To proceed, you need to answer yes to at least one of the questions presented. 
		Use the previous and next buttons to navigate, and make an appropriate selection.

		</p>
		
		<%--<a href="#nogo" class="btn">Back To Incident</a>--%>

                <asp:Button  ID="btnBackToIncident"  runat="server"  title= "<%$ Resources:Resource, lblbacktoIncident %>" Text="<%$ Resources:Resource, lblbacktoIncident %>" class="btn large align-" OnClick="btnBackToIncident_Click" />

				
		</div><!--End .boxed-content-body-->
		
		<nav class="pagination-nav"> 

<%--		<a href="ai-reports-17.php" title="Previous" class="btn large">Previous</a>
		<a href="ai-reports-19.php" title="Next" class="btn large align-r">Next</a>
		 --%>            
                <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"/>
             
                      
               <%--<asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r"/> --%>
        
        </nav><!--End .pagination-nav-->
	
	</div><!--End .boxed-content-->
	
	<footer>
  <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn" OnClick="btnSubmitwithoutSaving_Click"/>
         
<%--	<a href="#nogo" class="btn" title="Exit Without Saving">Exit Without Saving</a>--%>
	 </footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->



</asp:Content>

