﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIDetails6.aspx.cs" Inherits="AIReports_AIDetails6" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>
  
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../_styles/ai-reports.css" />
     <script   type="text/javascript">
         function GetAIDetails5()
         {
             $.ajax( {
                 type: "POST",
                 url: "CommonInterface.aspx/getQuestions",
                 data: "{pageType:'" + "IncidentAccidentDetail5" + "'}",
                 dataType: "json",
                 contentType: "application/json; charset=utf-8",
                 success: function ( data )
                 {
                     var index = 0;
                     $.each( data.d, function ( index, value )
                     {
                         if ( index == 0 )
                         {
                             $( "#<%=hdAIQuestionID1.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionText1" ).html( value.QuestionText );
                         }
                         
                         if ( index == 1 )
                         {
                             $( "#<%=hdAIQuestionID2.ClientID%>" ).val( value.QuestionID );
                              $( "#AIQuestionText2" ).html( value.QuestionText );
                         }
                         if ( index == 2 )
                         {
                             $( "#<%=hdAIQuestionID3.ClientID%>" ).val( value.QuestionID );
                              $( "#AIQuestionText3" ).html( value.QuestionText );
                         }
                         if ( index == 3 )
                         {
                             $( "#<%=hdAIQuestionID4.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionText4" ).html( value.QuestionText );
                         }
                         if ( index == 4 )
                         {
                             $( "#<%=hdAIQuestionID5.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionText5" ).html( value.QuestionText );
                         }
                         if ( index == 5 )
                         {
                             $( "#<%=hdAIQuestionID6.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionText6" ).html( value.QuestionText );
                         }
                         if ( index == 6)
                         {
                             $( "#<%=hdAIQuestionID7.ClientID%>" ).val( value.QuestionID );
                               $( "#AIQuestionText7" ).html( value.QuestionText );
                         }
                         if ( index == 7 )
                         {
                             $( "#<%=hdAIQuestionID8.ClientID%>" ).val( value.QuestionID );
                              $( "#AIQuestionText8" ).html( value.QuestionText );
                         }
                         if ( index == 8 )
                         {
                             $( "#<%=hdAIQuestionID9.ClientID%>" ).val( value.QuestionID );
                              $( "#AIQuestionText9" ).html( value.QuestionText );
                         }
                         if ( index == 9 )
                         {
                             $( "#<%=hdAIQuestionID10.ClientID%>" ).val( value.QuestionID );
                               $( "#AIQuestionText10" ).html( value.QuestionText );
                         }
                         if ( index == 10 )
                         {
                             $( "#<%=hdAIQuestionID11.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionText11" ).html( value.QuestionText );
                         }
                         if ( index == 11 )
                         {
                             $( "#<%=hdAIQuestionID12.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionText12" ).html( value.QuestionText );
                         }

                         if ( index == 12 )
                         {
                             $( "#<%=hdAIQuestionID13.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionText13" ).html( value.QuestionText );
                         }
                         if ( index == 13 )
                         {
                             $( "#<%=hdAIQuestionID14.ClientID%>" ).val( value.QuestionID );
                             $( "#AIQuestionText14" ).html( value.QuestionText );
                         } 

                         index = parseInt( index ) + 1;
                     } );
                 },
                 error: function ( XMLHttpRequest, textStatus, errorThrown )
                 {
                     //alert( errorThrown );
                 }
             } );
         }
         $( document ).ready( function ()
         {
             GetAIDetails5();
         } );
          </script>   

<section id="main-content">
<span class="mask"></span>
<div class="wrapper">

	<div class="plms-alert secondary-clr">
	  <p> 
             <%=Resources.Resource.lblAccidentIncident %>
             <strong class="employee-name">
             <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
             <asp:Label ID="lblEmpID" runat="server" /></span>)
     </p>	
	</div><!--End .plms-alert-->
 
	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		  <%=Resources.Resource.lblMandatoryField %></p>
	</div><!--End .plms-alert--> 

	<div class="layout-sidebar-right">

		<div class="boxed-content">			
			<div class="boxed-content-body">				
			<h2><%= Resources.Resource.lblDetails %></h2>
			
			<p> <%= Resources.Resource.lblAiWorkerSchedule1 %> </p>
			
	<%--		<p class="small-text pull-up-10"><strong>hh</strong> represents the number of complete hours that 
			have passed since midnight (00-24), <strong>mm</strong> represents the 
			number of complete minutes that have passed since the start of 
			the hour (00-59).</p>--%>
			
			<div class="plms-table scheduled-shifts">
			<table> 
			<caption>  <%= Resources.Resource.lblAiWorkerScheduleText1 %> </caption>
		<%--	<tr class="example">
				<th> <%= Resources.Resource.lblExample %> </th>
				<td><input class="plms-input skin2" maxlength="5" value="08:00" disabled="disabled" type="text" /></td>
				<td><input class="plms-input skin2" value="16:00" disabled="disabled" type="text" /></td>
			</tr>--%>
			<tr>
				<th>Monday</th>
				<td>
                    <asp:HiddenField ID="hdAIQuestionID1" runat="server"/>
                    <label for="mon-start-time"  id="AIQuestionText1"></label><input name="mon-start-time" id="txtmonstarttime" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text"   runat="server"/></td>
				<td>
                     <asp:HiddenField ID="hdAIQuestionID2" runat="server"/>
                    <label for="mon-end-time" id="AIQuestionText2"> </label><input class="plms-input skin2" name="mon-end-time" id="txtmonendtime" placeholder="End Time (hh:mm)" type="text"    runat="server"/></td>
			</tr>
			<tr>
				<th>Tuesday</th>
				<td>
                     <asp:HiddenField ID="hdAIQuestionID3" runat="server"/>
                    <label for="tue-start-time"  id="AIQuestionText3"> </label><input name="tue-start-time" id="txttuestarttime" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text"   runat="server" /></td>
				<td>
                    <asp:HiddenField ID="hdAIQuestionID4" runat="server"/>
                    <label for="tue-end-time" id="AIQuestionText4"> </label><input class="plms-input skin2" name="tue-end-time" id="txttueendtime" placeholder="End Time (hh:mm)" type="text"  runat="server" /></td>
			</tr>
			<tr>
				<th>Wednesday</th>
				<td>
                       <asp:HiddenField ID="hdAIQuestionID5" runat="server"/>
                    <label for="wed-start-time" id="AIQuestionText5"  > </label><input name="wed-start-time" id="txtwedstarttime" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text"  runat="server" /></td>
				<td>
                     <asp:HiddenField ID="hdAIQuestionID6" runat="server"/>
                    <label for="wed-end-time"   id="AIQuestionText6" ></label><input class="plms-input skin2" name="wed-end-time" id="txtwedendtime" placeholder="End Time (hh:mm)" type="text"   runat="server" /></td>
			</tr>
			<tr>
				<th>Thursday</th>
				<td>
                     <asp:HiddenField ID="hdAIQuestionID7" runat="server"/>
                    <label for="thur-start-time"  id="AIQuestionText7"></label><input name="thur-start-time" id="txtthurstarttime" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text"   runat="server"/></td>
				<td>
                    <asp:HiddenField ID="hdAIQuestionID8" runat="server"/>
                    <label for="thur-end-time"  id="AIQuestionText8"> </label><input class="plms-input skin2" name="thur-end-time" id="txtthurendtime" placeholder="End Time (hh:mm)" type="text"    runat="server"/></td>
			</tr>
			<tr>
				<th>Friday</th>
				<td>
                 <asp:HiddenField ID="hdAIQuestionID9" runat="server"/>
                    <label for="fri-start-time" id="AIQuestionText9"> </label><input name="fri-start-time" id="txtfristarttime" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text"   runat="server"/></td>
				<td>
                     <asp:HiddenField ID="hdAIQuestionID10" runat="server"/>
                    <label for="fri-end-time" id="AIQuestionText10"> </label><input class="plms-input skin2" name="fri-end-time" id="txtfriendtime" placeholder="End Time (hh:mm)" type="text"  runat="server"/></td>
			</tr>
			<tr>
				<th>Saturday</th>
				<td>
                    <asp:HiddenField ID="hdAIQuestionID11" runat="server"/>
                    <label for="sat-start-time"  id="AIQuestionText11"> </label><input name="sat-start-time" id="txtsatstarttime" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text"   runat="server"/></td>
				<td>
                    <asp:HiddenField ID="hdAIQuestionID12" runat="server"/>
                    <label for="sat-end-time" id="AIQuestionText12"></label><input class="plms-input skin2" name="sat-end-time" id="txtsatendtime" placeholder="End Time (hh:mm)" type="text"   runat="server"/></td>
			</tr>
			<tr>
				<th>Sunday</th>
				<td>
                    <asp:HiddenField ID="hdAIQuestionID13" runat="server"/>
                    <label for="sun-start-time" id="AIQuestionText13"> </label><input name="sun-start-time" id="txtsunstarttime" class="plms-input skin2" maxlength="5" placeholder="Start Time (hh:mm)" type="text"  runat="server" /></td>
				<td>
                    <asp:HiddenField ID="hdAIQuestionID14" runat="server"/>
                    <label for="sun-end-time"  id="AIQuestionText14"> </label><input class="plms-input skin2" name="sun-end-time" id="txtsunendtime" placeholder="End Time (hh:mm)" type="text"    runat="server"/></td>
			</tr>
			</table>
			</div>
					
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">

            
           <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click" />
           <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click"   />
  

            <%-- <a href="ai-reports-21-4.php" title="Previous" class="btn large">Previous</a>
			<a href="ai-reports-21-6.php" title="Next" class="btn large align-r">Next</a>--%>
			
            </nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span><%=Resources.Resource.lblHideMenu %></span>

					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
			 <%--<ul>
			<li><a id="A1" href="AIGeneralInformation.aspx" title=""  runat="server"><%= Resources.Resource.lblGenInfoPage %></a></li>
			<li class="is-active"><a id="A2" href="AiDetails.aspx" title=""  runat="server"><%= Resources.Resource.lblDetailPage %> </a></li>
			<li><a id="A3" href="AIAccidentIncidentDetail.aspx" title=""  runat="server">  <%= Resources.Resource.lblAccidentIncidentdetailPage %>   </a></li>
			<li><a id="A4" href="AIContributingFactor.aspx" title=""  runat="server">   <%= Resources.Resource.lblcontributingFactorPage %> </a></li>
			<li><a id="A5" href="AIActionAndPrevention.aspx" title="" runat="server">  <%= Resources.Resource.lblAiCorrectionActionPage %>   </a></li>
			<li><a id="A6" href="AIWitnessesCoWorker.aspx" title=""  runat="server">  <%= Resources.Resource.lblCoworkerPage %></a></li>
			<li><a id="A7" href="AIWorkerComment.aspx" title="" runat="server">  <%= Resources.Resource.lblWorkerCommentPage %> </a></li>
			<li><a id="A8" href="AIPeopleInvolved.aspx" title=""  runat="server">  <%= Resources.Resource.lblPeopleInvolvedPage %>   </a></li>
			</ul>--%>
                <UC1Menu:UC1  ID="lblmenu"   runat="server"/>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">

        <asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click"   /> 
         <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click"   />
         

<%--
	<a href="#nogo" class="btn" title="Save & Exit to Continue Later">Save &amp; Exit to Continue Later</a>
	<a href="#nogo" class="btn" title="Submit My Report">Submit My Report</a>--%>
	</div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->
</asp:Content>

