﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AIEmailValidation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnValidateEmail_Click(object sender, EventArgs e)
    { 
        // Insert Initiator ID
        Session["EmployeeDetails"] = null;

        List<Employee> empObj = new List<Employee>();
        Employee objEmp = new Employee();
        empObj.Add(new Employee { AIReportInitiatorEmailID = Convert.ToString(txtEmail.Text) });
        Session["EmployeeDetails"] = empObj;

        //if (Session["EmployeeDetails"] == null)
        //{
        //    List<Employee> empObj = new List<Employee>();
        //    Employee objEmp = new Employee();
        //    empObj.Add(new Employee { AIReportInitiatorEmailID = Convert.ToString(txtEmail.Text) });
        //    Session["EmployeeDetails"] = empObj;
        //}
        //else 
        //{
        //    Session["EmployeeDetails"] = null;
        //}
        Response.Redirect("AIStartANewAIInfo.aspx");
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }
}