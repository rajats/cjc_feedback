﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIGetPropertyDamageDetail : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
                getFormData();
            }
        }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        string redirectURL = string.Empty;
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        if (objList != null)
        {
            var ISLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
            if (ISLicensee != null)
            {
                // Get result from all scenarios
                var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AIDetails4.aspx", false);
                    return;
                }
                // Case 6 : Personal Injury AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 7 : Personal Injury AND First Aid AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 8 : Personal Injury AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 9 : Personal Injury AND First Aid AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 10: Personal Injury AND First Aid AND Health Care
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 11: Personal Injury AND Health Care
                if (IsPerosnalInjury != null && IsHealthCare != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 12: Property Damage AND Lost Time
                if (IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 13: Personal Injury AND First Aid
                if (IsPerosnalInjury != null && IsFirstAidBox != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }

                //Case 17: Personal Injury AND  Property Damage
                if (IsPerosnalInjury != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }

                // Case 14: Property Damage
                if (IsPropertyDamage != null)
                {
                    Response.Redirect("AIContributingFactor.aspx", false);
                    return;
                }
                // Case 15: Lost Time
                if (IsLostTime != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 16: Personal Injury
                if (IsPerosnalInjury != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }                 
            }
            else
            {
                // Get result from all scenarios
                var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 6 : Personal Injury AND Health Care AND Property Damage
                if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 7 : Personal Injury AND First Aid AND Property Damage
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 8 : Personal Injury AND Health Care AND Lost Time
                if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 9 : Personal Injury AND First Aid AND Lost Time
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 10: Personal Injury AND First Aid AND Health Care
                if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 11: Personal Injury AND Health Care
                if (IsPerosnalInjury != null && IsHealthCare != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 12: Property Damage AND Lost Time
                if (IsLostTime != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 13: Personal Injury AND First Aid
                if (IsPerosnalInjury != null && IsFirstAidBox != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                //Case 17: Personal Injury AND  Property Damage
                if (IsPerosnalInjury != null && IsPropertyDamage != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 14: Property Damage
                if (IsPropertyDamage != null)
                {
                    Response.Redirect("AIContributingFactor.aspx", false);
                    return;
                }
                // Case 15: Lost Time
                if (IsLostTime != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }
                // Case 16: Personal Injury
                if (IsPerosnalInjury != null)
                {
                    Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                    return;
                }  
            }
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.PropertyDamagedDetail).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }

                if(!string.IsNullOrEmpty( Convert.ToString(hdPropertyDamageDetailQuestion1.Value)))
                {
                    objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdPropertyDamageDetailQuestion1.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = txtdPropertyDamageDetailQuestionAns1.Text,
                        AIFormID = aiFormID,
                        AIPageName = PageName.PropertyDamagedDetail
                    });

                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdPropertyDamageDetailQuestion2.Value)))
                {
                    objList.Add(
                  new AIQuestionAnswer
                  {
                      AIQuestionID = Convert.ToInt32(hdPropertyDamageDetailQuestion2.Value),
                      AIQuestionType = QuestionType.TextType,
                      AIQuestionAnswerText = txtdPropertyDamageDetailQuestionAns2.Text,
                      AIFormID = aiFormID,
                      AIPageName = PageName.PropertyDamagedDetail
                  });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdPropertyDamageDetailQuestion3.Value)))
                {

                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdPropertyDamageDetailQuestion3.Value),
                            AIQuestionType = QuestionType.TextType,
                            AIQuestionAnswerText = txtdPropertyDamageDetailQuestionAns3.Text,
                            AIFormID = aiFormID,
                            AIPageName = PageName.PropertyDamagedDetail
                        });
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdPropertyDamageDetailQuestion4.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdPropertyDamageDetailQuestion4.Value),
                            AIQuestionType = QuestionType.TextType,
                            AIQuestionAnswerText = txtdPropertyDamageDetailQuestionAns4.Text,
                            AIFormID = aiFormID,
                            AIPageName = PageName.PropertyDamagedDetail
                        });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdPropertyDamageDetailQuestion5.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdPropertyDamageDetailQuestion5.Value),
                            AIQuestionType = QuestionType.TextType,
                            AIQuestionAnswerText = txtdPropertyDamageDetailQuestionAns5.Text,
                            AIFormID = aiFormID,
                            AIPageName = PageName.PropertyDamagedDetail
                        });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdPropertyDamageDetailQuestion6.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdPropertyDamageDetailQuestion6.Value),
                            AIQuestionType = QuestionType.TextType,
                            AIQuestionAnswerText = txtdPropertyDamageDetailQuestionAns6.Text,
                            AIFormID = aiFormID,
                            AIPageName = PageName.PropertyDamagedDetail
                        });
                }
                Session["AIReportQuestions"] = objList;

                managePreviousNext("");
                RedirectionLink();
                //Response.Redirect("AIActionAndPrevention.aspx");
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBoard.aspx", false);
    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBoard.aspx", false);
    }
    private void RedirectionLink()
    { 
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
        try
        {
            if (objList != null)
            {

                var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (IsLicensee != null)
                {

                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }

                    //Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }

                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }

                }
                else
                {


                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    
                    //Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }

                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIActionAndPrevention.aspx", false);
                        return;
                    }
 
                }
            }
        }
        catch { }
        finally { }
    }
    private void managePreviousNext(string NextPageUrl)
    {
        List<ManagePagePreviousNext> objPage = new List<ManagePagePreviousNext>();
        try
        {
            objPage = (List<ManagePagePreviousNext>)Session["AIPagesList"];
            if (objPage != null)
            {
                var resultItem = objPage.Where(x => x.pageName == PageName.InjuryIllness).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objPage.Remove(removeItem);
                    }
                }
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIGetPropertyDamageDetail.aspx", NextPageUrl = NextPageUrl, pageName = PageName.PropertyDamagedDetail, PreviousPageUrl = "" });
            }
            else
            {
                objPage = new List<ManagePagePreviousNext>();
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIGetPropertyDamageDetail.aspx", NextPageUrl = NextPageUrl, pageName = PageName.PropertyDamagedDetail, PreviousPageUrl = "" });
            }
            Session["AIPagesList"] = objPage;

        }
        catch { }
        finally { }
    }
    
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            var resultItem = objList.Where(x => x.AIPageName == PageName.PropertyDamagedDetail).OrderBy(x => x.AIQuestionID).ToList();
            if (resultItem != null && resultItem.Count > 0)
            {
                int count = 0;
                foreach (var result in resultItem)
                {
                    if (count == 0 )
                    {
                        txtdPropertyDamageDetailQuestionAns1.Text = Convert.ToString(result.AIQuestionAnswerText);
                    }
                    else if (count == 1 )
                    {
                        txtdPropertyDamageDetailQuestionAns2.Text = Convert.ToString(result.AIQuestionAnswerText);
                    }
                    else if (count == 2 )
                    {
                        txtdPropertyDamageDetailQuestionAns3.Text = Convert.ToString(result.AIQuestionAnswerText);
                    }
                    else if (count == 3 )
                    {
                        txtdPropertyDamageDetailQuestionAns4.Text = Convert.ToString(result.AIQuestionAnswerText);
                    }
                    else if (count == 4 )
                    {
                        txtdPropertyDamageDetailQuestionAns5.Text = Convert.ToString(result.AIQuestionAnswerText);
                    }
                    else if (count == 5 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        txtdPropertyDamageDetailQuestionAns6.Text = Convert.ToString(result.AIQuestionAnswerText);
                    }                     
                    count++;
                }
            }
        }
        catch { }
        finally { }
    }


}