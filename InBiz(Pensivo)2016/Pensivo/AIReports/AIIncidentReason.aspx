﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIIncidentReason.aspx.cs" Inherits="AIReports_AIIncidentReason" %>
<%@ Register  Src="~/AIReports/UserControl/IncidentMenu.ascx"  TagName="UC1"  TagPrefix="UC1Menu"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_libs/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../_styles/ai-reports.css" />
    <script type="text/javascript">
        function GetIncidentReason() {
            $.ajax({
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "AIIncidentReason" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var index = 0;
                    $.each(data.d, function (index, value) {
                        if (index == 0) {
                          $("#<%=hdAIDescriptionofAccidentQuestionID.ClientID%>").val(value.QuestionID);
                          $("#lblAIDescriptionofAccidentQuestionID").html(value.QuestionText);
                          $("#lblAIDescriptionofAccidentToolTip").html(value.QuestionText);
                          $("#<%=txtAIIncidentReason.ClientID%>").attr("placeholder", value.QuestionText);
                      }
                      index = parseInt(index) + 1;
                  });
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                  //alert( errorThrown );
              }
          });
      }
      $(document).ready(function () {
          GetIncidentReason();
      });

    </script>
    <section id="main-content">
        <span class="mask"></span>
        <div class="wrapper">

            <div class="plms-alert secondary-clr">
                <p>
                    <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %> #<span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->

     <%--     <div class="plms-alert neutral">
                <p class="mandatory-fields-message">
                    <i class="req-icon" title="Mandatory Field">*</i>
                     <%=Resources.Resource.lblMandatoryField %>
                </p>
            </div> 
            <!--End .plms-alert-->--%>



            <div class="layout-sidebar-right">

                <div class="boxed-content">

                    <div class="boxed-content-body">

                        <h2><%=Resources.Resource.lblAIDescriptionofAccidentIncident %></h2>
                        <asp:HiddenField ID="hdAIDescriptionofAccidentQuestionID" runat="server" />
                        <div class="plms-fieldset pull-up-10">
                            <label class="plms-label is-hidden" for="reason-for-acts-conditions" id="lblAIDescriptionofAccidentQuestionID"></label>
                            <div class="plms-tooltip-parent">
                                <%--<textarea rows="4" cols="4" class="plms-textarea skin2" id="reason-for-acts-conditions" name="reason-for-acts-conditions" placeholder="Indicate the underlying reasons for the existence of these acts or conditions (ask why)."></textarea>--%>
                                <textarea rows="4" cols="4" class="plms-textarea skin2" id="txtAIIncidentReason" name="reason-for-acts-conditions" runat="server" ></textarea>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="lblAIDescriptionofAccidentToolTip"></p>
                                    </div>
                                    <!--End .plms-tooltip-body-->
                                </div>
                                <!--End .plms-tooltip-->
                            </div>
                            <!--End .plms-tooltip-parent-->
                        </div>
                        <!--End .plms-fieldset-->

                    </div>
                    <!--End .boxed-content-body-->

                    <nav class="pagination-nav">

                        <asp:Button ID="btnPrevious" runat="server" title="<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click" />
                        <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />

                        <%--                        <a href="AIIncidentDescConfirm.aspx" title="Previous" class="btn large" runat="server">Previous</a>
                        <a href="AIContributingFactor.aspx" title="Next" class="btn large align-r" runat="server">Next</a>--%>
                    </nav>
                    <!--End .pagination-nav-->

                </div>
                <!--End .boxed-content-->

                <aside id="main-aside">
                    <div class="aside-toggle-handle">
                        <div class="flyout-message-wrapper">
                            <a href="#toggle-aside" class="btn">
                                <i class="icon"></i>
                                <span><%= Resources.Resource.lblHideMenu  %></span>
                            </a>
                        </div>
                        <!--End .flyout-message-wrapper-->
                    </div>
                    <!--End .aside-toggle-handle-->

                    <nav class="aside-main-nav">
                 <%--    <ul>
			<li><a id="A1" href="AIGeneralInformation.aspx" title=""  runat="server"><%= Resources.Resource.lblGenInfoPage %></a></li>
			<li><a id="A2" href="AiDetails.aspx" title=""  runat="server"><%= Resources.Resource.lblDetailPage %> </a></li>
			<li  class="is-active"><a id="A3" href="AIAccidentIncidentDetail.aspx" title=""  runat="server">  <%= Resources.Resource.lblAccidentIncidentdetailPage %>   </a></li>
			<li><a id="A4" href="AIContributingFactor.aspx" title=""  runat="server">   <%= Resources.Resource.lblcontributingFactorPage %> </a></li>
			<li><a id="A5" href="AIActionAndPrevention.aspx" title="" runat="server">  <%= Resources.Resource.lblAiCorrectionActionPage %>   </a></li>
			<li><a id="A6" href="AIWitnessesCoWorker.aspx" title=""  runat="server">  <%= Resources.Resource.lblCoworkerPage %></a></li>
			<li><a id="A7" href="AIWorkerComment.aspx" title="" runat="server">  <%= Resources.Resource.lblWorkerCommentPage %> </a></li>
			<li><a id="A8" href="AIPeopleInvolved.aspx" title=""  runat="server">  <%= Resources.Resource.lblPeopleInvolvedPage %>   </a></li>
			</ul>--%>

			      <UC1Menu:UC1  ID="lblmenu"   runat="server"/> 

                    </nav>
                    <!--End .aside-main-nav-->
                </aside>
                <!--End #main-aside-->

            </div>
            <!--End .layout-sidebar-right-->

            <footer>
                <div class="btngrp">
                 <%--   <a href="#nogo" class="btn" title="Save & Exit to Continue Later">Save &amp; Exit to Continue Later</a>
                    <a href="#nogo" class="btn" title="Submit My Report">Submit My Report</a>--%>

                       <asp:Button  ID="btnSubmitandExitLater"  runat="server"  title= "<%$ Resources:Resource, lblSaveAndExitLater %>" Text="<%$ Resources:Resource, lblSaveAndExitLater %>" class="btn" OnClick="btnSubmitandExitLater_Click"    /> 
         <asp:Button  ID="btnSubmitMyReport"  runat="server"  title= "<%$ Resources:Resource, lblSubmitMyReport %>" Text="<%$ Resources:Resource, lblSubmitMyReport %>" class="btn" OnClick="btnSubmitMyReport_Click1"     />
         
                </div>
            </footer>

        </div>
        <!--End .wrapper-->
    </section>
    <!--End #main-content-->


</asp:Content>

