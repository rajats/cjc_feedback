﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIIncidentDescription.aspx.cs" Inherits="AIReports_AIIncidentDescription" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <link rel="stylesheet" href="../_styles/ai-reports.css" />

    <section id="main-content">
<span class="mask"></span>
<div class="wrapper">

	<div class="plms-alert secondary-clr">
		<p>Accident / Incident report for <strong class="employee-name">Joshua Johnston</strong> 
		(Site #<span class="site-num">1308</span>, Employee ID <span class="employee-id">06690</span>)</p>
	</div><!--End .plms-alert-->
	
	<div class="plms-alert neutral">
		<p class="mandatory-fields-message">
		<i class="req-icon" title="Mandatory Field">*</i>
		Symbol signifies mandatory field which must be 
		completed prior to submitting the AI form.</p>
	</div><!--End .plms-alert-->
	
	

	<div class="layout-sidebar-right">

		<div class="boxed-content">
			
			<div class="boxed-content-body">
				
			<h2>Description of Accident / Incident</h2>
			
			<div class="plms-fieldset pull-up-10">
				<label class="plms-label is-hidden" for="address">Describe what happened in detail &mdash; include the sequence of events which preceded the incident.</label>
				
				<div class="plms-tooltip-parent">
					<i class="req-icon" title="Mandatory Field">*</i>
					<textarea rows="4" cols="4" class="plms-textarea skin2" 
					id="address" name="address" placeholder="Describe what happened in detail - include the sequence of events which preceded the incident. "></textarea>
					
					<div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
						<div class="plms-tooltip-body">
							<p>Describe what happened in detail - include the sequence of events which preceded the incident. </p>
						</div><!--End .plms-tooltip-body-->
					</div><!--End .plms-tooltip-->
				</div><!--End .plms-tooltip-parent-->
			</div><!--End .plms-fieldset-->
								
			</div><!--End .boxed-content-body-->
			
			<nav class="pagination-nav">
			<a href="AILicenseeDetails.aspx" title="Previous" class="btn large"  runat="server">Previous</a>
			<a href="AIIncidentDescConfirm.aspx" title="Next" class="btn large align-r"  runat="server">Next</a>
			</nav><!--End .pagination-nav-->
		
		</div><!--End .boxed-content-->
		
		<aside id="main-aside">
			<div class="aside-toggle-handle">
				<div class="flyout-message-wrapper">
					<a href="#toggle-aside" class="btn">
						<i class="icon"></i>
						<span>Hide Menu</span>
					</a>
				</div><!--End .flyout-message-wrapper-->
			</div><!--End .aside-toggle-handle-->
			
			<nav class="aside-main-nav">
			<ul>
			<li><a href="#nogo" title="">General Information</a></li>
			<li><a href="#nogo" title="">Details</a></li>
			<li class="is-active"><a href="#nogo" title="">Description of Accident / Incident</a></li>
			<li><a href="#nogo" title="">Contributing Factors / Conditions</a></li>
			<li><a href="#nogo" title="">Corrective Actions &amp; Prevention</a></li>
			<li><a href="#nogo" title="">Witnesses / Co-Workers</a></li>
			<li><a href="#nogo" title="">Worker's Comments</a></li>
			<li><a href="#nogo" title="">People Involved</a></li>
			</ul>
			</nav><!--End .aside-main-nav-->
		</aside><!--End #main-aside-->
	
	</div><!--End .layout-sidebar-right-->
	
	<footer>
	<div class="btngrp">
	<a href="#nogo" class="btn" title="Save & Exit to Continue Later">Save &amp; Exit to Continue Later</a>
	<a href="#nogo" class="btn" title="Submit My Report">Submit My Report</a>
	</div>
	</footer>

</div><!--End .wrapper-->
</section><!--End #main-content-->

</asp:Content>

