﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIWVHExp.aspx.cs" Inherits="AIReports_AIWVHExp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     
            <script>
                function showMore()
                {
                    if ( $( "#dvSecond" ).hasClass( "divHide" ) )
                    {
                        $( "#dvSecond" ).removeClass( 'divHide' );
                        $( "#dvSecond" ).addClass( 'divShow' );
                    }
                    else if ( $( "#dvThird" ).hasClass( "divHide" ) )
                    {
                        $( "#dvThird" ).removeClass( 'divHide' );
                        $( "#dvThird" ).addClass( 'divShow' );

                    }
                    else if ( $( "#dvThird" ).hasClass( "divShow" ) )
                    {
                        $( "#dvThird" ).removeClass( 'divShow' );
                        $( "#dvThird" ).addClass( 'divHide' );

                    }

                    if ( $( "#dvThird" ).hasClass( "divShow" ) && $( "#dvSecond" ).hasClass( "divShow" ) )
                    {
                        $( "#ancShowMore" ).hide();
                        
                    }
                    
                }
            </script>

    <link rel="stylesheet" href="../_styles/ai-reports.css" />
    <section id="main-content">
        <span class="mask"></span>
        <div class="wrapper width-med">

            <div class="plms-alert secondary-clr">
                <p>
                    <%=Resources.Resource.lblAccidentIncident %>
                    <strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %><span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->

            
            <div class="boxed-content">

                <h2 style="font-size: 23px;"><%= Resources.Resource.lblWVHTitleExp %></h2>

                <div class="boxed-content-body">
                    <h3 class="h4" style="font-size: 20px;"><%= Resources.Resource.lblWVHTitle1Exp %></h3>

                    <%--<%= Resources.Resource.lblWVHExpText %> --%>
                    <div id="dvFirst">

                        <%= Resources.Resource.lblWVHExp1 %>
                    </div>

                    <div id="dvMid">

                        <div class="divHide" id="dvSecond">

                            <%= Resources.Resource.lblWVHExp2 %>
                        </div>


                        <div class="divHide" id="dvThird">


                            <%= Resources.Resource.lblWVHExp3 %>
                        </div>
                    </div>
                    <div>
                        <br />
                    </div>

                    <a id="ancShowMore"  class="btn fluid large clearfix" title="Click to read other examples" onclick="showMore();"><%= Resources.Resource.lblLoadMore %></a>


                </div>
                <!--End .boxed-content-body-->
                <nav class="pagination-nav">
                    <asp:Button ID="btnPrevoius" runat="server" title="<%$ Resources:Resource, lblPrevoius  %>" Text="<%$ Resources:Resource, lblPrevoius %>" class="btn large" OnClick="btnPrevoius_Click" />
                    <asp:Button ID="btnNext" runat="server" title="<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />

                </nav>
                <!--End .pagination-nav-->
            </div>
            <!--End .boxed-content-->
            <footer>

                <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn" OnClick="btnSubmitwithoutSaving_Click" />

                <!--
	       <a href="#nogo" class="btn" title="Exit Without Saving">Exit Without Saving</a>
        -->

            </footer>
        </div>
        <!--End .wrapper-->
    </section>
    <!--End #main-content-->
</asp:Content>

