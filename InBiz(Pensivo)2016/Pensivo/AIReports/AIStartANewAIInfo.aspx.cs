﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AIStartANewAIInfo : System.Web.UI.Page
{
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
           
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIEmployeeDetails.aspx");
    }
    protected void btnPrevoius_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIEmailValidation.aspx");
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIDashBoard.aspx", false);
    }
}