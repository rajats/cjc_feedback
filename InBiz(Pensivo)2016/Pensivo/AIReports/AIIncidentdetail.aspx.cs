﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AIIncidentdetail : System.Web.UI.Page
{
    List<Employee> empObj = new List<Employee>();
    int aiFormID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.IncidentDetail).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {                    
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                } 


                objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdIncidentDetailQuestionID1.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = Convert.ToString(txtIncidentDetailQuestion1Ans.Text),
                        AIFormID = aiFormID,
                        AIPageName = PageName.IncidentDetail
                    });
                objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdIncidentDetailQuestionID2.Value),
                        AIQuestionType = QuestionType.TextType,
                        AIQuestionAnswerText = Convert.ToString(txtIncidentDetailQuestion2Ans.Text),
                        AIFormID = aiFormID,
                        AIPageName = PageName.IncidentDetail
                    });
                objList.Add(
                  new AIQuestionAnswer
                  {
                      AIQuestionID = Convert.ToInt32(hdIncidentDetailQuestionID3.Value),
                      AIQuestionType = QuestionType.TextType,
                      AIQuestionAnswerText = Convert.ToString(txtIncidentDetailQuestion3Ans.Text),
                      AIFormID = aiFormID,
                      AIPageName = PageName.IncidentDetail
                  });

                objList.Add(
                new AIQuestionAnswer
                {
                    AIQuestionID = Convert.ToInt32(hdIncidentDetailQuestionID4.Value),
                    AIQuestionType = QuestionType.TextType,
                    AIQuestionAnswerText = Convert.ToString(txtIncidentDetailQuestion4Ans.Text),
                    AIFormID = aiFormID,
                    AIPageName = PageName.IncidentDetail
                });
                objList.Add(
                new AIQuestionAnswer
                {
                    AIQuestionID = Convert.ToInt32(hdIncidentDetailQuestionID5.Value),
                    AIQuestionType = QuestionType.TextType,
                    AIQuestionAnswerText = Convert.ToString(txtIncidentDetailQuestion5Ans.Text),
                    AIFormID = aiFormID,
                    AIPageName = PageName.IncidentDetail
                });

                Session["AIReportQuestions"] = objList;
            }
            Response.Redirect("AIPropertyDamge.aspx", false);
        }
        catch (Exception ex) { throw ex; }
        finally { }

    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AILostTimeConfirm.aspx", false);
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {
        // Redirct to AI DashBoard 
        Response.Redirect("AIDashBoard.aspx", false);
    }
}