﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class AIReports_AIEmployeeDetails : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }    
    protected void gvRoles_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        //if (e.ColumnIndex == 6)
        //{
        //    e.CellHtml = string.Format(@"<a href=""javascript:;""  onclick=""EditUser({0})"">" + Resources.Resource.lblEdit + "</a>", e.CellHtml);
        //}
    }
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //List<Employee> empObj = new List<Employee>();
        try
        {
            Employee objEmp = new Employee();
            string txtName = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtEmpName"]);
            string txtUserID = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtEmpID"]);
            string txtSite = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtSite"]);

           // empObj.Add(new Employee { EmpName = txtName, EmpExtID = txtUserID, Location = txtSite });

            objEmp.EmpName = txtName;
            objEmp.EmpExtID = txtUserID;
            objEmp.Location = txtSite;

           // Session["EmployeeDetails"] = empObj;

            if (!string.IsNullOrEmpty(txtName) || !string.IsNullOrEmpty(txtUserID) || !string.IsNullOrEmpty(txtSite))
            {
                gvRoles.DataSource = objEmp.GetEmployeeList();
                gvRoles.DataBind();
            }

        }
        catch { }
        finally { }
       
    }
    protected void btnSubmitwithoutSaving_Click(object sender, EventArgs e)
    {

    }
}