﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AIIsLogistic.aspx.cs" Inherits="AIReports_AIIsLogistic" %>

  
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../_styles/ai-reports.css" />
    <link href="../_css/global.css" rel="stylesheet" />

    <script  type="text/javascript">
        function GetIsLogistic() {            
            $.ajax({
                type: "POST",
                url: "CommonInterface.aspx/getQuestions",
                data: "{pageType:'" + "IsLogistic" + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data)
                {
                    var index = 0;
                    $.each(data.d, function (index, value)
                    {
                        if (index == 0)
                        {
                            $('#<%=hdIsLogisticQuestion1.ClientID%>').val(value.QuestionID);
                            $('#<%=IsLogisticQuestion1.ClientID%>').html( value.QuestionText );
                        }
                        if ( index == 1)
                        {
                            $('#<%=hdIsLogisticQuestion2.ClientID%>').val( value.QuestionID );
                            $('#<%=IsLogisticQuestion2.ClientID%>').html( value.QuestionText );
                        }
                        index = parseInt(index) + 1;
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown)
                {
                    //alert( errorThrown );
                }
            });
        }
  
        $(document).ready(function()
        {
            GetIsLogistic();
        });
        $( document ).ready( function ()
        {
            if(  $( "#<%=rdcheckOfinside.ClientID %>" ).is(":checked")  == true ||  $( "#<%=rdCheckoffFleet.ClientID %>" ).is(":checked")  == true)
            {

            }
            else
            {
                $( "#<%=btnNext.ClientID %>" ).attr( "disabled", "disabled" );
                $( "#<%=btnNext.ClientID %>" ).removeClass( "btn" ).removeClass( "large" ).removeClass( "align-r" );
                $( "#<%=btnNext.ClientID %>" ).addClass( "btngrayout" ).addClass( "large" ).addClass( "align-r" );
            }
        });
        function activeButton()
        {            
            $( "#<%=btnNext.ClientID %>" ).removeAttr("disabled");
            $( "#<%=btnNext.ClientID %>" ).removeClass("btngrayout").removeClass("large").removeClass("align-r");
            $( "#<%=btnNext.ClientID %>" ).addClass("btn").addClass( "large" ).addClass( "align-r" );
        }
    </script> 
    <section id="main-content" class="pg-location">
        <span class="mask"></span>
        <div class="wrapper width-med">          
            
            <div class="plms-alert secondary-clr">
                <p>
                     <%=Resources.Resource.lblAccidentIncident %><strong class="employee-name">
                        <asp:Label ID="lblEmpName" runat="server" /></strong> (<%=Resources.Resource.lblAISite %>  <span class="site-num"><asp:Label ID="lblSite" runat="server" /></span>, <%=Resources.Resource.lblEmployeeID %> <span class="employee-id">
                            <asp:Label ID="lblEmpID" runat="server" /></span>)
                </p>
            </div>
            <!--End .plms-alert-->
            <div class="boxed-content">
                <h2> <%=Resources.Resource.lblLocation %></h2>
                <p>         
                </p>
                <div class="boxed-content-body"> 
                    <div class="fieldset-group location-type" onkeypress="return disableEnterKey(event)" >
                        <p>
                            <asp:HiddenField  ID="hdIsLogisticQuestion1"  runat="server"/>
                            <asp:Label ID="IsLogisticQuestion1"  runat="server"></asp:Label>
                            </p>
                        <div class="label-radio-group">
                            <label class="radio-input-label" for="licensee-yes">
                                <input type="radio" id="licensee-yes" name="Islicensee" value="1" />Yes</label>
                            <label class="radio-input-label" for="licensee-no">
                                <input type="radio" id="licensee-no" name="Islicensee" value="0"/>No</label>                           
                           
                            <label class="radio-input-label" for="licensee-na">
                                <input type="radio" id="licensee-na" name="Islicensee" value="2"/>N/A</label>
                        </div>
                        <!--End .label-radio-group-->
                    </div>
                    <!--End .fieldset-group-->
                    
                    <div class="fieldset-group location-type" onkeypress="return disableEnterKey(event)" >
                     <asp:HiddenField  ID="hdIsLogisticQuestion2"  runat="server"/>
                      <p>
                         <asp:Label ID="IsLogisticQuestion2"  runat="server"></asp:Label>
                       </p>
                        <div class="label-radio-group">
                            <label class="radio-input-label" for="licensee-yes">
                                <input type="radio" id="rdcheckOfinside" name="licensee" value="<%$ Resources:Resource, lblCheckOfinside %>"   runat = "server"   onclick="activeButton();"/>Yes</label>
                            <label class="radio-input-label" for="licensee-no">
                                <input type="radio" id="rdCheckoffFleet" name="licensee" value="<%$ Resources:Resource, lblCheckofffleet %>"  runat = "server"    onclick="activeButton();"/>No</label>                           
                            
                        </div>
                        <!--End .label-radio-group-->
                    </div>
                    <!--End .fieldset-group-->

                      
                </div>
                <!--End .boxed-content-body-->
                <nav class="pagination-nav">
                      <asp:Button  ID="btnPrevious"  runat="server"  title= "<%$ Resources:Resource, lblPrevious %>" Text="<%$ Resources:Resource, lblPrevious %>" class="btn large align-" OnClick="btnPrevious_Click"   />
                      
                     <asp:Button  ID="btnNext"  runat="server"  title= "<%$ Resources:Resource, lblNext %>" Text="<%$ Resources:Resource, btnNext %>" class="btn large align-r" OnClick="btnNext_Click" />
                      </nav>
                <!--End .pagination-nav-->

            </div>
            <!--End .boxed-content-->
            <footer>
              <asp:Button ID="btnSubmitwithoutSaving" runat="server" title="<%$ Resources:Resource, lblExitWithOutSaving %>" Text="<%$ Resources:Resource, lblExitWithOutSaving %>" class="btn"  />
            </footer>

        </div>
        <!--End .wrapper-->
    </section>    
</asp:Content>

