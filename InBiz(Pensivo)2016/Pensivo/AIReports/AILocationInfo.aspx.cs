﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AIReports_AILocationInfo : System.Web.UI.Page
{
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] == null)
            {
                //Employee objEmp = new Employee();
                //objEmp.GetEmployeeDetail(this.EmpID);
                //lblEmpName.Text = BusinessUtility.GetString(objEmp.EmpFirstName);
                //lblEmpID.Text = BusinessUtility.GetString(objEmp.EmpExtID);
                //lblSite.Text = BusinessUtility.GetString(objEmp.Location);
                //txtSite.Text = BusinessUtility.GetString(objEmp.Location);
                //empObj.Add(new Employee { EmpName = BusinessUtility.GetString(objEmp.EmpName), EmpExtID = BusinessUtility.GetString(objEmp.EmpExtID), Location = BusinessUtility.GetString(objEmp.Location), EmpFirstName = BusinessUtility.GetString(objEmp.EmpFirstName), EmpID = this.EmpID });
                //Session["EmployeeDetails"] = empObj;
            }
            else 
            {  
                   empObj = (List<Employee>)Session["EmployeeDetails"];
                   var result = empObj.FirstOrDefault();
                   if (result != null)
                   {
                       if (result.EmpID == 0 || string.IsNullOrEmpty(Convert.ToString(result.EmpID)))
                       {
                           // update existing one 
                           Employee objEmp = new Employee();
                           objEmp.GetEmployeeDetail(this.EmpID);
                           lblEmpName.Text = BusinessUtility.GetString(objEmp.EmpFirstName);
                           lblEmpID.Text = BusinessUtility.GetString(objEmp.EmpExtID);
                           lblSite.Text = BusinessUtility.GetString(objEmp.Location);
                           txtSite.Text = BusinessUtility.GetString(objEmp.Location);
                           
                           result.EmpName = BusinessUtility.GetString(objEmp.EmpName);
                           result.EmpExtID = BusinessUtility.GetString(objEmp.EmpExtID);
                           result.Location = BusinessUtility.GetString(objEmp.Location);
                           result.EmpFirstName = BusinessUtility.GetString(objEmp.EmpFirstName);
                           result.EmpID = this.EmpID;
                           
                           empObj.Clear();

                           empObj.Add(new Employee { EmpName = BusinessUtility.GetString(objEmp.EmpName), EmpExtID = BusinessUtility.GetString(objEmp.EmpExtID), Location = BusinessUtility.GetString(objEmp.Location), EmpFirstName = BusinessUtility.GetString(objEmp.EmpFirstName), EmpLastName = BusinessUtility.GetString(objEmp.EmpLastName), EmpID = this.EmpID, AIReportInitiatorEmailID = result.AIReportInitiatorEmailID });
                           Session["EmployeeDetails"] = empObj;                            
                       }
                       else
                       {
                           empObj = (List<Employee>)Session["EmployeeDetails"];
                           var result1 = empObj.FirstOrDefault();

                           lblEmpName.Text = BusinessUtility.GetString(result1.EmpName);
                           lblEmpID.Text = BusinessUtility.GetString(result1.EmpExtID);
                           lblSite.Text = BusinessUtility.GetString(result1.Location);
                           txtSite.Text = BusinessUtility.GetString(result1.Location);
                       }                        
                   } 
                   //if (empObj != null)
                   // { 
                   //     foreach (var result in empObj)
                   //     {
                   //         lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                   //         lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                   //         lblSite.Text = BusinessUtility.GetString(result.Location);
                   //         txtSite.Text = BusinessUtility.GetString(result.Location);
                   //     }                  
                   //  }            
            }
            //if (Session["EmployeeDetails"] != null)
            //{
            //    empObj = (List<Employee>)Session["EmployeeDetails"];
            //    if (empObj != null)
            //    {
            //        foreach (var result in empObj)
            //        {
            //            lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
            //            lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
            //            lblSite.Text = BusinessUtility.GetString(result.Location);
            //        }
            //    }
            //}
            //Employee objEmp = new Employee();
            //objEmp.GetEmployeeDetail(this.EmpID);
            //lblEmpName.Text = BusinessUtility.GetString(objEmp.EmpName);
            //lblEmpID.Text = BusinessUtility.GetString(objEmp.EmpExtID);
            //lblSite.Text = BusinessUtility.GetString(objEmp.Location);
            //txtSite.Text = BusinessUtility.GetString(objEmp.Location);
        }
    }    
    public int EmpID
    {
        get
        {
            int cStatus = 0;
            int.TryParse(Request.QueryString["empID"], out cStatus);
            return cStatus;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        validateSite();
    }
    private void validateSite()
    {
        Employee objEmp = new Employee();
        if (objEmp.IsValidEmpRef(EmpRefCode.Store, txtSite.Text) == true)
        {

            GlobalMessage.showAlertMessage(Resources.Resource.lblValidSite);
        }
        else
        {
            GlobalMessage.showAlertMessage(Resources.Resource.lblAiInvalidSite);
        }
        txtSite.Focus();
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {  
        // get the max form id  
        AIReportCls objAiReport = new AIReportCls();
        int maxFormID = objAiReport.getMaxFormID();  
        Session["AIReportQuestions"] = null;
        Employee objEmp = new Employee();
        List<AIQuestionAnswer> objQuestionAnswer = new List<AIQuestionAnswer>();
        try 
        {
            if (objEmp.IsValidEmpRef(EmpRefCode.Store, txtSite.Text))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(hdlocationQuestion1.Value)))
                {
                    objQuestionAnswer.Add(new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdlocationQuestion1.Value),
                        AIQuestionAnswerText = Convert.ToString(txtSite.Text),
                        AISection = PageName.Location,
                        AIQuestionType = QuestionType.TextType,
                        AIFormID = maxFormID,
                        AIPageName = PageName.Location
                    });
                }
                Session["AIReportQuestions"] = objQuestionAnswer;
                //GlobalMessage.showAlertMessage(Resources.Resource.lblValidSite);
                Response.Redirect("AIIsReatilLogistic.aspx", false);
            }
            else
            {
                GlobalMessage.showAlertMessage(Resources.Resource.lblAiInvalidSite);
            }  
        }
        catch { }
        finally { }

    }
}