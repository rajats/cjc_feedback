﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AIReports_AIContributingFactor : System.Web.UI.Page
{
    int aiFormID = 0;
    List<Employee> empObj = new List<Employee>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EmployeeDetails"] != null)
            {
                empObj = (List<Employee>)Session["EmployeeDetails"];
                if (empObj != null)
                {
                    foreach (var result in empObj)
                    {
                        lblEmpName.Text = BusinessUtility.GetString(result.EmpName);
                        lblEmpID.Text = BusinessUtility.GetString(result.EmpExtID);
                        lblSite.Text = BusinessUtility.GetString(result.Location);
                    }
                }
            }
            getFormData();
        }
    }
    private void getFormData()
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            var resultItem = objList.Where(x => x.AIPageName == PageName.ContributingFactor).OrderBy(x => x.AIQuestionID).ToList();
            if (resultItem != null && resultItem.Count > 0)
            {
                int count = 0;
                foreach (var result in resultItem)
                {
                    if (count == 0 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox1.Checked = true;
                    }
                    else if (count == 1 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox2.Checked = true;
                    }
                    else if (count == 2 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox3.Checked = true;
                    }
                    else if (count == 3 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox4.Checked = true;
                    }

                    else if (count == 4 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox5.Checked = true;
                    }
                    else if (count == 5 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox6.Checked = true;
                    }
                    else if (count == 6 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox7.Checked = true;
                    }
                    else if (count == 7 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox8.Checked = true;
                    }
                    else if (count == 8 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox9.Checked = true;
                    }
                    else if (count == 9 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox10.Checked = true;
                    }
                    else if (count == 10 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {

                        chkbox11.Checked = true;

                    }
                    else if (count == 11 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {

                        chkbox12.Checked = true;

                    }
                    else if (count == 12 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {

                        chkbox13.Checked = true;

                    }
                    else if (count == 13 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {

                        chkbox14.Checked = true;
                    }
                    else if (count == 14 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {

                        chkbox15.Checked = true;

                    }
                    else if (count == 15 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {

                        chkbox16.Checked = true;

                    }
                    else if (count == 16 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox17.Checked = true;
                    }
                    else if (count == 17 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox18.Checked = true;
                    }
                    else if (count == 18 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox19.Checked = true;
                    }
                    else if (count == 19 && Convert.ToString(result.AIQuestionAnswerText) == "1")
                    {
                        chkbox19.Checked = true;
                        dvOthers.Attributes.Remove("Class");
                        dvOthers.Attributes.Add("Class", "divShow");
                    }
                    else if (count == 20)
                    {
                        txtother.Value = Convert.ToString(result.AIQuestionAnswerText);
                    }

                    count++;
                }
            }
        }
        catch { }
        finally { }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        // Redirect to personal injury page
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];
            if (objList != null)
            {
                // Remove item if Item is already Exists(Based on PageName)
                var resultItem = objList.Where(x => x.AIPageName == PageName.ContributingFactor).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objList.Remove(removeItem);
                    }
                }
                // Get the AI Form ID
                var fromIDResult = objList.FirstOrDefault(x => x.AIPageName == PageName.Location);
                if (fromIDResult != null)
                {
                    aiFormID = Convert.ToInt32(fromIDResult.AIFormID);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID1.Value)))
                {
                    objList.Add(
                         new AIQuestionAnswer
                         {
                             AIQuestionID = Convert.ToInt32(hdQuestionID1.Value),
                             AIQuestionType = QuestionType.BooleanType,
                             AIQuestionAnswerText = Convert.ToString(chkbox1.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                             AIFormID = aiFormID,
                             AIPageName = PageName.ContributingFactor,
                             AISequence = 1
                         });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID2.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdQuestionID2.Value),
                            AIQuestionType = QuestionType.BooleanType,
                            AIQuestionAnswerText = Convert.ToString(chkbox2.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                            AIFormID = aiFormID,
                            AIPageName = PageName.ContributingFactor,
                            AISequence = 2
                        });
                }


                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID3.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdQuestionID3.Value),
                            AIQuestionType = QuestionType.BooleanType,
                            AIQuestionAnswerText = Convert.ToString(chkbox3.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                            AIFormID = aiFormID,
                            AIPageName = PageName.ContributingFactor,
                            AISequence = 3
                        });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID4.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                        {
                            AIQuestionID = Convert.ToInt32(hdQuestionID4.Value),
                            AIQuestionType = QuestionType.BooleanType,
                            AIQuestionAnswerText = Convert.ToString(chkbox4.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                            AIFormID = aiFormID,
                            AIPageName = PageName.ContributingFactor,
                            AISequence = 4
                        });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID5.Value)))
                {
                    objList.Add(
                       new AIQuestionAnswer
                       {
                           AIQuestionID = Convert.ToInt32(hdQuestionID5.Value),
                           AIQuestionType = QuestionType.BooleanType,
                           AIQuestionAnswerText = Convert.ToString(chkbox5.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                           AIFormID = aiFormID,
                           AIPageName = PageName.ContributingFactor,
                           AISequence = 5
                       });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID6.Value)))
                {

                    objList.Add(
                     new AIQuestionAnswer
                     {
                         AIQuestionID = Convert.ToInt32(hdQuestionID6.Value),
                         AIQuestionType = QuestionType.BooleanType,
                         AIQuestionAnswerText = Convert.ToString(chkbox6.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                         AIFormID = aiFormID,
                         AIPageName = PageName.ContributingFactor,
                         AISequence = 6
                     });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID7.Value)))
                {
                    objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdQuestionID7.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(chkbox7.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                        AIFormID = aiFormID,
                        AIPageName = PageName.ContributingFactor,
                        AISequence = 7
                    });

                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID8.Value)))
                {
                    objList.Add(
              new AIQuestionAnswer
              {
                  AIQuestionID = Convert.ToInt32(hdQuestionID8.Value),
                  AIQuestionType = QuestionType.BooleanType,
                  AIQuestionAnswerText = Convert.ToString(chkbox8.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                  AIFormID = aiFormID,
                  AIPageName = PageName.ContributingFactor,
                  AISequence = 8

              });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID9.Value)))
                {
                    objList.Add(
              new AIQuestionAnswer
              {
                  AIQuestionID = Convert.ToInt32(hdQuestionID9.Value),
                  AIQuestionType = QuestionType.BooleanType,
                  AIQuestionAnswerText = Convert.ToString(chkbox9.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                  AIFormID = aiFormID,
                  AIPageName = PageName.ContributingFactor,
                  AISequence = 9
              });


                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID10.Value)))
                {


                    objList.Add(
                            new AIQuestionAnswer
                            {
                                AIQuestionID = Convert.ToInt32(hdQuestionID10.Value),
                                AIQuestionType = QuestionType.BooleanType,
                                AIQuestionAnswerText = Convert.ToString(chkbox10.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                                AIFormID = aiFormID,
                                AIPageName = PageName.ContributingFactor,
                                AISequence = 10
                            });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID11.Value)))
                {

                    objList.Add(
                          new AIQuestionAnswer
                          {
                              AIQuestionID = Convert.ToInt32(hdQuestionID11.Value),
                              AIQuestionType = QuestionType.BooleanType,
                              AIQuestionAnswerText = Convert.ToString(chkbox11.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                              AIFormID = aiFormID,
                              AIPageName = PageName.ContributingFactor,
                              AISequence = 11
                          });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID12.Value)))
                {
                    objList.Add(
                          new AIQuestionAnswer
                          {
                              AIQuestionID = Convert.ToInt32(hdQuestionID12.Value),
                              AIQuestionType = QuestionType.BooleanType,
                              AIQuestionAnswerText = Convert.ToString(chkbox12.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                              AIFormID = aiFormID,
                              AIPageName = PageName.ContributingFactor,
                              AISequence = 12
                          });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID13.Value)))
                {
                    objList.Add(
                       new AIQuestionAnswer
                       {
                           AIQuestionID = Convert.ToInt32(hdQuestionID13.Value),
                           AIQuestionType = QuestionType.BooleanType,
                           AIQuestionAnswerText = Convert.ToString(chkbox13.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                           AIFormID = aiFormID,
                           AIPageName = PageName.ContributingFactor,
                           AISequence = 13
                       });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID14.Value)))
                {
                    objList.Add(
                      new AIQuestionAnswer
                      {
                          AIQuestionID = Convert.ToInt32(hdQuestionID14.Value),
                          AIQuestionType = QuestionType.BooleanType,
                          AIQuestionAnswerText = Convert.ToString(chkbox14.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                          AIFormID = aiFormID,
                          AIPageName = PageName.ContributingFactor,
                          AISequence = 14
                      });

                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID15.Value)))
                {

                    objList.Add(
                      new AIQuestionAnswer
                      {
                          AIQuestionID = Convert.ToInt32(hdQuestionID15.Value),
                          AIQuestionType = QuestionType.BooleanType,
                          AIQuestionAnswerText = Convert.ToString(chkbox15.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                          AIFormID = aiFormID,
                          AIPageName = PageName.ContributingFactor,
                          AISequence = 15
                      });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID16.Value)))
                {

                    objList.Add(
                      new AIQuestionAnswer
                      {
                          AIQuestionID = Convert.ToInt32(hdQuestionID16.Value),
                          AIQuestionType = QuestionType.BooleanType,
                          AIQuestionAnswerText = Convert.ToString(chkbox16.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                          AIFormID = aiFormID,
                          AIPageName = PageName.ContributingFactor,
                          AISequence = 16
                      });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID17.Value)))
                {

                    objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdQuestionID17.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(chkbox17.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                        AIFormID = aiFormID,
                        AIPageName = PageName.ContributingFactor,
                        AISequence = 17
                    });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID18.Value)))
                {

                    objList.Add(
                    new AIQuestionAnswer
                    {
                        AIQuestionID = Convert.ToInt32(hdQuestionID18.Value),
                        AIQuestionType = QuestionType.BooleanType,
                        AIQuestionAnswerText = Convert.ToString(chkbox18.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                        AIFormID = aiFormID,
                        AIPageName = PageName.ContributingFactor,
                        AISequence = 18
                    });
                }
                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID19.Value)))
                {
                    objList.Add(
                new AIQuestionAnswer
                {
                    AIQuestionID = Convert.ToInt32(hdQuestionID19.Value),
                    AIQuestionType = QuestionType.BooleanType,
                    AIQuestionAnswerText = Convert.ToString(chkbox19.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
                    AIFormID = aiFormID,
                    AIPageName = PageName.ContributingFactor,
                    AISequence = 19
                });
                }

                if (!string.IsNullOrEmpty(Convert.ToString(hdQuestionID20.Value)))
                {

                    objList.Add(
           new AIQuestionAnswer
           {
               AIQuestionID = Convert.ToInt32(hdQuestionID20.Value),
               AIQuestionType = QuestionType.BooleanType,
               AIQuestionAnswerText = Convert.ToString(chkbox20.Checked == true ? ConfirmationYesNo.Yes : ConfirmationYesNo.NotAnswered),
               AIFormID = aiFormID,
               AIPageName = PageName.ContributingFactor,
               AISequence = 20
           });
                }

                if (chkbox20.Checked && !string.IsNullOrEmpty(Convert.ToString(hdQuestionID21.Value)))
                {
                    objList.Add(
                        new AIQuestionAnswer
                {
                    AIQuestionID = Convert.ToInt32(hdQuestionID21.Value),
                    AIQuestionType = QuestionType.TextType,
                    AIQuestionAnswerText = Convert.ToString(txtother.Value),
                    AIFormID = aiFormID,
                    AIPageName = PageName.ContributingFactor,
                    AISequence = 21
                });

                }


                Session["AIReportQuestions"] = objList;

                managePreviousNext("");

                RedirectionLink();

                //Response.Redirect("AIActionAndPrevention.aspx", false);
            }
        }
        catch (Exception ex) { throw ex; }
        finally { }

    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        Response.Redirect("AIIncidentReason.aspx", false);
    }
    protected void btnSubmitandExitLater_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmitMyReport_Click(object sender, EventArgs e)
    {

    }
    private void managePreviousNext(string NextPageUrl)
    {
        List<ManagePagePreviousNext> objPage = new List<ManagePagePreviousNext>();
        try
        {
            objPage = (List<ManagePagePreviousNext>)Session["AIPagesList"];
            if (objPage != null)
            {
                var resultItem = objPage.Where(x => x.pageName == PageName.ContributingFactor).ToList();
                if (resultItem != null && resultItem.Count > 0)
                {
                    foreach (var removeItem in resultItem)
                    {
                        objPage.Remove(removeItem);
                    }
                }
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIContributingFactor.aspx", NextPageUrl = NextPageUrl, pageName = PageName.ContributingFactor, PreviousPageUrl = "" });
            }
            else
            {
                objPage = new List<ManagePagePreviousNext>();
                objPage.Add(new ManagePagePreviousNext { currentPageUrl = "AIContributingFactor.aspx", NextPageUrl = NextPageUrl, pageName = PageName.ContributingFactor, PreviousPageUrl = "" });
            }
            Session["AIPagesList"] = objPage;
        }
        catch { }
        finally { }
    }
    private void RedirectionLink()
    {
        string redirectURL = string.Empty;
        List<AIQuestionAnswer> objList = new List<AIQuestionAnswer>();
        try
        {
            objList = (List<AIQuestionAnswer>)Session["AIReportQuestions"];

            if (objList != null)
            {

                var incidentYES = objList.FirstOrDefault(x => x.AIPageName == PageName.Incident && x.AIQuestionAnswerText == "1");
                if (incidentYES != null)
                {
                    // Default for Incident Yes
                    Response.Redirect("AIActionAndPrevention.aspx", false);
                    return;
                }
                var IsLicensee = objList.FirstOrDefault(x => x.AIPageName == PageName.IsLogistic && x.AIQuestionAnswerText == "1" && x.AISequence == 1);
                if (IsLicensee != null)
                {

                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }

                    //Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }


                }
                else
                {

                    // Get result from all scenarios
                    var IsPerosnalInjury = objList.FirstOrDefault(x => x.AIPageName == PageName.PersonalInjury && x.AIQuestionAnswerText == "1");
                    var IsFirstAidBox = objList.FirstOrDefault(x => x.AIPageName == PageName.FirstAidBox && x.AIQuestionAnswerText == "1");
                    var IsHealthCare = objList.FirstOrDefault(x => x.AIPageName == PageName.HealthCareConfirmation && x.AIQuestionAnswerText == "1");
                    var IsLostTime = objList.FirstOrDefault(x => x.AIPageName == PageName.LostTime && x.AIQuestionAnswerText == "1");
                    var IsPropertyDamage = objList.FirstOrDefault(x => x.AIPageName == PageName.PropertyDamageQuestion && x.AIQuestionAnswerText == "1");

                    // Case 1:  Personal Injury AND First Aid AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 2 : Personal Injury AND Health Care AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 3 : Personal Injury AND First Aid AND Property Damage AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 4 : Personal Injury AND First Aid AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 5 : Personal Injury AND First Aid AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 6 : Personal Injury AND Health Care AND Property Damage
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 7 : Personal Injury AND First Aid AND Property Damage
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 8 : Personal Injury AND Health Care AND Lost Time
                    if (IsPerosnalInjury != null && IsHealthCare != null && IsLostTime != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 9 : Personal Injury AND First Aid AND Lost Time
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsLostTime != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 10: Personal Injury AND First Aid AND Health Care
                    if (IsPerosnalInjury != null && IsFirstAidBox != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 11: Personal Injury AND Health Care
                    if (IsPerosnalInjury != null && IsHealthCare != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 12: Property Damage AND Lost Time
                    if (IsLostTime != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 13: Personal Injury AND First Aid
                    if (IsPerosnalInjury != null && IsFirstAidBox != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }

                    //Case 17: Personal Injury AND  Property Damage
                    if (IsPerosnalInjury != null && IsPropertyDamage != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }

                    // Case 14: Property Damage
                    if (IsPropertyDamage != null)
                    {
                        Response.Redirect("AIGetPropertyDamageDetail.aspx", false);
                        return;
                    }
                    // Case 15: Lost Time
                    if (IsLostTime != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                    // Case 16: Personal Injury
                    if (IsPerosnalInjury != null)
                    {
                        Response.Redirect("AIPersonalProtectiveEquipment.aspx", false);
                        return;
                    }
                }
            }
        }
        catch { }
        finally { }
    }


}