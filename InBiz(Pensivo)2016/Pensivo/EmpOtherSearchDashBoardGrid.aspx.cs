﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;

public partial class EmpOtherSearchDashBoardGrid : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Role objRole = new Role();
        if (this.ReportOption != "")
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
        }
        //if (!IsPostBack && !IsPagePostBack(gvRoles))
        {
            HdnRoleID.Value = BusinessUtility.GetString(this.RoleID);
            HdnSearchBy.Value = BusinessUtility.GetString(this.SearchBy);
            HdnFlag.Value = BusinessUtility.GetString(this.Flag);
            HdnReportOption.Value = BusinessUtility.GetString(this.ReportOption);
            HdnActionID.Value = BusinessUtility.GetString(this.ActionID);
            if (this.Flag == "add" || this.ReportOption != "")
            {
                hrfNext.InnerText = Resources.Resource.lblNext;
                HdnSysRoleOperator.Value = this.SysRoleOperator;
            }
            else if (this.Flag == "remove")
            {
                hrfNext.InnerText = Resources.Resource.lblRemove;
            }

            if (this.SearchBy == EmpSearchBy.Store)
            {
                if (Utils.TrainingInst == (int)Institute.bdl)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblDeptID, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                    gvRoles.Columns[1].HeaderText = Resources.Resource.lblDeptID;
                    ltrTitle.Text = Resources.Resource.lblDeptID;
                    ltrSearchTitle.Text = Resources.Resource.lblFindDeptID;
                    ltrSearchMessage.Text = Resources.Resource.lblFindDeptIDMessage;
                    lblRoleName.Text = Resources.Resource.lblDeptID;
                    txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblDeptID);
                }
                else
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchSite, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                    gvRoles.Columns[1].HeaderText = Resources.Resource.lblSearchSite;
                    ltrTitle.Text = Resources.Resource.lblSearchSite;
                    ltrSearchTitle.Text = Resources.Resource.lblFindSite;
                    ltrSearchMessage.Text = Resources.Resource.lblFindSiteMessage;
                    lblRoleName.Text = Resources.Resource.lblSearchSite;
                    txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblSearchSite);
                }
            }
            else if (this.SearchBy == EmpSearchBy.JobCode)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchJobCode, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                gvRoles.Columns[1].HeaderText = Resources.Resource.lblSearchJobCode;
                ltrTitle.Text = Resources.Resource.lblSearchJobCode;
                ltrSearchTitle.Text = Resources.Resource.lblFindJobCode;
                ltrSearchMessage.Text = Resources.Resource.lblFindJobCodeMessage;
                lblRoleName.Text = Resources.Resource.lblSearchJobCode;
                txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblSearchJobCode);
            }
            else if (this.SearchBy == EmpSearchBy.District)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchDistrict, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                gvRoles.Columns[1].HeaderText = Resources.Resource.lblSearchDistrict;
                ltrTitle.Text = Resources.Resource.lblSearchDistrict;
                ltrSearchTitle.Text = Resources.Resource.lblFindDistrictCode;
                ltrSearchMessage.Text = Resources.Resource.lblFindDistrictCodeMessage;
                lblRoleName.Text = Resources.Resource.lblSearchDistrict;
                txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblPlaceHolderDistrict);
            }
            else if (this.SearchBy == EmpSearchBy.RegionTBS)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchRegion, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                gvRoles.Columns[1].HeaderText = Resources.Resource.lblSearchRegion;
                ltrTitle.Text = Resources.Resource.lblSearchRegion;
                ltrSearchTitle.Text = Resources.Resource.lblFindRegionCode;
                ltrSearchMessage.Text = Resources.Resource.lblFindRegionCodeMessage;
                lblRoleName.Text = Resources.Resource.lblSearchRegion;
                txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblPlaceHolderRegion);
            }
            else if (this.SearchBy == EmpSearchBy.DivisionTBS)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchDivision, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                gvRoles.Columns[1].HeaderText = Resources.Resource.lblSearchDivision;
                ltrTitle.Text = Resources.Resource.lblSearchDivision;
                ltrSearchTitle.Text = Resources.Resource.lblFindDivisionCode;
                ltrSearchMessage.Text = Resources.Resource.lblFindDivisionCodeMessage;
                lblRoleName.Text = Resources.Resource.lblSearchDivision;
                txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblPlaceHolderDivision);
            }

            else if (this.SearchBy == EmpSearchBy.Site)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchSite, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                gvRoles.Columns[1].HeaderText = Resources.Resource.lblSearchSite;
                ltrTitle.Text = Resources.Resource.lblSearchSite;
                ltrSearchTitle.Text = Resources.Resource.lblFindSite;
                ltrSearchMessage.Text = Resources.Resource.lblFindSiteMessage;
                lblRoleName.Text = Resources.Resource.lblSearchSite;
                txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblSearchSite);
            }
            else if (this.SearchBy == EmpSearchBy.Province)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblProvince, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                gvRoles.Columns[1].HeaderText = Resources.Resource.lblProvince;
                ltrTitle.Text = Resources.Resource.lblProvince;
                ltrSearchTitle.Text = Resources.Resource.lblFindProvince;
                ltrSearchMessage.Text = Resources.Resource.lblFindProvinceMessage;
                lblRoleName.Text = Resources.Resource.lblProvince;
                txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblProvince);
            }

            if (this.SysRoleOperator == SysRoleSelOperator.AND)
            {
                HdnIsAllowMultipleSelection.Value = "0";
            }
            else if (this.SysRoleOperator == SysRoleSelOperator.OR)
            {
                HdnIsAllowMultipleSelection.Value = "1";
            }

            
            string roleName = "";
            string functionalityName = "";
            if (this.RoleID > 0)
            {
                objRole = new Role();
                roleName = objRole.GetRoleName(this.RoleID);
            }


            if (this.SearchBy == EmpRefCode.Store)
            {
                if (Utils.TrainingInst == (int)Institute.bdl)
                {
                    functionalityName = Resources.Resource.lblDeptID;
                }
                else
                {
                    functionalityName = Resources.Resource.lblSearchSite;
                }
            }
            else if (this.SearchBy == EmpRefCode.Region)
            {
                functionalityName = Resources.Resource.lblSearchRegion;
            }
            else if (this.SearchBy == EmpRefCode.JobCode)
            {
                functionalityName = Resources.Resource.lblSearchJobCode;
            }
            else if (this.SearchBy == EmpRefCode.Division)
            {
                functionalityName = Resources.Resource.lblSearchLocation;
            }

            else if (this.SearchBy == EmpRefCode.DivisionTBS)
            {
                functionalityName = Resources.Resource.lblSearchDivision;
            }
            else if (this.SearchBy == EmpRefCode.RegionTBS)
            {
                functionalityName = Resources.Resource.lblSearchRegion;
            }
            else if (this.SearchBy == EmpRefCode.District)
            {
                functionalityName = Resources.Resource.lblSearchDistrict;
            }
            else if (this.SearchBy == EmpRefCode.Site)
            {
                    functionalityName = Resources.Resource.lblSearchSite;
            }
            else if (this.SearchBy == EmpRefCode.Province)
            {
                functionalityName = Resources.Resource.lblProvince;
            }

            if (this.Flag == "add")
            {
                HdnMessage.Value = Resources.Resource.lblRoleSysRefAdd.Replace("#SYSREFNAME#", functionalityName).Replace("#ROLENAME#", roleName);
            }
            else if (this.Flag == "remove")
            {
                HdnMessage.Value = Resources.Resource.lblRoleSysRefRemove.Replace("#SYSREFNAME#", functionalityName).Replace("#ROLENAME#", roleName);
            }

        }



        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addActionRoleSysRefAssociation")
        {
            try
            {
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                string sysRoleOperator = BusinessUtility.GetString(Request.Form["SysRoleOperator"]);
                if (searchBy != "" && roleID > 0 && searchValue != "")
                {
                    objRole = new Role();
                    if (objRole.RoleAddEmpRef(roleID, searchBy, searchValue, sysRoleOperator) == true)
                    {
                        ExecutingThread.ThreadEmployeeAssignedCoursesByRole(roleID, 0, (int)AssignedCourseInActiveReason.BySysAdminRole, (int)AssignedCourseUpdatedSource.BySysAdminRoleUpdate);

                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch(Exception ex)
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
        else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeActionRoleSysRefAssociation")
        {
            try
            {
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                if (searchBy != "" && roleID > 0 && searchValue != "")
                {
                    objRole = new Role();
                    if (objRole.RoleRemoveEmpRef(roleID, searchBy, searchValue) == true)
                    {
                        //ExecutingThread.ThreadRemoveEmployeeCoursesByRole(roleID, 0);
                        ExecutingThread.ThreadEmployeeAssignedCoursesByRole(roleID, 0, (int)AssignedCourseInActiveReason.BySysAdminRole, (int)AssignedCourseUpdatedSource.BySysAdminRoleUpdate);
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }


        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addActionSysRefInReportUserManagment")
        {
            try
            {
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                if (searchBy != "" && roleID > 0 && searchValue != "" && actionID > 0)
                {
                    objRole = new Role();
                    if (objRole.AddSysRefInReportUserManagment(roleID, searchBy, searchValue, actionID) == true)
                    {
                        //ExecutingThread.ThreadEmployeeAssignedCoursesByRole(roleID, 0);
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
        else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeActionSysRefInReportUserManagment")
        {
            try
            {
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                if (searchBy != "" && roleID > 0 && searchValue != "" && actionID > 0)
                {
                    objRole = new Role();
                    if (objRole.RemoveSysRefInReportUserManagment(roleID, searchBy, searchValue, actionID) == true)
                    {
                        //ExecutingThread.ThreadEmployeeAssignedCoursesByRole(roleID, 0);
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }


    }

    /// <summary>
    /// To Bind JQ Grid With Data Source Differtent System Refference value Associated with role or not 
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtRole = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        Role objRole = new Role();
        if (this.ReportOption != "")
        {
            DataTable dt = objRole.GetSysRef(Utils.ReplaceDBSpecialCharacter(this.SearchBy), Utils.ReplaceDBSpecialCharacter(txtRole), Globals.CurrentAppLanguageCode);
            gvRoles.DataSource = dt;
            gvRoles.DataBind();
        }
        else if (this.ActionID > 0)
        {
            if (this.Flag == "add")
            {
                DataTable dt = objRole.GetSysRefNotInReportUserManagment(this.RoleID, Utils.ReplaceDBSpecialCharacter(this.SearchBy), this.ActionID, Utils.ReplaceDBSpecialCharacter(txtRole));
                gvRoles.DataSource = dt;
                gvRoles.DataBind();
            }
            else if (this.Flag == "remove")
            {
                DataTable dt = objRole.GetSysRefInReportUserManagment(this.RoleID, Utils.ReplaceDBSpecialCharacter(this.SearchBy), this.ActionID, Utils.ReplaceDBSpecialCharacter(txtRole));
                gvRoles.DataSource = dt;
                gvRoles.DataBind();
            }
        }
        else
        {
            if (this.Flag == "add")
            {
                DataTable dt = objRole.GetSysRefNotInRole(this.RoleID, Utils.ReplaceDBSpecialCharacter(this.SearchBy), Utils.ReplaceDBSpecialCharacter(txtRole));
                gvRoles.DataSource = dt;
                gvRoles.DataBind();
            }
            else if (this.Flag == "remove")
            {
                DataTable dt = objRole.GetSysRefInRole(this.RoleID, Utils.ReplaceDBSpecialCharacter(this.SearchBy), Utils.ReplaceDBSpecialCharacter(txtRole));
                gvRoles.DataSource = dt;
                gvRoles.DataBind();
            }
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search By Value 
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Report Option
    /// </summary>
    public string ReportOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }

    /// <summary>
    /// To Get Sys Role Operator AND/OR
    /// </summary>
    public string SysRoleOperator
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["operator"]);
        }
    }

}