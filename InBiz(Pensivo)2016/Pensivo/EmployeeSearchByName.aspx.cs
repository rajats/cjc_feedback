﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Text;


public partial class EmployeeSearchByName : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ReportOption != "")
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
        }

        if (!IsPostBack)
        {
            if (this.SearchBy == EmpSearchBy.EmpCode)
            {
                if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
                {
                    if (isManageListEmpSearch == true)
                    {
                        lblTitle.Text = Resources.Resource.lblManageListTDCSearchByEmpEmailID;
                    }
                    else
                    {
                        lblTitle.Text = Resources.Resource.lblSearchByEmpEmailID;
                    }
                    txtSearchName.Attributes.Add("placeholder", Resources.Resource.lblEmpEmailID);
                    rfSearchName.Text = Resources.Resource.reqName;
                    rfSearchName.Text = Resources.Resource.reqEmpEmailID;
                    lblName.Text = Resources.Resource.lblEmpEmailID;
                    htmLblName.InnerHtml = Resources.Resource.lblEmpEmailID;
                }
                else if (Utils.TrainingInst == (int)Institute.bdl)
                {
                    if (isManageListEmpSearch == true)
                    {
                        lblTitle.Text = Resources.Resource.lblManageListBDLSearchByEmpCode;
                    }
                    else
                    {
                        lblTitle.Text = Resources.Resource.lblBDLSearchByEmpCode;
                    }
                    txtSearchName.Attributes.Add("placeholder", Resources.Resource.lblBDLUserName);
                    rfSearchName.Text = Resources.Resource.reqBDLEmpCode;
                    lblName.Text = Resources.Resource.lblBDLUserName;
                    htmLblName.InnerHtml = Resources.Resource.lblBDLUserName;
                }
                else if (Utils.TrainingInst == (int)Institute.EDE2)
                {
                    if (isManageListEmpSearch == true)
                    {
                        lblTitle.Text = Resources.Resource.lblManageListBDLSearchByEmpCode;
                    }
                    else
                    {
                        lblTitle.Text = Resources.Resource.lblBDLSearchByEmpCode;
                    }
                    txtSearchName.Attributes.Add("placeholder", Resources.Resource.lblEDE2UserName);
                    rfSearchName.Text = Resources.Resource.reqEDE2EmpCode;
                    lblName.Text = Resources.Resource.lblEDE2UserName;
                    htmLblName.InnerHtml = Resources.Resource.lblEDE2UserName;
                }
                else
                {
                    if (isManageListEmpSearch == true)
                    {
                        lblTitle.Text = Resources.Resource.lblManageListSearchByEmpCode;
                    }
                    else
                    {
                        lblTitle.Text = Resources.Resource.lblSearchByEmpCode;
                    }
                    txtSearchName.Attributes.Add("placeholder", Resources.Resource.lblEmpCode);
                    rfSearchName.Text = Resources.Resource.reqEmpCode;
                    lblName.Text = Resources.Resource.lblEmpCode;
                    htmLblName.InnerHtml = Resources.Resource.lblEmpCode;
                }
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblEmpoyeeID, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, BusinessUtility.GetString(Request.UrlReferrer));
            }
            else
            {
                if (isManageListEmpSearch == true)
                {
                    lblTitle.Text = Resources.Resource.lblManageListSearchByEmpName;
                }
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchName, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, BusinessUtility.GetString(Request.UrlReferrer));
            }
        }
    }

    /// <summary>
    /// To Search Employee By Name and EmpCode and Move to Next Page
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnSearchByName_OnClick(object sender, EventArgs e)
    {
        if (this.isManageListEmpSearch == true)
        {
            Response.Redirect("ManageListAddUser.aspx?managelistempsearch=" + "1" + "&searchby=" + this.SearchBy + "&searchval=" + Server.UrlEncode(txtSearchName.Text) + "&siteID=" + this.ManageListSiteID + "&roleManageListID=" + this.RoleManageListID + "&FunctionalityID=" + this.ActionID + "&ListID=" + this.ListID + "", true);
        }

        if (this.ReportOption != "")
        {
            Response.Redirect("ReportFilterSummary.aspx?roption=" + this.ReportOption + "&searchby=" + this.SearchBy + "&searchval=" + Server.UrlEncode(txtSearchName.Text));
        }
        if (this.ActionID == 0)
        {
            if (this.Flag == "remove")
            {
                Response.Redirect("RoleRemoveUser.aspx?roleID=" + this.RoleID + "&searchby=" + this.SearchBy + "&searchval=" + Server.UrlEncode(txtSearchName.Text) + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "");
            }
            else
            {
                Response.Redirect("RoleAddUser.aspx?roleID=" + this.RoleID + "&searchby=" + this.SearchBy + "&searchval=" + Server.UrlEncode(txtSearchName.Text) + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "");
            }
        }
        else if (this.ActionID > 0)
        {
            Response.Redirect("FunctionalityAddRemoveUser.aspx?roleID=" + this.RoleID + "&searchby=" + this.SearchBy + "&searchval=" + Server.UrlEncode(txtSearchName.Text) + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "");
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Report Option
    /// </summary>
    public string ReportOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }


    /// <summary>
    /// To Get Is Manage List Emp Search
    /// </summary>
    public Boolean isManageListEmpSearch
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["managelistempsearch"]) == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// To Get Manage List Site ID
    /// </summary>
    public string ManageListSiteID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["siteID"]);
        }
    }

    /// <summary>
    /// To Get Role Manage List ID
    /// </summary>
    public string RoleManageListID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roleManageListID"]);
        }
    }

    /// <summary>
    /// To Get List ID
    /// </summary>
    public int ListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["ListID"]);
        }
    }
}