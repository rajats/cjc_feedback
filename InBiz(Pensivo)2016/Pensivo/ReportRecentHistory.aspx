﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ReportRecentHistory.aspx.cs" Inherits="ReportRecentHistory" %>
<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
        <%else if (Utils.TrainingInst == (int)Institute.EDE2)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>

    <%--Define Inline Css Set JQ Grid Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>
    <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>

     <%--Define Inline Css to Wrap Grid Header and Row Data--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th div {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
        }

        .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
            vertical-align: text-top;
        }
    </style>

    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <h4>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h4>
            <div id="grid_wrapper" style="width: 100%;" onkeypress="return disableEnterKey(event)">
                <trirand:JQGrid runat="server" ID="gvReportRecentHistory" Height="300px"
                    AutoWidth="True" OnCellBinding="gvReportRecentHistory_CellBinding" OnDataRequesting="gvReportRecentHistory_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="idempreportfilter" Visible="false" PrimaryKey="True" />
                        <trirand:JQGridColumn DataField="createdDateTime" HeaderText="<%$ Resources:Resource, lblRunDate %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="reporttype" HeaderText="<%$ Resources:Resource, BtnDetails %>" TextAlign="Center" Width="100"
                            Editable="false" />
                    </Columns>
                    <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="loadComplete" />
                </trirand:JQGrid>
            </div>
            <asp:HiddenField ID="hdnReportFilter" runat="server" />
            <asp:HiddenField ID="hdnEmpID" runat="server" />
            <asp:HiddenField ID="hdnReportType" runat="server" />
            <asp:HiddenField ID="hdnReportID" runat="server" />
            <asp:HiddenField ID="hdnMessageText" runat="server" />
        </div>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvReportRecentHistory.ClientID%>");
        $grid.initGridHelper({
            gridWrapPanleID: "grid_wrapper"
        });

        // Resized JQ Grid
        function jqGridResize() {
            $("#<%=gvReportRecentHistory.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // Define JQ Grid Load Complete
        function loadComplete(data) {
            jqGridResize();
        }

        // Show Saved Report Summary Detail
        function ShowReportDetails(reportID) {
            if (reportID > 0) {
                $.ajax(
                {
                    type: "POST",
                    url: "ReportRecentHistory.aspx/GetReportSummary",
                    data: "{reportID:'" + reportID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function
                    (msg) {
                        var title = "<%=Resources.Resource.BtnReportDetails%>";
                        var messageText = msg.d;
                        okButtonText = "<%=Resources.Resource.BtnRunAgain%>";
                        LaterCnclButtonText = "<%=Resources.Resource.btnClose%>";
                        okButtonRedirectlink = "RunReportAgain(" + reportID + ");";
                        LaterCnclButtonRedirectLink = "ConfirmReportCancel();";
                        PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);

                    },
                    error: function (x, e) {
                    }
                }
                );
            }
        }

        // Cancel Saved Report Summary Detail 
        function ConfirmReportCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
        }

        // Run Report Again with Summary Report Detail
        function RunReportAgain(reportID) {
            window.location.href = "Report.aspx?repID=" + reportID;
        }
    </script>
</asp:Content>
