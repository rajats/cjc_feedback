﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;

public partial class ConfirmAnswer : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        dvLogInErrMsg.Visible = false;
        if (!IsPostBack)
        {
            Employee objEmp = new Employee();
            ddlQuestion.DataSource = objEmp.GetEmployeeSelectedQuestionList(this.EmpID, Globals.CurrentAppLanguageCode);
            ddlQuestion.DataTextField = "question";
            ddlQuestion.DataValueField = "idemployeequestions";
            ddlQuestion.DataBind();
            ddlQuestion.Items.Insert(0, new ListItem(Resources.Resource.lblPleaseSelectSecurityQuestion, ""));

            if (ddlQuestion.Items.Count <= 1)
            {
                GlobalMessage.showAlertMessage(Resources.Resource.lblNoQuestionSelectedContactAdministrator, "public.aspx");
                return;
            }
            else if (ddlQuestion.Items.Count == 3)
            {
                ddlQuestion.Items.Remove(ddlQuestion.Items[2]);
            }
            else if (ddlQuestion.Items.Count == 2)
            {
                ddlQuestion.SelectedIndex = 1;
            }
            txtAnswer.Focus();
        }
    }

    /// <summary>
    /// To Validate User Question Answer and Move to Next Page
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void BtnConfirmAnswer_OnClick(object sender, EventArgs e)
    {
        if (ValidatePage() == true)
        {
            Employee objEmp = new Employee();
            if (objEmp.ValidadateEmployeeAnswer(this.EmpID, BusinessUtility.GetInt(ddlQuestion.SelectedItem.Value), Utils.ReplaceDBSpecialCharacter(txtAnswer.Text)))
            {
                Response.Redirect("ChangePassword.aspx?uid=" + BusinessUtility.GetString(this.EmpID));
            }
            else
            {
                DataTable dt = objEmp.GetEmployeeSelectedQuestionList(this.EmpID, Globals.CurrentAppLanguageCode);
                if (dt.Rows.Count == 1)
                {
                    GlobalMessage.showAlertMessage(Resources.Resource.msgInvalidAnswer);
                }
                else
                {
                    Response.Redirect("ConfirmOtherAnswer.aspx?uid=" + BusinessUtility.GetString(this.EmpID) + "&QID=" + ddlQuestion.SelectedItem.Value);
                }
            }
        }
    }

    /// <summary>
    /// To Get Employee ID
    /// </summary>
    private int EmpID { get { return BusinessUtility.GetInt(Request.QueryString["uid"]); } }

    /// <summary>
    /// To Show Message
    /// </summary>
    /// <param name="message">Pass Message</param>
    private void showMessage(string message)
    {
        lblErrMsg.Text = message;
        dvLogInErrMsg.Visible = true;
    }

    /// <summary>
    /// To Validate Page
    /// </summary>
    /// <returns></returns>
    private Boolean ValidatePage()
    {
        if (ddlQuestion.SelectedItem.Value == "")
        {
            GlobalMessage.showAlertMessage(Resources.Resource.reqMsgSecretQuestion);
            ddlQuestion.Focus();
            return false;
        }
        else if (txtAnswer.Text == "")
        {
            GlobalMessage.showAlertMessage(Resources.Resource.reqMsgRequiredAnswer);
            txtAnswer.Focus();
            return false;
        }
        return true;
    }

}