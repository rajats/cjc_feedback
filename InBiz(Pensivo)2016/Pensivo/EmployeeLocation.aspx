﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeeLocation.aspx.cs" Inherits="EmployeeLocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <header class="main-content-header">
                <h2>
                    <%= Resources.Resource.lblMyCourses %>
                </h2>
                <h5>
                    <%= Resources.Resource.lblBeforwegetStarted %>
                </h5>
                <h6>
                    <%= Resources.Resource.lblEmpLocationMessage %>
                </h6>
            </header>
            <%=htmlText %>
        </div>
        <asp:HiddenField ID="hdnSearchBy" runat="server" />
        <asp:HiddenField ID="hdnUserID" runat="server" />
        <asp:HiddenField ID="hdnNextSearchBy" runat="server" />
        <asp:HiddenField ID="hdnQuery" runat="server" />
        <asp:HiddenField ID="hdnAskLocation" runat="server" />
        <asp:HiddenField ID="hdnEmpUUID" runat="server" />
        <asp:HiddenField ID="hdnHomeURL" runat="server" />
    </section>
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Save Location
        function Savelocation(svalue) {
            if ($('#<%=hdnAskLocation.ClientID%>').val() == "1") {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "addLocation";
                datatoPost.SearchBy = $('#<%=hdnSearchBy.ClientID%>').val();
                datatoPost.SearchValue = svalue;
                datatoPost.EmpID = $('#<%=hdnUserID.ClientID%>').val();
                datatoPost.EmpUUID = $('#<%=hdnEmpUUID.ClientID%>').val();

                $.post("EmployeeLocation.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        window.location.href = $('#<%=hdnHomeURL.ClientID%>').val();
                    }
                    else {
                        ShowPensivoMessage("<%=Resources.Resource.lblContactAdministrator%>");
                    }
                });
            }
            else {
                window.location.href = "EmployeeType.aspx" + $("#<%=hdnQuery.ClientID%>").val() + "&searchbyvalue=" + svalue + "&searchbynew=" + $("#<%=hdnNextSearchBy.ClientID%>").val();
            }
        }
    </script>
</asp:Content>

