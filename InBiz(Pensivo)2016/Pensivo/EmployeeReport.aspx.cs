﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;

public partial class EmployeeReport : BasePage
{
    /// <summary>
    /// To Hold Html Content
    /// </summary>
    protected string htmlText = "";

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Reporting);
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            Breadcrumbs.BreadcrumbsSetRootMenu();
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblReporting, Path.GetFileName(Request.Url.AbsolutePath), Path.GetFileName(Request.Url.AbsolutePath));
            htmlText = loadEmpSearchValue();
            Session["ReportMultiFilter"] = null;
        }
    }

    /// <summary>
    /// To Get HTML Content For Employee sysReference Reporting
    /// </summary>
    /// <returns>String</returns>
    private string loadEmpSearchValue()
    {
        StringBuilder sbHtml = new StringBuilder();
        //Role objRole = new Role();
        Employee objEmployee = new Employee();
        DataTable dt = objEmployee.GetEmployeeSites(CurrentEmployee.EmpID);

        if (dt.Rows.Count > 1)
        {
            dt.Columns.Add("Desc", typeof(string));
            dt.AcceptChanges();


            DataRow dRow = dt.NewRow();
            dRow[0] = BusinessUtility.GetString("ALL");
            dRow[1] = BusinessUtility.GetString("ALL");
            dt.Rows.InsertAt(dRow, 0);
            dt.AcceptChanges();
        }

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sbHtml.Append(" <div class='boxset' id='dvAdminDashBoard" + BusinessUtility.GetString(i) + "'> ");
                for (int j = 0; j <= 2; j++)
                {
                    if (i < dt.Rows.Count)
                    {
                        string sText = "";
                        string sFunction = "";
                        string sSite = "";

                        if (BusinessUtility.GetString(dt.Rows[i]["SiteNo"]) != "ALL")
                        {
                            sSite = BusinessUtility.GetString(dt.Rows[i]["SiteNo"]).PadLeft(4, '0');
                            sText = Resources.Resource.lblSiteInformationFor + " " + sSite;
                            sFunction = "href='#'  onclick='ShowReport(\"" + BusinessUtility.GetString(sSite).ToUpper() + "\");'";
                        }
                        else
                        {
                            sText = Resources.Resource.lblInformationForAllMySites;
                            sFunction = "href='#'  onclick='ShowAllReport();'";
                        }

                        sbHtml.Append(" <div class='boxset-box'> ");
                        sbHtml.Append(" <a id='hrfSearchEmpName' runat='server' " + sFunction + "  class='boxset-box-wrapper'> ");
                        sbHtml.Append(" <div class='boxset-box-inner'> ");
                        sbHtml.Append(" <h4 class='boxset-title'> " + sText + " </h4> ");
                        sbHtml.Append(" </div> ");
                        sbHtml.Append(" </a> ");
                        sbHtml.Append(" </div> ");
                        if (j <= 1)
                        {
                            i += 1;
                        }
                    }
                }
                sbHtml.Append(" </div> ");
                //i += 1;
            }
        }
        //else 
        //{
        //    sbHtml+= "  <table style='width: 100%'> ";
        //    sbHtml+= "  <tr> ";
        //    sbHtml+= "  <td>" + Resources.Resource.lblSiteNotAssigned + " </td> ";
        //    sbHtml+= "  </tr> ";
        //    sbHtml+= "  </table> ";
        //}

        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// To Show Report 
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnShowReport_Click(object sender, EventArgs e)
    {
        ReportFilter objReportFilter = new ReportFilter();
        string reportType = "";
        if (Utils.TrainingInst == (int)Institute.bdl)
        {
            reportType = BusinessUtility.GetString(EmpRefCode.Site).ToUpper();
        }
        else
        {
            reportType = BusinessUtility.GetString(EmpRefCode.Store).ToUpper();
        }
        objReportFilter.CreateNewFilter(ReportOption.Who, reportType, BusinessUtility.GetString(hdnSiteNo.Value), "0");
        Response.Redirect("ReportProgress.aspx");
    }




    /// <summary>
    /// To Show All Site Report 
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnShowAllReport_Click(object sender, EventArgs e)
    {
        ReportFilter objReportFilter = new ReportFilter();
        Employee objEmployee = new Employee();
        DataTable dt = objEmployee.GetEmployeeSites(CurrentEmployee.EmpID);
        if (dt.Rows.Count > 1)
        {
            dt.Columns.Add("Desc", typeof(string));
            dt.AcceptChanges();

            DataRow dRow = dt.NewRow();
            dRow[0] = 0;
            dRow[1] = BusinessUtility.GetString("ALL");
            dt.Rows.InsertAt(dRow, 0);
            dt.AcceptChanges();
        }

        string sSearchValue = "";
        string reportType = "";
        foreach (DataRow dRow in dt.Rows)
        {
            if ( BusinessUtility.GetString(dRow["SiteNo"]) != "0")
            {
                if (sSearchValue != "")
                {
                    sSearchValue += "," + BusinessUtility.GetString(dRow["SiteNo"]);
                }
                else
                {
                    sSearchValue += BusinessUtility.GetString(dRow["SiteNo"]);
                }
            }
        }

        if (Utils.TrainingInst == (int)Institute.bdl)
        {
            reportType = BusinessUtility.GetString(EmpRefCode.Site).ToUpper();
        }
        else
        {
            reportType = BusinessUtility.GetString(EmpRefCode.Store).ToUpper();
        }

        objReportFilter.CreateNewFilter(ReportOption.Who, reportType, sSearchValue, "0");
        Response.Redirect("ReportProgress.aspx");
    }
}