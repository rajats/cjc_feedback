﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTECH.Library.Utilities;
using iTECH.Pensivo.BusinessLogic;

public partial class EmployeeDashBoard : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (BusinessUtility.GetString(Request.QueryString["TA"]) != "Y")
        //{
        //    DateTime dtCurrentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        //    DateTime dtStartDate = new DateTime(2015, 12, 31);
        //    DateTime dtEndDate = new DateTime(2016, 01, 03);
        //    int i = 0;
        //    int LastBound = (dtEndDate - dtStartDate).Days;
        //    for (i = 0; i <= LastBound; i++)
        //    {
        //        if ((dtCurrentDate >= dtStartDate) && (dtCurrentDate <= dtEndDate))
        //        {
        //            Response.Redirect("TrainingNotAvailable.aspx");
        //        }
        //        dtStartDate.AddDays(1);
        //    }
        //}

        if (!IsPostBack)
        {
            if (BusinessUtility.GetString(Request.QueryString["returnfromoldPIB"]) == "1")
            {
                EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.ReturnToPibFromOldSystem,
                    BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), "", 0, (int)RusticRegStatusCode.None, "");
            }

            if (Breadcrumbs.BreadcrumbsHasMenu() == false)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                if (Request.QueryString["error"] != "")
                {
                    Breadcrumbs.BreadcrumbsSetRootMenu();
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblHome, Path.GetFileName(Request.Url.AbsolutePath), Path.GetFileName(Request.Url.AbsolutePath));
                }
                else
                {
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblHome, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);
                }
            }
            else
            {
                Breadcrumbs.BreadcrumbsSetRootMenu();
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.GetBreadcrumbsList("EmployeeDashboard.aspx");
                if (CurrentEmployee.EmpType == "S")
                {
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblMyTraining, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);
                }
            }
        }
    }

}