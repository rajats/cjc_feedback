﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleTrainingEvent.aspx.cs" Inherits="RoleTrainingEvent" %>

<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.EDE2)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>

    <%--Define Inline Css to Wrap Grid Header and Row Data--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th div {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
        }

        .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
            vertical-align: text-top;
        }
    </style>
    <section id="main-content" class="pg-advanced-list">
        <div class="wrapper">
            <h1>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h1>
            <aside class="column span-4 fixed-onscroll">
                <div class="search-box" style="width: 384px; left: 216px;">
                    <header class="search-box-header">
                        <h2 class="h6"><%=Resources.Resource.lblFindTrainingEvent %></h2>
                        <p class="last-child"><%=Resources.Resource.lblFindTrainingEventMessage %></p>
                    </header>
                    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
                        <div class="search-box-body">
                            <div class="plms-input-group">
                                <div class="plms-fieldset">
                                    <asp:Label ID="Label2" AssociatedControlID="txtRoleName" class="filter-key plms-label is-hidden no-height" runat="server"
                                        Text="<%$ Resources:Resource, lblFindAssignTrainingEvents %>" for="name"></asp:Label>
                                    <asp:TextBox ID="txtRoleName" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblFindAssignTrainingEvents %>">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <input id="btnSearch" class="btn fluid clearfix" type="button" style="width: 100%;" value="<%=Resources.Resource.lbSearch%>" />
                        </div>
                    </asp:Panel>
                </div>
            </aside>
            <div class="main-content-body column span-8">
                <div id="grid_wrapper" style="width: 100%;" onkeypress="return disableEnterKey(event)">
                    <trirand:JQGrid runat="server" ID="gvRoles" Height="300px" MultiSelect="true"
                        AutoWidth="True" OnDataRequesting="gvRoles_DataRequesting">
                        <Columns>
                            <trirand:JQGridColumn DataField="eventID" Visible="false" PrimaryKey="True" />
                            <trirand:JQGridColumn DataField="EventTitle" HeaderText="<%$ Resources:Resource, lblTrainingEventName %>"
                                Editable="false" Width="500" />
                            <trirand:JQGridColumn DataField="eventID" HeaderText="<%$ Resources:Resource, lblTrainingEventID %>"
                                Editable="false" Width="100" />
                            <trirand:JQGridColumn DataField="EventType" HeaderText="<%$ Resources:Resource, lblTrainingEventType %>" TextAlign="Center"
                                Editable="false" Width="100" />
                        </Columns>
                        <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn=""></SortSettings>
                        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                        <ClientSideEvents LoadComplete="loadComplete" RowSelect="updateIdsOfSelectedRows" BeforeRowSelect="beforeSelectRow" />
                    </trirand:JQGrid>
                </div>
                <footer class="form-footer">
                    <div class="btngrp align-r">
                        <a id="hrfNext" runat="server" href="#nogo" class="btn round" onclick="getSelectedTrainingEvent();"><%=Resources.Resource.lblNext %></a>
                    </div>
                </footer>
                <asp:HiddenField ID="HdnRoleID" runat="server" />
                <asp:HiddenField ID="HdnSearchBy" runat="server" />
                <asp:HiddenField ID="HdnFlag" runat="server" />
                <asp:HiddenField ID="HdnActionID" runat="server" />
                <asp:HiddenField ID="HdnFunctionalityType" runat="server" />
                <asp:HiddenField ID="HdnCreatedBy" runat="server" />
                <asp:HiddenField ID="HdnMessage" runat="server" />
            </div>
        </div>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvRoles.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        // Resized JQ Grid
        function jqGridResize() {
            $("#<%=gvRoles.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // Define JQ Grid Load Complete
        function loadComplete(data) {
            $("#cb_ContentPlaceHolder1_gvRoles").hide();
            jqGridResize();
            for (i = 0, count = idsOfSelectedRows.length; i < count; i++) {
                $("#<%=gvRoles.ClientID%>").jqGrid('setSelection', idsOfSelectedRows[i], false);
            }
        }

        // To Hold JQ Grid Selected Item
        $grid = $("#<%=gvRoles.ClientID%>"),
        idsOfSelectedRows = [],
        updateIdsOfSelectedRows = function (id, isSelected) {
            var index = $.inArray(id, idsOfSelectedRows);
            if (!isSelected && index >= 0) {
                idsOfSelectedRows.splice(index, 1);
            } else if (index < 0) {
                idsOfSelectedRows.push(id);
            }
        };

        // To Get Selected
        function beforeSelectRow(id) {
            return (true);
        }

        // To Add Selected Training Event
        function getSelectedTrainingEvent() {
            var arrRooms = $grid.getGridParam('selarrrow');
            if (idsOfSelectedRows.length <= 0) {
                ShowPensivoMessage("<%=Resources.Resource.reqPleaseSelectAvalue%>");
                return false;
            }
            else {
                $.ajax(
                {
                    type: "POST",
                    url: "RoleTrainingEvent.aspx/GetEventName",
                    data: "{eventID:'" + idsOfSelectedRows.join(",") + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function 
                    (msg) {
                        var title = "<%=Resources.Resource.lblConfirmation%>";
                        var messageText = ($("#<%=HdnMessage.ClientID%>").val()).replace("#EVENTNAME#", msg.d);
                        okButtonText = "OK";
                        LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
                        okButtonRedirectlink = "TriggerAddRoleEvent('" + idsOfSelectedRows.join(",") + "');";
                        LaterCnclButtonRedirectLink = " HideConfirmationDialog();";
                        PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);

                    },
                    error: function (x, e) {
                    }
                }
                );
            }
        }

        // To Trigger Add Role Event OK Button
        function TriggerAddRoleEvent(searchValue) {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                datatoPost.callBack = "addAction";
            }
            else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                datatoPost.callBack = "removeAction";
            }
            datatoPost.SearchBy = $("#<%=HdnSearchBy.ClientID%>").val();
            datatoPost.SearchValue = searchValue;
            datatoPost.RoleID = $("#<%=HdnRoleID.ClientID%>").val();
            datatoPost.ActionID = $("#<%=HdnActionID.ClientID%>").val();
            datatoPost.FunctionalityType = $("#<%=HdnFunctionalityType.ClientID%>").val();
            datatoPost.createdBy = $("#<%=HdnCreatedBy.ClientID%>").val();
            $.post("RoleTrainingEventConfirmation.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                        window.location.href = "roleActivity.aspx?roleID=" + $("#<%=HdnRoleID.ClientID%>").val();
                    }
                    else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                        window.location.href = "roleActivity.aspx?roleID=" + $("#<%=HdnRoleID.ClientID%>").val();
                    }
            }
            else {
                if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                        ShowPensivoMessage("<%=Resources.Resource.msgTrainingEventNotAddedToRole%>")
                }
                else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                    ShowPensivoMessage("<%=Resources.Resource.msgTrainingNotRemovedToRole%>")
                }
        }
            });
}


$(document).ready(function () {
    $("#<%=txtRoleName.ClientID%>").focus();
    });
    </script>
</asp:Content>



