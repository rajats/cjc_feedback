﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTECH.Library.Utilities;
using iTECH.Pensivo.BusinessLogic;

public partial class EmployeeSiteMangerDashboard : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Manage_List);
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblManageListDashBoard, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            if (ListID == 5)
            {
                hrfAddNewSiteManager.HRef = "EmployeeAddDistrict.aspx?managelistempsearch=1&empID=" + this.EmpID + "&roleManageListID=" + this.RoleManageListID + "&SearchBy=" + this.SearchBy + "&FunctionalityID=" + this.FunctionalityID + "&ListID=" + this.ListID;
                hrfEditSiteManager.HRef = "EmployeeRemoveDistrict.aspx?roleManageListID=" + this.RoleManageListID + "&empID=" + this.EmpID + "&SearchBy=" + this.SearchBy + "&FunctionalityID=" + this.FunctionalityID + "&ListID=" + this.ListID;
            }
            else
            {
                hrfAddNewSiteManager.HRef = "EmpSearchDashBoard.aspx?managelistempsearch=1&siteID=" + this.SiteID + "&roleManageListID=" + this.RoleManageListID + "&SearchBy=" + this.SearchBy + "&actionID=" + this.FunctionalityID + "&ListID=" + this.ListID;
                hrfEditSiteManager.HRef = "EmployeeSiteManagerEdit.aspx?roleManageListID=" + this.RoleManageListID + "&siteID=" + this.SiteID + "&SearchBy=" + this.SearchBy + "&FunctionalityID=" + this.FunctionalityID + "&ListID=" + this.ListID;
            }
        }
    }


    /// <summary>
    /// To Get Role Manage List ID
    /// </summary>
    public int RoleManageListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleManageListID"]);
        }
    }


    /// <summary>
    /// To Get Site ID
    /// </summary>
    public int SiteID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["siteID"]);
        }
    }


    public int EmpID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["empID"]);
        }
    }

    /// <summary>
    /// To Get SearchBy
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["SearchBy"]);
        }
    }

    /// <summary>
    /// To Get Functionality ID
    /// </summary>
    public int FunctionalityID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["FunctionalityID"]);
        }
    }


    /// <summary>
    /// To Get List ID
    /// </summary>
    public int ListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["ListID"]);
        }
    }

}