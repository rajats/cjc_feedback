﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using RusticiSoftware.HostedEngine.Client;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;

public partial class NewTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {



        RoleManageLists objRoleManageList = new RoleManageLists();
        StringBuilder sbDelete = new StringBuilder();
        StringBuilder sbInsertQuery = new StringBuilder();
        Employee objEmp = new Employee();


        Role objRole = new Role();
        //DataTable dt = objRole.GetRoleEmployeeList(BusinessUtility.GetInt(Request["roleID"]));
        //Response.Write(dt.Rows.Count);

        EmpAssignedCourses objAssignedCourse = new EmpAssignedCourses();
        DataTable dt = objEmp.GetEmployeeList();

        //StringBuilder sbRowConten = new StringBuilder();  
        // To Update Emp Assigned Courses
        //objAssignedCourse.ResetEmployeeAssignedCourses(32086);
       foreach (DataRow dRow in dt.Rows)
        {
            int iInsertedEmpID = BusinessUtility.GetInt(dRow["empHdrID"]);
            //objAssignedCourse.ResetEmployeeAssignedCourses(iInsertedEmpID);


            bool bReturn = false;
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                StringBuilder sbQueryEmployeeCourse = new StringBuilder();

                sbQueryEmployeeCourse.Append(" select * ,   ");
                sbQueryEmployeeCourse.Append(" fun_get_Course_completed(  (SELECT   ");
                sbQueryEmployeeCourse.Append(" idRegistration  ");
                sbQueryEmployeeCourse.Append(" FROM  ");
                sbQueryEmployeeCourse.Append(" employeecourseregistration  ");
                sbQueryEmployeeCourse.Append(" WHERE  ");
                sbQueryEmployeeCourse.Append(" empHeader_empHdrID = Result.CourseEmpHdrID  ");
                sbQueryEmployeeCourse.Append(" AND courseHeader_courseHdrID = Result.CourseID   ");
                sbQueryEmployeeCourse.Append(" AND courseHeader_courseVerNo = Result.CourseVer)) AS IsCompleted ,  ");
                sbQueryEmployeeCourse.Append(" fun_get_Event_completed(  (SELECT   ");
                sbQueryEmployeeCourse.Append(" idRegistration  ");
                sbQueryEmployeeCourse.Append(" FROM  ");
                sbQueryEmployeeCourse.Append(" employeecourseregistration  ");
                sbQueryEmployeeCourse.Append(" WHERE  ");
                sbQueryEmployeeCourse.Append(" empHeader_empHdrID = Result.CourseEmpHdrID  ");
                sbQueryEmployeeCourse.Append(" AND courseHeader_courseHdrID = Result.CourseID   ");
                sbQueryEmployeeCourse.Append(" AND courseHeader_courseVerNo = Result.CourseVer)) AS IsContentCompleted  ");
                sbQueryEmployeeCourse.Append(" from   ");
                sbQueryEmployeeCourse.Append(" (  ");
                sbQueryEmployeeCourse.Append(" SELECT DISTINCT @empID  AS CourseEmpHdrID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseHdrID AS CourseID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseVerNo AS CourseVer, CURRENT_TIMESTAMP(), 1  ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" emprolesincludes AS empRolesIn ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" emprolecourses AS empRolCourses ON empRolCourses.empRoles_empRoleID = empRolesIn.empRoles_empRoleID ");
                sbQueryEmployeeCourse.Append(" AND empRolesIn.empHeader_empHdrID = @empID  ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" traningevent AS courseHdr ON courseHdr.courseHdrID = empRolCourses.courseHeader_courseHdrID ");
                sbQueryEmployeeCourse.Append(" AND courseHdr.CourseActive = 1 AND courseHdr.isVersionActive = 1 ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" coursetype AS courstyp ON courstyp.Type = courseHdr.courseType ");
                sbQueryEmployeeCourse.Append(" LEFT JOIN ");
                sbQueryEmployeeCourse.Append(" trainingeventdetail trnDtl ON trnDtl.trn_course_hdr_id = empRolCourses.courseHeader_courseHdrID ");
                sbQueryEmployeeCourse.Append(" WHERE 1=1 ");
                //sbQueryEmployeeCourse.Append(" empRolCourses.courseHeader_courseHdrID NOT IN (SELECT DISTINCT ");
                //sbQueryEmployeeCourse.Append(" courseHeader_courseHdrID ");
                //sbQueryEmployeeCourse.Append(" FROM ");
                //sbQueryEmployeeCourse.Append(" employeecourseregistration ");
                //sbQueryEmployeeCourse.Append(" WHERE ");
                //sbQueryEmployeeCourse.Append(" empHeader_empHdrID = @empID)   ");
                sbQueryEmployeeCourse.Append(" UNION SELECT DISTINCT @empID,  ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseHdrID, ");
                sbQueryEmployeeCourse.Append(" courseHdr.courseVerNo, CURRENT_TIMESTAMP(), 1 ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel AS syroleSel ");
                sbQueryEmployeeCourse.Append(" JOIN ");
                sbQueryEmployeeCourse.Append(" empreference eRef ON eRef.sysEmpReferenceCodes_sysRefCode = syroleSel.sys_ref_code ");
                sbQueryEmployeeCourse.Append(" AND eRef.empHeader_empHdrID = @empID  ");
                sbQueryEmployeeCourse.Append(" AND (syroleSel.sys_cond IS NULL  ");
                sbQueryEmployeeCourse.Append(" OR syroleSel.sys_cond = 'OR')  ");
                sbQueryEmployeeCourse.Append(" AND  ");
                sbQueryEmployeeCourse.Append(" (   ");
                sbQueryEmployeeCourse.Append(" ( ");
                sbQueryEmployeeCourse.Append(" (SELECT  ");
                sbQueryEmployeeCourse.Append(" COUNT(*) ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel AS AND1syroleSel ");
                sbQueryEmployeeCourse.Append(" JOIN ");
                sbQueryEmployeeCourse.Append(" empreference eRef1 ON eRef1.sysEmpReferenceCodes_sysRefCode = AND1syroleSel.sys_ref_code ");
                sbQueryEmployeeCourse.Append(" AND eRef1.empHeader_empHdrID =    @empID  ");
                sbQueryEmployeeCourse.Append(" AND AND1syroleSel.sys_cond = 'AND' AND AND1syroleSel.sys_cond IS NOT NULL  ");
                sbQueryEmployeeCourse.Append(" AND AND1syroleSel.sys_ref_value = FUN_GET_EMP_REF_VAL(AND1syroleSel.sys_ref_code,  ");
                sbQueryEmployeeCourse.Append(" eRef1.empHeader_empHdrID) where AND1syroleSel.emp_roleid = syroleSel.emp_roleid) =  ( select count(*)  FROM  sysrolerefsel as AND2syroleSel WHERE  ");
                sbQueryEmployeeCourse.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbQueryEmployeeCourse.Append(" AND AND2syroleSel.sys_cond = 'AND')  ");
                sbQueryEmployeeCourse.Append(" AND (SELECT  ");
                sbQueryEmployeeCourse.Append(" COUNT(*) ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel as AND2syroleSel ");
                sbQueryEmployeeCourse.Append(" WHERE ");
                sbQueryEmployeeCourse.Append(" AND2syroleSel.emp_roleid = syroleSel.emp_roleid ");
                sbQueryEmployeeCourse.Append(" AND AND2syroleSel.sys_cond = 'AND') > 0  ");
                sbQueryEmployeeCourse.Append(" )  ");
                sbQueryEmployeeCourse.Append(" OR  ");
                sbQueryEmployeeCourse.Append(" ( ");
                sbQueryEmployeeCourse.Append(" (SELECT  ");
                sbQueryEmployeeCourse.Append(" COUNT(*) ");
                sbQueryEmployeeCourse.Append(" FROM ");
                sbQueryEmployeeCourse.Append(" sysrolerefsel as ORsyroleSel ");
                sbQueryEmployeeCourse.Append(" WHERE ");
                sbQueryEmployeeCourse.Append(" ORsyroleSel.emp_roleid =  syroleSel.emp_roleid ");
                sbQueryEmployeeCourse.Append(" AND ORsyroleSel.sys_cond = 'AND') = 0 ");
                sbQueryEmployeeCourse.Append(" )   ");
                sbQueryEmployeeCourse.Append(" ) ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" emprolecourses AS empRolCourses ON empRolCourses.empRoles_empRoleID = syroleSel.emp_roleID ");
                sbQueryEmployeeCourse.Append(" AND (syroleSel.sys_ref_value = fun_get_emp_ref_val(syroleSel.sys_ref_code, ");
                sbQueryEmployeeCourse.Append(" empHeader_empHdrID)) ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" traningevent AS courseHdr ON courseHdr.courseHdrID = empRolCourses.courseHeader_courseHdrID ");
                sbQueryEmployeeCourse.Append(" AND courseHdr.CourseActive = 1 AND courseHdr.isVersionActive = 1 ");
                sbQueryEmployeeCourse.Append(" INNER JOIN ");
                sbQueryEmployeeCourse.Append(" coursetype AS courstyp ON courstyp.Type = courseHdr.courseType ");
                sbQueryEmployeeCourse.Append(" LEFT JOIN ");
                sbQueryEmployeeCourse.Append(" trainingeventdetail trnDtl ON trnDtl.trn_course_hdr_id = empRolCourses.courseHeader_courseHdrID ");
                sbQueryEmployeeCourse.Append(" WHERE 1=1 ");
                //sbQueryEmployeeCourse.Append(" empRolCourses.courseHeader_courseHdrID NOT IN (SELECT DISTINCT ");
                //sbQueryEmployeeCourse.Append(" courseHeader_courseHdrID ");
                //sbQueryEmployeeCourse.Append(" FROM ");
                //sbQueryEmployeeCourse.Append(" employeecourseregistration ");
                //sbQueryEmployeeCourse.Append(" WHERE ");
                //sbQueryEmployeeCourse.Append(" empHeader_empHdrID =   @empID  )   ");
                sbQueryEmployeeCourse.Append(" ) as Result   ");

                DataTable dtEmployeeCourse = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sbQueryEmployeeCourse), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("empID", iInsertedEmpID, MyDbType.Int)
                        });


            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                ErrorLog.createLog("EmpID " + iInsertedEmpID);
                ErrorLog.CreateLog(ex   );
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }

        }

    }
}