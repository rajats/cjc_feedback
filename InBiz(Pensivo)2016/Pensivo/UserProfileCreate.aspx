﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="UserProfileCreate.aspx.cs" Inherits="UserProfileCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnlUserDetail" runat="server" DefaultButton="btnCreateEmployee">
        <section id="main-content" class="pg-dashboard">
            <div class="wrapper width-med" onkeypress="return disableEnterKey(event)">
                <h1>
                    <asp:Literal ID="ltrTitle" runat="server"></asp:Literal>
                </h1>

                <div class="btngrp" style="float: right;" id="dvChangePassword" runat="server" visible="false">
                    <a href="#nogo" class="btn round" id="btnNext" onclick="ResetPasswordQuestionConfirmation();"><%=Resources.Resource.btnResetPasswordandSectQestion %></a>
                </div>
                <br />
                <br />
                <div class="boxed-content">
                    <asp:Label ID="ltrErrMsg" runat="server" ForeColor="Red"></asp:Label>
                    <div class="form-body">
                        <div class="plms-fieldset is-first">
                            <label class="plms-label is-hidden" for="txtFirstName"><%= Resources.Resource.lblEmpFirstName%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtFirstName" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblEmpFirstName %>" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement" style="display: none;">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEmpFirstName%></p>
                                    </div>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfFirstName" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="txtFirstName" Text="<%$ Resources:Resource, reqFirstName %>" />
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label is-hidden" for="txtlastName"><%= Resources.Resource.lblEmpLastName%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtlastName" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblEmpLastName %>" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEmpLastName%></p>
                                    </div>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfLastName" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="txtLastName" Text="<%$ Resources:Resource, reqLastName %>" />
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label is-hidden" for="txtPassword"><%= Resources.Resource.lblPassword%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox class="plms-input skin2 " ID="txtPassword" runat="server" placeholder="<%$ Resources:Resource, lblPassword %>" TextMode="Password" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblPassword%></p>
                                    </div>
                                </div>
                            </div>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtPassword" ID="RegularExpressionValidator1" class="formels-feedback invalid"
                                ValidationExpression="^[\s\S]{4,}$" runat="server" ErrorMessage="<%$ Resources:Resource, lblPasswordLength %>" ValidationGroup="vg1"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="rfPassword" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="txtPassword" Text="<%$ Resources:Resource, reqPassword %>" />
                        </div>
                        <div class="plms-fieldset" id="dvEmail" runat="server">
                            <label class="plms-label is-hidden" for="txtEmail"><%= Resources.Resource.lblEmail%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtEmail" runat="server" class="plms-input skin2 " placeholder="<%$ Resources:Resource, lblEmail %>" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEmail%></p>
                                    </div>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfMail" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="txtEmail" Text="<%$ Resources:Resource, reqMail %>" />
                            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" class="formels-feedback invalid" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="vg1" ErrorMessage="<%$ Resources:Resource, errMsgInvalidMailFormat %>" SetFocusOnError="true"></asp:RegularExpressionValidator>
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label is-hidden" for="txtEmpID" id="lblEmpID" runat="server"></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtEmpID" runat="server" class="plms-input skin2 " />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="pEmpID" runat="server"></p>
                                    </div>
                                </div>
                            </div>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtEmpID" ID="valEmpIDExp" class="formels-feedback invalid"
                                ValidationExpression="^[\s\S]{4,}$" runat="server" ErrorMessage="<%$ Resources:Resource, lblInvalidLogInIDFormat %>" ValidationGroup="vg1"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="rfEmpID" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="txtEmpID" Text="<%$ Resources:Resource, reqEmpID %>" />
                        </div>
                        <div class="plms-fieldset" id="dvjobcode" runat="server">
                            <label class="plms-label is-hidden" for="ddlJobCode"><%= Resources.Resource.lblJobCode%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlJobCode" runat="server" class="plms-select skin2 is-placeholder ">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblJobCode%></p>
                                    </div>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfJobCode" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="ddlJobCode" Text="<%$ Resources:Resource, reqJobCode %>" />
                        </div>
                        <div class="plms-fieldset" id="dvStore" runat="server">
                            <label class="plms-label is-hidden" for="ddlStore"><%= Resources.Resource.lblStore%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlStore" runat="server" class="plms-select skin2 is-placeholder  ">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblStore%></p>
                                    </div>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfddlStore" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="ddlStore" Text="<%$ Resources:Resource, reqStore %>" />
                        </div>
                        <div class="plms-fieldset" id="dvDivission" runat="server">
                            <label class="plms-label is-hidden" for="ddlDivision"><%= Resources.Resource.lblDivision%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlDivision" runat="server" class="plms-select skin2 is-placeholder  ">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblDivision%></p>
                                    </div>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfddlDivision" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="ddlDivision" Text="<%$ Resources:Resource, reqDivision %>" />
                        </div>
                        <div class="plms-fieldset" id="dvType" runat="server">
                            <label class="plms-label is-hidden" for="ddlType"><%= Resources.Resource.lblType%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlType" runat="server" class="plms-select skin2 is-placeholder  ">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblType%></p>
                                    </div>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfddlType" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="ddlType" Text="<%$ Resources:Resource, reqType %>" />
                        </div>
                        <div class="plms-fieldset" id="dvTypeParsed" runat="server">
                            <label class="plms-label is-hidden" for="ddlTypeParsed"><%= Resources.Resource.lblTypeParsed%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlTypeParsed" runat="server" class="plms-select skin2 is-placeholder  ">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblTypeParsed%></p>
                                    </div>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfddlTypeParsed" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="vg1" SetFocusOnError="true"
                                ControlToValidate="ddlTypeParsed" Text="<%$ Resources:Resource, reqTypeParsed %>" />
                        </div>
                        <div class="plms-fieldset" id="dvProvince" runat="server" visible="false">
                            <label class="plms-label is-hidden" for="ddlProvince"><%= Resources.Resource.lblProvince%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlProvince" runat="server" class="plms-select skin2 is-placeholder  is-disabled" disabled="disabled">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblProvince%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField ID="hdnUUID" runat="server" />
                        <footer class="form-footer">
                            <div class="btngrp">
                                <asp:Button ID="btnCreateEmployee" class="btn round" runat="server" Text="<%$ Resources:Resource, btnCreate %>" ValidationGroup="vg1" OnClick="btnCreateEmployee_OnClick" />
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </section>
    </asp:Panel>
    <asp:HiddenField ID="hdnConfirmationMsg" runat="server" />
    <asp:HiddenField ID="hdnInformationMsg" runat="server" />
    <asp:HiddenField ID="hdnUserID" runat="server" />

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Create Document Ready Function
        $(document).ready(function () {
            $("#<%=txtFirstName.ClientID%>").focus();
        });


        // To Show Confirmation Dialog for Reset Password/question
        function ResetPasswordQuestionConfirmation() {
            var title = "<%=Resources.Resource.lblConfirmation%>";
            var messageText = $("#<%=hdnConfirmationMsg.ClientID%>").val();
            okButtonText = "<%=Resources.Resource.lblOk%>"
            LaterCnclButtonText = "<%=Resources.Resource.btnLogInCancel%>"
            okButtonRedirectlink = "OkChangePasswordConfirmation();";
            LaterCnclButtonRedirectLink = "CloseChangePasswordConfirmation();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Trigger on OK Button to Change Password
        function OkChangePasswordConfirmation() {
            HideConfirmationDialog();

            var title = "<%=Resources.Resource.lblInformation%>";
            var messageText = $("#<%=hdnInformationMsg.ClientID%>").val();
            okButtonText = "<%=Resources.Resource.lblOk%>"
            LaterCnclButtonText = "fgdsg"
            okButtonRedirectlink = "OkChangePasswordInformation();";
            LaterCnclButtonRedirectLink = "OkChangePasswordInformation();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
            $("#hypCancel").css("visibility", "hidden");
            $("#hypOK").addClass("round");
        }


        // To Trigger on Close Button to Change Password
        function CloseChangePasswordConfirmation() {
            $("#hypCancel").css("visibility", "hidden");
            $("#hypOK").addClass("round");
            HideConfirmationDialog();
            window.location.href = 'User.aspx';
        }



        // To Trigger on Close Button to Change Password on Information Popup
        function CloseChangePasswordInformation() {
            HideConfirmationDialog();
        }


        // To Trigger on OK Button to Change Password Information
        function OkChangePasswordInformation() {
            HideConfirmationDialog();
            $.ajax(
                {
                    type: "POST",
                    url: "UserProfileCreate.aspx/ResetUserPasswordQuestion",
                    contentType: "application/json; charset=utf-8",
                    data: "{userID:'" + $("#<%=hdnUserID.ClientID%>").val() + "'}",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function 
                    (msg) {

                        if (msg.d == "1") {
                            window.location.href = 'ViewUser.aspx?isEdit=1';
                        }

                    },
                    error: function (x, e) {

                    }
                }
                );

            }
    </script>
</asp:Content>

