﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleEventDefault.aspx.cs" Inherits="RoleEventDefault" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper width-med">
            <h1>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal>
            </h1>
            <div class="boxed-content">
                <asp:Label ID="ltrErrMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                <div class="btngrp" style="float: right;" id="dvCreateEventVersion" runat="server" visible="false">
                    <%--visible="false"--%>
                    <%--<a href="#nogo" class="btn round" id="btnNext" onclick="ResetPasswordQuestionConfirmation();"><%=Resources.Resource.btnResetPasswordandSectQestion %></a>--%>
                    <%--<asp:Button ID="btnCreateVersion" class="btn round" runat="server" Text="<%$ Resources:Resource, BtnCreateNewVersion %>" UseSubmitBehavior="false" OnClick="btnCreateVersion_Click" />--%>
                </div>

                <div class="form-body">
                    <%--<div class="plms-fieldset is-first">
                        <label class="plms-label" for="txtCourseVerNo"><%= Resources.Resource.lblVersionNo%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtCourseVerNo" runat="server" class="plms-input skin2 numeric is-disabled" placeholder="<%$ Resources:Resource, lblVersionNo %>" MaxLength="5" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblVersionNo%></p>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfCourseVerNo" runat="server" class="formels-feedback invalid" ValidationGroup="vGroup1" Display="Dynamic" SetFocusOnError="true"
                                ControlToValidate="txtCourseVerNo" Text="<%$ Resources:Resource, msgReqVerNo %>" />
                        </div>
                    </div>--%>
                    <div class="plms-fieldset is-first" style="display: none;">
                        <label class="plms-label" for="ddlCourseVersion"><%= Resources.Resource.lblVersionNo%></label>
                        <div class="plms-tooltip-parent">
                            <asp:DropDownList ID="ddlCourseVersion" runat="server" class="plms-select skin2 is-placeholder"></asp:DropDownList>
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblVersionNo%></p>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="plms-fieldset ">
                        <label class="plms-label" for="txtReportingKeyWords"><%= Resources.Resource.lblReportingKeyWords%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtReportingKeyWords" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblReportingKeyWords %>" TextMode="SingleLine" MaxLength="400" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblReportingKeyWords%></p>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfReportingKeyWords" runat="server" class="formels-feedback invalid" ValidationGroup="vGroup1" Display="Dynamic" SetFocusOnError="true"
                                ControlToValidate="txtReportingKeyWords" Text="<%$ Resources:Resource, lblRequiredReportingKeyWords %>" />
                        </div>
                    </div>
                    <div class="plms-fieldset ">
                        <label class="plms-label" for="txtMinimumPassScore"><%= Resources.Resource.lblMinimumPassScore%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtMinimumPassScore" runat="server" class="plms-input skin2 numeric" placeholder="<%$ Resources:Resource, lblMinimumPassScore %>" MaxLength="3" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblMinimumPassScore%></p>
                                </div>
                            </div>
                            <%--<asp:RequiredFieldValidator ID="rfMinimumPassScore" runat="server" class="formels-feedback invalid" ValidationGroup="vGroup1" Display="Dynamic" SetFocusOnError="true"
                                ControlToValidate="txtMinimumPassScore" Text="<%$ Resources:Resource, lblRequiredMinimumPassScore %>" />--%>
                        </div>
                    </div>

                    <div class="plms-fieldset " style="display: none;">
                        <label class="plms-label" for="txtMaximumPassScore"><%= Resources.Resource.lblMaximumPassScore%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtMaximumPassScore" runat="server" class="plms-input skin2 numeric" placeholder="<%$ Resources:Resource, lblMaximumPassScore %>" MaxLength="3" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblMaximumPassScore%></p>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfMaximumPassScore" runat="server" class="formels-feedback invalid" ValidationGroup="vGroup1" Display="Dynamic" SetFocusOnError="true"
                                ControlToValidate="txtMaximumPassScore" Text="<%$ Resources:Resource, lblRequiredMaximumPassScore %>" />
                        </div>
                    </div>

                    <div class="plms-fieldset">
                        <label class="plms-label" for="ddlAttemptResetType"><%= Resources.Resource.lblAttemptResetType%></label>
                        <div class="plms-tooltip-parent">
                            <asp:DropDownList ID="ddlAttemptResetType" runat="server" class="plms-select skin2 is-placeholder">
                                <asp:ListItem Text="Auto" Value="A" Selected="True" />
                                <asp:ListItem Text="None" Value="N" />
                            </asp:DropDownList>

                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblAttemptResetType%></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="plms-fieldset ">
                        <label class="plms-label" for="txtAttemptGrantWaitingTime"><%= Resources.Resource.lblAttemptGrantWaitingTime%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtAttemptGrantWaitingTime" runat="server" class="plms-input skin2 numeric" placeholder="<%$ Resources:Resource, lblAttemptGrantWaitingTime %>" MaxLength="5" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblAttemptGrantWaitingTime%></p>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfAttemptGrantWaitingTime" runat="server" class="formels-feedback invalid" ValidationGroup="vGroup1" Display="Dynamic" SetFocusOnError="true"
                                ControlToValidate="txtAttemptGrantWaitingTime" Text="<%$ Resources:Resource, lblRequiredAttemptGrantWaitingTime %>" />
                        </div>
                    </div>

                    <div class="plms-fieldset ">
                        <label class="plms-label" for="txtOrderSeqInDisplay"><%= Resources.Resource.lblOrderSeqInDisplay%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtOrderSeqInDisplay" runat="server" class="plms-input skin2 numeric" placeholder="<%$ Resources:Resource, lblOrderSeqInDisplay %>" MaxLength="3" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblOrderSeqInDisplay%></p>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfOrderSeqInDisplay" runat="server" class="formels-feedback invalid" ValidationGroup="vGroup1" Display="Dynamic" SetFocusOnError="true"
                                ControlToValidate="txtOrderSeqInDisplay" Text="<%$ Resources:Resource, lblRequiredOrderSeqInDisplay %>" />
                        </div>
                    </div>

                    <div class="plms-fieldset " style="display: none;">
                        <label class="plms-label" for="txtUserDisplayPriority"><%= Resources.Resource.lblUserDisplayPriority%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtUserDisplayPriority" runat="server" class="plms-input skin2 numeric" placeholder="<%$ Resources:Resource, lblUserDisplayPriority %>" MaxLength="3" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblUserDisplayPriority%></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="plms-fieldset ">
                        <label class="plms-label" for="txtCourseStartDate"><%= Resources.Resource.lblTrainingStartDate%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtCourseStartDate" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblTrainingStartDate %>" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblTrainingStartDate%></p>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfUserDisplayPriority" runat="server" class="formels-feedback invalid" ValidationGroup="vGroup1" Display="Dynamic" EnableClientScript="true" SetFocusOnError="true"
                                ControlToValidate="txtCourseStartDate" Text="<%$ Resources:Resource, lblRequiredTrainingStartDate %>" />
                            <asp:CustomValidator ID="rfValidateStart" runat="server" ControlToValidate="txtCourseStartDate" ErrorMessage="<%$ Resources:Resource, lblInvalidDateFormat %>" class="formels-feedback invalid" ValidationGroup="vGroup1" Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="ValidateDate" />
                        </div>
                    </div>



                    <div class="plms-fieldset label-radio">
                        <label class="plms-label" for="rdRepeatRequired"><%= Resources.Resource.lblRepeatRequired%></label>
                        <div class="label-radio-group">
                            <label class="plms-label inline no-margin" for="event-status-active">
                                <input type="radio" class="plms-radio" id="rdRepeatRequiredYes" name="rdRepeatRequired" value="1" runat="server" />
                                <%= Resources.Resource.lblTrainingEventYes%>
                            </label>
                            <label class="plms-label inline no-margin" for="event-status-inactive">
                                <input type="radio" class="plms-radio" id="rdRepeatRequiredNo" name="rdRepeatRequired" value="0" runat="server" />
                                <%= Resources.Resource.lblTrainingEventNo%>
                            </label>
                        </div>
                    </div>


                    <div id="dvRepeatDetail">
                        <div class="plms-fieldset">
                            <label class="plms-label" for="ddlRepeatTriggerType"><%= Resources.Resource.lblRepeatTriggerType%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlRepeatTriggerType" runat="server" class="plms-select skin2 is-placeholder">
                                    <asp:ListItem Text="Fixed Date" Value="F" Selected="True" />
                                    <asp:ListItem Text="Relative No. of Days From Start Date" Value="S" />
                                    <asp:ListItem Text="Relative No. of Days From Completed Date" Value="C" />
                                    <asp:ListItem Text="Repeat From Eligible Date" Value="E" />
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblRepeatTriggerType%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="dvRepeatInDays">
                            <label class="plms-label" for="txtRepeatInDays"><%= Resources.Resource.lblRepeatInDays%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtRepeatInDays" runat="server" class="plms-input skin2 numeric" placeholder="<%$ Resources:Resource, lblRepeatInDays %>" MaxLength="20" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblRepeatInDays%></p>
                                    </div>
                                </div>
                                <asp:RequiredFieldValidator ID="rfRepeatInDays" runat="server" class="formels-feedback invalid" ValidationGroup="vGroup1" SetFocusOnError="true"
                                    ControlToValidate="txtRepeatInDays" Text="<%$ Resources:Resource, lblRequiredRepeatInDays %>" />
                            </div>
                        </div>

                        <div id="dvFixedDate">
                            <div class="plms-fieldset">
                                <label class="plms-label" for="ddlFixedDay"><%= Resources.Resource.lblDay%></label>
                                <div class="plms-tooltip-parent">
                                    <asp:DropDownList ID="ddlFixedDay" runat="server" class="plms-select skin2 is-placeholder">
                                        <asp:ListItem Text="1" Value="1" Selected="True" />
                                        <asp:ListItem Text="2" Value="2" />
                                        <asp:ListItem Text="3" Value="3" />
                                        <asp:ListItem Text="4" Value="4" />
                                        <asp:ListItem Text="5" Value="5" />
                                        <asp:ListItem Text="6" Value="6" />
                                        <asp:ListItem Text="7" Value="7" />
                                        <asp:ListItem Text="8" Value="8" />
                                        <asp:ListItem Text="9" Value="9" />
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="11" Value="11" />
                                        <asp:ListItem Text="12" Value="12" />
                                        <asp:ListItem Text="13" Value="13" />
                                        <asp:ListItem Text="14" Value="14" />
                                        <asp:ListItem Text="15" Value="15" />
                                        <asp:ListItem Text="16" Value="16" />
                                        <asp:ListItem Text="17" Value="17" />
                                        <asp:ListItem Text="18" Value="18" />
                                        <asp:ListItem Text="19" Value="19" />
                                        <asp:ListItem Text="20" Value="20" />
                                        <asp:ListItem Text="21" Value="21" />
                                        <asp:ListItem Text="22" Value="22" />
                                        <asp:ListItem Text="23" Value="23" />
                                        <asp:ListItem Text="24" Value="24" />
                                        <asp:ListItem Text="25" Value="25" />
                                        <asp:ListItem Text="26" Value="26" />
                                        <asp:ListItem Text="27" Value="27" />
                                        <asp:ListItem Text="28" Value="28" />
                                        <asp:ListItem Text="29" Value="29" />
                                        <asp:ListItem Text="30" Value="30" />
                                        <asp:ListItem Text="31" Value="31" />
                                    </asp:DropDownList>
                                    <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                        <div class="plms-tooltip-body">
                                            <p><%= Resources.Resource.lblDay%></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="plms-fieldset">
                                <label class="plms-label" for="ddlFixedMonth"><%= Resources.Resource.lblMonth%></label>
                                <div class="plms-tooltip-parent">
                                    <asp:DropDownList ID="ddlFixedMonth" runat="server" class="plms-select skin2 is-placeholder">
                                        <asp:ListItem Text="1" Value="1" Selected="True" />
                                        <asp:ListItem Text="2" Value="2" />
                                        <asp:ListItem Text="3" Value="3" />
                                        <asp:ListItem Text="4" Value="4" />
                                        <asp:ListItem Text="5" Value="5" />
                                        <asp:ListItem Text="6" Value="6" />
                                        <asp:ListItem Text="7" Value="7" />
                                        <asp:ListItem Text="8" Value="8" />
                                        <asp:ListItem Text="9" Value="9" />
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="11" Value="11" />
                                        <asp:ListItem Text="12" Value="12" />
                                    </asp:DropDownList>
                                    <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                        <div class="plms-tooltip-body">
                                            <p><%= Resources.Resource.lblMonth%></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="plms-fieldset label-radio">
                        <label class="plms-label" for="rdAlertRequired"><%= Resources.Resource.lblAlertRequired%></label>
                        <div class="label-radio-group">
                            <label class="plms-label inline no-margin" for="event-status-active">
                                <input type="radio" class="plms-radio" id="rdAlertRequiredYes" name="rdAlertRequired" value="1" runat="server" />
                                <%= Resources.Resource.lblTrainingEventYes%>
                            </label>
                            <label class="plms-label inline no-margin" for="event-status-inactive">
                                <input type="radio" class="plms-radio" id="rdAlertRequiredNo" name="rdAlertRequired" value="0" runat="server" />
                                <%= Resources.Resource.lblTrainingEventNo%>
                            </label>
                        </div>
                    </div>

                    <div id="dvAlert">

                        <div class="plms-fieldset label-radio">
                            <label class="plms-label" for="rdBeforeLaunch"><%= Resources.Resource.lblBeforeLaunch%></label>
                            <div class="label-radio-group">
                                <label class="plms-label inline no-margin" for="event-status-active">
                                    <input type="radio" class="plms-radio" id="rdBeforeLaunchYes" name="rdBeforeLaunch" value="1" runat="server" />
                                    <%= Resources.Resource.lblTrainingEventYes%>
                                </label>
                                <label class="plms-label inline no-margin" for="event-status-inactive">
                                    <input type="radio" class="plms-radio" id="rdBeforeLaunchNo" name="rdBeforeLaunch" value="0" runat="server" />
                                    <%= Resources.Resource.lblTrainingEventNo%>
                                </label>
                            </div>
                        </div>

                        <div class="plms-fieldset label-radio">
                            <label class="plms-label" for="rdNotCompleted"><%= Resources.Resource.lblNotCompleted%></label>
                            <div class="label-radio-group">
                                <label class="plms-label inline no-margin" for="event-status-active">
                                    <input type="radio" class="plms-radio" id="rdNotCompletedYes" name="rdNotCompleted" value="1" runat="server" />
                                    <%= Resources.Resource.lblTrainingEventYes%>
                                </label>
                                <label class="plms-label inline no-margin" for="event-status-inactive">
                                    <input type="radio" class="plms-radio" id="rdNotCompletedNo" name="rdNotCompleted" value="0" runat="server" />
                                    <%= Resources.Resource.lblTrainingEventNo%>
                                </label>
                            </div>
                        </div>


                        <div class="plms-fieldset label-radio">
                            <label class="plms-label" for="rdManagerNotCompleted"><%= Resources.Resource.lblManagerNotCompleted%></label>
                            <div class="label-radio-group">
                                <label class="plms-label inline no-margin" for="event-status-active">
                                    <input type="radio" class="plms-radio" id="rdManagerNotCompletedYes" name="rdManagerNotCompleted" value="1" runat="server" />
                                    <%= Resources.Resource.lblTrainingEventYes%>
                                </label>
                                <label class="plms-label inline no-margin" for="event-status-inactive">
                                    <input type="radio" class="plms-radio" id="rdManagerNotCompletedNo" name="rdManagerNotCompleted" value="0" runat="server" />
                                    <%= Resources.Resource.lblTrainingEventNo%>
                                </label>
                            </div>
                        </div>

                        <div class="plms-fieldset ">
                            <label class="plms-label" for="txtAlertInDays"><%= Resources.Resource.lblAlertInDays%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtAlertInDays" runat="server" class="plms-input skin2 numeric" placeholder="<%$ Resources:Resource, lblAlertInDays %>" MaxLength="5" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblAlertInDays%></p>
                                    </div>
                                </div>
                                <%--<asp:RequiredFieldValidator ID="rfAlertInDays" runat="server" class="formels-feedback invalid" ValidationGroup="vGroup1" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="txtAlertInDays" Text="<%$ Resources:Resource, lblRequiredAlertInDays %>" />--%>
                            </div>
                        </div>

                        <div class="plms-fieldset ">
                            <label class="plms-label" for="txtAlertInDaysRepeat"><%= Resources.Resource.lblAlertInDaysRepeat%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtAlertInDaysRepeat" runat="server" class="plms-input skin2 numeric" placeholder="<%$ Resources:Resource, lblAlertInDaysRepeat %>" MaxLength="5" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblAlertInDaysRepeat%></p>
                                    </div>
                                </div>
                                <%--<asp:RequiredFieldValidator ID="rftxtAlertInDaysRepeat" runat="server" class="formels-feedback invalid" ValidationGroup="vGroup1" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="txtAlertInDaysRepeat" Text="<%$ Resources:Resource, lblRequiredAlertInDaysRepeat %>" />--%>
                            </div>
                        </div>

                        <div class="plms-fieldset">
                            <label class="plms-label" for="ddlAlertFormat"><%= Resources.Resource.lblAlertFormat%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlAlertFormat" runat="server" class="plms-select skin2 is-placeholder">
                                    <asp:ListItem Text="Text" Value="T" Selected="True" />
                                    <asp:ListItem Text="Mail" Value="M" />

                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblAlertFormat%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label" for="txtAlertPortalMsg"><%= Resources.Resource.lblAlertPortalMsg%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtAlertPortalMsg" runat="server" class="plms-input skin2 " placeholder="<%$ Resources:Resource, lblAlertPortalMsg %>" TextMode="MultiLine" Height="100" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblAlertPortalMsg%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label" for="txtAlertEmailMsg"><%= Resources.Resource.lblAlertEmailMsg%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtAlertEmailMsg" runat="server" class="plms-input skin2 " placeholder="<%$ Resources:Resource, lblAlertEmailMsg %>" TextMode="MultiLine" Height="100" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblAlertEmailMsg%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label" for="txtAlertEmailSubject"><%= Resources.Resource.lblAlertEmailSubject%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtAlertEmailSubject" runat="server" class="plms-input skin2 " placeholder="<%$ Resources:Resource, lblAlertEmailSubject %>" TextMode="SingleLine" MaxLength="200" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblAlertEmailSubject%></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="plms-fieldset">
                            <label class="plms-label" for="txtAlertEscalationEmailMsg"><%= Resources.Resource.lblAlertEscalationEmailMsg%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtAlertEscalationEmailMsg" runat="server" class="plms-input skin2 " placeholder="<%$ Resources:Resource, lblAlertEscalationEmailMsg %>" TextMode="MultiLine" Height="100" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblAlertEscalationEmailMsg%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label" for="txtAlertEscalationEmailSubject"><%= Resources.Resource.lblAlertEscalationEmailSubject%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtAlertEscalationEmailSubject" runat="server" class="plms-input skin2 " placeholder="<%$ Resources:Resource, lblAlertEscalationEmailSubject %>" TextMode="SingleLine" MaxLength="200" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblAlertEscalationEmailSubject%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="form-footer">
                        <div class="btngrp ">
                            <asp:Button ID="btnEventConfirmationMessage" class="btn round" runat="server" Text="<%$ Resources:Resource, btnCreate %>" ValidationGroup="vGroup1" OnClick="btnEventConfirmationMessage_OnClick" />
                            <asp:Button ID="btnCreateEvent" class="btn round" runat="server" Text="<%$ Resources:Resource, btnCreate %>" Style="display: none;" OnClick="btnCreateEvent_OnClick" />
                            <%--<a href="#nogo" class="btn round" id="btnSubmit" runat="server" ValidationGroup="vGroup1" ><%=Resources.Resource.lblAddEvent %></a>--%>
                        </div>
                    </footer>
                </div>


            </div>
        </div>
    </section>

    <asp:HiddenField ID="hdnShowRepeat" runat="server" Value="0" />
    <asp:HiddenField ID="hdnShowAlert" runat="server" Value="0" />

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        var Page_Validators;

        $(".numeric").keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                (e.keyCode == 65 && e.ctrlKey === true) ||
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });



        // To Define Document Get Ready Event
        $(document).ready(function () {
            $("#dvSlide").show();
            $("#dvTestExtID").hide();
            $("#dvQuestion").hide();
            $("#dvPassGrade").hide();
            $("#dvNoAttempts").hide();
            $("#dvRepeatDetail").hide();
            $("#dvAlert").hide();
            $("#dvRepeatInDays").hide();
            $("#dvFixedDate").hide();

            $("#<%=rdRepeatRequiredYes.ClientID%>").trigger("change");
            $("#<%=rdRepeatRequiredNo.ClientID%>").trigger("change");
            $("#<%=rdAlertRequiredYes.ClientID%>").trigger("change");
            $("#<%=rdAlertRequiredNo.ClientID%>").trigger("change");

            $("#<%=ddlRepeatTriggerType.ClientID%>").trigger("change");

            if ($("#<%=hdnShowRepeat.ClientID%>").val() == "1") {
                ShowRepeatDetail()
            }
            else {
                HideRepeatDetail()
            }

            if ($("#<%=hdnShowAlert.ClientID%>").val() == "1") {
                ShowAlertDetail()
            }
            else {
                HideAlertDetail()
            }

        });


        $("#<%=ddlRepeatTriggerType.ClientID%>").live("change", function () {

            var valdiatorRepeatInDays = document.getElementById("<%=rfRepeatInDays.ClientID%>");

            var repetValue = $(this).val();
            if (repetValue == "F") {
                $("#dvFixedDate").show();
                $("#dvRepeatInDays").hide();

                //ValidatorEnable(valdiatorRepeatInDays, false);
                
                $('#<%= txtRepeatInDays.ClientID %>').hide();
                //ValidatorEnable($('#<%= rfRepeatInDays.ClientID %>'), false);
                ValidatorEnable($("#<%= rfRepeatInDays.ClientID %>")[0], false);
            }
            else {
                $("#dvFixedDate").hide();
                $("#dvRepeatInDays").show();

                //ValidatorEnable(valdiatorRepeatInDays, true);
                $('#<%= txtRepeatInDays.ClientID %>').show();
                //ValidatorEnable($('#<%= rfRepeatInDays.ClientID %>'), true);
                ValidatorEnable($("#<%= rfRepeatInDays.ClientID %>")[0], true);
                return;
            }
        });


        // To Define Radio Button Repeated Yes Change Event
        $("#<%=rdRepeatRequiredYes.ClientID%>").live("change", function () {
            $("#dvRepeatDetail").show();
        });


        // To Define Radio Button Repeated No Change Event
        $("#<%=rdRepeatRequiredNo.ClientID%>").live("change", function () {
            $("#dvRepeatDetail").hide();
        });


        // To Define Radio Button Repeated Yes Change Event
        $("#<%=rdAlertRequiredYes.ClientID%>").live("change", function () {
            $("#dvAlert").show();
        });


        // To Define Radio Button Repeated No Change Event
        $("#<%=rdAlertRequiredNo.ClientID%>").live("change", function () {
            $("#dvAlert").hide();
        });

        // To Define Document Ready Function
        $(document).ready(function () {
            $("#<%=txtReportingKeyWords.ClientID%>").focus();
        });


        function ShowRepeatDetail() {

            $("#dvRepeatDetail").show();
        }

        function HideRepeatDetail() {
            $("#dvRepeatDetail").hide();
        }

        function ShowAlertDetail() {
            $("#dvAlert").show();
        }

        function HideAlertDetail() {
            $("#dvAlert").hide();
        }


        function AddEvent(eventID, roleID, message) {
            //window.location.href = "RoleEventDefault.aspx?roleID=" + roleID + "&eventID=" + eventID + "";
            var title = "<%=Resources.Resource.lblConfirmation%>";
            var messageText = message.replace(new RegExp("{-", "gm"), '"').replace(new RegExp("{_", "gm"), "'");
            okButtonText = "OK";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "TriggerRoleAddEventOkButton('" + eventID + "', '" + roleID + "');";
            LaterCnclButtonRedirectLink = " HideConfirmationDialog();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Trigger Role Add Event Ok Button
        function TriggerRoleAddEventOkButton(eventID, roleID) {
            ShowPensivoWaitingMessage('"<%=Resources.Resource.lblRoleModifyPleaseWait%>"');
            $("#<% =btnCreateEvent.ClientID %>").trigger("click");
            //var datatoPost = {};
            //datatoPost.isAjaxCall = 1;
            //datatoPost.callBack = "addEvent";
            //datatoPost.EventID = eventID;
            //datatoPost.RoleID = roleID;
            //$.post("RoleAddEvent.aspx", datatoPost, function (data) {
            //    if (data == "ok") {
            //        //ShowPensivoMessage("<%=Resources.Resource.msgEventAddedToRole%>", "TraningEventRoleDashBoard.aspx?roleID=" + roleID + "");
            //        //window.location.href = "TraningEventRoleDashBoard.aspx?roleID=" + roleID;
            //        window.location.href = "RoleEventDefault.aspx?roleID=" + roleID + "&eventID=" + eventID + "";
            //    }
            //    else {
            //        ShowPensivoMessage("<%=Resources.Resource.msgCouldNotAddEvent%>")
            //    }
            //});
        }


        function ValidateDate(sender, args) {
            var dateString = document.getElementById(sender.controltovalidate).value;
            var regex = /(((0)[1-9]|(1)[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/((19|20)\d\d))$/;
            if (regex.test(dateString)) {
                var parts = dateString.split("/");
                var dt = new Date(parts[1] + "/" + parts[0] + "/" + parts[2]);
                args.IsValid = (dt.getDate() == parts[0] && dt.getMonth() + 1 == parts[1] && dt.getFullYear() == parts[2]);
            } else {
                args.IsValid = false;
            }
        }

    </script>
</asp:Content>


