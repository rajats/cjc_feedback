﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AssignTrainingEventSearchUserDashboard.aspx.cs" Inherits="AssignTrainingEventSearchUserDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div id="dvUserToAccess" class="boxset-box" runat="server">
                    <a id="hrfUserToAccess" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSelectUserToAccess%></h4>
                        </div>
                    </a>
                </div>
                <div id="dvSiteToAccess" class="boxset-box" runat="server">
                    <a id="hrfSiteToAccess" runat="server" href="#" class="boxset-box-wrapper"><%--is-disabled AccessDataDashBoard.aspx--%>
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSelectSiteToAccess%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>