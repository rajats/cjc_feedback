﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeeCrossDocklist.aspx.cs" Inherits="EmployeeCrossDocklist" %>

<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada )  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
        <%else if (Utils.TrainingInst == (int)Institute.EDE2 )  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>

    <%--Define Inline Css to Wrap Grid Header and Row Data--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th div {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
        }

        .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
            vertical-align: text-top;
        }
    </style>

    <section id="main-content" class="pg-advanced-list">
        <div class="wrapper">
            <h1>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h1>
            <aside class="column span-4 fixed-onscroll">
                <div class="search-box" style="width: 384px; left: 216px;">
                    <header class="search-box-header">
                        <h2 class="h6">
                            <asp:Literal ID="ltrSearchTitle" runat="server"></asp:Literal></h2>
                        <p class="last-child">
                            <asp:Literal ID="ltrSearchMessage" runat="server"></asp:Literal>
                        </p>
                    </header>
                    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
                        <div class="search-box-body">
                            <div class="plms-input-group">
                                <div class="plms-fieldset">
                                    <asp:Label ID="lblRoleName" AssociatedControlID="txtRoleName" class="filter-key plms-label is-hidden no-height" runat="server"
                                        Text="<%$ Resources:Resource, lblRoleName %>" for="name"></asp:Label>
                                    <asp:TextBox ID="txtRoleName" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblRoleName %>">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <input id="btnSearch" class="btn fluid clearfix" type="button" style="width: 100%;" value="<%=Resources.Resource.lbSearch%>" />
                        </div>
                    </asp:Panel>
                </div>
            </aside>
            <div class="main-content-body column span-8" onkeypress="return disableEnterKey(event)">
                <div id="grid_wrapper" style="width: 100%;">
                    <trirand:JQGrid runat="server" ID="gvSite" Height="300px"
                        AutoWidth="True" OnDataRequesting="gvSite_DataRequesting" OnCellBinding="gvSite_CellBinding">
                        <Columns>
                            <trirand:JQGridColumn DataField="empHdrID" Visible="false" PrimaryKey="True" />
                            <trirand:JQGridColumn DataField="empExtID" HeaderText="<%$ Resources:Resource, lblEmpoyeeID %>"
                                Editable="false" Width="200" />
                            <trirand:JQGridColumn DataField="empFirstName" HeaderText="<%$ Resources:Resource, lblEmpFirstName %>"
                                Editable="false" Width="200" />
                            <trirand:JQGridColumn DataField="empLastName" HeaderText="<%$ Resources:Resource, lblEmpLastName %>"
                                Editable="false" Width="200" />
                            <trirand:JQGridColumn DataField="empSite" HeaderText="<%$ Resources:Resource, lblSiteNumber %>"
                                Editable="false" Width="100" />
                            <trirand:JQGridColumn DataField="InclSiteReorting" HeaderText="<%$ Resources:Resource, lblSiteNumberUserIsIncludedInForSiteReporting %>"
                                Editable="false" Width="150" />
                        </Columns>
                        <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn=""></SortSettings>
                        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                        <ClientSideEvents LoadComplete="loadComplete" />
                    </trirand:JQGrid>
                </div>

                <footer class="form-footer">
                    <div class="btngrp align-r">
                        <a id="hrfNext" runat="server" href="#nogo" class="btn round" onclick="ClosePage();">CLOSE</a>
                    </div>
                </footer>
            </div>
        </div>
    </section>
    <asp:HiddenField ID="hdnSiteNoUser" runat="server" Value="" />
    <asp:HiddenField ID="hdnListID" runat="server" Value="" />

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvSite.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });


        // Resized JQ Grid
        function jqGridResize() {
            $("#<%=gvSite.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // Define JQ Grid Load Complete and Load Saved JQ Grid Selected value on Page Changes
        function loadComplete(data) {
            $("#cb_ContentPlaceHolder1_gvSite").hide();
            jqGridResize();
        }


        // To Call User Canceled Confirmation Message
        function ConfirmAddRoleUsrCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");

        }

        // To Go To Site Manager Dashboard
        function SiteManagerDashboard(siteID, roleManageListID, searchBy, functionalityID, listID) {
            window.location.href = "EmployeeSiteMangerDashboard.aspx?siteID=" + siteID + "&roleManageListID=" + roleManageListID + "&SearchBy=" + searchBy + "&FunctionalityID=" + functionalityID + "&ListID=" + listID;
        }

        // To Trigger Event on Document Ready
        $(document).ready(function () {
            $("#<%=txtRoleName.ClientID%>").focus();
        });

            // To Define Page Close Event
            function ClosePage() {
                window.location.href = $("#hrefHomeNavigation").attr("href");
            }
    </script>
</asp:Content>
