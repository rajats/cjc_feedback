﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Pagewithoutheader.master" AutoEventWireup="true" CodeFile="CertificateDownload.aspx.cs" Inherits="CertificateDownload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
     <section id="main-content"  class="pg-dashboard">
 
 
    <div class="wrapper" style="display:none;">

        <h1><%=Resources.Resource.lblCertificateDownload %></h1>

        <p><%=Resources.Resource.lblCertificateWhichCourse %></p>

        <p>Please select course, then click "Download PDF".</p>

        <div class="plms-horlist">
            <ul>
                <%=sHtml %>
            </ul>
        </div>
        <asp:Button ID="btnCreateEmployee" class="btn large btn-create-user " runat="server" Text="<%$ Resources:Resource, btnDownload %>" OnClientClick="return CheckCourseSelected();" OnClick="btnDonloadPDF_OnClick" />
        <asp:HiddenField id="hdnSelectedValue" runat="server" Value="" />
    </div>

    </section>

    <script type="text/javascript">
        $('li').click(function (e) {

            $("li").each(function () {
                if ($(this).hasClass("is-selected") == true) {
                    $(this).removeClass("is-selected");
                    if ($(this).find("span.plms-icon.size-24").hasClass("checkbox-checked") == true) {
                        $(this).find("span.plms-icon.size-24").removeClass("checkbox-checked checkbox-checked-white is-selected");
                        $(this).find("span.plms-icon.size-24").addClass("checkbox-unchecked");
                    }
                }
            });


            if ($(this).hasClass("is-selected") == true) {
                $(this).removeClass("is-selected");
                if ($(this).find("span.plms-icon.size-24").hasClass("checkbox-checked") == true) {
                    $(this).find("span.plms-icon.size-24").removeClass("checkbox-checked checkbox-checked-white is-selected");
                    $(this).find("span.plms-icon.size-24").addClass("checkbox-unchecked");
                }
            }
            else {
                $(this).addClass("is-selected");
                $(this).find("span.plms-icon.size-24").addClass("checkbox-checked checkbox-checked-white is-selected");
            }
        });


        function CheckCourseSelected() {
            var isSelected = false;
            var selectedValue = "";

            $("li").each(function () {
                if ($(this).hasClass("is-selected") == true) {
                    isSelected = true;

                    if (typeof $(this).attr("regID") === 'undefined') {
                    }
                    else {
                        if (selectedValue == '') {
                            selectedValue = $(this).attr("regID");
                        }
                        else {
                            selectedValue += "," + $(this).attr("regID");
                        }
                    }
                }

            });

            if (selectedValue != "") {
                $("#<%= hdnSelectedValue.ClientID %>").val(selectedValue);
            }

            if (isSelected == false) {
                ShowPensivoMessage("<%=Resources.Resource.lblPleaseSelectCourseToDownload %>");
            }
            return isSelected;
        }

    </script>

</asp:Content>

