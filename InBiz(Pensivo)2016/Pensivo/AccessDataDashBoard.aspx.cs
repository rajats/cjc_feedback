﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class AccessDataDashBoard : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblCanAccessSpecificData, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            Role objRole = new Role();
            if (objRole.IsAllowAllSpecificAccessData(this.RoleID, this.FunctionalityID, this.FunctionalityType) == false)
            {
                objRole.RoleAddFunctionalityAccessAllSpecificData(this.RoleID, this.FunctionalityID, this.FunctionalityType, false, true);
            }

            hrfSearchEmpSite.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.Store + "&setRole=1";
            hrfSearchEmpRegion.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.JobCode + "&setRole=1";

            if (Utils.TrainingInst == (int)Institute.bdl)
            {
                hSite.InnerHtml = Resources.Resource.lblDeptID;
            }
            else
            {
                hSite.InnerHtml = Resources.Resource.lblSearchSite;
            }
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Functionality ID
    /// </summary>
    public int FunctionalityID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Functionality Type
    /// </summary>
    public string FunctionalityType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["functionalityType"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

}