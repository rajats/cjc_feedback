﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class ViewUser : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvUser))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblEditViewUser, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            if (IsEdit)
            {
                ltrTitle.Text = Resources.Resource.lblEditUser;
            }
            else
            {
                ltrTitle.Text = Resources.Resource.lblViewUser;
                gvUser.Columns[6].Visible = false;
            }
        }

        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
        {
            lblSearchText.Text = Resources.Resource.lblTDCViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblTDCViewUserSearchTextHolder);

            gvUser.Columns[4].Visible = false;
            gvUser.Columns[5].Visible = false;
            gvUser.Columns[3].HeaderText = Resources.Resource.lblEmpEmailID;
        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            lblSearchText.Text = Resources.Resource.lblBDLViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblBDLViewUserSearchTextHolder);
        }
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            lblSearchText.Text = Resources.Resource.lblEDE2ViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblEDE2ViewUserSearchTextHolder);
        }
        else
        {
            lblSearchText.Text = Resources.Resource.lblTBSViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblTBSViewUserSearchTextHolder);
        }
    }

    /// <summary>
    ///  To Define JQ Grid Cell Binding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event Args</param>
    protected void gvUser_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 6)
        {
            e.CellHtml = string.Format(@"<a href=""javascript:;""  onclick=""EditUser({0})"">" + Resources.Resource.lblEdit + "</a>", e.CellHtml);
        }
    }

    /// <summary>
    /// To Bind JQ Grid with User List
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvUser_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Employee objEmp = new Employee();
        string txtName = Utils.ReplaceDBSpecialCharacter(BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]));
        objEmp.EmpName = txtName;
        objEmp.ExcludeReportUser = false;
        gvUser.DataSource = objEmp.GetEmployeeList();
        gvUser.DataBind();
    }

    /// <summary>
    /// To Get Is User Edited or Added
    /// </summary>
    public Boolean IsEdit
    {
        get
        {
            if (BusinessUtility.GetInt(Request.QueryString["isEdit"]) == 1)
                return true;
            else
                return false;
        }
    }

}