﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;

public partial class AssignTrainingAdditionalAddUsers : BasePage
{



    Employee objEmp = new Employee();
    Event objEvent = new Event();
    Role objRole = new Role();


    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Assign_Training);
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAssignTrainingAdditionalUsers, Path.GetFileName(Request.Url.AbsolutePath), Path.GetFileName(Request.Url.AbsolutePath));
        }
        hAdditionalUsers.InnerHtml = Resources.Resource.lblAssignTrainingSelectAdditionalUsers;



        string sTrainingEventID = this.TrainingEventIDs;
        string sTrainingEventAccessUserID = "";

        if (this.SearchBy == "")
        {
            if (BusinessUtility.GetString(Session["TrainningEventUsersCanAccess"]) != "")
            {

                sTrainingEventAccessUserID = BusinessUtility.GetString(Session["TrainningEventUsersCanAccess"]) + "," + this.TrainingEventAccessUserID;
            }
            else
            {
                sTrainingEventAccessUserID = this.TrainingEventAccessUserID;
            }
        }
        else
        {
            if (BusinessUtility.GetString(Session["TrainningEventUsersCanAccess"]) != "")
            {

                sTrainingEventAccessUserID = BusinessUtility.GetString(Session["TrainningEventUsersCanAccess"]) + "," + this.TrainingEventAccessUserID;
            }
            else
            {
                sTrainingEventAccessUserID = this.TrainingEventAccessUserID;
            }
        }

        hdnAssignByUserID.Value = BusinessUtility.GetString(CurrentEmployee.EmpID);
        hdnTrainingEventAccessUserID.Value = sTrainingEventAccessUserID;
        hdnTrainingEvent.Value = sTrainingEventID;
        Session["TrainningAssignEventID"] = sTrainingEventID;


        string[] arrTrainingEventAccessUserID = sTrainingEventAccessUserID.Split(',');
        arrTrainingEventAccessUserID = arrTrainingEventAccessUserID.Distinct().ToArray();

        hdnTrainingEventAccessUserID.Value = string.Join(",", arrTrainingEventAccessUserID);


        if (arrTrainingEventAccessUserID.Length > 0)
        {
            if (arrTrainingEventAccessUserID.Length > 1)
            {

                string sMessage = "";


                string sTrainingEventName = "";
                string[] searchval = sTrainingEventID.Split(',');
                foreach (string sValues in searchval)
                {
                    objEvent = new Event();
                    objEvent.GetEventDetail(BusinessUtility.GetInt(sValues), Globals.CurrentAppLanguageCode);
                    if (sTrainingEventName == "")
                    {
                        sTrainingEventName = objEvent.EventName;
                    }
                    else
                    {
                        sTrainingEventName += "<br/>" + objEvent.EventName;
                    }
                }


                string sUserName = "";


                string sEventID = "";
                string[] arrEvent = BusinessUtility.GetString(hdnTrainingEventAccessUserID.Value).Split(',');
                foreach (string sValues in arrEvent)
                {
                    if (sValues != "")
                    {
                        if (sEventID == "")
                        {
                            sEventID = BusinessUtility.GetString(sValues);
                        }
                        else
                        {
                            sEventID +=","+ BusinessUtility.GetString(sValues);
                        }
                    }
                }
                DataTable dt = objEmp.GetEmployeeListEmpIDIn(sEventID);
                //DataTable dt = objEmp.GetEmployeeListEmpIDIn(hdnTrainingEventAccessUserID.Value);


                foreach (DataRow dRow in dt.Rows)
                {
                    if (sUserName == "")
                    {
                        sUserName = BusinessUtility.GetString(dRow["EmpName"]) + " " + "(" + BusinessUtility.GetString(dRow["EmpExtID"]) + ")";
                    }
                    else
                    {
                        sUserName += "<br/>" + BusinessUtility.GetString(dRow["EmpName"]) + " " + "(" + BusinessUtility.GetString(dRow["EmpExtID"]) + ")";
                    }
                }


                sMessage = Resources.Resource.lblMsgAssignTrainingEventAddUserConfirmationMultipleUsers.Replace("#USERCOUNT#", BusinessUtility.GetString(arrTrainingEventAccessUserID.Length))
                .Replace("#EVENTNAME#", sTrainingEventName)
                .Replace("#USERSNAME#", sUserName);

                HdnMessage.Value = sMessage;

            }
            else
            {
                int empID = BusinessUtility.GetInt(arrTrainingEventAccessUserID[0]);
                if (empID > 0)
                {
                    objEmp = new Employee();
                    objEvent = new Event();
                    objEmp.GetEmployeeDetail(BusinessUtility.GetInt(arrTrainingEventAccessUserID[0]));

                    string sMessage = "";
                    sMessage = Resources.Resource.lblMsgAssignTrainingEventAddUserConfirmation.Replace("#Name#", objEmp.EmpName + " (" + objEmp.EmpLogInID + ")");

                    string sTrainingEventName = "";
                    string[] searchval = sTrainingEventID.Split(',');
                    foreach (string sValues in searchval)
                    {
                        objEvent = new Event();
                        objEvent.GetEventDetail(BusinessUtility.GetInt(sValues), Globals.CurrentAppLanguageCode);
                        if (sTrainingEventName == "")
                        {
                            sTrainingEventName = objEvent.EventName;
                        }
                        else
                        {
                            sTrainingEventName += "<br/>" + objEvent.EventName;
                        }
                    }
                    HdnMessage.Value = sMessage.Replace("#EVENTNAME#", sTrainingEventName);
                }
            }
        }

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "assignUserTrainingEvent")
        {
            try
            {
                string userID = BusinessUtility.GetString(Request.Form["UserID"]);
                string trainingEnventID = BusinessUtility.GetString(Request.Form["TrainingEnventID"]);
                int assignByUserID = BusinessUtility.GetInt(Request.Form["AssignByUserID"]);

                ErrorLog.createLog("User ID " + userID + " TEvent ID " + trainingEnventID + " assignByUserID" + BusinessUtility.GetString(assignByUserID));

                if (trainingEnventID != "" && userID != "")
                {
                    string[] arrTrainingEvent = trainingEnventID.Split(',');
                    string[] arrTrainingAccessUserID = userID.Split(',');
                    foreach (string sTrainingAccessUserID in arrTrainingAccessUserID)
                    {
                        foreach (string sTrainingEvent in arrTrainingEvent)
                        {
                            if ((BusinessUtility.GetInt(sTrainingEvent) > 0) && (BusinessUtility.GetInt(sTrainingAccessUserID) > 0))
                            {
                                objRole = new Role();
                                objRole.AssignUserTrainingEvent(BusinessUtility.GetInt(sTrainingAccessUserID), BusinessUtility.GetString(sTrainingEvent), assignByUserID);

                                ExecutingThread.ThreadAssignedCoursesByEmployee(BusinessUtility.GetInt(sTrainingAccessUserID), BusinessUtility.GetInt(sTrainingEvent), (int)AssignedCourseInActiveReason.BySysAdminRole, (int)AssignedCourseUpdatedSource.ByAssignCourseRoleUpdate);
                            }
                        }
                    }
                    Response.Write("ok");
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }
            Response.End();
            Response.SuppressContent = true;
        }
    }

    /// <summary>
    /// To Get Training Event ID(s) Spreated by ","
    /// </summary>
    public string TrainingEventIDs
    {
        get
        {
            if ((BusinessUtility.GetString(Request.QueryString["trainingeventid"]) != ""))
            {
                return BusinessUtility.GetString(Request.QueryString["trainingeventid"]);
            }
            else
            {
                return BusinessUtility.GetString(BusinessUtility.GetString(Session["TrainningAssignEventID"]));
            }
        }
    }

    /// <summary>
    /// To Get User IDs To Access Event ID
    /// </summary>
    public string TrainingEventAccessUserID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["trainingeventaccessuserID"]);
        }
    }


    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchvalue"]);
        }
    }

    /// <summary>
    /// To Add additional User
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnSelectAdditionalUser_Click(object sender, EventArgs e)
    {
        if (this.SearchBy == "")
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(Session["TrainningEventUsersCanAccess"])))
            {
                Session["TrainningEventUsersCanAccess"] = BusinessUtility.GetString(this.TrainingEventAccessUserID);
            }
            else
            {
                Session["TrainningEventUsersCanAccess"] = BusinessUtility.GetString(Session["TrainningEventUsersCanAccess"]) + "," + BusinessUtility.GetString(this.TrainingEventAccessUserID);
            }
            Response.Redirect("AssignTrainingEventByEmpCode.aspx?trainingeventid=" + this.TrainingEventIDs, false);
        }
        else
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(Session["TrainningEventUsersCanAccess"])))
            {
                Session["TrainningEventUsersCanAccess"] = BusinessUtility.GetString(this.TrainingEventAccessUserID);
            }
            else
            {
                Session["TrainningEventUsersCanAccess"] = BusinessUtility.GetString(Session["TrainningEventUsersCanAccess"]) + "," + BusinessUtility.GetString(this.TrainingEventAccessUserID);
            }
            Response.Redirect("AssignTrainingSiteUsers.aspx?trainingeventid=" + this.TrainingEventIDs + "&searchby=" + this.SearchBy + "&searchvalue=" + this.SearchValue + "");
        }
    }
}