﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AddReportingDashboard.aspx.cs" Inherits="AddReportingDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div id="dvAccessAllData" class="boxset-box" runat="server" visible="false">
                    <a id="hrfAccessAllData" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblCanAccessAllDataforReporting%></h4>
                        </div>
                    </a>
                </div>
                <div id="dvAccessAllspecificData" class="boxset-box" runat="server" visible="false">
                    <a id="hrfAccessSpecificData" runat="server" href="#" class="boxset-box-wrapper "><%--is-disabled AccessDataDashBoard.aspx--%>
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblCanAccessSpecificData%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnRoleID" runat="server" />
        <asp:HiddenField ID="hdnActionID" runat="server" />
        <asp:HiddenField ID="hdnActionType" runat="server" />
        <asp:HiddenField ID="hdnFlag" runat="server" />
        <asp:HiddenField ID="hdnMessage" runat="server" />
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Remove Action Confirmation
        function RemoveAction() {
            var title = "Confirmation";
            var messageText = ($("#<%=hdnMessage.ClientID%>").val());
            okButtonText = "OK";
            LaterCnclButtonText = "Cancel";
            okButtonRedirectlink = "ConfirmRemoveActionOk()";
            LaterCnclButtonRedirectLink = "ConfirmRemoveActionCancel();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Trigger Remove Action OK Button
        function ConfirmRemoveActionOk() {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "removeAction";
            datatoPost.ActionID = $("#<%=hdnActionID.ClientID%>").val();
           datatoPost.ActionType = $("#<%=hdnActionType.ClientID%>").val();
            datatoPost.RoleID = $("#<%=hdnRoleID.ClientID%>").val();
            $.post("AddReportingDashboard.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    window.location.href = "roleActivity.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val();
                }
                else {
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotRemoveFunctionality%>");
                }
           });
        }

        // To Trigger Remove Action Cancel Button
        function ConfirmRemoveActionCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
        }

        // To Allo Access All Data Confirmation
        function AllowAllAccessData() {
            var title = "<%=Resources.Resource.lblConfirmation%>";
            var messageText = ($("#<%=hdnMessage.ClientID%>").val());
            okButtonText = "OK";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "TriggerAllAccessData();";
            LaterCnclButtonRedirectLink = " HideConfirmationDialog();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Trigger All Access Data Confirmation OK Button
        function TriggerAllAccessData() {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "addAction";
            datatoPost.ActionID = $("#<%=hdnActionID.ClientID%>").val();
            datatoPost.ActionType = $("#<%=hdnActionType.ClientID%>").val();
            datatoPost.RoleID = $("#<%=hdnRoleID.ClientID%>").val();
            $.post("AddReportingDashboard.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    window.location.href = "roleActivity.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val();
                }
                else {
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotAddFunctionality%>")
                }
            });
        }

    </script>
</asp:Content>

