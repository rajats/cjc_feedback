﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Data;

public partial class RoleUserList : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvUser))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblRoleDetailUsers, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            //gvUser.Columns[6].Visible = false;
        }

        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
        {
            lblSearchText.Text = Resources.Resource.lblTDCViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblTDCViewUserSearchTextHolder);

            //gvUser.Columns[4].Visible = false;
            //gvUser.Columns[5].Visible = false;
            gvUser.Columns[3].HeaderText = Resources.Resource.lblEmpEmailID;
        }
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            lblSearchText.Text = Resources.Resource.lblEDE2ViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblEDE2ViewUserSearchTextHolder);
        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            lblSearchText.Text = Resources.Resource.lblBDLViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblBDLViewUserSearchTextHolder);
        }
        else
        {
            lblSearchText.Text = Resources.Resource.lblTBSViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblTBSViewUserSearchTextHolder);
        }
    }

    /// <summary>
    ///  To Define JQ Grid Cell Binding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event Args</param>
    protected void gvUser_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        //if (e.ColumnIndex == 6)
        //{
        //    e.CellHtml = string.Format(@"<a href=""javascript:;""  onclick=""EditUser({0})"">" + Resources.Resource.lblEdit + "</a>", e.CellHtml);
        //}
    }

    /// <summary>
    /// To Bind JQ Grid with User List
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvUser_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //Employee objEmp = new Employee();
        //string txtName = Utils.ReplaceDBSpecialCharacter(BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]));
        //objEmp.EmpName = txtName;
        //objEmp.ExcludeReportUser = false;
        //gvUser.DataSource = objEmp.GetEmployeeList();
        //gvUser.DataBind();

        Role objRole = new Role();
        DataTable dtRoleDetailList = objRole.GetRoleEmployeeDetailsList(this.RoleID, this.RefCode, this.RefCodeValue);
        //string sFilterCol = "";

        //if (this.RefCode != "")
        //{
        //    if (this.RefCode == EmpRefCode.Store)
        //    {
        //        sFilterCol = "SiteNumber";
        //    }
        //    else if (this.RefCode == EmpRefCode.Region)
        //    {
        //        sFilterCol = "Region";
        //    }
        //    else if (this.RefCode == EmpRefCode.JobCode)
        //    {
        //        sFilterCol = "JobCode";
        //    }
        //    else if (this.RefCode == EmpRefCode.Division)
        //    {
        //        sFilterCol = "empLocTypeValue";
        //    }

        //    else if (this.RefCode == EmpRefCode.DivisionTBS)
        //    {
        //        sFilterCol = "Division";
        //    }
        //    else if (this.RefCode == EmpRefCode.RegionTBS)
        //    {
        //        sFilterCol = "Region";
        //    }
        //    else if (this.RefCode == EmpRefCode.District)
        //    {
        //        sFilterCol = "District";
        //    }
        //    else if (this.RefCode == EmpRefCode.Site)
        //    {
        //        sFilterCol = "empSite";
        //    }
        //    else if (this.RefCode == EmpRefCode.Province)
        //    {
        //        sFilterCol = "Province";
        //    }
        //    else if (this.RefCode == EmpSearchBy.Type)
        //    {
        //        sFilterCol = "EmployeeType";
        //    }
        //}

        //if (sFilterCol != "")
        //{
        //    DataView dv = dtRoleDetailList.DefaultView;
        //    dv.RowFilter = " "+ sFilterCol +"  ='" + this.RefCodeValue + "' ";
        //    dtRoleDetailList = dv.ToTable();
        //}

        gvUser.DataSource = dtRoleDetailList;
        gvUser.DataBind();
    }

    /// <summary>
    /// To Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleid"]);
        }
    }

    /// <summary>
    /// To Get Ref Code
    /// </summary>
    public string RefCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["refCode"]);
        }
    }

    /// <summary>
    /// To Get Ref Code Value
    /// </summary>
    public string RefCodeValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["refcodeValue"]);
        }
    }
}