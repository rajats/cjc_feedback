﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AdministrationDashBoard.aspx.cs" Inherits="AdministrationDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper ">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box">
                    <a href="User.aspx" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblUser%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a href="RoleDashBoard.aspx" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblRole%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a href="TrainingDashBoard.aspx" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTraning%></h4>
                        </div>
                    </a>
                </div>
                <div id="dvManageList" runat="server" visible="false" class="boxset-box">
                    <a href="EmployeeManageList.aspx" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblManageLists%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

