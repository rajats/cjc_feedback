﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class RoleRemoveEvent : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvRoles))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblModifiyExistingTrainingEventToTheSystemRole, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            objRole = new Role();
            string sRoleName = objRole.GetRoleName(this.RoleID);
            ltrTitle.Text = Resources.Resource.lblExistingEventAssociateWithRole.Replace("#RoleName#", sRoleName);
        }

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeEvent")
        {
            try
            {
                int eventID = BusinessUtility.GetInt(Request.Form["EventID"]);
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                if (eventID > 0 && roleID > 0)
                {
                    List<Role> lRoleUser = new List<Role>();
                    lRoleUser.Add(new Role { EventID = eventID });
                    objRole = new Role();
                    if (objRole.RoleRemoveEvent(roleID, lRoleUser) == true)
                    {
                        //ExecutingThread.ThreadRemoveEmployeeCoursesByRole(roleID, eventID);
                        ExecutingThread.ThreadEmployeeAssignedCoursesByRole(roleID, eventID, (int)AssignedCourseInActiveReason.BySysAdminRole, (int)AssignedCourseUpdatedSource.BySysAdminRoleUpdate);
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }
            Response.End();
            Response.SuppressContent = true;
        }
    }

    /// <summary>
    /// To Define JQ Grid Cell Binding Event 
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event </param>
    protected void gvRoles_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            objRole = new Role();
            string sRoleName = objRole.GetRoleName(this.RoleID);
            string str = string.Empty;
            str += Resources.Resource.lblRoleRemoveEvent.Replace("#ROLENAME#", sRoleName).Replace("#EVENTNAME#", BusinessUtility.GetString(e.RowValues[1]));
            string sMessage = str.Replace("\"", "{-").Replace("'", "{_");
            e.CellHtml = string.Format(@"<a href=""javascript:;""  onclick=""RemoveEvent({0},{1},'{2}')"">" + Resources.Resource.lblRemove + "</a>", e.CellHtml, this.RoleID, sMessage);
        }
    }

    /// <summary>
    /// To Bind JQ Grid With Event List Added with the Role
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Role objRole = new Role();
        string txtRole = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        gvRoles.DataSource = objRole.GetEventInRole(this.RoleID, Globals.CurrentAppLanguageCode, Utils.ReplaceDBSpecialCharacter(txtRole));
        gvRoles.DataBind();
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }
}