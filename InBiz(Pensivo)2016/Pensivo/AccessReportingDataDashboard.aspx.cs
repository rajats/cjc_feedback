﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;

public partial class AccessReportingDataDashboard : BasePage
{
    /// <summary>
    /// To Hold Html Content
    /// </summary>
    protected string htmlText = "";

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAccessReportType, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            htmlText = loadEmpSearchValue();
        }
    }

    /// <summary>
    /// To Get Reporting HTML Content
    /// </summary>
    /// <returns>String</returns>
    private string loadEmpSearchValue()
    {
        string sbHtml = "";
        Role objRole = new Role();
        DataTable dt = objRole.ReportTypeListExistsInRole(this.RoleID);
        ReportType objReportType = new ReportType();
        List<ReportType> reportFilterList = objReportType.GetReportType();

        foreach (DataRow dr in dt.Rows)
        {
            var resultItem = reportFilterList.Where(x => x.ReportName == BusinessUtility.GetString(dr["reportType"])).ToList();
            if (resultItem != null && resultItem.Count > 0)
            {
                foreach (var removeItem in resultItem)
                {
                    reportFilterList.Remove(removeItem);
                }
            }
        }

        if (reportFilterList != null && reportFilterList.Count > 0)
        {
            string sHeader = "";
            int i = 0;
            foreach (var filterItem in reportFilterList)
            {
                sbHtml += " <div class='boxset' id='dvAdminDashBoard" + BusinessUtility.GetString(i) + "'> ";
                for (int j = 0; j <= 2; j++)
                {
                    if (i < reportFilterList.Count)
                    {
                        {
                            string sText = "";
                            string sValue = "";
                            string sFunction = "";

                            if (reportFilterList[i].ReportName.ToUpper() == EmpSearchBy.Store.ToUpper())
                            {
                                //sText = Resources.Resource.lblSearchSite;
                                sText = Resources.Resource.lblSiteReportingRetailWholesalesLocation;
                                sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.Store + "&setRole=1";
                                sFunction = "href='" + sValue + "'  ";
                            }
                            else if (reportFilterList[i].ReportName.ToUpper() == EmpSearchBy.SiteLogisticsReport.ToUpper())
                            {
                                sText = Resources.Resource.lblLogisticsSiteReporting;
                                sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.SiteLogisticsReport + "&setRole=1";
                                sFunction = "href='" + sValue + "'  ";
                            }
                            else if (reportFilterList[i].ReportName.ToUpper() == EmpSearchBy.District.ToUpper())
                            {
                                sText = Resources.Resource.lblSearchDistrict;
                                sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.District + "&setRole=1";
                                sFunction = "href='#' onclick='ShowMessageNotAvailable();' class='boxset-box-wrapper is-disabled' ";
                            }
                            else if (reportFilterList[i].ReportName.ToUpper() == EmpSearchBy.DivisionTBS.ToUpper())
                            {
                                sText = Resources.Resource.lblSearchDivision;
                                sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.DivisionTBS + "&setRole=1";
                                sFunction = "href='#' onclick='ShowMessageNotAvailable();' class='boxset-box-wrapper is-disabled' ";
                            }

                            else if (reportFilterList[i].ReportName.ToUpper() == EmpSearchBy.RegionTBS.ToUpper())
                            {
                                sText = Resources.Resource.lblSearchRegion;
                                sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.RegionTBS + "&setRole=1";
                                sFunction = "href='#' onclick='ShowMessageNotAvailable();'  class='boxset-box-wrapper is-disabled'";
                            }
                            else if (reportFilterList[i].ReportName.ToUpper() == EmpSearchBy.Division.ToUpper())
                            {
                                sText = Resources.Resource.lblSearchLocation;
                                sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.Division + "&setRole=1";
                                sFunction = "href='#' onclick='ShowMessageNotAvailable();' class='boxset-box-wrapper is-disabled' ";
                            }

                            sbHtml += " <div class='boxset-box'> ";
                            sbHtml += " <a id='hrfSearchEmpName' runat='server' " + sFunction + "  class='boxset-box-wrapper'> "; //" + sFunction + "       //href='#' " + sFunction + " // href='" + sValue + "'
                            sbHtml += " <div class='boxset-box-inner'> ";
                            sbHtml += " <h4 class='boxset-title'> " + sText + " </h4> ";
                            sbHtml += " </div> ";
                            sbHtml += " </a> ";
                            sbHtml += " </div> ";
                            if (j <= 1)
                            {
                                i += 1;
                            }
                        }
                    }
                }
                sbHtml += " </div> ";
                i += 1;
            }
        }
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Functionality ID
    /// </summary>
    public int FunctionalityID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Functionality Type
    /// </summary>
    public string FunctionalityType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["functionalityType"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

}