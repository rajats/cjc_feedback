﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="FeedbackUser.aspx.cs" Inherits="FeedbackUser" %>

<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.EDE2)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>

    <section id="main-content" class="pg-advanced-list">
        <div class="wrapper">
            <h1><%=Resources.Resource.lblFeedbackUserTitle %></h1>
            <aside class="column span-4 fixed-onscroll">
                <div class="search-box" style="width: 384px; left: 216px;">
                    <header class="search-box-header">
                        <h2 class="h6"><%=Resources.Resource.lbFindUsers %></h2>
                        <p class="last-child"><%=Resources.Resource.lblFindUserMessage %></p>
                    </header>
                    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
                        <div class="search-box-body">
                            <div class="plms-input-group">
                                <div class="plms-fieldset">
                                    <asp:Label ID="lblUserName" AssociatedControlID="txtRoleName" class="filter-key plms-label is-hidden no-height" runat="server"
                                        Text="<%$ Resources:Resource, lblUserName %>" for="name"></asp:Label>
                                    <asp:TextBox ID="txtRoleName" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblUserName %>">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <input id="btnSearch" class="btn fluid clearfix" type="button" style="width: 100%;" value="<%=Resources.Resource.lbSearch%>" />
                        </div>
                    </asp:Panel>
                </div>
            </aside>
            <div class="main-content-body column span-8" onkeypress="return disableEnterKey(event)">
                <div id="grid_wrapper" style="width: 100%;">
                    <trirand:JQGrid runat="server" ID="gvEmployee" Height="300px" MultiSelect="true" multiboxonly="true"
                        AutoWidth="True" OnDataRequesting="gvEmployee_DataRequesting">
                        <Columns>
                            <trirand:JQGridColumn DataField="empHdrID" Visible="false" PrimaryKey="True" />
                            <trirand:JQGridColumn DataField="EmpFirstName" HeaderText="<%$ Resources:Resource, lblEmpFirstName %>"
                                Editable="false" Width="40" />
                            <trirand:JQGridColumn DataField="EmpLastName" HeaderText="<%$ Resources:Resource, lblEmpLastName %>"
                                Editable="false" Width="40" />
                                                        <trirand:JQGridColumn DataField="EmpExtID" HeaderText="<%$ Resources:Resource, lblEmpoyeeEmailID %>"
                                Editable="false" Width="100" />
                            <%--<trirand:JQGridColumn DataField="Department" HeaderText="<%$ Resources:Resource, lblDepartment %>"
                                Editable="false" Width="200" />--%>
                        </Columns>
                        <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn=""></SortSettings>
                        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                        <ClientSideEvents LoadComplete="loadComplete" />
                    </trirand:JQGrid>
                </div>
                <footer class="form-footer">
                    <div class="btngrp align-r">
                        <a href="#nogo" class="btn  btn-create-user " id="btnCancel" onclick="getCancel();"><%=Resources.Resource.btnCancel %></a>
                        <a href="#nogo" class="btn  btn-create-user " onclick="getSelectedRole();"><%=Resources.Resource.lblNext %></a>
                    </div>
                </footer>
                <%-- <a href="#nogo" class="btn align-r" onclick="getSelectedRole();"><%=Resources.Resource.lblNext %></a>--%>
            </div>
        </div>
        <asp:HiddenField  ID="hdnFeedbackUser" runat="server" />
        <asp:Button id="btnFeedbackUser" runat="server" style="display:none" OnClick="btnFeedbackUser_Click"/>


    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvEmployee.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        // Resized JQ Grid
        function jqGridResize() {
            $("#<%=gvEmployee.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // Define JQ Grid Load Complete
        function loadComplete(data) {
            $("#cb_ContentPlaceHolder1_gvEmployee").hide();
            jqGridResize();
        }



        function getCancel() {
            window.location.href = "FeedbackQuestion.aspx";
        }


        // To Get JQ Grid Selected Row Value
        function getSelectedRole() {
            var arrRooms = $grid.getGridParam('selarrrow');
            if (arrRooms.length <= 0) {
                ShowPensivoMessage("<%=Resources.Resource.reqPleaseSelectAvalue%>");
                return false;
            }
            else {
                //window.location.href = "ReportFilterSummary.aspx?roption=who&searchby=NAM&searchval=" + arrRooms.join(",");

                $("#<%=hdnFeedbackUser.ClientID%>").val(arrRooms.join(","));
                $("#<%=btnFeedbackUser.ClientID%>").trigger("click");
            }
        }

        $(document).ready(function () {
            $("#<%=txtRoleName.ClientID%>").focus();
        });
    </script>
</asp:Content>
