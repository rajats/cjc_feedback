﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleAdd.aspx.cs" Inherits="RoleAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper width-med">
            <h4>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h4>
            <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnCreateRole">
                <div class="form-body" onclick="HideMessage()">
                    <div id="dvLogInErrMsg" class="plms-alert invalid" runat="server" visible="false">
                        <p class="last-child"><%=Resources.Resource.errMesageRoleExists %></p>
                    </div>
                    <div class="plms-fieldset is-first">
                        <label class="plms-label is-hidden" for="txtRoleName"><%= Resources.Resource.lblRoleName%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtRoleName" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblRoleName %>" MaxLength="135" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblRoleName%></p>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfRoleName" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole"
                                ControlToValidate="txtRoleName" Text="<%$ Resources:Resource, lblRoleNameReq %>" />
                        </div>
                    </div>
                    <asp:Button ID="btnCreateRole" class="btn large btn-create-user " runat="server" ValidationGroup="grpRole" OnClientClick="return BtnClick();" OnClick="btnCreateRole_OnClick" />
                </div>
            </asp:Panel>
            <%--Define JavaScript Function--%>
            <script type="text/javascript">
                // To Validate Page
                function BtnClick() {
                    var val = Page_ClientValidate();
                    if (!val) {
                        var i = 0;
                        for (; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $("#" + Page_Validators[i].controltovalidate).addClass("has-error");
                                break;
                            }
                            else {
                                $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                            }
                        }
                    }
                    else {
                        var i = 0;
                        for (; i < Page_Validators.length; i++) {

                            {
                                $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                            }
                        }
                    }
                    return val;
                }

                // To Hide Message
                function HideMessage() {
                    $("#<%=dvLogInErrMsg.ClientID %>").hide();
                }


                $(document).ready(function () {
                    $("#<%=txtRoleName.ClientID%>").focus();
                       });
            </script>

        </div>
    </section>
</asp:Content>

