﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ALIAS.aspx.cs" Inherits="ALIAS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <asp:Panel ID="pnlContent" runat="server" DefaultButton="BtnLoginAsAnotherAuser">
            <div class="wrapper width-med">
                <h1><%= Resources.Resource.lblUserLogIN%>  </h1>
                <div class="boxed-content">
                    <div class="boxed-content-body">
                        <header class="form-section-header">
                            <h6 id="hTitle" runat="server"> </h6>
                            <div id="dvLogInErrMsg" class="plms-alert invalid" runat="server" visible="false" onclick="HideMessage();">
                                <p class="last-child">
                                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                                </p>
                            </div>
                        </header>
                        <div class="plms-fieldset" onkeypress="return disableEnterKey(event)">
                            <label class="plms-label is-hidden" for="first-name" id="lblUserName" runat="server"></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtUserName" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblUserName %>"></asp:TextBox>
                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtUserName" ID="valEmpIDExp" class="formels-feedback invalid"
                                    ValidationExpression="^[\s\S]{4,}$" runat="server" ErrorMessage="<%$ Resources:Resource, lblInvalidLogInIDFormat %>" ValidationGroup="vg1"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="rfRoleName" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole"
                                    ControlToValidate="txtUserName" Text="<%$ Resources:Resource, reqUserName %>" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="pTooltipUserName" runat="server"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="form-footer">
                            <asp:Button ID="BtnLoginAsAnotherAuser" class="btn round" runat="server" Text="<%$ Resources:Resource, btnLogIN %>" ValidationGroup="grpRole" OnClientClick="return BtnClick();" OnClick="BtnLoginAsAnotherAuser_OnClick" />
                        </footer>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </section>
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Define To Show Contorls Custom Error message
        function BtnClick() {
            var val = Page_ClientValidate();
            if (!val) {
                var i = 0;
                for (; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $("#" + Page_Validators[i].controltovalidate).addClass("has-error");
                        break;
                    }
                    else {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            else {
                var i = 0;
                for (; i < Page_Validators.length; i++) {

                    {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            return val;
        }

        // To Hide Message
        function HideMessage() {
            $("#<%=dvLogInErrMsg.ClientID %>").hide();
        }

        $(document).ready(function () {
            $("#<%=txtUserName.ClientID%>").focus();
                });
    </script>
</asp:Content>
