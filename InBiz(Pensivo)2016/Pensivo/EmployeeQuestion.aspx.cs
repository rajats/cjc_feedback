﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class EmployeeQuestion : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Employee objEmp = new Employee();
            ddlQuestion.DataSource = objEmp.GetEmployeeQuestion(Globals.CurrentAppLanguageCode);
            ddlQuestion.DataTextField = "question";
            ddlQuestion.DataValueField = "idemployeequestions";
            ddlQuestion.DataBind();
            ddlQuestion.Items.Insert(0, new ListItem(Resources.Resource.lblPleaseSelectSecurityQuestion, ""));
            ddlQuestion.Focus();
        }
    }

    /// <summary>
    /// To Hold Question Answer and move to next page
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event Args</param>
    protected void btnSaveQuestion_OnClick(object sender, EventArgs e)
    {
        if (Request.QueryString["askQuestion"] == "1")
        {
            //Employee objEmp = new Employee();
            //if (objEmp.InsertEmployeeAnswer(CurrentEmployee.EmpID, BusinessUtility.GetInt(ddlQuestion.SelectedItem.Value), txtAnswer.Text) == true)
            //{
            //    Response.Redirect("EmployeeQuestionOther.aspx" + Request.Url.Query + "&QID=" + BusinessUtility.GetInt(ddlQuestion.SelectedItem.Value) + "&QAns=" + txtAnswer.Text + "");
            //}
            Response.Redirect("EmployeeQuestionOther.aspx" + Request.Url.Query + "&QID=" + BusinessUtility.GetInt(ddlQuestion.SelectedItem.Value) + "&QAns=" + txtAnswer.Text + "");
        }
        else
        {
            Response.Redirect("EmployeeQuestionOther.aspx" + Request.Url.Query + "&QID=" + BusinessUtility.GetInt(ddlQuestion.SelectedItem.Value) + "&QAns=" + txtAnswer.Text + "");
        }
    }

}