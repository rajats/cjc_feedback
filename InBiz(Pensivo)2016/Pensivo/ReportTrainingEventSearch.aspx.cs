﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class ReportTrainingEventSearch : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ReportOption != "")
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
        }
        if (!IsPostBack && !IsPagePostBack(gvRoles))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblTrainingByList, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            hdnROption.Value = this.ReportOption;
            hdnSearchBy.Value = this.SearchBy;
            hdnIsAllEvent.Value = BusinessUtility.GetString(this.IsAllEvent);

            if (this.SearchBy == ReportFilterOption.ByList)
            {
                dvSearch.Visible = false;
            }
        }
    }

    /// <summary>
    /// Bind Report JQ Grid
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtName = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        Event objRole = new Event();
        objRole.EventName = Utils.ReplaceDBSpecialCharacter(txtName);
        objRole.TestType = CourseTestType.TestOnly;
        gvRoles.DataSource = objRole.GetEventList(Globals.CurrentAppLanguageCode);
        gvRoles.DataBind();
    }

    /// <summary>
    /// To Get Report Option
    /// </summary>
    public string ReportOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }

    /// <summary>
    /// To Get Is All Event
    /// </summary>
    public int IsAllEvent
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["isAllEvent"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

}