﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using iTECH.Library.Utilities;

/// <summary>
/// To define functionality regarding breadcrumbs
/// </summary>
public static class Breadcrumbs
{
    /// <summary>
    /// To Add breadcrumbs in session object
    /// </summary>
    /// <param name="pageTitle">Pass Page Title</param>
    /// <param name="pageName">Pass Page Name</param>
    /// <param name="pageURL">Pass Page URL</param>
    /// <returns>string</returns>
    public static string BreadcrumbsAdd(string pageTitle, string pageName, string pageURL)
    {
        DataTable dt = (DataTable)GetBreadcrumbsTable();
        DataView dv = (DataView)dt.DefaultView;
        dv.RowFilter = "PageURL ='" + pageURL + "'";
        if (dv.Count > 0)
        {
            DataRow[] rows = dt.Select(" BreadCorumID > " + BusinessUtility.GetInt(dv.ToTable().Rows[0][0]));
            if (rows.Length > 0)
            {
                foreach (DataRow dr in rows)
                {
                    dr.Delete();
                    dt.AcceptChanges();
                }
                HttpContext.Current.Session["dtBrdCrumb"] = dt;
            }
        }
        else
        {
            DataRow dRow = dt.NewRow();
            dRow["PageTitle"] = pageTitle;
            dRow["PageName"] = pageName;
            dRow["PageURL"] = pageURL;
            dt.Rows.Add(dRow);
            dt.AcceptChanges();
            HttpContext.Current.Session["dtBrdCrumb"] = dt;
        }

        return GetBreadcrumbsList(pageName);
    }

    /// <summary>
    /// To add breadcrumbs with parent page url
    /// </summary>
    /// <param name="pageTitle">Pass Page Title</param>
    /// <param name="pageName">Pass Page Name</param>
    /// <param name="pageURL">Pass Page URL</param>
    /// <param name="sRequestedParentPageUrl">Pass Requested Parent Page URL</param>
    /// <returns>String</returns>
    public static string BreadcrumbsAdd(string pageTitle, string pageName, string pageURL, string sRequestedParentPageUrl)
    {
        DataTable dt = (DataTable)GetBreadcrumbsTable();
        DataView dv = (DataView)dt.DefaultView;
        dv.RowFilter = "PageURL ='" + Utils.ReplaceDBSpecialCharacter(pageURL) + "'";
        if (dv.Count > 0)
        {
            DataRow[] rows = dt.Select(" BreadCorumID > " + BusinessUtility.GetInt(dv.ToTable().Rows[0][0]));
            if (rows.Length > 0)
            {
                foreach (DataRow dr in rows)
                {
                    dr.Delete();
                    dt.AcceptChanges();
                }
                HttpContext.Current.Session["dtBrdCrumb"] = dt;
            }
        }
        else
        {
            DataView dvUrlRefreee = (DataView)dt.DefaultView;
            dvUrlRefreee.RowFilter = "PageURL ='" + sRequestedParentPageUrl + "'";
            if (dvUrlRefreee.Count > 0)
            {
                DataRow[] rows = dt.Select(" BreadCorumID > " + BusinessUtility.GetInt(dvUrlRefreee.ToTable().Rows[0][0]));
                if (rows.Length > 0)
                {
                    foreach (DataRow dr in rows)
                    {
                        dr.Delete();
                        dt.AcceptChanges();
                    }
                    HttpContext.Current.Session["dtBrdCrumb"] = dt;
                }
            }

            DataRow dRow = dt.NewRow();
            dRow["PageTitle"] = pageTitle;
            dRow["PageName"] = pageName;
            dRow["PageURL"] = pageURL;
            dt.Rows.Add(dRow);
            dt.AcceptChanges();
            HttpContext.Current.Session["dtBrdCrumb"] = dt;
        }

        return GetBreadcrumbsList(pageName);
    }

    /// <summary>
    /// To set breadcrumbs root menu
    /// </summary>
    public static void BreadcrumbsSetRootMenu()
    {
        DataTable dt = (DataTable)GetBreadcrumbsTable();
        DataRow[] rows = dt.Select(" BreadCorumID > 1");
        if (rows.Length > 0)
        {
            foreach (DataRow dr in rows)
            {
                dr.Delete();
                dt.AcceptChanges();
            }
            HttpContext.Current.Session["dtBrdCrumb"] = dt;
        }
    }

    /// <summary>
    /// To check if breadcrumb have submenu or not
    /// </summary>
    /// <returns>True/False</returns>
    public static Boolean BreadcrumbsHasMenu()
    {
        DataTable dt = (DataTable)GetBreadcrumbsTable();
        if (dt.Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// To reset report breadcrumb menu
    /// </summary>
    public static void ResetReportMenu()
    {
        DataTable dt = (DataTable)GetBreadcrumbsTable();
        DataView dv = (DataView)dt.DefaultView;
        dv.RowFilter = "PageTitle ='Reporting'";
        if (dv.Count > 0)
        {
            DataRow[] rows = dt.Select(" BreadCorumID > " + BusinessUtility.GetInt(dv.ToTable().Rows[0][0]));
            if (rows.Length > 0)
            {
                foreach (DataRow dr in rows)
                {
                    dr.Delete();
                    dt.AcceptChanges();
                }
                HttpContext.Current.Session["dtBrdCrumb"] = dt;
            }
        }
    }

    /// <summary>
    /// To reset training menu breadcrumb
    /// </summary>
    public static void ResetTrainingMenu()
    {
        DataTable dt = (DataTable)GetBreadcrumbsTable();
        DataView dv = (DataView)dt.DefaultView;
        dv.RowFilter = "PageTitle ='" + Resources.Resource.lblHome + "'";
        if (dv.Count > 0)
        {
            DataRow[] rows = dt.Select(" BreadCorumID > " + BusinessUtility.GetInt(dv.ToTable().Rows[0][0]));
            if (rows.Length > 0)
            {
                foreach (DataRow dr in rows)
                {
                    dr.Delete();
                    dt.AcceptChanges();
                }
                HttpContext.Current.Session["dtBrdCrumb"] = dt;
            }
        }

        DataView dv1 = (DataView)dt.DefaultView;
        dv1.RowFilter = "PageTitle ='" + Resources.Resource.lblMyTraining + "'";
        if (dv1.Count > 0)
        {
            DataRow[] rows = dt.Select(" BreadCorumID > " + BusinessUtility.GetInt(dv1.ToTable().Rows[0][0]));
            if (rows.Length > 0)
            {
                foreach (DataRow dr in rows)
                {
                    dr.Delete();
                    dt.AcceptChanges();
                }
                HttpContext.Current.Session["dtBrdCrumb"] = dt;
            }
        }
    }

    /// <summary>
    /// To reset my training menu breadcrumbs
    /// </summary>
    public static void ResetMyTrainingMenu()
    {
        DataTable dt = (DataTable)GetBreadcrumbsTable();
        DataView dv1 = (DataView)dt.DefaultView;
        dv1.RowFilter = "PageTitle ='" + Resources.Resource.lblMyTraining + "'";
        if (dv1.Count > 0)
        {
            DataRow[] rows = dt.Select(" BreadCorumID >= " + BusinessUtility.GetInt(dv1.ToTable().Rows[0][0]));
            if (rows.Length > 0)
            {
                foreach (DataRow dr in rows)
                {
                    dr.Delete();
                    dt.AcceptChanges();
                }
                HttpContext.Current.Session["dtBrdCrumb"] = dt;
            }
        }


        DataView dv3 = (DataView)dt.DefaultView;
        dv3.RowFilter = "PageName ='EmployeeCourses.aspx'";
        if (dv3.Count > 0)
        {
            DataRow[] rows = dt.Select(" BreadCorumID >= " + BusinessUtility.GetInt(dv3.ToTable().Rows[0][0]));
            if (rows.Length > 0)
            {
                foreach (DataRow dr in rows)
                {
                    dr.Delete();
                    dt.AcceptChanges();
                }
                HttpContext.Current.Session["dtBrdCrumb"] = dt;
            }
        }

    }

    /// <summary>
    /// To get breadcrumbs list to show
    /// </summary>
    /// <param name="pageName">Pass Page Name</param>
    /// <returns>string</returns>
    public static string GetBreadcrumbsList(string pageName)
    {
        DataTable dt = (DataTable)GetBreadcrumbsTable();
        string sNavigation = "";

        foreach (DataRow dRow in dt.Rows)
        {
            if (sNavigation == "")
            {
                if (BusinessUtility.GetString(dRow["PageName"]) == pageName)
                {
                    sNavigation += "<li class='cur'>";
                    sNavigation += "<span class='textnode'>" + BusinessUtility.GetString(dRow["PageTitle"]) + "</span>";
                    sNavigation += "</li>";
                }
                else
                {
                    sNavigation += "<li>    <a  runat='server' href='" + BusinessUtility.GetString(dRow["PageURL"]) + "'>" + BusinessUtility.GetString(dRow["PageTitle"]) + "</a></li>";
                }
            }
            else
            {
                if (BusinessUtility.GetString(dRow["PageName"]) == pageName)
                {
                    sNavigation += "  <li class='cur'>";
                    sNavigation += " <i class='chevron'>&gt;</i>";
                    sNavigation += "<span class='textnode'>" + BusinessUtility.GetString(dRow["PageTitle"]) + "</span>";
                    sNavigation += "</li>";
                }
                else
                {
                    sNavigation += "<li> <i class='chevron'>&gt;</i>   <a  runat='server' href='" + BusinessUtility.GetString(dRow["PageURL"]) + "'>" + BusinessUtility.GetString(dRow["PageTitle"]) + "</a></li>";
                }

            }
        }
        return "<ul>" + sNavigation + "</ul>";
    }

    /// <summary>
    /// To get breadcrumbs list from session object
    /// </summary>
    /// <returns>Datatable</returns>
    public static DataTable GetBreadcrumbsTable()
    {
        if (HttpContext.Current.Session["dtBrdCrumb"] == null)
        {
            DataTable dtBrdCrumb = new DataTable();
            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "BreadCorumID";
            column.AutoIncrement = true;
            column.AutoIncrementSeed = 1;
            column.AutoIncrementStep = 1;
            dtBrdCrumb.Columns.Add(column);
            dtBrdCrumb.Columns.Add(new DataColumn("PageName", typeof(string)));
            dtBrdCrumb.Columns.Add(new DataColumn("PageTitle", typeof(string)));
            dtBrdCrumb.Columns.Add(new DataColumn("PageURL", typeof(string)));
            HttpContext.Current.Session["dtBrdCrumb"] = dtBrdCrumb;
            return dtBrdCrumb;
        }
        else
        {
            return (DataTable)HttpContext.Current.Session["dtBrdCrumb"];
        }
    }

    /// <summary>
    /// To check if page url exists in breadcrumbs or not
    /// </summary>
    /// <param name="pageURL">Pass Page URL</param>
    /// <returns>True/False</returns>
    public static Boolean ChkUrlExistInBreadCrumb( string pageURL)
    {
        DataTable dt = (DataTable)GetBreadcrumbsTable();
        DataView dv = (DataView)dt.DefaultView;
        dv.RowFilter = "PageURL ='" + pageURL + "'";
        if (dv.Count > 0)
        {
            DataRow[] rows = dt.Select(" BreadCorumID >= " + BusinessUtility.GetInt(dv.ToTable().Rows[0][0]));
            if (rows.Length > 0)
            {
                foreach (DataRow dr in rows)
                {
                    dr.Delete();
                    dt.AcceptChanges();
                }
                HttpContext.Current.Session["dtBrdCrumb"] = dt;
            }
            return false;
        }
        else
        {
            return true;
        }
    }
}