﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Text;
using System.IO;
using System.Configuration;

/// <summary>
/// To define functionality for global message
/// </summary>
public static class GlobalMessage
{
    /// <summary>
    /// To show custom message popup
    /// </summary>
    /// <param name="sMessage">Pass Message</param>
    public static void showAlertMessage(string sMessage)
    {
        Page page = HttpContext.Current.Handler as Page;
        ClientScriptManager cs = page.ClientScript;
        ScriptManager.RegisterStartupScript(page, page.GetType(), "WithUpdatePanel", "javascript: ShowPensivoMessage('" + sMessage + "');", true);
    }

    /// <summary>
    /// To show custom message popup with closing url
    /// </summary>
    /// <param name="sMessage">Pass Message</param>
    /// <param name="sURLOnClose">Pass URL Called on Popup Message Close</param>
    public static void showAlertMessage(string sMessage, string sURLOnClose)
    {
        Page page = HttpContext.Current.Handler as Page;
        ClientScriptManager cs = page.ClientScript;
        ScriptManager.RegisterStartupScript(page, page.GetType(), "WithUpdatePanel", "javascript: ShowPensivoMessage('" + sMessage + "', '" + sURLOnClose + "');", true);
    }

    /// <summary>
    /// To show custom message with setfocus on a control on close popup
    /// </summary>
    /// <param name="sMessage">Pass Message</param>
    /// <param name="ctrlIdToFocus">Pass Control ID To set Focus on Popup Close</param>
    public static void showAlertMessageWithFocus(string sMessage, string ctrlIdToFocus)
    {
        Page page = HttpContext.Current.Handler as Page;
        ClientScriptManager cs = page.ClientScript;
        ScriptManager.RegisterStartupScript(page, page.GetType(), "WithUpdatePanel", "javascript: ShowPensivoMessageWithFocusCtrl('" + sMessage + "', '" + ctrlIdToFocus + "');", true);
    }

    /// <summary>
    /// To show custom message with call function on Popup close
    /// </summary>
    /// <param name="sMessage">Pass Message</param>
    /// <param name="sFunctionOnClose">Pass Function Called on Popup Close</param>
    public static void showAlertMessageWithClose(string sMessage, string sFunctionOnClose)
    {
        Page page = HttpContext.Current.Handler as Page;
        ClientScriptManager cs = page.ClientScript;
        ScriptManager.RegisterStartupScript(page, page.GetType(), "WithUpdatePanel", "javascript: ShowPensivoMessageCallFunctionOnClose('" + sMessage + "', '" + sFunctionOnClose + "');", true);
    }

}