﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using System.Text;
using MySql.Data.MySqlClient;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using System.Diagnostics;

/// <summary>
/// To include functionality to import data
/// </summary>
public class Import
{
    /// <summary>
    /// Get/Set Import Error Message
    /// </summary>
    public string ImportErrorMessage { get; set; }

    /// <summary>
    /// Get/Set Import Error Count
    /// </summary>
    public int ImportErrorCount { get; set; }

    /// <summary>
    /// Get/Set Import Success Count
    /// </summary>
    public int ImportSuccessCount { get; set; }

    /// <summary>
    /// To get value 
    /// </summary>
    /// <param name="Name">Pass String</param>
    /// <returns>String</returns>
    private string GetValue(string Name)
    {
        string strValue = Name;
        strValue = strValue.Replace("'", "''");
        return strValue;
    }

    /// <summary>
    /// To import data from excel sheet
    /// </summary>
    /// <param name="dtCustomer">Pass Employee List as Datatable</param>
    /// <returns>int</returns>
    public int ImportEmployee(DataTable dtCustomer)
    {
        List<ErrorDetails> lstErrDtl = new List<ErrorDetails>();
        foreach (DataRow dRow in dtCustomer.Rows)
        {
            string sEmpID = BusinessUtility.GetString(dRow["username"]);
            string sUserPass = BusinessUtility.GetString(dRow["userpass"]);
            string sPensivoUID = BusinessUtility.GetString(dRow["uuid"]);
            int iActive = BusinessUtility.GetInt(dRow["active"]);
            int iDeleted = BusinessUtility.GetInt(dRow["deleted"]);

            if (sEmpID != "")
            {
                DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
                try
                {
                    dbTransactionHelper.BeginTransaction();

                    StringBuilder sbQuery = new StringBuilder();
                    sbQuery.Append(" SELECT empHdrID FROM empheader  where empExtID = @empExtID ");

                    Object rValue = dbTransactionHelper.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("empExtID", sEmpID, MyDbType.String)
                        });

                    if (BusinessUtility.GetInt(rValue) > 0)
                    {
                        lstErrDtl.Add(new ErrorDetails { EmpCode = sEmpID, ErrorCode = (int)EmpImportErrCode.DuplicateEmpCode });
                        this.ImportErrorCount += 1;

                        StringBuilder sbUpdate = new StringBuilder();
                        sbUpdate.Append(" UPDATE  empheader set PensivoUUID = @PensivoUUID  where empExtID = @empExtID ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbUpdate), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("empExtID", sEmpID, MyDbType.String),
                            DbUtility.GetParameter("PensivoUUID", BusinessUtility.GetInt( sPensivoUID), MyDbType.Int)
                            });
                    }
                    else
                    {
                        string sFirstName = BusinessUtility.GetString(dRow["firstname"]);
                        string sLastName = BusinessUtility.GetString(dRow["lastname"]);
                        string sEmail = BusinessUtility.GetString(dRow["email"]);

                        DateTime dtHire;
                        DateTime dtStart;
                        DateTime dtBirth;
                        DateTime.TryParseExact("2000-01-01", new string[] { "yyyy-MM-dd" }, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtHire);
                        DateTime.TryParseExact("2000-01-01", new string[] { "yyyy-MM-dd" }, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtStart);
                        DateTime.TryParseExact("2000-01-01", new string[] { "yyyy-MM-dd" }, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirth);

                        StringBuilder sbInsertQuery = new StringBuilder();
                        sbInsertQuery.Append(" INSERT INTO empheader (empPassword, empFirstName, empLastName, empCreatedOn, empEmail, empActive, empNotifyPref, empLastUpdatedOn, empExtID, empLoginID, emptype,  hire_date, start_date, empDateOfBirth,  ");//emp_type, action, 
                        sbInsertQuery.Append("  sysSates_sysStateCode, sysCountries_sysCountryCode, isDeleted, PensivoUUID, createdSource )"); //empOfficeAddrLine1, empOfficeAddrLine2, empOfficeCity, empPhone, zip_code, 
                        sbInsertQuery.Append(" VALUES (@empPassword, @empFirstName, @empLastName, @empCreatedOn, @empEmail, @empActive, @empNotifyPref, @empLastUpdatedOn, @empExtID, @empLoginID, @emptype,   @hire_date, @start_date, @empDateOfBirth,  ");//@emp_type, @action,
                        sbInsertQuery.Append(" @sysSates_sysStateCode, @sysCountries_sysCountryCode, @isDeleted, @PensivoUUID, @createdSource  "); //@empOfficeAddrLine1, @empOfficeAddrLine2, @empOfficeCity,   @empPhone, @zip_code,
                        sbInsertQuery.Append(" ) ");
                        dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("empPassword", "pass", MyDbType.String),    
                        DbUtility.GetParameter("empFirstName", GetValue( sFirstName), MyDbType.String),
                        DbUtility.GetParameter("empLastName", GetValue(sLastName), MyDbType.String),
                        DbUtility.GetParameter("empCreatedOn", DateTime.Now, MyDbType.DateTime),
                        DbUtility.GetParameter("empEmail", sEmail, MyDbType.String),
                        DbUtility.GetParameter("empActive", iActive, MyDbType.Int),
                        DbUtility.GetParameter("isDeleted", iDeleted, MyDbType.Int),
                        DbUtility.GetParameter("empNotifyPref", "1", MyDbType.String),
                        DbUtility.GetParameter("empLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                        DbUtility.GetParameter("empExtID", GetValue(sEmpID) , MyDbType.String),
                        DbUtility.GetParameter("empLoginID", GetValue(sEmpID) , MyDbType.String),
                        DbUtility.GetParameter("emptype", "E" , MyDbType.String),
                        DbUtility.GetParameter("hire_date", dtHire , MyDbType.DateTime),
                        DbUtility.GetParameter("start_date", dtStart , MyDbType.DateTime),
                        DbUtility.GetParameter("empDateOfBirth", dtBirth , MyDbType.DateTime),
                        DbUtility.GetParameter("sysSates_sysStateCode", GetValue( BusinessUtility.GetString("ON")) , MyDbType.String),
                        DbUtility.GetParameter("sysCountries_sysCountryCode", GetValue( BusinessUtility.GetString("CAN")) , MyDbType.String),
                        DbUtility.GetParameter("PensivoUUID ", BusinessUtility.GetInt( sPensivoUID) , MyDbType.Int),
                        DbUtility.GetParameter("createdSource ", UserCreatedSource.Import , MyDbType.String),
                        });

                        int iInsertedEmpID = dbTransactionHelper.GetLastInsertID();

                        if (iInsertedEmpID > 0)
                        {
                            string sDivisionID = GetValue(BusinessUtility.GetString(dRow["tbs_tax_loc"]));
                            string sLocation = GetValue(BusinessUtility.GetString(dRow["tbs_department_id"]));
                            string sJobID = GetValue(BusinessUtility.GetString(dRow["tbs_job_code"]));
                            string sType = GetValue(BusinessUtility.GetString(dRow["employee_type_parsed"]));
                            string sTypeParsed = GetValue(BusinessUtility.GetString(dRow["tbs_emp_type"]));

                            if (sDivisionID != "")
                            {
                                sbInsertQuery = new StringBuilder();
                                sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                sbInsertQuery.Append(" ) ");
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Division, MyDbType.String),
                                DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sDivisionID, MyDbType.String)
                                });
                            }

                            if (sJobID != "")
                            {
                                sbInsertQuery = new StringBuilder();
                                sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                sbInsertQuery.Append(" ) ");
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.JobCode, MyDbType.String),
                                DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sJobID, MyDbType.String)
                                });
                            }


                            if (sLocation != "")
                            {
                                sbInsertQuery = new StringBuilder();
                                sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                sbInsertQuery.Append(" ) ");
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Store, MyDbType.String),
                                DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sLocation, MyDbType.String)
                                });
                            }


                            if (sType != "")
                            {
                                sbInsertQuery = new StringBuilder();
                                sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                sbInsertQuery.Append(" ) ");
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.Type, MyDbType.String),
                                DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sType, MyDbType.String)
                                });
                            }


                            if (sTypeParsed != "")
                            {
                                sbInsertQuery = new StringBuilder();
                                sbInsertQuery.Append(" INSERT INTO empreference (empHeader_empHdrID, sysEmpReferenceCodes_sysRefCode, sysEmpReferenceCodes_sysRefCodeValue) ");
                                sbInsertQuery.Append(" VALUES (@empHeader_empHdrID, @sysEmpReferenceCodes_sysRefCode, @sysEmpReferenceCodes_sysRefCodeValue ");
                                sbInsertQuery.Append(" ) ");
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("empHeader_empHdrID", iInsertedEmpID, MyDbType.Int),    
                                DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCode", EmpRefCode.TypeParsed, MyDbType.String),
                                DbUtility.GetParameter("sysEmpReferenceCodes_sysRefCodeValue", sTypeParsed, MyDbType.String)
                                });
                            }
                        }
                        this.ImportSuccessCount += 1;
                    }
                    dbTransactionHelper.CommitTransaction();
                }
                catch (Exception e)
                {
                    lstErrDtl.Add(new ErrorDetails { EmpCode = sEmpID, ErrorCode = (int)EmpImportErrCode.UnkownError, ErrorMessage = e.Message });
                    this.ImportErrorCount += 1;
                    dbTransactionHelper.RollBackTransaction();
                }
                finally
                {
                    dbTransactionHelper.CloseDatabaseConnection();
                }
            }
        }

        if (this.ImportErrorCount > 0)
        {
            this.ImportErrorMessage = GetErrorMessage(lstErrDtl);
        }
        return this.ImportSuccessCount;
    }

    /// <summary>
    /// To define object to handle excel import error
    /// </summary>
    private class ErrorDetails
    {
        /// <summary>
        /// Get/Set Employee Code
        /// </summary>
        public string EmpCode { get; set; }

        /// <summary>
        /// Get/Set Employee Error Code
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Get/Set Error Message
        /// </summary>
        public string ErrorMessage { get; set; }
    }

    /// <summary>
    /// To get excel data import error list
    /// </summary>
    /// <param name="lstErroDtl">Pass List of Error Details</param>
    /// <returns>string</returns>
    private string GetErrorMessage(List<ErrorDetails> lstErroDtl)
    {
        StringBuilder sbHtml = new StringBuilder();
        sbHtml.Append("<table>");
        sbHtml.Append("<tr>");
        sbHtml.Append("<td colspan='2' style='font-weight:bold; font-size:14px'>");
        sbHtml.Append("EmpID");
        sbHtml.Append("</td>");
        sbHtml.Append("</tr>");
        sbHtml.Append("<tr>");
        sbHtml.Append("<td style='font-size:10px;font-weight:bold; '>");
        sbHtml.Append("ErrorCode");
        sbHtml.Append("</td>");
        sbHtml.Append("<td style='font-size:10px;font-weight:bold; '>");
        sbHtml.Append("Error Message");
        sbHtml.Append("</td>");
        sbHtml.Append("</tr>");
        foreach (var item in lstErroDtl)
        {
            sbHtml.Append("<tr>");
            sbHtml.Append("<td>");
            sbHtml.Append(BusinessUtility.GetString(item.EmpCode));
            sbHtml.Append("</td>");
            sbHtml.Append("<td>");
            sbHtml.Append(BusinessUtility.GetString(item.ErrorCode));
            sbHtml.Append("</td>");
            sbHtml.Append("<td>");
            sbHtml.Append(BusinessUtility.GetString(item.ErrorMessage));
            sbHtml.Append("</td>");
            sbHtml.Append("</tr>");
        }
        sbHtml.Append("</table>");
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// To read xml data and save it in database 
    /// </summary>
    /// <param name="xmlContent">Pass XML Content</param>
    /// <param name="xmlPostType">Pass XML Post Type</param>
    /// <returns>True/False</returns>
    public static Boolean SaveRegistrationXML(string xmlContent, string xmlPostType)
    {
        RegisterEventResult objRegResult = new RegisterEventResult();
        int iRegID = 0;
        bool isCourseTest = false;
        DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
        dbTransactionHelper.BeginTransaction();
        try
        {
            #region SaveRegistrationResult
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlContent);

            XmlNodeList List = doc.GetElementsByTagName("registrationreport");


            int i = 0;
            foreach (XmlNode node in List)
            {
                string sRegID = node.Attributes[1].Value.Replace(Utils.ScromRegPrfx, "");
                if (BusinessUtility.GetInt(sRegID) > 0)
                {
                    iRegID = BusinessUtility.GetInt(sRegID);
                    if (i == 0)
                    {
                        objRegResult = new RegisterEventResult();
                        if (objRegResult.IsRegTestCourse(BusinessUtility.GetInt(sRegID)) == false)
                        {
                            EmployeeCourses objEmpCourse = new EmployeeCourses();
                            if ((objEmpCourse.IsEmployeeCompleteEvent(BusinessUtility.GetInt(sRegID)) == true))
                            {
                                return true;
                            }

                            i += 1;
                            RegisterEventResult objDeleteResult = new RegisterEventResult();
                            objDeleteResult.DeleteRegistrationResult(dbTransactionHelper, BusinessUtility.GetInt(sRegID));
                        }
                    }

                    XmlNodeList xmlCourseHeader = doc.SelectNodes("/registrationreport/activity");
                    string sCourseIsCompleted = "";
                    string sCourseProgressStatus = "";
                    string sNoAttempts = "";
                    string title = "";
                    string completed = "";
                    string totalTime = "";
                    string location = "";
                    string successStatus = "";
                    string score = "";
                    foreach (XmlNode xn in xmlCourseHeader)
                    {
                        sCourseIsCompleted = xn["completed"].InnerText;
                        sCourseProgressStatus = xn["progressstatus"].InnerText;
                        sNoAttempts = xn["attempts"].InnerText;
                    }

                    RegisterEventResult objXmlParsed = new RegisterEventResult();
                    objXmlParsed.RegID = BusinessUtility.GetInt(sRegID);
                    objXmlParsed.CourseStatus = sCourseIsCompleted;
                    objXmlParsed.xmlPostType = xmlPostType;
                    objXmlParsed.SaveXMLParsed(dbTransactionHelper);

                    XmlNodeList xnList = doc.SelectNodes("/registrationreport/activity/children/activity");
                    foreach (XmlNode xn in xnList)
                    {
                        title = xn["title"].InnerText;
                        completed = xn["completed"].InnerText;

                        XmlNodeList xnruntime = xn.SelectNodes("runtime");
                        if (xnruntime.Count > 0)
                        {
                            foreach (XmlNode xnrntime in xnruntime)
                            {
                                score = xnrntime["score_raw"].InnerText;
                                totalTime = xnrntime["timetracked"].InnerText;
                                location = xnrntime["location"].InnerText;
                                successStatus = xnrntime["success_status"].InnerText;

                                RegisterEventResult objEventResult = new RegisterEventResult();
                                objEventResult.RegID = BusinessUtility.GetInt(sRegID);
                                objEventResult.CourseStatus = sCourseIsCompleted;
                                objEventResult.CourseAttempts = sNoAttempts;
                                objEventResult.Title = title;
                                objEventResult.Completed = completed;
                                objEventResult.Score = score;
                                objEventResult.TotalTime = totalTime;
                                objEventResult.Location = location;
                                objEventResult.ProgressStatus = sCourseProgressStatus;
                                objEventResult.xmlPostType = xmlPostType;
                                objEventResult.XMLParsedID = objXmlParsed.XMLParsedID;
                                objEventResult.SaveRegistrationResult(dbTransactionHelper);

                                XmlNodeList xmlInteractions = xnrntime.SelectNodes("interactions/interaction");
                                foreach (XmlNode xnIntr in xmlInteractions)
                                {
                                    String learnerResponse = "";
                                    String result = "";
                                    String description = "";

                                    learnerResponse = xnIntr["learner_response"].InnerText;
                                    result = xnIntr["result"].InnerText;
                                    description = xnIntr["description"].InnerText;

                                    XmlNodeList xmlCorrectResponse = xnIntr.SelectNodes("correct_responses");
                                    foreach (XmlNode xncResponse in xmlCorrectResponse)
                                    {
                                        XmlNodeList xnResponse = xncResponse.SelectNodes("response");
                                        string ResponseID = "";
                                        if (xnResponse.Count > 0)
                                        {
                                            ResponseID = xnResponse[0].Attributes[0].Value;
                                        }
                                        if (objEventResult.ResultID > 0)
                                        {
                                            objEventResult.SaveRegistrationInteractions(dbTransactionHelper, objEventResult.ResultID, learnerResponse, result, ResponseID, description);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RegisterEventResult objEventResult = new RegisterEventResult();
                            objEventResult.RegID = BusinessUtility.GetInt(sRegID);
                            objEventResult.CourseStatus = sCourseIsCompleted;
                            objEventResult.CourseAttempts = sNoAttempts;
                            objEventResult.Title = title;
                            objEventResult.Completed = completed;
                            objEventResult.Score = score;
                            objEventResult.TotalTime = totalTime;
                            objEventResult.Location = location;
                            objEventResult.ProgressStatus = sCourseProgressStatus;
                            objEventResult.xmlPostType = xmlPostType;
                            objEventResult.XMLParsedID = objXmlParsed.XMLParsedID;
                            objEventResult.SaveRegistrationResult(dbTransactionHelper);

                            XmlNodeList xnNewListNew = xn.SelectNodes("children/activity");
                            if (xnNewListNew.Count > 0)
                            {
                                ReadXMLTitleNodeList(xnNewListNew, dbTransactionHelper, sRegID, xmlPostType, sCourseIsCompleted, sNoAttempts, sCourseProgressStatus, objXmlParsed.XMLParsedID);
                            }
                        }
                    }

                }
            }
            #endregion
            dbTransactionHelper.CommitTransaction();
        }
        catch (Exception e)
        {
            dbTransactionHelper.RollBackTransaction();
            ErrorLog.createLog(e.ToString());
            throw;
        }
        finally
        {
            dbTransactionHelper.CloseDatabaseConnection();
        }




        try
        {
            #region UpdateReportSummary
            EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();

            objRegResult = new RegisterEventResult();
            if (objRegResult.IsRegTestCourse(BusinessUtility.GetInt(iRegID)) == true)
            {
                objempCourseReg = new EmployeeCourseRegistration();
                iRegID = objempCourseReg.GetTestCourseRegID(iRegID);

                isCourseTest = true;
            }

            if (iRegID > 0)
            {
                objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(iRegID));
                Event objEvent = new Event();
                objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);

                int empID = objempCourseReg.empID;

                float TestPerCentage = objempCourseReg.GetTestPercentage(BusinessUtility.GetInt(iRegID));
                int attemptCount = objempCourseReg.GetTestAttemptCount(BusinessUtility.GetInt(iRegID));

                float passTestScore = 0;
                EmployeeCourses objEmpCourse = new EmployeeCourses();
                Boolean isCourseCompleted = objEmpCourse.IsCourseCompleted(BusinessUtility.GetInt(iRegID));
                int coursecompleted = 0;
                if (isCourseCompleted == true)
                {
                    coursecompleted = 1;
                    passTestScore = TestPerCentage;
                }

                Employee objEmp = new Employee();
                objEmp.GetEmployeeDetail(empID);

                Report objReport = new Report();
                objReport.UpdateReportSummary(empID, iRegID, objEvent.Total_no_slides, objEvent.TestType, TestPerCentage, attemptCount, passTestScore, coursecompleted, objEmp.EmpExtID);

                // Code To Repeat Course
                if (isCourseCompleted == true) //isCourseTest == false &&
                {
                    // To Get Registration Role ID
                    if (BusinessUtility.GetInt(objempCourseReg.roleID) > 0)
                    {
                        if (objempCourseReg.RepeatRequired == true)
                        {
                            // Get Repeat Type
                            //RoleTrainingEventDefault objRoleTraningEventDefault = new RoleTrainingEventDefault();
                            //objRoleTraningEventDefault.GetRoleCourseDetail(BusinessUtility.GetInt(objempCourseReg.roleID), BusinessUtility.GetInt(objempCourseReg.eventID));
                            EmpAssignedCourses objEmpAssignedCourse = new EmpAssignedCourses();
                            if (objEmpAssignedCourse.IsRequestedToRepeat(BusinessUtility.GetInt(objempCourseReg.empID), BusinessUtility.GetInt(objempCourseReg.eventID)) == false)
                            {
                                objEmpAssignedCourse.RepeatCourse(iRegID, objempCourseReg.roleID, objempCourseReg.repeatTriggerType, objempCourseReg.orderSeqInDisplay, objempCourseReg.repeatInDays);
                            }
                        }
                    }
                }
                // End Code

            }
            #endregion
        }
        catch (Exception ex)
        {
            ErrorLog.createLog(ex.ToString());
        }

        return true;

    }

    /// <summary>
    /// To read xml activity node list
    /// </summary>
    /// <param name="doc">Pass XML Node List</param>
    /// <param name="dbTransactionHelper">Pass DB Transaction Connection</param>
    /// <param name="sRegID">Pass Registration ID</param>
    /// <param name="xmlPostType">Pass XML Post Type</param>
    /// <param name="sCourseIsCompleted">Pass Course ID Completed</param>
    /// <param name="sNoAttempts">Pass No of Attempts</param>
    /// <param name="sCourseProgressStatus">Pass Course Progress Status</param>
    /// <param name="xmlParsedID">Pass XML Parsed ID</param>
    public static void ReadXMLTitleNodeList(XmlNodeList doc, DbTransactionHelper dbTransactionHelper, string sRegID, string xmlPostType, string sCourseIsCompleted, string sNoAttempts, string sCourseProgressStatus, int xmlParsedID)
    {
        string title = "";
        string completed = "";
        string score = "";
        string totalTime = "";
        string location = "";
        string successStatus = "";

        XmlNodeList xnList = doc;
        foreach (XmlNode xn in xnList)
        {
            title = xn["title"].InnerText;
            completed = xn["completed"].InnerText;
            sCourseProgressStatus = xn["progressstatus"].InnerText;

            XmlNodeList xnruntime = xn.SelectNodes("runtime");
            if (xnruntime.Count > 0)
            {
                foreach (XmlNode xnrntime in xnruntime)
                {
                    score = xnrntime["score_raw"].InnerText;
                    totalTime = xnrntime["timetracked"].InnerText;
                    location = xnrntime["location"].InnerText;
                    successStatus = xnrntime["success_status"].InnerText;

                    RegisterEventResult objEventResult = new RegisterEventResult();
                    objEventResult.RegID = BusinessUtility.GetInt(sRegID);
                    objEventResult.CourseStatus = sCourseIsCompleted;
                    objEventResult.CourseAttempts = sNoAttempts;
                    objEventResult.Title = title;
                    objEventResult.Completed = completed;
                    objEventResult.Score = score;
                    objEventResult.TotalTime = totalTime;
                    objEventResult.Location = location;
                    objEventResult.ProgressStatus = sCourseProgressStatus;
                    objEventResult.xmlPostType = xmlPostType;
                    objEventResult.XMLParsedID = xmlParsedID;
                    objEventResult.SaveRegistrationResult(dbTransactionHelper);

                    XmlNodeList xmlInteractions = xnrntime.SelectNodes("interactions/interaction");
                    foreach (XmlNode xnIntr in xmlInteractions)
                    {
                        String learnerResponse = "";
                        String result = "";
                        String description = "";

                        learnerResponse = xnIntr["learner_response"].InnerText;
                        result = xnIntr["result"].InnerText;
                        description = xnIntr["description"].InnerText;

                        XmlNodeList xmlCorrectResponse = xnIntr.SelectNodes("correct_responses");
                        foreach (XmlNode xncResponse in xmlCorrectResponse)
                        {
                            XmlNodeList xnResponse = xncResponse.SelectNodes("response");
                            string ResponseID = xnResponse[0].Attributes[0].Value;

                            if (objEventResult.ResultID > 0)
                            {
                                objEventResult.SaveRegistrationInteractions(dbTransactionHelper, objEventResult.ResultID, learnerResponse, result, ResponseID, description);
                            }
                        }
                    }
                }

                XmlNodeList xnNewListNew = xn.SelectNodes("children/activity");
                if (xnNewListNew.Count > 0)
                {
                    ReadXMLTitleNodeList(xnNewListNew, dbTransactionHelper, sRegID, xmlPostType, sCourseIsCompleted, sNoAttempts, sCourseProgressStatus, xmlParsedID);
                }
            }
            else
            {
                XmlNodeList xnNewListNew = xn.SelectNodes("children/activity");
                if (xnNewListNew.Count > 0)
                {
                    ReadXMLTitleNodeList(xnNewListNew, dbTransactionHelper, sRegID, xmlPostType, sCourseIsCompleted, sNoAttempts, sCourseProgressStatus, xmlParsedID);
                }
            }

        }
    }

    /// <summary>
    /// To get registration id from xml
    /// </summary>
    /// <param name="xmlContent">Pass XML Content</param>
    /// <param name="xmlPostType">Pass XML Post Type</param>
    /// <returns></returns>
    public static int GetRegIDFromXML(string xmlContent, string xmlPostType)
    {
        #region SaveRegistrationResult
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xmlContent);
        XmlNodeList List = doc.GetElementsByTagName("registrationreport");
        RegisterEventResult objRegResult = new RegisterEventResult();

        int i = 0;
        int iRegID = 0;
        foreach (XmlNode node in List)
        {
            string sRegID = node.Attributes[1].Value.Replace(Utils.ScromRegPrfx, "");
            if (BusinessUtility.GetInt(sRegID) > 0)
            {
                iRegID = BusinessUtility.GetInt(sRegID);
            }
        }

        return iRegID;

        #endregion
    }

    /// <summary>
    /// To Save XML Content in XML File 
    /// </summary>
    /// <param name="xmlContent">Pass XML Content</param>
    /// <param name="xmlPostType">Pass XML Post Type</param>
    /// <param name="iRegID">Pass Registration ID</param>
    /// <returns></returns>
    public static string SaveXMLFile(string xmlContent, string xmlPostType, int iRegID)
    {
        #region SaveRegistrationResult

        string xmlFilePath = "";
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xmlContent);
        XmlNodeList List = doc.GetElementsByTagName("registrationreport");
        RegisterEventResult objRegResult = new RegisterEventResult();

        int i = 0;
        foreach (XmlNode node in List)
        {
            string sRegID = node.Attributes[1].Value.Replace(Utils.ScromRegPrfx, "");
            if (BusinessUtility.GetInt(sRegID) > 0)
            {

                #region SaveXMLDataAsFile

                string sXMLPath = Utils.XMLPath;


                if (!(Directory.Exists(sXMLPath)))
                {
                    Directory.CreateDirectory(sXMLPath);
                }

                xmlFilePath += @"\" + DateTime.Now.ToString("yyyyMMdd");
                sXMLPath += @"\" + DateTime.Now.ToString("yyyyMMdd");
                if (!(Directory.Exists(sXMLPath)))
                {
                    Directory.CreateDirectory(sXMLPath);
                }

                xmlFilePath += @"\" + DateTime.Now.ToString("HH");
                sXMLPath += @"\" + DateTime.Now.ToString("HH");
                if (!(Directory.Exists(sXMLPath)))
                {
                    Directory.CreateDirectory(sXMLPath);
                }

                xmlFilePath += @"\" + BusinessUtility.GetString(iRegID) + "_" + DateTime.Now.ToString("HHmmss") + "_" + BusinessUtility.GetString(DateTime.Now.Millisecond * 1000000) + ".xml";
                sXMLPath += @"\" + BusinessUtility.GetString(iRegID) + "_" + DateTime.Now.ToString("HHmmss") + "_" + BusinessUtility.GetString(DateTime.Now.Millisecond * 1000000) + ".xml";

                doc.Save(sXMLPath);
                #endregion

            }
        }

        return xmlFilePath;

        #endregion
    }


}