﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Data;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.Pensivo.BusinessLogic;
using System.Data;
using System.Diagnostics;


/// <summary>
/// To define functionality to save on Postgres  server
/// </summary>
public class PensivoExecutions
{
    /// <summary>
    /// To save data on Postgres server 
    /// </summary>
    /// <param name="fName">Pass First Name</param>
    /// <param name="lName">Pass Last Name</param>
    /// <param name="sPass">Pass Password</param>
    /// <param name="sEmail">Pass Email</param>
    /// <param name="uuID">Pass UUID</param>
    /// <returns>True/False</returns>
    public static Boolean PensivoPostData(string fName, string lName, string sPass, string sEmail, long uuID)
    {
        bool status = false;
        //OdbcConnection DbConnection = new OdbcConnection(ConfigurationManager.AppSettings["PensivoDBConnection"]);
        OdbcConnection DbConnection = new OdbcConnection(AppConfig.GetAppConfigValue("PensivoDBConnection"));
        try
        {
            if (uuID > 0)
            {
                DbConnection.Open();
                if (sPass != "")
                {
                    OdbcCommand updateemail = new OdbcCommand("UPDATE view_tbs_essentials_for_pib SET userpass =MD5('" + sPass + "') WHERE uuid = " + uuID + ";", DbConnection);
                    updateemail.ExecuteNonQuery();
                    updateemail.Dispose();
                }

                if (sEmail != "")
                {
                    OdbcCommand updateemail = new OdbcCommand("UPDATE view_tbs_essentials_for_pib SET email = '" + sEmail + "' WHERE uuid = " + uuID + ";", DbConnection);
                    updateemail.ExecuteNonQuery();
                    updateemail.Dispose();
                }

                status = true;
            }
            else
            {
                status = false;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.createLog("update pensivo DB " + ex.Message + ex.StackTrace);
        }
        finally
        {
            DbConnection.Close();
        }

        return status;
    }

    /// <summary>
    /// To save data on Postgres server 
    /// </summary>
    /// <param name="userName">Pass User Name</param>
    /// <param name="userPass">Pass User Password</param>
    /// <param name="firstName">Pass User First Name</param>
    /// <param name="lastName">Pass Last Name</param>
    /// <param name="tbs_emp_type">Pass TBS User Type</param>
    /// <param name="tbs_tax_loc">Pass TBS Location</param>
    /// <returns>True/False</returns>
    public static string PensivoPostUserData(string userName, string userPass, string firstName, string lastName, string tbs_emp_type, string tbs_tax_loc, string dept_id, string tbs_job_code)
    {
        string returnUUID = string.Empty;
        StringBuilder sqlString = new StringBuilder();
        //OdbcConnection DbConnection = new OdbcConnection(ConfigurationManager.AppSettings["PensivoDBConnection"]);
        OdbcConnection DbConnection = new OdbcConnection(AppConfig.GetAppConfigValue("PensivoDBConnection"));
        try
        {
            DbConnection.Open();
            sqlString.Append(" INSERT INTO view_tbs_essentials_for_pib (username, userpass, firstname, lastname, tbs_emp_type, tbs_tax_loc, tbs_department_id, tbs_job_code)");
            sqlString.Append("VALUES ('" + userName + "',MD5('" + userPass + "'),'" + firstName.Replace("'", "") + "', '" + lastName.Replace("'", "") + "' , '" + tbs_emp_type + "','" + tbs_tax_loc + "', '" + dept_id + "',  '" + tbs_job_code + "') RETURNING uuid;");

            OdbcCommand insertSQlCmd = new OdbcCommand(Convert.ToString(sqlString), DbConnection);
            returnUUID = Convert.ToString(insertSQlCmd.ExecuteScalar());
            insertSQlCmd.Dispose();
        }
        catch (Exception ex)
        { ErrorLog.createLog("update pensivo DB " + ex.Message + ex.StackTrace); }
        finally { DbConnection.Close(); }

        return returnUUID;
    }


    /// <summary>
    /// To update data on Postgres server 
    /// </summary>
    /// <param name="userName">Pass User Name</param>
    /// <param name="userPass">Pass User Password</param>
    /// <param name="firstName">Pass User First Name</param>
    /// <param name="lastName">Pass User Last Name</param>
    /// <param name="tbs_emp_type">Pass User Type</param>
    /// <param name="tbs_tax_loc">Pass User Location</param>
    /// <returns></returns>
    public static Boolean PensivoUpdateUserData(int pensivoUUID, string userName, string userPass, string firstName, string lastName, string tbs_emp_type, string tbs_tax_loc, string dept_id, string tbs_job_code)
    {
        Boolean returnFlag = false;
        StringBuilder sqlString = new StringBuilder();
        //OdbcConnection DbConnection = new OdbcConnection(ConfigurationManager.AppSettings["PensivoDBConnection"]);
        OdbcConnection DbConnection = new OdbcConnection(AppConfig.GetAppConfigValue("PensivoDBConnection"));
        try
        {
            DbConnection.Open();
            sqlString.Append(" update view_tbs_essentials_for_pib set username = '" + userName + "',  firstname = '" + firstName.Replace("'", "") + "', lastname = '" + lastName.Replace("'", "") + "', tbs_emp_type =  '" + tbs_emp_type + "', tbs_tax_loc = '" + tbs_tax_loc + "', tbs_department_id =  '" + dept_id + "' , tbs_job_code = '" + tbs_job_code + "'   where uuid =" + pensivoUUID + ";  ");
            OdbcCommand insertSQlCmd = new OdbcCommand(Convert.ToString(sqlString), DbConnection);
            insertSQlCmd.ExecuteNonQuery();

            if (userPass != "")
            {
                sqlString = new StringBuilder();
                sqlString.Append(" update view_tbs_essentials_for_pib set  userpass = MD5('" + userPass + "') where uuid =" + pensivoUUID + ";  ");
                insertSQlCmd = new OdbcCommand(Convert.ToString(sqlString), DbConnection);
                insertSQlCmd.ExecuteNonQuery();
            }

            insertSQlCmd.Dispose();
            returnFlag = true;
        }
        catch (Exception ex)
        { ErrorLog.createLog("update pensivo DB " + ex.Message + ex.StackTrace); }
        finally { DbConnection.Close(); }

        return returnFlag;
    }


    /// <summary>
    /// To update user site on Postgres Server
    /// </summary>
    /// <param name="pensivoUUID"></param>
    /// <param name="dept_id"></param>
    /// <returns></returns>
    public static Boolean PensivoUpdateUserSite(int pensivoUUID, string dept_id)
    {
        Boolean returnFlag = false;
        StringBuilder sqlString = new StringBuilder();
        //OdbcConnection DbConnection = new OdbcConnection(ConfigurationManager.AppSettings["PensivoDBConnection"]);
        OdbcConnection DbConnection = new OdbcConnection(AppConfig.GetAppConfigValue("PensivoDBConnection"));
        try
        {
            DbConnection.Open();
            sqlString.Append(" update view_tbs_essentials_for_pib set  tbs_department_id =  '" + dept_id + "'   where uuid =" + pensivoUUID + ";  ");
            OdbcCommand insertSQlCmd = new OdbcCommand(Convert.ToString(sqlString), DbConnection);
            insertSQlCmd.ExecuteNonQuery();

            insertSQlCmd.Dispose();
            returnFlag = true;
        }
        catch (Exception ex)
        { ErrorLog.createLog("update pensivo DB " + ex.Message + ex.StackTrace); }
        finally { DbConnection.Close(); }

        return returnFlag;
    }


    /// <summary>
    /// To update user location on Postgres Server
    /// </summary>
    /// <param name="pensivoUUID"></param>
    /// <param name="tbs_tax_loc"></param>
    /// <returns></returns>
    public static Boolean PensivoUpdateUserLocation(int pensivoUUID, string tbs_tax_loc)
    {
        Boolean returnFlag = false;
        StringBuilder sqlString = new StringBuilder();
        OdbcConnection DbConnection = new OdbcConnection(AppConfig.GetAppConfigValue("PensivoDBConnection"));
        try
        {
            DbConnection.Open();
            sqlString.Append(" update view_tbs_essentials_for_pib set  tbs_tax_loc =  '" + tbs_tax_loc + "'   where uuid =" + pensivoUUID + ";  ");
            OdbcCommand insertSQlCmd = new OdbcCommand(Convert.ToString(sqlString), DbConnection);
            insertSQlCmd.ExecuteNonQuery();

            insertSQlCmd.Dispose();
            returnFlag = true;
        }
        catch (Exception ex)
        { ErrorLog.createLog("update pensivo DB " + ex.Message + ex.StackTrace); }
        finally { DbConnection.Close(); }

        return returnFlag;
    }


    /// <summary>
    /// To get site details from Postgres server 
    /// </summary> 
    /// <returns>Data Table of Sites</returns>
    public static DataTable PensivoSiteDetails(string city, string streetName)
    {
        //OdbcConnection DbConnection = new OdbcConnection(ConfigurationManager.AppSettings["PensivoDBConnection"]);
        OdbcConnection DbConnection = new OdbcConnection(AppConfig.GetAppConfigValue("PensivoDBConnection"));
        DataTable dt = new System.Data.DataTable();
        StringBuilder sqlString = new StringBuilder();
        try
        {
            DbConnection.Open();
            sqlString.Append("select dept_id, descr, address, city, plaza from view_tbs_store_tax_loc WHERE 1=1");
            if (!string.IsNullOrEmpty(city))
            {
                sqlString.Append("  AND  lower(city)  like '%" + city.ToLower() + "%'    ");
            }
            if (!string.IsNullOrEmpty(streetName))
            {
                sqlString.Append("  AND  lower(address)  like '%" + streetName.ToLower() + "%' ");
            }
            //ErrorLog.createLog(BusinessUtility.GetString( sqlString));

            OdbcDataAdapter da = new OdbcDataAdapter(Convert.ToString(sqlString), DbConnection);
            da.Fill(dt);

            if (city == "" && streetName == "")
            {
                dt = dt.Clone();
            }

            //string sExcelDownloadPath = Utils.ReportDownloadDiractory;
            //string sFileName = "PensivoStores" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            //sExcelDownloadPath += @"\" + sFileName;
            //ExcelHelper.ExportToExcel(dt, sExcelDownloadPath);              
        }
        catch (Exception ex)
        {
            ErrorLog.createLog("update pensivo DB " + ex.Message + ex.StackTrace);
        }
        finally
        {
            DbConnection.Close();
        }
        return dt;
    }

    /// <summary>
    /// To Get Site Detail
    /// </summary>
    /// <param name="siteID">Pass Site ID</param>
    /// <returns>Datatable</returns>
    public static DataTable PensivoSiteDetails(string siteID)
    {
        //OdbcConnection DbConnection = new OdbcConnection(ConfigurationManager.AppSettings["PensivoDBConnection"]);
        OdbcConnection DbConnection = new OdbcConnection(AppConfig.GetAppConfigValue("PensivoDBConnection"));
        DataTable dt = new System.Data.DataTable();
        StringBuilder sqlString = new StringBuilder();
        try
        {
            DbConnection.Open();
            sqlString.Append("select dept_id, descr, address, city, plaza from view_tbs_store_tax_loc WHERE 1=1");
            if (siteID != "")
            {
                sqlString.Append("  AND  dept_id  = '" + siteID + "'    ");
            }
            OdbcDataAdapter da = new OdbcDataAdapter(Convert.ToString(sqlString), DbConnection);
            da.Fill(dt);
        }
        catch (Exception ex)
        {
            ErrorLog.createLog("update pensivo DB " + ex.Message + ex.StackTrace);
        }
        finally
        {
            DbConnection.Close();
        }
        return dt;
    }

}