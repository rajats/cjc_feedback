﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Configuration;
using System.IO;
using EvoPdf;
using System.Web.UI;

using System.Resources;



/// <summary>
/// To define functionality for AI report
/// </summary>
public class CommonPrint
{
    /// <summary>
    /// To get AI report print 
    /// </summary>
    /// <param name="aiReportType">Pass AI Report Type</param>
    /// <param name="objAIQuestion">Pass AI Question List</param>
    /// <param name="objEmpDetails">Pass Employee Details List</param>
    /// <returns>string</returns>
    public string GetAIReportPrint(string aiReportType, List<AIQuestionAnswer> objAIQuestion, List<Employee> objEmpDetails)
    {
        string sFolderPath = this.CertificatePath;
        if (!(Directory.Exists(sFolderPath)))
        {
            Directory.CreateDirectory(sFolderPath);
        }

        string sDate = this.CertificatePath + @"/" + DateTime.Now.ToString("yyyy-MM-dd");
        if (!(Directory.Exists(sDate)))
        {
            Directory.CreateDirectory(sDate);
        }
        sFolderPath = sDate;

        var affectedEmployeeDetails = objEmpDetails.FirstOrDefault();
        string sFileName = "AIReport_" + BusinessUtility.GetString(affectedEmployeeDetails.EmpExtID) + "_" + BusinessUtility.GetString(DateTime.Now.ToString("yyyyMMddhhmmss")) + ".pdf";
        sFolderPath += @"/" + sFileName;
        string printTemplet = "";

        if (aiReportType == AIReportType.WVHOnly)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/AIWVHOnly.html"));

            if (objAIQuestion != null)
            {
                var result = objAIQuestion.Where(x => x.AIPageName == PageName.IncidentAccidentDetail).ToList();

                foreach (var reportResult in objAIQuestion)
                {

                }

                var detail = objAIQuestion.FirstOrDefault(x => x.AIPageName == PageName.IncidentAccidentDetail6);

            }

            printTemplet = printTemplet.Replace("#AFFECTEDEMPNAME#", affectedEmployeeDetails.EmpName)
                           .Replace("#EMPNO#", affectedEmployeeDetails.EmpExtID)
                           .Replace("#LOCATION#", affectedEmployeeDetails.Location);

            var objWVHoffenders = objAIQuestion.Where(x => x.AIPageName == PageName.WVHOffenders).ToList();
            if (objWVHoffenders != null && objWVHoffenders.Count > 0)
            {
                foreach (var res in objWVHoffenders)
                {
                    if (res.AISequence == 1)
                    {
                        printTemplet = printTemplet.Replace("#OffenderName#", res.AIQuestionAnswerText);
                    }
                    else if (res.AISequence == 2)
                    {
                        printTemplet = printTemplet.Replace("#OffenderIdentifyingInfo#", res.AIQuestionAnswerText);
                    }
                    else if (res.AISequence == 3 && res.AIQuestionAnswerText != "")
                    {
                        printTemplet = printTemplet.Replace("#OffenderCoWorker#", "air-checkboxis-on");
                    }
                    else if (res.AISequence == 4)
                    {
                        printTemplet = printTemplet.Replace("#OffenderSupervisor#", "air-checkboxis-on");
                    }
                    else if (res.AISequence == 5)
                    {
                        printTemplet = printTemplet.Replace("#OffenderFormerEmployee#", "air-checkboxis-on");
                    }
                    else if (res.AISequence == 6)
                    {
                        printTemplet = printTemplet.Replace("#OffenderFormerEmployee#", "air-checkboxis-on");
                    }
                    else if (res.AISequence == 7)
                    {
                        printTemplet = printTemplet.Replace("#OffenderSpouse#", "air-checkboxis-on");
                    }
                    else if (res.AISequence == 9)
                    {
                        printTemplet = printTemplet.Replace("#OffenderOtherText#", res.AIQuestionAnswerText);
                    }
                }
            }

            var objWVDescription = objAIQuestion.FirstOrDefault(x => x.AIPageName == PageName.WVHDescription);
            if (objWVDescription != null)
            {
                printTemplet = printTemplet.Replace("#Details7Description#", objWVDescription.AIQuestionAnswerText);

            }
            var objWVDescription8 = objAIQuestion.FirstOrDefault(x => x.AIPageName == PageName.WVHEventOccurrence);
            if (objWVDescription != null)
            {
                printTemplet = printTemplet.Replace("#Details8Description#", objWVDescription.AIQuestionAnswerText);
            }


            var objActionPrevention = objAIQuestion.Where(x => x.AIPageName == PageName.ActionPreventionForm).ToList();
            foreach (var res in objActionPrevention)
            {
                if (res.AISequence == 1)
                {
                    printTemplet = printTemplet.Replace("#ActionPrevAction1#", res.AIQuestionAnswerText);
                }
                else if (res.AISequence == 2)
                {
                    printTemplet = printTemplet.Replace("#ActionPrevResponsible1#", res.AIQuestionAnswerText);
                }
                else if (res.AISequence == 3)
                {
                    printTemplet = printTemplet.Replace("#ActionPrevDate1#", res.AIQuestionAnswerText);
                }
            }

        }

        printTemplet = printTemplet + "<div style='page-break-before: always; width: 100%; height: 500px; background-color: gainsboro; border: 2px solid gray; text-align: center'>";
        printTemplet = printTemplet + File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/AIWVHCommon.html"));
        printTemplet = printTemplet + "</div>";

        if (CreateCertificatePdf(printTemplet, sFolderPath))
        {
            return sFolderPath;
        }
        else
        {
            return "";
        }
    }

    /// <summary>
    /// To create AI report from HTML file
    /// </summary>
    /// <param name="sHtml">Pass HTML</param>
    /// <param name="sFileName">Pass File Name</param>
    /// <returns>True/False</returns>
    public Boolean CreateCertificatePdf(string sHtml, string sFileName)
    {
        bool bReturn = false;
        try
        {
            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();
            CertificateCreate objCreateCrt = new CertificateCreate();
            htmlToPdfConverter.LicenseKey = objCreateCrt.EVOlicense;
            htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
            htmlToPdfConverter.ConversionDelay = int.Parse("0");

            byte[] outPdfBuffer = null;

            string htmlString = sHtml;
            string baseUrl = "";

            outPdfBuffer = htmlToPdfConverter.ConvertHtml(htmlString, baseUrl);

            using (Stream file = File.OpenWrite(sFileName))
            {
                file.Write(outPdfBuffer, 0, outPdfBuffer.Length);
                file.Close();
            }

            bReturn = true;
        }
        catch (Exception e)
        {
            ErrorLog.createLog(e.Message);
            throw;
        }
        return bReturn;
    }

    /// <summary>
    /// To get folder path where AI report to be saved
    /// </summary>
    public string CertificatePath
    {
        get
        {
            return BusinessUtility.GetString(ConfigurationManager.AppSettings["AIReportDirectory"]);
        }
    }

}

public class Resource
{
    #region ReadResourceFile
    public static string ResourceValue(string key)
    {
        string resourceValue = string.Empty;
        try
        {
            string resxFile = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "App_GlobalResources\\" + ResourseFileName();
            //using (ResXResourceReader resxReader = new ResXResourceReader(resxFile))
            using (ResXResourceSet resxSet = new ResXResourceSet(resxFile))
            {
                resourceValue = resxSet.GetString(key);
                //foreach (DictionaryEntry entry in resxReader)
                //{
                //    //if (((string)entry.Key).StartsWith("EarlyAuto"))
                //    //    autos.Add((Automobile)entry.Value);
                //    //else if (((string)entry.Key).StartsWith("Header"))
                //    //    headers.Add((string)entry.Key, (string)entry.Value);
                //}
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            resourceValue = string.Empty;
        }
        return resourceValue;
    }

    public static string ResourceValue(string key, string culterName)
    {
        string resourceValue = string.Empty;
        try
        {
            string resxFile = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "App_GlobalResources\\" + ResourseFileName(culterName);
            using (ResXResourceSet resxSet = new ResXResourceSet(resxFile))
            {
                resourceValue = resxSet.GetString(key);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            resourceValue = string.Empty;
        }
        return resourceValue;
    }

    private static string ResourseFileName()
    {
        if (Globals.CurrentCultureName == "fr-CA")
        {
            return "Resource.fr-CA.resx";
        }
        else
        {
            return "Resource.resx";
        }
    }

    private static string ResourseFileName(string cultureName)
    {
        if (cultureName == "fr-CA")
        {
            return "Resource.fr-CA.resx";
        }
        else
        {
            return "Resource.resx";
        }
    }
    #endregion
}