using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using RusticiSoftware.HostedEngine.Client;
using iTECH.Library.Utilities;
using System.Text.RegularExpressions;
using iTECH.Pensivo.BusinessLogic;

/// <summary>
/// To define attributes to read web config values
/// </summary>
public class Utils
{
    /// <summary>
    /// To Set Site Name
    /// </summary>
    protected static String _siteName = AppConfig.GetAppConfigValue("SiteName");//ConfigurationManager.AppSettings["SiteName"];

    /// <summary>
    /// To Set Scorm Registration Prefix
    /// </summary>
    protected static String _ScromRegPrfx = AppConfig.GetAppConfigValue("ScromRegPrfx");//ConfigurationManager.AppSettings["ScromRegPrfx"];

    /// <summary>
    /// To Set Security Question
    /// </summary>
    //protected static String _EnableSecurityQuesetionOnLogIN = ConfigurationManager.AppSettings["EnableSecurityQuesetionOnLogIN"];
    protected static String _EnableSecurityQuesetionOnLogIN = AppConfig.GetAppConfigValue("EnableSecurityQuesetionOnLogIN");

    /// <summary>
    /// To Set Enable To Create New User
    /// </summary>
    //protected static String _EnableNewUser = ConfigurationManager.AppSettings["EnableNewUser"];
    protected static String _EnableNewUser = AppConfig.GetAppConfigValue("EnableNewUser");

    /// <summary>
    /// To Set Employee ID Validator Expression
    /// </summary>
    //protected static String _EmpIdValidatorExpression = BusinessUtility.GetString(ConfigurationManager.AppSettings["EmpIdValidatorExpression"]);
    protected static String _EmpIdValidatorExpression = AppConfig.GetAppConfigValue("EmpIdValidatorExpression");

    /// <summary>
    /// To Set AI Server URL
    /// </summary>
    //protected static String _AIServerURL = BusinessUtility.GetString(ConfigurationManager.AppSettings["AIServerURL"]);
    protected static String _AIServerURL = BusinessUtility.GetString(AppConfig.GetAppConfigValue("AIServerURL"));

    /// <summary>
    /// To Set Beer College Server URL
    /// </summary>
    //protected static String _BeerCollegeServerURL = BusinessUtility.GetString(ConfigurationManager.AppSettings["BeerCollegeServerURL"]);
    protected static String _BeerCollegeServerURL = BusinessUtility.GetString(AppConfig.GetAppConfigValue("BeerCollegeServerURL"));

    /// <summary>
    /// To Set Beer College Server URL
    /// </summary>
    //protected static String _AIDashboardServerURL = BusinessUtility.GetString(ConfigurationManager.AppSettings["AIDashboardServerURL"]);
    protected static String _AIDashboardServerURL = BusinessUtility.GetString(AppConfig.GetAppConfigValue("AIDashboardServerURL"));

    /// <summary>
    /// To Set Store Manager Server URL
    /// </summary>
    //protected static String _StoreManagerServerURL = BusinessUtility.GetString(ConfigurationManager.AppSettings["StoreManagerServerURL"]);
    protected static String _StoreManagerServerURL = BusinessUtility.GetString(AppConfig.GetAppConfigValue("StoreManagerServerURL"));

    /// <summary>
    /// To Set Scorm Css URL
    /// </summary>
    //protected static String _ScormCssUrl = BusinessUtility.GetString(ConfigurationManager.AppSettings["ScormCssUrl"]);
    protected static String _ScormCssUrl = BusinessUtility.GetString(AppConfig.GetAppConfigValue("ScormCssUrl"));

    /// <summary>
    /// To Set Support Email From
    /// </summary>
    //protected static String _SupportEmailFrom = BusinessUtility.GetString(ConfigurationManager.AppSettings["SupportEmailFrom"]);
    protected static String _SupportEmailFrom = BusinessUtility.GetString(AppConfig.GetAppConfigValue("SupportEmailFrom"));

    /// <summary>
    /// To Set Support Email To
    /// </summary>
    //protected static String _SupportEmailTo = BusinessUtility.GetString(ConfigurationManager.AppSettings["SupportEmailTo"]);
    protected static String _SupportEmailTo = BusinessUtility.GetString(AppConfig.GetAppConfigValue("SupportEmailTo"));

    /// <summary>
    /// To Set Training Institute
    /// </summary>
    //protected static int _TrainingInst = BusinessUtility.GetInt(ConfigurationManager.AppSettings["TrainingInst"]);
    protected static int _TrainingInst = BusinessUtility.GetInt(AppConfig.GetAppConfigValue("TrainingInst"));

    /// <summary>
    /// To Set LogoPath
    /// </summary>
    //protected static string _LogoPath = BusinessUtility.GetString(ConfigurationManager.AppSettings["LogoPath"]);
    protected static string _LogoPath = BusinessUtility.GetString(AppConfig.GetAppConfigValue("LogoPath"));

    /// <summary>
    /// To Set Training Institute Title
    /// </summary>
    //protected static string _TrainingInstTitle = BusinessUtility.GetString(ConfigurationManager.AppSettings["TrainingInstTitle"]);
    protected static string _TrainingInstTitle = BusinessUtility.GetString(AppConfig.GetAppConfigValue("TrainingInstTitle"));

    /// <summary>
    /// To Set Training Institute Logo
    /// </summary>
    //protected static string _TrainingInstLogo = BusinessUtility.GetString(ConfigurationManager.AppSettings["TrainingInstLogo"]);
    protected static string _TrainingInstLogo = BusinessUtility.GetString(AppConfig.GetAppConfigValue("TrainingInstLogo"));

    /// <summary>
    /// To Set Training Inst Web Address
    /// </summary>
    //protected static string _TrainingInstWebAdd = BusinessUtility.GetString(ConfigurationManager.AppSettings["TrainingInstWebAdd"]);
    protected static string _TrainingInstWebAdd = BusinessUtility.GetString(AppConfig.GetAppConfigValue("TrainingInstWebAdd"));

    /// <summary>
    /// To Set Training Inst Web Title
    /// </summary>
    //protected static string _TrainingInstWebAddTitle = BusinessUtility.GetString(ConfigurationManager.AppSettings["TrainingInstWebAddTitle"]);
    protected static string _TrainingInstWebAddTitle = BusinessUtility.GetString(AppConfig.GetAppConfigValue("TrainingInstWebAddTitle"));

    /// <summary>
    /// To Set Test Attempt No
    /// </summary>
    //protected static int _TestAttemptsNo = BusinessUtility.GetInt(ConfigurationManager.AppSettings["TestAttemptsNo"]);
    protected static int _TestAttemptsNo = BusinessUtility.GetInt(AppConfig.GetAppConfigValue("TestAttemptsNo"));

    /// <summary>
    /// To Set Test Attempt Eligible Minutes
    /// </summary>
    //protected static int _TestAttemptsEligableMins = BusinessUtility.GetInt(ConfigurationManager.AppSettings["TestAttemptsEligableMins"]);
    protected static int _TestAttemptsEligableMins = BusinessUtility.GetInt(AppConfig.GetAppConfigValue("TestAttemptsEligableMins"));

    /// <summary>
    /// To Set Report Download Path
    /// </summary>
    //protected static string _ReportDownloadDiractory = BusinessUtility.GetString(ConfigurationManager.AppSettings["ReportDownloadDiractory"]);
    protected static string _ReportDownloadDiractory = BusinessUtility.GetString(AppConfig.GetAppConfigValue("ReportDownloadDiractory"));


    /// <summary>
    /// To split string into array
    /// </summary>
    /// <param name="list">Pass Text String</param>
    /// <param name="splitVal">Pass Char To split</param>
    /// <returns>string</returns>
    public static String[] SplitAndTrim(String list, Char splitVal)
    {
        String[] vals = list.Split(splitVal);
        for (int i = 0; i < vals.Length; i++)
        {
            vals[i] = vals[i].Trim();
        }
        return vals;
    }

    /// <summary>
    /// To replace special character
    /// </summary>
    /// <param name="stringToReplaceSpclChar">Pass String To validate</param>
    /// <param name="sValueTobeReplace">Pass Char to replace</param>
    /// <returns></returns>
    public static String ReplaceSpecialCharacter(string stringToReplaceSpclChar, string sValueTobeReplace)
    {
        Regex rgx = new Regex(@"([^a-zA-Z0-9_]|^\s)");
        string str = rgx.Replace(stringToReplaceSpclChar, sValueTobeReplace);
        return str;
    }


    /// <summary>
    /// To Replace Special Char To support DB
    /// </summary>
    /// <param name="stringToReplace"></param>
    /// <returns></returns>
    public static String ReplaceDBSpecialCharacter(string stringToReplace)
    {
        return stringToReplace.Replace("'", @"\'").Replace("%", @"\%").Replace("_", @"\_").Replace(@"\", @"\\\");
    }

    /// <summary>
    /// To Get Site Name
    /// </summary>
    /// <returns></returns>
    public static string GetSiteName()
    {
        return BusinessUtility.GetString(AppConfig.GetAppConfigValue("SiteName"));
    }

    /// <summary>
    /// To Get Scorm Registration Prefix
    /// </summary>
    public static string ScromRegPrfx
    {
        get
        {
            return AppConfig.GetAppConfigValue("ScromRegPrfx");// _ScromRegPrfx;
        }
    }

    /// <summary>
    /// To Get  Security Question
    /// </summary>
    public static Boolean EnableSecurityQuesetionOnLogIN
    {
        get
        {
            if (BusinessUtility.GetString(AppConfig.GetAppConfigValue("EnableSecurityQuesetionOnLogIN")).ToUpper() == "TRUE")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// To Enable New user creation on signup
    /// </summary>
    public static Boolean EnableNewUser
    {
        get
        {
            if (BusinessUtility.GetString(AppConfig.GetAppConfigValue("EnableNewUser")).ToUpper() == "TRUE")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// To Get Employee ID Validator Expression
    /// </summary>
    public static string EmpIdValidatorExpression
    {
        get
        {
            return AppConfig.GetAppConfigValue("EmpIdValidatorExpression");
        }
    }

    /// <summary>
    /// To Get AI Server URL
    /// </summary>
    public static string AIServerURL
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("AIServerURL"));
        }
    }

    /// <summary>
    /// To Get Beer College Server URL
    /// </summary>
    public static string BeerCollegeServerURL
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("BeerCollegeServerURL"));
        }
    }

    /// <summary>
    /// To Get AI Dashboard Server URL
    /// </summary>
    public static string AIDashboardServerURL
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("AIDashboardServerURL"));
        }
    }

    /// <summary>
    /// To Get Store Manager Server URL
    /// </summary>
    public static string StoreManagerServerURL
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("StoreManagerServerURL"));
        }
    }


    /// <summary>
    /// To Get AI2015 URL 
    /// </summary>
    public static string AI2015URL
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("AI2015URL"));
        }
    }

    /// <summary>
    /// To Get Scorm Css URL
    /// </summary>
    public static string ScormCssUrl
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("ScormCssUrl"));
        }
    }

    /// <summary>
    /// To Get Support Email From
    /// </summary>
    public static string SupportEmailFrom
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("SupportEmailFrom"));
        }
    }

    /// <summary>
    ///  To Get Support Email To
    /// </summary>
    public static string SupportEmailTo
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("SupportEmailTo"));
        }
    }

    /// <summary>
    /// To Get Training Institute
    /// </summary>
    public static int TrainingInst
    {
        get
        {
            return BusinessUtility.GetInt(AppConfig.GetAppConfigValue("TrainingInst"));
        }
    }

    /// <summary>
    /// To Get LogoPath
    /// </summary>
    public static string LogoPath
    {
        get
        {
            //return _LogoPath;
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("LogoPath"));
        }
    }

    /// <summary>
    /// To Get Training Institute Title
    /// </summary>
    public static string TrainingInstTitle
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("TrainingInstTitle"));
        }
    }

    /// <summary>
    /// To Get Training Institute Logo
    /// </summary>
    public static string TrainingInstLogo
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("TrainingInstLogo"));
        }
    }

    /// <summary>
    /// To Get Training Inst Web Address
    /// </summary>
    public static string TrainingInstWebAdd
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("TrainingInstWebAdd"));
        }
    }

    /// <summary>
    /// To Get Training Inst Web Title
    /// </summary>
    public static string TrainingInstWebAddTitle
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("TrainingInstWebAddTitle")); 
        }
    }

    /// <summary>
    /// To Get XML Save Path
    /// </summary>
    public static string XMLPath
    {
        get
        {
            //return _XMLPath;
            if (BusinessUtility.GetString(ConfigurationManager.AppSettings["server-type"]).ToUpper() == "call-back".ToUpper())
            {
                return BusinessUtility.GetString(AppConfig.GetAppConfigValue("XMLDirectory-callback"));
            }
            else
            {
                return BusinessUtility.GetString(AppConfig.GetAppConfigValue("XMLDirectory"));
            }
        }
    }

    /// <summary>
    /// To Get Test Attempt No
    /// </summary>
    public static int TestAttemptsNo
    {
        get
        {
            return BusinessUtility.GetInt(AppConfig.GetAppConfigValue("TestAttemptsNo"));
        }
    }

    /// <summary>
    /// To Get Test Attempt Eligible Minutes
    /// </summary>
    public static int TestAttemptsEligableMins
    {
        get
        {
            return BusinessUtility.GetInt(AppConfig.GetAppConfigValue("TestAttemptsEligableMins"));
        }
    }


    /// <summary>
    /// To Get Report Download Diractory
    /// </summary>
    public static string ReportDownloadDiractory
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("ReportDownloadDiractory"));
        }
    }

    /// <summary>
    /// To Get Role Edit Mail To
    /// </summary>
    public static string RoleEditMailTo
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("RoleEditMailTo"));
        }
    }

    /// <summary>
    /// To Get Application Environment
    /// </summary>
    public static string ApplicationEnvironement
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("AppEnvironment"));
        }
    }


    public static string FeedbackMessage
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("FeedbackMessage"));
        }
    }

}


