﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Data;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.Pensivo.BusinessLogic;
using System.Data;
using System.Diagnostics;
using System.Threading;

/// <summary>
/// Summary description for ExecutingThread
/// </summary>
public class ExecutingThread
{
	/// <summary>
	/// Assign Course By Role
	/// </summary>
	/// <param name="roleID">Pass Role ID</param>
	/// <param name="eventID">Pass Event ID</param>
	/// <param name="inActiveReason">Pass InActive Reason</param>
	/// <param name="lastRecordUpdateSource">Last Updated Source</param>
	/// <returns></returns>
    public static Boolean ThreadEmployeeAssignedCoursesByRole(int roleID, int eventID, int inActiveReason, int lastRecordUpdateSource)
    {
        EmpAssignedCourses objEmpAssignedCourses = new EmpAssignedCourses();
        objEmpAssignedCourses.RoleID = roleID;
        objEmpAssignedCourses.CourseID = eventID;
        objEmpAssignedCourses.EmailFrom = Utils.SupportEmailFrom;
        objEmpAssignedCourses.EmailTo = Utils.RoleEditMailTo;
        objEmpAssignedCourses.CurrentUserLogInID = CurrentEmployee.EmpID;
        objEmpAssignedCourses.InActiveReason = inActiveReason;
        objEmpAssignedCourses.LastRecordUpdateSource = lastRecordUpdateSource;
        Thread workerThread = new Thread(objEmpAssignedCourses.ResetEmployeeAssignedCoursesByRoleThread);
        workerThread.Start();
        return true;
    }

    /// <summary>
    /// Assign Course By Employee ID
    /// </summary>
    /// <param name="empID">Pass Employee ID</param>
    /// <param name="courseID">Pass Course ID</param>
    /// <param name="inActiveReason">Pass InActive Reason</param>
    /// <param name="lastRecordUpdateSource">Pass Last Updated Source</param>
    /// <returns></returns>
    public static Boolean ThreadAssignedCoursesByEmployee(int empID, int courseID, int inActiveReason, int lastRecordUpdateSource)
    {
        EmpAssignedCourses objEmpAssignedCourses = new EmpAssignedCourses();
        objEmpAssignedCourses.EmployeeID = empID;
        objEmpAssignedCourses.CourseID = courseID;
        objEmpAssignedCourses.EmailFrom = Utils.SupportEmailFrom;
        objEmpAssignedCourses.EmailTo = Utils.RoleEditMailTo;
        objEmpAssignedCourses.CurrentUserLogInID = CurrentEmployee.EmpID;
        objEmpAssignedCourses.InActiveReason = inActiveReason;
        objEmpAssignedCourses.LastRecordUpdateSource = lastRecordUpdateSource;
        Thread workerThread = new Thread(objEmpAssignedCourses.EmployeeAssignedCoursesByThread);
        workerThread.Start();
        return true;
    }


    //public static Boolean ThreadRemoveEmployeeCoursesByRole(int roleID, int eventID)
    //{
    //    EmpAssignedCourses objEmpAssignedCourses = new EmpAssignedCourses();
    //    objEmpAssignedCourses.RoleID = roleID;
    //    objEmpAssignedCourses.CourseID = eventID;
    //    objEmpAssignedCourses.EmailFrom = Utils.SupportEmailFrom;
    //    objEmpAssignedCourses.EmailTo = Utils.RoleEditMailTo;
    //    objEmpAssignedCourses.CurrentUserLogInID = CurrentEmployee.EmpID;
    //    Thread workerThread = new Thread(objEmpAssignedCourses.RemoveEmployeeAssignedCoursesByRoleThread);
    //    workerThread.Start();
    //    return true;
    //}

}