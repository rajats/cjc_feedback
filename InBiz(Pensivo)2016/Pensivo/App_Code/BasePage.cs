﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iTECH.Library.Web;
using iTECH.Pensivo.BusinessLogic;
using jq = Trirand.Web.UI.WebControls;


/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : System.Web.UI.Page
{
    /// <summary>
    /// To override page class
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);
    }

    /// <summary>
    /// To Initialize Culture
    /// </summary>
    protected override void InitializeCulture()
    {

        Globals.SetCultureInfo(AppCulture.ENGLISH_CANADA);
        //Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    /// <summary>
    /// To handle jqgrid postback event
    /// </summary>
    /// <param name="grids">Pass JQGrid Array List</param>
    /// <returns></returns>
    public bool IsPagePostBack(params jq.JQGrid[] grids)
    {
        bool isPostBack = IsPostBack;
        foreach (jq.JQGrid item in grids)
        {
            if (item.AjaxCallBackMode != jq.AjaxCallBackMode.None)
            {
                isPostBack = true;
                break;
            }
        }
        return isPostBack;
    }
}