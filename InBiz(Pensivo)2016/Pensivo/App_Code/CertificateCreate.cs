﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Text;
using System.IO;
using System.Configuration;
using EvoPdf;

/// <summary>
/// To define functionality for certificate
/// </summary>
public class CertificateCreate
{
    /// <summary>
    /// To Define PDF File Name
    /// </summary>
    public string sPdfFileName = "";

    /// <summary>
    /// To Define PDF File Path
    /// </summary>
    public string sPdfFilePath = "";


    /// <summary>
    /// To save and download certificate
    /// </summary>
    /// <param name="regID">Pass Course Registration ID</param>
    public void DownloadCertificate(int regID, Boolean isDownloadCertificate)
    {
        if (regID > 0)
        {
            Certificate objCert = new Certificate();
            string sSaveFileName = objCert.GetCertificateFilePath(regID);

            if (sSaveFileName == "")
            {
                EmployeeCourses obj = new EmployeeCourses();
                DataTable dt = new DataTable();
                obj.regID = regID;


                EmployeeCourseRegistration objEmpCourseReg = new EmployeeCourseRegistration();
                objEmpCourseReg.GetEmpRegCourseCertificateDetail(regID);

                //if (dt.Rows.Count > 0)
                //{
                Employee objEmp = new Employee();
                objEmp.GetEmployeeDetail(BusinessUtility.GetInt(objEmpCourseReg.empID));
                int empID = objEmpCourseReg.empID;

                string sFolderPath = this.CertificatePath;
                if (!(Directory.Exists(sFolderPath)))
                {
                    Directory.CreateDirectory(sFolderPath);
                }

                string sDate = this.CertificatePath + @"/" + DateTime.Now.ToString("yyyy-MM-dd");
                if (!(Directory.Exists(sDate)))
                {
                    Directory.CreateDirectory(sDate);
                }
                sFolderPath = sDate;

                string sFileName = "Certificate_" + BusinessUtility.GetString(empID) + "_" + BusinessUtility.GetString(objEmpCourseReg.eventID) + "_" + BusinessUtility.GetString(objEmpCourseReg.eventVerNo) + BusinessUtility.GetString(DateTime.Now.ToString("yyyyMMddhhmmss")) + ".pdf";
                sFolderPath += @"/" + sFileName;

                objCert = new Certificate();
                objCert.certificatePath = @"/" + DateTime.Now.ToString("yyyy-MM-dd") + @"/" + sFileName;
                objCert.empID = empID;
                objCert.eventID = objEmpCourseReg.eventID;
                objCert.eventVerNo = objEmpCourseReg.eventVerNo;
                objCert.regID = regID;
                objCert.SaveCertificate();

                //string sTitle = BusinessUtility.GetString(dt.Rows[0]["CourseTitle"]);
                //string sCompletedDate = Convert.ToDateTime(dt.Rows[0]["enddate"]).ToString("MMMM dd, yyyy");
                //string sScore = BusinessUtility.GetDecimal(BusinessUtility.GetString(dt.Rows[0]["score"])).ToString("###.##");

                string sTitle = "";
                string sCompletedDate = Convert.ToDateTime(objEmpCourseReg.endDate).ToString("MMMM dd, yyyy");
                string sScore = BusinessUtility.GetDecimal(BusinessUtility.GetString(objEmpCourseReg.passingScore)).ToString("###.##");

                string printTemplet = "";
                string sEpochTime = "";

                TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
                int secondsSinceEpoch = (int)t.TotalSeconds;
                sEpochTime = BusinessUtility.GetString(secondsSinceEpoch);

                Event objEventDetail = new Event();
                objEventDetail.GetEventDetail(BusinessUtility.GetInt(objEmpCourseReg.eventID), Globals.CurrentAppLanguageCode);

                if (objEmpCourseReg.eventLaunchedLanguage == AppLanguageCode.FR)
                {
                    sTitle = objEventDetail.CourseTitleFr;
                }
                else
                {
                    sTitle = objEventDetail.CourseTitleEn;
                }

                if (objEventDetail.CertificateID == 1)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/CourseCertificate.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 5)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/CourseTestCertificate.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#COURSEPERCENTAGE#", sScore)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 2)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/CourseCertificateLogistics.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 3)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/CourseCertificateRetail.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 4)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/CourseCertificateCompany.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 6)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewCompanyCore2015Crt.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 7)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewCompanyEST2015Crt.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 8)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewRetailCore2015Crt.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 9)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewRetailCF2015Crt.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 10)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewRetailACC2015Crt.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 11)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewRetailBIO2015Crt.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 12)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewLogisticsCore2015Crt.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 13)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewLogisticsCFJ2015Crt.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 14)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewCore2015.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 15)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewNS2015.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 16)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PolicyReviewCoreManagerSupervisor2015.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 17)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/TDCCourseCertificate.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 18)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NavCanadaCourseCertificate.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 19)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyCORE.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 20)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyLDSX.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 21)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyLOG.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 22)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyLOGDS.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 23)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyLOGDSSAL.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 24)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyRDS.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 25)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyRET.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 26)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyRL.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 27)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyRLDS.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 28)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyRLDSSAL.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 29)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanySAL.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 30)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewDSCORE.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 31)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewDSPV.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 32)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewDSVA.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 33)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewDSVSU.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 34)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewBDLLDSX.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 35)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewBDLCORE.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }
                else if (objEventDetail.CertificateID == 36)
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewBDLCORESAL.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }

                else
                {
                    printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/CourseCertificate.html"));
                    printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                        .Replace("#COURSENAME#", sTitle)
                        .Replace("#EMPLYEENAME#", objEmp.EmpName)
                        .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                        .Replace("#COMPLETEDATE#", sCompletedDate)
                        .Replace("#LOGOPATH#", Utils.LogoPath)
                        .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                        .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                        .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                        .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
                }

                if (CreateCertificatePdf(printTemplet, sFolderPath) == true)
                {
                    if (isDownloadCertificate == true)
                    {
                        DownloadCertificate(sFolderPath, sFileName);
                    }
                }
                else
                {
                    showAlertMessage();
                }
                //}
                //else
                //{
                //    showAlertMessage();
                //}
            }
            else
            {
                if (isDownloadCertificate == true)
                {
                    string sFolderPath = this.CertificatePath;
                    DownloadCertificate(sFolderPath + sSaveFileName, sSaveFileName.Replace("/", ""));
                }
            }
        }
        //}
    }

    /// <summary>
    /// To save and download test certificate
    /// </summary>
    /// <param name="testRegistrationID"></param>
    /// <returns></returns>
    public Boolean DownloadTestCertificate(int testRegistrationID)
    {
        try
        {
            if (testRegistrationID > 0)
            {
                Certificate objCert = new Certificate();
                string sSaveFileName = objCert.GetCertificateFilePath(testRegistrationID);

                if (sSaveFileName == "")
                {
                    EmployeeCourses obj = new EmployeeCourses();
                    DataTable dt = new DataTable();

                    EmployeeCourseRegistration objEmpCourseReg = new EmployeeCourseRegistration();
                    objEmpCourseReg.GetEmployeeRegistraionDetail(testRegistrationID);
                    if (BusinessUtility.GetInt(objEmpCourseReg.empID) > 0)
                    {
                        Employee objEmp = new Employee();
                        objEmp.GetEmployeeDetail(BusinessUtility.GetInt(objEmpCourseReg.empID));
                        int empID = objEmpCourseReg.empID;

                        string sFolderPath = this.CertificatePath;
                        if (!(Directory.Exists(sFolderPath)))
                        {
                            Directory.CreateDirectory(sFolderPath);
                        }

                        string sDate = this.CertificatePath + @"/" + DateTime.Now.ToString("yyyy-MM-dd");
                        if (!(Directory.Exists(sDate)))
                        {
                            Directory.CreateDirectory(sDate);
                        }
                        sFolderPath = sDate;

                        string sFileName = "TestCertificate_" + BusinessUtility.GetString(empID) + "_" + BusinessUtility.GetString(objEmpCourseReg.eventID) + "_" + BusinessUtility.GetString(objEmpCourseReg.eventVerNo) + BusinessUtility.GetString(DateTime.Now.ToString("yyyyMMddhhmmss")) + ".pdf";
                        sFolderPath += @"/" + sFileName;

                        objCert = new Certificate();
                        objCert.certificatePath = @"/" + DateTime.Now.ToString("yyyy-MM-dd") + @"/" + sFileName;
                        objCert.empID = empID;
                        objCert.eventID = objEmpCourseReg.eventID;
                        objCert.eventVerNo = objEmpCourseReg.eventVerNo;
                        objCert.regID = testRegistrationID;
                        objCert.SaveCertificate();


                        Event objEvent = new Event();
                        objEvent.GetEventDetail(BusinessUtility.GetInt(objEmpCourseReg.eventID), Globals.CurrentAppLanguageCode);
                        Boolean isCompletedCount = false;
                        if (BusinessUtility.GetInt(objEvent.TestCompletedCount) == 1)
                        {
                            isCompletedCount = true;
                        }
                        else
                        {
                            isCompletedCount = false;
                        }
                        string sTitle = BusinessUtility.GetString(objEvent.EventName);


                        EmployeeCourses objempCourse = new EmployeeCourses();
                        DataTable dtTestAttempts = objempCourse.TestCourseAttempts(testRegistrationID, isCompletedCount);//isCompletedCount
                        if (dtTestAttempts.Rows.Count > 0)
                        {
                            string printTemplet = "";
                            string sEpochTime = "";

                            string sCompletedDate = Convert.ToDateTime(dtTestAttempts.Rows[dtTestAttempts.Rows.Count - 1]["createdDateTime"]).ToString("yyyy-MM-dd");
                            //string sScore = Convert.ToDecimal(dtTestAttempts.Rows[dtTestAttempts.Rows.Count - 1]["score_raw"]).ToString("###.##");
                            string sScore = BusinessUtility.GetDecimal(BusinessUtility.GetString(dtTestAttempts.Rows[dtTestAttempts.Rows.Count - 1]["score_raw"])).ToString("###.##");

                            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
                            int secondsSinceEpoch = (int)t.TotalSeconds;
                            sEpochTime = BusinessUtility.GetString(secondsSinceEpoch);

                            Event objEventDetail = new Event();
                            objEventDetail.GetEventDetail(BusinessUtility.GetInt(objEmpCourseReg.eventID), Globals.CurrentAppLanguageCode);
                            if (objEventDetail.CertificateID == 1)
                            {
                                StringBuilder sbInteraction = new StringBuilder();
                                string interactionTemplate = "  ";
                                interactionTemplate += "<div class='contents-a' style='font-weight:bold; font-size:17px'> ";
                                interactionTemplate += "#Question#";
                                interactionTemplate += "</div> ";
                                interactionTemplate += "<div class='contents-a'> ";
                                interactionTemplate += "<span class='client-name'>Learner Answer:</span> #LearnerAnswer#   ";
                                interactionTemplate += "</div> ";
                                interactionTemplate += "<div class='contents-a' > ";
                                interactionTemplate += "<span class='client-name' >Answer Status:</span> #AnswerStatus# ";
                                interactionTemplate += "</div> ";
                                interactionTemplate += "<div class='contents-a'> ";
                                interactionTemplate += "<span class='client-name'>Correct Answer:</span> #CorrectAnswer# ";
                                interactionTemplate += "</div> ";
                                interactionTemplate += "<div style='clear:both; height:15px;'></div> ";

                                RegisterEventResult objRegResult = new RegisterEventResult();
                                Int64 iInterationID = objRegResult.GetLastTestIntrecationID(testRegistrationID);
                                if (iInterationID > 0)
                                {
                                    DataTable dtInteractionDetail = objRegResult.GetTestIntrecationDetails(iInterationID);

                                    int iquestion = 1;

                                    foreach (DataRow dRow in dtInteractionDetail.Rows)
                                    {

                                        sbInteraction.Append(
                                            interactionTemplate.Replace("#Question#", iquestion + ". " + BusinessUtility.GetString(dRow["description"]))
                                            .Replace("#LearnerAnswer#", BusinessUtility.GetString(dRow["learner_response"]).Replace("_", " "))
                                            .Replace("#AnswerStatus#", BusinessUtility.GetString(dRow["result"]).Replace("_", " "))
                                            .Replace("#CorrectAnswer#", BusinessUtility.GetString(dRow["response"]).Replace("_", " "))
                                            );

                                        iquestion += 1;
                                    }
                                }

                                printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/TestCertificate.html"));
                                printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                                    .Replace("#COURSENAME#", sTitle)
                                    .Replace("#EMPLYEENAME#", objEmp.EmpName)
                                    .Replace("#EMPID#", objEmp.EmpExtID)
                                    .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID) + "-" + sEpochTime)
                                    .Replace("#COMPLETEDATE#", sCompletedDate)
                                    .Replace("#LOGOPATH#", Utils.LogoPath)
                                    .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                                    .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                                    .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                                    .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle)
                                    .Replace("#SCORE#", sScore)
                                    .Replace("#TestInteraction#", BusinessUtility.GetString(sbInteraction));
                                //}

                                if (CreateTestCertificatePdf(printTemplet, sFolderPath) == true)
                                {
                                    return true;
                                    //DownloadCertificate(sFolderPath, sFileName);
                                }
                                else
                                {
                                    showAlertMessage();
                                    return false;
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            showAlertMessage();
                            return false;
                        }
                    }
                    else
                    {
                        //string sFolderPath = this.CertificatePath;
                        //DownloadCertificate(sFolderPath + sSaveFileName, sSaveFileName.Replace("/", ""));
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.CreateLog(ex);
            return false;
        }

    }

    /// <summary>
    /// To convert HTML certificate to pdf file
    /// </summary>
    /// <param name="sHtml">Pass Certificate HTML </param>
    /// <param name="sFileName">Pass Certificate File Name</param>
    /// <returns>True/False</returns>
    public Boolean CreateCertificatePdf(string sHtml, string sFileName)
    {
        //try
        {
            System.Globalization.CultureInfo cultureUTC = new System.Globalization.CultureInfo("en-US");

            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();
            htmlToPdfConverter.LicenseKey = EVOlicense;
            //htmlToPdfConverter.HtmlViewerWidth = int.Parse("1100");
            //htmlToPdfConverter.HtmlViewerHeight = int.Parse("900");
            htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = SelectedPdfPageOrientation();
            //htmlToPdfConverter.PdfDocumentOptions.LeftMargin =   float.Parse("24.0");
            //htmlToPdfConverter.PdfDocumentOptions.RightMargin = float.Parse("24.0");
            //htmlToPdfConverter.PdfDocumentOptions.TopMargin = float.Parse("25.0");

            htmlToPdfConverter.HtmlViewerWidth = 1100;// int.Parse("1100");
            htmlToPdfConverter.HtmlViewerHeight = 900;// int.Parse("900");
            htmlToPdfConverter.PdfDocumentOptions.LeftMargin = BusinessUtility.GetFloat(BusinessUtility.GetDouble("24.0").ToString("N2", cultureUTC));
            htmlToPdfConverter.PdfDocumentOptions.RightMargin = BusinessUtility.GetFloat(BusinessUtility.GetDouble("24.0").ToString("N2", cultureUTC));
            htmlToPdfConverter.PdfDocumentOptions.TopMargin = BusinessUtility.GetFloat(BusinessUtility.GetDouble("25.0").ToString("N2", cultureUTC));
            htmlToPdfConverter.ConversionDelay = int.Parse("0");

            byte[] outPdfBuffer = null;

            string htmlString = sHtml;
            string baseUrl = "";

            outPdfBuffer = htmlToPdfConverter.ConvertHtml(htmlString, baseUrl);

            using (Stream file = File.OpenWrite(sFileName))
            {
                file.Write(outPdfBuffer, 0, outPdfBuffer.Length);
                file.Close();
            }

            return true;
        }
        //catch (Exception e)
        //{
        //    ErrorLog.createLog(e.Message);
        //    throw;
        //}
    }

    /// <summary>
    /// To convert Test HTML certificate to pdf file
    /// </summary>
    /// <param name="sHtml">Pass Certificate HTML </param>
    /// <param name="sFileName">Pass Certificate File Name</param>
    /// <returns>True/False</returns>
    public Boolean CreateTestCertificatePdf(string sHtml, string sFileName)
    {
        try
        {
            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();
            htmlToPdfConverter.LicenseKey = EVOlicense;
            //htmlToPdfConverter.HtmlViewerWidth = int.Parse("1100");
            //htmlToPdfConverter.HtmlViewerHeight = int.Parse("900");
            htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
            htmlToPdfConverter.PdfDocumentOptions.LeftMargin = float.Parse("24.0");
            htmlToPdfConverter.PdfDocumentOptions.RightMargin = float.Parse("24.0");
            htmlToPdfConverter.PdfDocumentOptions.TopMargin = float.Parse("25.0");
            htmlToPdfConverter.ConversionDelay = int.Parse("0");

            byte[] outPdfBuffer = null;

            string htmlString = sHtml;
            string baseUrl = "";

            outPdfBuffer = htmlToPdfConverter.ConvertHtml(htmlString, baseUrl);

            using (Stream file = File.OpenWrite(sFileName))
            {
                file.Write(outPdfBuffer, 0, outPdfBuffer.Length);
                file.Close();
            }

            return true;
        }
        catch (Exception e)
        {
            ErrorLog.createLog(e.Message);
            throw;
        }
    }

    /// <summary>
    /// To allow download certificate
    /// </summary>
    /// <param name="sFilePath">Pass Certificate File Path</param>
    /// <param name="fileName">Pass Certificate File Name</param>
    public void DownloadCertificate(string sFilePath, string fileName)
    {
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.ClearContent();
        response.Clear();
        response.ContentType = "application/pdf";
        response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ";");
        response.TransmitFile(sFilePath);
        response.Flush();
        response.End();
        response.Close();
    }

    /// <summary>
    /// To set pdf page orientation
    /// </summary>
    /// <returns>PdfPageOrientation</returns>
    public PdfPageOrientation SelectedPdfPageOrientation()
    {
        return PdfPageOrientation.Landscape;
    }

    /// <summary>
    /// To save error message 
    /// </summary>
    public void showAlertMessage()
    {
        Page page = HttpContext.Current.Handler as Page;
        ClientScriptManager cs = page.ClientScript;
        ScriptManager.RegisterStartupScript(page, page.GetType(), "WithUpdatePanel", "javascript:  ShowPensivoMessage('" + Resources.Resource.lblUnableToDownloadPleaseContactAdministrator + "')", true);
    }

    /// <summary>
    /// To get evo pdf license
    /// </summary>
    public string EVOlicense
    {
        get
        {
            //return BusinessUtility.GetString(ConfigurationManager.AppSettings["EVOlicense"]);
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("EVOlicense"));
        }
    }

    /// <summary>
    /// To get certificate path file to be saved
    /// </summary>
    public string CertificatePath
    {
        get
        {
            //return BusinessUtility.GetString(ConfigurationManager.AppSettings["CertificateDiractory"]);
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("CertificateDiractory"));
        }
    }

}