﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Data;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.Pensivo.BusinessLogic;
using System.Data;

/// <summary>
/// To define functionality to access activity to user
/// </summary>
public static class AuthorizedToViewPage
{
    /// <summary>
    /// To check if user is authorized to access activity or not 
    /// </summary>
    /// <param name="empID">Pass Employee/User ID</param>
    /// <param name="actionID">Pass Action ID</param>
    public static void RedirectNotAuthorized(int empID, int actionID)
    {
        Role objhasRole = new Role();
        Employee objEmpNew = new Employee();
        if ((objhasRole.GetUserHasFunctionality(empID, actionID, BusinessUtility.GetString(objEmpNew.GetEmpRefCodeValue(CurrentEmployee.EmpID, EmpSearchBy.Store))) == false) && (CurrentEmployee.EmpType != EmpType.SuperAdmin))
        {
            HttpContext.Current.Response.Redirect("public.aspx");
        }
    }



    public static bool GetAuthorizedToView(int empID, int actionID)
    {
        Role objhasRole = new Role();
        //if ((objhasRole.GetUserHasFunctionality(empID, actionID) == false) && (CurrentEmployee.EmpType != EmpType.SuperAdmin))
        //{
        //    HttpContext.Current.Response.Redirect("public.aspx");
        //}

        Employee objEmpNew = new Employee();
        return objhasRole.GetUserHasFunctionality(empID, actionID,  BusinessUtility.GetString(objEmpNew.GetEmpRefCodeValue(CurrentEmployee.EmpID, EmpSearchBy.Store)));
    }
}