﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Text;
using System.Xml;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.IO;
using iTECH.Pensivo.BusinessLogic;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;

/// <summary>
/// To define functionality regarding excel
/// </summary>
public class ExcelHelper
{
    /// <summary>
    /// To read data from excel file
    /// </summary>
    /// <param name="filePath">Pass Excel File Path</param>
    /// <returns></returns>
    public static DataTable exceldata(string filePath)
    {
        DataTable dtexcel = new DataTable();
        bool hasHeaders = false;
        string HDR = hasHeaders ? "Yes" : "No";
        string strConn;
        if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
            strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
        else
            strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
        OleDbConnection conn = new OleDbConnection(strConn);
        conn.Open();
        DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

        DataRow schemaRow = schemaTable.Rows[0];
        string sheet = schemaRow["TABLE_NAME"].ToString();
        if (!sheet.EndsWith("_"))
        {
            string query = "SELECT  * FROM [" + sheet + "]";
            OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
            daexcel.Fill(dtexcel);
        }

        conn.Close();
        return dtexcel;
    }


    public static void ExportToExcel(System.Data.DataTable Tbl, string ExcelFilePath)
    {
        try
        {
            DataSet ds = new DataSet();
            if (ds.Tables.Count > 0)
            {
                foreach (DataTable dt in ds.Tables)
                {
                    ds.Tables.Remove(dt);
                }
            }
            ds.Tables.Add(Tbl.Copy());
            DataSetsToExcel(ds, ExcelFilePath);

            return;
        }
        catch (Exception ex)
        {
            throw new Exception("ExportToExcel: \n" + ex.Message);
        }
    }

    public static void DataSetsToExcel(DataSet dataSet, string filepath)
    {
        try
        {
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + ";Extended Properties=Excel 12.0 Xml;";
            string tablename = "";
            DataTable dt = new DataTable();
            foreach (System.Data.DataTable dataTable in dataSet.Tables)
            {
                dt = dataTable;
                tablename = dataTable.TableName;
                using (OleDbConnection con = new OleDbConnection(connString))
                {
                    con.Open();
                    StringBuilder strSQL = new StringBuilder();
                    strSQL.Append("CREATE TABLE ").Append("[" + tablename + "]");
                    strSQL.Append("(");
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        strSQL.Append("[" + dt.Columns[i].ColumnName + "] text,");
                    }
                    strSQL = strSQL.Remove(strSQL.Length - 1, 1);
                    strSQL.Append(")");

                    OleDbCommand cmd = new OleDbCommand(strSQL.ToString(), con);
                    cmd.ExecuteNonQuery();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //strSQL.Clear();
                        strSQL = new StringBuilder();
                        StringBuilder strfield = new StringBuilder();
                        StringBuilder strvalue = new StringBuilder();
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            strfield.Append("[" + dt.Columns[j].ColumnName + "]");
                            strvalue.Append("'" + dt.Rows[i][j].ToString().Replace("'", "''") + "'");
                            if (j != dt.Columns.Count - 1)
                            {
                                strfield.Append(",");
                                strvalue.Append(",");
                            }
                            else
                            {
                            }
                        }
                        if (strvalue.ToString().Contains("<br/>"))
                        {
                            strvalue = strvalue.Replace("<br/>", Environment.NewLine);
                        }
                        cmd.CommandText = strSQL.Append(" insert into [" + tablename + "]( ")
                            .Append(strfield.ToString())
                            .Append(") values (").Append(strvalue).Append(")").ToString();
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void ConvertCSVToXLSX(string csvFilePath, string fileName)
    {
        try
        {
            DataTable dtexcel = new DataTable();
            string connectionString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + csvFilePath + ";";
            OdbcConnection conn = new OdbcConnection(connectionString);
            conn.Open();
            string query = "SELECT  * FROM [" + fileName + "]";
            OdbcDataAdapter daexcel = new OdbcDataAdapter(query, conn);
            daexcel.Fill(dtexcel);
            string sFileNme = DateTime.Now.ToString("yyyymmddhhmmss");
            //ExcelHelper.Instance.ExportToExcel(dtexcel, HttpContext.Current.Request.PhysicalApplicationPath + "Upload\\CSVFiles\\Customers" + sFileNme + ".xlsx");
            //HttpContext.Current.Response.Redirect("~/Upload/CSVFiles/Customers" + sFileNme + ".xlsx");
        }
        catch
        {
        }
    }

    public static void ExportToCSV(System.Data.DataTable Tbl, string ExcelFilePath)
    {
        try
        {
            DataSetsToCSV(Tbl, ExcelFilePath);
            return;
        }
        catch (Exception ex)
        {
            throw new Exception("ExportToExcel: \n" + ex.Message);
        }
    }


    public static void DataSetsToCSV(DataTable dtDataTable, string filepath)
    {

        StreamWriter sw = new StreamWriter(filepath, false);

        for (int i = 0; i < dtDataTable.Columns.Count; i++)
        {
            sw.Write(dtDataTable.Columns[i]);
            if (i < dtDataTable.Columns.Count - 1)
            {
                sw.Write(",");
            }
        }

        sw.Write(sw.NewLine);
        foreach (DataRow dr in dtDataTable.Rows)
        {
            for (int i = 0; i < dtDataTable.Columns.Count; i++)
            {
                if (!Convert.IsDBNull(dr[i]))
                {
                    string value = dr[i].ToString();
                    if (value.Contains(','))
                    {
                        value = String.Format("\"{0}\"", value);
                        sw.Write(value);
                    }
                    else
                    {
                        sw.Write(dr[i].ToString());
                    }
                }
                if (i < dtDataTable.Columns.Count - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
        }
        sw.Close();


    }


    public static void DataSetsToExl(DataTable dtDataTable, string filepath, string fileName, List<ReportFilter> reportFilterList)
    {
        using (ExcelPackage excelPkg = new ExcelPackage())
        {
            ExcelWorksheet oSheet = CreateSheet(excelPkg, fileName);

            Event objEvent = new Event();
            Employee objEmp = new Employee();
            Role objRole = new Role();
            int rowIndex = 1;


            CreateFilterHeader(oSheet, ref rowIndex);
            CreateFilterBlankRow(oSheet, ref rowIndex);
            //CreateFilterSubHeader(oSheet, ref rowIndex, Resource.ResourceValue("lblReportWho", Globals.CurrentCultureName) + ":");

            //var resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.Who.ToLower()).ToList();
            //if (resultItem != null && resultItem.Count <= 0)
            //{
            //    CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblAllTrainingUser", Globals.CurrentCultureName));
            //}
            //else
            //{
            //    if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "".ToLower()) && (resultItem.Count == 1))
            //    {
            //        CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblAllTrainingUser", Globals.CurrentCultureName));
            //    }
            //    else
            //    {

            //        foreach (var filterItem in resultItem)
            //        {
            //            if (filterItem.ReportOption.ToLower() == ReportOption.Who.ToLower())
            //            {
            //                if (filterItem.SearchBy.ToLower() == EmpSearchBy.Name.ToLower())
            //                {
            //                    string[] sName = BusinessUtility.GetString(filterItem.SearchValue).Split(',');
            //                    int i = 0;
            //                    foreach (string sEmName in sName)
            //                    {

            //                        objEmp = new Employee();
            //                        objEmp.GetEmployeeDetail(BusinessUtility.GetInt(sEmName));

            //                        if (i == 0)
            //                        {
            //                            CreateFilterDataRow(oSheet, ref rowIndex, BusinessUtility.GetString(Resource.ResourceValue("lblSearchName", Globals.CurrentCultureName)) + ":", BusinessUtility.GetString(objEmp.EmpName));
            //                        }
            //                        else
            //                        {
            //                            CreateFilterDataRow(oSheet, ref rowIndex, "", BusinessUtility.GetString(objEmp.EmpName));
            //                        }
            //                        i += 1;
            //                    }

            //                }
            //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.EmpCode.ToLower())
            //                {
            //                    if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry))//|| (Utils.TrainingInst == (int)Institute.Feedback)
            //                    {
            //                        CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblEmpEmailID", Globals.CurrentCultureName) + ":", filterItem.SearchValue);
            //                    }
            //                    else
            //                    {
            //                        CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblSearchEmployeeCode", Globals.CurrentCultureName) + ":", filterItem.SearchValue);
            //                    }
            //                }
            //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.District.ToLower())
            //                {
            //                    CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblSearchDistrict", Globals.CurrentCultureName) + ":", filterItem.SearchValue);
            //                }
            //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.RegionTBS.ToLower())
            //                {
            //                    CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblSearchRegion", Globals.CurrentCultureName) + ":", filterItem.SearchValue);
            //                }
            //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.DivisionTBS.ToLower())
            //                {
            //                    CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblSearchDivision", Globals.CurrentCultureName) + ":", filterItem.SearchValue);
            //                }
            //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Type.ToLower())
            //                {
            //                    CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblEmpType", Globals.CurrentCultureName) + ":", filterItem.SearchValue);
            //                }
            //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.JobCode.ToLower())
            //                {
            //                    CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblSearchJobCode", Globals.CurrentCultureName) + ":", filterItem.SearchValue);
            //                }
            //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Division.ToLower())
            //                {
            //                    if (filterItem.SearchValue.ToLower() == Resource.ResourceValue("lblSubTileAll", Globals.CurrentCultureName).ToLower())
            //                    {
            //                        CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblSearchLocation", Globals.CurrentCultureName) + ":", Resource.ResourceValue("lblSubTileAll", Globals.CurrentCultureName));
            //                    }
            //                    else
            //                    {

            //                        objRole = new Role();
            //                        DataTable dt = new DataTable();

            //                        DataView dv = objRole.GetSysRef(filterItem.SearchBy, "", Globals.CurrentAppLanguageCode).DefaultView;
            //                        dv.RowFilter = "sysRefCodeValue ='" + filterItem.SearchValue + "'";
            //                        string sValue = BusinessUtility.GetString(dv[0]["tiles"]);
            //                        CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblSearchLocation", Globals.CurrentCultureName) + ":", sValue);
            //                    }
            //                }
            //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Store.ToLower())
            //                {
            //                    if (Utils.TrainingInst == (int)Institute.bdl)
            //                    {
            //                        CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblDeptID", Globals.CurrentCultureName) + ":", filterItem.SearchValue);
            //                    }
            //                    else
            //                    {
            //                        CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblSearchSite", Globals.CurrentCultureName) + ":", filterItem.SearchValue);
            //                    }
            //                }
            //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Site.ToLower())
            //                {
            //                    CreateFilterDataRow(oSheet, ref rowIndex, Resource.ResourceValue("lblSearchSite", Globals.CurrentCultureName) + ":", filterItem.SearchValue);
            //                }
            //            }
            //        }
            //    }
            //}



            //CreateFilterBlankRow(oSheet, ref rowIndex);
            //CreateFilterSubHeader(oSheet, ref rowIndex, Resource.ResourceValue("lblReportWhich", Globals.CurrentCultureName) + ":");
            //resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.Which.ToLower()).ToList();
            //if (resultItem != null && resultItem.Count <= 0)
            //{
            //    CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblAllTrainingEvents", Globals.CurrentCultureName));
            //}
            //else
            //{
            //    if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "".ToLower()) && (resultItem.Count == 1))
            //    {
            //        CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblAllTrainingEvents", Globals.CurrentCultureName));
            //    }
            //    else
            //    {
            //        foreach (var filterItem in resultItem)
            //        {
            //            string searchBy = "";
            //            if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByEventName.ToLower()))
            //            {
            //                searchBy = Resource.ResourceValue("lblTrainingByTitle", Globals.CurrentCultureName);
            //            }
            //            else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByList.ToLower()))
            //            {
            //                searchBy = Resource.ResourceValue("lblTrainingByList", Globals.CurrentCultureName);
            //            }
            //            else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByCategory.ToLower()))
            //            {
            //                searchBy = Resource.ResourceValue("lblTrainingByCategory", Globals.CurrentCultureName);
            //                objEvent = new Event();
            //                DataTable dt = objEvent.GetCourseTypeList(Globals.CurrentAppLanguageCode);
            //                DataView dv = dt.DefaultView;
            //                dv.RowFilter = "TypeValue='" + filterItem.SearchValue + "'";
            //                CreateFilterDataRow(oSheet, ref rowIndex, BusinessUtility.GetString(searchBy) + ":", BusinessUtility.GetString(dv[0][1]));
            //            }


            //            if ((filterItem.SearchBy.ToLower() != ReportFilterOption.ByCategory.ToLower()))
            //            {
            //                string[] sName = BusinessUtility.GetString(filterItem.SearchValue).Replace("<br/>", "").Split(',');
            //                int i = 0;
            //                foreach (string sEmName in sName)
            //                {
            //                    objEvent = new Event();
            //                    objEvent.GetEventDetail(BusinessUtility.GetInt(sEmName), Globals.CurrentAppLanguageCode);

            //                    if (i == 0)
            //                    {
            //                        CreateFilterDataRow(oSheet, ref rowIndex, BusinessUtility.GetString(searchBy) + ":", BusinessUtility.GetString(objEvent.EventName));
            //                    }
            //                    else
            //                    {
            //                        CreateFilterDataRow(oSheet, ref rowIndex, "", BusinessUtility.GetString(objEvent.EventName));
            //                    }
            //                    i += 1;
            //                }
            //            }
            //        }
            //    }
            //}
            //CreateFilterBlankRow(oSheet, ref rowIndex);
            //CreateFilterSubHeader(oSheet, ref rowIndex, Resource.ResourceValue("lblReportWhatProgress", Globals.CurrentCultureName) + ":");
            //resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.What.ToLower()).ToList();
            //if (resultItem != null && resultItem.Count <= 0)
            //{
            //    CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblAllTraininProgress", Globals.CurrentCultureName));
            //}
            //else
            //{
            //    if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "all".ToLower()) && (resultItem.Count == 1))
            //    {
            //        CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblAllTraininProgress", Globals.CurrentCultureName));
            //    }
            //    else
            //    {

            //        foreach (var filterItem in resultItem)
            //        {
            //            if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Once_Completed.ToLower()))
            //            {
            //                CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblTrainingCompleteOnce", Globals.CurrentCultureName));
            //            }
            //            else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Completed.ToLower()))
            //            {
            //                CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblTrainingNotCompletedOnce", Globals.CurrentCultureName));
            //            }
            //            else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Started.ToLower()))
            //            {
            //                CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblTrainingNotStartedAtOnce", Globals.CurrentCultureName));
            //            }
            //            else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.All_Course_Progress.ToLower()))
            //            {
            //                CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblAllCourseProgress", Globals.CurrentCultureName));
            //            }
            //            else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Only_Those_Not_Completed_Training.ToLower()))
            //            {
            //                CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblTrainingNotCompletedOnce", Globals.CurrentCultureName));
            //            }
            //        }
            //    }
            //}


            //CreateFilterBlankRow(oSheet, ref rowIndex);
            //CreateFilterSubHeader(oSheet, ref rowIndex, Resource.ResourceValue("lblReportWhen", Globals.CurrentCultureName) + ":");
            //resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.When.ToLower()).ToList();
            //if (resultItem != null && resultItem.Count <= 1)
            //{
            //    CreateFilterDataRow(oSheet, ref rowIndex, "", Resource.ResourceValue("lblReportTrainingYearToDate", Globals.CurrentCultureName));
            //}


            rowIndex += 2;
            int iRowHeader = rowIndex;

            CreateHeader(oSheet, ref rowIndex, dtDataTable);


            //string sAplhabate = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //string cellRef = "A" + rowIndex + ":" + sAplhabate[dtDataTable.Columns.Count-1] + "" + rowIndex + "";
            //oSheet.Cells[cellRef].AutoFilter = true;

            CreateData(oSheet, ref rowIndex, dtDataTable);
            oSheet.Cells["A1"].AutoFitColumns(70);
            


            Byte[] content = excelPkg.GetAsByteArray();
            File.WriteAllBytes(filepath, content);

        }

    }

    private static ExcelWorksheet CreateSheet(ExcelPackage excelPkg, string sheetName)
    {
        ExcelWorksheet oSheet = excelPkg.Workbook.Worksheets.Add(sheetName);
        return oSheet;
    }



    private static void CreateHeader(ExcelWorksheet oSheet, ref int rowIndex, DataTable dt)
    {
        int colIndex = 1;
        foreach (DataColumn dc in dt.Columns)
        {
            var cell = oSheet.Cells[rowIndex, colIndex];
            cell.AutoFilter = true;
            cell.Value = dc.ColumnName;
            cell.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
            cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            if (colIndex > 1)
            {
                cell.Style.WrapText = true;
                cell.Style.ShrinkToFit = false;
                cell.AutoFitColumns(15);
            }
            
            colIndex++;
        }
    }


    private static void CreateData(ExcelWorksheet oSheet, ref int rowIndex, DataTable dt)
    {
        int colIndex = 0;
        foreach (DataRow dr in dt.Rows)
        {
            colIndex = 1;
            rowIndex++;

            foreach (DataColumn dc in dt.Columns)
            {
                var cell = oSheet.Cells[rowIndex, colIndex];

                cell.Value = Convert.ToString(dr[dc.ColumnName]).Replace("</br>", "");
                colIndex++;
            }
        }
    }


    private static void CreateFilterData(ExcelWorksheet oSheet, ref int rowIndex, DataTable dt)
    {
        int colIndex = 0;
        foreach (DataRow dr in dt.Rows)
        {
            colIndex = 1;
            rowIndex++;

            foreach (DataColumn dc in dt.Columns)
            {
                var cell = oSheet.Cells[rowIndex, colIndex];

                cell.Value = Convert.ToString(dr[dc.ColumnName]);
                colIndex++;



            }
        }
    }

    private static void CreateFilterHeader(ExcelWorksheet oSheet, ref int rowIndex)
    {
        var cell = oSheet.Cells[rowIndex, 1];
        cell.Value = Convert.ToString(Resource.ResourceValue("lblReportDetails", Globals.CurrentCultureName ));//"Report Details"
        var font = cell.Style.Font;
        font.Bold = true;
        font.Size = 14;
        font.Name = "Calibri";

        cell = oSheet.Cells[rowIndex, 2];
        cell.Value = Convert.ToString("");
        rowIndex += 1;
    }

    private static void CreateFilterBlankRow(ExcelWorksheet oSheet, ref int rowIndex)
    {
        var cell = oSheet.Cells[rowIndex, 1];
        cell.Value = Convert.ToString("");
        cell = oSheet.Cells[rowIndex, 2];
        cell.Value = Convert.ToString("");
        rowIndex += 1;
    }

    private static void CreateFilterEndRow(ExcelWorksheet oSheet, ref int rowIndex)
    {
        var cell = oSheet.Cells[rowIndex, 1];
        cell.Value = Convert.ToString("");
        cell = oSheet.Cells[rowIndex, 2];
        cell.Value = Convert.ToString("");
        rowIndex += 1;
    }

    private static void CreateFilterSubHeader(ExcelWorksheet oSheet, ref int rowIndex, string label)
    {
        var cell = oSheet.Cells[rowIndex, 1];
        cell.Value = Convert.ToString(label);
        var font = cell.Style.Font;
        font.Bold = true;
        font.Size = 12;
        font.Name = "Calibri";

        cell = oSheet.Cells[rowIndex, 2];
        cell.Value = Convert.ToString("");
        rowIndex += 1;
    }

    private static void CreateFilterDataRow(ExcelWorksheet oSheet, ref int rowIndex, string label, string labelValue)
    {
        var cell = oSheet.Cells[rowIndex, 1];
        cell.Value = Convert.ToString(label);
        var font = cell.Style.Font;
        font.Size = 11;
        font.Name = "Calibri";
        font.Italic = true;

        cell = oSheet.Cells[rowIndex, 2];
        cell.Value = Convert.ToString(labelValue);
        font = cell.Style.Font;
        font.Size = 11;
        font.Name = "Calibri";
        rowIndex += 1;
    }
}

