﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class AssignTrainingEventByEmpCode : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Assign_Training);
        if (!IsPostBack)
        {
            //lblTitle.Text = Resources.Resource.lblAssignTrainingSearchByEmpCode;  
            //txtSearchName.Attributes.Add("placeholder", Resources.Resource.lblAssignTraininEnventEmpNumber);
            //rfSearchName.Text = Resources.Resource.reqEmpNumber;
            //lblName.Text = Resources.Resource.lblAssignTraininEnventEmpNumber;

            if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
            {
                lblTitle.Text = Resources.Resource.lblSearchByEmpEmailID;
                txtSearchName.Attributes.Add("placeholder", Resources.Resource.lblEmpEmailID);
                rfSearchName.Text = Resources.Resource.reqEmpEmailID;
                lblName.Text = Resources.Resource.lblEmpEmailID;
                htmLblName.InnerHtml = Resources.Resource.lblEmpEmailID;
            }
            else if (Utils.TrainingInst == (int)Institute.EDE2)
            {
                lblTitle.Text = Resources.Resource.lblEDE2AssignTrainingSearchByEmpCode;
                txtSearchName.Attributes.Add("placeholder", Resources.Resource.lblEDE2UserName);
                rfSearchName.Text = Resources.Resource.reqEDE2EmpCode;
                lblName.Text = Resources.Resource.lblEDE2UserName;
                htmLblName.InnerHtml = Resources.Resource.lblEDE2UserName;
            }
            else if (Utils.TrainingInst == (int)Institute.bdl)
            {
                lblTitle.Text = Resources.Resource.lblBDLAssignTrainingSearchByEmpCode;
                txtSearchName.Attributes.Add("placeholder", Resources.Resource.lblBDLUserName);
                rfSearchName.Text = Resources.Resource.reqBDLEmpCode;
                lblName.Text = Resources.Resource.lblBDLUserName;
                htmLblName.InnerHtml = Resources.Resource.lblBDLUserName;
            }
            else
            {
                lblTitle.Text = Resources.Resource.lblAssignTrainingSearchByEmpCode;
                txtSearchName.Attributes.Add("placeholder", Resources.Resource.lblAssignTraininEnventEmpNumber);
                rfSearchName.Text = Resources.Resource.reqEmpNumber;
                lblName.Text = Resources.Resource.lblAssignTraininEnventEmpNumber;
                htmLblName.InnerHtml = Resources.Resource.lblAssignTraininEnventEmpNumber;
            }



            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblEmpoyeeID, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, BusinessUtility.GetString(Request.UrlReferrer));
        }
    }

    /// <summary>
    /// To Hold Search By Employee Code and Move to Next Page
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnSearchByName_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("AssignTrainingEventEmployee.aspx?trainingeventid=" + this.TrainingEventID+"&empcode="+ txtSearchName.Text);
    }

    /// <summary>
    /// To Get Training Event ID
    /// </summary>
    public string TrainingEventID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["trainingeventid"]);
        }
    }

}