﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeeReport.aspx.cs" Inherits="EmployeeReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <%=htmlText %>
        </div>
    </section>
    <asp:HiddenField ID="hdnSiteNo" runat="server" />
    <asp:Button ID="btnShowReport" runat="server" OnClick="btnShowReport_Click" />
    <asp:Button ID="btnShowAllReport" runat="server" OnClick="btnShowAllReport_Click" />
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Trigger Show Report
        function ShowReport(siteNo) {
            $("#<%=hdnSiteNo.ClientID %>").val(siteNo);
            $("#<%=btnShowReport.ClientID %>").trigger("click");
        }

        function ShowAllReport() {
            $("#<%=btnShowAllReport.ClientID %>").trigger("click");
        }
    </script>
</asp:Content>

