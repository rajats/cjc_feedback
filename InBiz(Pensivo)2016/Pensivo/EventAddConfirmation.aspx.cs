﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class EventAddConfirmation : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblConfirmation, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            string roleName = "";
            string eventName = "";
            if (this.RoleID > 0)
            {
                objRole = new Role();
                roleName = objRole.GetRoleName(this.RoleID);
            }

            if (this.EventID > 0)
            {
                Event objEvent = new Event();
                objEvent.GetEventDetail(this.EventID, Globals.CurrentAppLanguageCode);
                eventName = objEvent.EventName;
            }

            ltrTitle.Text = Resources.Resource.lblAddedEventToTheSystemRole + " '" + roleName + "' : " + eventName;
            hdnEventID.Value = BusinessUtility.GetString(this.EventID);
            hdnRoleID.Value = BusinessUtility.GetString(this.RoleID);
            hrefNo.HRef = "RoleAddEvent.aspx?roleID=" + this.RoleID;

            if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addEvent")
            {
                try
                {
                    int eventID = BusinessUtility.GetInt(Request.Form["EventID"]);
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);

                    if (eventID > 0 && roleID > 0)
                    {
                        List<Role> lRoleEvent = new List<Role>();
                        lRoleEvent.Add(new Role { EventID = eventID });
                        objRole = new Role();
                        if (objRole.RoleAddEvent(roleID, lRoleEvent) == true)
                        {
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }

                }
                catch
                {
                    Response.Write("error");
                }

                Response.End();
                Response.SuppressContent = true;
            }
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Event ID
    /// </summary>
    public int EventID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["eventID"]);
        }
    }
}