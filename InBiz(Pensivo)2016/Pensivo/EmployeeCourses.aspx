﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeeCourses.aspx.cs" Inherits="EmployeeViewCourses" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .tbl_separator {
            border-right: 1px solid #e4e4e4;
            vertical-align: top;
        }

        .tbl_hdr {
            background: black;
        }

            .tbl_hdr div {
                color: white;
                text-align: center;
                vertical-align: middle;
                width: 200px;
                height: 50px;
                display: table-cell;
                word-break: break-word;
                padding: 0 10px;
            }

        .tbl_ctnt div {
            width: 200px;
            height: 50px;
            display: table-cell;
            word-break: break-word;
            padding: 5px 10px;
            text-align: center;
        }
    </style>
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper ">
            <div class="column span-8" style="width: 100%;" id="dv_hdr">
                <%--<h6 class="plms-accordion-title h5" ><%#Eval("CourseTitle") %></h6>--%>
                <%--<h6 class="plms-accordion-title h5" id="hCourseTitle" runat="server"><%#Eval("CourseTitle") %></h6>--%>
                <div class="tbl_hdr">
                    <div class="tbl_separator" style="width: 210px;">
                        <%=Resources.Resource.lblRequestor%>
                    </div>
                    <div class="tbl_separator" style="width: 188px;">
                        <%=Resources.Resource.lblFeedbackName%>
                    </div>
                    <div class="tbl_separator" style="width: 188px;">
                        <%=Resources.Resource.lblEventDate%>
                    </div>
                    <div class="tbl_separator" style="width: 188px;">
                        <%=Resources.Resource.lblEventName%>
                    </div>
                    <div class="tbl_separator" style="width: 273px;">
                        <%=Resources.Resource.lblStartFeedback%>
                    </div>
                </div>
                <div style="clear: both;">
                </div>
            </div>
            <asp:ListView ID="lstEmployeeCourses" runat="server" OnItemCommand="lstEmployeeCourses_ItemCommand" OnItemDataBound="lstEmployeeCourses_ItemDataBound">
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer">
                        <tr runat="server" id="itemPlaceholder">
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table style="width: 100%">
                        <tr>
                            <td><%=GetNoRecordFoundMessage() %></td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <div class="plms-accordion disable-toggle">
                        <header class="plms-accordion-header">
                            <span class="openclose-icon closed collapse"></span>
                            <div class="column span-8" style="width: 79%;">
                                <%--<h6 class="plms-accordion-title h5" ><%#Eval("CourseTitle") %></h6>--%>
                                <%--<h6 class="plms-accordion-title h5" id="hCourseTitle" runat="server"><%#Eval("CourseTitle") %></h6>--%>
                                <div class="tbl_hdr" style="display: none;">
                                    <div class="tbl_separator">
                                        <%=Resources.Resource.lblInitiator %>
                                    </div>
                                    <div class="tbl_separator">
                                        <%=Resources.Resource.lblQuestionnaireName %>
                                    </div>
                                    <div class="tbl_separator">
                                        <%=Resources.Resource.lblEventDate%>
                                    </div>
                                    <div class="tbl_separator">
                                        <%=Resources.Resource.lblEventName%>
                                    </div>
                                </div>
                                <div style="clear: both;">
                                </div>
                                <div class="tbl_ctnt">
                                    <div class="tbl_separator">
                                        <h6 class="plms-accordion-title h5"><%#Eval("InitiatorName") %> </h6>
                                    </div>
                                    <div class="tbl_separator">
                                        <h6 class="plms-accordion-title h5"><%#Eval("CourseTitle") %> </h6>
                                    </div>
                                    <div class="tbl_separator">
                                        <h6 class="plms-accordion-title h5"><%#Eval("eventDateFormatted") %> </h6>
                                    </div>
                                    <div class="tbl_separator">
                                        <h6 class="plms-accordion-title h5"><%#Eval("eventName") %> </h6>
                                    </div>
                                </div>
                                <div class="plms-accordion-status-grp">
                                    <span class="plms-accordion-status" id="spnNewCourse" runat="server" visible='<%# Convert.ToString( Eval("isNew")) == "True" %>'>New</span>
                                    <span class="plms-accordion-status" id="spnRecentlyUpdated" runat="server" visible='<%# Convert.ToString( Eval("isUpdated")) == "True" %>'>Recently Updated</span>
                                </div>
                                <ul class="plms-accordion-metadata is-hidden">
                                    <li class="plms-accordion-metadata-item">
                                        <span class="key">Categories</span>
                                        <span class="value"><%# Eval("trn_catg") %></span>
                                    </li>
                                    <li class="plms-accordion-metadata-item">
                                        <span class="key">Type</span>
                                        <span class="value"><%# Eval("course_type") %></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="column span-4" style="width: 20%; padding-left: 30px; display: table-cell; vertical-align: middle; height: 100%;">
                                <asp:Literal ID="ltrEmpHeaderID" runat="server" Text='<%# Eval("empHeader_empHdrID") %>' Visible="false" />
                                <asp:Literal ID="ltrCourseID" runat="server" Text='<%# Eval("courseHdrID") %>' Visible="false" />
                                <asp:Literal ID="ltrCourseVersion" runat="server" Text='<%# Eval("courseVerNo") %>' Visible="false" />
                                <asp:Literal ID="ltrCourseExternalID" runat="server" Text='<%# Eval("courseExtID") %>' Visible="false" />
                                <asp:Literal ID="ltrCourseTestType" runat="server" Text='<%# Eval("TestType") %>' Visible="false" />
                                <asp:Literal ID="ltrCourseTestID" runat="server" Text='<%# Eval("TestExtID") %>' Visible="false" />
                                <asp:Literal ID="ltrEmpCourseRegID" runat="server" Text='<%# Eval("idRegistration") %>' Visible="false" />
                                <asp:Literal ID="ltrEmpTestCourseRegID" runat="server" Text='<%# Eval("idTestRegistration") %>' Visible="false" />
                                <asp:Literal ID="ltrTestAttempted" runat="server" Text='<%# Eval("attempted") %>' Visible="false" />
                                <asp:Literal ID="ltrScormType" runat="server" Text='<%# Eval("Scorm_type") %>' Visible="false" />
                                <asp:Literal ID="ltrStartDate" runat="server" Text='<%# Eval("startedDate") %>' Visible="false" />
                                <asp:Literal ID="ltrProgressStatus" runat="server" Text='<%# Eval("progressstatus") %>' Visible="false" />
                                <asp:Literal ID="ltrCourseTitle" runat="server" Text='<%# Eval("CourseTitle") %>' Visible="false" />
                                <asp:Literal ID="ltrRoleID" runat="server" Text='<%# Eval("RoleID") %>' Visible="false" />
                                <asp:Literal ID="ltrIsRepeated" runat="server" Text='<%# Eval("isRepeated") %>' Visible="false" />
                                <asp:Literal ID="ltrEmpCourseAssignedID" runat="server" Text='<%# Eval("empCourseAssignedID") %>' Visible="false" />

                                <asp:LinkButton ID="lnkbLaunch" runat="server" Width="150" class="btn fluid" Text="<%# GetLaunchText() %>" CommandName="Launch" CommandArgument='<%# Eval("empHeader_empHdrID")%>'></asp:LinkButton>
                                <div class="plms-tooltip-parent">
                                    <asp:LinkButton ID="lnkbLaunchTest" runat="server" class="btn fluid skin2" Visible="false" Style="margin-top: 5px;" Text="<%$ Resources:Resource, lblTakeTheTest %>" CommandName="LaunchTest" CommandArgument='<%# Eval("empHeader_empHdrID")%>'><%=Resources.Resource.lblTakeTheTest %></asp:LinkButton>
                                </div>
                                <div class="plms-tooltip-parent">
                                    <asp:LinkButton ID="lnbDownloadTest" runat="server" class="btn fluid skin2" Style="margin-top: 5px;" Text="<%$ Resources:Resource, lblDownloadTest %>" CommandName="DownloadTest" CommandArgument='<%# Eval("empHeader_empHdrID")%>' Visible="false"><%=Resources.Resource.lblDownloadTest %></asp:LinkButton>
                                    <asp:LinkButton ID="lnbProgressStatus" runat="server" class="btn fluid skin2" Style="margin-top: 5px;" Text="<%$ Resources:Resource, lblProgress %>" CommandName="ProgressStatus" CommandArgument='<%# Eval("empHeader_empHdrID")%>'><%=Resources.Resource.lblProgress %></asp:LinkButton>
                                    <asp:LinkButton ID="lnkReviewContent" runat="server" class="btn fluid skin2" Style="margin-top: 5px;" Text="<%$ Resources:Resource, lblReviewTheCourseContent %>" CommandName="ReviewCourseContent" CommandArgument='<%# Eval("empHeader_empHdrID")%>' Visible="false"><%=Resources.Resource.lblReviewTheCourseContent %></asp:LinkButton>
                                    <div class="plms-tooltip right-btm outside">
                                        <header class="plms-tooltip-header">
                                            <h6>Progress</h6>
                                        </header>
                                        <div class="plms-tooltip-body">
                                            <ul>
                                                <li><b class="key">
                                                    <asp:Literal ID="ltrStatusTitle" runat="server" Text="Course"></asp:Literal>
                                                </b><b class="value">
                                                    <asp:Literal ID="ltrCourseStatus" runat="server" Text='<%# Eval("progressstatus") %>' /></b></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>
                        <div class="plms-accordion-body">
                            <nav class="plms-accordion-nav">
                                <ul>
                                    <li id="liSummary" runat="server" class="tabs" row_num="<%# Container.DataItemIndex %>" onclick="ShowTab(this.id);" txt_ctnr="hdnSummary" txt_toshow="divShowContent"><a><%= Resources.Resource.lblSummary%></a></li>
                                    <li id="liPreview" runat="server" class="tabs" row_num="<%# Container.DataItemIndex %>" onclick="ShowTab(this.id);" txt_ctnr="hdnPreview" txt_toshow="divShowContent"><a><%= Resources.Resource.lblPreview%></a></li>
                                    <li id="liSampleTest" runat="server" class="tabs" row_num="<%# Container.DataItemIndex %>" onclick="ShowTab(this.id);" txt_ctnr="hdnSampleTest" txt_toshow="divShowContent"><a><%= Resources.Resource.lblSampleTest%></a></li>
                                    <li id="liArticle" runat="server" class="tabs" row_num="<%# Container.DataItemIndex %>" onclick="ShowTab(this.id);" txt_ctnr="hdnArticle" txt_toshow="divShowContent"><a><%= Resources.Resource.lblRelatedArtical%></a></li>
                                    <asp:HiddenField ID="hdnSummary" runat="server" Value='<%# Eval("trn_summary") %>' />
                                    <asp:HiddenField ID="hdnPreview" runat="server" Value='<%# Eval("trn_preview") %>' />
                                    <asp:HiddenField ID="hdnSampleTest" runat="server" Value='<%# Eval("trn_sampletest") %>' />
                                    <asp:HiddenField ID="hdnArticle" runat="server" Value='<%# Eval("trn_articles") %>' />
                                </ul>
                            </nav>
                            <div id="divShowContent" runat="server" class="plms-accordion-body-content">
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:ListView>

            <asp:HiddenField ID="hdnTestEventID" runat="server" />
            <asp:HiddenField ID="hdnEmpID" runat="server" />
            <asp:HiddenField ID="hdnCourseVerNo" runat="server" />
            <asp:HiddenField ID="hdnIsRepeated" runat="server" />
            <asp:HiddenField ID="hdnEmpCourseAssignedID" runat="server" />
            <asp:HiddenField ID="hdnPostURL" runat="server" />
            <asp:HiddenField ID="hdnpregID" runat="server" />
            <asp:HiddenField ID="hdnCertificateDownloadRegID" runat="server" />
            <asp:Button ID="btnLaunchTest" runat="server" Style="display: none;" OnClick="btnLaunchTest_Click" />
            <asp:Button ID="btnDownloadCertificate" runat="server" Style="display: none;" OnClick="btnDownloadCertificate_Click" />
            <asp:Button ID="btnDownloadCertificateFile" runat="server" Style="display: none;" OnClick="btnDownloadCertificateFile_Click" />
            <asp:HiddenField ID="hdnAttemptRegID" runat="server" />
            <asp:Button ID="btnAttemptRequest" runat="server" Style="display: none;" OnClick="btnAttemptRequest_Click" />
            <asp:Button ID="btnAttemptRequestAlert" runat="server" Style="display: none;" OnClick="btnAttemptRequestAlert_Click" />
            <asp:HiddenField ID="hdnAttemptCourseRegID" runat="server" />
            <asp:HiddenField ID="hdnAttemptRequestID" runat="server" />
            <asp:HiddenField ID="hdnMessageAlertID" runat="server" />

            <asp:Button ID="btnChangeAlertMailID" runat="server" Style="display: none;" OnClick="btnChangeAlertMailID_Click" />
            <asp:Button ID="btnLogOut" runat="server" Text="<%$ Resources:Resource, lblLogOut %>" OnClick="btnLogOut_OnClick" class="btn" Style="display: none;" />
            <%--Define JavaScript Function--%>
            <script type="text/javascript">
                // To Show/Hide Current Tab
                function ShowTab(e) {
                    var vclass = $("#" + e).attr("row_num");
                    $('.row' + vclass).each(function () {
                        $(this).removeClass("cur");
                    });
                    $("#" + e).addClass("cur");
                    var vTxt_ToShow = $("#" + e).attr("txt_toshow");
                    var vRow_Num = $("#" + e).attr("row_num");
                    var vTxt_Ctnr = $("#" + e).attr("txt_ctnr");
                    var valToShow = $("#ContentPlaceHolder1_lstEmployeeCourses_" + vTxt_Ctnr + "_" + vRow_Num).val();
                    $("#ContentPlaceHolder1_lstEmployeeCourses_" + vTxt_ToShow + "_" + vRow_Num).html(valToShow);
                }

                // To Confirmation to launch Test Course
                function launchCourse() {
                    var title = "<%=Resources.Resource.lblTitleCongratulations%>";
                    var messageText = "<%=Resources.Resource.lblCompleteCourse%>";
                    okButtonText = "<%=Resources.Resource.lblOk%>";
                    LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
                    okButtonRedirectlink = "ConfirmLaunchCourseOk()";
                    LaterCnclButtonRedirectLink = "ConfirmLaunchCourseCancel();";
                    PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
                }

                // To Trigger Launch Test Course OK Button
                function ConfirmLaunchCourseOk() {
                    $("#<%=btnLaunchTest.ClientID%>").trigger("click");
                }

                // To Trigger Launch Course Cancel Button
                function ConfirmLaunchCourseCancel() {
                    $("#dvPensivoCnfrmDialog").removeClass("active");
                    if ($("#<%=btnLaunchTest.ClientID%>") != '') {
                        window.location.href = $("#<%=hdnPostURL.ClientID%>").val();
                    }
                }

                // To Confirmation To Download Certificate
                function downloadCertificate() {
                    var title = "<%=Resources.Resource.lblTitleCongratulations%>";
                    var messageText = "<%=Resources.Resource.lblConfirmDownloadCertificate%>";
                    okButtonText = "<%=Resources.Resource.lblOk%>";
                    LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
                    okButtonRedirectlink = "ConfirmdownloadCertificateOk()";
                    LaterCnclButtonRedirectLink = "ConfirmdownloadCertificateCancel();";
                    PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
                }

                // To Trigger Download Certificate OK Button
                function ConfirmdownloadCertificateOk() {
                    $("#<%=btnDownloadCertificate.ClientID%>").trigger("click");
                }

                // To Trigger Download Certificate Cancel Button
                function ConfirmdownloadCertificateCancel() {
                    $("#dvPensivoCnfrmDialog").removeClass("active");
                    if ($("#<%=btnLaunchTest.ClientID%>") != '') {
                        window.location.href = $("#<%=hdnPostURL.ClientID%>").val();
                    }
                }

                // To Show Course Progress Status
                function ShowProgressStatus() {
                    $("#dvProgressStatus").addClass("active");
                    var bodyNew = $("body");
                    bodyNew.addClass("lockdown");
                    var htmlNew = $("html");
                    htmlNew.addClass("lockdown");
                }

                // To Show Course Test Attempt Dialog Box
                function ShowAttemptRequestDialog(regid, courseRegID) {
                    var title = "Confirmation!";
                    var messageText = "<%=Resources.Resource.lblMsgToAddAttempt%>";
                    okButtonText = "<%=Resources.Resource.lblAlertMe%>";
                    LaterCnclButtonText = "<%=Resources.Resource.lblNoThanks%>";
                    DoNotAlertText = "<%=Resources.Resource.lblDonnotAlertMe%>";
                    okButtonRedirectlink = "AttemptAlertMe('" + regid + "', '" + courseRegID + "')";
                    LaterCnclButtonRedirectLink = "AttemptDonNotAlertMe('" + regid + "', '" + courseRegID + "')";
                    DoNotAlertLink = "AttemptDonNotAlertMe('" + regid + "', '" + courseRegID + "')";
                    PensivoAttemptsConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink, DoNotAlertText, DoNotAlertLink);
                }

                // To Show Test Attempts Complete Confirmation
                function ShowTestAttemptsFailure(regid, courseRegID) {
                    var title = "<%=Resources.Resource.lblSorry%>";
                    var messageText = "<%=Resources.Resource.lblMsgCompleteAttempt%>";
                    okButtonText = "<%=Resources.Resource.lblOk%>";
                    LaterCnclButtonText = "";
                    okButtonRedirectlink = "CloseTestAttemptRequest('" + regid + "', '" + courseRegID + "');";
                    LaterCnclButtonRedirectLink = "CloseTestAttemptRequest('" + regid + "', '" + courseRegID + "');";
                    PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
                }

                // To Show Test Attempt Alert Thanks Dialog
                function ShowAlertThanksDialog(regid, courseRegID, attemptRequestID, messageAlertID) {
                    var title = "<%=Resources.Resource.lblThanks%>";
                    var messageText = "<%=Resources.Resource.lblThankAttemptsAlertMessage%>".replace("#EMAILID#", $("#txtAttemptsEmail").val());
                    okButtonText = "<%=Resources.Resource.lblChangeEmail%>";
                    LaterCnclButtonText = "<%=Resources.Resource.lblOk%>";
                    okButtonRedirectlink = "ChangeMailDialog('" + regid + "', '" + courseRegID + "','" + attemptRequestID + "','" + messageAlertID + "' );";
                    LaterCnclButtonRedirectLink = "CloseAlertThanksDialog();";
                    PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
                }

                // To Close Alert Thanks Dialog
                function CloseAlertThanksDialog() {
                    HideConfirmationDialog();
                    window.location.href = 'EmployeeCourses.aspx?CourseStatus=1';
                }

                // To Show Test Attempt Alert Change Mail Dialog
                function ChangeMailDialog(regid, courseRegID, attemptRequestID, messageAlertID) {
                    HideConfirmationDialog();
                    $("#txtAttemptsEmail").val("");
                    var title = "<%=Resources.Resource.lblMessageConfirmation%>";
                    var messageText = "<%=Resources.Resource.lblMsgToAddAttempt%>";
                    okButtonText = "<%=Resources.Resource.lblAlertMe%>";
                    LaterCnclButtonText = "<%=Resources.Resource.lblNoThanks%>";
                    DoNotAlertText = "<%=Resources.Resource.lblDonnotAlertMe%>";
                    okButtonRedirectlink = "ChangeAlertMailID('" + attemptRequestID + "', '" + messageAlertID + "')";
                    LaterCnclButtonRedirectLink = "AttemptDonNotAlertMe('" + regid + "', '" + courseRegID + "')";
                    DoNotAlertLink = "AttemptDonNotAlertMe('" + regid + "', '" + courseRegID + "')";
                    PensivoAttemptsConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink, DoNotAlertText, DoNotAlertLink);
                }

                // To Trigger Change Test Attemp Alert Email ID
                function ChangeAlertMailID(attemptRequestID, messageAlertID) {
                    HideConfirmationDialog();
                    $("#<%=hdnAttemptRequestID.ClientID%>").val(attemptRequestID);
                    $("#<%=hdnMessageAlertID.ClientID%>").val(messageAlertID);
                    $("#<%=btnChangeAlertMailID.ClientID%>").trigger("click");
                }

                // To Show Attempt Thanks Alert
                function ShowNoThanksAlert() {
                    var title = "<%=Resources.Resource.lblThanks%>";
                    var messageText = "<%=Resources.Resource.lblNoThankAttemptsAlertMessage%>";
                    okButtonText = "<%=Resources.Resource.lblOk%>";
                    LaterCnclButtonText = "";
                    okButtonRedirectlink = " ShowNoThanksAlertClose();";
                    LaterCnclButtonRedirectLink = " ShowNoThanksAlertClose();;";
                    PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
                }

                // To Close Thanks Alert
                function ShowNoThanksAlertClose() {
                    HideConfirmationDialog();
                    window.location.href = 'EmployeeCourses.aspx?CourseStatus=1';
                }

                // To Show Attempt Dialog
                function ShowNoAttemptsDialog() {
                    var title = "<%=Resources.Resource.lblOpps%>";
                    var messageText = "<%=Resources.Resource.lblCompleteAttemts%>";
                    okButtonText = "<%=Resources.Resource.lblOk%>";
                    LaterCnclButtonText = "";
                    okButtonRedirectlink = " ShowNoThanksAlertClose();";
                    LaterCnclButtonRedirectLink = " ShowNoThanksAlertClose();;";
                    PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
                    return false;
                }

                // To Close Test Attempt Request
                function CloseTestAttemptRequest(regid, courseRegID) {
                    HideConfirmationDialog();
                    ShowAttemptRequestDialog(regid, courseRegID);
                }

                // To Trigger Attempt Alert Me
                function AttemptAlertMe(regid, courseRegID) {
                    if ($("#txtAttemptsEmail").val() == '') {
                        $("#spError").html("<%=Resources.Resource.reqMail%>");
                        $("#txtAttemptsEmail").focus();
                        return false;
                    } else if (checkemail($("#txtAttemptsEmail").val()) == false) {
                        $("#spError").html("<%=Resources.Resource.errMsgInvalidMailFormat%>");
                            $("#txtAttemptsEmail").focus();
                            return false;
                        }

                    HideAttemptConfirmationDialog();
                    $("#<%=hdnAttemptRegID.ClientID%>").val(regid);
                    $("#<%=hdnAttemptCourseRegID.ClientID%>").val(courseRegID);
                    $("#<%=btnAttemptRequestAlert.ClientID%>").trigger("click");
                }

                // To Hide Attempt Email Error
                function HideError() {
                    $("#spError").html("");
                }

                // To Trigger Don't Alert Me
                function AttemptDonNotAlertMe(regid, courseRegID) {
                    $("#<%=hdnAttemptRegID.ClientID%>").val(regid);
                    $("#<%=hdnAttemptCourseRegID.ClientID%>").val(courseRegID);
                    $("#<%=btnAttemptRequest.ClientID%>").trigger("click");
                }

                // To Close Attempt Request
                function CloseAttemptRequest() {
                    HideAttemptConfirmationDialog();
                    window.location.href = 'EmployeeCourses.aspx?CourseStatus=1';
                }

                // To Validate Email
                function checkemail(sEmail) {
                    var str = sEmail;
                    var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
                    if (filter.test(str))
                        return true;
                    else {
                        return false;
                    }
                }

                // To Define Document Ready Event
                $(document).ready(function () {
                    $("#itemPlaceholderContainer").css("width", "100%");
                    if ("<% =Session["CertificateDownloadRegID"] %>" > 0) {
                        $("#<%=btnDownloadCertificateFile.ClientID%>").trigger("click");
                    }
                });



                // To Show Confirmation Dialog to add user  <%=Resources.Resource.lblfeedbackCompletedConfirmationTitle%>;
                function showFeedbackCompletedConfirmationMessage(title, initiatorName, message) {
                    var title = title;
                    var messageText = message.replace("#INITIATORFIRSTNAME#", initiatorName);
                    okButtonText = "OK";
                    LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
                    okButtonRedirectlink = "ConfirmRoleAddUserOk()";
                    LaterCnclButtonRedirectLink = "ConfirmAddRoleUserCancel();";
                    PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
                }

                // To Trigger Role Add User Ok Button
                function ConfirmRoleAddUserOk() {

                    window.location.href = "EmployeeCourses.aspx?CourseStatus=3";
                }

                // To Trigger Role Add User Cancel Button
                function ConfirmAddRoleUserCancel() {
                    //$("#dvPensivoCnfrmDialog").removeClass("active");
                    // To Trigger Logout OK button
                    $("#<%=btnLogOut.ClientID%>").trigger("click");
                }

            </script>
        </div>
    </section>
</asp:Content>

