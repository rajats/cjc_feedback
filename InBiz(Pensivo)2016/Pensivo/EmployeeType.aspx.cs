﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;

public partial class EmployeeType : BasePage
{
    /// <summary>
    /// To Save HTML Content
    /// </summary>
    protected string htmlText = "";

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        htmlText = loadEmpSearchValue();
        hdnSearchBy.Value = this.SearchByNew;
        hdnUserID.Value = BusinessUtility.GetString(CurrentEmployee.EmpID);
        hdnQuery.Value = BusinessUtility.GetString(Request.Url.Query);

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addRef")
        {
            try
            {
                int userID = BusinessUtility.GetInt(Request.Form["UserID"]);
                string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                if (userID > 0 && searchBy != "" && searchValue != "")
                {
                    Employee objEmp = new Employee();
                    if (objEmp.InsertEmployeeSysRef(userID, searchBy, searchValue) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
    }

    /// <summary>
    /// To Get Employee Type HTML Tiles
    /// </summary>
    /// <returns>String</returns>
    private string loadEmpSearchValue()
    {
        string sbHtml = "";
        Role objRole = new Role();
        DataTable dt = new DataTable();

        DataView dv = objRole.GetSysRef(this.SearchByNew, "", Globals.CurrentAppLanguageCode).DefaultView;
        dv.RowFilter = "tiles<>''";
        dv.Sort = "tiles desc";
        dt = dv.ToTable();
        sbHtml += " <div class='boxset' id='dvAdminDashBoard'> ";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            for (int j = 0; j <= 2; j++)
            {
                if (i < dt.Rows.Count)
                {
                    string sText = BusinessUtility.GetString(dt.Rows[i]["tiles"]);
                    string sValue = BusinessUtility.GetString(dt.Rows[i]["sysRefCodeValue"]);
                    string sSubTitle = BusinessUtility.GetString(dt.Rows[i]["subtitle"]);
                    if (sText != "")
                    {
                        sbHtml += " <div class='boxset-box'> ";
                        sbHtml += string.Format(" <a id='hrfSearchEmpName' runat='server' href='#' onclick='savelocation(\"{0}\",\"{1}\" )' class='boxset-box-wrapper'> ", sValue, sSubTitle);
                        sbHtml += " <div class='boxset-box-inner'> ";
                        sbHtml += " <h4 class='boxset-title'> " + sText + " </h4> ";
                        sbHtml += " <div class='boxset-body'> ";
                        sbHtml += " <p>" + sSubTitle + " </p> ";
                        sbHtml += " </div> ";
                        sbHtml += " </div> ";
                        sbHtml += " </a> ";
                        sbHtml += " </div> ";
                        if (j <= 1)
                        {
                            i += 1;
                        }
                    }
                }
            }
        }
        sbHtml += " </div> ";
        if (dt.Rows.Count <= 0)
        {
            sbHtml += "<table style='width: 100%'>";
            sbHtml += "<tr>";
            sbHtml += "<td>" + Resources.Resource.lblNoRecordAvailable + "</td>";
            sbHtml += "</tr>";
            sbHtml += "</table>";
        }
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// To Create Employee Detail in Pensvio
    /// </summary>
    /// <param name="sender">Pass Object Sender</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnSaveInformation_Click(object sender, EventArgs e)
    {
        ErrorLog.createLog("Hit Save Button on Employee Type Page");
        Employee objEmp = new Employee();
        objEmp.EmpLogInID = this.EMPID;
        objEmp.EmpPassword = this.Pass;
        objEmp.PensivoUUID = this.UUID;
        objEmp.CreatedSource = this.Src;
        objEmp.EmpFirstName = this.Fname;
        objEmp.EmpLastName = this.Lname;


        // Set Job Code For Location
        string sJobCodeValue = "";
        if (this.searchbyvalue == "CALLCTR")
        {
            sJobCodeValue = "99995";
        }
        else if (this.searchbyvalue == "CORP")
        {
            sJobCodeValue = "99996";
        }
        else if (this.searchbyvalue == "LOGS")
        {
            sJobCodeValue = "99997";
        }
        else if (this.searchbyvalue == "RETL")
        {
            sJobCodeValue = "99998";
        }
        else if (this.searchbyvalue == "DRAUGHTSERVICES")
        {
            sJobCodeValue = "99994";
        }
        else if (this.searchbyvalue == "WR")
        {
            sJobCodeValue = "99994";
        }


        // Getting UUID from Pensivo Server edited on 23012015
        if (string.IsNullOrEmpty(this.UUID))
        {
            objEmp.PensivoUUID = PensivoExecutions.PensivoPostUserData(Convert.ToString(this.EMPID), Convert.ToString(this.Pass), Convert.ToString(this.Fname), Convert.ToString(this.Lname), this.searchbynewvalue, this.searchbyvalue, this.searchbySiteValue, sJobCodeValue);

            if (string.IsNullOrEmpty(objEmp.PensivoUUID))
            {
                ErrorLog.createLog("Postgresql retrun null for user  " + this.EMPID);
                objEmp.SaveSysErrorLog(this.EMPID);
            }
            else
            {
                ErrorLog.createLog("Postgresql retrun   " + this.EMPID);
            }
        }
        else
        {
            
            objEmp.PensivoUUID = this.UUID;
        }




        if (
            (objEmp.InsertEmployeeShortDesc() == true) &&
            (objEmp.UpdateEmployeeFirstLastName(BusinessUtility.GetInt(objEmp.EmpID)) == true) &&
            (objEmp.InsertEmployeeAnswer(BusinessUtility.GetInt(objEmp.EmpID), BusinessUtility.GetInt(this.QID), this.QAns) == true) &&
            (objEmp.InsertEmployeeAnswer(BusinessUtility.GetInt(objEmp.EmpID), BusinessUtility.GetInt(this.QOtherID), this.QOtherAns) == true) &&
            (objEmp.InsertEmployeeSysRef(BusinessUtility.GetInt(objEmp.EmpID), this.searchby, this.searchbyvalue) == true) &&
            (objEmp.InsertEmployeeSysRef(BusinessUtility.GetInt(objEmp.EmpID), this.SearchByNew, this.searchbynewvalue) == true) &&
            (objEmp.InsertEmployeeSysRef(BusinessUtility.GetInt(objEmp.EmpID), EmpRefCode.JobCode, sJobCodeValue) == true) &&
            (objEmp.InsertEmployeeSysRef(BusinessUtility.GetInt(objEmp.EmpID), this.searchbySite, this.searchbySiteValue) == true) &&
            (objEmp.InsertEmployeeSysRef(BusinessUtility.GetInt(objEmp.EmpID), EmpRefCode.CSite, this.searchbySiteValue) == true)
            )
        {
            EmployeeValidation objEmpValidate = new EmployeeValidation();
            if (objEmpValidate.ValidateEmployee(this.EMPID, this.Pass) == true)
            {
                RoleManageLists objRoleList = new RoleManageLists();
                objRoleList.UpdateEmployeeHasFunctionalityStatus(BusinessUtility.GetInt(objEmp.EmpID),"");

                EmpAssignedCourses objAssignedCourse = new EmpAssignedCourses();
                objAssignedCourse.ResetEmployeeAssignedCoursesByEmpID(BusinessUtility.GetInt(objEmp.EmpID), 0, (int)AssignedCourseInActiveReason.ByUserProfileChange , (int)AssignedCourseUpdatedSource.ByUserProfileChange);
                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideWaitMessage", "ClosePensivoWaitingMessage();", true);
                PensivoExecutions.PensivoPostData(this.Fname, this.Lname, this.Pass, "", BusinessUtility.GetInt(this.UUID));
                Response.Redirect("employeedashboard.aspx");
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideWaitMessage", "ClosePensivoWaitingMessage();", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideWaitMessage", "ClosePensivoWaitingMessage();", true);
            showAlertMessage();
        }
    }

    /// <summary>
    /// To Get Search By New
    /// </summary>
    public string SearchByNew
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchbynew"]);
        }
    }

    /// <summary>
    /// To Get Employee Created Source
    /// </summary>
    public string Src
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["src"]);
        }
    }

    /// <summary>
    /// To Get Employee ID
    /// </summary>
    public string EMPID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["EMPID"]);
        }
    }

    /// <summary>
    /// To Get Pensivo UUID
    /// </summary>
    public string UUID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["UUID"]);
        }
    }

    /// <summary>
    /// To Get First Name
    /// </summary>
    public string Fname
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["Fname"]);
        }
    }

    /// <summary>
    /// To Get Last Name
    /// </summary>
    public string Lname
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["Lname"]);
        }
    }

    /// <summary>
    /// To Get Question ID
    /// </summary>
    public int QID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["QID"]);
        }
    }
    
    /// <summary>
    /// To Get Question Answer
    /// </summary>
    public string QAns
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["QAns"]);
        }
    }

    /// <summary>
    /// To Get Other Question ID
    /// </summary>
    public int QOtherID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["QOtherID"]);
        }
    }

    /// <summary>
    /// To Get Other Question Answer
    /// </summary>
    public string QOtherAns
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["QOtherAns"]);
        }
    }

    /// <summary>
    /// To Get Password
    /// </summary>
    public string Pass
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["PP"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string searchby
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search By Value
    /// </summary>
    public string searchbyvalue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchbyvalue"]);
        }
    }

    /// <summary>
    /// To Get Search By New
    /// </summary>
    public string searchbynew
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchbynew"]);
        }
    }

    /// <summary>
    /// To Get Search By New Value
    /// </summary>
    public string searchbynewvalue
    {
        get
        {
            return BusinessUtility.GetString(hdnSearchNewValue.Value);
        }
    }

    /// <summary>
    /// To Get Search By Site
    /// </summary>
    public String searchbySite
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchbySite"]);
        }
    }

    /// <summary>
    /// To Get Search By Site Value
    /// </summary>
    public String searchbySiteValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchbySiteValue"]);
        }

    }

    /// <summary>
    /// To Show Message
    /// </summary>
    private void showAlertMessage()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowPensivoMessage('" + Resources.Resource.lblPleaseContactAdministrator + "');", true);
    }

}