﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;


public partial class RoleManageList : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// To Create Role Manage List Class object
    /// </summary>
    RoleManageLists objRoleManageLists;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvManageList))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblManageLists, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
        }

        HdnRoleID.Value = BusinessUtility.GetString(this.RoleID);
        HdnSearchBy.Value = BusinessUtility.GetString(this.SearchBy);
        HdnFlag.Value = BusinessUtility.GetString(this.Flag);
        HdnActionID.Value = BusinessUtility.GetString(this.ActionID);
        HdnFunctionalityType.Value = BusinessUtility.GetString(this.FunctionalityType);

        Event objEvent = new Event();
        string roleName = "";
        if (this.RoleID > 0)
        {
            objRole = new Role();
            roleName = objRole.GetRoleName(this.RoleID);
        }

        string sTrainingEventName = "";
        string[] searchval = this.SearchValue.Split(',');
        foreach (string sValues in searchval)
        {
            objEvent = new Event();
            objEvent.GetEventDetail(BusinessUtility.GetInt(sValues), Globals.CurrentAppLanguageCode);

            if (sTrainingEventName == "")
            {
                sTrainingEventName = objEvent.EventName;
            }
            else
            {
                sTrainingEventName += ",<br/>" + objEvent.EventName;
            }
        }

        if (this.Flag == "add")
        {
            HdnMessage.Value = Resources.Resource.lblRoleSysAddManageList.Replace("#ROLENAME#", roleName);
        }
        else if (this.Flag == "remove")
        {
            HdnMessage.Value = Resources.Resource.lblRoleSysRemoveManageList.Replace("#ROLENAME#", roleName);
        }


        HdnSearchBy.Value = BusinessUtility.GetString(this.SearchBy);
        HdnFlag.Value = BusinessUtility.GetString(this.Flag);
        HdnActionID.Value = BusinessUtility.GetString(this.ActionID);
        HdnFunctionalityType.Value = BusinessUtility.GetString(this.FunctionalityType);
        HdnCreatedBy.Value = BusinessUtility.GetString(CurrentEmployee.EmpID);

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addManageList")
        {
            try
            {
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                string functionalityType = BusinessUtility.GetString(Request.Form["FunctionalityType"]);
                int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                int manageListID = BusinessUtility.GetInt(Request.Form["ManageListID"]);
                if (manageListID > 0 && roleID > 0 && actionID > 0)
                {
                    objRole = new Role();
                    if (objRole.ActionExistsInRole(roleID, actionID, functionalityType) == false)
                    {
                        objRole.RoleAddFunctionality(roleID, actionID, functionalityType);
                    }

                    objRoleManageLists = new RoleManageLists();
                    if (objRoleManageLists.RoleAssignManageList(roleID, manageListID) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
        else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeManageList")
        {
            try
            {
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                string functionalityType = BusinessUtility.GetString(Request.Form["FunctionalityType"]);
                int manageListID = BusinessUtility.GetInt(Request.Form["ManageListID"]);
                if (roleID > 0 && manageListID >0 && actionID > 0)
                {
                    objRoleManageLists = new RoleManageLists();
                    if (objRoleManageLists.RemoveRoleAssignManageList(roleID, manageListID) == true)
                    {
                        objRole = new Role();

                        objRoleManageLists = new RoleManageLists();
                        if (objRoleManageLists.IsRoleAssignManageList(roleID) == false)
                        {
                            objRole.RoleRemoveFunctionality(roleID, actionID, functionalityType);
                        }
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
    }

    /// <summary>
    /// To Define JQ Grid Cell Binding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event</param>
    protected void gvManageList_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        Int64 iRoleID = BusinessUtility.GetInt(e.RowKey);
        if (e.ColumnIndex == 2)
        {
            string sManageListName = BusinessUtility.GetString(e.RowValues[1]).Replace("\"", "{-").Replace("'", "{_");
            e.CellHtml = string.Format(@"<a class='btn' style='float:none;'   onclick=""SelectManageList('{0}','{1}')"">" + Resources.Resource.BtnManageListSelect + "</a>", iRoleID, sManageListName);
        }
    }

    /// <summary>
    /// To Bind JQ Grid with DataSource with Manage List Data Source
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event </param>
    protected void gvManageList_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtListName = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtListName"]);
        objRoleManageLists = new RoleManageLists();
        if (this.Flag == "add")
        {
            gvManageList.DataSource = objRoleManageLists.GetManageListNotInRole(this.RoleID, Globals.CurrentAppLanguageCode, Utils.ReplaceDBSpecialCharacter(txtListName));
        }
        else if (this.Flag == "remove")
        {
            gvManageList.DataSource = objRoleManageLists.GetManageListInRole(this.RoleID, Globals.CurrentAppLanguageCode, Utils.ReplaceDBSpecialCharacter(txtListName));
        }
        gvManageList.DataBind();
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search By Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Functionality Type
    /// </summary>
    public string FunctionalityType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["functionalityType"]);
        }
    }

}