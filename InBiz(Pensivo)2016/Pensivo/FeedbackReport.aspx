﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="FeedbackReport.aspx.cs" Inherits="PrintReport" %>

<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>
<%@ Register Assembly="Trirand.Web" TagPrefix="trirand" Namespace="Trirand.Web.UI.WebControls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry || Utils.TrainingInst == (int)Institute.EDE2)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>

    <%--Define Inline Css to Wrap Grid Header and Row Data--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th div {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
        }

        .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
            vertical-align: text-top;
        }
    </style>

    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="btngrp " style="float: left; display: none;">
                <a id="btnReportDetails" runat="server" class="btn round ui-button ui-widget ui-state-default ui-corner-all" onclick="ShowReportDetails();"><%= Resources.Resource.BtnReportDetails%></a>
            </div>
            <div class="btngrp " style="float: right">
                <a id="hrfNewSiteReport" runat="server" href="EmployeeReport.aspx" class="btn "><%= Resources.Resource.lblNew%></a>
                <a id="hrfDownloadPDF" runat="server" class="btn " onclick="DownloadPDF();"><%=Resources.Resource.lblDownloadPDF %></a>
            </div>

            <div class="btngrp " style="float: right">
                <a id="hrfNew" runat="server" href="ReportDashBoard.aspx" class="btn " style="display: none"><%= Resources.Resource.lblNew%></a>
                <a id="hrfSaveReport" runat="server" class="btn" style="display: none" onclick="SaveReport();"><%= Resources.Resource.lblSaveToMyRecentReports%></a>
                <a id="hrfDownloadExcel" runat="server" class="btn " onclick="DownloadExcel();"><%= Resources.Resource.lblDownloadExcel%></a>
            </div>
            <br />
            <br />

            <div id="grid_wrapper" style="width: 100%; overflow: auto;" onkeypress="return disableEnterKey(event)">
                <trirand:JQGrid runat="server" ID="gvReport" Height="300px" LoadOnce="true"
                    AppearanceSettings-ShrinkToFit="false" OnDataRequesting="gvReport_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="idRegistration" Visible="false" PrimaryKey="true" />
                        <trirand:JQGridColumn DataField="courseTitleEn" HeaderText="Core Questionnaire Name"
                            Width="120" Editable="false" />
                        <trirand:JQGridColumn DataField="courseVerNo" HeaderText="Core Questionnaire Version"
                            Width="80" Editable="false" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="initiatorFirstName" HeaderText="Initiator First Name"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="initiatorLastName" HeaderText="Initiator Last Name"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="initiatorEmailID" HeaderText="Initiator Email ID"
                            Width="160" Editable="false" />
                        <trirand:JQGridColumn DataField="rolename" HeaderText="Event Name and Date"
                            Width="80" Editable="false" Visible="false" />
                        <trirand:JQGridColumn DataField="eventName" HeaderText="Event Name"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="eventDate" HeaderText="Event Date"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="respondantFirstName" HeaderText="Respondant First Name"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="respondantLastName" HeaderText="Respondant Last Name"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="respondantEmailID" HeaderText="Respondant EmailID"
                            Width="160" Editable="false" />
                        <trirand:JQGridColumn DataField="IsCompleted" HeaderText="Completed"
                            Width="60" Editable="false" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="completeddatetime" HeaderText="Date Completed"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="totaltime" HeaderText="Time Completed"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="question1" HeaderText="Question One" Visible="false"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="answer1" HeaderText="Answer One" TextAlign="Center"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="question2" HeaderText="Question Two" Visible="false"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="answer2" HeaderText="Answer Two" TextAlign="Center"
                            Width="90" Editable="false" />
                        <trirand:JQGridColumn DataField="question3" HeaderText="Question Three" Visible="false"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="answer3" HeaderText="Answer Three" TextAlign="Center"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="question4" HeaderText="Question Four" Visible="false"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="answer4" HeaderText="Answer Four" TextAlign="Center"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="question5" HeaderText="Question Five" Visible="false"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="answer5" HeaderText="Answer Five" TextAlign="Center"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="question6" HeaderText="Question Six" Visible="false"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="answer6" HeaderText="Answer Six" TextAlign="Center"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="question7" HeaderText="Question Seven" Visible="false"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="answer7" HeaderText="Answer Seven" TextAlign="Center"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="question8" HeaderText="Question Eight" Visible="false"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="answer8" HeaderText="Answer Eight" TextAlign="Center"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="question9" HeaderText="Question Nine" Visible="false"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="answer9" HeaderText="Answer Nine" TextAlign="Center"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="question10" HeaderText="Question Ten" Visible="false"
                            Width="80" Editable="false" />
                        <trirand:JQGridColumn DataField="answer10" HeaderText="Answer Ten" TextAlign="Center"
                            Width="80" Editable="false" />
                    </Columns>
                    <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,2000,5000,10000]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="loadComplete" />
                </trirand:JQGrid>
            </div>
            <asp:HiddenField ID="hdnReportFilter" runat="server" />
            <asp:HiddenField ID="hdnReportType" runat="server" />
            <asp:HiddenField ID="hdnFilePath" runat="server" />
            <asp:HiddenField ID="hdnFileName" runat="server" />

            <asp:Button ID="btnDonwnloadExcelFile" runat="server" Style="display: none;" OnClick="btnDonwnloadExcelFile_Click" />
            <asp:Button ID="btnDonwnloadPDFFile" runat="server" Style="display: none;" OnClick="btnDonwnloadPDFFile_Click" />
            <asp:Button ID="btnSaveReport" runat="server" Style="display: none;" OnClick="btnSaveReport_Click" />
        </div>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvReport.ClientID%>");
        $grid.initGridHelper({
            gridWrapPanleID: "grid_wrapper"
        });

        // Resized JQ Grid
        function jqGridResize() {
            $("#<%=gvReport.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // Define JQ Grid Load Complete
        function loadComplete(data) {
            jqGridResize();
            ClosePensivoWaitingMessage();

            if (data.rows.length == 0 && data.total == 0) {
                ShowPensivoMessage('<%= Resources.Resource.lblNoReportDataFoundMessage %>');
            }
        }

        // To Save Report
        function SaveReport() {
            var title = "<%=Resources.Resource.lblConfirmation%>";
            var messageText = "<%=Resources.Resource.msgConifrmationSaveReport%>";
            okButtonText = "<%=Resources.Resource.lblOk%>";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "ConfirmReportOk()";
            LaterCnclButtonRedirectLink = "ConfirmReportCancel();";

            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Save Report if ok
        function ConfirmReportOk() {
            $("#<%=btnSaveReport.ClientID%>").trigger("click");
        }

        // To Cancel Save Report
        function ConfirmReportCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
        }

        // To Download Report Excel Format
        function DownloadExcel() {
            ShowWaiting();
            $.ajax(
                {
                    type: "POST",
                    url: "FeedbackReport.aspx/DownloadExcel",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function 
                    (msg) {
                        var title = "<%=Resources.Resource.BtnReportDetails%>";
                        var filePathName = msg.d;
                        if (filePathName != '') {
                            var arr = filePathName.split('|');
                            var FilePath = arr[0];
                            var FileName = arr[1];

                            $('#<%=hdnFilePath.ClientID%>').val(FilePath);
                            $('#<%=hdnFileName.ClientID%>').val(FileName);

                            $('#<%=btnDonwnloadExcelFile.ClientID%>').trigger("click");
                        }
                        ClosePensivoWaitingMessage();
                    },
                    error: function (x, e) {
                        ClosePensivoWaitingMessage();
                    }
                }
                );
            }

            // To Show Report Summary Detail
            function ShowReportDetails() {
                var title = "<%=Resources.Resource.BtnReportDetails%>";
                var messageText = "<%= GetReportSummaryDetails() %>";
                okButtonText = "<%=Resources.Resource.btnClose%>";
                LaterCnclButtonText = "";
                okButtonRedirectlink = "ConfirmReportCancel();";
                LaterCnclButtonRedirectLink = "ConfirmReportCancel();";
                PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
            }

            // Create Report PDF Format
            function DownloadPDF() {
                ShowPensivoWaitingMessage('<%=Resources.Resource.lblReportWaitingMessage%>');
                $.ajax(
                    {
                        type: "POST",
                        url: "Report.aspx/DownloadPDF",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        cache: false,
                        success: function
                    (msg) {
                            var title = "<%=Resources.Resource.BtnReportDetails%>";
                            var filePathName = msg.d;
                            if (filePathName != '') {
                                var arr = filePathName.split('|');
                                var FilePath = arr[0];
                                var FileName = arr[1];

                                $('#<%=hdnFilePath.ClientID%>').val(FilePath);
                                $('#<%=hdnFileName.ClientID%>').val(FileName);

                                $('#<%=btnDonwnloadPDFFile.ClientID%>').trigger("click");
                            }
                            ClosePensivoWaitingMessage();
                        },
                        error: function (x, e) {
                            ClosePensivoWaitingMessage();
                        }
                    }
                );
                }

                // To Show Waiting Message For JQ Grid Load and Excel Report Download
                function ShowWaiting() {
                    ShowPensivoWaitingMessage('<%=Resources.Resource.lblShowWaitingMessage%>');
        }
    </script>
</asp:Content>
