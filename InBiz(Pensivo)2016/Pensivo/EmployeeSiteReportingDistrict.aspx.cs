﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;


public partial class EmployeeSiteReportingDistrict : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Manage_List);

        if (!IsPostBack && !IsPagePostBack(gvSite))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSiteReporting, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            gvSite.Columns[1].HeaderText = Resources.Resource.lblSearchSiteDistrict;
            //ltrTitle.Text = Resources.Resource.lblSiteReportingRetail;
            ltrTitle.Text = BusinessUtility.GetString(Request.QueryString["roleManageListName"]);
            ltrSearchTitle.Text = Resources.Resource.lblFindDistricts;
            ltrSearchMessage.Text = Resources.Resource.lblFindDistrictMessage;
            lblRoleName.Text = Resources.Resource.lblSearchSiteDistrict;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblSearchSiteDistrict);
        }

        hdnListID.Value = BusinessUtility.GetString(this.ListID);
    }


    /// <summary>
    /// To Define JQ Grid Cell Binding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event</param>
    protected void gvSite_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        string sSiteID = BusinessUtility.GetString(e.RowKey);
        if (e.ColumnIndex == 3)
        {
            string sManageListName = BusinessUtility.GetString(e.RowValues[1]).Replace("\"", "{-").Replace("'", "{_");
            e.CellHtml = string.Format(@"<a class='btn' style='float:none;'   onclick=""SiteManagerDashboard('{0}','{1}','{2}','{3}','{4}')"">" + Resources.Resource.BtnEdit + "</a>", sSiteID, RoleManageListID, EmpRefCode.District, BusinessUtility.GetString(this.FunctionalityID), BusinessUtility.GetString(this.ListID));
        }
        else if (e.ColumnIndex == 2)
        {
            RoleManageLists objRoleManageList = new RoleManageLists();
            //if (this.ListID == 1)
            //{
            //    string siteUser = objRoleManageList.GetUserSiteRetailWRAssgined(EmpRefCode.Store, sSiteID, (int)RoleAction.Reporting, this.ListID);
            //    e.CellHtml = siteUser;
            //}
            //else
            //{
            //    e.CellHtml = objRoleManageList.GetUserSiteLogisticsAssgined(EmpRefCode.Store, sSiteID, (int)RoleAction.Reporting, this.ListID);
            //}


            string siteUser = objRoleManageList.GetUserSiteDestrictReporting(EmpRefCode.District, sSiteID, (int)RoleAction.Reporting, this.ListID);
            e.CellHtml = siteUser;
        }
    }

    /// <summary>
    /// To Bind JQ Grid With Data Source Site SupervisorManager can see Site Report
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvSite_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtRole = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        RoleManageLists objRoleManageList = new RoleManageLists();
        DataTable dt;
        dt = objRoleManageList.GetEmployeeCanSeeSiteDistrictReport(Utils.ReplaceDBSpecialCharacter(txtRole));

        gvSite.DataSource = dt;
        gvSite.DataBind();
    }

    /// <summary>
    /// To Get Role Manage List ID
    /// </summary>
    public int RoleManageListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleManageListID"]);
        }
    }

    /// <summary>
    /// To Get Functionality ID
    /// </summary>
    public int FunctionalityID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["FunctionalityID"]);
        }
    }


    public int ListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["ListID"]);
        }
    }
}