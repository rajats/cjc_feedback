﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;

public partial class EmployeeCrossDocklist : BasePage
{
    //protected string siteNoUser = "";

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Manage_List);

        if (!IsPostBack && !IsPagePostBack(gvSite))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSiteReporting, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            //gvSite.Columns[1].HeaderText = Resources.Resource.lblSupervisorManagerSite;
            //ltrTitle.Text = Resources.Resource.lblSiteReportingRetail;
            ltrTitle.Text = BusinessUtility.GetString(Request.QueryString["roleManageListName"]);
            ltrSearchTitle.Text = Resources.Resource.lblManagelistFindAnCEmployee;
            ltrSearchMessage.Text = Resources.Resource.lblManagelistFindAnCEmployeeFindMessage;
            lblRoleName.Text = Resources.Resource.lblEmployee;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblEmployee);
        }

        hdnListID.Value = BusinessUtility.GetString(this.ListID);
    }


    /// <summary>
    /// To Define JQ Grid Cell Binding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event</param>
    protected void gvSite_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
    }

    /// <summary>
    /// To Bind JQ Grid With Data Source Cross Docks Empoyees
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvSite_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtRole = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        RoleManageLists objRoleManageList = new RoleManageLists();
        DataTable dt;
        dt = objRoleManageList.GetCrossDocksEmployeeList(Utils.ReplaceDBSpecialCharacter(txtRole), EmpSearchBy.Store);
        gvSite.DataSource = dt;
        gvSite.DataBind();
    }

    /// <summary>
    /// To Get Role Manage List ID
    /// </summary>
    public int RoleManageListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleManageListID"]);
        }
    }

    /// <summary>
    /// To Get Functionality ID
    /// </summary>
    public int FunctionalityID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["FunctionalityID"]);
        }
    }


    public int ListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["ListID"]);
        }
    }
}