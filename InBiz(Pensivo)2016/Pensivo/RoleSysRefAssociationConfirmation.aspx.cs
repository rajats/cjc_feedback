﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class RoleSysRefAssociationConfirmation : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblConfirmation, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            string roleName = "";
            string functionalityName = "";
            if (this.RoleID > 0)
            {
                objRole = new Role();
                roleName = objRole.GetRoleName(this.RoleID);
            }


            if (this.SearchBy == EmpRefCode.Store)
            {
                if (Utils.TrainingInst == (int)Institute.bdl)
                {
                    functionalityName = Resources.Resource.lblDeptID;
                }
                else
                {
                    functionalityName = Resources.Resource.lblSearchSite;
                }
            }
            else if (this.SearchBy == EmpRefCode.Region)
            {
                functionalityName = Resources.Resource.lblSearchRegion;
            }
            else if (this.SearchBy == EmpRefCode.JobCode)
            {
                functionalityName = Resources.Resource.lblSearchJobCode;
            }
            else if (this.SearchBy == EmpRefCode.Division)
            {
                functionalityName = Resources.Resource.lblSearchLocation;
            }

            else if (this.SearchBy == EmpRefCode.DivisionTBS)
            {
                functionalityName = Resources.Resource.lblSearchDivision;
            }
            else if (this.SearchBy == EmpRefCode.RegionTBS)
            {
                functionalityName = Resources.Resource.lblSearchRegion;
            }
            else if (this.SearchBy == EmpRefCode.District)
            {
                functionalityName = Resources.Resource.lblSearchDistrict;
            }

            if (this.Flag == "add")
            {
                ltrTitle.Text = "Added functionality to the system role '" + roleName + "' : " + "to  following " + functionalityName + " users </br>" + this.SearchValue.Replace(",", "</br>");
            }
            else if (this.Flag == "remove")
            {
                ltrTitle.Text = "Removed functionality to the system role '" + roleName + "' : " + "to  following " + functionalityName + " users </br>" + this.SearchValue.Replace(",", "</br>");
            }
            HdnSearchBy.Value = BusinessUtility.GetString(this.SearchBy);
            HdnSearchValue.Value = BusinessUtility.GetString(this.SearchValue);
            HdnFlag.Value = BusinessUtility.GetString(this.Flag);
            hdnRoleID.Value = BusinessUtility.GetString(this.RoleID);
            HdnSysRoleOperator.Value = BusinessUtility.GetString(this.SysRoleOperator);
            hrefNo.HRef = "roleActivity.aspx?roleID=" + this.RoleID;
            if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addAction")
            {
                try
                {
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                    string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                    string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                    string sysRoleOperator = BusinessUtility.GetString(Request.Form["SysRoleOperator"]);
                    if (searchBy != "" && roleID > 0 && searchValue != "")
                    {
                        objRole = new Role();
                        if (objRole.RoleAddEmpRef(roleID, searchBy, searchValue, sysRoleOperator) == true)
                        {
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }

                Response.End();
                Response.SuppressContent = true;
            }
            else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeAction")
            {
                try
                {
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                    string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                    string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                    if (searchBy != "" && roleID > 0 && searchValue != "")
                    {
                        objRole = new Role();
                        if (objRole.RoleRemoveEmpRef(roleID, searchBy, searchValue) == true)
                        {
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }

                Response.End();
                Response.SuppressContent = true;
            }
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Flag
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Sys Role Operator
    /// </summary>
    public string SysRoleOperator
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["operator"]);
        }
    }

}