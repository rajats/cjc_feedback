﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ReportDate.aspx.cs" Inherits="ReportSpecificDate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <section id="main-content"  class="pg-dashboard">
    <div class="wrapper" onkeypress="return disableEnterKey(event)">
        <h3>
            <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h3>
            <div class="plms-fieldset">
                <label class="plms-label is-hidden" for="txtDate"><%= Resources.Resource.lblDate%></label>
                <div class="plms-tooltip-parent">
                    <asp:TextBox ID="txtDate" runat="server" class="plms-input skin2 "  style="width:300px;" placeholder="<%$ Resources:Resource, lblDate %>"  />
                    <asp:RequiredFieldValidator ID="rfDate" runat="server" class="formels-feedback invalid" ValidationGroup="Group1"
                        ControlToValidate="txtDate" Text="<%$ Resources:Resource, reqMsgRequiredDate %>" />
                </div>
            </div>
            <div id="dvToDate" runat="server" visible="false" class="plms-fieldset">
                <label class="plms-label is-hidden" for="txtToDate"><%= Resources.Resource.lblUpToAndIncluding%></label>
                <div class="plms-tooltip-parent">
                    <asp:TextBox ID="txtToDate" runat="server" class="plms-input skin2 " style="width:300px;" placeholder="<%$ Resources:Resource, lblUpToAndIncluding %>"  />
                    <asp:RequiredFieldValidator ID="rfToDate" runat="server" class="formels-feedback invalid" ValidationGroup="Group1"
                        ControlToValidate="txtToDate" Text="<%$ Resources:Resource, reqMsgUpToAndIncluding %>" />
                </div>
            </div>
        <asp:Button ID="btnCountinue" class="btn align-r" runat="server" Text="<%$ Resources:Resource, lblCountinue %>" OnClick="btnCountinue_OnClick" ValidationGroup="Group1" />
        <asp:Button ID="btnBack" class="btn align-r" runat="server" Text="<%$ Resources:Resource, lblBack %>" OnClick="btnBack_OnClick"  />
    </div> </section>
</asp:Content>

