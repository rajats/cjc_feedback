﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeeDashBoard.aspx.cs" Inherits="EmployeeDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <header class="main-content-header">
                <h1><%=Resources.Resource.lblMyTraining %></h1>
            </header>
            <div class="boxset">
                <div class="boxset-box">
                    <a href="EmployeeCourses.aspx?CourseStatus=1" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTraningIamTaking%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a href="EmployeeCourses.aspx?CourseStatus=3" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTraningIshouldTake%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a href="TraningYearSelection.aspx" class="boxset-box-wrapper"><%--EmployeeCourses.aspx?CourseStatus=5--%>
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTrainingIHaveTaken%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

