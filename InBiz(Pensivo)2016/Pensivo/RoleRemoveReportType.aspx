﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleRemoveReportType.aspx.cs" Inherits="RoleRemvoveReportType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <%=htmlText %>
        </div>
    </section>


    <asp:HiddenField ID="hdnMessage" runat="server"   />

    <%--<asp:HiddenField ID="hdnRoleNames" runat="server"   />--%>
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Remove Role Report Confirmation
        function removeInRoleReport(roleID, actionType, actionItemID, reportType, reportTypeText) {
            var title = "Confirmation";
            var messageText = $("#<%=hdnMessage.ClientID%>").val().replace("#REPORTTYPE#", reportTypeText);

            okButtonText = "OK";
            LaterCnclButtonText = "Cancel";
            okButtonRedirectlink = "ConfirmRoleRemovefuncOk(" + roleID + ", '" + actionType + "', '" + actionItemID + "', '" + reportType + "')";
            LaterCnclButtonRedirectLink = "ConfirmRoleRemovefuncCancel();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Trigger Role Remove Ok Button
        function ConfirmRoleRemovefuncOk(roleID, actionType, actionItemID, reportType) {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "removeAction";
            datatoPost.RoleID = roleID;
            datatoPost.ActionType = actionType;
            datatoPost.ActionItemID = actionItemID;
            datatoPost.ReportType = reportType;
            $.post("RoleRemoveReportType.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    window.location.href = "roleActivity.aspx?roleID=" + roleID + "";
                }
                else {
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotRemoveFunctionality%>")
                }
            });
        }

        // To Trigger Role Remove Cancel Button
        function ConfirmRoleRemovefuncCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
        }

    </script>

</asp:Content>
