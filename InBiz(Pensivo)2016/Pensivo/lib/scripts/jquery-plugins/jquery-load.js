$(function () {
   
    /*$('select').selectmenu();*/
});
$(document).ready(function () {
    $("button, input:submit", "").not('.b_set').button();
    $("button, input:button", "").not('.b_set').button();
    //Autocomplete off for jquery
    $(".datepicker, .dob_datepicker").attr('autocomplete', 'off');
    $(".datepicker").datepicker($.extend($.datepicker.regional[_jqdplang]).extend({
        changeMonth: true,
        changeYear: true
    }));  

    //DOB date picker
    $(".dob_datepicker").datepicker($.extend($.datepicker.regional[_jqdplang]).extend({
        defaultDate: '01/01/1981',
        changeMonth: true,
        changeYear: true,
        yearRange: 'c-30:c+10'
    }));  

    $("#selectable").selectable();
    $("#radioset").buttonset();
    //$("input[type=text]").focus(function () { $(this).css('background-color', '#F6FBFF'); });
    //$("input[type=text]").blur(function () { $(this).css('background-color', '#fdfdfa'); });

    $("#dialog").dialog({
        autoOpen: false, modal: true
    });
    $("#dialogHelp").dialog({
        autoOpen: false, modal: true, minWidth: 600, minHeight: 500
    });
    $("#dialogMail").dialog({
        autoOpen: false, modal: true, minWidth: 600, minHeight: 500
    });

    $("#opener").click(function () {
        $("#dialog").dialog("open");
        return false;
    });
    $("#openerHelp").click(function () {
        $("#dialogHelp").dialog("open");
        return false;
    });


    $("table tr.grid_alter_rowstyle").css({ background: "F2F6F8" }).hover(
        function () { $(this).css({ background: "#B8C9D2" }); },
        function () { $(this).css({ background: "#F2F6F8" }); }
        );
    $("table tr.grid_rowstyle").css({ background: "ffffff" }).hover(
        function () { $(this).css({ background: "#B8C9D2" }); },
        function () { $(this).css({ background: "#ffffff" }); }
        );
});

function initDatePicker(selector) {    
    $(selector).datepicker($.extend($.datepicker.regional[_jqdplang]).extend({        
        changeMonth: true,
        changeYear: true
    }));  
}


function pageLoad() {
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(EndRequest);
}

function EndRequest(sender, args) {
    $("button, input:submit", "").button();
    $("button, input:button", "").button();    
    $(".datepicker").datepicker();
    $("#selectable").selectable();
    $("#radioset").buttonset();
    //$("input[type=text]").focus(function () { $(this).css('background-color', '#F6FBFF'); });
    //$("input[type=text]").blur(function () { $(this).css('background-color', '#fdfdfa'); });
}



function disableEnterKey(e) {
    var numCharCode;
    var elTarget;
    var strType;

    // get event if not passed
    if (!e) var e = window.event;

    // get character code of key pressed
    if (e.keyCode) numCharCode = e.keyCode;
    else if (e.which) numCharCode = e.which;

    // get target
    if (e.target) elTarget = e.target;
    else if (e.srcElement) elTarget = e.srcElement;

    // if form input field
    if (elTarget.tagName.toLowerCase() == 'input') {

        // get type
        strType = elTarget.getAttribute('type').toLowerCase();

        // based on type
        switch (strType) {
            case 'checkbox':
            case 'radio':
            case 'text':
            case 'button':
            case 'submit':

                // if this is a return
                if (numCharCode == 13) {
                    // cancel event to prevent form submission
                    return false;
                }

                break;

        }

    }

    // process default action
    return true;
}
function funBlank() {
    var x = document.getElementById("aspnetForm");
    x.target = "_blank";
}
function funSame() {
    var x = document.getElementById("aspnetForm");
    x.target = "_parent";
}
/*function GetAlert() {
    popupWithDybnamicCheck_CloseURL('Alert-1', '../Service.aspx', 'seq=0', 120000, '../Service.aspx', '');
    popupWithDybnamicCheck_CloseURL('Alert-2', '../Service.aspx', 'seq=1', 120000, '../Service.aspx', '');
    popupWithDybnamicCheck_CloseURL('Alert-3', '../Service.aspx', 'seq=2', 120000, '../Service.aspx', '');
    popupWithDybnamicCheck_CloseURL('Alert-4', '../Service.aspx', 'seq=3', 120000, '../Service.aspx', '');

}*/

function openPDF(strOpen) {
    window.open(strOpen, 'Document', 'height=365,width=810,left=180,top=150,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');
    return false;
}

//Fix for jquery ui dialog auto width issue in IE
(function ($) {
    var fixDialogAutoWidth = $.noop;
    if ($.browser.msie) {
        fixDialogAutoWidth = function (content) {
            var dialog = $(content).parent('.ui-dialog');
            var width = dialog.innerWidth();
            if (width) dialog.css('width', width);
        }
    }

    var _init = $.ui.dialog.prototype._init;
    $.ui.dialog.prototype._init = function () {
        // IE magick: (width: 'auto' not working correctly) :
        // http://dev.jqueryui.com/ticket/4437
        if (this.options.width == 'auto') {
            var open = this.options.open;
            this.options.open = function () {
                fixDialogAutoWidth(this);
                if (open) open.apply(this);
            }
        }
        // yet another bug options.hide: 'drop' does not work
        // in IE http://dev.jqueryui.com/ticket/5615
        if ($.browser.msie && this.options.hide == 'drop') {
            this.options.hide = 'fold';
        }
        return _init.apply(this); // calls open() if autoOpen
    };
})(jQuery);

//Default date time validation mm/DD/yyyy
function GetDateObj(op) {
    var dateorder = "mdy";
    function GetFullYear(year) {
        var twoDigitCutoffYear = 2029 % 100;
        var cutoffYearCentury = 2029 - twoDigitCutoffYear;
        return ((year > twoDigitCutoffYear) ? (cutoffYearCentury - 100 + year) : (cutoffYearCentury + year));
    }
    var yearFirstExp = new RegExp("^\\s*((\\d{4})|(\\d{2}))([-/]|\\. ?)(\\d{1,2})\\4(\\d{1,2})\\.?\\s*$");
    m = op.match(yearFirstExp);
    var day, month, year;
    if (m != null && (m[2].length == 4 || dateorder == "ymd")) {
        day = m[6];
        month = m[5];
        year = (m[2].length == 4) ? m[2] : GetFullYear(parseInt(m[3], 10))
    }
    else {
        if (dateorder == "ymd") {
            return null;
        }
        var yearLastExp = new RegExp("^\\s*(\\d{1,2})([-/]|\\. ?)(\\d{1,2})(?:\\s|\\2)((\\d{4})|(\\d{2}))(?:\\s\u0433\\.)?\\s*$");
        m = op.match(yearLastExp);
        if (m == null) {
            return null;
        }
        if (dateorder == "mdy") {
            day = m[3];
            month = m[1];
        }
        else {
            day = m[1];
            month = m[3];
        }
        year = (m[5].length == 4) ? m[5] : GetFullYear(parseInt(m[6], 10))
    }
    month -= 1;
    var date = new Date(year, month, day);
    if (year < 100) {
        date.setFullYear(year);
    }
    return (typeof (date) == "object" && year == date.getFullYear() && month == date.getMonth() && day == date.getDate()) ? date.valueOf() : null;
}

function ValidateDate(sender, args) {
    args.IsValid = GetDateObj(args.Value) != null;
}

/*function deploySetupMenu() {
    $('ul.menu ul').hide();
    $.each($('ul.menu'), function () {
        $('#' + this.id + '.expandfirst ul:first').show();
    });
    $.each($('li.open'), function () {
        $(this).parent().show();
        $(this).parent().parent().addClass('active');
    });
    $('ul.menu li').delegate("a", "click", function () {
        var checkElement = $(this).next();
        var parent = this.parentNode.parentNode.id;
        if ($('#' + parent).hasClass('noaccordion')) {
            checkElement.slideToggle('fast');
            checkElement.parent().toggleClass('active');
            return false;
        }
        if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            if ($('#' + parent).hasClass('collapsible')) {
                $('#' + parent + ' ul:visible').slideUp('fast').parent().toggleClass('active');
            }
            return false;
        }
        if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#' + parent + ' ul:visible').slideUp('fast').parent().toggleClass('active');
            checkElement.slideDown('fast').parent().toggleClass('active');
            return false;
        }
    });
}*/

