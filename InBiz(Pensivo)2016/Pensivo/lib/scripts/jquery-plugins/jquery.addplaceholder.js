/*!
 * 'addPlaceholder' Plugin for jQuery
 *
 * @author Hitendra Malviya
 * @created 04-25-2012
 * @updated 04-25-2012
 * @version 1.0.0
 *
 * Description:
 * jQuery plugin that adds functionality to add placeholders to input text
 * 
 * Usage:
 * $(selector).addPlaceholder(options);
 * $(selector).addPlaceholder({placeholderTextColor: 'PLACEHOLDER_TEXT_COLOR', placeholderAttr:'OTHER_ATTRIBUTE_NAME'});
 */
(function ($) {
    $.fn.addPlaceholder = function (options) {
        return this.each(function () {
            var settings = $.extend({
                placeholderAttr: 'ph', //attribut name to retreive placeholder text               
                placeholderTextColor: '#8D8D8D' //place holder text color 
            }, options);
            var input = this,
                $input = $(this),
                offset = $input.offset();

            var $div;

            function hide() {
                $div.hide();

                //$input.add($div).removeClass(settings.placeholderCss);
            };

            function show() {
                $div.show();
                //$input.add($div).addClass(settings.placeholderCss);
            }

            function focus() {
                if (input.value.length) hide();
                else show();
            };

            //Create Wrap element
            $wrap = $('<div />').css({ 'position': 'relative', 'display': 'inline-block' });
            //$innerWrap = $('<span />');
            $input.wrap($wrap);

            // Create div to put placeholder text in
            $div = $('<div>' + $input.attr(settings.placeholderAttr) + '</div>')
            // Position it to the same place as the input box:
                    .css({ position: 'absolute',
                        top: 0, //offset.top,
                        left: 4, //offset.left + 4,
                        cursor: 'text'
                    })
                    .click(function () {
                        $input.focus();
                        focus();
                    }).css({ 'color': settings.placeholderTextColor, 'line-height' : '25px', 'padding-left': '5px' });
            $input.after($div);


            // Also add the class to the input box:                    
            $input.keyup(focus).blur(function () {
                if (!input.value.length) show();
            }).change(function () {
                if (!input.value.length) show();
                else hide();
            });

            //finally need to check if textbox has value then no need to show placeholder
            if ($input.val() != "") {
                hide();
            }           
        });
    };
})(jQuery);