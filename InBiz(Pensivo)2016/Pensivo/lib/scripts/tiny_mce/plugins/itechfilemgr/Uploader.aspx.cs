﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.IO;

using iTECH.Library.Utilities;

public partial class FileManager_Uploader : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //AuthenticateFileManager();
        if (Session["_uploadMessageSuccess"] != null)
        {
            lblGreen.Text = Session["_uploadMessageSuccess"].ToString();
        }
        if (Session["_uploadMessageError"] != null)
        {
            lblRed.Text = Session["_uploadMessageError"].ToString();
        }
        Session.Remove("_uploadMessageSuccess");
        Session.Remove("_uploadMessageError");
    }



    public void AuthenticateFileManager()
    {
        /* Edit this funcation to  AuthenticateFileManager*/
        string SessionID = Request["sessionid"].ToString();

        if (Request.Cookies[SessionID] != null)
        {
            
        }
        else
        {
            Response.Clear();
            Response.Write("Access Denied");
            Response.End();
        }

    }    

    private string UploadImage(HttpPostedFile fu)
    {
        try
        {
            string fName = Path.GetFileName(fu.FileName);
            string ext = Path.GetExtension(fu.FileName);
            string tFName = string.Format("{0}{1}", FileManager.GetRandomFileName(), ext);
            string thumbName = "thumb." + tFName;
            if (FileManager.IsValidImageFileType(ext))
            {
                if (FileManager.UploadImage(fu, this.ImageWidth, this.ImageHeight, txtPath.Text.Replace("//", "/"), tFName))
                {
                    FileManager.UploadImage(fu, 100, 100, txtPath.Text.Replace("//", "/"), thumbName);
                    return tFName;
                }

                return string.Empty;
            }
            else
            {
                return string.Empty;
            }            
        }
        catch
        {
            return string.Empty;
        }
    }

    private int ImageWidth
    {
        get
        {
            int w = 0;
            int.TryParse(txtWidth.Text, out w);
            return w;
        }
    }

    private int ImageHeight
    {
        get
        {
            int h = 0;
            int.TryParse(txtHeight.Text, out h);
            return h;
        }
    }
    protected void bntUpload_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            try
            {
                if (fuUpload.HasFile)
                {
                    string ext = Path.GetExtension(fuUpload.FileName);
                    string fName = FileManager.GetRandomFileName() + ext;
                    if (FileManager.IsValidImageFileType(ext))
                    {
                        fName = UploadImage(fuUpload.PostedFile);
                    }
                    else 
                    {
                        fuUpload.SaveAs(Server.MapPath(txtPath.Text.Replace("//", "/")) + fName);
                    }
                    //Save File Info To Database Not Available Now
                    //if (!string.IsNullOrEmpty(txtFileDescription.Text.Trim()))
                    //{
                    //    string desc = txtFileDescription.Text;
                    //    desc = txtFileDescription.Text.Length > 150 ? desc.Substring(0, 150) : desc;
                    //    DateTime fileCreateDate = DateTime.Now;
                    //    desc += Environment.NewLine + Environment.NewLine + "<b>Created Date: " + fileCreateDate.ToString() + "<b>";
                    //    FileManager.SaveFileInfo(fName, fuUpload.FileName, desc, fileCreateDate);                         
                    //}
                    Session["_uploadMessageSuccess"] = "File has been uploaded successfully!";
                }     
            }
            catch (Exception ex)
            {
                Session["_uploadMessageError"] = ex.Message;//"Critical error occured during file uploading! Please check file size being uploaded, It should not exceed 4 MB.";               
            }
        }
        else
        {
            Session["_uploadMessageError"] = "Please select valid file!";
        }
        Response.Redirect(Request.RawUrl);
    }
}