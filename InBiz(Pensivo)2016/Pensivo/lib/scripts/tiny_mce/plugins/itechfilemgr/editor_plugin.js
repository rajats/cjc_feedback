/**
* iTECH image manager plugin for TinyMCE
*
* @author Hitendra
* @copyright Copyright iTECH Canada Inc. All rights reserved
*/

(function () {
    tinymce.create('tinymce.plugins.itechFileManager', {
        init: function (ed, url) {
            // Register commands
            ed.addCommand('mceItechFilesManager', function () {
                ed.windowManager.open({
                    file: url + '/itechfilemgr.aspx',
                    width: 700 + parseInt(ed.getLang('itechfilemgr.delta_width', 0)),
                    height: 507 + parseInt(ed.getLang('itechfilemgr.delta_height', 0)),
                    inline: true,
                    popup_css: false
                }, {
                    plugin_url: url
                });
            });

            // Register buttons
            ed.addButton('itechfilemgr', {
                title: 'File Manager',
                cmd: 'mceItechFilesManager',
                image: url + '/img/icon.png'
            });
        },

        getInfo: function () {
            return {
                longname: 'iTECH File Manager',
                author: 'Hitendra',
                authorurl: 'http://itechcanada.com/',
                infourl: 'http://itechcanada.com/',
                version: '1.0.0'
            };
        }
    });

    // Register plugin   
    tinymce.PluginManager.add('itechfilemgr', tinymce.plugins.itechFileManager);
})();