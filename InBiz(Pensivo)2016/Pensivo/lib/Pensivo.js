﻿// To show custom popup message
function ShowPensivoMessage(message) {
    var sMessage = '<p>' + message + '</p>';

    if (message.indexOf("<p>") > 0) {
        sMessage = message;
    }

    //$("#dvGlobalMessage").html(sMessage);
    $("#idMsgText").html(sMessage);
    $("#dvGlobalMessage").addClass("standalone");
    $("#dvMsgHeader").addClass("active");
    $("body").addClass("lockdown");
    $("html").addClass("lockdown");
    $("#dvSgMsg").addClass("flyin");

    $("#liPopupclose").click(function () {
        ClosePensivoMessage();
    });

    return false;
}

// Show Pensivo Message With Set Focus on Control on Popup Close
function ShowPensivoMessageWithFocusCtrl(message, ctrlIdToFocus) {
    var sMessage = '<p>' + message + '</p>';

    if (message.indexOf("<p>") > 0) {
        sMessage = message;
    }

    //$("#dvGlobalMessage").html(sMessage);
    $("#idMsgText").html(sMessage);
    $("#dvGlobalMessage").addClass("standalone");
    $("#dvMsgHeader").addClass("active");
    $("body").addClass("lockdown");
    $("html").addClass("lockdown");
    $("#dvSgMsg").addClass("flyin");

    $("#liPopupclose").click(function () {
        ClosePensivoMessage();
        $("#" + ctrlIdToFocus + "").focus();
    });

    return false;
}

// To show custom popup message with close url
function ShowPensivoMessage(message, surl) {
    if (surl != "" && surl != undefined) {
        $("#liPopupclose").click(function () {
            ClosePensivoMessage(surl);
        });
    }
    else {
        $("#liPopupclose").click(function () {
            ClosePensivoMessage();
        });
    }

    var sMessage = '<p>' + message + '</p>';

    if (message.indexOf("<p>") > 0) {
        sMessage = message;
    }

    //$("#dvGlobalMessage").html(sMessage);
    $("#idMsgText").html(sMessage);
    $("#dvGlobalMessage").addClass("standalone");
    $("#dvMsgHeader").addClass("active");
    $("body").addClass("lockdown");
    $("html").addClass("lockdown");
    $("#dvSgMsg").addClass("flyin");
    return false;
}

// To close custom popup message
function ClosePensivoMessage() {
    $("#dvGlobalMessage").html('');
    $("#idMsgText").html('');
    $("#dvMsgHeader").removeClass("active");
    $("body").removeClass("lockdown");
    $("html").removeClass("lockdown");
}

// To close custom popup message with close url
function ClosePensivoMessage(surl) {
    $("#dvGlobalMessage").html('');
    $("#idMsgText").html('');
    $("#dvMsgHeader").removeClass("active");
    $("body").removeClass("lockdown");
    $("html").removeClass("lockdown");
    if (surl != "" && surl != undefined) {
        window.location.href = surl;
    }
}

                                  
// To show custom confirmation popup message
function PensivoConfirmMessage( popupTitle, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink , CloseLink)
{
    $( ".TitleCss" ).html( popupTitle );
    $( ".MsgTextCss" ).html( messageText );
    $( ".OkCss" ).html( okButtonText );
    $( ".CancelCss" ).html( LaterCnclButtonText );

    var js = 'HideConfirmationDialog();'+ okButtonRedirectlink;    
    var okclick = new Function( js );
    $( "#hypOK" ).attr( 'onclick', '' ).click( okclick );

    var jsCancel =  LaterCnclButtonRedirectLink;
    var Cancelclick = new Function( jsCancel );

    $( "#hypCancel" ).attr( 'onclick', '' ).click( Cancelclick );
    if (LaterCnclButtonText == "") {
        $("#hypCancel").hide();
        $("#hypOK").addClass("round");
    }
    else {
        $("#hypCancel").show();
        $("#hypOK").removeClass("round");
    }

    var CloseLinkRedirect = CloseLink;
    var Closeclick = new Function( CloseLinkRedirect );
    $( "#clsLink" ).attr( 'onclick', '' ).click( Closeclick );
     
    $("#dvPensivoCnfrmDialog").addClass("active");
    $("body").addClass("lockdown");
    $("html").addClass("lockdown");
    $("#dvSgMsg").addClass("dvCnMsg");
}

// To close custom confirmation popup
function HideConfirmationDialog() {
    $("#dvPensivoCnfrmDialog").removeClass("active");
    $("body").removeClass("lockdown");
    $("html").removeClass("lockdown");
}

// To disable enter key on page
function disableEnterKey(e) {
    var numCharCode;
    var elTarget;
    var strType;

    if (!e) var e = window.event;

    if (e.keyCode) numCharCode = e.keyCode;
    else if (e.which) numCharCode = e.which;

    if (e.target) elTarget = e.target;
    else if (e.srcElement) elTarget = e.srcElement;

    if (elTarget.tagName.toLowerCase() == 'input') {

        strType = elTarget.getAttribute('type').toLowerCase();

        switch (strType) {
            case 'checkbox':
            case 'radio':
            case 'text':
            case 'button':
            case 'submit':

                if (numCharCode == 13) {
                    return false;
                }

                break;
        }
    }
    return true;
}

// To show dialog with call function on close
function ShowPensivoMessageCallFunctionOnClose(message, sFunctionOnClose) {
    if (sFunctionOnClose != "" && sFunctionOnClose != undefined) {
        var js = 'ClosePensivoMessage();' + sFunctionOnClose;
        var okclick = new Function(js);
        $("#liPopupclose").attr('onclick', '').click(okclick);
    }
    else {
        $("#liPopupclose").click(function () {
            ClosePensivoMessage();
        });
    }

    var sMessage = '<p>' + message + '</p>';

    if (message.indexOf("<p>") > 0) {
        sMessage = message;
    }

    //$("#dvGlobalMessage").html(sMessage);
    $("#idMsgText").html(sMessage);
    $("#dvGlobalMessage").addClass("standalone");
    $("#dvMsgHeader").addClass("active");
    $("body").addClass("lockdown");
    $("html").addClass("lockdown");
    $("#dvSgMsg").addClass("flyin");
    return false;
}

// To show attempts confirmation message
function PensivoAttemptsConfirmMessage(popupTitle, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, CloseLink, doNotAlertText, doNotAlertRedirect ) {
    $(".TitleCss").html(popupTitle);
    $(".MsgTextCss").html(messageText);
    $(".AlertMe").html(okButtonText);
    $(".NoThank").html(LaterCnclButtonText);
    $(".donotalert").html(doNotAlertText);
    

    //var js = 'HideAttemptConfirmationDialog();' + okButtonRedirectlink;
    var js = "return " + okButtonRedirectlink + ';';
    var okclick = new Function(js);
    $("#hypAlertMe").attr('onclick', '').click(okclick);
    

    var jsCancel = LaterCnclButtonRedirectLink;
    var Cancelclick = new Function(jsCancel);
    $("#hypNoThank").attr('onclick', '').click(Cancelclick);

    var jsDonotAlert = 'HideAttemptConfirmationDialog();' + doNotAlertRedirect;
    var DonotAlert = new Function(jsDonotAlert);

    $("#hypDonotAlert").attr('onclick', '').click(DonotAlert);

    var CloseLinkRedirect = CloseLink;
    var Closeclick = new Function(CloseLinkRedirect);
    $("#clsLinkAttmpt").attr('onclick', '').click(Closeclick);

    $("#dvPensivoAttempCnfrmDialog").addClass("active");
    $("body").addClass("lockdown");
    $("html").addClass("lockdown");
    $("#dvSgMsg").addClass("dvCnMsg");
}

// Hide Attempt Confirmation Dialog
function HideAttemptConfirmationDialog() {
    $("#dvPensivoAttempCnfrmDialog").removeClass("active");
    $("body").removeClass("lockdown");
    $("html").removeClass("lockdown");
}

// Show Pensivo Waiting Message
function ShowPensivoWaitingMessage(message) {
    var sMessage = '<p>' + message + '</p>';

    if (message.indexOf("<p>") > 0) {
        sMessage = message;
    }

    $("#dvLoadingMessage").html(sMessage);
    $("#dvLoadingMessage").addClass("standalone");
    $("#dvLoading").addClass("active");
    $("body").addClass("lockdown");
    $("html").addClass("lockdown");
    $("#dvLoadingSub").addClass("flyin");
    return false;
}

// Close Pensivo Waiting Message
function ClosePensivoWaitingMessage() {
    $("#dvLoadingMessage").html('');
    $("#dvLoading").removeClass("active");
    $("body").removeClass("lockdown");
    $("html").removeClass("lockdown");
}