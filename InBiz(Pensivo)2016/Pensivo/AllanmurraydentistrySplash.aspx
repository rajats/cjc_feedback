﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AllanmurraydentistrySplash.aspx.cs" Inherits="AllanmurraydentistrySplash" %>

<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="lt-ie9"><![endif]-->
<!--[if IE 9]><html class="lt-ie10 is-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    <meta http-equiv="cache-control" content="no-cache" />

    <title><%=Resources.Resource.lblAlMurrayDentistrySiteTitle %></title>
    <!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <% if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link rel="stylesheet" href="~/_themes/nav-canada/_styles/main.css" />
    <%} %>
</head>

<body class="splash-pg">
    <form id="frm" runat="server">
        <section class="splash-pg-content">
                        <img src="_themes/nav-canada/_images/nav-canada-logo-blue.png" class="branding-logo"  title="" alt="" />
            <h3>Welcome to the ALL ANMURRAY DENTISTRY Training Solution</h3>
            <h3>Bienvenue au portail de formation de ALL ANMURRAY DENTISTRY</h3>
            <div class="btngrp"> 
                <asp:Button  ID="btnEnglish" runat="server" class="btn large" Text="English" OnClick="btnEnglish_Click" />
                <asp:Button  ID="btnFrench" runat="server" class="btn large" Text="Français" OnClick="btnFrench_Click" />
<%--                <a href="public.aspx?lang=en" class="btn large"><%=Resources.Resource.lblEng %></a>
                <a href="public.aspx?lang=fr" class="btn large">Français</a>--%>
            </div>
        </section>
    </form>
</body>
</html>
