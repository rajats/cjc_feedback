﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AssignTrainingAdditionalAddUsers.aspx.cs" Inherits="AssignTrainingAdditionalAddUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div id="dvAdditionalUsers" class="boxset-box" runat="server">
                    <a id="hrfAdditionalUsers" runat="server" href="#" class="boxset-box-wrapper" onclick="SelectAdditionalUser();">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title" id="hAdditionalUsers" runat="server"    ></h4>
                        </div>
                    </a>
                </div>
                <div id="dvNoteAllDone" class="boxset-box" runat="server">
                    <a id="hrfNoteAllDone" runat="server" href="#" class="boxset-box-wrapper" onclick="AddUsersEvent()" ><%--is-disabled AccessDataDashBoard.aspx--%>
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblAssignTrainingNoteAllDone%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="HdnMessage" runat="server"   />
        <asp:HiddenField ID="hdnAssignByUserID" runat="server"   />
        <asp:HiddenField ID="hdnTrainingEvent" runat="server"   />
        <asp:HiddenField ID="hdnTrainingEventAccessUserID" runat="server"   />
        

    </section>
    <asp:Button ID="btnSelectAdditionalUser" runat="server" OnClick="btnSelectAdditionalUser_Click" />


    <script type="text/javascript">
        function SelectAdditionalUser() {
            $("#<%=btnSelectAdditionalUser.ClientID%>").trigger("click");
        }


        // To Show Confirmation Dialog to add user
        function AddUsersEvent() {
            var title = "<%=Resources.Resource.lblConfirmation%>";
                        var messageText = $("#<%=HdnMessage.ClientID%>").val();
            okButtonText = "OK";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "ConfirmRoleAddUserOk()";
            LaterCnclButtonRedirectLink = "ConfirmAddRoleUserCancel();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }
        
        // To Trigger Role Add User Ok Button
        function ConfirmRoleAddUserOk() {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "assignUserTrainingEvent";
            datatoPost.TrainingEnventID = $("#<%=hdnTrainingEvent.ClientID%>").val();
            datatoPost.UserID = $("#<%=hdnTrainingEventAccessUserID.ClientID%>").val();;
            datatoPost.AssignByUserID = $("#<%=hdnAssignByUserID.ClientID%>").val();
            $.post("AssignTrainingAdditionalAddUsers.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    window.location.href = "AssignTraining.aspx";
                }
                else {
                    ShowPensivoMessage("<%=Resources.Resource.msgAssignTrainingEventToUserFailure%>")
                }
            });
        }

        // To Trigger Role Add User Cancel Button
        function ConfirmAddRoleUserCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
        }
    </script>
</asp:Content>

