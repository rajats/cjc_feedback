﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ConfirmAnswer.aspx.cs" Inherits="ConfirmAnswer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <asp:Panel ID="cntPannel" runat="server" DefaultButton="BtnConfirmAnswer">
            <div class="wrapper width-med">
                <h1><%= Resources.Resource.lblResetPassword%>  </h1>
                <div class="boxed-content">
                    <div class="boxed-content-body">
                        <header class="form-section-header">
                            <h6><%= Resources.Resource.lblTypeAnswer%>  </h6>
                            <div id="dvLogInErrMsg" class="plms-alert invalid" runat="server" visible="false" onclick="HideMessage();">
                                <p class="last-child">
                                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                                </p>
                            </div>
                        </header>
                        <div class="plms-fieldset">
                            <label class="plms-label is-hidden" for="first-name"><%= Resources.Resource.lblSecretQuestion%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlQuestion" runat="server" class="plms-select skin2 is-placeholder  ">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblSecretQuestion%></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="plms-fieldset">
                            <label class="plms-label is-hidden" for="first-name"><%= Resources.Resource.lblAnswer%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtAnswer" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblAnswer %>"></asp:TextBox>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p>
                                            <asp:Label ID="lblAnswer" runat="server" Text="<%$ Resources:Resource, lblAnswer %>"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="form-footer">
                            <asp:Button ID="BtnConfirmAnswer" class="btn" runat="server" Text="<%$ Resources:Resource, btnNext %>" ValidationGroup="grpRole" OnClientClick="return BtnClick();" OnClick="BtnConfirmAnswer_OnClick" />
                        </footer>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">

        // Define To Show Contorls Custom Error message
        function BtnClick() {
            var val = Page_ClientValidate();
            if (!val) {
                var i = 0;
                for (; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $("#" + Page_Validators[i].controltovalidate).addClass("has-error");
                        break;
                    }
                    else {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            else {
                var i = 0;
                for (; i < Page_Validators.length; i++) {

                    {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            return val;
        }

        // To Hide Error Message
        function HideMessage() {
            $("#<%=dvLogInErrMsg.ClientID %>").hide();
        }

        $(document).ready(function () {
            $("#<%=txtAnswer.ClientID%>").focus();
                });
    </script>
</asp:Content>
