﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class AccessUserDataDetails : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAllowAccessReportType, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            string sTitle = Resources.Resource.lblOnlyCanSeeUserWithSame + " ";

            if ((this.SearchBy == EmpSearchBy.Store) || this.SearchBy == EmpSearchBy.SiteLogisticsReport)
            {
                sTitle += Resources.Resource.lblSiteNumber;
            }
            else if (this.SearchBy == EmpSearchBy.District)
            {
                sTitle += Resources.Resource.lblReportDistrict;
            }
            else if (this.SearchBy == EmpSearchBy.RegionTBS)
            {
                sTitle += Resources.Resource.lblReportRegion;
            }
            else if (this.SearchBy == EmpSearchBy.DivisionTBS)
            {
                sTitle += Resources.Resource.lblDivision;
            }
            else if (this.SearchBy == EmpSearchBy.Division)
            {
                sTitle += Resources.Resource.lblLocationType;
            }
            hTitle.InnerHtml = sTitle;
            hrfOnlyCanSeeSameInfo.Attributes.Add("onclick", "AllowAllSiteData();");

            Role objRole = new Role();
            string roleName = "";
            string functionalityName = "";
            if (this.RoleID > 0)
            {
                objRole = new Role();
                roleName = objRole.GetRoleName(this.RoleID);
            }


            if (this.SearchBy == EmpRefCode.Store)
            {
                functionalityName = Resources.Resource.lblSearchSite;
            }
            else if (this.SearchBy == EmpRefCode.Region)
            {
                functionalityName = Resources.Resource.lblSearchRegion;
            }
            else if (this.SearchBy == EmpRefCode.JobCode)
            {
                functionalityName = Resources.Resource.lblSearchJobCode;
            }
            else if (this.SearchBy == EmpRefCode.Division)
            {
                functionalityName = Resources.Resource.lblSearchDivision;
            }

            if (this.Flag == "add")
            {
                hdnMessage.Value = "Added functionality to the system role '" + roleName + "' : " + "to  following " + functionalityName + " users </br>" + this.SearchValue.Replace(",", "</br>") + sTitle;
            }
            else if (this.Flag == "remove")
            {
                hdnMessage.Value = "Removed functionality to the system role '" + roleName + "' : " + "to  following " + functionalityName + " users </br>" + this.SearchValue.Replace(",", "</br>");
            }
            HdnSearchBy.Value = BusinessUtility.GetString(this.SearchBy);
            HdnSearchValue.Value = BusinessUtility.GetString(this.SearchValue);
            HdnFlag.Value = BusinessUtility.GetString(this.Flag);
            hdnRoleID.Value = BusinessUtility.GetString(this.RoleID);
            HdnActionID.Value = BusinessUtility.GetString(this.ActionID);
            HdnFunctionalityType.Value = BusinessUtility.GetString(this.FunctionalityType);

            if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addAction")
            {
                try
                {
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                    string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                    string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                    string functionalityType = BusinessUtility.GetString(Request.Form["FunctionalityType"]);

                    int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                    if (searchBy != "" && roleID > 0 && actionID > 0)
                    {
                        objRole = new Role();
                        if (objRole.ActionExistsInRole(roleID, actionID, functionalityType) == false)
                        {
                            objRole.RoleAddFunctionality(roleID, actionID, functionalityType);
                        }

                        if (objRole.RoleAddReportType(roleID, searchBy) == true)
                        {
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }

                Response.End();
                Response.SuppressContent = true;
            }
            else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeAction")
            {
                try
                {
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                    string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                    string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                    int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                    if (searchBy != "" && roleID > 0 && searchValue != "" && actionID > 0)
                    {
                        objRole = new Role();
                        if (objRole.RemoveSysRefInReportUserManagment(roleID, searchBy, searchValue, actionID) == true)
                        {
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }

                Response.End();
                Response.SuppressContent = true;
            }
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search By Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Functionality Type
    /// </summary>
    public string FunctionalityType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["functionalityType"]);
        }
    }

}