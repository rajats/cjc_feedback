﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ReportFilterSummary.aspx.cs" Inherits="ReportFilterSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <h3>
                <asp:Literal ID="ltrTitle" runat="server" Text="Report Details"></asp:Literal></h3>
            <% =sHTML%>
            <br />
            <br />
            <br />
            <div class="btngrp " style="float: right;">
                <a id="hrefNo" runat="server" href="ReportNew.aspx" class="btn"><%=Resources.Resource.lblAddMoreFilterReport %></a>
                <a id="hrefYes" runat="server" href="#" class="btn"><%=Resources.Resource.lblRunThisReport %></a>&nbsp;
            </div>
        </div>
    </section>
</asp:Content>

