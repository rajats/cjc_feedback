﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class NewUser : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            Breadcrumbs.BreadcrumbsSetRootMenu();
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblMyProfile, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            GetEmpDetail();
        }

        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
        {
            dvEmail.Visible = false;
            dvjobcode.Visible = false;
            dvStore.Visible = false;
            dvDivission.Visible = false;
            dvType.Visible = false;
            if (Utils.TrainingInst == (int)Institute.EDE2)
            {
                lblEmpID.InnerHtml = Resources.Resource.lblEDE2UserName;
                txtEmpID.Attributes.Add("placeholder", Resources.Resource.lblEDE2UserName);
                pEmpID.InnerHtml = Resources.Resource.lblEDE2UserName;
            }
            else
            {
                lblEmpID.InnerHtml = Resources.Resource.lblEmail;
                txtEmpID.Attributes.Add("placeholder", Resources.Resource.lblEmail);
                pEmpID.InnerHtml = Resources.Resource.lblEmail;
            }
        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            lblEmpID.InnerHtml = Resources.Resource.lblBDLUserName;
            txtEmpID.Attributes.Add("placeholder", Resources.Resource.lblBDLUserName);
            pEmpID.InnerHtml = Resources.Resource.lblBDLUserName;
            dvProvince.Visible = true;
        }
        else
        {
            lblEmpID.InnerHtml = Resources.Resource.lblUserName;
            txtEmpID.Attributes.Add("placeholder", Resources.Resource.lblUserName);
            pEmpID.InnerHtml = Resources.Resource.lblUserName;
        }
    }

    /// <summary>
    /// To Update User Profile By User
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnCreateEmployee_OnClick(object sender, EventArgs e)
    {
        Employee obj = new Employee();
        obj.EmpFirstName = txtFirstName.Text;
        obj.EmpLastName = txtlastName.Text;
        obj.EmpEmail = txtEmail.Text;
        obj.EmpDateOfBirth = DateTime.Now;
        if (obj.UpdateEmployee(BusinessUtility.GetInt(CurrentEmployee.EmpID)) == true)
        {
            if (BusinessUtility.GetInt(hdnUUID.Value) > 0)
            {
                PensivoExecutions.PensivoPostData(txtFirstName.Text, txtlastName.Text, "", txtEmail.Text, BusinessUtility.GetInt(hdnUUID.Value));
            }

            showAlertMessage(Resources.Resource.lblProfileInformationSaved);
            GetEmpDetail();
        }
    }


    /// <summary>
    /// To Get User Details
    /// </summary>
    private void GetEmpDetail()
    {
        Employee objEmp = new Employee();
        ddlJobCode.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.JobCode);
        ddlJobCode.DataTextField = "sysRefCodeText";
        ddlJobCode.DataValueField = "sysRefCodeValue";
        ddlJobCode.DataBind();
        ddlJobCode.Items.Insert(0, new ListItem(Resources.Resource.lblJobCode, ""));

        ddlStore.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.Store);
        ddlStore.DataTextField = "sysRefCodeText";
        ddlStore.DataValueField = "sysRefCodeValue";
        ddlStore.DataBind();
        ddlStore.Items.Insert(0, new ListItem(Resources.Resource.lblStore, ""));

        ddlDivision.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.Division);
        ddlDivision.DataTextField = "sysRefCodeText";
        ddlDivision.DataValueField = "sysRefCodeValue";
        ddlDivision.DataBind();
        ddlDivision.Items.Insert(0, new ListItem(Resources.Resource.lblDivision, ""));

        ddlType.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.Type);
        ddlType.DataTextField = "sysRefCodeText";
        ddlType.DataValueField = "sysRefCodeValue";
        ddlType.DataBind();
        ddlType.Items.Insert(0, new ListItem(Resources.Resource.lblType, ""));


        ddlProvince.DataSource = objEmp.GetEmpSysCodes(EmpRefCode.Province);
        ddlProvince.DataTextField = "sysRefCodeText";
        ddlProvince.DataValueField = "sysRefCodeValue";
        ddlProvince.DataBind();
        ddlProvince.Items.Insert(0, new ListItem(Resources.Resource.lblProvince, ""));

        objEmp = new Employee();
        objEmp.GetEmployeeDetail(BusinessUtility.GetInt(CurrentEmployee.EmpID));
        txtFirstName.Text = BusinessUtility.GetString(objEmp.EmpFirstName);
        txtlastName.Text = BusinessUtility.GetString(objEmp.EmpLastName);
        txtEmail.Text = BusinessUtility.GetString(objEmp.EmpEmail);
        txtEmpID.Text = BusinessUtility.GetString(objEmp.EmpExtID);
        ddlJobCode.SelectedValue = BusinessUtility.GetString(objEmp.JobCode);
        ddlStore.SelectedValue = BusinessUtility.GetString(objEmp.Location);
        ddlDivision.SelectedValue = BusinessUtility.GetString(objEmp.Division);
        ddlType.SelectedValue = BusinessUtility.GetString(objEmp.Type);
        ddlProvince.SelectedValue = BusinessUtility.GetString(objEmp.Province);
        hdnUUID.Value=   BusinessUtility.GetString(objEmp.PensivoUUID);
    }

    /// <summary>
    /// To Show Message
    /// </summary>
    /// <param name="sMessage">Pass String as Message</param>
    private void showAlertMessage(string sMessage)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessages", " ShowPensivoMessage('" + sMessage + "');", true);
    }
}