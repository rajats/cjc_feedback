﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class RoleAndOrSelection : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblCondition, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            if ((BusinessUtility.GetString(Request.QueryString["searchby"]) == EmpSearchBy.Division) || (BusinessUtility.GetString(Request.QueryString["searchby"]) == EmpSearchBy.Type))
            {
                hrefAnd.HRef = "EmpOtherSearchDashBoard.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + BusinessUtility.GetString(Request.QueryString["searchby"]) + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption + "&operator=" + SysRoleSelOperator.AND;
                hrefOr.HRef = "EmpOtherSearchDashBoard.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + BusinessUtility.GetString(Request.QueryString["searchby"]) + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption + "&operator=" + SysRoleSelOperator.OR;
            }

            if ((BusinessUtility.GetString(Request.QueryString["searchby"]) == EmpSearchBy.JobCode) || (BusinessUtility.GetString(Request.QueryString["searchby"]) == EmpSearchBy.Store)
                || (BusinessUtility.GetString(Request.QueryString["searchby"]) == EmpSearchBy.District) || (BusinessUtility.GetString(Request.QueryString["searchby"]) == EmpSearchBy.Site)
                || (BusinessUtility.GetString(Request.QueryString["searchby"]) == EmpSearchBy.DivisionTBS) || (BusinessUtility.GetString(Request.QueryString["searchby"]) == EmpSearchBy.RegionTBS)
                || (BusinessUtility.GetString(Request.QueryString["searchby"]) == EmpSearchBy.Province)
                )
            {
                hrefAnd.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + BusinessUtility.GetString(Request.QueryString["searchby"]) + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption + "&operator=" + SysRoleSelOperator.AND;
                hrefOr.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + BusinessUtility.GetString(Request.QueryString["searchby"]) + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption + "&operator=" + SysRoleSelOperator.OR;
            }
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Report Option
    /// </summary>
    public string ReportOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }

}