﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class ReportProgress : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Reporting);

            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblWhatProgress, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            hrfAllTrainingProgress.Attributes.Add("onclick", "ShowReport('" + BusinessUtility.GetString(ReportEventProgressStatus.All_Course_Progress) + "')");
            hrfTrainingCompleteOnce.Attributes.Add("onclick", "ShowReport('" + BusinessUtility.GetString(ReportEventProgressStatus.Course_Once_Completed) + "')");
            hrfThoseNotCompletedTraining.Attributes.Add("onclick", "ShowReport('" + BusinessUtility.GetString(ReportEventProgressStatus.Only_Those_Not_Completed_Training) + "')");

            //hrfTrainingNotCompletedOnce.Attributes.Add("onclick", "ShowReport('" + BusinessUtility.GetString(ReportEventProgressStatus.Course_Not_Once_Completed) + "')");
            //hrfNotStartedAtOnce.Attributes.Add("onclick", "ShowReport('" + BusinessUtility.GetString(ReportEventProgressStatus.Course_Not_Once_Started) + "')");
        }
    }

    /// <summary>
    /// Create/Modify Report Filter Session Object and Move to Report Page
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnShowReport_Click(object sender, EventArgs e)
    {
        ReportFilter objReportFilter = new ReportFilter();
        List<ReportFilter> reportFilterList = objReportFilter.GetReportFilter().OrderBy(o => o.ReportOption).ToList();
        var resultItem = reportFilterList.Where(x => x.ReportOption == BusinessUtility.GetString(ReportOption.What)).ToList();
        if (resultItem != null && resultItem.Count > 0)
        {
            foreach (var removeItem in resultItem)
            {
                reportFilterList.Remove(removeItem);
            }
        }
        Session["ReportMultiFilter"] = reportFilterList;
        objReportFilter.CreateNewFilter(ReportOption.What, hdnReportType.Value, "", "0");

        reportFilterList = objReportFilter.GetReportFilter().OrderBy(o => o.ReportOption).ToList();
        resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.When.ToLower()).ToList();
        if (resultItem != null && resultItem.Count ==0)
        {
            objReportFilter.CreateNewFilter(ReportOption.When, ReportDateFilterOption.CurrentYearToDate, "", "0");
        }
        Response.Redirect("Report.aspx?HideTools=1");
    }
}