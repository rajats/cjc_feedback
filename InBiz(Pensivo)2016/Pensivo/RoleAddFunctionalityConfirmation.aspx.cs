﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class RoleAddFunctionalityConfirmation : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblConfirmation, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            string roleName = "";
            string functionalityName = "";
            if (this.RoleID > 0)
            {
                objRole = new Role();
                roleName = objRole.GetRoleName(this.RoleID);
            }

            if (this.FunctionalityID > 0)
            {
                Role objRole = new Role();
                objRole.GetActionDetail(this.FunctionalityID, Globals.CurrentAppLanguageCode);
                functionalityName = objRole.ActionName;
            }

            ltrTitle.Text = Resources.Resource.lblAddFunctionalityToSystemRole.Replace("#RoleName#", roleName).Replace("#ActionName#", functionalityName);
            hdnActionID.Value = BusinessUtility.GetString(this.FunctionalityID);
            hdnActionType.Value = BusinessUtility.GetString(this.FunctionalityType);
            hdnRoleID.Value = BusinessUtility.GetString(this.RoleID);
            hrefNo.HRef = "roleActivity.aspx?roleID=" + this.RoleID;

            if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addAction")
            {
                try
                {
                    int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                    string actionType = BusinessUtility.GetString(Request.Form["ActionType"]);
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                    if (actionID > 0 && roleID > 0)
                    {
                        objRole = new Role();
                        if (objRole.RoleAddFunctionality(roleID, actionID, actionType) == true)
                        {
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }
                Response.End();
                Response.SuppressContent = true;
            }
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Functionality ID
    /// </summary>
    public int FunctionalityID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["functionalityID"]);
        }
    }

    /// <summary>
    /// To Get Functionality Type
    /// </summary>
    public string FunctionalityType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["functionalityType"]);
        }
    }

}