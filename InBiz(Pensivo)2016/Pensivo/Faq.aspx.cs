﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTECH.Library.Utilities;
using iTECH.Pensivo.BusinessLogic;

public partial class Faq : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
        Breadcrumbs.BreadcrumbsSetRootMenu();
        currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblFaq, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

        if (Utils.TrainingInst == (int)Institute.navcanada || Utils.TrainingInst == (int)Institute.AlMurrayDentistry || Utils.TrainingInst == (int)Institute.EDE2)
        {
            liIETBS.Visible = false;
            liIENNAV.Visible = true;
        }
    }
}