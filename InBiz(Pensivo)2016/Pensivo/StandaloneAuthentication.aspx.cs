﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI.HtmlControls;


public partial class StandaloneAuthentication : System.Web.UI.Page
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Employee objEmp = new Employee();
            objEmp.GetEmployeeDetail(BusinessUtility.GetInt(CurrentEmployee.EmpID));

            uuid.Value = objEmp.PensivoUUID;
            string sServerPostURL = "";
            if (this.Source == (int)StandAloneAuthenticationDestination.beercollege)
            {
                AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Beer_College);
                destination.Value = StandAloneAuthenticationDestinationType.BeerCollege;
                sServerPostURL = Utils.BeerCollegeServerURL;
            }
            else if (this.Source == (int)StandAloneAuthenticationDestination.ai)
            {
                AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.AI_Reporting);
                destination.Value = StandAloneAuthenticationDestinationType.AI;
                sServerPostURL = Utils.AIServerURL;
                //Response.Redirect("~/AIReports/AIDashboard.aspx");
            }
            else if (this.Source == (int)StandAloneAuthenticationDestination.aidash)
            {
                AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.AI_Report_Initiation);
                destination.Value = StandAloneAuthenticationDestinationType.AiDashboard;
                sServerPostURL = Utils.AIDashboardServerURL;
            }
            else if (this.Source == (int)StandAloneAuthenticationDestination.storemgr)
            {
                AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Store_Manager);
                destination.Value = StandAloneAuthenticationDestinationType.StoreManager;
                sServerPostURL = Utils.StoreManagerServerURL;
            }
            else if (this.Source == (int)StandAloneAuthenticationDestination.ai2015)
            {
                AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.AI_2015);
                destination.Value = StandAloneAuthenticationDestinationType.AI2015;
                sServerPostURL = Utils.AI2015URL;
            }

            EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.TransferredToOldSystem,
                    BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), "", 0, (int)RusticRegStatusCode.None, "");

            string sRedirectPath = "";
            if (BusinessUtility.GetInt(CurrentEmployee.EmpID) > 0)
            {
                Role objRole = new Role();
                if ((objRole.GetUserHasFunctionality(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)RoleAction.Administration, "") == true) || (CurrentEmployee.EmpType == EmpType.SuperAdmin))
                {
                    sRedirectPath += Utils.GetSiteName() + "/AdministrationDashBoard.aspx?returnfromoldPIB=1";
                }
                else
                {
                    sRedirectPath += Utils.GetSiteName() + "/employeedashboard.aspx?returnfromoldPIB=1";
                }
            }

            thisForm.Action = sServerPostURL;
            backURL.Value = sRedirectPath;
            sessionTimeoutURL.Value = sRedirectPath;
            errorURL.Value = sRedirectPath;

            string source = objEmp.PensivoUUID + DateTime.Now.ToString("yyyy-MM-dd");
            using (MD5 md5Hash = MD5.Create())
            {
                key.Value = GetMd5Hash(md5Hash, source);
            }

            secret.Value = DateTime.Now.ToString("yyyy-MM-dd");
        }
    }

    /// <summary>
    /// To Get StandAlone Calling Source i.e. BeerCollage, AI Dashboard, Store Manager, AI
    /// </summary>
    private int Source
    {
        get { return BusinessUtility.GetInt(Request.QueryString["src"]); }
    }

    /// <summary>
    /// To Get StandAloneBackURL
    /// </summary>
    private string StandAloneBackURL
    {
        get { return BusinessUtility.GetString(AppConfig.GetAppConfigValue("StandAloneBackURL")); }
    }

    /// <summary>
    /// To Get StandAloneTimeOutURL
    /// </summary>
    private string StandAloneTimeOutURL
    {
        get { return BusinessUtility.GetString(AppConfig.GetAppConfigValue("StandAloneTimeOutURL")); }
    }

    /// <summary>
    /// To Get StandAloneErrorURL
    /// </summary>
    private string StandAloneErrorURL
    {
        get { return BusinessUtility.GetString(AppConfig.GetAppConfigValue("StandAloneErrorURL")); }
    }

    /// <summary>
    /// To Convert String to MD5 String
    /// </summary>
    /// <param name="md5Hash"></param>
    /// <param name="input"></param>
    /// <returns></returns>
    static string GetMd5Hash(MD5 md5Hash, string input)
    {

        // Convert the input string to a byte array and compute the hash. 
        byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

        // Create a new Stringbuilder to collect the bytes 
        // and create a string.
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data  
        // and format each one as a hexadecimal string. 
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        // Return the hexadecimal string. 
        return sBuilder.ToString();
    }

}