﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AssignTrainingEventByEmpCode.aspx.cs" Inherits="AssignTrainingEventByEmpCode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper width-med" style="max-width: 800px;">
            <h4>
                <asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:Resource, lblSearchByEmpName %>"></asp:Label></h4>
            <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearchByName">
                <div class="form-body" onclick="HideMessage()">
                    <div id="dvLogInErrMsg" class="plms-alert invalid" runat="server" visible="false">
                        <p class="last-child">Role Allready Exists</p>
                    </div>
                    <div class="plms-fieldset is-first">
                        <label class="plms-label is-hidden" for="txtSearchName" id="htmLblName" runat="server" ></label> <%--<%= Resources.Resource.lblAssignTraininEnventEmpNumber%>--%>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtSearchName" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblAssignTraininEnventEmpNumber %>" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p>
                                        <asp:Label ID="lblName" runat="server" Text="<%$ Resources:Resource, lblName %>"></asp:Label>
                                    </p>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfSearchName" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole"
                                ControlToValidate="txtSearchName" Text="<%$ Resources:Resource, reqName %>" />
                        </div>
                    </div>
                    <asp:Button ID="btnSearchByName" class="btn large btn-create-user " runat="server" Text="<%$ Resources:Resource, lbSearch %>" ValidationGroup="grpRole" OnClientClick="return BtnClick();" OnClick="btnSearchByName_OnClick" />
                </div>
            </asp:Panel>
        </div>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Define To Show Contorls Custom Error message
        function BtnClick() {
            var val = Page_ClientValidate();
            if (!val) {
                var i = 0;
                for (; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $("#" + Page_Validators[i].controltovalidate).addClass("has-error");
                        break;
                    }
                    else {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            else {
                var i = 0;
                for (; i < Page_Validators.length; i++) {

                    {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            return val;
        }

        // To Hide Message
        function HideMessage() {
            $("#<%=dvLogInErrMsg.ClientID %>").hide();
        }

        $(document).ready(function () {
            $("#<%=txtSearchName.ClientID%>").focus();
                });
    </script>
</asp:Content>

