﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class EmployeeShortDetail : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFirstName.Focus();
        }
    }

    /// <summary>
    /// To Hold Employee First Name and Last Name and move to Employee Question Page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearchByName_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("EmployeeQuestion.aspx" + Request.Url.Query + "&Fname=" + txtFirstName.Text + "&Lname=" + txtLastName.Text + "");
    }
}