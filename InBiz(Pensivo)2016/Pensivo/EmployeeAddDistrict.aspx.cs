﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Data;

public partial class EmployeeAddDistrict : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// To Create Role Manage List Class Object
    /// </summary>
    RoleManageLists objRoleManageList;


    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvSiteManager))
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Manage_List);
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSiteReportAddUser, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            HdnListID.Value = BusinessUtility.GetString(this.ListID);
            objRoleManageList = new RoleManageLists();
            gvSiteManager.Columns[1].HeaderText = Resources.Resource.lblSearchSiteDistrict;

            ltrSearchTitle.Text = Resources.Resource.lblFindDistricts;
            ltrSearchMessage.Text = Resources.Resource.lblFindDistrictMessage;
            lblRoleName.Text = Resources.Resource.lblSearchSiteDistrict;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblSearchSiteDistrict);


            Employee objEmp = new Employee();
            objEmp.GetEmployeeDetail(this.DistrictManagerID);
            ltrTitle.Text = Resources.Resource.lblSearchRegionalDirector + " - " + objEmp.EmpName;


            HdnReturnURL.Value = "EmployeeSiteReportingRegionalDirector.aspx?roleManageListID=" + this.RoleManageListID + "&roleManageListName=" + Server.UrlEncode(objRoleManageList.GetManageListName(this.ListID)) + "&SearchBy=" + this.SearchBy + "&FunctionalityID=" + BusinessUtility.GetString(this.FunctionalityID) + "&ListID=" + BusinessUtility.GetString(this.ListID) + "";

        }

        {
            if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addUser")
            {
                try
                {
                    int userID = BusinessUtility.GetInt(Request.Form["UserID"]);
                    int roleManageListID = BusinessUtility.GetInt(Request.Form["RoleManageListID"]);
                    int createdBy = BusinessUtility.GetInt(Request.Form["CreatedBy"]);
                    string siteID = BusinessUtility.GetString(Request.Form["SiteID"]);
                    string sysRefCode = BusinessUtility.GetString(Request.Form["SysRefCode"]);
                    int functionalityID = BusinessUtility.GetInt(Request.Form["FunctionalityID"]);
                    int listID = BusinessUtility.GetInt(Request.Form["ListID"]);
                    if (userID > 0 && roleManageListID > 0)
                    {
                        sysRefCode = EmpRefCode.District;
                        objRoleManageList = new RoleManageLists();
                        if (objRoleManageList.AddSiteEmployeeList(userID, roleManageListID, BusinessUtility.GetString(siteID), createdBy, sysRefCode, siteID, functionalityID, listID) == true)
                        {
                            objRoleManageList.UpdateEmployeeHasFunctionalityStatus(userID, BusinessUtility.GetString(siteID));
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }
                Response.End();
                Response.SuppressContent = true;
            }
        }
    }

    /// <summary>
    /// To Create JQ Grid Cell Binding
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Binding Event</param>
    protected void gvSiteManager_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 2)
        {
            string sDesc = string.Empty;
            Employee objEmp = new Employee();
            objEmp.GetEmployeeDetail(this.DistrictManagerID);

            objRole = new Role();
            string str = string.Empty;
            str += "<div class='marginleft'>";
            str += Resources.Resource.lblAddDistrictToRegionalDirectorConfirmation.Replace("#USERDETAIL#", objEmp.EmpName + " (" + objEmp.EmpExtID + ")").Replace("#SITEDETAIL#", e.CellHtml);
            str += "</div>";
            string sMessage = str.Replace("\"", "{-").Replace("'", "{_");
            e.CellHtml = string.Format(@"<a href=""javascript:;""  class='btn' style='float:none;' onclick=""AddUser({0},{1},'{2}','{3}','{4}','{5}')"">" + Resources.Resource.lblAdd + "</a>", BusinessUtility.GetString(this.DistrictManagerID), this.RoleManageListID, e.CellHtml, sMessage, this.SearchBy, BusinessUtility.GetString(this.FunctionalityID));
        }
    }

    /// <summary>
    /// To Bind JQ Grid with datasource to user can see site report
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event</param>
    protected void gvSiteManager_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        objRoleManageList = new RoleManageLists();
        string txtRole = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        gvSiteManager.DataSource = objRoleManageList.GetDestrictListNotAssociateToManager(Utils.ReplaceDBSpecialCharacter(txtRole), this.DistrictManagerID);
        gvSiteManager.DataBind();
    }


    /// <summary>
    /// To Get Role Manage List ID
    /// </summary>
    public int RoleManageListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleManageListID"]);
        }
    }


    public int DistrictManagerID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["empID"]);
        }
    }


    /// <summary>
    /// To Get Site ID
    /// </summary>
    public string SiteID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["siteID"]);
        }
    }


    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Functionality ID
    /// </summary>
    public int FunctionalityID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["FunctionalityID"]);
        }
    }


    /// <summary>
    /// To Get List ID
    /// </summary>
    public int ListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["ListID"]);
        }
    }
}