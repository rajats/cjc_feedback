﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class RoleTrainingEventConfirmation : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Event objEvent = new Event();
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAllowAccessReportTypeConfirmation, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            string roleName = "";
            if (this.RoleID > 0)
            {
                objRole = new Role();
                roleName = objRole.GetRoleName(this.RoleID);
            }

            string sTrainingEventName = "";
            string[] searchval = this.SearchValue.Split(',');
            foreach (string sValues in searchval)
            {
                objEvent = new Event();
                objEvent.GetEventDetail(BusinessUtility.GetInt(sValues), Globals.CurrentAppLanguageCode);

                if (sTrainingEventName == "")
                {
                    sTrainingEventName = objEvent.EventName;
                }
                else
                {
                    sTrainingEventName += ",<br/>"+ objEvent.EventName;
                }
            }

            ltrSubSummary.Text = sTrainingEventName;
            if (this.Flag == "add")
            {
                ltrTitle.Text = Resources.Resource.lblAssignTrainingEventConfirmationAllow.Replace("#ROLENAME#", roleName);
            }
            else if (this.Flag == "remove")
            {
                ltrTitle.Text = Resources.Resource.lblAssignTrainingEventConfirmationDisAllow.Replace("#ROLENAME#", roleName);
            }
            HdnSearchBy.Value = BusinessUtility.GetString(this.SearchBy);
            HdnSearchValue.Value = BusinessUtility.GetString(this.SearchValue);
            HdnFlag.Value = BusinessUtility.GetString(this.Flag);
            hdnRoleID.Value = BusinessUtility.GetString(this.RoleID);
            HdnActionID.Value = BusinessUtility.GetString(this.ActionID);
            HdnFunctionalityType.Value = BusinessUtility.GetString(this.FunctionalityType);
            HdnCreatedBy.Value = BusinessUtility.GetString(CurrentEmployee.EmpID);
            hrefNo.HRef = "RoleTrainingEvent.aspx?roleID=" + this.RoleID + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "";

            if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addAction")
            {
                try
                {
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                    string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                    string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                    string functionalityType = BusinessUtility.GetString(Request.Form["FunctionalityType"]);
                    int createdBy = BusinessUtility.GetInt(Request.Form["createdBy"]);

                    int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                    if (searchValue != "" && roleID > 0 && actionID > 0)
                    {
                        objRole = new Role();
                        if (objRole.ActionExistsInRole(roleID, actionID, functionalityType) == false)
                        {
                            objRole.RoleAddFunctionality(roleID, actionID, functionalityType);
                        }

                        if (objRole.RoleAssignTrainingEvent(roleID, searchValue, createdBy) == true)
                        {
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }
                Response.End();
                Response.SuppressContent = true;
            }
            else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeAction")
            {
                try
                {
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                    string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                    string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                    int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                    string functionalityType = BusinessUtility.GetString(Request.Form["FunctionalityType"]);
                    if ( roleID > 0 && searchValue != "" && actionID > 0)
                    {
                        objRole = new Role();
                        if (objRole.RemoveRoleAssignTrainingEvent(roleID, searchValue) == true)
                        {
                            objRole = new Role();
                            if (objRole.IsRoleAssignTrainingEvent(roleID) == false)
                            {
                                objRole.RoleRemoveFunctionality(roleID, actionID, functionalityType);
                            }
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }

                Response.End();
                Response.SuppressContent = true;
            }
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Functionality Type
    /// </summary>
    public string FunctionalityType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["functionalityType"]);
        }
    }
}