﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Text;
using System.IO;
using System.Configuration;
using EvoPdf;

// Use EVO PDF Namespace
using EvoPdf;



public partial class CertificateDownload : BasePage
{
    protected static string sHtml = "";
    protected void Page_Load(object sender, EventArgs e)
    {


        Employee objEmp = new Employee();
        //objEmp.GetEmployeeDetail(BusinessUtility.GetInt(objEmpCourseReg.empID));
        //int empID = objEmpCourseReg.empID;


        objEmp.EmpName = "Itech Test";

        string sFolderPath = this.CertificatePath;
        if (!(Directory.Exists(sFolderPath)))
        {
            Directory.CreateDirectory(sFolderPath);
        }

        string sDate = this.CertificatePath + @"/" + DateTime.Now.ToString("yyyy-MM-dd");
        if (!(Directory.Exists(sDate)))
        {
            Directory.CreateDirectory(sDate);
        }
        sFolderPath = sDate;

        string sFileName = "";//"Certificate_" + BusinessUtility.GetString(empID) + "_" + BusinessUtility.GetString(objEmpCourseReg.eventID) + "_" + BusinessUtility.GetString(objEmpCourseReg.eventVerNo) + BusinessUtility.GetString(DateTime.Now.ToString("yyyyMMddhhmmss")) + ".pdf";
        sFileName = "Policy_Review_BDL_CORE" + ".pdf";
        sFolderPath += @"/" + sFileName;
        string sTitle = "";//BusinessUtility.GetString(dt.Rows[0]["CourseTitle"]);
        string sCompletedDate = Convert.ToDateTime(DateTime.Now).ToString("MMMM dd, yyyy");
        string sScore = BusinessUtility.GetDecimal(BusinessUtility.GetString("50")).ToString("###.##");
        string printTemplet = "";
        string sEpochTime = "";

        TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
        int secondsSinceEpoch = (int)t.TotalSeconds;
        sEpochTime = BusinessUtility.GetString(secondsSinceEpoch);

        Event objEventDetail = new Event();
        objEventDetail.CertificateID = 35;
        sTitle = " Sample Certificate ";
        if (objEventDetail.CertificateID == 19)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyCORE.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 20)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyLDSX.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 21)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyLOG.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 22)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyLOGDS.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 23)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyLOGDSSAL.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 24)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyRDS.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 25)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyRET.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 26)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyRL.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 27)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyRLDS.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 28)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanyRLDSSAL.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 29)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewCompanySAL.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 30)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewDSCORE.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 31)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewDSPV.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 32)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewDSVA.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 33)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewDSVSU.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 34)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewBDLLDSX.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 35)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewBDLCORE.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }
        else if (objEventDetail.CertificateID == 36)
        {
            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NewPolicyReviewBDLCORESAL.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                .Replace("#COURSENAME#", sTitle)
                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                .Replace("#CERT_NO#", BusinessUtility.GetString("5") + "-" + sEpochTime)
                .Replace("#COMPLETEDATE#", sCompletedDate)
                .Replace("#LOGOPATH#", Utils.LogoPath)
                .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
                .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
                .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
                .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);
        }

 

        if (CreateCertificatePdf(printTemplet, sFolderPath) == true)
        {
            
                DownloadCertificate(sFolderPath, sFileName);
            
        }







        //string printTemplet;
        //printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/NavCanadaCourseCertificate.html"));
        //printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
        //    .Replace("#COURSENAME#", "dghds")
        //    .Replace("#EMPLYEENAME#", "shfdhsfdhdfh")
        //    .Replace("#CERT_NO#", "5")
        //    .Replace("#COMPLETEDATE#", "sdghasdhg")
        //    .Replace("#LOGOPATH#", Utils.LogoPath)
        //    .Replace("#TRAININGINSTTITLE#", Utils.TrainingInstTitle)
        //    .Replace("#TRAININGINSTLOGO#", Utils.TrainingInstLogo)
        //    .Replace("#TRAININGINSTWEBADD#", Utils.TrainingInstWebAdd)
        //    .Replace("#TRAININGINSTWEBADDTITLE#", Utils.TrainingInstWebAddTitle);


        //Response.Write(printTemplet);

        ////if (!IsPostBack)
        ////{
        ////    CertificateCreate objCertificate = new CertificateCreate();
        ////    objCertificate.DownloadCertificate(BusinessUtility.GetInt(Request.QueryString["Regid"]),true);
        ////    return;
        ////    sHtml = GetCompletedCourseList();
        ////}
    }


    private string GetCompletedCourseList()
    {
        EmployeeCourses obj = new EmployeeCourses();
        DataTable dt = new DataTable();



        //dt = obj.GetEmployeeHaveTakenCourses(CurrentEmployee.EmpID, Globals.CurrentAppLanguageCode);

        StringBuilder sbHtml = new StringBuilder();

        foreach (DataRow dRow in dt.Rows)
        {
            sbHtml.Append(" <li regID='" + BusinessUtility.GetString(dRow["idRegistration"]) + "'> ");
            sbHtml.Append(" <span class='plms-icon size-24 checkbox-unchecked '></span> ");
            sbHtml.Append(BusinessUtility.GetString(dRow["CourseTitle"]));
            sbHtml.Append(" </li> ");
        }

        return BusinessUtility.GetString(sbHtml);
    }


    protected void btnDonloadPDF_OnClick(object sender, EventArgs e)
    {

        string[] sRegID = hdnSelectedValue.Value.Split(',');
        foreach (string sReg in sRegID)
        {
            if (BusinessUtility.GetInt(sReg) > 0)
            {

                Certificate objCert = new Certificate();
                string sSaveFileName = objCert.GetCertificateFilePath(BusinessUtility.GetInt(sReg));

                if (sSaveFileName == "")
                {
                    EmployeeCourses obj = new EmployeeCourses();
                    DataTable dt = new DataTable();
                    obj.regID = BusinessUtility.GetInt(sReg);

                    int tYear = 0;
                    if (tYear <= 0)
                    {
                        tYear = DateTime.Now.Year;
                    }


                    dt = obj.GetEmployeeHaveTakenCourses(CurrentEmployee.EmpID, Globals.CurrentAppLanguageCode, tYear);

                    EmployeeCourseRegistration objEmpCourseReg = new EmployeeCourseRegistration();
                    objEmpCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(sReg));

                    if (dt.Rows.Count > 0)
                    {
                        Employee objEmp = new Employee();
                        objEmp.GetEmployeeDetail(BusinessUtility.GetInt(objEmpCourseReg.empID));
                        int empID = objEmpCourseReg.empID;

                        string sFolderPath = this.CertificatePath;//(Server.MapPath("~") + @"/Certificate");
                        if (!(Directory.Exists(sFolderPath)))
                        {
                            Directory.CreateDirectory(sFolderPath);
                        }

                        string sFileName = "Certificate_" + BusinessUtility.GetString(empID) + "_" + BusinessUtility.GetString(objEmpCourseReg.eventID) + "_" + BusinessUtility.GetString(objEmpCourseReg.eventVerNo) + BusinessUtility.GetString(DateTime.Now.ToString("yyyymmddhhmmss")) + ".pdf";
                        sFolderPath += @"/" + sFileName;

                        objCert = new Certificate();
                        objCert.certificatePath = @"/" + sFileName;
                        objCert.empID = empID;
                        objCert.eventID = objEmpCourseReg.eventID;
                        objCert.eventVerNo = objEmpCourseReg.eventVerNo;
                        objCert.regID = BusinessUtility.GetInt(sReg);
                        objCert.SaveCertificate();

                        string sTitle = BusinessUtility.GetString(dt.Rows[0]["CourseTitle"]);
                        string sCompletedDate = BusinessUtility.GetString(dt.Rows[0]["enddate"]);
                        string sScore = BusinessUtility.GetString(dt.Rows[0]["score"]);
                        string printTemplet = "";


                        Event objEventDetail = new Event();
                        objEventDetail.GetEventDetail(BusinessUtility.GetInt(objEmpCourseReg.eventID), Globals.CurrentAppLanguageCode);
                        if (objEventDetail.CertificateID == 1)
                        {
                            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/CourseCertificate.html"));
                            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                                .Replace("#COURSENAME#", sTitle)
                                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                                .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID))
                                .Replace("#COMPLETEDATE#", sCompletedDate);
                        }
                        else if (objEventDetail.CertificateID == 5)
                        {
                            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/CourseTestCertificate.html"));
                            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                                .Replace("#COURSENAME#", sTitle)
                                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                                .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID))
                                .Replace("#COMPLETEDATE#", sCompletedDate)
                                .Replace("#COURSEPERCENTAGE#", sScore);
                        }
                        else if (objEventDetail.CertificateID == 2 || objEventDetail.CertificateID == 3 || objEventDetail.CertificateID == 4)
                        {
                            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/CourseCertificateLogistics.html"));
                            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                                .Replace("#COURSENAME#", sTitle)
                                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                                .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID))
                                .Replace("#COMPLETEDATE#", sCompletedDate);
                        }
                        else
                        {
                            printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/CourseCertificate.html"));
                            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                                .Replace("#COURSENAME#", sTitle)
                                .Replace("#EMPLYEENAME#", objEmp.EmpName)
                                .Replace("#CERT_NO#", BusinessUtility.GetString(objCert.certificateID))
                                .Replace("#COMPLETEDATE#", sCompletedDate);
                        }

                        if (CreateCertificatePdf(printTemplet, sFolderPath) == true)
                        {
                            DownloadCertificate(sFolderPath, sFileName);
                        }
                        else
                        {
                            showAlertMessage();
                        }
                    }
                    else
                    {
                        showAlertMessage();
                    }
                }
                else
                {
                    string sFolderPath = this.CertificatePath;//(Server.MapPath("~") + @"/Certificate");
                    DownloadCertificate(sFolderPath + sSaveFileName, sSaveFileName.Replace("/", ""));
                }
            }
        }
    }


    public Boolean CreateCertificatePdf(string sHtml, string sFileName)
    {
        //try
        {
            System.Globalization.CultureInfo cultureUTC = new System.Globalization.CultureInfo("en-US");

            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();
            htmlToPdfConverter.LicenseKey = EVOlicense;
            //htmlToPdfConverter.HtmlViewerWidth = int.Parse("1100");
            //htmlToPdfConverter.HtmlViewerHeight = int.Parse("900");
            htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = SelectedPdfPageOrientation();
            //htmlToPdfConverter.PdfDocumentOptions.LeftMargin =   float.Parse("24.0");
            //htmlToPdfConverter.PdfDocumentOptions.RightMargin = float.Parse("24.0");
            //htmlToPdfConverter.PdfDocumentOptions.TopMargin = float.Parse("25.0");

            htmlToPdfConverter.HtmlViewerWidth = 1100;// int.Parse("1100");
            htmlToPdfConverter.HtmlViewerHeight = 900;// int.Parse("900");
            htmlToPdfConverter.PdfDocumentOptions.LeftMargin = BusinessUtility.GetFloat(BusinessUtility.GetDouble("24.0").ToString("N2", cultureUTC));
            htmlToPdfConverter.PdfDocumentOptions.RightMargin = BusinessUtility.GetFloat(BusinessUtility.GetDouble("24.0").ToString("N2", cultureUTC));
            htmlToPdfConverter.PdfDocumentOptions.TopMargin = BusinessUtility.GetFloat(BusinessUtility.GetDouble("25.0").ToString("N2", cultureUTC));
            htmlToPdfConverter.ConversionDelay = int.Parse("0");

            byte[] outPdfBuffer = null;

            string htmlString = sHtml;
            string baseUrl = "";

            outPdfBuffer = htmlToPdfConverter.ConvertHtml(htmlString, baseUrl);

            using (Stream file = File.OpenWrite(sFileName))
            {
                file.Write(outPdfBuffer, 0, outPdfBuffer.Length);
                file.Close();
            }

            return true;
        }
        //catch (Exception e)
        //{
        //    ErrorLog.createLog(e.Message);
        //    throw;
        //}
    }

    public void DownloadCertificate(string sFilePath, string fileName)
    {
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.ClearContent();
        response.Clear();
        response.ContentType = "application/pdf";
        response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ";");
        response.TransmitFile(sFilePath);
        response.Flush();
        response.End();
        response.Close();
    }

    public PdfPageOrientation SelectedPdfPageOrientation()
    {
        return PdfPageOrientation.Landscape;
    }


    private void showAlertMessage()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('" + Resources.Resource.lblUnableToDownloadPleaseContactAdministrator + "');", true);
    }


    /// <summary>
    /// To get evo pdf license
    /// </summary>
    public string EVOlicense
    {
        get
        {
            //return BusinessUtility.GetString(ConfigurationManager.AppSettings["EVOlicense"]);
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("EVOlicense"));
        }
    }

    /// <summary>
    /// To get certificate path file to be saved
    /// </summary>
    public string CertificatePath
    {
        get
        {
            //return BusinessUtility.GetString(ConfigurationManager.AppSettings["CertificateDiractory"]);
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("CertificateDiractory"));
        }
    }

}