﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Data;
using System.Globalization;

public partial class TrainingEventCreate : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        showMessge("");
        if (!IsPostBack)
        {
            GetTrainingEventDetail();
            if (this.EvenID > 0)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblModifyRoleTrainingEvent, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
                ltrTitle.Text = Resources.Resource.lblEditEvent;
                btnCreateEvent.Text = Resources.Resource.lblEditEvent;
            }
            else
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAddNewTrainingEvent, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                ltrTitle.Text = Resources.Resource.lblCreateEvent;
                btnCreateEvent.Text = Resources.Resource.lblCreateEvent;

                btnCreateVersion.Visible = false;
            }


            if (Utils.TrainingInst == (int)Institute.tdc)
            {
                lblExtCourseIDEnglish.InnerHtml = Resources.Resource.lblCourseExtID;
                txtCourseExtID.Attributes.Add("placeholder", Resources.Resource.lblCourseExtID);
                pExtCourseIDEnglish.InnerHtml = Resources.Resource.lblCourseExtID;
                rfCourseExtID.Text = Resources.Resource.msgReqExtID;

                dvCourseExtIDFrench.Visible = false;
                rfCourseExtIDFr.Enabled = false;
            }
            else if (Utils.TrainingInst == (int)Institute.navcanada || Utils.TrainingInst == (int)Institute.EDE2)
            {
                lblExtCourseIDEnglish.InnerHtml = Resources.Resource.lblCourseExtIDEnglish;
                txtCourseExtID.Attributes.Add("placeholder", Resources.Resource.lblCourseExtIDEnglish);
                pExtCourseIDEnglish.InnerHtml = Resources.Resource.lblCourseExtIDEnglish;
                rfCourseExtID.Text = Resources.Resource.msgReqExtIDEnglish;

                dvCourseExtIDFrench.Visible = true;
                rfCourseExtIDFr.Enabled = true;
            }
            else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)
            {
                lblExtCourseIDEnglish.InnerHtml = Resources.Resource.lblCourseExtIDEnglish;
                txtCourseExtID.Attributes.Add("placeholder", Resources.Resource.lblCourseExtIDEnglish);
                pExtCourseIDEnglish.InnerHtml = Resources.Resource.lblCourseExtIDEnglish;
                rfCourseExtID.Text = Resources.Resource.msgReqExtIDEnglish;

                dvCourseExtIDFrench.Visible = true;
                rfCourseExtIDFr.Enabled = true;
            }
            else if (Utils.TrainingInst == (int)Institute.bdl)
            {
                lblExtCourseIDEnglish.InnerHtml = Resources.Resource.lblCourseExtID;
                txtCourseExtID.Attributes.Add("placeholder", Resources.Resource.lblCourseExtID);
                pExtCourseIDEnglish.InnerHtml = Resources.Resource.lblCourseExtID;
                rfCourseExtID.Text = Resources.Resource.msgReqExtID;

                dvCourseExtIDFrench.Visible = false;
                rfCourseExtIDFr.Enabled = false;
            }
            else
            {
                lblExtCourseIDEnglish.InnerHtml = Resources.Resource.lblCourseExtID;
                txtCourseExtID.Attributes.Add("placeholder", Resources.Resource.lblCourseExtID);
                pExtCourseIDEnglish.InnerHtml = Resources.Resource.lblCourseExtID;
                rfCourseExtID.Text = Resources.Resource.msgReqExtID;

                dvCourseExtIDFrench.Visible = false;
                rfCourseExtIDFr.Enabled = false;
            }
        }
    }

    /// <summary>
    /// To Define Create Training Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnCreateEvent_OnClick(object sender, EventArgs e)
    {
        //if (Page.IsValid)
        {
            try
            {
                if (ddlCourseTestType.SelectedItem.Value == CourseTestType.CourseTest)
                {
                    if (txtTestExtID.Text != "")
                    {
                        Event obj = new Event();
                        obj = new Event();
                        if (obj.IsValidTestCourse(BusinessUtility.GetInt(txtTestExtID.Text)) == false)
                        {
                            showMessge(Resources.Resource.lblPleaseEnterValidCourseID);
                            txtTestExtID.Focus();
                            return;
                        }
                    }
                    else
                    {
                        showMessge(Resources.Resource.reqTestCourseID);
                        txtTestExtID.Focus();
                        return;
                    }
                }

                Event objEvent = new Event();
                //objEvent.CourseVerNo = BusinessUtility.GetInt(txtCourseVerNo.Text);
                objEvent.CourseVerNo = BusinessUtility.GetInt(ddlCourseVersion.SelectedItem.Value);
                objEvent.CourseTitleEn = txtTitleEnglish.Text;
                objEvent.CourseTitleFr = txtTitleFrench.Text;
                objEvent.CourseActive = rdCourseIsActive.Checked == true ? true : false;
                objEvent.CourseExtID = txtCourseExtID.Text;
                objEvent.CourseExtIDFr = txtCourseExtIDFr.Text;
                objEvent.CourseExtLink = txtCourseExtLnk.Text;
                objEvent.CourseQueNo = BusinessUtility.GetInt(txtCourseQuestionNo.Text);
                objEvent.CourseTestQueNo = BusinessUtility.GetInt(txtCourseTestQuestionNo.Text);
                objEvent.CourseCertOffered = rdCourseCertOfferedYes.Checked == true ? true : false;
                objEvent.IsReviewContent = rdCourseReviewContentYes.Checked == true ? true : false;
                objEvent.CourseType = BusinessUtility.GetString(ddlCourseType.SelectedItem.Value);
                objEvent.CoursePassingGrade = BusinessUtility.GetInt(txtCoursePassingGrade.Text);
                objEvent.Course_no_of_attempts = BusinessUtility.GetInt(txtCourseNoOfAttempts.Text);
                objEvent.Course_version_typ = txtCourseVersionType.Text;
                objEvent.Is_new = rdCourseIsNewYes.Checked == true ? true : false;
                objEvent.Is_updated = rdCourseIsUpdatedYes.Checked == true ? true : false;
                objEvent.Scorm_type = BusinessUtility.GetString(ddlCourseScromType.SelectedItem.Value);
                objEvent.CourseType = BusinessUtility.GetString(ddlCourseType.SelectedItem.Value);
                objEvent.Total_no_slides = BusinessUtility.GetInt(txtCourseNoOfSlide.Text);
                objEvent.Trn_Catg = txtCourseCatg.Text;
                objEvent.Trn_Summary = txtSummary.Text;
                objEvent.Trn_Preview = txtPreview.Text;
                objEvent.Trn_Sampletest = txtSampleTest.Text;
                objEvent.Trn_Articles = txtArticles.Text;
                objEvent.Trn_File_nm1 = txtFileName1.Text;
                objEvent.Trn_File_nm2 = txtFileName2.Text;
                objEvent.Trn_File_nm3 = txtFileName3.Text;
                objEvent.Trn_File_nm4 = txtFileName4.Text;
                objEvent.Trn_Video_link = txtVideoLink.Text;
                objEvent.CourseID = this.EvenID;
                objEvent.TestType = ddlCourseTestType.SelectedItem.Value;
                objEvent.TestExtID = txtTestExtID.Text;
                objEvent.TestCompletedCount = rdCountCompletedYes.Checked == true ? 1 : 0;


                EventDefaults objEventDefaults = new EventDefaults();


                //objEventDefaults.TrainingEventID 
                //objEventDefaults.TrainingEventVer = BusinessUtility.GetInt(txtCourseVerNo.Text);
                objEventDefaults.TrainingEventVer = BusinessUtility.GetInt(ddlCourseVersion.SelectedItem.Value);
                objEventDefaults.ReportingKeywords = txtReportingKeyWords.Text;
                objEventDefaults.MinimumPassScore = BusinessUtility.GetInt(txtMinimumPassScore.Text);
                objEventDefaults.MaximumPassScore = BusinessUtility.GetInt(txtMaximumPassScore.Text);
                objEventDefaults.AttemptResetType = ddlAttemptResetType.SelectedItem.Value;
                objEventDefaults.AttemptGrantWaitingTime = BusinessUtility.GetInt(txtAttemptGrantWaitingTime.Text);
                objEventDefaults.OrderSeqInDisplay = BusinessUtility.GetInt(txtOrderSeqInDisplay.Text);
                objEventDefaults.UserDisplayPriority = BusinessUtility.GetInt(txtUserDisplayPriority.Text);
                objEventDefaults.RepeatRequired = rdRepeatRequiredYes.Checked == true ? true : false;
                objEventDefaults.CourseStartDateTime = BusinessUtility.GetDateTime(txtCourseStartDate.Text, DateFormat.ddMMyyyy);

                if (rdRepeatRequiredYes.Checked == true)
                {
                    objEventDefaults.RepeatTriggerType = ddlRepeatTriggerType.SelectedItem.Value;
                    if (objEventDefaults.RepeatTriggerType == TrainingRepeatTriggerType.FixedDate)
                    {
                        objEventDefaults.FixedDay = BusinessUtility.GetInt(ddlFixedDay.SelectedItem.Value);
                        objEventDefaults.FixedMonth = BusinessUtility.GetInt(ddlFixedMonth.SelectedItem.Value);
                    }
                    else
                    {
                        objEventDefaults.RepeatInDays = BusinessUtility.GetInt(txtRepeatInDays.Text);
                    }
                }
                else
                {
                    objEventDefaults.RepeatTriggerType = ddlRepeatTriggerType.SelectedItem.Value;
                }

                objEventDefaults.AlertRequired = rdAlertRequiredYes.Checked == true ? true : false;
                if (rdAlertRequiredYes.Checked == true)
                {
                    objEventDefaults.AlertBeforeLaunch = rdBeforeLaunchYes.Checked == true ? true : false;
                    objEventDefaults.AlertNotCompleted = rdNotCompletedYes.Checked == true ? true : false;
                    objEventDefaults.AlertManagerNotCompleted = rdManagerNotCompletedYes.Checked == true ? true : false;
                    objEventDefaults.AlertRepeatInDays = BusinessUtility.GetInt(txtAlertInDaysRepeat.Text);

                    objEventDefaults.AlertInDays = BusinessUtility.GetInt(txtAlertInDays.Text);
                    objEventDefaults.AlertFormat = ddlAlertFormat.SelectedItem.Value;
                    objEventDefaults.AlertPortalMsg = txtAlertPortalMsg.Text;
                    objEventDefaults.AlertEmailMsg = txtAlertEmailMsg.Text;
                    objEventDefaults.AlertEscalationEmailMsg = txtAlertEscalationEmailMsg.Text;
                    objEventDefaults.AlertEmailSubject = txtAlertEmailSubject.Text;
                    objEventDefaults.AlertEscalationEmailSubject = txtAlertEmailSubject.Text;
                }
                objEventDefaults.AlertType = "U";//trainingeventdetail


                if (BusinessUtility.GetInt(ddlCertificateType.SelectedItem.Value) > 0)
                {
                    objEvent.CertificateID = BusinessUtility.GetInt(ddlCertificateType.SelectedItem.Value);
                }
                if (BusinessUtility.GetInt(objEvent.CourseID) > 0)
                {

                    if ((objEvent.UpdateEvent(BusinessUtility.GetInt(CurrentEmployee.EmpID)) == true))
                    {
                        objEventDefaults.TrainingEventID = BusinessUtility.GetInt(objEvent.CourseID);
                        objEventDefaults.SaveUpdateTrainingEventDefault();
                        Response.Redirect("AdministrationDashBoard.aspx", false);
                    }
                }
                else
                {
                    if (objEvent.CreateEvent(BusinessUtility.GetInt(CurrentEmployee.EmpID)) == true)
                    {
                        objEventDefaults.TrainingEventID = BusinessUtility.GetInt(objEvent.CourseID);
                        objEventDefaults.SaveUpdateTrainingEventDefault();

                        Response.Redirect("AdministrationDashBoard.aspx", false);
                    }
                }
            }
            catch (Exception err)
            {
                ErrorLog.createLog(err.ToString());
            }
        }
    }

    /// <summary>
    /// To Get Training Event Detail
    /// </summary>
    private void GetTrainingEventDetail()
    {
        Event objEvent = new Event();
        EventVersion objEventVersion = new EventVersion();

        ddlCourseType.DataSource = objEvent.GetCourseTypeList(Globals.CurrentAppLanguageCode);
        ddlCourseType.DataTextField = "TypeText";
        ddlCourseType.DataValueField = "TypeValue";
        ddlCourseType.DataBind();

        Certificate objCert = new Certificate();
        ddlCertificateType.DataSource = objCert.GetCertificateList(Globals.CurrentAppLanguageCode);
        ddlCertificateType.DataTextField = "certificateName";
        ddlCertificateType.DataValueField = "idCetificateType";
        ddlCertificateType.DataBind();


        //rdCourseIsNewNo.Checked = true;
        //rdCourseIsUpdatedNo.Checked = true;
        //rdRepeatRequiredNo.Checked = true;
        //rdAlertRequiredNo.Checked = true;
        //rdBeforeLaunchNo.Checked = true;
        //rdNotCompletedNo.Checked = true;
        //rdManagerNotCompletedNo.Checked = true;
        BindCourseVersion();

        if (this.EvenID > 0)
        {
            //dvCreateEventVersion.Visible = true;

            objEvent = new Event();
            objEvent.GetEventDetail(this.EvenID, Globals.CurrentAppLanguageCode);

            //txtCourseVerNo.Text = BusinessUtility.GetString(objEvent.CourseVerNo);
            //txtCourseVerNo.Text = BusinessUtility.GetString(objEventVersion.GetCourseCurrentVersion(this.EvenID));
            ddlCourseVersion.SelectedValue = BusinessUtility.GetString(objEvent.CourseVerNo);
            txtTitleEnglish.Text = objEvent.CourseTitleEn;
            txtTitleFrench.Text = objEvent.CourseTitleFr;
            if (objEvent.CourseActive == true)
            {
                rdCourseIsActive.Checked = true;
            }
            else
            {
                rdCourseIsInActive.Checked = true;
            }

            txtCourseExtID.Text = objEvent.CourseExtID;
            txtCourseExtIDFr.Text = objEvent.CourseExtIDFr;
            txtCourseExtLnk.Text = objEvent.CourseExtLink;
            txtCourseQuestionNo.Text = BusinessUtility.GetString(objEvent.CourseQueNo);
            txtCourseTestQuestionNo.Text = BusinessUtility.GetString(objEvent.CourseTestQueNo);
            if (objEvent.CourseCertOffered == true)
            {
                rdCourseCertOfferedYes.Checked = true;
            }
            else
            {
                rdCourseCertOfferedNo.Checked = true;
            }

            if (objEvent.IsReviewContent == true)
            {
                rdCourseReviewContentYes.Checked = true;
            }
            else
            {
                rdCourseReviewContentNo.Checked = true;
            }


            ddlCourseType.SelectedValue = objEvent.CourseType;
            txtCoursePassingGrade.Text = BusinessUtility.GetString(objEvent.CoursePassingGrade);
            txtCourseNoOfAttempts.Text = BusinessUtility.GetString(objEvent.Course_no_of_attempts);
            txtCourseVersionType.Text = objEvent.Course_version_typ;
            if (objEvent.Is_new == true)
            {
                rdCourseIsNewYes.Checked = true;
            }
            else
            {
                rdCourseIsNewNo.Checked = true;
            }

            if (objEvent.Is_updated == true)
            {
                rdCourseIsUpdatedYes.Checked = true;
            }
            else
            {
                rdCourseIsUpdatedNo.Checked = true;
            }

            ddlCourseScromType.SelectedValue = BusinessUtility.GetString(objEvent.Scorm_type);
            txtCourseNoOfSlide.Text = BusinessUtility.GetString(objEvent.Total_no_slides);
            txtCourseCatg.Text = objEvent.Trn_Catg;
            txtSummary.Text = objEvent.Trn_Summary;
            txtPreview.Text = objEvent.Trn_Preview;
            txtSampleTest.Text = objEvent.Trn_Sampletest;
            txtArticles.Text = objEvent.Trn_Articles;
            txtFileName1.Text = objEvent.Trn_File_nm1;
            txtFileName2.Text = objEvent.Trn_File_nm2;
            txtFileName3.Text = objEvent.Trn_File_nm3;
            txtFileName4.Text = objEvent.Trn_File_nm4;
            txtVideoLink.Text = objEvent.Trn_Video_link;
            ddlCourseTestType.SelectedValue = objEvent.TestType;
            ddlCertificateType.SelectedValue = BusinessUtility.GetString(objEvent.CertificateID);
            txtTestExtID.Text = objEvent.TestExtID;

            if (objEvent.TestCompletedCount == 1)
            {
                rdCountCompletedYes.Checked = true;
            }
            else
            {
                rdCountCompletedNo.Checked = true;
            }

            EventDefaults objEventDefaults = new EventDefaults();
            objEventDefaults.GetTrainingEventDefault(this.EvenID, Globals.CurrentAppLanguageCode);

            //txtCourseVerNo.Text = BusinessUtility.GetString(objEventDefaults.TrainingEventVer);
            txtReportingKeyWords.Text = objEventDefaults.ReportingKeywords;
            txtMinimumPassScore.Text = BusinessUtility.GetString(objEventDefaults.MinimumPassScore);
            txtMaximumPassScore.Text = BusinessUtility.GetString(objEventDefaults.MaximumPassScore);
            ddlAttemptResetType.SelectedValue = BusinessUtility.GetString(objEventDefaults.AttemptResetType);
            txtAttemptGrantWaitingTime.Text = BusinessUtility.GetString(objEventDefaults.AttemptGrantWaitingTime);
            txtOrderSeqInDisplay.Text = BusinessUtility.GetString(objEventDefaults.OrderSeqInDisplay);
            txtUserDisplayPriority.Text = BusinessUtility.GetString(objEventDefaults.UserDisplayPriority);
            txtCourseStartDate.Text = Convert.ToDateTime(objEventDefaults.CourseStartDateTime).ToString("dd/MM/yyyy",  CultureInfo.InvariantCulture);

            if (txtCourseStartDate.Text == "01/01/0001")
            {
                txtCourseStartDate.Text = "";
            }

            if (objEventDefaults.RepeatRequired)
            {
                hdnShowRepeat.Value = "1";
                rdRepeatRequiredYes.Checked = true;

                ddlRepeatTriggerType.SelectedValue = objEventDefaults.RepeatTriggerType;

                if (objEventDefaults.RepeatTriggerType == TrainingRepeatTriggerType.FixedDate)
                {
                    ddlFixedDay.SelectedValue = BusinessUtility.GetString(objEventDefaults.FixedDay);
                    ddlFixedMonth.SelectedValue = BusinessUtility.GetString(objEventDefaults.FixedMonth);
                }
                else
                {
                    txtRepeatInDays.Text = BusinessUtility.GetString(objEventDefaults.RepeatInDays);
                }
            }
            else
            {
                rdRepeatRequiredNo.Checked = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "repeatRequired", "HideRepeatDetail();", true);
            }


            if (objEventDefaults.AlertRequired)
            {
                hdnShowAlert.Value = "1";
                rdAlertRequiredYes.Checked = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertRequired", "ShowAlertDetail();", true);

                if (objEventDefaults.AlertBeforeLaunch == true)
                {
                    rdBeforeLaunchYes.Checked = true;
                }
                else
                {
                    rdBeforeLaunchNo.Checked = true;
                }


                if (objEventDefaults.AlertNotCompleted == true)
                {
                    rdNotCompletedYes.Checked = true;
                }
                else
                {
                    rdNotCompletedNo.Checked = true;
                }

                if (objEventDefaults.AlertManagerNotCompleted == true)
                {
                    rdManagerNotCompletedYes.Checked = true;
                }
                else
                {
                    rdManagerNotCompletedNo.Checked = true;
                }

                txtAlertInDaysRepeat.Text = BusinessUtility.GetString(objEventDefaults.AlertRepeatInDays);
                txtAlertInDays.Text = BusinessUtility.GetString(objEventDefaults.AlertInDays);
                ddlAlertFormat.SelectedValue = BusinessUtility.GetString(objEventDefaults.AlertFormat);
                txtAlertPortalMsg.Text = BusinessUtility.GetString(objEventDefaults.AlertPortalMsg);
                txtAlertEmailMsg.Text = BusinessUtility.GetString(objEventDefaults.AlertEmailMsg);
                txtAlertEscalationEmailMsg.Text = BusinessUtility.GetString(objEventDefaults.AlertEscalationEmailMsg);
                txtAlertEmailSubject.Text = BusinessUtility.GetString(objEventDefaults.AlertEmailSubject);
                txtAlertEscalationEmailSubject.Text = BusinessUtility.GetString(objEventDefaults.AlertEscalationEmailSubject);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertRequired", "HideAlertDetail();", true);
                rdAlertRequiredNo.Checked = true;
            }
        }
        else
        {
            //txtCourseVerNo.Text = BusinessUtility.GetString(objEventVersion.GetCourseCurrentVersion(this.EvenID));
            ddlCourseVersion.SelectedValue = BusinessUtility.GetString("0");
        }
    }

    /// <summary>
    /// To Get Training Event ID
    /// </summary>
    public int EvenID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["eventID"]);
        }
    }

    /// <summary>
    /// To Show Message
    /// </summary>
    /// <param name="message">Pass Message String</param>
    public void showMessge(string message)
    {
        if (message != "")
        {
            ltrErrMsg.Text = message;
            ltrErrMsg.Visible = true;
        }
        else
        {
            ltrErrMsg.Visible = false;
        }
    }

    /// <summary>
    /// To Create Training Event Version
    /// </summary>
    /// <param name="sender">Pass Sender Object </param>
    /// <param name="e">Pass Event Args</param>
    protected void btnCreateVersion_Click(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        {
            EventVersion objEventVersion = new EventVersion();
            objEventVersion.CreateEventVersion(this.EvenID, BusinessUtility.GetInt(CurrentEmployee.EmpID));
            BindCourseVersion();
            int courseCurrentVersion = BusinessUtility.GetInt(objEventVersion.GetCourseCurrentVersion(this.EvenID));
            ddlCourseVersion.SelectedValue = BusinessUtility.GetString(courseCurrentVersion);
            txtTitleEnglish.Focus();


            //if (this.EvenID > 0)
            //{
            //    Event objEvent = new Event();
            //    objEvent.UpdatEventVersion(this.EvenID, courseCurrentVersion);
            //}
        }
    }

    /// <summary>
    /// To Bind Course Version List
    /// </summary>
    private void BindCourseVersion()
    {
        EventVersion objEventVersion = new EventVersion();
        DataTable dtEventVersion = objEventVersion.GetCourseVersionList(this.EvenID);

        if (dtEventVersion.Rows.Count <= 0)
        {
            DataRow drNewVerwsion = dtEventVersion.NewRow();
            drNewVerwsion["trainingEventVer"] = 0;
            dtEventVersion.Rows.Add(drNewVerwsion);
            dtEventVersion.AcceptChanges();
        }

        ddlCourseVersion.DataSource = dtEventVersion;
        ddlCourseVersion.DataTextField = "trainingEventVer";
        ddlCourseVersion.DataValueField = "trainingEventVer";
        ddlCourseVersion.DataBind();
    }



}