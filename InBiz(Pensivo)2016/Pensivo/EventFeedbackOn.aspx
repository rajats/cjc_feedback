﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EventFeedbackOn.aspx.cs" Inherits="EventFeedbackOn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper width-med">
            <h4>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h4>
            <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnCreateRole">
                <div class="form-body" onclick="HideMessage()">
                    <div id="dvLogInErrMsg" class="plms-alert invalid" runat="server" visible="false">
                        <p class="last-child"><%=Resources.Resource.errMesageRoleExists %></p>
                    </div>
                    <div class="plms-fieldset is-first">
                        <label class="plms-label is-hidden" for="txtRoleName"><%= Resources.Resource.lblFeedbackEventName%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtRoleName" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, txtFeedbackEventName %>" MaxLength="135" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblFeedbackEventName%></p>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfRoleName" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole"
                                ControlToValidate="txtRoleName" Text="<%$ Resources:Resource, lblEventNameReq %>" />
                        </div>
                    </div>

                    <div class="plms-fieldset ">
                        <label class="plms-label" for="txtCourseStartDate"><%= Resources.Resource.lblFeedbackEventDate%></label>
                        <div class="plms-tooltip-parent">
                            <asp:TextBox ID="txtCourseStartDate" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, txtFeedbackEventDate %>" />
                            <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                <div class="plms-tooltip-body">
                                    <p><%= Resources.Resource.lblFeedbackEventDate%></p>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfUserDisplayPriority" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic" SetFocusOnError="true"
                                ControlToValidate="txtCourseStartDate" Text="<%$ Resources:Resource, lblRequiredDate %>" />
                            <asp:CustomValidator ID="rfValidateStart" runat="server" ControlToValidate="txtCourseStartDate" ErrorMessage="<%$ Resources:Resource, lblInvalidDateFormat %>" class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="ValidateDate" />
                        </div>
                    </div>
                    <%--<asp:Button ID="btnCreateRole" class="btn large btn-create-user " runat="server" ValidationGroup="grpRole" OnClientClick="return BtnClick();" OnClick="btnCreateRole_OnClick" />--%>
                </div>
                <br /><br /><br /><br /><br /><br /><br />
                <div class="btngrp " style="float: right;">
                    <asp:Button ID="btnCreateRole" class="btn  btn-create-user " runat="server" ValidationGroup="grpRole" OnClientClick="return BtnClick();" OnClick="btnCreateRole_OnClick" Text="<%$ Resources:Resource, lblOk %>" />
                    <asp:Button ID="btnCancel" class="btn  btn-create-user " runat="server" OnClick="btnCancel_Click" Text="<%$ Resources:Resource, btnCancel %>" />
                </div>
            </asp:Panel>
            <%--Define JavaScript Function--%>
            <script type="text/javascript">
                // To Validate Page
                function BtnClick() {
                    var val = Page_ClientValidate();
                    if (!val) {
                        var i = 0;
                        for (; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $("#" + Page_Validators[i].controltovalidate).addClass("has-error");
                                break;
                            }
                            else {
                                $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                            }
                        }
                    }
                    else {
                        var i = 0;
                        for (; i < Page_Validators.length; i++) {

                            {
                                $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                            }
                        }
                    }
                    return val;
                }

                // To Hide Message
                function HideMessage() {
                    $("#<%=dvLogInErrMsg.ClientID %>").hide();
                }

                function ValidateDate(sender, args) {
                    var dateString = document.getElementById(sender.controltovalidate).value;
                    var regex = /((0[1-9]|1[0-2])-((0)[1-9]|(1)[0-9]|2[0-9]|3[0-1])-((19|20)\d\d))$/;
                    //var regex = /(((19|20)\d\d)-(0[1-9]|1[0-2])-((0)[1-9]|(1)[0-9]|2[0-9]|3[0-1]))$/;
                    if (regex.test(dateString)) {
                        var parts = dateString.split("-");
                        var dt = new Date(parts[0] + "/" + parts[1] + "/" + parts[2]);
                        args.IsValid = (dt.getDate() == parts[1] && dt.getMonth() + 1 == parts[0] && dt.getFullYear() == parts[2]);
                    } else {
                        args.IsValid = false;
                    }
                }

                $(document).ready(function () {
                    $("#<%=txtRoleName.ClientID%>").focus();
                });
            </script>

        </div>
    </section>
</asp:Content>
