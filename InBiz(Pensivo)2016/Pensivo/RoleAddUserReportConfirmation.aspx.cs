﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class RoleAddUserReportConfirmation : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string roleName = "";
            string functionalityName = "";
            if (this.RoleID > 0)
            {
                objRole = new Role();
                roleName = objRole.GetRoleName(this.RoleID);
            }

            if (this.ActionID > 0)
            {
                Role objRole = new Role();
                objRole.GetActionDetail(this.ActionID, Globals.CurrentAppLanguageCode);
                functionalityName = objRole.ActionName;
            }

            ltrTitle.Text = Resources.Resource.lblAddFunctionalityToSystemRole.Replace("#RoleName#", roleName).Replace("#ActionName#", functionalityName);
            hdnActionID.Value = BusinessUtility.GetString(this.ActionID);
            hdnUserID.Value = BusinessUtility.GetString(this.UserID);
            hdnRoleID.Value = BusinessUtility.GetString(this.RoleID);
            hrefNo.HRef = "roleActivity.aspx?roleID=" + this.RoleID;

            if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addAction")
            {
                try
                {
                    int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                    int userID = BusinessUtility.GetInt(Request.Form["UserID"]);
                    if (actionID > 0 && roleID > 0)
                    {
                        objRole = new Role();
                        if (objRole.AddUsersInReportUserManagment(roleID, userID, actionID) == true)
                        {
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }

                }
                catch
                {
                    Response.Write("error");
                }

                Response.End();
                Response.SuppressContent = true;
            }
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get User ID
    /// </summary>
    public int UserID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["userID"]);
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

}