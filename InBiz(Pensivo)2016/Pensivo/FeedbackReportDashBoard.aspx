﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="FeedbackReportDashBoard.aspx.cs" Inherits="ReportDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">

                <div class="boxset-box">
                    <a href="FeedbackReport.aspx" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title">Get Feedback Report</h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" style="display:none;">
                    <a href="ReportRecentHistory.aspx" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblRecentTrainingReports%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

