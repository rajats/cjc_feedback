﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class EmployeeQuestionOther : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Employee objEmp = new Employee();
            ddlQuestion.DataSource = objEmp.GetEmployeeQuestion(Globals.CurrentAppLanguageCode);
            ddlQuestion.DataTextField = "question";
            ddlQuestion.DataValueField = "idemployeequestions";
            ddlQuestion.DataBind();
            ddlQuestion.Items.Insert(0, new ListItem(Resources.Resource.lblPleaseSelectSecurityQuestion, ""));

            if (BusinessUtility.GetInt(Request.QueryString["QID"]) > 0)
            {
                ddlQuestion.Items.Remove(ddlQuestion.Items.FindByValue(Request.QueryString["QID"]));
            }
            ddlQuestion.Focus();
        }
    }

    /// <summary>
    /// To Hold Question Answer and Move to Next Page
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnNext_OnClick(object sender, EventArgs e)
    {
        if (Request.QueryString["askQuestion"] == "1")
        {
            //Employee objEmp = new Employee();
            //if ((objEmp.InsertEmployeeAnswer(CurrentEmployee.EmpID, BusinessUtility.GetInt(Request.QueryString["QID"]), BusinessUtility.GetString(Request.QueryString["QAns"])) == true) && (objEmp.InsertEmployeeAnswer(CurrentEmployee.EmpID, BusinessUtility.GetInt(ddlQuestion.SelectedItem.Value), txtAnswer.Text) == true))
            //{
            //    Response.Redirect("ChangePassword.aspx?uid=" + CurrentEmployee.EmpID);
            //}
            Response.Redirect("ChangePassword.aspx" + Request.Url.Query + "&uid=" + CurrentEmployee.EmpID + "&QOtherID=" + BusinessUtility.GetInt(ddlQuestion.SelectedItem.Value) + "&QotherAns=" + txtAnswer.Text + "");
        }
        else
        {
            Response.Redirect("EmployeePass.aspx" + Request.Url.Query + "&QOtherID=" + BusinessUtility.GetInt(ddlQuestion.SelectedItem.Value) + "&QotherAns=" + txtAnswer.Text + "");
        }
    }
}