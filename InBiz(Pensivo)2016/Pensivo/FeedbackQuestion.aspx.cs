﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections;

public partial class FeedbackQuestion : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvRoles))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblFeedbackQuestion, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
        }
    }

    /// <summary>
    /// To Define JQ Grid Cell Binding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event</param>
    protected void gvRoles_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        Int64 iRoleID = BusinessUtility.GetInt(e.RowKey);
        string iEventName = BusinessUtility.GetString(e.RowValues[2]);
        if (e.ColumnIndex == 3)
        {
            e.CellHtml = string.Format(@"<a class='btn' style='float:none;font-size:9px;'   onclick=""EditRole('{0}','{1}')"">" + Resources.Resource.lblSelect + "</a>", iRoleID, iEventName);
        }
    }

    /// <summary>
    /// To Bind JQ Grid with DataSource
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event </param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtName = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        Event objRole = new Event();
        objRole.EventName = Utils.ReplaceDBSpecialCharacter(txtName);
        objRole.TestType = CourseTestType.TestOnly;
        gvRoles.DataSource = objRole.GetEventListForFeedbackChooseQuestionnaire(Globals.CurrentAppLanguageCode);
        gvRoles.DataBind();
    }



    /// <summary>
    /// To Create Web Service To Get Role Editable or Not
    /// </summary>
    /// <returns>string</returns>
    [System.Web.Services.WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string IsRoleLocked(int roleID)
    {
        int isLockRole = 0;
        try
        {
            Role objRole = new Role();
            if (objRole.IsRoleLocked(roleID) == true)
            {
                isLockRole = 1;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.CreateLog(ex);
        }
        return BusinessUtility.GetString(isLockRole);
    }

    protected void btnFeedbackQuestion_Click(object sender, EventArgs e)
    {
        int eventid = BusinessUtility.GetInt(hdnEventID.Value);
        string eventnm = BusinessUtility.GetString(hdnEventName.Value);
        Feedback objFeedback = (Feedback)Session["FeedbackDetails"];
        objFeedback.TrainingEventID = BusinessUtility.GetString(eventid);
        objFeedback.TrainingEventName = BusinessUtility.GetString(eventnm);
        Session["FeedbackDetails"] = objFeedback;
        Response.Redirect("FeedbackUser.aspx");
    }
}