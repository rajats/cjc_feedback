﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class EmpSearchDashBoard : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
        {
            dvSite.Visible = false;
            dvTypeDivJobCode.Visible = false;
            dvTypeRegDistDiv.Visible = false;

            hEmpCode.InnerHtml = Resources.Resource.lblEmployeeEmail;
            hSite.InnerHtml = Resources.Resource.lblSearchSite;
        }
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            dvBDLSITE.Visible = true;
            hEmpCode.InnerHtml = Resources.Resource.lblSearchEmployeeCode;
            hSite.InnerHtml = Resources.Resource.lblDeptID;
        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            dvBDLSITE.Visible = true;
            hEmpCode.InnerHtml = Resources.Resource.lblSearchEmployeeCode;
            hSite.InnerHtml = Resources.Resource.lblDeptID;
        }
        else
        {
            hEmpCode.InnerHtml = Resources.Resource.lblSearchEmployeeCode;
            hSite.InnerHtml = Resources.Resource.lblSearchSite;
        }

        if (!IsPostBack)
        {
            if (this.ReportOption != "")
            {
                AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblWho, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                hrfSearchEmpName.HRef = "EmployeeSearchByNameGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Name + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                hrfSearchEmpCode.HRef = "EmployeeSearchByName.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.EmpCode + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
            }
            else if (this.isManageListEmpSearch)
            {
                AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Manage_List);
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblEmployeeSearchDashBoard, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                hrfSearchEmpName.HRef = "EmployeeSearchByName.aspx?managelistempsearch=" + "1" + "&searchby=" + EmpSearchBy.Name + "&siteID=" + this.ManageListSiteID + "&roleManageListID=" + this.RoleManageListID + "&actionID=" + this.ActionID + "&ListID=" + this.ListID;
                hrfSearchEmpCode.HRef = "EmployeeSearchByName.aspx?managelistempsearch=" + "1" + "&searchby=" + EmpSearchBy.EmpCode + "&siteID=" + this.ManageListSiteID + "&roleManageListID=" + this.RoleManageListID + "&actionID=" + this.ActionID + "&ListID=" + this.ListID;
                dvSite.Visible = false;
                dvTypeDivJobCode.Visible = false;
                dvSearchEmpDivision.Visible = false;
                dvSearchEmpJobCode.Visible = false;
                dvTypeRegDistDiv.Visible = false;
                dvSearchEmpTBSREG.Visible = false;
                dvSearchEmpTBSDIV.Visible = false;
                dvBDLSITE.Visible = false;
            }
            else
            {
                hrfSearchEmpName.HRef = "EmployeeSearchByName.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Name + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                hrfSearchEmpCode.HRef = "EmployeeSearchByName.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.EmpCode + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
            }

            if (this.ReportOption == "")
            {
                if (this.Flag == "remove")
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblExistingModifyUserInSystemRole, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                    hrfSearchEmpDivision.HRef = "EmpOtherSearchDashBoard.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Division + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                    hrfSearchEmpRegion.HRef = "EmpOtherSearchDashBoard.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Type + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                    hrfSearchEmpJobCode.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.JobCode + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                    hrfSearchEmpSite.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Store + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                    hrfSearchEmpTBSDIV.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.DivisionTBS + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                    hrfSearchEmpTBSREG.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.RegionTBS + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                    hrfSearchEmpTBSDIST.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.District + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                    hrfSearchBDLSite.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Site + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                    hrfSearchBDLProvince.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Province + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                }

                if (this.Flag == "add")
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAddNewUserToSystemRole, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
                    Role objRole = new Role();

                    if (objRole.RoleSysRefSelHavingUser(BusinessUtility.GetInt(Request.QueryString["roleID"])) == true)
                    {
                        hrfSearchEmpDivision.HRef = "RoleAndOrSelection.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Division + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpRegion.HRef = "RoleAndOrSelection.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Type + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpJobCode.HRef = "RoleAndOrSelection.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.JobCode + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpSite.HRef = "RoleAndOrSelection.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Store + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpTBSDIV.HRef = "RoleAndOrSelection.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.DivisionTBS + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpTBSREG.HRef = "RoleAndOrSelection.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.RegionTBS + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpTBSDIST.HRef = "RoleAndOrSelection.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.District + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchBDLSite.HRef = "RoleAndOrSelection.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Site + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchBDLProvince.HRef = "RoleAndOrSelection.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Province + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                    }
                    else
                    {
                        hrfSearchEmpDivision.HRef = "EmpOtherSearchDashBoard.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Division + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpRegion.HRef = "EmpOtherSearchDashBoard.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Type + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpJobCode.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.JobCode + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpSite.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Store + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpTBSDIV.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.DivisionTBS + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpTBSREG.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.RegionTBS + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchEmpTBSDIST.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.District + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchBDLSite.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Site + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                        hrfSearchBDLProvince.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Province + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                    }
                }
            }
            else
            {
                hrfSearchEmpDivision.HRef = "EmpOtherSearchDashBoard.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Division + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                hrfSearchEmpRegion.HRef = "EmpOtherSearchDashBoard.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Type + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                hrfSearchEmpJobCode.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.JobCode + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                hrfSearchEmpSite.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Store + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                hrfSearchEmpTBSDIV.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.DivisionTBS + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                hrfSearchEmpTBSREG.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.RegionTBS + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                hrfSearchEmpTBSDIST.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.District + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                hrfSearchBDLSite.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Site + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
                hrfSearchBDLProvince.HRef = "EmpOtherSearchDashBoardGrid.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&searchby=" + EmpSearchBy.Province + "&actionID=" + this.ActionID + "&flag=" + this.Flag + "&roption=" + this.ReportOption;
            }
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Flag ADD/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Report Option
    /// </summary>
    public string ReportOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }

    /// <summary>
    /// To Get Is Manage List Emp Search
    /// </summary>
    public Boolean isManageListEmpSearch
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["managelistempsearch"]) == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// To Get Manage List Site ID
    /// </summary>
    public string ManageListSiteID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["siteID"]);
        }
    }

    /// <summary>
    /// To Get Role Manage List ID
    /// </summary>
    public string RoleManageListID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roleManageListID"]);
        }
    }


    /// <summary>
    /// To Get List ID
    /// </summary>
    public int ListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["ListID"]);
        }
    }

}