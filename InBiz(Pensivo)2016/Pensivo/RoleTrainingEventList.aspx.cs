﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class RoleTrainingEventList : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvRoles))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblRoleDetailTraningEvents, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            //gvUser.Columns[6].Visible = false;
        }

        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
        {
            lblSearchText.Text = Resources.Resource.lblTDCViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblTDCViewUserSearchTextHolder);

            //gvUser.Columns[4].Visible = false;
            //gvUser.Columns[5].Visible = false;
            //gvUser.Columns[3].HeaderText = Resources.Resource.lblEmpEmailID;
        }
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            lblSearchText.Text = Resources.Resource.lblEDE2ViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblEDE2ViewUserSearchTextHolder);
        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            lblSearchText.Text = Resources.Resource.lblBDLViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblBDLViewUserSearchTextHolder);
        }
        else
        {
            lblSearchText.Text = Resources.Resource.lblTBSViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblTBSViewUserSearchTextHolder);
        }
    }


    /// <summary>
    /// To Define JQ Grid Cell Binding Event 
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event </param>
    protected void gvRoles_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
    }

    /// <summary>
    /// To Bind JQ Grid With Event List Added with the Role
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Role objRole = new Role();
        string txtRole = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        gvRoles.DataSource = objRole.GetEventInRole(this.RoleID, Globals.CurrentAppLanguageCode, Utils.ReplaceDBSpecialCharacter(txtRole));
        gvRoles.DataBind();
    }



    /// <summary>
    /// To Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleid"]);
        }
    }
}