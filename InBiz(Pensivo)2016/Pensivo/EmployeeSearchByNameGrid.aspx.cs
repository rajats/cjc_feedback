﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class EmployeeSearchByNameGrid : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvEmployee))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchName, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery));
        }


        //if (Utils.TrainingInst == (int)Institute.bdl)
        //{
        //    lblUserName.Text = Resources.Resource.lblBDLUserName;
        //    txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblBDLUserName);
        //}

        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
        {
            lblUserName.Text = Resources.Resource.lblTDCViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblTDCViewUserSearchTextHolder);
            gvEmployee.Columns[3].HeaderText = Resources.Resource.lblEmpEmailID;
        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            lblUserName.Text = Resources.Resource.lblBDLViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblBDLViewUserSearchTextHolder);
        }
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            lblUserName.Text = Resources.Resource.lblEDE2ViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblEDE2ViewUserSearchTextHolder);
        }
        else
        {
            lblUserName.Text = Resources.Resource.lblTBSViewUserSearchTextHolder;
            txtRoleName.Attributes.Add("placeholder", Resources.Resource.lblTBSViewUserSearchTextHolder);
        }
    }

    /// <summary>
    /// To Bind JQ Grid with Employee List Data Source
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvEmployee_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Employee objEmp = new Employee();
        string txtName = Utils.ReplaceDBSpecialCharacter(BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]));
        objEmp.EmpName = txtName;
        objEmp.ExcludeReportUser = true;
        gvEmployee.DataSource = objEmp.GetEmployeeList();
        gvEmployee.DataBind();
    }
}