﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeeType.aspx.cs" Inherits="EmployeeType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <header class="main-content-header">
                <h2 style="margin-bottom: 5px;">
                    <%= Resources.Resource.lblMyCourses %>
                </h2>
                <h5 style="margin-bottom: 0px;">
                    <%= Resources.Resource.lblBeforwegetStarted %>
                </h5>
                <h6 style="font-weight: lighter">
                    <%= Resources.Resource.lblEmpTypeMessage %>
                </h6>
            </header>
            <%=htmlText %>
        </div>
        <asp:HiddenField ID="hdnSearchBy" runat="server" />
        <asp:HiddenField ID="hdnUserID" runat="server" />
        <asp:HiddenField ID="hdnQuery" runat="server" />
        <asp:HiddenField ID="hdnSearchNewValue" runat="server" />
        <asp:Button ID="btnSaveInformation" runat="server" OnClick="btnSaveInformation_Click" />
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Trigger Save Location Event 
        function savelocation(svalue, sSubTitle) {
            $("#<%=hdnSearchNewValue.ClientID%>").val(svalue);
            var title = "<%=Resources.Resource.lblConfirmation%>";
            var messageText = sSubTitle;
            okButtonText = "<%=Resources.Resource.lblThatsRight %>";
            LaterCnclButtonText = "<%=Resources.Resource.lblSignupTryAgain %>";
            okButtonRedirectlink = "TriggerOkButton();";
            LaterCnclButtonRedirectLink = " HideConfirmationDialog();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
            $("#clsLink").hide();
        }


        // Trigger OK Button of confirmation Message
        function TriggerOkButton() {
            ShowPensivoWaitingMessage("<%=Resources.Resource.lblSignUpWaitingMessage%>");
            $("#<%=btnSaveInformation.ClientID%>").trigger("click");
        }
    </script>
</asp:Content>



