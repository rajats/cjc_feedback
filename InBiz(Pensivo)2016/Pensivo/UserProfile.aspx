﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="UserProfile.aspx.cs" Inherits="NewUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper width-med">
            <h1><%= Resources.Resource.lblEditProfile%></h1>
            <asp:Panel ID="pnlContent" runat="server" DefaultButton="btnCreateEmployee">
                <div class="boxed-content">
                    <div class="form-body">
                        <div class="plms-fieldset is-first">
                            <label class="plms-label " for="txtFirstName"><%= Resources.Resource.lblEmpFirstName%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtFirstName" runat="server" class="plms-input skin2 is-disabled" placeholder="<%$ Resources:Resource, lblEmpFirstName %>" disabled="disabled" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEmpFirstName%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label" for="txtlastName"><%= Resources.Resource.lblEmpLastName%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtlastName" runat="server" class="plms-input skin2 is-disabled" placeholder="<%$ Resources:Resource, lblEmpLastName %>" disabled="disabled" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEmpLastName%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset" id="dvEmail" runat="server">
                            <label class="plms-label" for="txtEmail"><%= Resources.Resource.lblEmail%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtEmail" runat="server" class="plms-input skin2 " placeholder="<%$ Resources:Resource, lblEmail %>" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEmail%></p>
                                    </div>
                                </div>
                                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" class="formels-feedback invalid" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="vg1" ErrorMessage="<%$ Resources:Resource, errMsgInvalidMailFormat %>" SetFocusOnError="true"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="plms-fieldset" id="dvEmpID" runat="server">
                            <label class="plms-label" for="txtEmpID" id="lblEmpID" runat="server"></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtEmpID" runat="server" class="plms-input skin2 is-disabled"  disabled="disabled" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="pEmpID" runat="server"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset" id="dvjobcode" runat="server">
                            <label class="plms-label is-hidden" for="ddlJobCode"><%= Resources.Resource.lblJobCode%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlJobCode" runat="server" class="plms-select skin2 is-placeholder  is-disabled" disabled="disabled">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblJobCode%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset" id="dvStore" runat="server">
                            <label class="plms-label is-hidden" for="ddlStore"><%= Resources.Resource.lblStore%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlStore" runat="server" class="plms-select skin2 is-placeholder  is-disabled" disabled="disabled">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblStore%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset" id="dvDivission" runat="server">
                            <label class="plms-label is-hidden" for="ddlDivision"><%= Resources.Resource.lblDivision%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlDivision" runat="server" class="plms-select skin2 is-placeholder  is-disabled" disabled="disabled">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblDivision%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset" id="dvType" runat="server">
                            <label class="plms-label is-hidden" for="ddlType"><%= Resources.Resource.lblType%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlType" runat="server" class="plms-select skin2 is-placeholder  is-disabled" disabled="disabled">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblType%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset" id="dvProvince" runat="server" visible="false">
                            <label class="plms-label is-hidden" for="ddlProvince"><%= Resources.Resource.lblProvince%></label>
                            <div class="plms-tooltip-parent">
                                <asp:DropDownList ID="ddlProvince" runat="server" class="plms-select skin2 is-placeholder  is-disabled" disabled="disabled">
                                </asp:DropDownList>
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblProvince%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField ID="hdnUUID" runat="server" />
                        <footer class="form-footer">
                            <div class="btngrp">
                                <asp:Button ID="btnCreateEmployee" class="btn round" runat="server" Text="<%$ Resources:Resource, lblUpdate %>" ValidationGroup="vg1" OnClientClick="return BtnClick();" OnClick="btnCreateEmployee_OnClick" />
                            </div>
                        </footer>
                    </div>
                </div>
            </asp:Panel>
            <%--Define JavaScript Function--%>
            <script type="text/javascript">
                // Define To Show Contorls Custom Error message
                function BtnClick() {
                    var val = Page_ClientValidate();
                    if (!val) {
                        var i = 0;
                        for (; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $("#" + Page_Validators[i].controltovalidate).addClass("has-error");
                                break;
                            }
                            else {
                                $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                                break;
                            }
                        }
                    }
                    else {
                        var i = 0;
                        for (; i < Page_Validators.length; i++) {

                            {
                                $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                                break;
                            }
                        }
                    }
                    return val;
                }


                $(document).ready(function () {
                    $("#<%=txtEmail.ClientID%>").focus();
                        });
            </script>
        </div>
    </section>
</asp:Content>

