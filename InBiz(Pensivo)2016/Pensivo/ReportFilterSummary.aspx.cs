﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using System.IO;
using System.Text;


public partial class ReportFilterSummary : BasePage
{
    /// <summary>
    /// To Report Summar Detail HTML
    /// </summary>
    protected string sHTML = "";

    /// <summary>
    /// Set Report Filter Summary Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
            if (!IsPostBack)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblReportFilterSummary, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                ReportFilter objReportFilter = new ReportFilter();

                if ((this.ReportSearchOption.ToLower() == ReportOption.Which.ToLower()) && ((BusinessUtility.GetString(this.IsAllEvent)=="1")))
                {
                    List<ReportFilter> reportFilterListRemove = objReportFilter.GetReportFilter();
                    if (reportFilterListRemove != null && reportFilterListRemove.Count > 0)
                    {
                        var removeList = reportFilterListRemove.FirstOrDefault(x => x.ReportOption.ToLower() == ReportOption.Which.ToLower());
                        reportFilterListRemove.Remove(removeList);
                        HttpContext.Current.Session["ReportMultiFilter"] = reportFilterListRemove;
                    }
                }
                else if ((this.ReportSearchOption.ToLower() == ReportOption.Which.ToLower()) && ((BusinessUtility.GetString(this.IsAllEvent) == "0")))
                {
                    List<ReportFilter> reportFilterListRemove = objReportFilter.GetReportFilter();

                    if (reportFilterListRemove != null && reportFilterListRemove.Count > 0)
                    {
                        var removeList = reportFilterListRemove.FirstOrDefault(x => x.ReportOption.ToLower() == ReportOption.Which.ToLower() && x.SearchBy.ToLower() == "");
                        reportFilterListRemove.Remove(removeList);
                        HttpContext.Current.Session["ReportMultiFilter"] = reportFilterListRemove;
                    }
                }

                objReportFilter.CreateNewFilter(this.ReportSearchOption, this.SearchBy, this.SearchValue, BusinessUtility.GetString(this.IsAllEvent));
                List<ReportFilter> reportFilterList = objReportFilter.GetReportFilter().OrderBy(o => o.ReportOption).ToList();
                var resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.When.ToLower()).ToList();
                if (resultItem != null && resultItem.Count==0)
                {
                    objReportFilter.CreateNewFilter(ReportOption.When, ReportDateFilterOption.CurrentYearToDate, "", BusinessUtility.GetString(this.IsAllEvent));
                }

                reportFilterList = objReportFilter.GetReportFilter().OrderBy(o => o.ReportOption).ToList();
                sHTML = BusinessUtility.GetString(GetReportSummaryDetails(reportFilterList));
                hrefYes.HRef = "Report.aspx" + Request.Url.Query;
            }
        }

        ltrTitle.Text = Resources.Resource.lblReportDetails;
    }

    /// <summary>
    /// To Get Report Summary Details
    /// </summary>
    /// <param name="reportFilterList">Pass Report Filter List</param>
    /// <returns>String</returns>
    private string GetReportSummaryDetails(List<ReportFilter> reportFilterList)
    {
        StringBuilder sbHtml = new StringBuilder();
        Event objEvent = new Event();
        Employee objEmp = new Employee();
        Role objRole = new Role();


        sbHtml = new StringBuilder();
        sbHtml.Append("<style>.reportTable{width:100%;}  .reportCol1{width:200px;height:auto;word-break:break-word}  .reportCol2{width:100px;height:auto;word-break:break-word;vertical-align:top;padding-left:0px} .reportCol3{width:200px;height:auto;word-break:break-all;max-width: 200px;}  .reportTable tr:nth-child(2n+1) td{background: none repeat scroll 0 0 transparent;} .reportTable tr{ border-bottom: 0px solid rgba(0, 0, 0, 0.1);}    .reportTableDesc tr:nth-child(2n+1) td{background: none repeat scroll 0 0 transparent;padding-left: 0px;} .reportTableDesc tr{ border-bottom: 0px solid rgba(0, 0, 0, 0.1);} .titlelable{font-weight:bold;font-size:18px} .subtitlelable{font-weight:normal;font-size:16px;font-style:italic;padding-left: 15px;}  </style>");
        sbHtml.Append("<table class='reportTable' style='border:1'>");
        int rowIndex = 1;



        sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblReportWho + ":"));
        var resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.Who.ToLower()).ToList();
        if (resultItem != null && resultItem.Count <= 0)
        {
            sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTrainingUser));
        }
        else
        {
            if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "".ToLower()) && (resultItem.Count == 1))
            {
                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTrainingUser));
            }
            else
            {

                foreach (var filterItem in resultItem)
                {
                    if (filterItem.ReportOption.ToLower() == ReportOption.Who.ToLower())
                    {
                        if (filterItem.SearchBy.ToLower() == EmpSearchBy.Name.ToLower())
                        {
                            string[] sName = BusinessUtility.GetString(filterItem.SearchValue).Split(',');
                            int i = 0;
                            foreach (string sEmName in sName)
                            {

                                objEmp = new Employee();
                                objEmp.GetEmployeeDetail(BusinessUtility.GetInt(sEmName));

                                if (i == 0)
                                {
                                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, BusinessUtility.GetString(Resources.Resource.lblSearchName) + ":", BusinessUtility.GetString(objEmp.EmpName)));
                                }
                                else
                                {
                                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", BusinessUtility.GetString(objEmp.EmpName)));
                                }
                                i += 1;
                            }

                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.EmpCode.ToLower())
                        {
                            if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry))
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblEmpEmailID + ":", filterItem.SearchValue));
                            }
                            else
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchEmployeeCode + ":", filterItem.SearchValue));
                            }
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.District.ToLower())
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchDistrict + ":", filterItem.SearchValue));
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.RegionTBS.ToLower())
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchRegion + ":", filterItem.SearchValue));
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.DivisionTBS.ToLower())
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchDivision + ":", filterItem.SearchValue));
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Type.ToLower())
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblEmpType + ":", filterItem.SearchValue));
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.JobCode.ToLower())
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchJobCode + ":", filterItem.SearchValue));
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Division.ToLower())
                        {
                            if (filterItem.SearchValue.ToLower() == Resources.Resource.lblSubTileAll.ToLower())
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchLocation + ":", Resources.Resource.lblSubTileAll));
                            }
                            else
                            {
                                objRole = new Role();
                                DataTable dt = new DataTable();

                                DataView dv = objRole.GetSysRef(filterItem.SearchBy, "", Globals.CurrentAppLanguageCode).DefaultView;
                                dv.RowFilter = "sysRefCodeValue ='" + filterItem.SearchValue + "'";
                                string sValue = BusinessUtility.GetString(dv[0]["tiles"]);
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchLocation + ":", sValue));
                            }
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Store.ToLower())
                        {
                            if (Utils.TrainingInst == (int)Institute.bdl)
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblDeptID + ":", filterItem.SearchValue));
                            }
                            else
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchSite + ":", filterItem.SearchValue));
                            }
                        }
                        else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Site.ToLower())
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchSite + ":", filterItem.SearchValue));
                        }
                    }
                }
            }
        }



        sbHtml.Append(CreateFilterBlankRow(ref rowIndex));
        sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblReportWhich + ":"));
        resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.Which.ToLower()).ToList();
        if (resultItem != null && resultItem.Count <= 0)
        {
            sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTrainingEvents));
        }
        else
        {
            if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "".ToLower()) && (resultItem.Count == 1))
            {
                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTrainingEvents));
            }
            else
            {
                foreach (var filterItem in resultItem)
                {
                    string searchBy = "";
                    if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByEventName.ToLower()))
                    {
                        searchBy = Resources.Resource.lblTrainingByTitle;
                    }
                    else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByList.ToLower()))
                    {
                        searchBy = Resources.Resource.lblTrainingByList;
                    }
                    else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByCategory.ToLower()))
                    {
                        searchBy = Resources.Resource.lblTrainingByCategory;
                        objEvent = new Event();
                        DataTable dt = objEvent.GetCourseTypeList(Globals.CurrentAppLanguageCode);
                        DataView dv = dt.DefaultView;
                        dv.RowFilter = "TypeValue='" + filterItem.SearchValue + "'";
                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, BusinessUtility.GetString(searchBy) + ":", BusinessUtility.GetString(dv[0][1])));
                    }


                    if ((filterItem.SearchBy.ToLower() != ReportFilterOption.ByCategory.ToLower()) )
                    {
                        string[] sName = BusinessUtility.GetString(filterItem.SearchValue).Replace("<br/>", "").Split(',');
                        int i = 0;
                        foreach (string sEmName in sName)
                        {
                            objEvent = new Event();
                            objEvent.GetEventDetail(BusinessUtility.GetInt(sEmName), Globals.CurrentAppLanguageCode);

                            if (i == 0)
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, BusinessUtility.GetString(searchBy) + ":", BusinessUtility.GetString(objEvent.EventName)));
                            }
                            else
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", BusinessUtility.GetString(objEvent.EventName)));
                            }
                            i += 1;
                        }
                    }
                }
            }
        }
        sbHtml.Append(CreateFilterBlankRow(ref rowIndex));
        sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblReportWhatProgress + ":"));
        resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.What.ToLower()).ToList();
        if (resultItem != null && resultItem.Count <= 0)
        {
            sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTraininProgress));
        }
        else
        {
            if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "all".ToLower()) && (resultItem.Count == 1))
            {
                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTraininProgress));
            }
            else
            {

                foreach (var filterItem in resultItem)
                {
                    if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Once_Completed.ToLower()))
                    {
                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblTrainingCompleteOnce));
                    }
                    else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Completed.ToLower()))
                    {
                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblTrainingNotCompletedOnce));
                    }
                    else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Started.ToLower()))
                    {
                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblTrainingNotStartedAtOnce));
                    }
                    else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.All_Course_Progress.ToLower()))
                    {
                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllCourseProgress));
                    }
                    else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Only_Those_Not_Completed_Training.ToLower()))
                    {
                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblTrainingNotCompletedOnce));
                    }
                }
            }
        }

        sbHtml.Append(CreateFilterBlankRow(ref rowIndex));
        sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblReportWhen + ":"));
        resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.When.ToLower()).ToList();
        if (resultItem != null && resultItem.Count <= 1)
        {
            sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblReportTrainingYearToDate));
        }
        sbHtml.Append("</table>");
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// Create Report Filter Sub Header Row in Report Filter Detail
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <param name="label">Pass Label</param>
    /// <returns>String</returns>
    private string CreateFilterSubHeader(ref int rowIndex, string label)
    {
        string rValue = "";
        if (rowIndex == 1)
        {
            rValue = (" <tr><td class='reportCol1'>" + Resources.Resource.lblReportFilterSummaryMessage + "</td><td class='reportCol2 titlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + "" + "</td></tr>");
        }
        else
        {
            rValue = (" <tr><td class='reportCol1'>" + "" + "</td><td class='reportCol2 titlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + "" + "</td></tr>");
        }
        rowIndex += 1;
        return rValue;
    }

    /// <summary>
    /// Create Report Filter Data Row in Report Filter Detail
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <param name="label">Pass Label</param>
    /// <param name="labelValue">Pass Label Value</param>
    /// <returns>String</returns>
    private string CreateFilterDataRow(ref int rowIndex, string label, string labelValue)
    {
        rowIndex += 1;
        return (" <tr><td class='reportCol1'>" + "" + "</td><td class='reportCol2 subtitlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + labelValue + "</td></tr>");
    }

    /// <summary>
    /// Create Report Filter Blank Row
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <returns>String</returns>
    private string CreateFilterBlankRow(ref int rowIndex)
    {
        rowIndex += 1;
        return (" <tr><td class='reportCol1'>" + "" + "</td><td class='reportCol2 titlelable'> &nbsp; </td><td class='reportCol3' style='padding-top:0px'>&nbsp;</td></tr>");
    }

    /// <summary>
    /// Get Report Search Option
    /// </summary>
    public string ReportSearchOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }

    /// <summary>
    /// Get Report Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// Get Report Search By Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Is All Event
    /// </summary>
    public int IsAllEvent
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["isAllEvent"]);
        }
    }

}