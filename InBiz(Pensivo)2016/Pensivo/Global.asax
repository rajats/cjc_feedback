﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="RusticiSoftware.HostedEngine.Client" %>
<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>

<script RunAt="server">
    /// <summary>
    /// Startup Setting for RusticiSoftware
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        ScormCloud.Configuration =
       new RusticiSoftware.HostedEngine.Client.Configuration(
            AppConfig.GetAppConfigValue("HostedEngineWebServicesUrl"),
            AppConfig.GetAppConfigValue("HostedEngineAppId"),
           AppConfig.GetAppConfigValue("HostedEngineSecurityKey"),
            AppConfig.GetAppConfigValue("Origin"));

    }

    /// <summary>
    /// To Define Event Application Ended
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown
    }

    /// <summary>
    /// To Define Application Error Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
    }

    /// <summary>
    /// To Define Application Session Start Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
    }

    /// <summary>
    /// To Define Application Session Ended
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
