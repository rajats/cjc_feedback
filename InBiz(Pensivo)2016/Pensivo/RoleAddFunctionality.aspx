﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleAddFunctionality.aspx.cs" Inherits="RoleAddFunctionality" %>

<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <%=htmlText %>
        </div>
        <asp:HiddenField ID="hdnMsgFunctionalityConfirmation" runat="server" />
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Show Submenu of Functionality
        function showsubmenu(roleID, functionalityID, functionalityType, flag) {
            if (functionalityID == "<%=(int)RoleAction.Reporting%>") {
                window.location.href = "AccessReportingDataDashboard.aspx?roleID=" + roleID + "&actionID=" + functionalityID + "&flag=add&functionalityType=" + functionalityType + "";
            }
            else if (functionalityID == "<%=(int)RoleAction.Manage_List%>") {
                window.location.href = "RoleManageList.aspx?roleID=" + roleID + "&actionID=" + functionalityID + "&flag=add&functionalityType=" + functionalityType + "";
            }
            else if (functionalityID == "<%=(int)RoleAction.Assign_Training%>") {
                window.location.href = "RoleTrainingEvent.aspx?roleID=" + roleID + "&actionID=" + functionalityID + "&flag=add&functionalityType=" + functionalityType + "";
            }
            else {
                window.location.href = "AddReportingDashBoard.aspx?roleID=" + roleID + "&actionID=" + functionalityID + "&flag=" + flag + "&functionalityType=" + functionalityType + " ";
            }
        }

        // To Add/Edit Functionality with Role
        function addInRole(roleID, actionID, actionType, flag, actionName) {
            if (actionID == "<%=(int)RoleAction.Administration%>") {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "addAction";
                datatoPost.ActionID = actionID;
                datatoPost.ActionType = actionType;
                datatoPost.RoleID = roleID;
                $.post("RoleAddFunctionality.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        if (datatoPost.ActionID == "<%=(int)RoleAction.Administration%>" || datatoPost.ActionID == "<%=(int)RoleAction.Reporting%>") {
                            window.location.href = "AddReportingDashBoard.aspx?roleID=" + roleID + "&actionID=" + actionID + "&flag=add&functionalityType=" + actionType;
                        }
                    }
                    else {
                        ShowPensivoMessage("<%=Resources.Resource.msgCouldNotAddFunctionality%>")
                    }
                });
            }
            else if (actionID == "<%=(int)RoleAction.Reporting%>") {
                window.location.href = "AccessReportingDataDashboard.aspx?roleID=" + roleID + "&actionID=" + actionID + "&flag=add&functionalityType=" + actionType + "";
            }
            else if (actionID == "<%=(int)RoleAction.Admin_Reporting%>") {
                window.location.href = "AdminReporting.aspx?roleID=" + roleID + "&actionID=" + actionID + "&flag=add&functionalityType=" + actionType + "";
            }
            else {
                var title = "<%=Resources.Resource.lblConfirmation%>";
                var messageText = ($("#<%=hdnMsgFunctionalityConfirmation.ClientID%>").val()).replace("#FUNCTIONALITYNAME#", actionName);
                okButtonText = "OK";
                LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
                okButtonRedirectlink = "TriggerRoleAddFunctionalityOkButton('" + actionID + "', '" + actionType + "', '" + roleID + "');";
                LaterCnclButtonRedirectLink = " HideConfirmationDialog();";
                PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
            }
        }

        // Trigger Role Add Functionality Ok Button
        function TriggerRoleAddFunctionalityOkButton(actionID, actionType, roleID) {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "addAction";
            datatoPost.ActionID = actionID;
            datatoPost.ActionType = actionType;
            datatoPost.RoleID = roleID;
            $.post("RoleAddFunctionalityConfirmation.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    if (datatoPost.ActionID == "<%=(int)RoleAction.Administration %>" || datatoPost.ActionID == "<%=(int)RoleAction.Reporting %>") {
                        window.location.href = "AddReportingDashBoard.aspx?roleID=" + roleID + "&actionID=" + actionID + "&flag=add&functionalityType=" + actionType;
                    }
                    else {
                          window.location.href=  "roleActivity.aspx?roleID=" + roleID;
                    }
                }
                else {
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotAddFunctionality%>")
                }
            });
        }

    </script>
</asp:Content>

