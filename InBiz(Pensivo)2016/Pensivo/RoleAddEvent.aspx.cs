﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class RoleAddEvent : BasePage
{
    /// <summary>
    /// Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvRoles))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAddNewTrainingEventToTheSystemRole, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            objRole = new Role();
            string sRoleName = objRole.GetRoleName(this.RoleID);
            ltrTitle.Text = Resources.Resource.lblAddEventWithSystemRole.Replace("#RoleName#", sRoleName);
        }

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addEvent")
        {
            try
            {
                int eventID = BusinessUtility.GetInt(Request.Form["EventID"]);
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);

                if (eventID > 0 && roleID > 0)
                {
                    List<Role> lRoleEvent = new List<Role>();
                    lRoleEvent.Add(new Role { EventID = eventID });
                    objRole = new Role();
                    if (objRole.RoleAddEvent(roleID, lRoleEvent) == true)
                    {
                        ExecutingThread.ThreadEmployeeAssignedCoursesByRole(roleID, eventID, (int)AssignedCourseInActiveReason.BySysAdminRole, (int)AssignedCourseUpdatedSource.BySysAdminRoleUpdate);
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
    }

    /// <summary>
    /// To Define JQGrid CellBinding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Binding Event</param>
    protected void gvRoles_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            objRole = new Role();
            string sRoleName = objRole.GetRoleName(this.RoleID);
            string str = string.Empty;
            str += Resources.Resource.lblRoleAddEvent.Replace("#ROLENAME#", sRoleName).Replace("#EVENTNAME#", BusinessUtility.GetString(e.RowValues[1]));
            string sMessage = str.Replace("\"", "{-").Replace("'", "{_");
            e.CellHtml = string.Format(@"<a href=""javascript:;""  onclick=""AddEvent({0},{1},'{2}')"">" + Resources.Resource.lblAdd + "</a>", e.CellHtml, this.RoleID, sMessage);
        }
    }

    /// <summary>
    /// To Bind JQ Grid with Datasource
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid DataRequest Event</param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtRole = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        Role objRole = new Role();
        gvRoles.DataSource = objRole.GetEventNotInRole(this.RoleID, Globals.CurrentAppLanguageCode, Utils.ReplaceDBSpecialCharacter(txtRole));
        gvRoles.DataBind();
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }
}