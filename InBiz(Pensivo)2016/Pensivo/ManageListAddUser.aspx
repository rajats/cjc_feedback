﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ManageListAddUser.aspx.cs" Inherits="ManageListAddUser" %>
<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada )  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.EDE2 )  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
        <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>
    <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>

     <%--Define Inline Css to Wrap Grid Header and Row Data--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th div {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
        }

        .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
            vertical-align: text-top;
        }
    </style>
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <h4>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h4>
            <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" Visible="false">
                <div class="search-box-body">
                    <div class="plms-input-group">
                        <div class="plms-fieldset">
                            <asp:Label ID="Label2" CssClass="filter-key" AssociatedControlID="txtRoleName" class="plms-label is-hidden no-height" runat="server"
                                Text=""></asp:Label>
                            <asp:TextBox runat="server" Width="180px" placeholder="role name" ID="txtRoleName" class="plms-input skin2" CssClass="filter-key"></asp:TextBox>
                        </div>
                    </div>
                    <input id="btnSearch" type="button" class="btn fluid clearfix" value="search" />
                </div>
            </asp:Panel>
            <div id="grid_wrapper" style="width: 100%;" onkeypress="return disableEnterKey(event)">
                <trirand:JQGrid runat="server" ID="gvSiteManager" Height="300px"
                    AutoWidth="True" OnCellBinding="gvSiteManager_CellBinding" OnDataRequesting="gvSiteManager_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="empHdrID" Visible="false" PrimaryKey="True" />
                        <trirand:JQGridColumn DataField="empFirstName" HeaderText="<%$ Resources:Resource, lblEmpFirstName %>"
                            Editable="false" Width="200" />
                        <trirand:JQGridColumn DataField="empLastName" HeaderText="<%$ Resources:Resource, lblEmpLastName %>"
                            Editable="false" Width="200" />
                        <trirand:JQGridColumn DataField="empExtID" HeaderText="<%$ Resources:Resource, lblSiteReportEmpID %>"
                            Editable="false" Width="200" />
                        <trirand:JQGridColumn DataField="empSiteID" HeaderText="<%$ Resources:Resource, lblSupervisorManagerSite %>" DataType="String"
                            Editable="false" Width="200" />
                        <trirand:JQGridColumn DataField="empHdrID" HeaderText="<%$ Resources:Resource, lblAdd %>"
                            Sortable="false" TextAlign="Center" Width="100" />
                    </Columns>
                    <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="loadComplete" />
                </trirand:JQGrid>
            </div>
            <asp:HiddenField ID="HdnFlag" runat="server" />
            <asp:HiddenField ID="HdnReturnURL" runat="server" />
            <asp:HiddenField ID="HdnListID" runat="server" />
        </div>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvSiteManager.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        // To Resize JQ Grid
        function jqGridResize() {
            $("#<%=gvSiteManager.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // To Define JQ Grid Load Complete
        function loadComplete(data) {
            jqGridResize();
        }


        // To Add User with Role 
        function AddUser(userID, roleManageListID, siteID, message, searchBy, functionalityID) {
            var title = "Confirmation";
            var messageText = message.replace(new RegExp("{-", "gm"), '"').replace(new RegExp("{_", "gm"), "'");// "<%=Resources.Resource.msgRuWantToAddUser%>";
            okButtonText = "OK";
            LaterCnclButtonText = "Cancel";
            okButtonRedirectlink = "ConfirmRoleAddUserOk(" + userID + " , " + roleManageListID + ", '" + siteID + "', '" + searchBy + "', '" + functionalityID + "')";
            LaterCnclButtonRedirectLink = "ConfirmAddRoleUsrCancel();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
            $("#clsLink").hide();
        }

        // To Move Add User with Role Confirmation Page
        function ConfirmRoleAddUserOk(userID, roleManageListID, siteID, searchBy, functionalityID) {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "addUser";
            datatoPost.UserID = userID;
            datatoPost.RoleManageListID = roleManageListID;
            datatoPost.CreatedBy = "<%=CurrentEmployee.EmpID %>";
            datatoPost.SiteID = siteID;
            datatoPost.SysRefCode = searchBy;
            datatoPost.FunctionalityID = functionalityID;
            datatoPost.ListID = $("#<%=HdnListID.ClientID %>").val();
            $.post("ManageListAddUser.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    window.location.href = $("#<%=HdnReturnURL.ClientID%>").val();
                    //window.location.href = "AdministrationDashBoard.aspx";
                }
                else {
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotAddUsertoSeeSiteReport%>")
                }
            });
        }

        // To Call User Canceled Confirmation Message
        function ConfirmAddRoleUsrCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
            window.location.href = $("#<%=HdnReturnURL.ClientID%>").val();
        }

        // To Show Error Message
        function ShowErrMessage() {
            ShowPensivoMessage("<%=Resources.Resource.lblNotRemoveUserNeedAtLeastOneUser%>");
        }

        $(document).ready(function () {
            $("#<%=txtRoleName.ClientID%>").focus();
                });

    </script>

</asp:Content>