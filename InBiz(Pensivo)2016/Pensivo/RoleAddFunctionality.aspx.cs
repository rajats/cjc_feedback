﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;

public partial class RoleAddFunctionality : BasePage
{
    /// <summary>
    /// To Store HTML Text
    /// </summary>
    protected string htmlText = "";

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Role objRole = new Role();
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAddNewFunctionalityToTheSystemRole, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            htmlText = loadEmpSearchValue();
        }

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addAction")
        {
            try
            {
                int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                string actionType = BusinessUtility.GetString(Request.Form["ActionType"]);
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                if (actionID > 0 && roleID > 0)
                {

                    if (objRole.RoleAddFunctionality(roleID, actionID, actionType) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }
            Response.End();
            Response.SuppressContent = true;
        }

        string roleName = "";
        if (this.RoleID > 0)
        {
            objRole = new Role();
            roleName = objRole.GetRoleName(this.RoleID);
        }

        hdnMsgFunctionalityConfirmation.Value = Resources.Resource.lblRoleFunctionalityAdd.Replace("#ROLENAME#", roleName);
    }

    /// <summary>
    /// To Load Available Functionality
    /// </summary>
    /// <returns>String</returns>
    private string loadEmpSearchValue()
    {
        string sbHtml = "";
        Role objRole = new Role();
        DataTable dt = objRole.GetFunctionalityNotInRole(this.RoleID, Globals.CurrentAppLanguageCode);

        if (Utils.TrainingInst == (int)Institute.navcanada || Utils.TrainingInst == (int)Institute.EDE2)
        {
            DataRow[] drr = dt.Select("ActionItem=' " + (int)RoleAction.Administration + " ' ");
            for (int i = 0; i < drr.Length; i++)
                drr[i].Delete();
            dt.AcceptChanges();
        }

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sbHtml += " <div class='boxset' id='dvAdminDashBoard" + BusinessUtility.GetString(i) + "'> ";
            for (int j = 0; j <= 2; j++)
            {
                if (i < dt.Rows.Count)
                {
                    string sText = BusinessUtility.GetString(dt.Rows[i]["ItemName"]);
                    string sValue = BusinessUtility.GetString(dt.Rows[i]["ActionItem"]);

                    if (BusinessUtility.GetInt(sValue) == (int)RoleAction.AI_2015)
                    {
                        sText = Resources.Resource.lblAI2015;
                    }

                    if (sText != "")
                    {
                        string sFunction = "";
                        string sRoleType = BusinessUtility.GetString(dt.Rows[i]["RoleType"]);

                        if (sRoleType == RoleType.Menu)
                        {
                            objRole = new Role();
                            if (objRole.ActionExistsInRole(this.RoleID, BusinessUtility.GetInt(sValue), sRoleType) == true)
                            {
                                sFunction = string.Format("onclick='showsubmenu({0},{1},\"" + sRoleType + "\",\"" + this.Flag + "\",\"" + sText + "\");'", this.RoleID, sValue, sRoleType);
                            }
                            else
                            {
                                if ((BusinessUtility.GetInt(sValue) == (int)RoleAction.Assign_Training) || (BusinessUtility.GetInt(sValue) == (int)RoleAction.Manage_List))
                                {
                                    sFunction = string.Format("onclick='showsubmenu({0},{1},\"" + sRoleType + "\",\"" + this.Flag + "\",\"" + sText + "\");'", this.RoleID, sValue, sRoleType);
                                }
                                else
                                {
                                    sFunction = string.Format("onclick='addInRole({0},{1},\"" + sRoleType + "\",\"" + this.Flag + "\",\"" + sText + "\");'", this.RoleID, sValue, sRoleType);
                                }
                            }
                        }
                        else if (sRoleType == RoleType.System)
                        {
                            sFunction = string.Format("onclick='addInRole({0},{1},\"" + sRoleType + "\",\"" + this.Flag + "\",\"" + sText + "\");'", this.RoleID, sValue, sRoleType);
                        }

                        sbHtml += " <div class='boxset-box'> ";
                        sbHtml += " <a id='hrfSearchEmpName' runat='server' href='#'  " + sFunction + " class='boxset-box-wrapper'> ";
                        sbHtml += " <div class='boxset-box-inner'> ";
                        sbHtml += " <h4 class='boxset-title'> " + sText + " </h4> ";
                        sbHtml += " </div> ";
                        sbHtml += " </a> ";
                        sbHtml += " </div> ";
                        if (j <= 1)
                        {
                            i += 1;
                        }
                    }
                }
            }
            sbHtml += " </div> ";
        }
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Add/Remove Flag
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

}