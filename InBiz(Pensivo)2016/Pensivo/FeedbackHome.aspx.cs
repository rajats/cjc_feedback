﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTECH.Library.Utilities;
using iTECH.Pensivo.BusinessLogic;

public partial class FeedbackHome : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            //currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblFeedback, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            if (BusinessUtility.GetString(Request.QueryString["returnfromoldPIB"]) == "1")
            {
                EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.ReturnToPibFromOldSystem,
                    BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), "", 0, (int)RusticRegStatusCode.None, "");
            }

            if (Breadcrumbs.BreadcrumbsHasMenu() == false)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                if (Request.QueryString["error"] != "")
                {
                    Breadcrumbs.BreadcrumbsSetRootMenu();
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblFeedback, Path.GetFileName(Request.Url.AbsolutePath), Path.GetFileName(Request.Url.AbsolutePath));
                }
                else
                {
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblFeedback, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);
                }
            }
            else
            {
               Breadcrumbs.BreadcrumbsSetRootMenu();
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                //currdiv.InnerHtml = Breadcrumbs.GetBreadcrumbsList("FeedbackHome.aspx");
                if (CurrentEmployee.EmpType == "S")
                {
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblFeedback, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);
                }
                else
                {
                    HttpContext.Current.Session["dtBrdCrumb"] = null;
                    Breadcrumbs.ResetMyTrainingMenu();
                    currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblFeedback, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);
                }
            }
        }
    }
}