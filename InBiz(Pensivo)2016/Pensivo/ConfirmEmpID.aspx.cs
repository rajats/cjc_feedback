﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class ConfirmEmpID : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        valEmpIDExp.ValidationExpression = Utils.EmpIdValidatorExpression;
        dvLogInErrMsg.Visible = false;
        txtUserName.Focus();

        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
        {
            hTitle.InnerHtml = Resources.Resource.lblTypeEmpEmailID;
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblEmpEmailID);
            rfRoleName.Text = Resources.Resource.reqEmpEmailID;
            valEmpIDExp.Text = Resources.Resource.lblTDCInvalidLogInIDFormat;
            lblUserName.InnerHtml = Resources.Resource.lblEmpEmailID;
            pTooltipUserName.InnerHtml = Resources.Resource.lblEmpEmailID;
        }
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            hTitle.InnerHtml = Resources.Resource.lblEDE2TypeUserName;
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblEDE2UserName);
            rfRoleName.Text = Resources.Resource.reqEDE2EmpCode;
            valEmpIDExp.Text = Resources.Resource.lblEDE2InvalidLogInIDFormat;
            lblUserName.InnerHtml = Resources.Resource.lblEDE2UserName;
            pTooltipUserName.InnerHtml = Resources.Resource.lblEDE2UserName;

        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            hTitle.InnerHtml = Resources.Resource.lblBDLTypeUserName;
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblBDLUserName);
            rfRoleName.Text = Resources.Resource.reqBDLEmpCode;
            valEmpIDExp.Text = Resources.Resource.lblBdlInvalidLogInIDFormat;
            lblUserName.InnerHtml = Resources.Resource.lblBDLUserName;
            pTooltipUserName.InnerHtml = Resources.Resource.lblBDLUserName;
            
        }
        else
        {
            hTitle.InnerHtml = Resources.Resource.lblTypeUserName;
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblUserName);
            rfRoleName.Text = Resources.Resource.reqEmpNumber;
            valEmpIDExp.Text = Resources.Resource.lblTBSInvalidLogInIDFormat;
            lblUserName.InnerHtml = Resources.Resource.lblUserName;
            pTooltipUserName.InnerHtml = Resources.Resource.lblUserName;
        }
    }

    /// <summary>
    /// To Varify Employee ID and Move to next page
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnVarifyEmpID_OnClick(object sender, EventArgs e)
    {
        Employee objEmp = new Employee();
        if (objEmp.EmployeeIDExists(txtUserName.Text, 0))
        {
            int iUserID = objEmp.GetEmpID(txtUserName.Text);
            Response.Redirect("ConfirmAnswer.aspx?username=" + txtUserName.Text + "&uid=" + BusinessUtility.GetString(iUserID));
        }
        else
        {
            showMessage(Resources.Resource.msgInvalidUserID);
        }
    }

    /// <summary>
    /// To Show Message
    /// </summary>
    /// <param name="message"></param>
    private void showMessage(string message)
    {
        lblErrMsg.Text = message;
        dvLogInErrMsg.Visible = true;
    }
}