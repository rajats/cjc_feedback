﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class FunctionalityAddRemoveUser : BasePage
{
    /// <summary>
    /// Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            objRole = new Role();
            string sRoleName = objRole.GetRoleName(this.RoleID);
            string sActionName = objRole.GetRoleName(this.RoleID);
            if (this.ActionID > 0)
            {
                objRole = new Role();
                objRole.GetActionDetail(this.ActionID, Globals.CurrentAppLanguageCode);
                sActionName = objRole.ActionName;
            }

            if (this.Flag == "add")
            {
                ltrTitle.Text = Resources.Resource.lblAssociateRoleWithAction.Replace("#RoleName#", sRoleName).Replace("#ActionName#", sActionName);
            }
            else if (this.Flag == "remove")
            {
                ltrTitle.Text = Resources.Resource.lblModifyRoleWithAction.Replace("#RoleName#", sRoleName).Replace("#ActionName#", sActionName);
            }

            if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeAction")
            {
                try
                {
                    int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                    int userID = BusinessUtility.GetInt(Request.Form["UserID"]);
                    if (actionID > 0 && roleID > 0)
                    {
                        objRole = new Role();
                        if (objRole.RemoveUsersInReportUserManagment(roleID, userID, actionID) == true)
                        {
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }

                Response.End();
                Response.SuppressContent = true;
            }
        }
    }

    /// <summary>
    /// To Define JQ Grid CellBinding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object  </param>
    /// <param name="e">Pass JQ Grid Cell Bind Event Args</param>
    protected void gvRoles_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 4)
        {
            if (this.Flag == "add")
            {
                e.CellHtml = string.Format(@"<a href=""javascript:;""  onclick=""AddUsers({0},{1},'{2}','{3}',{4} )"">" + Resources.Resource.lblAdd + "</a>", e.CellHtml, this.RoleID, this.SearchBy, this.SearchValue, this.ActionID);
            }
            else if (this.Flag == "remove")
            {
                e.CellHtml = string.Format(@"<a href=""javascript:;""  onclick=""RemoveUsers({0},{1},'{2}','{3}',{4} )"">" + Resources.Resource.lblRemove + "</a>", e.CellHtml, this.RoleID, this.SearchBy, this.SearchValue, this.ActionID);
            }
        }
    }

    /// <summary>
    /// To Bind JQ Grid With Data User In or Not in Report/User Managment Functionality
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Role objRole = new Role();
        if (this.Flag == "add")
        {
            gvRoles.DataSource = objRole.GetUsersNotInReportUserManagment(this.RoleID, this.SearchBy, this.SearchValue, this.ActionID);
        }
        else if (this.Flag == "remove")
        {
            gvRoles.DataSource = objRole.GetUsersInReportUserManagment(this.RoleID, this.SearchBy, this.SearchValue, this.ActionID);
        }
        gvRoles.DataBind();
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Search By 
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search By Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

}