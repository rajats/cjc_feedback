﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class RoleAdd : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv;
            if (this.RoleID > 0)
            {
                currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblChangeRoleName, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
                ltrTitle.Text = Resources.Resource.lblChangeRoleName;
                btnCreateRole.Text = Resources.Resource.BtnUpdateRoleName;

                Role objRole = new Role();
                txtRoleName.Text = objRole.GetRoleName(this.RoleID);
            }
            else
            {
                currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblCreateSystemRole, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
                ltrTitle.Text = Resources.Resource.lblCreateRole;
                btnCreateRole.Text = Resources.Resource.btnSaveRole;
            }
        }
    }

    /// <summary>
    /// To Add/Edit Role and Move to Next Page
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnCreateRole_OnClick(object sender, EventArgs e)
    {
        Role objRole = new Role();
        if (this.RoleID <= 0)
        {
            if (!objRole.IsExistsRole(txtRoleName.Text))
            {
                if (objRole.CreateRole(txtRoleName.Text, CurrentEmployee.EmpID))
                {
                    Response.Redirect("RoleDashBoard.aspx");
                }
            }
            else
            {
                dvLogInErrMsg.Visible = true;
            }
        }
        else
        {
            if (objRole.UpdateRoleName(txtRoleName.Text, this.RoleID, CurrentEmployee.EmpID))
            {
                Response.Redirect("RoleEdit.aspx");
            }
            else
            {
                GlobalMessage.showAlertMessage(Resources.Resource.lblUpdateRoleNameFailure);
            }
        }
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }
}