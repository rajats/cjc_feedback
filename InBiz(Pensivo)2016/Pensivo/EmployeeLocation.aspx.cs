﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;

public partial class EmployeeLocation : BasePage
{   
    /// <summary>
    /// To Hold HTML Content
    /// </summary>
    protected string htmlText = "";

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        htmlText = loadEmpSearchValue();
        hdnSearchBy.Value = this.SearchBy;
        hdnNextSearchBy.Value = EmpSearchBy.Type;
        hdnUserID.Value = BusinessUtility.GetString(CurrentEmployee.EmpID);
        hdnQuery.Value = BusinessUtility.GetString(Request.Url.Query);
        hdnAskLocation.Value = this.AskLocation;

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addLocation")
        {
            try
            {
                int empID = BusinessUtility.GetInt(Request.Form["EmpID"]);
                string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                string empUUID = BusinessUtility.GetString(Request.Form["EmpUUID"]);
                if (empID > 0 && searchBy != "" && searchValue != "")
                {

                    Boolean isUpdatePensivoSite = PensivoExecutions.PensivoUpdateUserLocation(BusinessUtility.GetInt(empUUID), Utils.ReplaceDBSpecialCharacter(searchValue));
                    if (isUpdatePensivoSite == false)
                    {
                        ErrorLog.createLog("Location Not Updated on PostGress Employee UUID " + empUUID);
                    }

                    Employee objEmp = new Employee();
                    if (objEmp.InsertEmployeeSysRef(empID, searchBy, searchValue) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        ErrorLog.createLog("Not Able To update Location in PIB for UserID " + empID);
                        Response.Write("ok");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }

        if (AskLocation == "1")
        {
            Employee objEmp = new Employee();
            objEmp.GetEmployeeDetail(CurrentEmployee.EmpID);
            hdnEmpUUID.Value = BusinessUtility.GetString(objEmp.PensivoUUID);
            hdnUserID.Value = BusinessUtility.GetString(CurrentEmployee.EmpID);
            if (BusinessUtility.GetInt(CurrentEmployee.EmpID) > 0)
            {
                Role objRole = new Role();
                if ((objRole.GetUserHasFunctionality(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)RoleAction.Administration, "") == true) || (CurrentEmployee.EmpType == EmpType.SuperAdmin))
                {
                    hdnHomeURL.Value = "AdministrationDashBoard.aspx";
                }
                else
                {
                    hdnHomeURL.Value = "employeedashboard.aspx";
                }
            }
        }
    }

    /// <summary>
    /// To Get Location HTML Content
    /// </summary>
    /// <returns>String</returns>
    private string loadEmpSearchValue()
    {
        string sbHtml = "";
        Role objRole = new Role();
        DataTable dt = new DataTable();

        DataView dv = objRole.GetSysRef(this.SearchBy, "", Globals.CurrentAppLanguageCode).DefaultView;
        dv.RowFilter = "tiles<>'' and sysRefCodeValue<>'WR'";
        dt = dv.ToTable();
        sbHtml += " <div class='boxset' id='dvAdminDashBoard'> ";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            for (int j = 0; j <= 2; j++)
            {
                if (i < dt.Rows.Count)
                {
                    string sText = BusinessUtility.GetString(dt.Rows[i]["tiles"]);
                    string sValue = BusinessUtility.GetString(dt.Rows[i]["sysRefCodeValue"]);
                    string sSubTitle = BusinessUtility.GetString(dt.Rows[i]["subtitle"]);
                    if (sText != "")
                    {
                        sbHtml += " <div class='boxset-box'> ";
                        sbHtml += string.Format(" <a id='hrfSearchEmpName' runat='server' href='#' onclick='Savelocation(\"{0}\")' class='boxset-box-wrapper'> ", sValue);
                        sbHtml += " <div class='boxset-box-inner'> ";
                        sbHtml += " <h4 class='boxset-title'> " + sText + " </h4> ";
                        sbHtml += " <div class='boxset-body'> ";
                        sbHtml += " <p>" + sSubTitle + " </p> ";
                        sbHtml += " </div> ";
                        sbHtml += " </div> ";
                        sbHtml += " </a> ";
                        sbHtml += " </div> ";
                        if (j <= 1)
                        {
                            i += 1;
                        }
                    }
                }
            }
        }
        sbHtml += " </div> ";
        if (dt.Rows.Count <= 0)
        {
            sbHtml += "<table style='width: 100%'>";
            sbHtml += "<tr>";
            sbHtml += "<td>" + Resources.Resource.lblNoRecordAvailable + "</td>";
            sbHtml += "</tr>";
            sbHtml += "</table>";
        }
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Is Ask for Location
    /// </summary>
    public string AskLocation
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["askLocation"]);
        }
    }
}