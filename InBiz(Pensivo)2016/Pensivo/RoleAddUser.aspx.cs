﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;


public partial class RoleAddRemoveUser : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;


    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvRoles))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAddUser, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            objRole = new Role();
            string sRoleName = objRole.GetRoleName(this.RoleID);
            ltrTitle.Text = Resources.Resource.lblAssociateUserWithSystemRole.Replace("#RoleName#", sRoleName);

            HdnFlag.Value = this.Flag;

            if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry))
            {
                gvRoles.Columns[3].HeaderText = Resources.Resource.lblEmpEmailID;
            }
            else
            {
                gvRoles.Columns[3].HeaderText = Resources.Resource.lblEmpoyeeID;
            }
        }

        if (!IsPostBack)
        {
            if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addUser")
            {
                try
                {
                    int userID = BusinessUtility.GetInt(Request.Form["UserID"]);
                    int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);

                    if (userID > 0 && roleID > 0)
                    {
                        List<Role> lRoleUser = new List<Role>();
                        lRoleUser.Add(new Role { UserID = userID });
                        objRole = new Role();
                        if (objRole.RoleAddUsers(roleID, lRoleUser) == true)
                        {
                            RoleManageLists objRoleManageList = new RoleManageLists();
                            objRoleManageList.UpdateEmployeeHasFunctionalityStatus(userID, "");

                            ExecutingThread.ThreadAssignedCoursesByEmployee(BusinessUtility.GetInt(userID), 0, (int)AssignedCourseInActiveReason.BySysAdminRole, (int)AssignedCourseUpdatedSource.BySysAdminRoleUpdate);
                            //ExecutingThread.ThreadEmployeeAssignedCoursesByRole(roleID, 0);
                            Response.Write("ok");
                        }
                        else
                        {
                            Response.Write("error");
                        }
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                catch
                {
                    Response.Write("error");
                }
                Response.End();
                Response.SuppressContent = true;
            }
        }
    }

    /// <summary>
    /// To Create JQ Grid Cell Binding
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Binding Event</param>
    protected void gvRoles_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 4)
        {
            objRole = new Role();
            string str = string.Empty;
            str += "<div class='marginleft'>";
            str += Resources.Resource.lblAddUserSystemRoleConfirmation.Replace("#USERDETAIL#", BusinessUtility.GetString(e.RowValues[1]) + " " + BusinessUtility.GetString(e.RowValues[2]) + " (" + BusinessUtility.GetString(e.RowValues[3]) + ")").Replace("#ROLENAME#", objRole.GetRoleName(this.RoleID));
            str += "</div>";
            string sMessage = str.Replace("\"", "{-").Replace("'", "{_");
            e.CellHtml = string.Format(@"<a href=""javascript:;""  onclick=""AddUsers({0},{1},'{2}','{3}','{4}')"">" + Resources.Resource.lblAdd + "</a>", e.CellHtml, this.RoleID, this.SearchBy, Utils.ReplaceDBSpecialCharacter(this.SearchValue), sMessage);
        }
    }

    /// <summary>
    /// To Bind JQ Grid with datasource
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event</param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Role objRole = new Role();
        gvRoles.DataSource = objRole.GetUsersNotInRole(this.RoleID, Utils.ReplaceDBSpecialCharacter(this.SearchBy), Utils.ReplaceDBSpecialCharacter(Server.UrlDecode(this.SearchValue)));
        gvRoles.DataBind();
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

}