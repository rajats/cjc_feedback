var pibLabelSub = { // abbreviation for "label substitute".
	
	init: function() {
		
		// define object properties.
		
		pibLabelSub.tooltip = $(".plms-tooltip.label-replacement"),
		pibLabelSub.fieldset = pibLabelSub.tooltip.parents(".plms-fieldset"),
		pibLabelSub.label = pibLabelSub.fieldset.find(".plms-label"),
		pibLabelSub.input = pibLabelSub.fieldset.find(".plms-input"),
		pibLabelSub.textarea = pibLabelSub.fieldset.find(".plms-textarea"),
		pibLabelSub.select = pibLabelSub.fieldset.find(".plms-select");
		
		// event handlers.
		
		pibLabelSub.select.bind("change", function() {
			var option_index = $(this).find("option:selected").index();
			if (option_index > 0) {
				$(this).removeClass("is-placeholder");
			} else {
				$(this).addClass("is-placeholder");
			}
		}).change(); // change.
		
		pibLabelSub.select.bind("blur change", function() {
			var label = $(this).parent().siblings("label");
			if (!$(this).hasClass("is-placeholder")) {
				pibLabelSub.showLabel(label);
			} else {
				pibLabelSub.hideLabel(label);
			}
		}).blur(); // blur, change.
		
		pibLabelSub.input.add(pibLabelSub.textarea).bind("keyup focus", function() {
			
			var tooltip = $(this).siblings(".plms-tooltip.label-replacement");
			var label = $(this).parent().siblings("label");
			
			if (pibLabelSub.charCount($(this)) == 0) {
				pibLabelSub.hideTooltip(tooltip);
				pibLabelSub.hideLabel(label);
			} else if (!label.hasClass("is-hidden")) {
				pibLabelSub.hideTooltip(tooltip);
			} else {
				pibLabelSub.showTooltip(tooltip);
			}
			
		}); // keyup, focus.
		
		pibLabelSub.input.add(pibLabelSub.textarea).bind("blur change", function() {
			
			var tooltip = $(this).siblings(".plms-tooltip.label-replacement");
			var label = $(this).parent().siblings("label");
			
			if (pibLabelSub.charCount($(this)) > 0) {
				pibLabelSub.showLabel(label);
				pibLabelSub.hideTooltip(tooltip);
			} else {
				pibLabelSub.hideLabel(label);
			}
			
		}).blur(); // blur, change.
		
	}, // init();
	
	charCount: function(el) {
		return el.val().length;
	},
	
	showTooltip: function(tooltip) {
		tooltip.fadeIn(250);
	},
	
	hideTooltip: function(tooltip) {
		tooltip.fadeOut(250);
	},
	
	showLabel: function(label) {
		label.removeClass("is-hidden");
	},
	
	hideLabel: function(label) {
		label.addClass("is-hidden");
	}
	
};

// !! only applies to browsers w/ placeholder attribute support.
if ($("html.placeholder").length > 0) pibLabelSub.init();