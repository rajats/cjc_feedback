// !! this should be re-written following the literal object notation.

var show_aside = false;

function asideToggle() {
	
	// console.log("aside toggle function.");
	
	var parel = $(".layout-sidebar-right"); // controls our selector scope.
	
	if (parel.length > 0) {
		var toggle_handle = parel.find(".aside-toggle-handle");
		var menu_btn = toggle_handle.find(".btn");
		var aside_nav = parel.find(".aside-main-nav");
		var main_content_body = parel.find("> .boxed-content");
		var btnstr = [menu_btn.find("span").text(), "Show Menu"];
		
		//console.log(btnstr);
		
		// if our show_aside variable is truthy,
		if (show_aside) {
			parel.removeClass("slim"); // remove the slim class from parent element,
			menu_btn.find("span").text(btnstr[1]); // display alternate button text value.
		}
		
		/**
		* @desc Toggle aside visibility
		*/
		
		// when menu button is clicked,
		menu_btn.click(function() {
			
			parel.toggleClass("slim");
			
			if (parel.hasClass("slim")) {
				aside_nav.hide(); // hide <ul> if slim class present.
				show_aside = false;
				menu_btn.find("span").text(btnstr[1]);
			} else {
				aside_nav.show(); // show <ul> by default,
				show_aside = true;
				menu_btn.find("span").text(btnstr[0]);
			}
		}); // .click();
		
		if (parel.hasClass("slim")) { // onload event.
			aside_nav.hide();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		* @desc Show tooltip module on hover event explaining the 
		* button purpose and functionality.
		*/
		
		// First up: create markup for tooltip.
		// We want to exclude this from the core content / markup.
		// Doing so means screen readers and similar tools or conditions,
		// will not read our behavioural / instructional content.
		
		var tooltip = $('<p><i class="tooltip-arrow-icon"></i>Click this button to toggle the main sections menu visibility.</p>');
		
		tooltip.appendTo(toggle_handle.find(".flyout-message-wrapper"));
		
		// now that our element is created, let's find it in DOM and store the DOM object as our tooltip var.
		tooltip = parel.find(".flyout-message-wrapper p");
		
		menu_btn.hover(function() {
			tooltip.addClass("is-active");
		}, function() {
			tooltip.removeClass("is-active");
		}); // .hover();
	}
		
} // asideToggle();

asideToggle();
