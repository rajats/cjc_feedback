(function() {
	
	var body = $("body");
	var login_modal = $(".plms-modal.login");
	var login_modal_box = login_modal.find(".plms-modal-box");
	var login_handle = $('[data-modal-id="login"]');
	var hide_modal_handle = login_modal.find('[data-action="close-modal"]');

	// SHOW MODAL

	login_handle.click(function() {
		// first, add class to body.
		// doing so will lock scroll bars.
		body.addClass("lockdown");
		login_modal.addClass("active");
		setTimeout(function() {
		login_modal_box.addClass("flyin"); // initiates css3 animation.
		}, 150);
	}); // click();

	// HIDE MODAL

	hide_modal_handle.click(function() {
		// remove "lockdown" class from body.
		body.removeClass("lockdown");
		// hide box (slide up using css3 animation)
		login_modal_box.removeClass("flyin");
		// hide modal.
		login_modal.removeClass("active");
	}); // click();

	// console.log(login_modal);

})();
