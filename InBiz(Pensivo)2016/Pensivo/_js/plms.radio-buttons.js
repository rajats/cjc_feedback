(function() {
	// look for instances of .btngrp.radio-group
	var parEl = $(".btngrp.radio-group"); // control selector scope.
	var radioButton = parEl.find(".btn");
	radioButton.click(function(e) {
		e.preventDefault();
		// is this a <button /> element? If so, disable default event behaviour.
		$(this).toggleClass("selected");
	}); // click();
})();