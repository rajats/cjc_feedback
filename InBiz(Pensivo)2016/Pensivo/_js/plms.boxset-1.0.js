(function() {
	
	// some browsers don't support pseudo-class selectors,
	// for now, all browsers are affected (no browser sniffing)
	// add a class to box representing it's pseudo-class selector.
	
	// !! worth noting, there are "pseudo-class" polyfill(s)
	// consider / evaluate worthiness at a later time.
	
	var boxset_box = $(".boxset-box");
	
	boxset_box.each(function() {
		if ($(this).is(":nth-child(3n+1)")) $(this).addClass("nth-child-3np1");
		if ($(this).is(":nth-child(3n-1)")) $(this).addClass("nth-child-3nm1");
		if ($(this).is(":nth-child(3n+3)")) $(this).addClass("nth-child-3np3");
	});

})();
