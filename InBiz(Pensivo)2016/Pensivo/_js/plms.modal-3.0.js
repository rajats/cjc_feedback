// let's create a literal object.

var pibModal = {
	
	html: $("html"),
	body: $("body"),
	modalHandle: $("[data-modal-id]"),
	closeHandles: $('[data-action="close-modal"]'),
	
	// !! next up, object customization options.
	// fade in delay.
	// number of buttons,
	// e.g. width of content box.
	// also, pull up scroll bar on window load event.
	
	// instead of having all "modal" modules in DOM, we should fetch data as per needed.
	
	config: {
	},
	
	init: function(config) {
		pibModal.modalHandle.click(function() {
			pibModal.showModal($(this).attr("data-modal-id"));
		}); // click();
		pibModal.closeHandles.click(function() {
			pibModal.hideModal();
		}); // click();
		// pibModal.showModal(config);
	},
	
	showModal: function(id) {
		pibModal.init();
		var modal = $(".plms-modal."+id);
		modal.addClass("active");
		var modalBox = modal.find(".plms-modal-box");
		modal.scrollTop(0);
		setTimeout(function() {
			modalBox.addClass("flyin");
		}, 500);
		pibModal.disableScroll();
	},
	
	disableScroll: function() {
		pibModal.html.add(pibModal.body).addClass("lockdown");
	},
	
	hideModal: function() {
		var modal = $(".plms-modal");
		var modalBox = $(".plms-modal-box");
		modalBox.removeClass("flyin");
		window.setTimeout(function() {
			modal.removeClass("active");
		}, 500);
		pibModal.enableScroll();
	},
	
	enableScroll: function() {
		pibModal.html.add(pibModal.body).removeClass("lockdown");
	}
};

pibModal.init();