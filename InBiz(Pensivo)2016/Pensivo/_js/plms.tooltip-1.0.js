(function() {
	
	// for more information on the methods used to achieve this, 
	// see this article: http://css-tricks.com/dangers-stopping-event-propagation/
	
	$(document).click(function(event) {
		
		// (1) Evaluate what DOM element has been clicked.
		// If the selector has the attribute "data-tooltip-handle",
		// we should show the corresponding "plms-tooltip"
		// while hiding all others visible on page.
		
		// (2) If any other DOM selector is clicked, we should hide the .plms-tooltip but, with some exceptions.
		// if the DOM selector clicked is either the .plms-tooltip itself or a child of the .plms-tooltip,
		// we should circumvent.
		
		// (3) Lastly, we should hide the .plms-tooltip in the event that the selector clicked is 
		// in fact either a child of or the handle itself but the .plms-tooltip is already visible.
		
		var eventTarget = $(event.target);
		var tooltips = $(".plms-tooltip");
		
		if (eventTarget.closest("[data-tooltip-handle]").length > 0) {
			
			var tooltip;
			
			// This is our corresponding tooltip element relative to our handle.
			tooltip = eventTarget.siblings(".plms-tooltip");
			
			// we need to traverse the DOM differently
			// if this is a child of the data-tooltip-handle (e.g. child selector).	
			if (tooltip.length < 1) {
				tooltip = eventTarget.parent().siblings(".plms-tooltip");
			}
			
			// console.log(tooltip);
			
			// before we show our corresponding tooltip, let's hide all tooltips.
			tooltips.removeClass("active");
			
			// and finally, show the corresponding tooltip.
			tooltip.addClass("active");
			
			// !! IE 7: "z-index" (layering) is all fudged.
			// We need to set a higher z-index value to the active <header /> element.
			
			var headers = $(".plms-accordion-header");
			var header = eventTarget.parents(".plms-accordion-header");
			
			headers.removeClass("tooltip-on");
			header.addClass("tooltip-on");
			
			 // < IE 7 (end)
			
		} else if (eventTarget.closest(".plms-tooltip").length > 0) {
			
			// do not hide anything if this is either the .plms-tooltip itself or child of.
			// console.log(eventTarget.closest(".plms-tooltip").length);
			
		} else {
			
			// hide all tooltip elements.
			tooltips.removeClass("active");			
			
		}
		
	}); // click();
    
})();