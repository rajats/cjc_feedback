(function() {
	
	/*
	!! The art of intuitive graceful degradation:
	Our less capable browsers (IE 7, IE 8) do not get 
	the fixed position, instead they resort to standard 
	scroll.
	*/
	
	if ($(".lt-ie9").length > 0) return;
	
	var sticky = $(".sticky");
	var num_sticky = sticky.length;
	var primary_bar = $(".topbar.primary");
	var secondary_bar = $(".topbar.secondary");
	var tertiary_bar = $(".topbar.tertiary");
	var main_header = $("#main-header");
	var scenario;
	
	function stickiesHeight() {
		var stickies_height = 0;
		sticky.each(function() {
			stickies_height += $(this).outerHeight(true);
		}); // .each();
		return stickies_height;
	} // stickiesHeight();
	
	stickiesHeight();
	
	// regardless of scenarios, we wrap all sticky elements in a div.
	// that div, will toggle from a fixed to static position.
	
	var sticky_wrapper = $('<div class="sticky-wrapper" />');
	sticky.wrapAll(sticky_wrapper);
	// this point onwards, sticky_wrapper variable will refer to DOM element.
	sticky_wrapper = $(".sticky-wrapper");
	
	// each scenario is treated slightly differently,
	// for that reason, we first determine what scenario is applicable.
	
	if (primary_bar.hasClass("sticky") && num_sticky == 1) scenario = 1;
	if (secondary_bar.hasClass("sticky") && num_sticky == 1) scenario = 2;
	if (tertiary_bar.hasClass("sticky") && num_sticky == 1) scenario = 3;
	if (primary_bar.hasClass("sticky") && secondary_bar.hasClass("sticky")) scenario = 4;
	if (primary_bar.hasClass("sticky") && tertiary_bar.hasClass("sticky")) scenario = 5;
	if (secondary_bar.hasClass("sticky") && tertiary_bar.hasClass("sticky")) scenario = 6;
	if (num_sticky == 3) scenario = 7;
	
	// console.log("scenario: ", scenario);
	
	// now that we know which card we are dealt,
	// we can perform different logic for each scenario.
	
	function stickyFixed() {
	
	// -------------------------------------------------------------------------------------
	
	if (scenario == 1) {
		
		// @desc: In this scenario, only the primary navigation is fixed.
			
		var scrollTop = $(window).scrollTop();
		
		if (scrollTop > secondary_bar.outerHeight(true)) {
			main_header.css("padding-top", primary_bar.outerHeight(true));
			sticky_wrapper.addClass("fixed");
		} else {
			main_header.css("padding-top", 0);
			sticky_wrapper.removeClass("fixed");
		}
		
	} // scenario 1 (end)
	
	// -------------------------------------------------------------------------------------
	
	if (scenario == 2) {
		
		// @desc: In this scenario, only secondary bar is fixed.
		// bar is initially fixed.
		
		main_header.css("padding-top", secondary_bar.outerHeight());
		sticky_wrapper.addClass("fixed");
		
	}
	
	// -------------------------------------------------------------------------------------
	
	if (scenario == 3) {
		
		// @desc: In this scenario, only tertiary bar is fixed.
		// bar is "fixed" when scroll bar location (top) is greater
		// than height of primary and secondary elements.
	
		var scrollTop = $(window).scrollTop();
		var siblings_height = primary_bar.outerHeight(true) + secondary_bar.outerHeight(true);
		
		if (scrollTop > siblings_height) {
			main_header.css("padding-top", tertiary_bar.outerHeight(true));
			sticky_wrapper.addClass("fixed");
		} else {
			main_header.css("padding-top", 0);
			sticky_wrapper.removeClass("fixed");
		}
		
	} // scenario 3 (end)
	
	// -------------------------------------------------------------------------------------
	
	if (scenario == 4) {
		
		// @desc: In this scenario, primary and secondary bars are fixed.
		// bars are fixed on onload event.
		
		// secondary_bar.css("padding-bottom", 0);
		main_header.css("padding-top", stickiesHeight());
		sticky_wrapper.addClass("fixed");
		
	} // scenario 4 (end)
	
	// -------------------------------------------------------------------------------------
	
	if (scenario == 5) {
		
		// @desc: In this scenario, only the primary navigation is fixed.
			
		var scrollTop = $(window).scrollTop();
		var sum = primary_bar.outerHeight(true) + tertiary_bar.outerHeight(true);
		
		if (scrollTop > secondary_bar.outerHeight(true)) {
			main_header.css("padding-top", sum);
			sticky_wrapper.addClass("fixed");
		} else {
			main_header.css("padding-top", 0);
			sticky_wrapper.removeClass("fixed");
		}
		
	} // scenario 5 (end)
	
	// -------------------------------------------------------------------------------------
	
	if (scenario == 6) {}
	
	// -------------------------------------------------------------------------------------
	
	if (scenario == 7) {
		
		// @desc: In this scenario, all bars are fixed.
		// bars are fixed on onload event.
		
		// secondary_bar.css("padding-bottom", 0);
		main_header.css("padding-top", stickiesHeight());
		sticky_wrapper.addClass("fixed");
		
	} // scenario 7 (end)
	
	// -------------------------------------------------------------------------------------
	
	} // stickyFixed();
	
	////////////////////////////////////////////////////////////////////////////////////////
	
	$(window).scroll(function() {
	stickyFixed();
	}); // .scroll();

	$(document).ready(function() {
	stickyFixed();
	}); // .ready();

})();
