(function() {
	
	$(document).mouseup(function(e) {
		
		// first, let's detect whether or not the clicked DOM element has a "data-tooltip-handle" attribute.
		// if it doesn't, we can "return" / skip.
		var thisEl = $(e.target);
		var tooltips = $(".plms-tooltip");
		var tooltipHandle = thisEl.is("[data-tooltip-handle]");
		var tooltipHandleChild = thisEl.parents().is("[data-tooltip-handle]");
		// we'll also need to check if "child of" tooltip element.
		
		// !! should not execute if not a tooltip handle or child of.
			
		if (tooltipHandle || tooltipHandleChild) {
			console.log(thisEl);
			console.log("Child of tooltip handle? ", tooltipHandleChild);
			// (1) show the tooltip.
			var tooltip = thisEl.siblings(".plms-tooltip");
			tooltips.removeClass("active"); // hide all other tooltips.
			tooltip.addClass("active"); // show appropriate tooltip.
			console.log("Tooltip: ", thisEl.siblings(".plms-tooltip"));
		}
		
		// && $(this).has(e.target).length === 0) { // nor a descendant of the container.
		
		// console.log(thisEl);
		// console.log(tooltips);
		
		
	}); // mouseup();
    
    // when progress button is clicked,
    // toggle tooltip contents display.
    /*
    $('[href="#progress"]').click(function() {
		var header_els = $(".plms-accordion-header");
		var tooltip_els = $(".plms-tooltip");
		var this_header = $(this).parents(".plms-accordion-header"); 
		var this_tooltip = $(this).parent().find(".plms-tooltip");
		// first, hide all tooltip elements.
		tooltip_els.removeClass("active");
		// second, show this tooltip.
		this_tooltip.toggleClass("active");
		// make sure tooltip floats atop other siblings.
		header.toggleClass("tooltip-on");
    }); // click();
    */
    
    /*
    // when progress button is clicked,
    // toggle tooltip contents display.
    $('[href="#progress"]').click(function() {
		var header = $(this).parents(".plms-accordion-header");
		// $(".plms-tooltip").removeClass("active");
		$(this).parent().find(".plms-tooltip").toggleClass("active");
		header.toggleClass("tooltip-on");
    });

	// if anything aside from the progress box is clicked,
	// hide progress box.
	
	$(document).mouseup(function(e) {
		
		var tooltip = $(".plms-tooltip");
		
		tooltip.each(function() {
			if (!$(this).is(e.target) // if the target of the click isn't the container,
			&& !$('[data-tooltip-handle]').is(e.target) // nor the tooltip handle (usually a button),
			&& $(this).has(e.target).length === 0) { // nor a descendant of the container.
				$(this).removeClass("active");
				// Because of a z-index stacking problem in IE 7 browser,
				// we need to toggle a class on the accordion header container element.
				// Doing so assigns a higher z-index css property and value to accordions where tooltip is visible / on.
				$(this).parents(".plms-accordion-header").removeClass("tooltip-on");
			}
		}); // each();
		
		// console.log($(e.target).parents(".plms-tooltip"));
		
	}); // mouseup();

	// the way we toggle visibility is as following:
	// each "tooltip" module has a custom "data-tooltip-id" attribute
	// somewhere (anywhere) in the DOM is an element with a "data-tooltip-handle" attribute,
	// when those attribute values share the same value, we show / hide the tooltip.
	
	// find all instances of "data-tooltip-id" attribute
	var tooltips = $('[data-tooltip-id]');
	
	tooltips.each(function() {
		
		var tooltip = $(this);
		// get the attribute value.
		var val = tooltip.attr("data-tooltip-id");
		// now, let's find the corresponding selector / value.
		var handle = $('[data-tooltip-handle="'+val+'"]');
		
		// console.log("value:", val);
		// console.log("handle selector:", handle);
		
		handle.click(function() {
			// hide all other "tooltip" elements.
			$(".plms-tooltip").not(tooltip).removeClass("active");
			// show this tooltip.
			tooltip.toggleClass("active");
		}); // click();
		
	}); // each();
	*/
    
})();