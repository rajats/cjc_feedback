(function() {
	
	// !! only applies if browser supports placeholder attribute.
	
	if ($("html.placeholder").length < 1) return;
	
	// find all label replacement tooltips.
	var tooltip = $(".plms-tooltip.label-replacement");
	
	tooltip.each(function() {
		
		var tooltip = $(this);
		var fieldset = tooltip.parents(".plms-fieldset");
		var label = fieldset.find(".plms-label");
		var input = fieldset.find(".plms-input");
		var textarea = fieldset.find(".plms-textarea");
		var select = fieldset.find(".plms-select");
		
		// on focus event, hide label.
		input.add(textarea).focus(function() {
			if ($(this).val() == "") {tooltip.fadeIn(250);}
		}); // focus();

		input.add(textarea).blur(function() {
			tooltip.fadeOut(100);
			if ($(this).val()) {
				label.css("visibility", "visible");
				label.animate({
				"margin-top": "10px",
				"opacity": 1
				}, 150); // animate();
			} else {
				label.animate({
				"margin-top": 0,
				"opacity": 0
				}, 150); // animate();
				label.css("visibility", "hidden");
			}
		}); // blur();

		// ----------------------------------------------------------------- //
		
		// anything below applies to "select" elements.
		
		function selectClassToggle() {
			var option_index = select.find("option:selected").index();
			if (option_index > 0) {
				select.removeClass("is-placeholder");
			} else {
				select.addClass("is-placeholder");
			}
		} // selectClassToggle();
		
		select.change(function() {
			selectClassToggle();
		}); // change();
		
		select.blur(function() {
			if (!select.hasClass("is-placeholder")) {
				label.css("visibility", "visible");
				label.animate({
				"margin-top": "10px",
				"opacity": 1
				}, 150); // animate();
			} else {
				label.animate({
				"margin-top": 0,
				"opacity": 0
				}, 150); // animate();
				label.css("visibility", "hidden");
			}
		}); // blur();
		
		// ----------------------------------------------------------------- //
		
		// affects both input[type="text"] and <select /> elements.
		
		$(document).ready(function() {
			selectClassToggle();
			
			function showLabel() {
				label.css({
					"visibility": "visible", 
					"opacity": 1,
					"margin-top": "10px"
				});
			} // showLabel();
			
			if (select.length > 0) { // only affect <select /> elements.
				if (!select.hasClass("is-placeholder")) {
					showLabel();
				}
			}
			
			if (input.length > 0) { // only affect input[type="text"] elements.
				if (input.val() != "") {
					showLabel();
				}
			}
		}); // ready();
		
	}); // each();

})();




