(function() {
	
	var scroll_el = $(".search-box");
	
	// does scroll element exist?
	
	if (scroll_el.length < 1) return;
	
	var scroll_el_width = $("#main-content .fixed-onscroll").width();
	var win_h = $(window).height();
	var doc_h = $(document).height();
	var scroll_loc = $(window).scrollTop();
	var xcoord = Math.round($("#main-content .wrapper").offset().left);
	
	// first, get the "x" coordinate of our scroll element
	// doing so will allow us to determine when it should
	// switch classes and become "fixed".
	
	var offset_top = Math.round(scroll_el.offset().top);
	var offset_left = Math.round(scroll_el.offset().left);
	
	function isFixedToggle() {
		
		// we don't want this behaviour for small screens.
		
		if ($(window).width() < 700) {
			scroll_el.addClass("is-static");
		} else {
			scroll_el.removeClass("is-static");
		}
		
		scroll_el.css("width", scroll_el_width);
		
		if ( (scroll_loc+20) >= offset_top ) {
			scroll_el.addClass("is-fixed");
			scroll_el.css("left", xcoord);
		} else {
			scroll_el.removeClass("is-fixed");
		}
		
	} // isFixedToggle();
	
	isFixedToggle();
	
	$(window).scroll(function() {
		scroll_loc = $(window).scrollTop();
		isFixedToggle();
	}); // scroll();
	
	$(window).resize(function() {
		xcoord = Math.round($("#main-content .wrapper").offset().left);
		scroll_el_width = $("#main-content .fixed-onscroll").width();
		isFixedToggle();
	}); // resize();
	
	
	
	
	
	
	
	
	
})();