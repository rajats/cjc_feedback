(function() {
	
	var fontsize_tooltip = $(".plms-tooltip.fontsize");
	var fontsize_menu = fontsize_tooltip.find("select");
	
	// change <select /> option on DOM load to "medium".
	fontsize_menu.find("option:eq(1)").attr("selected", "selected");
	
	fontsize_tooltip.change(function() {
		
		var menu_index = $(this).find(":selected").index();
		
		// remove all classes before adding.
		$("html").removeClass("font-size-s font-size-m font-size-l");
		
		// hide tooltip element.
		fontsize_tooltip.removeClass("active");

		switch (menu_index) {
		case 0 :
			$("html").addClass("font-size-s");
			break;
		case 1 :
			$("html").addClass("font-size-m");
			break;
		case 2 :
			$("html").addClass("font-size-l");
			break;
		}
		
	}); // click();
	
})();
