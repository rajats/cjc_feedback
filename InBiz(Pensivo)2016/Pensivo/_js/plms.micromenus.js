(function() {

	// var debug_mode = true;
	var mainmenu_handle = $(".menu-handle");
	var menu = $(".globalnav-main ul");
	var menu_height = menu.outerHeight();
	var window_height;
	var window_width;
	var menu_top_pos;
	var menu_maxheight_val;
	// console.log(mainmenu_handle);
	
	// To prevent errors, we need to make sure our menu exists.
	// if not, return.
	
	if (menu.length < 1) return;
	
	function menuMaxHeight() {
		
		// problem: Items become inaccessible when the menu height is greater than the window height.
		// solution: we need to do the following...
		
		// 2: what is the window height?
		window_height = $(window).height();
		
		// 3: what is the top position of our menu?
		menu_top_pos = menu.offset().top;
		// !! window and not document relative when we subtract window scroll top.
		menu_top_pos = menu_top_pos - $(window).scrollTop();
		
		// 4: substract the window height by the value of the top position of our menu
		menu_maxheight_val = window_height - menu_top_pos;
		
		// if console object exists, AND our debug mode variable is truthy and exits.
		// !! breaks IE 9, need to change this so it doesn't affect IE browsers.
		/*
		if (console && typeof debug_mode !== "undefined") {
			console.log("Menu height: ", menu_height);
			console.log("Window Height: ", window_height);
			// !! expect problem(s) if this value (below) changes.
			console.log("Menu Top Position: ", menu_top_pos);
			console.log("Menu Max Height Value: ", menu_maxheight_val);
		}
		*/
		
		// if this is truthy, likely our menu is not visible; therefore — do nothing / skip.
		if (menu_top_pos <= 0) return;
		
		// apply this max height value only if our menu height is
		// greater than the window height minus the menu top position.
		// if (menu_height > (window_height - menu_top_pos)) {
		// }
		
		// 5: assign the max height value to menu <ul />
		menu.css("max-height", menu_maxheight_val);
		
	} // menuMaxHeight();
	
	mainmenu_handle.click(function() {
		menu.toggleClass("active");
		mainmenu_handle.toggleClass("active");
		menuMaxHeight();
	}); // click();
	
	$(window).resize(function() {
		// before we do anything, close menu.
		menu.removeClass("active");
		menuMaxHeight();
		window_width = $(this).width();
		// !! this value is dependent of css breaking point declaration.
		if (window_width >= "850") menu.css("max-height", "auto");
	}); // resize();

})();