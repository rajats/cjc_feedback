(function() {
    
    // when progress button is clicked,
    // toggle tooltip contents display.
    $('[href="#progress"]').click(function() {
		var header = $(this).parents(".plms-accordion-header");
		$(this).parent().find(".plms-tooltip").toggleClass("active");
		header.toggleClass("tooltip-on");
    });

	// if anything aside from the progress box is clicked,
	// hide progress box.
	
	$(document).mouseup(function(e) {
		
		var tooltip = $(".plms-tooltip");
		
		tooltip.each(function() {
			if (!$(this).is(e.target) // if the target of the click isn't the container,
			&& !$('[data-tooltip-handle]').is(e.target) // nor the tooltip handle (usually a button),
			&& $(this).has(e.target).length === 0) { // nor a descendant of the container.
				$(this).removeClass("active");
				// Because of a z-index stacking problem in IE 7 browser,
				// we need to toggle a class on the accordion header container element.
				// Doing so assigns a higher z-index css property and value to accordions where tooltip is visible / on.
				$(this).parents(".plms-accordion-header").removeClass("tooltip-on");
			}
		}); // each();
		
	}); // mouseup();

	// the way we toggle visibility is as following:
	// each "tooltip" module has a custom "data-tooltip-id" attribute
	// somewhere (anywhere) in the DOM is an element with a "data-tooltip-handle" attribute,
	// when those attribute values share the same value, we show / hide the tooltip.
	
	// find all instances of "data-tooltip-id" attribute
	var tooltips = $('[data-tooltip-id]');
	
	tooltips.each(function() {
		
		var tooltip = $(this);
		// get the attribute value.
		var val = tooltip.attr("data-tooltip-id");
		// now, let's find the corresponding selector / value.
		var handle = $('[data-tooltip-handle="'+val+'"]');
		
		// console.log("value:", val);
		// console.log("handle selector:", handle);
		
		handle.click(function() {
			// hide all other "tooltip" elements.
			$(".plms-tooltip").not(tooltip).removeClass("active");
			// show this tooltip.
			tooltip.toggleClass("active");
		}); // click();
		
	}); // each();
    
})();