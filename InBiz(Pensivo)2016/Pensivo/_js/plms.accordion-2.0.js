(function() {
	var accordion = $(".plms-accordion");
	
	accordion.each(function() {
		// if disabled class, do nothing.
		if ($(this).hasClass("disabled")) return;
		// look for "data-toggle-handle" attribute,
		// if value is header, toggle body when "header" is clicked.
		// if value is icon, only toggle body when icon is clicked.
		var accordion = $(this);
		var toggle_handle = $(this).attr('data-toggle-handle');
		var icon = $(this).find(".openclose-icon");
		var icon2 = $(this).find(".plms-icon-accordion");
		var header = $(this).find(".plms-accordion-header");
		var body = $(this).find(".plms-accordion-body");
		
		function toggleStuff() {
			if (!$("html.lt-ie8").length > 0) body.slideToggle(); // bypass IE 7.
			icon.toggleClass("opened");
			icon.toggleClass("closed");
			accordion.toggleClass("is-open");
			icon2.toggleClass("chevron-down");
		} // toggleStuff();
		
		if (toggle_handle == "header") {
			header.click(function() {
				toggleStuff();
			}); // click();
		} else if (toggle_handle == "icon" || typeof(toggle_handle) === "undefined") {
			icon.click(function() {
				toggleStuff();
			}); // click();
		}
		
		// if none specified, fallback to default (entire header clickable)
	}); // each();
})();