﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeeSiteDetail.aspx.cs" Inherits="EmployeeSiteDetail" %>
<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada )  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
        <%else if (Utils.TrainingInst == (int)Institute.EDE2 )  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>

    <%--Define Inline Css To Adjust Grid Button Font Size--%>
    <style type="text/css">
        .locationDesc {
            font-size: 12px;
            font-weight: bold;
        }
    </style>

    <section id="main-content" class="pg-advanced-list">
        <div class="wrapper">
            <header class="main-content-header">
                <h2>
                    <%= Resources.Resource.lblMyCourses %>
                </h2>
                <h5>
                    <%= Resources.Resource.lblBeforwegetStarted %>
                </h5>
                <h6>
                    <%= Resources.Resource.lblEmpSiteSelectionMessage %>
                </h6>
            </header>
            <div class="location-filter" id="SearchPanel">
                <div class="column span-4 city">
                    <label for="txtCity"><%= Resources.Resource.lblCity%></label>
                    <asp:Label ID="Label3" AssociatedControlID="txtCity" class="filter-key plms-label is-hidden" runat="server"
                        for="txtCity"></asp:Label>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="plms-input skin2" placeholder="<%$ Resources:Resource, lblCity %>"></asp:TextBox>
                    <p class="conjunction">AND/OR</p>
                </div>
                <div class="column span-4 street-name">
                    <label for="txtStreetName"><%= Resources.Resource.lblStreetName%></label>
                    <asp:Label ID="Label1" AssociatedControlID="txtStreetName" class="filter-key plms-label is-hidden" runat="server"
                        for="txtStreetName"></asp:Label>
                    <asp:TextBox ID="txtStreetName" runat="server" CssClass="plms-input skin2" placeholder="<%$ Resources:Resource, lblStreetName %>"></asp:TextBox>
                    <div class="button">
                        <input id="btnSearch" class="btn large" type="button" value="<%=Resources.Resource.lbSearch%>" />
                    </div>
                </div>
            </div>
            <div style="clear: both; height: 20px;"></div>
            <div class="main-content-body column span-8" style="padding-left: 0px; width: 70%;">
                <div id="grid_wrapper" style="width: 100%;" onkeypress="return disableEnterKey(event)">
                    <trirand:JQGrid runat="server" ID="grdSiteDetail" Height="300px"
                        AutoWidth="True" OnDataRequesting="grdSiteDetail_DataRequesting" OnCellBinding="grdSiteDetail_CellBinding">
                        <Columns>
                            <trirand:JQGridColumn DataField="dept_id" Visible="false" PrimaryKey="True" />
                            <trirand:JQGridColumn DataField="descr" HeaderText="<%$ Resources:Resource, lblLocation %>"
                                Editable="false" Width="200" />
                            <trirand:JQGridColumn DataField="dept_id" HeaderText="<%$ Resources:Resource, lblSelect %>"
                                Sortable="false" TextAlign="Center" Width="20" />
                            <trirand:JQGridColumn DataField="plaza" Visible="false"
                                Editable="false" Width="200" />
                            <trirand:JQGridColumn DataField="address" Visible="false"
                                Editable="false" Width="200" />
                            <trirand:JQGridColumn DataField="city" Visible="false"
                                Editable="false" Width="200" />
                        </Columns>
                        <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn=""></SortSettings>
                        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                        <ClientSideEvents LoadComplete="loadComplete" />
                    </trirand:JQGrid>
                </div>
                <asp:HiddenField ID="hdnQuery" runat="server" />
                <asp:HiddenField ID="hdnAskSite" runat="server" />
                <asp:HiddenField ID="hdnEmpID" runat="server" />
                <asp:HiddenField ID="hdnEmpUUID" runat="server" />
                <asp:HiddenField ID="hdnHomeURL" runat="server" />
            </div>
        </div>

    </section>
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=grdSiteDetail.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "SearchPanel",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        // Resized JQ Grid
        function jqGridResize() {
            $("#<%=grdSiteDetail.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // Define JQ Grid Load Complete
        function loadComplete(data) {
            jqGridResize();
        }

        // To Show Add Site Info Confirmation 
        function AddSiteInfo(siteID, sitedesc, locationDetail) {
            var title = "Confirmation";
            var messageText = "<%=Resources.Resource.lblMessageSiteSelectionConfirmation%>".replace("#STROEADDRESS#", locationDetail);
            okButtonText = "<%=Resources.Resource.lblThatRight%>";
            LaterCnclButtonText = "<%=Resources.Resource.lblTryAgain%>";
            okButtonRedirectlink = "ConfirmRemoveUserOk('" + siteID + "' , '" + sitedesc + "')";
            LaterCnclButtonRedirectLink = "ConfirmRemoveUsrCancel();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Define Site Infor Confirmation OK Event and Move to Next Page
        function ConfirmRemoveUserOk(siteID, sitedesc) {
            if ($('#<%=hdnAskSite.ClientID%>').val() == "1") {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "addSite";
                datatoPost.SearchBy = siteID;
                datatoPost.SearchValue = sitedesc;
                datatoPost.EmpID = $('#<%=hdnEmpID.ClientID%>').val();
                datatoPost.EmpUUID = $('#<%=hdnEmpUUID.ClientID%>').val();

                $.post("EmployeeSiteDetail.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        window.location.href = $('#<%=hdnHomeURL.ClientID%>').val();
                    }
                    else {
                        ShowPensivoMessage("<%=Resources.Resource.lblContactAdministrator%>");
                    }
                });
            }
            else {
                window.location.href = "EmployeeLocation.aspx" + $("#<%=hdnQuery.ClientID%>").val() + "&searchbySite=" + siteID + "&searchbySiteValue=" + sitedesc
            }
        }

        // To Define Site Infor Confirmation Cancel Event and Move to Next Page
        function ConfirmRemoveUsrCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
            window.location = window.location;
        }

        $(document).ready(function () {
            $("#<%=txtCity.ClientID%>").focus();
              });
    </script>
</asp:Content>
