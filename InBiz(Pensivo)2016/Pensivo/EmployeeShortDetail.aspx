﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeeShortDetail.aspx.cs" Inherits="EmployeeShortDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <asp:Panel ID="cntPannel" runat="server" DefaultButton="btnSearchByName">
            <div class="wrapper width-med">
                <h1><%= Resources.Resource.lblCreateNewAccount%>  </h1>
                <div class="boxed-content">
                    <div class="boxed-content-body">
                        <header class="form-section-header">
                            <h6><%= Resources.Resource.lblPersonalInformation%>  </h6>
                            <p>
                                <asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:Resource, lblEmpDetailMesage %>"></asp:Label>
                            </p>
                        </header>
                        <div class="plms-fieldset">
                            <label class="plms-label is-hidden" for="first-name"><%= Resources.Resource.lblEmpFirstName%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtFirstName" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblEmpFirstName %>" />
                                <asp:RequiredFieldValidator ID="rfSearchName" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic"
                                    ControlToValidate="txtFirstName" Text="<%$ Resources:Resource, reqFirstName %>" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEmpFirstName%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label is-hidden" for="last-name"><%= Resources.Resource.lblEmpLastName%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtLastName" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblEmpLastName %>" />
                                <asp:RequiredFieldValidator ID="rfLastName" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic"
                                    ControlToValidate="txtLastName" Text="<%$ Resources:Resource, reqLastName %>" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEmpLastName%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="form-footer">
                            <asp:Button ID="btnSearchByName" class="btn" runat="server" Text="<%$ Resources:Resource, btnNext %>" ValidationGroup="grpRole" OnClientClick="return BtnClick();" OnClick="btnSearchByName_OnClick" />
                        </footer>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Show Custom Error Message for each contorl on Page
        function BtnClick() {
            var val = Page_ClientValidate();
            if (!val) {
                var i = 0;
                for (; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $("#" + Page_Validators[i].controltovalidate).addClass("has-error");
                        break;
                    }
                    else {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            else {
                var i = 0;
                for (; i < Page_Validators.length; i++) {
                    {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            return val;
        }

        $(document).ready(function () {
            $("#<%=txtFirstName.ClientID%>").focus();
                });
    </script>
</asp:Content>
