﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class ReportSpecificDate : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ReportOption != "")
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
        }

        if (!IsPostBack)
        {
            if (this.DateType == ReportDateFilter.IncludeSpecificDate)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblUpToAndIncludingASpecificdate, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                ltrTitle.Text = Resources.Resource.lblUpToAndIncludingASpecificdate;
                rfToDate.Enabled = false;
                dvToDate.Visible = false;
            }
            else if (this.DateType == ReportDateFilter.BetweenTwoDates)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblBetweenTwoDates, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                txtDate.Attributes.Add("placeholder", Resources.Resource.lblBeginingDate);
                ltrTitle.Text = Resources.Resource.lblBetweenTwoDates;
                rfToDate.Enabled = true;
                rfDate.Text = Resources.Resource.reqMsgBeginningOn;
                dvToDate.Visible = true;
            }
        }
    }

    /// <summary>
    /// Redirect to specify Date 
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnBack_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("ReportSpecficDate.aspx?roption=" + this.ReportOption + "&searchby="+ this.SearchBy +"");
    }

    /// <summary>
    /// Redirect to Report Filter Page
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnCountinue_OnClick(object sender, EventArgs e)
    {
        if (this.DateType == ReportDateFilter.IncludeSpecificDate)
        {
            Response.Redirect("ReportFilterSummary.aspx?roption=" + this.ReportOption + "&searchby=" + this.SearchBy + "&dateType=" + ReportDateFilter.IncludeSpecificDate + "&fdate=" + txtDate.Text + "");   
        }
        else if (this.DateType == ReportDateFilter.BetweenTwoDates)
        {
            Response.Redirect("ReportFilterSummary.aspx?roption=" + this.ReportOption + "&searchby=" + this.SearchBy + "&dateType=" + ReportDateFilter.BetweenTwoDates + "&fdate=" + txtDate.Text + "&tdate=" + txtToDate.Text + "");               
        }
    }

    /// <summary>
    /// To Get Report Option
    /// </summary>
    public string ReportOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }

    /// <summary>
    /// To Get Report Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Report Date Type
    /// </summary>
    public string DateType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["dateType"]);
        }
    }
}