﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;

public partial class EmpOtherSearchDashBoard : BasePage
{
    /// <summary>
    /// To Save HTML Content
    /// </summary>
    protected string htmlText = "";

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ReportOption != "")
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
        }

        //if (!IsPostBack)
        {
            if (this.SearchBy == EmpSearchBy.Division)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchLocation, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            }
            if (this.SearchBy == EmpSearchBy.Type)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblEmpType, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            }

            if (this.SearchBy == EmpSearchBy.DivisionTBS)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchDivision, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            }

            if (this.SearchBy == EmpSearchBy.RegionTBS)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSearchRegion, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            }

            htmlText = loadEmpSearchValue();
        }

        Role objRole = new Role();

        string roleName = "";
        string functionalityName = "";
        if (this.RoleID > 0)
        {
            objRole = new Role();
            roleName = objRole.GetRoleName(this.RoleID);
        }


        if (this.SearchBy == EmpRefCode.Store)
        {
            if (Utils.TrainingInst == (int)Institute.bdl)
            {
                functionalityName = Resources.Resource.lblDeptID;
            }
            else
            {
                functionalityName = Resources.Resource.lblSearchSite;
            }
        }
        else if (this.SearchBy == EmpRefCode.Region)
        {
            functionalityName = Resources.Resource.lblSearchRegion;
        }
        else if (this.SearchBy == EmpRefCode.JobCode)
        {
            functionalityName = Resources.Resource.lblSearchJobCode;
        }
        else if (this.SearchBy == EmpRefCode.Division)
        {
            functionalityName = Resources.Resource.lblSearchLocation;
        }

        else if (this.SearchBy == EmpRefCode.DivisionTBS)
        {
            functionalityName = Resources.Resource.lblSearchDivision;
        }
        else if (this.SearchBy == EmpRefCode.RegionTBS)
        {
            functionalityName = Resources.Resource.lblSearchRegion;
        }
        else if (this.SearchBy == EmpRefCode.District)
        {
            functionalityName = Resources.Resource.lblSearchDistrict;
        }
        else if (this.SearchBy == EmpRefCode.Type)
        {
            functionalityName = Resources.Resource.lblEmpType;
        }

        if (this.Flag == "add")
        {
            HdnMessage.Value = Resources.Resource.lblRoleSysRefAddOther.Replace("#SYSREFNAME#", functionalityName).Replace("#ROLENAME#", roleName);
        }
        else if (this.Flag == "remove")
        {
            HdnMessage.Value = Resources.Resource.lblRoleSysRefRemoveOther.Replace("#SYSREFNAME#", functionalityName).Replace("#ROLENAME#", roleName);
        }

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addActionRoleSysRefAssociation")
        {
            try
            {
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                string sysRoleOperator = BusinessUtility.GetString(Request.Form["SysRoleOperator"]);
                if (searchBy != "" && roleID > 0 && searchValue != "")
                {
                    objRole = new Role();
                    if (objRole.RoleAddEmpRef(roleID, searchBy, searchValue, sysRoleOperator) == true)
                    {
                        ExecutingThread.ThreadEmployeeAssignedCoursesByRole(roleID, 0, (int)AssignedCourseInActiveReason.BySysAdminRole, (int)AssignedCourseUpdatedSource.BySysAdminRoleUpdate);
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
        else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeActionRoleSysRefAssociation")
        {
            try
            {
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                if (searchBy != "" && roleID > 0 && searchValue != "")
                {
                    objRole = new Role();
                    if (objRole.RoleRemoveEmpRef(roleID, searchBy, searchValue) == true)
                    {
                        //ExecutingThread.ThreadRemoveEmployeeCoursesByRole(roleID, 0);
                        ExecutingThread.ThreadEmployeeAssignedCoursesByRole(roleID, 0, (int)AssignedCourseInActiveReason.BySysAdminRole, (int)AssignedCourseUpdatedSource.BySysAdminRoleUpdate);
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
    }

    /// <summary>
    /// To Get HTML Content To Create SysRefrence Tiles
    /// </summary>
    /// <returns></returns>
    private string loadEmpSearchValue()
    {
        string sbHtml = "";
        Role objRole = new Role();
        DataTable dt = new DataTable();

        if (this.ReportOption != "")
        {
            dt = objRole.GetSysRef(this.SearchBy, "", Globals.CurrentAppLanguageCode);
        }
        else
        {
            if (this.Flag == "add")
            {
                dt = objRole.GetSysRefNotInRole(this.RoleID, this.SearchBy, "");
            }
            else if (this.Flag == "remove")
            {
                dt = objRole.GetSysRefInRole(this.RoleID, this.SearchBy, "");
            }
        }

        if (this.ReportOption != "" && this.SearchBy.ToLower() == EmpSearchBy.Division.ToLower())
        {
            DataColumn dcSeq = new DataColumn();
            dcSeq.DataType = typeof(int);
            dcSeq.DefaultValue = 0;

            dt.Columns.Add(dcSeq);
            dt.AcceptChanges();

            DataRow dNew = dt.NewRow();
            dNew[0] = this.SearchBy;
            dNew[1] = Resources.Resource.lblSubTileAll;
            dNew[2] = Resources.Resource.lblSubTileAll;
            dNew[3] = Resources.Resource.lblSubTileAll;
            dNew[4] = Resources.Resource.lblSubTileAll;
            dNew[5] = 1;
            dt.Rows.Add(dNew);
        }

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sbHtml += " <div class='boxset' id='dvAdminDashBoard" + BusinessUtility.GetString(i) + "'> ";
            for (int j = 0; j <= 2; j++)
            {
                if (i < dt.Rows.Count)
                {
                    string sText = "";
                    string sValue = "";

                    if (this.ReportOption != "")
                    {
                        sText = BusinessUtility.GetString(dt.Rows[i]["tiles"]);
                        sValue = BusinessUtility.GetString(dt.Rows[i]["sysRefCodeValue"]);

                        if (this.SearchBy.ToLower() == EmpSearchBy.Type.ToLower())
                        {
                            sText = BusinessUtility.GetString(dt.Rows[i]["sysRefCodeText"]);
                            sValue = BusinessUtility.GetString(dt.Rows[i]["sysRefCodeText"]);
                        }
                    }
                    else
                    {
                        sText = BusinessUtility.GetString(dt.Rows[i]["sysRefCodeText"]);
                        sValue = BusinessUtility.GetString(dt.Rows[i]["sysRefCodeValue"]);
                    }

                    if (sText != "")
                    {
                        string sUrl = "";
                        string sFunction = "";
                        if (this.ReportOption != "")
                        {
                            sUrl = string.Format("ReportFilterSummary.aspx?roption={0}&searchby={1}&searchval={2}&actionID={3}&flag={4}", this.ReportOption, this.SearchBy, sValue, this.ActionID, this.Flag);
                            sFunction = "href='" + sUrl + "'  ";
                        }
                        else
                        {
                            if (this.ActionID == 0)
                            {
                                if (this.Flag == "add")
                                {
                                    sFunction = string.Format(" href='#' onclick='RoleSysRefAssociationConfirmation({0},\"" + this.SearchBy + "\",\"" + sValue + "\",\"" + this.Flag + "\",\"" + this.SysRoleOperator + "\",\"" + sText + "\");'", this.RoleID);
                                }
                                else
                                {
                                    sFunction = string.Format(" href='#' onclick='RoleSysRefAssociationConfirmation({0},\"" + this.SearchBy + "\",\"" + sValue + "\",\"" + this.Flag + "\",\"" + this.SysRoleOperator + "\",\"" + sText + "\");'", this.RoleID);
                                }
                            }
                            else if (this.ActionID > 0)
                            {
                                sUrl = string.Format("FunctionalityAddRemoveUser.aspx?roleID={0}&searchby={1}&searchval={2}&actionID={3}&flag={4}", this.RoleID, this.SearchBy, sValue, this.ActionID, this.Flag);
                                sFunction = "href='" + sUrl + "'  ";
                            }
                            if (BusinessUtility.GetString(Request.QueryString["setRole"]) == "1")
                            {
                                sUrl = "#";
                                sFunction = "href='" + sUrl + "'  ";
                            }
                        }
                        sbHtml += " <div class='boxset-box'> ";
                        sbHtml += " <a id='hrfSearchEmpName' runat='server' " + sFunction + "  class='boxset-box-wrapper'> ";
                        sbHtml += " <div class='boxset-box-inner'> ";
                        sbHtml += " <h4 class='boxset-title'> " + sText + " </h4> ";
                        sbHtml += " </div> ";
                        sbHtml += " </a> ";
                        sbHtml += " </div> ";
                        if (j <= 1)
                        {
                            i += 1;
                        }
                    }
                }
            }
            sbHtml += " </div> ";
        }

        if (dt.Rows.Count <= 0)
        {
            sbHtml += "<table style='width: 100%'>";
            sbHtml += "<tr>";
            sbHtml += "<td>" + Resources.Resource.lblNoRecordAvailable + "</td>";
            sbHtml += "</tr>";
            sbHtml += "</table>";
        }
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    ///  To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Report Option
    /// </summary>
    public string ReportOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }

    /// <summary>
    /// To Get Sys Role Operator AND/OR
    /// </summary>
    public string SysRoleOperator
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["operator"]);
        }
    }
}