﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EventAddConfirmation.aspx.cs" Inherits="EventAddConfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <h4>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h4>
            <h5>
                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, lblSummary %>"></asp:Literal>
                : </h5>
            <br />
            <br />
            <br />
            <br />
            <h6>
                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, lblIsItCorrect %>"></asp:Literal></h6>
            <div class="btngrp " style="float: right">
                <a href="#nogo" class="btn" onclick="AddRole();"><%=Resources.Resource.lblYes %></a>
                <a id="hrefNo" runat="server" href="#nogo" class="btn"><%=Resources.Resource.lblNo %></a>
            </div>
        </div>
        <asp:HiddenField ID="hdnEventID" runat="server" />
        <asp:HiddenField ID="hdnRoleID" runat="server" />
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Add Event With Role
        function AddRole() {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "addEvent";
            datatoPost.EventID = $("#<%=hdnEventID.ClientID%>").val();
            datatoPost.RoleID = $("#<%=hdnRoleID.ClientID%>").val();
            $.post("EventAddConfirmation.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    ShowPensivoMessage("<%=Resources.Resource.msgEventAddedToRole%>", "TraningEventRoleDashBoard.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val() + "");
                }
                else {
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotAddEvent%>")
                }
            });
        }
    </script>
</asp:Content>
