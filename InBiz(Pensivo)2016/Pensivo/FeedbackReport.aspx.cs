﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using System.IO;
using System.Text;
using Trirand.Web.UI.WebControls;
using EvoPdf;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections;


public partial class PrintReport : BasePage
{
    /// <summary>
    /// Set Report Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (this.HideTools == false)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showwaitingmessage", "ShowWaiting();", true);
            }
            if (CurrentEmployee.EmpType != EmpType.SuperAdmin)
            {
                if ((AuthorizedToViewPage.GetAuthorizedToView(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting) == true) || (AuthorizedToViewPage.GetAuthorizedToView(CurrentEmployee.EmpID, (int)RoleAction.Reporting) == true))
                {
                }
                else
                {
                    HttpContext.Current.Response.Redirect("public.aspx");
                }
            }

            if (!IsPostBack)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblReport, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                if (Utils.TrainingInst == (int)Institute.bdl)
                {
                    gvReport.Columns[6].HeaderText = Resources.Resource.lblReportEmployeeCurrentDepartmentNumber;
                    gvReport.Columns[7].HeaderText = Resources.Resource.lblReportEmployeeCurrentSiteNumber;
                    gvReport.Columns[7].Visible = true;
                }
                else if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) || (Utils.TrainingInst == (int)Institute.EDE2))
                {
                    //gvReport.Columns[1].Width = 200;
                    //gvReport.Columns[2].Width = 110;
                    //gvReport.Columns[3].Width = 110;
                    //gvReport.Columns[4].Width = 170;
                    //if (Utils.TrainingInst != (int)Institute.EDE2)
                    //{
                    //    gvReport.Columns[4].HeaderText = Resources.Resource.lblReportEmpEmailID;
                    //}
                    //gvReport.Columns[6].HeaderText = Resources.Resource.lblReportEmployeeCurrentSiteNumber;
                    //gvReport.Columns[5].Visible = false;
                    //gvReport.Columns[6].Visible = false;
                    //gvReport.Columns[7].Visible = false;
                    //gvReport.Columns[8].Visible = false;
                    //gvReport.Columns[9].Visible = false;
                    //gvReport.Columns[10].Visible = false;
                    //gvReport.Columns[11].Visible = false;
                    //gvReport.Columns[12].Visible = false;
                }
                else
                {
                    gvReport.Columns[6].HeaderText = Resources.Resource.lblReportEmployeeCurrentSiteNumber;
                }
            }

            if (this.HideTools)
            {
                hrfDownloadExcel.Visible = false;
                hrfSaveReport.Visible = false;
                hrfNew.Visible = false;
                hrfDownloadPDF.Visible = true;
                hrfNewSiteReport.Visible = true;
            }
            else
            {
                hrfDownloadPDF.Visible = false;
                hrfNewSiteReport.Visible = false;
            }

            if (this.ReportID > 0)
            {
                Report objReport = new Report();
                objReport.GetReportFilter(this.ReportID);
                HttpContext.Current.Session["ReportMultiFilter"] = objReport.GetReportFilter(this.ReportID);
                hrfSaveReport.Visible = false;
            }
        }
    }

    /// <summary>
    /// Bind Report JQ Grid
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void gvReport_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Report objReport = new Report();
        DataTable dtReportData = objReport.GetCompletedFeedbackReport();
        Session["ReportData"] = dtReportData;

        gvReport.DataSource = dtReportData;
        gvReport.DataBind();

        if (e.SortExpression != "")
        {
            DataView dv = dtReportData.DefaultView;
            dv.Sort = "" + e.SortExpression + " " + e.SortDirection + " ";
            DataTable sortedDT = dv.ToTable();
            Session["PDFReport"] = sortedDT;
        }
        else
        {
            Session["PDFReport"] = dtReportData;
        }
    }

    /// <summary>
    /// Run New Report
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnNew_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("ReportDashBoard.aspx");
    }

    /// <summary>
    /// Save Report
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnSaveReport_Click(object sender, EventArgs e)
    {
        Report objRep = new Report();
        ReportFilter objReportFilter = new ReportFilter();

        int empID = BusinessUtility.GetInt(CurrentEmployee.EmpID);
        List<ReportFilter> reportFilterList = objReportFilter.GetReportFilter().OrderBy(o => o.ReportOption).ToList();
        if (objRep.SaveReportFilters(empID, reportFilterList) == true)
        {
            //GlobalMessage.showAlertMessage(Resources.Resource.msgReportSaved, "ReportRecentHistory.aspx");
            Response.Redirect("ReportRecentHistory.aspx");
        }
        else
        {
            GlobalMessage.showAlertMessage(Resources.Resource.msgReportNotSaved);
        }
    }

    /// <summary>
    /// Download Report in PDF Format
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnDonwnloadPDFFile_Click(object sender, EventArgs e)
    {
        DownloadFile(hdnFilePath.Value, hdnFileName.Value);
    }

    /// <summary>
    /// Download Report in Excel Format
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void btnDonwnloadExcelFile_Click(object sender, EventArgs e)
    {
        DownloadExcelFile(hdnFilePath.Value, hdnFileName.Value);
    }

    /// <summary>
    /// Get Report Summary Detail For Report
    /// </summary>
    /// <returns></returns>
    protected string GetReportSummaryDetails()
    {
        //ReportFilter objReportFilter = new ReportFilter();
        //List<ReportFilter> reportFilterList = objReportFilter.GetReportFilter().OrderBy(o => o.ReportOption).ToList();

        //StringBuilder sbHtml = new StringBuilder();
        //Event objEvent = new Event();
        //Employee objEmp = new Employee();
        //Role objRole = new Role();


        //sbHtml = new StringBuilder();
        //sbHtml.Append("<style>.reportTable{width:100%;}  .reportCol1{width:auto;height:auto;word-break:break-word}  .reportCol2{width:100px;height:auto;word-break:break-word;vertical-align:top;padding-left:0px} .reportCol3{width:200px;height:auto;word-break:break-all;max-width: 200px;}  .reportTable tr:nth-child(2n+1) td{background: none repeat scroll 0 0 transparent;} .reportTable tr{ border-bottom: 0px solid rgba(0, 0, 0, 0.1);}    .reportTableDesc tr:nth-child(2n+1) td{background: none repeat scroll 0 0 transparent;padding-left: 0px;} .reportTableDesc tr{ border-bottom: 0px solid rgba(0, 0, 0, 0.1);} .titlelable{font-weight:bold;font-size:18px} .subtitlelable{font-weight:normal;font-size:16px;font-style:italic;padding-left: 15px;}  </style>");
        //sbHtml.Append("<table class='reportTable' style='border:1'>");
        //int rowIndex = 1;



        //sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblReportWho + ":"));
        //var resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.Who.ToLower()).ToList();
        //if (resultItem != null && resultItem.Count <= 0)
        //{
        //    sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTrainingUser));
        //}
        //else
        //{
        //    if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "".ToLower()) && (resultItem.Count == 1))
        //    {
        //        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTrainingUser));
        //    }
        //    else
        //    {

        //        foreach (var filterItem in resultItem)
        //        {
        //            if (filterItem.ReportOption.ToLower() == ReportOption.Who.ToLower())
        //            {
        //                if (filterItem.SearchBy.ToLower() == EmpSearchBy.Name.ToLower())
        //                {
        //                    string[] sName = BusinessUtility.GetString(filterItem.SearchValue).Split(',');
        //                    int i = 0;
        //                    foreach (string sEmName in sName)
        //                    {

        //                        objEmp = new Employee();
        //                        objEmp.GetEmployeeDetail(BusinessUtility.GetInt(sEmName));

        //                        if (i == 0)
        //                        {
        //                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, BusinessUtility.GetString(Resources.Resource.lblSearchName) + ":", BusinessUtility.GetString(objEmp.EmpName)));
        //                        }
        //                        else
        //                        {
        //                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", BusinessUtility.GetString(objEmp.EmpName)));
        //                        }
        //                        i += 1;
        //                    }

        //                }
        //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.EmpCode.ToLower())
        //                {
        //                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchEmployeeCode + ":", filterItem.SearchValue));
        //                }
        //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.District.ToLower())
        //                {
        //                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchDistrict + ":", filterItem.SearchValue));
        //                }
        //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.RegionTBS.ToLower())
        //                {
        //                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchRegion + ":", filterItem.SearchValue));
        //                }
        //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.DivisionTBS.ToLower())
        //                {
        //                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchDivision + ":", filterItem.SearchValue));
        //                }
        //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Type.ToLower())
        //                {
        //                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblEmpType + ":", filterItem.SearchValue));
        //                }
        //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.JobCode.ToLower())
        //                {
        //                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchJobCode + ":", filterItem.SearchValue));
        //                }
        //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Division.ToLower())
        //                {
        //                    if (filterItem.SearchValue.ToLower() == Resources.Resource.lblSubTileAll.ToLower())
        //                    {
        //                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchLocation + ":", Resources.Resource.lblSubTileAll));
        //                    }
        //                    else
        //                    {
        //                        objRole = new Role();
        //                        DataTable dt = new DataTable();

        //                        DataView dv = objRole.GetSysRef(filterItem.SearchBy, "", Globals.CurrentAppLanguageCode).DefaultView;
        //                        dv.RowFilter = "sysRefCodeValue ='" + filterItem.SearchValue + "'";
        //                        string sValue = BusinessUtility.GetString(dv[0]["tiles"]);
        //                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchLocation + ":", sValue));
        //                    }
        //                }
        //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Store.ToLower())
        //                {
        //                    if (Utils.TrainingInst == (int)Institute.bdl)
        //                    {
        //                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblDeptID + ":", filterItem.SearchValue));
        //                    }
        //                    else
        //                    {
        //                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchSite + ":", filterItem.SearchValue));
        //                    }
        //                }
        //                else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Site.ToLower())
        //                {
        //                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resources.Resource.lblSearchSite + ":", filterItem.SearchValue));
        //                }
        //            }
        //        }
        //    }
        //}



        //sbHtml.Append(CreateFilterBlankRow(ref rowIndex));
        //sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblReportWhich + ":"));
        //resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.Which.ToLower()).ToList();
        //if (resultItem != null && resultItem.Count <= 0)
        //{
        //    sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTrainingEvents));
        //}
        //else
        //{
        //    if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "".ToLower()) && (resultItem.Count == 1))
        //    {
        //        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTrainingEvents));
        //    }
        //    else
        //    {
        //        foreach (var filterItem in resultItem)
        //        {
        //            string searchBy = "";
        //            if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByEventName.ToLower()))
        //            {
        //                searchBy = Resources.Resource.lblTrainingByTitle;
        //            }
        //            else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByList.ToLower()))
        //            {
        //                searchBy = Resources.Resource.lblTrainingByList;
        //            }
        //            else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByCategory.ToLower()))
        //            {
        //                searchBy = Resources.Resource.lblTrainingByCategory;
        //                objEvent = new Event();
        //                DataTable dt = objEvent.GetCourseTypeList(Globals.CurrentAppLanguageCode);
        //                DataView dv = dt.DefaultView;
        //                dv.RowFilter = "TypeValue='" + filterItem.SearchValue + "'";
        //                sbHtml.Append(CreateFilterDataRow(ref rowIndex, BusinessUtility.GetString(searchBy) + ":", BusinessUtility.GetString(dv[0][1])));
        //            }


        //            if ((filterItem.SearchBy.ToLower() != ReportFilterOption.ByCategory.ToLower()))
        //            {
        //                string[] sName = BusinessUtility.GetString(filterItem.SearchValue).Replace("<br/>", "").Split(',');
        //                int i = 0;
        //                foreach (string sEmName in sName)
        //                {
        //                    objEvent = new Event();
        //                    objEvent.GetEventDetail(BusinessUtility.GetInt(sEmName), Globals.CurrentAppLanguageCode);

        //                    if (i == 0)
        //                    {
        //                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, BusinessUtility.GetString(searchBy) + ":", BusinessUtility.GetString(objEvent.EventName)));
        //                    }
        //                    else
        //                    {
        //                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", BusinessUtility.GetString(objEvent.EventName)));
        //                    }
        //                    i += 1;
        //                }
        //            }
        //        }
        //    }
        //}
        //sbHtml.Append(CreateFilterBlankRow(ref rowIndex));
        //sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblReportWhatProgress + ":"));
        //resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.What.ToLower()).ToList();
        //if (resultItem != null && resultItem.Count <= 0)
        //{
        //    sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTraininProgress));
        //}
        //else
        //{
        //    if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "all".ToLower()) && (resultItem.Count == 1))
        //    {
        //        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllTraininProgress));
        //    }
        //    else
        //    {

        //        foreach (var filterItem in resultItem)
        //        {
        //            if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Once_Completed.ToLower()))
        //            {
        //                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblTrainingCompleteOnce));
        //            }
        //            else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Completed.ToLower()))
        //            {
        //                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblTrainingNotCompletedOnce));
        //            }
        //            else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Started.ToLower()))
        //            {
        //                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblTrainingNotStartedAtOnce));
        //            }
        //            else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.All_Course_Progress.ToLower()))
        //            {
        //                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblAllCourseProgress));
        //            }
        //            else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Only_Those_Not_Completed_Training.ToLower()))
        //            {
        //                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblTrainingNotCompletedOnce));
        //            }
        //        }
        //    }
        //}

        //sbHtml.Append(CreateFilterBlankRow(ref rowIndex));
        //sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblReportWhen + ":"));
        //resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.When.ToLower()).ToList();
        //if (resultItem != null && resultItem.Count <= 1)
        //{
        //    sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resources.Resource.lblReportTrainingYearToDate));
        //}

        //sbHtml.Append("</table>");
        //return BusinessUtility.GetString(sbHtml);
        return "";
    }

    /// <summary>
    /// Create Report Filter Sub Header Row in Report Filter Detail
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <param name="label">Pass Label</param>
    /// <returns>String</returns>
    private string CreateFilterSubHeader(ref int rowIndex, string label)
    {
        string rValue = "";
        if (rowIndex == 1)
        {
            rValue = (" <tr><td class='reportCol2 titlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + "" + "</td></tr>");
        }
        else
        {
            rValue = (" <tr><td class='reportCol2 titlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + "" + "</td></tr>");
        }
        rowIndex += 1;
        return rValue;
    }

    /// <summary>
    /// Create Report Filter Data Row in Report Filter Detail
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <param name="label">Pass Label</param>
    /// <param name="labelValue">Pass Label Value</param>
    /// <returns></returns>
    private string CreateFilterDataRow(ref int rowIndex, string label, string labelValue)
    {
        rowIndex += 1;
        return (" <tr><td class='reportCol2 subtitlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + labelValue + "</td></tr>");
    }

    /// <summary>
    /// Create Report Filter Blank Row
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <returns></returns>
    private string CreateFilterBlankRow(ref int rowIndex)
    {
        rowIndex += 1;
        return (" <tr><td class='reportCol2 titlelable'> &nbsp; </td><td class='reportCol3' style='padding-top:0px'>&nbsp;</td></tr>");
    }

    /// <summary>
    /// Download File 
    /// </summary>
    /// <param name="filePath">Pass File Path </param>
    /// <param name="fileName">Pass File Name</param>
    /// <returns>True/False</returns>
    private static bool DownloadFile(string filePath, string fileName)
    {
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.ClearContent();
        response.Clear();
        response.ContentType = "application/pdf";
        response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ";");
        response.TransmitFile(filePath);
        response.Flush();
        response.Close();
        return true;
    }

    /// <summary>
    /// To Download Excel File 
    /// </summary>
    /// <param name="filePath">Pass File Path</param>
    /// <param name="fileName">Pass File Name</param>
    /// <returns></returns>
    private static bool DownloadExcelFile(string filePath, string fileName)
    {
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.ClearContent();
        response.Clear();
        response.ContentType = "application/csv";
        response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ";");
        response.TransmitFile(filePath);
        response.Flush();
        response.Close();
        return true;
    }

    /// <summary>
    /// Get HTML Content To Download PDF Report Format 
    /// </summary>
    /// <returns></returns>
    public static string GetPDFReportContent()
    {

        StringBuilder sbPDFHTML = new StringBuilder();
        if (HttpContext.Current.Session["PDFReport"] != null)
        {
            DataTable dt = (DataTable)HttpContext.Current.Session["PDFReport"];

            //if (dt.Rows.Count > 0)
            {
                if (dt.Columns.Contains("courseHdrID"))
                {
                    dt.Columns.Remove("courseHdrID");
                }

                if (dt.Columns.Contains("CourseName"))
                    dt.Columns["CourseName"].ColumnName = Resources.Resource.lblReportCourseName;
                if (dt.Columns.Contains("LastName"))
                    dt.Columns["LastName"].ColumnName = Resources.Resource.lblReportLastName;
                if (dt.Columns.Contains("FirstName"))
                    dt.Columns["FirstName"].ColumnName = Resources.Resource.lblReportFirstName;
                if (dt.Columns.Contains("EmployeeID"))
                    dt.Columns["EmployeeID"].ColumnName = Resources.Resource.lblReportEmployeeCurrentID;


                if (Utils.TrainingInst == (int)Institute.bdl)
                {
                    if (dt.Columns.Contains("SiteNumber"))
                        dt.Columns["SiteNumber"].ColumnName = Resources.Resource.lblReportEmployeeCurrentDepartmentNumber;

                    if (dt.Columns.Contains("empSite"))
                        dt.Columns["empSite"].ColumnName = Resources.Resource.lblReportEmployeeCurrentSiteNumber;
                }

                if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) ||  (Utils.TrainingInst == (int)Institute.EDE2))
                {
                    if (dt.Columns.Contains("EmployeeType"))
                    {
                        dt.Columns.Remove("EmployeeType");
                    }

                    if (dt.Columns.Contains("JobCode"))
                        dt.Columns.Remove("JobCode");

                    if (dt.Columns.Contains("LocationType"))
                        dt.Columns.Remove("LocationType");

                    if (dt.Columns.Contains("Region"))
                        dt.Columns.Remove("Region");

                    if (dt.Columns.Contains("District"))
                        dt.Columns.Remove("District");

                    if (dt.Columns.Contains("Division"))
                        dt.Columns.Remove("Division");
                }
                else
                {
                    if (dt.Columns.Contains("SiteNumber"))
                        dt.Columns["SiteNumber"].ColumnName = Resources.Resource.lblReportEmployeeCurrentSiteNumber;

                    if (dt.Columns.Contains("empSite"))
                    {
                        dt.Columns.Remove("empSite");
                    }
                }


                if (dt.Columns.Contains("EmployeeType"))
                {
                    dt.Columns["EmployeeType"].ColumnName = Resources.Resource.lblReportCurrentEmployeeType;
                }

                if (dt.Columns.Contains("JobCode"))
                    dt.Columns["JobCode"].ColumnName = Resources.Resource.lblReportEmployeeCurrentJobCode;

                if (dt.Columns.Contains("LocationType"))
                    dt.Columns["LocationType"].ColumnName = Resources.Resource.lblReportEmployeeCurrentLocationType;

                if (dt.Columns.Contains("Region"))
                    dt.Columns["Region"].ColumnName = Resources.Resource.lblReportEmployeeCurrentRegion;

                if (dt.Columns.Contains("District"))
                    dt.Columns["District"].ColumnName = Resources.Resource.lblReportEmployeeCurrentDistrict;

                if (dt.Columns.Contains("Division"))
                    dt.Columns["Division"].ColumnName = Resources.Resource.lblReportEmployeeCurrentDivision;


                if (dt.Columns.Contains("DateStarted"))
                    dt.Columns["DateStarted"].ColumnName = Resources.Resource.lblReportDateStarted;

                if (dt.Columns.Contains("DateStartedYearMonth"))
                    dt.Columns["DateStartedYearMonth"].ColumnName = Resources.Resource.lblReportDateStartedYearMonth;

                if (dt.Columns.Contains("CourseContentProgress"))
                    dt.Columns["CourseContentProgress"].ColumnName = Resources.Resource.lblReportCourseContentProgress;

                if (dt.Columns.Contains("SuccessfullyCompleted"))
                    dt.Columns["SuccessfullyCompleted"].ColumnName = Resources.Resource.lblReportSuccessfullyCompleted;

                if (dt.Columns.Contains("DateCompleted"))
                    dt.Columns["DateCompleted"].ColumnName = Resources.Resource.lblReportDateCompleted;

                if (dt.Columns.Contains("DateCompletedYearMonth"))
                    dt.Columns["DateCompletedYearMonth"].ColumnName = Resources.Resource.lblReportCompletedYearMonth;

                dt.AcceptChanges();
            }


            string sLogo = "";
            if (Utils.TrainingInst == (int)Institute.beercollege)
            {
                sLogo = "<img class='tbs-logo' src='" + Utils.GetSiteName() + "/_images/tbs-logo-bnw.png' width='160' height='28' title='The Beer Store' alt='The Beer Store Logo.' />";
            }
            else
            {
                sLogo = "<img class='bdl-logo' src='" + Utils.GetSiteName() + "/_images/bdl-logo-bnw.png' width='51' height='33'  title='Brewers Distributor Ltd.' alt='Brewers Distributor Ltd. Logo.' />";
            }


            StringBuilder sbSummary = new StringBuilder();

            #region ReportSummary

            ReportFilter objReportFilter = new ReportFilter();
            List<ReportFilter> reportFilterList = objReportFilter.GetReportFilter().OrderBy(o => o.ReportOption).ToList();
            StringBuilder sbHtml = new StringBuilder();
            Event objEvent = new Event();
            Employee objEmp = new Employee();
            Role objRole = new Role();

            var resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.Who.ToLower()).ToList();
            if (resultItem != null && resultItem.Count <= 0)
            {
                sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWho + "</th><td>" + Resources.Resource.lblAllTrainingUser + "</td></tr>");
            }
            else
            {
                if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "".ToLower()) && (resultItem.Count == 1))
                {
                    sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWho + "</th><td>" + Resources.Resource.lblAllTrainingUser + "</td></tr>");
                }
                else
                {
                    sbSummary.Append("<tr><th class='no-bg' colspan='2'>" + Resources.Resource.lblReportWho + "</th></tr>");
                    foreach (var filterItem in resultItem)
                    {
                        if (filterItem.ReportOption.ToLower() == ReportOption.Who.ToLower())
                        {
                            if (filterItem.SearchBy.ToLower() == EmpSearchBy.Name.ToLower())
                            {
                                string[] sName = BusinessUtility.GetString(filterItem.SearchValue).Split(',');
                                int i = 0;
                                foreach (string sEmName in sName)
                                {

                                    objEmp = new Employee();
                                    objEmp.GetEmployeeDetail(BusinessUtility.GetInt(sEmName));

                                    if (i == 0)
                                    {
                                        sbSummary.Append("<tr><td rowspan='" + BusinessUtility.GetString(sName.Length) + "'>" + BusinessUtility.GetString(Resources.Resource.lblSearchName) + ":</td></tr>");
                                        sbSummary.Append(" <tr><td>" + BusinessUtility.GetString(objEmp.EmpName) + "</td></tr>");
                                    }
                                    else
                                    {
                                        sbSummary.Append(" <tr><td>" + BusinessUtility.GetString(objEmp.EmpName) + "</td></tr>");
                                    }
                                    i += 1;
                                }

                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.EmpCode.ToLower())
                            {
                                sbSummary.Append("<tr><td>" + Resources.Resource.lblSearchEmployeeCode + ":</td><td>" + BusinessUtility.GetString(filterItem.SearchValue) + "</td></tr>");
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.District.ToLower())
                            {
                                sbSummary.Append("<tr><td>" + Resources.Resource.lblSearchDistrict + ":</td><td>" + BusinessUtility.GetString(filterItem.SearchValue) + "</td></tr>");
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.RegionTBS.ToLower())
                            {
                                sbSummary.Append("<tr><td>" + Resources.Resource.lblSearchRegion + ":</td><td>" + BusinessUtility.GetString(filterItem.SearchValue) + "</td></tr>");
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.DivisionTBS.ToLower())
                            {
                                sbSummary.Append("<tr><td>" + Resources.Resource.lblSearchDivision + ":</td><td>" + BusinessUtility.GetString(filterItem.SearchValue) + "</td></tr>");
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Type.ToLower())
                            {
                                sbSummary.Append("<tr><td>" + Resources.Resource.lblEmpType + ":</td><td>" + BusinessUtility.GetString(filterItem.SearchValue) + "</td></tr>");
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.JobCode.ToLower())
                            {
                                sbSummary.Append("<tr><td>" + Resources.Resource.lblSearchJobCode + ":</td><td>" + BusinessUtility.GetString(filterItem.SearchValue) + "</td></tr>");
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Division.ToLower())
                            {
                                if (filterItem.SearchValue.ToLower() == Resources.Resource.lblSubTileAll.ToLower())
                                {
                                    sbSummary.Append("<tr><td>" + Resources.Resource.lblSearchLocation + ":</td><td>" + BusinessUtility.GetString(Resources.Resource.lblSubTileAll) + "</td></tr>");
                                }
                                else
                                {
                                    objRole = new Role();
                                    DataView dv = objRole.GetSysRef(filterItem.SearchBy, "", Globals.CurrentAppLanguageCode).DefaultView;
                                    dv.RowFilter = "sysRefCodeValue ='" + filterItem.SearchValue + "'";
                                    string sValue = BusinessUtility.GetString(dv[0]["tiles"]);

                                    sbSummary.Append("<tr><td>" + Resources.Resource.lblSearchLocation + ":</td><td>" + BusinessUtility.GetString(sValue) + "</td></tr>");
                                }
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Store.ToLower())
                            {
                                if (Utils.TrainingInst == (int)Institute.bdl)
                                {
                                    sbSummary.Append("<tr><td>" + Resources.Resource.lblDeptID + ":</td><td>" + BusinessUtility.GetString(filterItem.SearchValue) + "</td></tr>");
                                }
                                else
                                {
                                    sbSummary.Append("<tr><td>" + Resources.Resource.lblSearchSite + ":</td><td>" + BusinessUtility.GetString(filterItem.SearchValue) + "</td></tr>");
                                }
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Site.ToLower())
                            {
                                sbSummary.Append("<tr><td>" + Resources.Resource.lblSearchSite + ":</td><td>" + BusinessUtility.GetString(filterItem.SearchValue) + "</td></tr>");
                            }
                        }
                    }
                }
            }


            resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.Which.ToLower()).ToList();
            if (resultItem != null && resultItem.Count <= 0)
            {
                sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWhich + "</th><td>" + Resources.Resource.lblAllTrainingEvents + "</td></tr>");
            }
            else
            {
                if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "".ToLower()) && (resultItem.Count == 1))
                {
                    sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWhich + "</th><td>" + Resources.Resource.lblAllTrainingEvents + "</td></tr>");
                }
                else
                {
                    sbSummary.Append("<tr><th class='no-bg' colspan='2'>" + Resources.Resource.lblReportWhich + "</th></tr>");
                    foreach (var filterItem in resultItem)
                    {
                        string searchBy = "";
                        if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByEventName.ToLower()))
                        {
                            searchBy = Resources.Resource.lblTrainingByTitle;
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByList.ToLower()))
                        {
                            searchBy = Resources.Resource.lblTrainingByList;
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByCategory.ToLower()))
                        {
                            searchBy = Resources.Resource.lblTrainingByCategory;
                            objEvent = new Event();
                            DataTable dtCatg = objEvent.GetCourseTypeList(Globals.CurrentAppLanguageCode);
                            DataView dv = dtCatg.DefaultView;
                            dv.RowFilter = "TypeValue='" + filterItem.SearchValue + "'";
                            sbSummary.Append("<tr><td>" + BusinessUtility.GetString(searchBy) + ":</td><td>" + BusinessUtility.GetString(dv[0][1]) + "</td></tr>");
                        }


                        if ((filterItem.SearchBy.ToLower() != ReportFilterOption.ByCategory.ToLower()))
                        {
                            string[] sName = BusinessUtility.GetString(filterItem.SearchValue).Replace("<br/>", "").Split(',');
                            int i = 0;
                            foreach (string sEmName in sName)
                            {
                                objEvent = new Event();
                                objEvent.GetEventDetail(BusinessUtility.GetInt(sEmName), Globals.CurrentAppLanguageCode);

                                if (i == 0)
                                {
                                    sbSummary.Append("<tr><td rowspan='" + BusinessUtility.GetString(sName.Length + 1) + "'>" + BusinessUtility.GetString(searchBy) + ":</td></tr>");
                                    sbSummary.Append(" <tr><td>" + BusinessUtility.GetString(objEvent.EventName) + "</td></tr>");
                                }
                                else
                                {
                                    sbSummary.Append(" <tr><td>" + BusinessUtility.GetString(objEvent.EventName) + "</td></tr>");
                                }
                                i += 1;
                            }
                        }
                    }
                }
            }

            resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.What.ToLower()).ToList();
            if (resultItem != null && resultItem.Count <= 0)
            {
                sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWhatProgress + "</th><td>" + Resources.Resource.lblAllTraininProgress + "</td></tr>");
            }
            else
            {
                if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "all".ToLower()) && (resultItem.Count == 1))
                {
                    sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWhatProgress + "</th><td>" + Resources.Resource.lblAllTraininProgress + "</td></tr>");
                }
                else
                {
                    foreach (var filterItem in resultItem)
                    {
                        if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Once_Completed.ToLower()))
                        {
                            sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWhatProgress + "</th><td>" + Resources.Resource.lblTrainingCompleteOnce + "</td></tr>");
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Completed.ToLower()))
                        {
                            sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWhatProgress + "</th><td>" + Resources.Resource.lblTrainingNotCompletedOnce + "</td></tr>");
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Started.ToLower()))
                        {
                            sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWhatProgress + "</th><td>" + Resources.Resource.lblTrainingNotStartedAtOnce + "</td></tr>");
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.All_Course_Progress.ToLower()))
                        {
                            sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWhatProgress + "</th><td>" + Resources.Resource.lblAllCourseProgress + "</td></tr>");
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Only_Those_Not_Completed_Training.ToLower()))
                        {
                            sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWhatProgress + "</th><td>" + Resources.Resource.lblTrainingNotCompletedOnce + "</td></tr>");
                        }
                    }
                }
            }

            resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.When.ToLower()).ToList();
            if (resultItem != null && resultItem.Count <= 1)
            {
                sbSummary.Append("<tr><th class='no-bg'>" + Resources.Resource.lblReportWhen + "</th><td>" + Resources.Resource.lblReportTrainingYearToDate + "</td></tr>");
            }
            #endregion

            #region ReportHeader
            StringBuilder sbHeaderHtml = new StringBuilder();
            sbHeaderHtml.Append("<tr>");
            foreach (DataColumn dc in dt.Columns)
            {
                sbHeaderHtml.Append("<th>");
                sbHeaderHtml.Append(dc.ColumnName);
                sbHeaderHtml.Append("</th>");
            }
            sbHeaderHtml.Append("</tr>");
            #endregion

            #region ReportDetail
            StringBuilder sbDetailHtml = new StringBuilder();
            foreach (DataRow dRow in dt.Rows)
            {
                sbDetailHtml.Append("<tr>");
                foreach (DataColumn dc in dt.Columns)
                {
                    sbDetailHtml.Append("<td>");
                    sbDetailHtml.Append(BusinessUtility.GetString(dRow[dc.ColumnName]));
                    sbDetailHtml.Append("</td>");
                }
                sbDetailHtml.Append("</tr>");
            }
            #endregion

            string printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PDFReport.html"));
            printTemplet = printTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                  .Replace("#LOGO#", sLogo)
                  .Replace("#DATETIME#", DateTime.Now.ToString("dd-MM-yyyy"))
                  .Replace("#REPORTTITLE#", Resources.Resource.lblPDFReportTitle)
                  .Replace("#REPORTSUMMARY#", BusinessUtility.GetString(sbSummary))
                  .Replace("#REPORTHEADER#", BusinessUtility.GetString(sbHeaderHtml))
                  .Replace("#REPORTDETAIL#", BusinessUtility.GetString(sbDetailHtml));

            sbPDFHTML.Append(printTemplet);
        }

        return BusinessUtility.GetString(sbPDFHTML);
    }

    /// <summary>
    /// Get EVO License
    /// </summary>
    public static string EVOlicense
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("EVOlicense"));
        }
    }

    /// <summary>
    /// To Get Report ID
    /// </summary>
    public int ReportID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["repID"]);
        }
    }

    /// <summary>
    /// To Get Report Tools To Hide or Not
    /// </summary>
    public Boolean HideTools
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["HideTools"]) == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Get Is Site Reporting or Not
    /// </summary>
    public Boolean IsSiteReporting
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["HideTools"]) == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Create Web Service To Create Report PDF Format
    /// </summary>
    /// <returns>String</returns>
    [System.Web.Services.WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DownloadPDF()
    {
        string sPDFReportPath = Utils.ReportDownloadDiractory;
        string sFName = "Report" + DateTime.Now.ToString("yyyyMMddHHmm") + BusinessUtility.GetString(DateTime.Now.Ticks / 1000000);
        string sFileNamePdf = sFName + ".pdf";

        try
        {
            if (HttpContext.Current.Session["PDFReport"] != null)
            {
                DataTable dt = (DataTable)HttpContext.Current.Session["PDFReport"];
            }

            string sHtmlText = GetPDFReportContent();

            //ErrorLog.createLog(sHtmlText);

            //string printFooterTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/PDFReportFooter.html"));
            //printFooterTemplet = printFooterTemplet.Replace("#DATETIME#", DateTime.Now.ToString("dd-MM-yyyy"));
            //printFooterTemplet = printFooterTemplet.Replace("#REPORTTITLE#", Resources.Resource.lblPDFReportTitle);

            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();
            htmlToPdfConverter.LicenseKey = EVOlicense;
            htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Landscape;
            if (Utils.TrainingInst == (int)Institute.bdl)
            {
                htmlToPdfConverter.HtmlViewerWidth = int.Parse("1500");
            }
            htmlToPdfConverter.PdfDocumentOptions.LeftMargin = float.Parse("5");
            htmlToPdfConverter.PdfDocumentOptions.RightMargin = float.Parse("5");
            htmlToPdfConverter.PdfDocumentOptions.TopMargin = 10;
            htmlToPdfConverter.ConversionDelay = int.Parse("0");
            htmlToPdfConverter.NavigationTimeout = 5000;
            htmlToPdfConverter.PdfDocumentOptions.ShowFooter = true;
            htmlToPdfConverter.PdfFooterOptions.FooterHeight = 30;

            string baseUrl = "";
            if (!(Directory.Exists(sPDFReportPath)))
            {
                Directory.CreateDirectory(sPDFReportPath);
            }
            sPDFReportPath += @"\" + sFileNamePdf;
            byte[] outPdfBuffer = null;
            outPdfBuffer = htmlToPdfConverter.ConvertHtml(sHtmlText, baseUrl);
            using (Stream file = File.OpenWrite(sPDFReportPath))
            {
                file.Write(outPdfBuffer, 0, outPdfBuffer.Length);
                file.Close();
            }
        }
        catch (Exception ex)
        {
            ErrorLog.createLog(ex.ToString());
            sPDFReportPath = "";
        }
        return sPDFReportPath + "|" + sFName + ".pdf";

    }

    /// <summary>
    /// To Create Web Service To Create Excel File
    /// </summary>
    /// <returns>string</returns>
    [System.Web.Services.WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DownloadExcel()
    {
        

        string sExcelDownloadPath = Utils.ReportDownloadDiractory;
        string sFName = "Report" + DateTime.Now.ToString("yyyyMMddHHmm") + BusinessUtility.GetString(DateTime.Now.Ticks / 1000000);
        string sFileNameXlsx = sFName + ".xlsx";
        try
        {
            if (HttpContext.Current.Session["ReportData"] != null)
            {
                DataTable dt = (DataTable)HttpContext.Current.Session["ReportData"];

                if (dt.Rows.Count > 0)
                {
                    if (dt.Columns.Contains("initiatorID"))
                    {
                        dt.Columns.Remove("initiatorID");
                    }
                    if (dt.Columns.Contains("respondantID"))
                    {
                        dt.Columns.Remove("respondantID");
                    }
                    if (dt.Columns.Contains("createdDateTime"))
                    {
                        dt.Columns.Remove("createdDateTime");
                    }
                    if (dt.Columns.Contains("courseHeader_courseHdrID"))
                    {
                        dt.Columns.Remove("courseHeader_courseHdrID");
                    }
                    if (dt.Columns.Contains("idRegistration"))
                    {
                        dt.Columns.Remove("idRegistration");
                    }
                    if (dt.Columns.Contains("roleID"))
                    {
                        dt.Columns.Remove("roleID");
                    }
                    if (dt.Columns.Contains("rolename"))
                    {
                        dt.Columns.Remove("rolename");
                    }
                    //remove all question columns
                    if (dt.Columns.Contains("question1"))
                    {
                        dt.Columns.Remove("question1");
                    }
                    if (dt.Columns.Contains("question2"))
                    {
                        dt.Columns.Remove("question2");
                    }
                    if (dt.Columns.Contains("question3"))
                    {
                        dt.Columns.Remove("question3");
                    }
                    if (dt.Columns.Contains("question4"))
                    {
                        dt.Columns.Remove("question4");
                    }
                    if (dt.Columns.Contains("question5"))
                    {
                        dt.Columns.Remove("question5");
                    }
                    if (dt.Columns.Contains("question6"))
                    {
                        dt.Columns.Remove("question6");
                    }
                    if (dt.Columns.Contains("question7"))
                    {
                        dt.Columns.Remove("question7");
                    }
                    if (dt.Columns.Contains("question8"))
                    {
                        dt.Columns.Remove("question8");
                    }
                    if (dt.Columns.Contains("question9"))
                    {
                        dt.Columns.Remove("question9");
                    }
                    if (dt.Columns.Contains("question10"))
                    {
                        dt.Columns.Remove("question10");
                    }
                    //remove all question columns
                    if (dt.Columns.Contains("completeddatetime"))
                    {
                        dt.Columns["completeddatetime"].ColumnName = "Date Completed";
                    }
                    if (dt.Columns.Contains("totaltime"))
                    {
                        dt.Columns["totaltime"].ColumnName = "Time Completed";
                    }
                    if (dt.Columns.Contains("courseTitleEn"))
                    {
                        dt.Columns["courseTitleEn"].ColumnName = "Core Questionnaire Name";
                    }
                    if (dt.Columns.Contains("courseVerNo"))
                    {
                        dt.Columns["courseVerNo"].ColumnName = "Core Questionnaire Version";
                    }
                    if (dt.Columns.Contains("initiatorFirstName"))
                    {
                        dt.Columns["initiatorFirstName"].ColumnName = "Initiator First Name";
                    }
                    if (dt.Columns.Contains("initiatorLastName"))
                    {
                        dt.Columns["initiatorLastName"].ColumnName = "Initiator Last Name";
                    }
                    if (dt.Columns.Contains("initiatorEmailID"))
                    {
                        dt.Columns["initiatorEmailID"].ColumnName = "Initiator Email ID";
                    }
                    if (dt.Columns.Contains("eventName"))
                    {
                        dt.Columns["eventName"].ColumnName = "Event Name";
                    }
                    if (dt.Columns.Contains("eventDate"))
                    {
                        dt.Columns["eventDate"].ColumnName = "Event Date";
                    }
                    if (dt.Columns.Contains("respondantFirstName"))
                    {
                        dt.Columns["respondantFirstName"].ColumnName = "Respondant First Name";
                    }
                    if (dt.Columns.Contains("respondantLastName"))
                    {
                        dt.Columns["respondantLastName"].ColumnName = "Respondant Last Name";
                    }
                    if (dt.Columns.Contains("respondantEmailID"))
                    {
                        dt.Columns["respondantEmailID"].ColumnName = "Respondant Email ID";
                    }
                    if (dt.Columns.Contains("IsCompleted"))
                    {
                        dt.Columns["IsCompleted"].ColumnName = "Completed";
                    }
                    // Rename all Answer columns 
                    if (dt.Columns.Contains("answer1"))
                    {
                        dt.Columns["answer1"].ColumnName = "Answer One";
                    }
                    if (dt.Columns.Contains("answer2"))
                    {
                        dt.Columns["answer2"].ColumnName = "Answer Two";
                    }
                    if (dt.Columns.Contains("answer3"))
                    {
                        dt.Columns["answer3"].ColumnName = "Answer Three";
                    }
                    if (dt.Columns.Contains("answer4"))
                    {
                        dt.Columns["answer4"].ColumnName = "Answer Four";
                    }
                    if (dt.Columns.Contains("answer5"))
                    {
                        dt.Columns["answer5"].ColumnName = "Answer Five";
                    }
                    if (dt.Columns.Contains("answer6"))
                    {
                        dt.Columns["answer6"].ColumnName = "Answer Six";
                    }
                    if (dt.Columns.Contains("answer7"))
                    {
                        dt.Columns["answer7"].ColumnName = "Answer Seven";
                    }
                    if (dt.Columns.Contains("answer8"))
                    {
                        dt.Columns["answer8"].ColumnName = "Answer Eight";
                    }
                    if (dt.Columns.Contains("answer9"))
                    {
                        dt.Columns["answer9"].ColumnName = "Answer Nine";
                    }
                    if (dt.Columns.Contains("answer10"))
                    {
                        dt.Columns["answer10"].ColumnName = "Answer Ten";
                    }
                    // Rename all Answer columns 

                    //if (dt.Columns.Contains("courseHdrID"))
                    //{
                    //    dt.Columns.Remove("courseHdrID");
                    //}

                    //if (dt.Columns.Contains("CourseName"))
                    //    dt.Columns["CourseName"].ColumnName = Resource.ResourceValue("lblReportCourseName", Globals.CurrentCultureName);
                    //if (dt.Columns.Contains("LastName"))
                    //    dt.Columns["LastName"].ColumnName = Resource.ResourceValue("lblReportLastName", Globals.CurrentCultureName);
                    //if (dt.Columns.Contains("FirstName"))
                    //    dt.Columns["FirstName"].ColumnName = Resource.ResourceValue("lblReportFirstName", Globals.CurrentCultureName);
                    //if (dt.Columns.Contains("EmployeeID"))
                    //{

                    //    if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) || (Utils.TrainingInst == (int)Institute.EDE2))
                    //    {
                    //        dt.Columns["EmployeeID"].ColumnName = Resource.ResourceValue("lblReportEmpEmailID", Globals.CurrentCultureName);


                    //        if (dt.Columns.Contains("JobCode"))
                    //            dt.Columns.Remove("JobCode");

                    //        if (dt.Columns.Contains("LocationType"))
                    //            dt.Columns.Remove("LocationType");

                    //        if (dt.Columns.Contains("Region"))
                    //            dt.Columns.Remove("Region");

                    //        if (dt.Columns.Contains("District"))
                    //            dt.Columns.Remove("District");

                    //        if (dt.Columns.Contains("Division"))
                    //            dt.Columns.Remove("Division");

                    //        if (dt.Columns.Contains("SiteNumber"))
                    //            dt.Columns.Remove("SiteNumber");
                    //    }
                    //    else
                    //    {

                    //        dt.Columns["EmployeeID"].ColumnName = Resource.ResourceValue("lblReportEmployeeCurrentID", Globals.CurrentCultureName);
                    //    }
                    //}


                    //if (Utils.TrainingInst == (int)Institute.bdl)
                    //{
                    //    if (dt.Columns.Contains("SiteNumber"))
                    //        dt.Columns["SiteNumber"].ColumnName = Resource.ResourceValue("lblReportEmployeeCurrentDepartmentNumber", Globals.CurrentCultureName);

                    //    if (dt.Columns.Contains("empSite"))
                    //        dt.Columns["empSite"].ColumnName = Resource.ResourceValue("lblReportEmployeeCurrentSiteNumber", Globals.CurrentCultureName);
                    //}

                    //if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) || (Utils.TrainingInst == (int)Institute.EDE2))
                    //{
                    //    if (dt.Columns.Contains("EmployeeType"))
                    //    {
                    //        dt.Columns.Remove("EmployeeType");
                    //    }

                    //    if (dt.Columns.Contains("empSite"))
                    //    {
                    //        dt.Columns.Remove("empSite");
                    //    }

                    //    if (dt.Columns.Contains("SiteNumber"))
                    //        dt.Columns["SiteNumber"].ColumnName = Resource.ResourceValue("lblReportEmployeeCurrentSiteNumber", Globals.CurrentCultureName);
                    //}
                    //else
                    //{
                    //    if (dt.Columns.Contains("SiteNumber"))
                    //        dt.Columns["SiteNumber"].ColumnName = Resource.ResourceValue("lblReportEmployeeCurrentSiteNumber", Globals.CurrentCultureName);

                    //    if (dt.Columns.Contains("empSite"))
                    //    {
                    //        dt.Columns.Remove("empSite");
                    //    }
                    //}


                    //if (dt.Columns.Contains("EmployeeType"))
                    //{
                    //    dt.Columns["EmployeeType"].ColumnName = Resource.ResourceValue("lblReportCurrentEmployeeType", Globals.CurrentCultureName);
                    //}

                    //if (dt.Columns.Contains("JobCode"))
                    //    dt.Columns["JobCode"].ColumnName = Resource.ResourceValue("lblReportEmployeeCurrentJobCode", Globals.CurrentCultureName);

                    //if (dt.Columns.Contains("LocationType"))
                    //    dt.Columns["LocationType"].ColumnName = Resource.ResourceValue("lblReportEmployeeCurrentLocationType", Globals.CurrentCultureName);

                    //if (dt.Columns.Contains("Region"))
                    //    dt.Columns["Region"].ColumnName = Resource.ResourceValue("lblReportEmployeeCurrentRegion", Globals.CurrentCultureName);

                    //if (dt.Columns.Contains("District"))
                    //    dt.Columns["District"].ColumnName = Resource.ResourceValue("lblReportEmployeeCurrentDistrict", Globals.CurrentCultureName);

                    //if (dt.Columns.Contains("Division"))
                    //    dt.Columns["Division"].ColumnName = Resource.ResourceValue("lblReportEmployeeCurrentDivision", Globals.CurrentCultureName);


                    //if (dt.Columns.Contains("DateStarted"))
                    //    dt.Columns["DateStarted"].ColumnName = Resource.ResourceValue("lblReportDateStarted", Globals.CurrentCultureName);

                    //if (dt.Columns.Contains("DateStartedYearMonth"))
                    //    dt.Columns["DateStartedYearMonth"].ColumnName = Resource.ResourceValue("lblReportDateStartedYearMonth", Globals.CurrentCultureName);

                    //if (dt.Columns.Contains("CourseContentProgress"))
                    //    dt.Columns["CourseContentProgress"].ColumnName = Resource.ResourceValue("lblReportCourseContentProgress", Globals.CurrentCultureName);

                    //if (dt.Columns.Contains("SuccessfullyCompleted"))
                    //    dt.Columns["SuccessfullyCompleted"].ColumnName = Resource.ResourceValue("lblReportSuccessfullyCompleted", Globals.CurrentCultureName);

                    //if (dt.Columns.Contains("DateCompleted"))
                    //    dt.Columns["DateCompleted"].ColumnName = Resource.ResourceValue("lblReportDateCompleted", Globals.CurrentCultureName);

                    //if (dt.Columns.Contains("DateCompletedYearMonth"))
                    //    dt.Columns["DateCompletedYearMonth"].ColumnName = Resource.ResourceValue("lblReportCompletedYearMonth", Globals.CurrentCultureName);

                    //dt.AcceptChanges();


                    if (!(Directory.Exists(sExcelDownloadPath)))
                    {
                        Directory.CreateDirectory(sExcelDownloadPath);
                    }
                    sExcelDownloadPath += @"\" + sFileNameXlsx;

                    ReportFilter objReportFilter = new ReportFilter();
                    ExcelHelper.DataSetsToExl(dt, sExcelDownloadPath, sFName, objReportFilter.GetReportFilter());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.createLog(ex.ToString());
            sExcelDownloadPath = "";
        }
        return sExcelDownloadPath + "|" + sFName + ".xlsx";
    }


}