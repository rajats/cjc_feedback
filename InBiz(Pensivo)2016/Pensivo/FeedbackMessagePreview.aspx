﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="FeedbackMessagePreview.aspx.cs" Inherits="FeedbackMessagePreview" %>

<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.EDE2)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>

    <section id="main-content" class="pg-advanced-list">
        <div class="wrapper">
            <h1><%=Resources.Resource.lblFeedbackMsgPreviewTitle %></h1>
            <aside class="column span-4 fixed-onscroll" style="display: none;">
                <div class="search-box" style="width: 384px; left: 216px;">
                    <header class="search-box-header">
                        <h2 class="h6"><%=Resources.Resource.lbFindUsers %></h2>
                        <p class="last-child"><%=Resources.Resource.lblFindUserMessage %></p>
                    </header>
                    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
                        <div class="search-box-body">
                            <div class="plms-input-group">
                                <div class="plms-fieldset">
                                    <asp:Label ID="lblUserName" AssociatedControlID="txtRoleName" class="filter-key plms-label is-hidden no-height" runat="server"
                                        Text="<%$ Resources:Resource, lblUserName %>" for="name"></asp:Label>
                                    <asp:TextBox ID="txtRoleName" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblUserName %>">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <input id="btnSearch" class="btn fluid clearfix" type="button" style="width: 100%;" value="<%=Resources.Resource.lbSearch%>" />
                        </div>
                    </asp:Panel>
                </div>
            </aside>
            <div class="main-content-body column span-8" onkeypress="return disableEnterKey(event)" style="width: 100%;">
                <div id="grid_wrapper" style="width: 100%;">
                    <%=feedbackMessage %>
                  <%--  <b>Subject</b>: Feedback Please - Less Than One Minute!<br />
                    <br />
                    Hey #TOUSER#,<br />
                    #TOUSER# has requested feedback from you on the following.<br />
                    <b>Event Name</b>: #EVENTNAME#<br />
                    <b>Event Date</b>: #EVENTDATE#<br />
                    <br />
                    Please click the link below and answer the brief Questionnaire<br />
                    #LINK#<br />
                    Thanks
                    #FROMUSERNAME#  --%>
                </div>
                <footer class="form-footer">
                    <div class="btngrp align-r">
                        <a href="#nogo" class="btn  btn-create-user " id="btnCancel" onclick="getCancel();"><%=Resources.Resource.btnCancel %></a>
                        <a href="#nogo" class="btn  btn-create-user " onclick="AddUsersEvent();"><%=Resources.Resource.lblOk %></a>
                    </div>
                </footer>
                <%-- <a href="#nogo" class="btn align-r" onclick="getSelectedRole();"><%=Resources.Resource.lblNext %></a>--%>
            </div>
        </div>

        <asp:HiddenField ID="HdnMessage" runat="server" />
        <asp:HiddenField ID="HdnMessageSent" runat="server" />
        <asp:HiddenField ID="HdnFormattedEventDate" runat="server" />
        <asp:Button ID="btnSendMessage" runat="server" Style="display: none" OnClick="btnSendMessage_Click" />
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">




        function getCancel() {
            window.location.href = "FeedbackUser.aspx";
        }


        // To Get JQ Grid Selected Row Value
        function getSelectedRole() {
            var arrRooms = $grid.getGridParam('selarrrow');
            if (arrRooms.length <= 0) {
                ShowPensivoMessage("<%=Resources.Resource.reqPleaseSelectAvalue%>");
                return false;
            }
            else {
                //window.location.href = "ReportFilterSummary.aspx?roption=who&searchby=NAM&searchval=" + arrRooms.join(",");
            }
        }

        $(document).ready(function () {
            $("#<%=txtRoleName.ClientID%>").focus();
        });


        // To Show Confirmation Dialog to add user
        function AddUsersEvent() {
            var title = "<%=Resources.Resource.lblConfirmation%>";
            var messageText = $("#<%=HdnMessage.ClientID%>").val();
            okButtonText = "OK";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "ConfirmRoleAddUserOk()";
            LaterCnclButtonRedirectLink = "ConfirmAddRoleUserCancel();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Trigger Role Add User Ok Button
        function ConfirmRoleAddUserOk() {
            $("#<%=btnSendMessage.ClientID%>").trigger("click");
            //window.location.href = "AssignTraining.aspx";
        }

        // To Trigger Role Add User Cancel Button
        function ConfirmAddRoleUserCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
            ClosePensivoMessage(); // added by rajat on 20160427
        }


        // To Show Test Attempts Complete Confirmation
        function MessageSendConfirmation() {
            var title = "<%=Resources.Resource.lblFeedbackEmailSent%>";
                    var messageText =  $("#<%=HdnMessageSent.ClientID%>").val();
                    okButtonText = "<%=Resources.Resource.lblOk%>";
                    LaterCnclButtonText = "";
                    okButtonRedirectlink = "CloseTestAttemptRequest();";
                    LaterCnclButtonRedirectLink = "CloseTestAttemptRequest();";
                    PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
                }


                // To Close Test Attempt Request
                function CloseTestAttemptRequest() {
                    HideConfirmationDialog();
                    window.location.href = "FeedbackHome.aspx";
                }

    </script>
</asp:Content>
