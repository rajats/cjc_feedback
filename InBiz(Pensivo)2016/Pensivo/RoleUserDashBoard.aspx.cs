﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Library.Utilities;
using System.IO;

public partial class RoleUserDashBoard : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblModifyUserAssocationWithSystem, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            hrfRemoveRoleUser.HRef = "EmpSearchDashBoard.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&flag=remove";
            hrfAddRoleUser.HRef = "EmpSearchDashBoard.aspx?roleID=" + BusinessUtility.GetString(Request.QueryString["roleID"]) + "&flag=add";
        }
    }
}