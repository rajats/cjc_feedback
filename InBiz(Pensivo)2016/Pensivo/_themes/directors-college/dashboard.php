<?php require("../../_config.php"); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="lt-ie9"><![endif]-->
<!--[if IE 9]><html class="lt-ie10 is-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />

	<title>Director's College: Dashboard Example</title>

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<link rel="stylesheet" href="<?php echo $themes[THEME]["stylesheet_main"]; ?>" />
</head>

<?php // ============================================================ // ?>

<body>

<?php include(ROOT_DIR . "/_modules/modal-logout.php"); ?>
<?php include(ROOT_DIR . "/_modules/modal-progress-details.php"); ?>

<section id="global-wrapper">
	<?php include(ROOT_DIR . "/_includes/header-private.php"); ?>
	
	<?php // ============================================================ // ?>
	
	<section id="main-content">
		<span class="mask"></span>
		<div class="wrapper">
			<header class="main-content-header">
			<h1>My Training</h1>
			<h4>Vestibulum id ligula porta felis euismod semper.</h4>
			<p class="hero">Dapibus Mattis Risus? Donec sed odio dui. 
			Donec id elit non mi porta gravida at eget metus. Aenean lacinia 
			bibendum nulla sed consectetur.</p>
			</header><!--End .main-content-header-->
			
			<?php include(ROOT_DIR . "/_modules/boxset.php"); ?>
		</div><!--End .wrapper-->
	</section><!--End #main-content-->
	
	<?php // ============================================================ // ?>
	
	<div class="layout-push"></div>
</section><!--End #global-wrapper-->

<?php include(ROOT_DIR . "/_includes/footer.php"); ?>

<script src="/_js/respond.min.js"></script>
<script src="/_js/modernizr.custom.159181214.js"></script>
<script src="/_js/jquery-1.5.1.min.js"></script>
<script src="/_js/main.min.js"></script>

</body>
</html>



