<?php require("../../_config.php"); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="lt-ie9"><![endif]-->
<!--[if IE 9]><html class="lt-ie10 is-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />

	<title>New LMS Sample &mdash; Standard template showing recurring base level elements</title>

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<link rel="stylesheet" href="<?php echo $themes[THEME]["stylesheet_main"]; ?>" />
</head>

<?php // ============================================================ // ?>

<body>

<form id="frm" action="" method="post">

<?php include(ROOT_DIR . "/_modules/modal-login.php"); ?>

<section id="global-wrapper">

<?php include(ROOT_DIR . "/_includes/header-public.php"); ?>

<?php // ============================================================ // ?>

<section id="main-content">
<span class="mask"></span>
<div class="wrapper">

<h1 class="h2">Welcome to The Directors College Training Solution</h2>

<div style="width:260px;height:195px;float:left;background:rgba(0,0,0,0.1);margin:10px 30px 0 0;"></div>
<img style="display:none;" class="left" id="img-7580114" src="<?php echo ROOT_URI; ?>/_images/side-brick-building-tbs-branding.jpg" />

<p class="hero">At The Directors College we want to provide the necessary tools for you to conduct your role safely and effectively. E-learning is something relatively new to our business, but it is well established as one of the most powerful means to facilitate training within an organization the size of ours.</p>

<p>Training is a necessary piece in providing the required knowledge for you to ensure a safe and efficient working environment for everyone in your location. The courses offered here have been designed to make this experience informative, interesting and more importantly... enjoyable. You will be exposed to critical information as well as activities and quick knowledge checks that are meant to be fun and educational.</p>

<p>You are the pioneers of this new way of training at The Directors College... congratulations!</p>

<p>If you already have a username and password, enter them in the login form to the left. Otherwise, use your Employee ID as both your username and password and you will be guided through the next steps.</p>

<p>Some courses need special permission to be accessed. If you are interested in the following, please contact the person responsible:</p>

<ul>
<li>For the Worker Health &amp; Safety Rep course, please contact Jeff Wilcox</li>
</ul>

<p>Please be aware that we have revised the Loss Prevention courses. There are now 3, fully updated courses available to participants. If you completed the previous courses, we have kept a backend report with your information. Please contact Suzanne Makins if you require any additional information on these changes.</p>

</div><!--End .wrapper-->
</section><!--End #main-content-->

<div class="layout-push"></div>
</section><!--End #global-wrapper-->

<?php // ============================================================ // ?>

<?php include(ROOT_DIR . "/_includes/footer.php"); ?>

<script src="<?php echo ROOT_URI; ?>/_js/respond.min.js"></script>
<script src="<?php echo ROOT_URI; ?>/_js/modernizr.custom.159181214.js"></script>
<script src="<?php echo ROOT_URI; ?>/_js/jquery-1.5.1.min.js"></script>
<script src="<?php echo ROOT_URI; ?>/_js/main.min.js"></script>

</form>

</body>
</html>
