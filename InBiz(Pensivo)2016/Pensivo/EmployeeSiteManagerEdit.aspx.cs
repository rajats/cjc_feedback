﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Data;

public partial class EmployeeSiteManagerEdit : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// To Create Role Manage List Class Object
    /// </summary>
    RoleManageLists objRoleManageList;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvSiteManager))
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Manage_List);
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSiteReportRemoveUser, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            ltrTitle.Text = "Site " + this.SiteID;


            objRoleManageList = new RoleManageLists();


            HdnListID.Value = BusinessUtility.GetString(this.ListID);

            if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || Utils.TrainingInst == (int)Institute.AlMurrayDentistry  )
            {
                gvSiteManager.Columns[3].HeaderText = Resources.Resource.lblEmpEmailID;
            }
            else
            {
                gvSiteManager.Columns[3].HeaderText = Resources.Resource.lblSiteReportEmpID;
            }


            if (this.ListID == 4)
            {
                gvSiteManager.Columns[4].HeaderText = Resources.Resource.lblSearchSiteDistrict;
                ltrTitle.Text = Resources.Resource.lblSearchSiteDistrict + " " + this.SiteID;
                HdnReturnURL.Value = "EmployeeSiteReportingDistrict.aspx?roleManageListID=" + this.RoleManageListID + "&roleManageListName=" + Server.UrlEncode(objRoleManageList.GetManageListName(this.ListID)) + "&SearchBy=" + this.SearchBy + "&FunctionalityID=" + BusinessUtility.GetString(this.FunctionalityID) + "&ListID=" + BusinessUtility.GetString(this.ListID) + "";
            }
            else
            {
                ltrTitle.Text = "Site " + this.SiteID;
                HdnReturnURL.Value = "EmployeeSiteManager.aspx?roleManageListID=" + this.RoleManageListID + "&roleManageListName=" + Server.UrlEncode(objRoleManageList.GetManageListName(this.ListID)) + "&SearchBy=" + this.SearchBy + "&FunctionalityID=" + BusinessUtility.GetString(this.FunctionalityID) + "&ListID=" + BusinessUtility.GetString(this.ListID) + "";
            }
        }

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeUser")
        {
            try
            {
                int userID = BusinessUtility.GetInt(Request.Form["UserID"]);
                int roleManageListID = BusinessUtility.GetInt(Request.Form["RoleManageListID"]);
                int createdBy = BusinessUtility.GetInt(Request.Form["CreatedBy"]);
                string siteID = BusinessUtility.GetString(Request.Form["SiteID"]);
                string sysRefCode = BusinessUtility.GetString(Request.Form["SysRefCode"]);
                int functionalityID = BusinessUtility.GetInt(Request.Form["FunctionalityID"]);
                int listID = BusinessUtility.GetInt(Request.Form["ListID"]);
                if (userID > 0 && roleManageListID > 0)
                {
                    if (listID == 4)
                    {
                        sysRefCode = EmpRefCode.District;
                    }
                    else
                    {
                        sysRefCode = EmpRefCode.Store;
                    }

                    objRoleManageList = new RoleManageLists();
                    //if (objRoleManageList.RemoveSiteEmployeeList(userID, roleManageListID, BusinessUtility.GetString(siteID), createdBy, EmpRefCode.Store, BusinessUtility.GetString(siteID), functionalityID, listID) == true)
                    if (objRoleManageList.RemoveSiteEmployeeList(userID, roleManageListID, BusinessUtility.GetString(siteID), createdBy, sysRefCode, BusinessUtility.GetString(siteID), functionalityID, listID) == true)
                    {
                        objRoleManageList.UpdateEmployeeHasFunctionalityStatus(userID, BusinessUtility.GetString(siteID));
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }
            Response.End();
            Response.SuppressContent = true;
        }

        HdnSysRefCode.Value = this.SearchBy;
        HdnFunctionalityID.Value = BusinessUtility.GetString(this.FunctionalityID);

    }

    /// <summary>
    /// To Create JQ Grid Cell Binding
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Binding Event</param>
    protected void gvSiteManager_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            string sDesc = string.Empty;
            if (BusinessUtility.GetString(e.RowValues[4]) != "")
            {
                //DataTable dt = PensivoExecutions.PensivoSiteDetails(BusinessUtility.GetString(e.RowValues[4]));
                DataTable dt = new DataTable();
                if (this.ListID == 4)
                {
                    int iEmpID = BusinessUtility.GetInt(BusinessUtility.GetString(e.RowValues[0]));
                    Employee objEmp = new Employee();
                    dt = PensivoExecutions.PensivoSiteDetails(BusinessUtility.GetString(objEmp.GetEmpRefCodeValue(iEmpID, EmpRefCode.Store)));
                }
                else
                {
                    dt = PensivoExecutions.PensivoSiteDetails(BusinessUtility.GetString(this.SiteID));
                }



                if (dt.Rows.Count > 0)
                {
                    string sPlaza = BusinessUtility.GetString(dt.Rows[0]["plaza"]);
                    string sAddress = BusinessUtility.GetString(dt.Rows[0]["address"]);
                    string sCity = BusinessUtility.GetString(dt.Rows[0]["city"]);
                    string sSite = BusinessUtility.GetString(dt.Rows[0]["dept_id"]);

                    if (sPlaza != "")
                    {
                        sDesc += "<span class='locationDesc'>" + sPlaza + "</span>";
                    }
                    else
                    {
                        sDesc += sPlaza;
                    }

                    if (sDesc != "")
                    {
                        sDesc += "<br/>" + sAddress;
                    }
                    else
                    {
                        if (sAddress != "")
                            sDesc += "<span class='locationDesc'>" + sAddress + "</span>";
                    }

                    if (sDesc != "")
                    {
                        sDesc += "<br/>" + sCity;
                    }
                    else
                    {
                        if (sCity != "")
                            sDesc += "<span class='locationDesc'>" + sCity + "</span>";
                    }

                    if (sDesc != "")
                    {
                        sDesc += "<br/>" + Resources.Resource.lblSiteNumber + ": " + sSite;
                    }
                    else
                    {
                        if (sSite != "")
                            sDesc += "<span class='locationDesc'>" + Resources.Resource.lblSiteNumber + ": " + sSite + "</span>";
                    }
                }
            }

            objRole = new Role();
            string str = string.Empty;
            str += "<div class='marginleft'>";
            if (this.ListID == 4)
            {
                str += Resources.Resource.lblRemoveUserDistrictConfirmation.Replace("#USERDETAIL#", BusinessUtility.GetString(e.RowValues[1]) + " " + BusinessUtility.GetString(e.RowValues[2]) + " (" + BusinessUtility.GetString(e.RowValues[3]) + ")").Replace("#SITEDETAIL#", this.SiteID);
            }
            else
            {
                str += Resources.Resource.lblRemoveUserSiteConfirmation.Replace("#USERDETAIL#", BusinessUtility.GetString(e.RowValues[1]) + " " + BusinessUtility.GetString(e.RowValues[2]) + " (" + BusinessUtility.GetString(e.RowValues[3]) + ")").Replace("#SITEDETAIL#", sDesc);
            }

            str += "</div>";
            string sMessage = str.Replace("\"", "{-").Replace("'", "{_");
            e.CellHtml = string.Format(@"<a href=""javascript:;""  class='btn' style='float:none;' onclick=""RemoveUser({0},{1},'{2}','{3}','{4}','{5}')"">" + Resources.Resource.lblRemove + "</a>", e.CellHtml, this.RoleManageListID, this.SiteID, sMessage, this.SearchBy, this.FunctionalityID);


            //DataTable dtHasRecord = (DataTable)gvSiteManager.DataSource;
            //if (dtHasRecord.Rows.Count == 1)
            //{
            //    e.CellHtml = string.Format(@"<a href=""javascript:;""  class='btn' style='float:none;' onclick=""ShowNotDeleteMessage('{0}','{1}','{2}','{3}')"">" + Resources.Resource.lblRemove + "</a>", this.SiteID, this.RoleManageListID, this.FunctionalityID, this.ListID); //this.SearchBy,
            //}
        }
    }

    /// <summary>
    /// To Bind JQ Grid with datasource to user can see site report
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event</param>
    protected void gvSiteManager_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        objRoleManageList = new RoleManageLists();

        if (this.ListID == 1)
        {
            gvSiteManager.DataSource = objRoleManageList.GetSiteEmployeeList(EmpRefCode.Store, Utils.ReplaceDBSpecialCharacter(this.SiteID), this.ListID);
        }
        else if (this.ListID == 4)
        {
            gvSiteManager.DataSource = objRoleManageList.GetSiteDistrictEmployeeList(EmpRefCode.District, Utils.ReplaceDBSpecialCharacter(this.SiteID), this.ListID);
        }
        else
        {
            gvSiteManager.DataSource = objRoleManageList.GetSiteEmployeeLogisticsList(EmpRefCode.Store, Utils.ReplaceDBSpecialCharacter(this.SiteID), this.ListID);
        }
        gvSiteManager.DataBind();
    }


    /// <summary>
    /// To Get Role Manage List ID
    /// </summary>
    public int RoleManageListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleManageListID"]);
        }
    }


    /// <summary>
    /// To Get Site ID
    /// </summary>
    public string SiteID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["siteID"]);
        }
    }

    /// <summary>
    /// To Get SearchBy
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["SearchBy"]);
        }
    }

    /// <summary>
    /// To Get Functionality ID
    /// </summary>
    public int FunctionalityID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["FunctionalityID"]);
        }
    }

    /// <summary>
    /// To Get List ID
    /// </summary>
    public int ListID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["ListID"]);
        }
    }
}