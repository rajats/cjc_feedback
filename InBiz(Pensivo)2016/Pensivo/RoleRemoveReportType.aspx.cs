﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;

public partial class RoleRemvoveReportType : BasePage
{
    /// <summary>
    /// To Hold HTML Content
    /// </summary>
    protected string htmlText = "";

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblModifyAccessReportType, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            htmlText = loadEmpSearchValue();
        }

        Role objRole = new Role();
        string sRoleName = objRole.GetRoleName(this.RoleID);
        


        hdnMessage.Value = Resources.Resource.msgRuWantToRemoveReportType.Replace("#ROLENAME#", sRoleName);

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeAction")
        {
            try
            {
                string reportType = BusinessUtility.GetString(Request.Form["ReportType"]);
                string actionType = BusinessUtility.GetString(Request.Form["ActionType"]);
                string actionItemID = BusinessUtility.GetString(Request.Form["ActionItemID"]);

                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                if (roleID > 0)
                {
                    objRole = new Role();
                    if (objRole.RoleRemoveReportType(roleID, reportType) == true)
                    {
                        DataTable dt = objRole.ReportTypeListExistsInRole(roleID);
                        if (dt.Rows.Count <= 0)
                        {
                            objRole.RoleRemoveFunctionality(roleID, BusinessUtility.GetInt(actionItemID), actionType);
                        }

                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
    }

    /// <summary>
    /// To Get Reporting HTML Content
    /// </summary>
    /// <returns>String</returns>
    private string loadEmpSearchValue()
    {
        string sbHtml = "";
        Role objRole = new Role();
        DataTable dt = objRole.ReportTypeListExistsInRole(this.RoleID);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sbHtml += " <div class='boxset' id='dvAdminDashBoard" + BusinessUtility.GetString(i) + "'> ";
            for (int j = 0; j <= 2; j++)
            {
                if (i < dt.Rows.Count)
                {
                    {
                        string sText = "";
                        string sValue = "";
                        string sFunction = "";

                        if (BusinessUtility.GetString(dt.Rows[i]["reportType"]).ToUpper() == EmpSearchBy.Store.ToUpper())
                        {
                            //sText = Resources.Resource.lblSearchSite;
                            sText = Resources.Resource.lblSiteReportingRetailWholesalesLocation;
                            sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.Store + "&setRole=1";
                            sFunction = "href='" + sValue + "'  ";

                            sFunction = string.Format(" href='#' onclick='removeInRoleReport({0},\"" + this.FunctionalityType + "\",\"" + this.FunctionalityID + "\",\"" + EmpSearchBy.Store + "\",\"" + sText + "\");'", this.RoleID);
                        }
                        else if (BusinessUtility.GetString(dt.Rows[i]["reportType"]).ToUpper() == EmpSearchBy.SiteLogisticsReport.ToUpper())
                        {
                            sText = Resources.Resource.lblLogisticsSiteReporting;
                            sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.SiteLogisticsReport + "&setRole=1";
                            sFunction = "href='" + sValue + "'  ";

                            sFunction = string.Format(" href='#' onclick='removeInRoleReport({0},\"" + this.FunctionalityType + "\",\"" + this.FunctionalityID + "\",\"" + EmpSearchBy.SiteLogisticsReport + "\",\"" + sText + "\");'", this.RoleID);
                        }
                        else if (BusinessUtility.GetString(dt.Rows[i]["reportType"]).ToUpper() == EmpSearchBy.District.ToUpper())
                        {
                            sText = Resources.Resource.lblSearchDistrict;
                            sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.District + "&setRole=1";
                            sFunction = "href='#' onclick='ShowMessageNotAvailable();' class='boxset-box-wrapper is-disabled' ";
                        }

                        else if (BusinessUtility.GetString(dt.Rows[i]["reportType"]).ToUpper() == EmpSearchBy.DivisionTBS.ToUpper())
                        {
                            sText = Resources.Resource.lblSearchDivision;
                            sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.DivisionTBS + "&setRole=1";
                            sFunction = "href='#' onclick='ShowMessageNotAvailable();' class='boxset-box-wrapper is-disabled' ";
                        }

                        else if (BusinessUtility.GetString(dt.Rows[i]["reportType"]).ToUpper() == EmpSearchBy.RegionTBS.ToUpper())
                        {
                            sText = Resources.Resource.lblSearchRegion;
                            sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.RegionTBS + "&setRole=1";
                            sFunction = "href='#' onclick='ShowMessageNotAvailable();'  class='boxset-box-wrapper is-disabled'";
                        }
                        else if (BusinessUtility.GetString(dt.Rows[i]["reportType"]).ToUpper() == EmpSearchBy.Division.ToUpper())
                        {
                            sText = Resources.Resource.lblSearchLocation;
                            sValue = "AccessUserDataDetails.aspx?roleID=" + this.RoleID + "&actionID=" + this.FunctionalityID + "&flag=" + this.Flag + "&functionalityType=" + this.FunctionalityType + "&searchby=" + EmpSearchBy.Division + "&setRole=1";
                            sFunction = "href='#' onclick='ShowMessageNotAvailable();' class='boxset-box-wrapper is-disabled' ";
                        }

                        sbHtml += " <div class='boxset-box'> ";
                        sbHtml += " <a id='hrfSearchEmpName' runat='server' " + sFunction + "  class='boxset-box-wrapper'> "; //" + sFunction + "       //href='#' " + sFunction + " // href='" + sValue + "'
                        sbHtml += " <div class='boxset-box-inner'> ";
                        sbHtml += " <h4 class='boxset-title'> " + sText + " </h4> ";
                        sbHtml += " </div> ";
                        sbHtml += " </a> ";
                        sbHtml += " </div> ";
                        if (j <= 1)
                        {
                            i += 1;
                        }
                    }
                }
            }
            sbHtml += " </div> ";
            i += 1;
        }
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Functionality ID
    /// </summary>
    public int FunctionalityID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Functionality Type
    /// </summary>
    public string FunctionalityType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["functionalityType"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }
}