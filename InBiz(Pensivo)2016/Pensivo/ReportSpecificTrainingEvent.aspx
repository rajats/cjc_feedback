﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ReportSpecificTrainingEvent.aspx.cs" Inherits="ReportSpecificTrainingEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">

                <div class="boxset-box">
                    <a id="hrfTrainingByList" runat="server" href="#" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTrainingByList%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfTrainingByCategory" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTrainingByCategory%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfTrainingEventByTitle" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTrainingByTitle%></h4>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </section>
</asp:Content>
