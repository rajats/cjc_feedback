﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.Web.Configuration;


public partial class MasterPages_Pagewithoutheader : System.Web.UI.MasterPage
{
    protected StringBuilder sMenu = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Page.Title = Utils.TrainingInstTitle;
        imgLogo.Src = Utils.LogoPath;
        imgLogo.Attributes.Add("tile", Utils.TrainingInstTitle);
        imgLogo.Attributes.Add("alt", Utils.TrainingInstLogo);



        if (BusinessUtility.GetString(Request.QueryString["scalability"]) == "1")
        {
            HttpContext.Current.Session["EmpID"] = BusinessUtility.GetInt(Request.QueryString["userid"]);
        }


        btnLogOut.Visible = false;
        hrfHomelink.HRef = "~/Public.aspx";
        hrefHomeNavigation.HRef = "~/Public.aspx";
        hrefHomeNavigationnew.HRef = "~/Public.aspx";

        Role objRole = new Role();
        Employee objEmpNew = new Employee();
        DataTable dt = objRole.GetRoleFunctionality(CurrentEmployee.EmpID, Globals.CurrentAppLanguageCode, CurrentEmployee.EmpType, BusinessUtility.GetString(objEmpNew.GetEmpRefCodeValue(CurrentEmployee.EmpID, EmpSearchBy.Store)));
        foreach (DataRow dRow in dt.Rows)
        {
            sMenu.Append(" <li><a href='" + BusinessUtility.GetString(dRow["ActionUrl"]) + "'>" + BusinessUtility.GetString(dRow["Title"]) + "</a></li> ");
        }
        sMenu.Append(" <li><a href='employeedashboard.aspx'>" + Resources.Resource.lblMyCourse + "</a></li> ");
        sMenu.Append(" <li><a href='userprofile.aspx'>" + Resources.Resource.lblMyProfile + "</a></li> ");
        sMenu.Append(" <li><a href='Faq.aspx'>" + Resources.Resource.lblFaq + "</a></li> ");
    }

    protected void btnLogOut_OnClick(object sender, EventArgs e)
    {
        CurrentEmployee.RemoveEmployeeFromSession();
        Response.Redirect("Public.aspx");
    }

    //private static bool isImport
    //{
    //    get
    //    {
    //        if (BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["ShowImport"]) == "1")
    //        {
    //            return true;
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }
    //}
}
