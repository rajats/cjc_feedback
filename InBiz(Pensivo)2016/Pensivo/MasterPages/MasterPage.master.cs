﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text.RegularExpressions;

public partial class MasterPages_MasterPage : System.Web.UI.MasterPage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Page.Title = Utils.TrainingInstTitle;
        imgLogo.Src = Utils.LogoPath;
        imgLogo.Attributes.Add("tile", Utils.TrainingInstTitle);
        imgLogo.Attributes.Add("alt", Utils.TrainingInstLogo);

        if (this.Source.ToUpper() == UserCreatedSource.SignIn.ToUpper())
        {
            if (this.ExtID != "")
            {
                hdLoginFailed.Value = "2";
                txtUserName.Text = this.ExtID;
                txtPassword.Focus();
            }
        }
        hdnCreateSrc.Value = UserCreatedSource.PensivoNew;
        if (Request.QueryString["askQuestion"] == "")
        {
            if (BusinessUtility.GetInt(CurrentEmployee.EmpID) > 0)
            {
                Role objRole = new Role();
                Employee objEmpNew = new Employee();
                if ((objRole.GetUserHasFunctionality(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)RoleAction.Administration,  BusinessUtility.GetString(objEmpNew.GetEmpRefCodeValue(CurrentEmployee.EmpID, EmpSearchBy.Store))) == true) || (CurrentEmployee.EmpType == EmpType.SuperAdmin))
                {
                    Response.Redirect("AdministrationDashBoard.aspx");
                }
                else
                {
                    Response.Redirect("employeedashboard.aspx");
                }
            }
        }

        if ((Utils.TrainingInst == (int)Institute.tdc) )
        {
            this.Page.Title = Resources.Resource.lblSiteTitleTDC;
            lilanguage.Visible = false;
        }
        else if ((Utils.TrainingInst == (int)Institute.navcanada) )
        {
            this.Page.Title = Resources.Resource.lblSiteTitleNavcanada;
            lilanguage.Visible = true;
        }
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            this.Page.Title = Resources.Resource.lblSiteTitleNavcanada;
            lilanguage.Visible = true;
        }
        else if ((Utils.TrainingInst == (int)Institute.AlMurrayDentistry))
        {
            this.Page.Title = Resources.Resource.lblAlMurrayDentistrySiteTitle;
            lilanguage.Visible = true;
        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            this.Page.Title = Resources.Resource.lblSiteTitleBDL;
            lilanguage.Visible = false;
        }
        else
        {
            this.Page.Title = Resources.Resource.lblSiteTitle;
            lilanguage.Visible = false;
        }

        if (Utils.TrainingInst == (int)Institute.bdl)
        {
            lblUserName.InnerHtml = Resources.Resource.lblBDLUserName;
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblBDLUserName);
            dvLogInErrMsg.InnerHtml = "<p class='last-child'>" + Resources.Resource.lblTBSInvalidEmplyeeID + " </p>";
        }
        else if (Utils.TrainingInst == (int)Institute.beercollege)
        {
            lblUserName.InnerHtml = Resources.Resource.lblTBSUserName;
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblTBSUserName);

            dvLogInErrMsg.InnerHtml = "<p class='last-child'>" + Resources.Resource.lblTBSInvalidEmplyeeID + " </p>";
        }
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            lblUserName.InnerHtml = Resources.Resource.lblEDE2UserName;
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblEDE2UserName);

            dvLogInErrMsg.InnerHtml = "<p class='last-child'>" + Resources.Resource.lblEDE2InvalidEmplyeeID + " </p>";
        }
        else
        {
            lblUserName.InnerHtml = Resources.Resource.lblEmpEmailID;
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblEmpEmailID);

            dvLogInErrMsg.InnerHtml ="<p class='last-child'>" + Resources.Resource.lblTDCInvalidEmplyeeID + " </p>";
        }

        if (!IsPostBack)
        {
            lanDropDown();
        }
    }

    /// <summary>
    /// To Login User 
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnLogIn_OnClick(object sender, EventArgs e)
    {
        Session["Logout"] = null;
        if (txtUserName.Text != "" && txtPassword.Text != "")
        {
            EmployeeValidation objEmpValidate = new EmployeeValidation();
            if (objEmpValidate.ValidateEmployee(txtUserName.Text, txtPassword.Text) == true)
            {
                EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.SignedInToPIB,
                    BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), "", 0, (int)RusticRegStatusCode.None, "");

                Role objRole = new Role();
                Employee objEmpNew = new Employee();
                if ((objRole.GetUserHasFunctionality(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)RoleAction.Administration, BusinessUtility.GetString(objEmpNew.GetEmpRefCodeValue(CurrentEmployee.EmpID, EmpSearchBy.Store))) == true) || (CurrentEmployee.EmpType == EmpType.SuperAdmin))
                {
                    Response.Redirect("AdministrationDashBoard.aspx");
                }
                else
                {
                    //Response.Redirect("employeedashboard.aspx");
                    Response.Redirect("FeedbackHome.aspx");
                }
            }
            else
            {
                hdLoginFailed.Value = "1";
                Employee objEmp = new Employee();

                #region ToCheckUserNameRenamedOrNot
                int iChangedEmpHdrID = objEmp.GetEmpIDChanged(txtUserName.Text);
                if (iChangedEmpHdrID > 0)
                {
                    objEmp.GetEmployeeDetail(iChangedEmpHdrID);
                    string sNewEmpUserName = BusinessUtility.GetString(objEmp.EmpExtID);

                    GlobalMessage.showAlertMessage(Resources.Resource.lblChangeUserNameMessage.Replace("#OLDEMPID#", txtUserName.Text).Replace("#NEWEMPID#", sNewEmpUserName));
                    hdLoginFailed.Value = "2";
                    txtUserName.Text = "";
                    txtPassword.Text = "";
                    return;
                }

                #endregion

                if (objEmp.IsLoginIDExists(txtUserName.Text) == true)
                {
                    // To allow create a new user on login uncomment below line
                    if (Utils.EnableNewUser)
                    {
                        Regex rgx = new Regex(Utils.EmpIdValidatorExpression);
                        if (rgx.IsMatch(txtUserName.Text))
                        {
                            hdnIsCredintial.Value = "1";
                        }
                        else
                        {
                            if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) )
                            {
                                GlobalMessage.showAlertMessage(Resources.Resource.lblTDCInvalidLogInIDFormat);
                            }
                            else if (Utils.TrainingInst == (int)Institute.EDE2)
                            {
                                GlobalMessage.showAlertMessage(Resources.Resource.lblEDE2InvalidLogInIDFormat);
                            }
                            else if (Utils.TrainingInst == (int)Institute.bdl)
                            {
                                GlobalMessage.showAlertMessage(Resources.Resource.lblBdlInvalidLogInIDFormat);
                            }
                            else
                            {
                                GlobalMessage.showAlertMessage(Resources.Resource.lblTBSInvalidLogInIDFormat);
                            }
                            hdLoginFailed.Value = "2";
                        }
                    }
                }
                else
                {
                    hdnIsCredintial.Value = "0";
                }

                hdnUserID.Value = txtUserName.Text;
                hdnPassword.Value = txtPassword.Text;

                txtUserName.Text = "";
                txtPassword.Text = "";
            }
        }
        else
        {
            hdLoginFailed.Value = "1";
            txtUserName.Text = "";
            txtPassword.Text = "";
        }
    }

    /// <summary>
    /// To Auto Login
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnAutoLogIn_OnClick(object sender, EventArgs e)
    {
        if (hdnUserID.Value != "" && hdnPassword.Value != "")
        {
            EmployeeValidation objEmpValidate = new EmployeeValidation();
            if (objEmpValidate.ValidateEmployee(hdnUserID.Value, hdnPassword.Value) == true)
            {
                Response.Redirect("EmployeeShortDetail.aspx?");
            }
        }
    }

    /// <summary>
    /// To Define Drope Down Selected Index
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args </param>
    protected void ddlGlobalLanguage_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Globals.SetCultureInfo(ddlGlobalLanguage.SelectedItem.Value);
        Response.Redirect(Request.RawUrl);
    }

    /// <summary>
    /// To Get Current Language Value
    /// </summary>
    /// <returns></returns>
    protected string GetCurrentValue()
    {
        switch (Globals.CurrentAppLanguageCode)
        {
            case AppLanguageCode.FR:
                return AppCulture.FRENCH_CANADA;
            case AppLanguageCode.EN:
            default:
                return AppCulture.ENGLISH_CANADA;
        }
    }

    /// <summary>
    /// To Get Created Source
    /// </summary>
    private string Source
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["src"]);
        }
    }

    /// <summary>
    /// To Get External Emp ID
    /// </summary>
    private string ExtID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["EmpID"]);
        }
    }

    /// <summary>
    /// To Get Current Selected Language Text
    /// </summary>
    /// <returns>String</returns>
    protected string GetCurrentLanguage()
    {
        switch (Globals.CurrentAppLanguageCode)
        {
            case AppLanguageCode.FR:
                return Resources.Resource.French;
            case AppLanguageCode.EN:
                return Resources.Resource.English;
            default:
                return Resources.Resource.French;
        }
    }

    /// <summary>
    /// To Select Selected Language in Dropdown
    /// </summary>
    private void lanDropDown()
    {
        if (GetCurrentLanguage() == "Fr")
        {
            ddlGlobalLanguage.Items.FindByValue("fr-CA").Selected = true;
        }
        else
        {
            ddlGlobalLanguage.Items.FindByValue("en-CA").Selected = true;
        }
    }

}
