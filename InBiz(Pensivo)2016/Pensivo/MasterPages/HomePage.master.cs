﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.Web.Configuration;


public partial class MasterPages_HomePage : System.Web.UI.MasterPage
{
    /// <summary>
    /// To Hold Menu String
    /// </summary>
    protected StringBuilder sMenu = new StringBuilder();
    protected int topButtonCount = 1;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (BusinessUtility.GetString(Request.QueryString["isFeedback"]) == "1" && BusinessUtility.GetInt(Request.QueryString["roleid"]) > 0)
        {
            //Session.Contents.RemoveAll();
            EmployeeValidation objEmpValidation = new EmployeeValidation();
            objEmpValidation.ValidateEmployeeWhenCameThroughMailLink(BusinessUtility.GetInt(Request.QueryString["empid"]));
        }


        if ((Utils.TrainingInst == (int)Institute.tdc))
        {
            this.Page.Title = Resources.Resource.lblSiteTitleTDC;
            lilanguage.Visible = false;
        }
        else if ((Utils.TrainingInst == (int)Institute.navcanada))
        {
            this.Page.Title = Resources.Resource.lblSiteTitleNavcanada;
            lilanguage.Visible = true;
        }
        //else if ((Utils.TrainingInst == (int)Institute.Feedback))
        //{
        //    this.Page.Title = Resources.Resource.lblSiteTitleNavcanada;
        //    lilanguage.Visible = true;
        //}
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            this.Page.Title = Resources.Resource.lblSiteTitleNavcanada;
            lilanguage.Visible = true;
        }
        else if ((Utils.TrainingInst == (int)Institute.AlMurrayDentistry))
        {
            this.Page.Title = Resources.Resource.lblAlMurrayDentistrySiteTitle;
            lilanguage.Visible = true;
        }

        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            this.Page.Title = Resources.Resource.lblSiteTitleBDL;
            lilanguage.Visible = false;
        }
        else
        {
            this.Page.Title = Resources.Resource.lblSiteTitle;
            lilanguage.Visible = false;
        }

        dvAppEnvironment.InnerHtml = Utils.ApplicationEnvironement;
        imgLogo.Src = Utils.LogoPath;
        imgLogo.Attributes.Add("tile", Utils.TrainingInstTitle);
        imgLogo.Attributes.Add("alt", Utils.TrainingInstLogo);

        if (BusinessUtility.GetString(Request.Url).Contains("EmployeeShortDetail") || BusinessUtility.GetString(Request.Url).Contains("EmployeeQuestion") || BusinessUtility.GetString(Request.Url).Contains("EmployeeOtherQuestion") ||
            BusinessUtility.GetString(Request.Url).Contains("EmployeePass") || BusinessUtility.GetString(Request.Url).Contains("EmployeeSiteDetail") || BusinessUtility.GetString(Request.Url).Contains("EmployeeLocation") || BusinessUtility.GetString(Request.Url).Contains("EmployeeType")
            || BusinessUtility.GetString(Request.Url).Contains("Error.aspx")
            || BusinessUtility.GetString(Request.Url).Contains("ConfirmEmpID.aspx")
            || BusinessUtility.GetString(Request.Url).Contains("ConfirmAnswer.aspx")
            || BusinessUtility.GetString(Request.Url).Contains("ConfirmOtherAnswer.aspx")
            || BusinessUtility.GetString(Request.Url).Contains("ChangePassword.aspx")
            )
        {
            dvBreadCrumbsNavigation.Visible = false;
            dvTopMenu.Visible = false;
            hrfHomelink.HRef = "~/Public.aspx";
            hrefHomeNavigation.HRef = "~/Public.aspx";


            if (CurrentEmployee.EmpID > 0)
            {
                hrefUserInfo.Visible = true;
                hrefUserInfo.InnerHtml = Resources.Resource.lblLoggedinAs + ": <span class='username'>" + CurrentEmployee.EmpName + " (" + CurrentEmployee.EmpExtID + ") " + "</span>";
            }

        }
        else
        {
            hrefUserInfo.Visible = true;
            hrefUserInfo.InnerHtml = Resources.Resource.lblLoggedinAs + ": <span class='username'>" + CurrentEmployee.EmpName + " (" + CurrentEmployee.EmpExtID + ") " + "</span>";

            if (CurrentEmployee.EmpID <= 0)
            {
                Response.Redirect("~/Public.aspx");
            }

            if (Session["LogOut"] != "1")
            {
                if (Utils.EnableSecurityQuesetionOnLogIN == true)
                {
                    Employee objEmp = new Employee();
                    if (objEmp.IsEmployeeAnswerSecurityQuestion(CurrentEmployee.EmpID) == false)
                    {
                        Response.Redirect("EmployeeQuestion.aspx?askQuestion=1");
                        return;
                    }

                    //if (objEmp.IsEmployeeSavedAllTwoQuestion(CurrentEmployee.EmpID) == 1)
                    //{
                    //  DataTable dtEmpQuestion=  objEmp.GetEmployeeSelectedQuestionList(CurrentEmployee.EmpID, Globals.CurrentAppLanguageCode);
                    //  Response.Redirect("EmployeeQuestionOther.aspx?askQuestion=1&QID=" + BusinessUtility.GetInt(dtEmpQuestion.Rows[0][0]) + "");

                    //    return;
                    //}
                }

                if ((Utils.TrainingInst == (int)Institute.beercollege))
                {
                    if (CurrentEmployee.EmpType != EmpType.SuperAdmin)
                    {
                        Employee objEmp = new Employee();
                        if (String.IsNullOrEmpty(objEmp.GetEmpRefCodeValue(CurrentEmployee.EmpID, EmpSearchBy.Store)))
                        {
                            Response.Redirect("EmployeeSiteDetail.aspx?askSite=1");
                        }
                        else if (String.IsNullOrEmpty(objEmp.GetEmpRefCodeValue(CurrentEmployee.EmpID, EmpSearchBy.Division)))
                        {
                            Response.Redirect("EmployeeLocation.aspx?askLocation=1&searchby=DIV");
                        }
                    }
                }
            }

            Role objRole = new Role();
            Employee objEmpNew = new Employee();
            DataTable dt = objRole.GetRoleFunctionality(CurrentEmployee.EmpID, Globals.CurrentAppLanguageCode, CurrentEmployee.EmpType, BusinessUtility.GetString(objEmpNew.GetEmpRefCodeValue(CurrentEmployee.EmpID, EmpSearchBy.Store)));
            foreach (DataRow dRow in dt.Rows)
            {
                //Role objhasRole = new Role();
                //if (objhasRole.GetUserHasFunctionality(CurrentEmployee.EmpID, BusinessUtility.GetInt(dRow["ActionItem"])) == true)
                if (BusinessUtility.GetInt(dRow["ActionItem"]) != (int)RoleAction.Assign_Training)
                {
                    sMenu.Append(" <li><a href='" + Utils.GetSiteName() + "/" + BusinessUtility.GetString(dRow["ActionUrl"]) + "'>" + BusinessUtility.GetString(dRow["Title"]) + "</a></li> ");
                }
            }

            //if (Utils.TrainingInst == (int)Institute.Feedback)
            //{
                sMenu.Append(" <li><a href='FeedbackHome.aspx'>" + Resources.Resource.lblFeedback + "</a></li> ");
                sMenu.Append(" <li><a href='userprofile.aspx'>" + Resources.Resource.lblMyProfile + "</a></li> ");
                //sMenu.Append(" <li><a href='Faq.aspx'>" + Resources.Resource.lblFaq + "</a></li> ");
            //}
            //else
            //{
            //    sMenu.Append(" <li><a href='employeedashboard.aspx'>" + Resources.Resource.lblMyCourse + "</a></li> ");
            //    sMenu.Append(" <li><a href='userprofile.aspx'>" + Resources.Resource.lblMyProfile + "</a></li> ");
            //    sMenu.Append(" <li><a href='Faq.aspx'>" + Resources.Resource.lblFaq + "</a></li> ");
            //}

            if (BusinessUtility.GetInt(CurrentEmployee.EmpID) > 0)
            {
                objRole = new Role();
                if ((objRole.GetUserHasFunctionality(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)RoleAction.Administration, BusinessUtility.GetString(objEmpNew.GetEmpRefCodeValue(CurrentEmployee.EmpID, EmpSearchBy.Store))) == true) || (CurrentEmployee.EmpType == EmpType.SuperAdmin))
                {
                    hrfHomelink.HRef = "~/AdministrationDashBoard.aspx";
                    hrefHomeNavigation.HRef = "~/AdministrationDashBoard.aspx";
                    hypALIAS.Visible = true;
                    topButtonCount = 2;
                }
                else
                {
                    hrfHomelink.HRef = "~/FeedbackHome.aspx";
                    hrefHomeNavigation.HRef = "~/FeedbackHome.aspx";
                    hypALIAS.Visible = false;
                }
            }
        }

        if (CurrentEmployee.IsSuperAdminLoginAsAnotherUser)
        {
            hrfLogOut.Attributes.Add("onclick", "CallLogOut();");
            hrfLogOutSingle.Attributes.Add("onclick", "CallLogOut();");
        }
        else
        {
            hrfLogOut.Attributes.Add("onclick", "LogOut();");
            hrfLogOutSingle.Attributes.Add("onclick", "LogOut();");
        }

        if (!IsPostBack)
        {
            lanDropDown();
        }
    }

    /// <summary>
    /// To Logout 
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnLogOut_OnClick(object sender, EventArgs e)
    {
        Session["Logout"] = "1";
        if (CurrentEmployee.IsSuperAdminLoginAsAnotherUser == false)
        {
            EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.SignedOutofPIB,
                        BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), "", 0, (int)RusticRegStatusCode.None, "");

            CurrentEmployee.RemoveEmployeeFromSession();
            Response.Redirect("~/Public.aspx");
        }
        else
        {
            Response.Redirect("~/AdminLogOut.aspx");
        }
    }

    /// <summary>
    /// To Define Drope Down Selected Index
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args </param>
    protected void ddlGlobalLanguage_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Globals.SetCultureInfo(ddlGlobalLanguage.SelectedItem.Value);
        Response.Redirect(Request.RawUrl);
    }


    /// <summary>
    /// To Get Current Language Value
    /// </summary>
    /// <returns></returns>
    protected string GetCurrentValue()
    {
        switch (Globals.CurrentAppLanguageCode)
        {
            case AppLanguageCode.FR:
                return AppCulture.FRENCH_CANADA;
            case AppLanguageCode.EN:
            default:
                return AppCulture.ENGLISH_CANADA;
        }
    }

    /// <summary>
    /// To Get Current Selected Language Text
    /// </summary>
    /// <returns>String</returns>
    protected string GetCurrentLanguage()
    {
        switch (Globals.CurrentAppLanguageCode)
        {
            case AppLanguageCode.FR:
                return Resources.Resource.French;
            case AppLanguageCode.EN:
                return Resources.Resource.English;
            default:
                return Resources.Resource.French;
        }
    }

    /// <summary>
    /// To Select Selected Language in Dropdown
    /// </summary>
    private void lanDropDown()
    {
        if (GetCurrentLanguage() == "Fr")
        {
            ddlGlobalLanguage.Items.FindByValue("fr-CA").Selected = true;
        }
        else
        {
            ddlGlobalLanguage.Items.FindByValue("en-CA").Selected = true;
        }
    }
}
