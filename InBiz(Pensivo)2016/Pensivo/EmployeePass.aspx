﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeePass.aspx.cs" Inherits="EmployeePass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <asp:Panel ID="cntPannel" runat="server" DefaultButton="btnPass">
            <div class="wrapper width-med">
                <h1><%= Resources.Resource.lblCreateNewAccount%>  </h1>
                <div class="boxed-content">
                    <div class="boxed-content-body">
                        <header class="form-section-header">
                            <h6></h6>
                            <p>
                                <%= Resources.Resource.lblPasswordMessage%>
                            </p>
                        </header>
                        <div class="plms-fieldset">
                            <label class="plms-label is-hidden" for="first-name" id="lblUserName" runat="server"></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtUserName" runat="server" class="plms-input skin2 is-disabled" placeholder="<%$ Resources:Resource, lblUserName %>" disabled="disabled" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p id="pTooltipUserName" runat="server"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label is-hidden" for="last-name"><%= Resources.Resource.lblEnterNewPassword%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtNewPassword" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblEnterNewPassword %>" TextMode="Password" />
                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtNewPassword" ID="RegularExpressionValidator1" class="formels-feedback invalid"
                                    ValidationExpression="^[\s\S]{4,}$" runat="server" ErrorMessage="<%$ Resources:Resource, lblPasswordLength %>"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="rfNewPassword" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic"
                                    ControlToValidate="txtNewPassword" Text="<%$ Resources:Resource, reqMsgNewPassword %>" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEnterNewPassword%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plms-fieldset">
                            <label class="plms-label is-hidden" for="last-name"><%= Resources.Resource.lblEnterConfirmPassword%></label>
                            <div class="plms-tooltip-parent">
                                <asp:TextBox ID="txtConfirmPassword" runat="server" class="plms-input skin2 " placeholder="<%$ Resources:Resource, lblEnterConfirmPassword %>" TextMode="Password" />
                                <asp:RequiredFieldValidator ID="rfConfirmPassword" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic"
                                    ControlToValidate="txtConfirmPassword" Text="<%$ Resources:Resource, reqMsgConfirmPassword %>" />
                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtConfirmPassword" ID="valEmpIDExp" class="formels-feedback invalid"
                                    ValidationExpression="^[\s\S]{4,}$" runat="server" ErrorMessage="<%$ Resources:Resource, lblPasswordLength %>"></asp:RegularExpressionValidator>
                                <asp:CompareValidator ID="cmpValidator" ControlToValidate="txtConfirmPassword" ControlToCompare="txtNewPassword" Type="String"
                                    Operator="Equal" Text="<%$ Resources:Resource, reqMsgPasswordNotMatch %>" runat="Server"
                                    class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic" />
                                <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                    <div class="plms-tooltip-body">
                                        <p><%= Resources.Resource.lblEnterConfirmPassword%></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="form-footer">
                            <asp:Button ID="btnPass" class="btn" runat="server" Text="<%$ Resources:Resource, btnSave %>" ValidationGroup="grpRole" OnClientClick="return BtnClick();" OnClick="btnPass_OnClick" />
                        </footer>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Show Custom Error Message for each Control
        function BtnClick() {
            var val = Page_ClientValidate();
            if (!val) {
                var i = 0;
                for (; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $("#" + Page_Validators[i].controltovalidate).addClass("has-error");
                        break;
                    }
                    else {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            else {
                var i = 0;
                for (; i < Page_Validators.length; i++) {

                    {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            return val;
        }


        $(document).ready(function () {
            $("#<%=txtNewPassword.ClientID%>").focus();
                        });
    </script>
</asp:Content>
