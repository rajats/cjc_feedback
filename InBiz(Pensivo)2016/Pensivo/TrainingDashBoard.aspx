﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="TrainingDashBoard.aspx.cs" Inherits="TrainingDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box">
                    <a href="TrainingEventCreate.aspx" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblAddNewTrainingEvent%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a href="TrainingEventView.aspx" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblModifyExistingTrainingEvent%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

