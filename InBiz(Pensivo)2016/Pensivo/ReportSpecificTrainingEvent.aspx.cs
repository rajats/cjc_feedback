﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class ReportSpecificTrainingEvent : BasePage
{
    /// <summary>
    /// To Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (this.ReportOption != "")
            {
                AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
            }

            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSpecificTrainingEvent, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            hrfTrainingByList.HRef = "ReportTrainingEventSearch.aspx?roption=" + this.ReportOption + "&isAllEvent=" + IsAllEvent + "&searchby=" + ReportFilterOption.ByList + "";
            hrfTrainingByCategory.HRef = "ReportTrainingEventCatg.aspx?roption=" + this.ReportOption + "&isAllEvent=" + IsAllEvent + "&searchby=" + ReportFilterOption.ByCategory + "";
            hrfTrainingEventByTitle.HRef = "ReportTrainingEventSearch.aspx?roption=" + this.ReportOption + "&isAllEvent=" + IsAllEvent + "&searchby=" + ReportFilterOption.ByEventName + "";
        }
    }

    /// <summary>
    /// To Get Report Option
    /// </summary>
    public string ReportOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }

    /// <summary>
    /// To Get Report Is All Event
    /// </summary>
    public int IsAllEvent
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["isAllEvent"]);
        }
    }
}