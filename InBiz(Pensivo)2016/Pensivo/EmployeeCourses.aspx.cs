﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using RusticiSoftware.HostedEngine.Client;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;



public partial class EmployeeViewCourses : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {



            //if (BusinessUtility.GetString(Request.QueryString["TA"]) != "Y")
            //{
            //    DateTime dtCurrentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            //    DateTime dtStartDate = new DateTime(2015, 12, 31);
            //    DateTime dtEndDate = new DateTime(2016, 01, 03);
            //    int i = 0;
            //    int LastBound = (dtEndDate - dtStartDate).Days;
            //    for (i = 0; i <= LastBound; i++)
            //    {
            //        if (dtStartDate == dtCurrentDate)
            //        {
            //            Response.Redirect("TrainingNotAvailable.aspx");
            //        }
            //        dtStartDate.AddDays(1);
            //    }
            //}


            if (!IsPostBack)
        {
            if (this.RegID > 0)
            {
                if (IsReviewCourseContent == true)
                {
                    Response.Redirect(Utils.GetSiteName() + "/EmployeeCourses.aspx?CourseStatus=" + (int)CourseStatus.Traning_Employee_have_taken, false);
                    return;
                }
                else
                {
                    SaveRegistrationXMLResult(this.RegID);
                }
            }

            if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_Taking)
            {
                Breadcrumbs.ResetMyTrainingMenu();
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblTraningIamTaking, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);
            }
            else if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_have_taken)
            {
                if (this.TraningYear > 0)
                {
                }
                else
                {
                    Breadcrumbs.ResetMyTrainingMenu();
                }
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblTrainingIHaveTaken, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);
            }
            else if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_should_take)
            {
                //HttpContext.Current.Session["dtBrdCrumb"] = null;
                Breadcrumbs.ResetMyTrainingMenu();
                
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblGiveFeedback, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);
            }
            BindCourseList();
        }
    }

    /// <summary>
    /// To Bind Course List
    /// </summary>
    private void BindCourseList()
    {
        EmployeeCourses obj = new EmployeeCourses();
        DataTable dt = new DataTable();
        if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_Taking)
        {
            dt = obj.GetEmployeeFeedback(CurrentEmployee.EmpID, 0, Globals.CurrentAppLanguageCode);
            //dt = obj.GetEmployeeTakingCourses(CurrentEmployee.EmpID, Globals.CurrentAppLanguageCode);
        }
        else if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_have_taken)
        {
            int tYear = this.TraningYear;

            if (tYear <= 0)
            {
                tYear = DateTime.Now.Year;
            }

            dt = obj.GetEmployeeHaveTakenCourses(CurrentEmployee.EmpID, Globals.CurrentAppLanguageCode, tYear);
        }
        else if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_should_take)
        {
            //dt = obj.GetEmployeeShouldTakeCourses(CurrentEmployee.EmpID, Globals.CurrentAppLanguageCode);

            if (Request.QueryString["isFeedback"] != null)
            {
                HttpContext.Current.Session["dtBrdCrumb"] = null;
                Breadcrumbs.ResetMyTrainingMenu();

                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblGiveFeedback, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);

                ViewState["roleid"] = Request.QueryString["roleid"];

                bool bChkFedbkExstnce = obj.CheckEmployeeFeedbackExistence(Convert.ToInt32(Request.QueryString["empid"]), Convert.ToInt32(Request.QueryString["roleid"]));
                if (!bChkFedbkExstnce)
                {
                    // To show message if user record does not exists in empassignedcourse table
                    showAlertMessage();
                    //showFeedbackCompletedConfirmationMessage(Resources.Resource.lblSorry, "", Resources.Resource.lblNoFeedbackExists);
                }
                else
                {
                    dt = obj.GetEmployeeFeedback(Convert.ToInt32(Request.QueryString["empid"]), Convert.ToInt32(Request.QueryString["roleid"]), Globals.CurrentAppLanguageCode);
                    if (dt != null && dt.Rows.Count <= 0)
                    {
                        Employee objEmployee = new Employee();
                        objEmployee.GetAssignedFeedbackEmployeeDetailByRole(BusinessUtility.GetInt(ViewState["roleid"]));

                        showFeedbackCompletedConfirmationMessage(Resources.Resource.lblfeedbackCompletedConfirmationTitleWhenCameThroughLink, objEmployee.EmpFirstName + " " + objEmployee.EmpLastName, Resources.Resource.lblFeedbackCompletedMessageWhenCameThroughLink);
                    }
                }
            }
            else
            {
                dt = obj.GetEmployeeFeedback(CurrentEmployee.EmpID, 0, Globals.CurrentAppLanguageCode);
            }
        }

        lstEmployeeCourses.DataSource = dt;
        lstEmployeeCourses.DataBind();
    }

    /// <summary>
    /// To Get Course Selected Status
    /// </summary>
    public int CourseSelectedStatus
    {
        get
        {
            int cStatus = 0;
            int.TryParse(Request.QueryString["CourseStatus"], out cStatus);
            return cStatus;
        }
    }

    /// <summary>
    /// To Define Item Command Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass List View Command Event Args</param>
    protected void lstEmployeeCourses_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (e.CommandName == CourseAction.CourseLaunch)
        {
            if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_have_taken)
            {
                Literal ltrEmpCRegID = (Literal)e.Item.FindControl("ltrEmpCourseRegID");
                CertificateCreate objCertificate = new CertificateCreate();

                Certificate objCert = new Certificate();
                string sSaveFileName = objCert.GetCertificateFilePath(BusinessUtility.GetInt(ltrEmpCRegID.Text));
                if (!string.IsNullOrEmpty(sSaveFileName))
                {
                    string sFolderPath = this.CertificatePath;
                    objCertificate.DownloadCertificate(sFolderPath + sSaveFileName, sSaveFileName.Replace("/", ""));
                }
                else
                {
                    objCertificate.DownloadCertificate(BusinessUtility.GetInt(ltrEmpCRegID.Text), true);
                }
                return;
            }

            Literal ltrEmpHeaderID = (Literal)e.Item.FindControl("ltrEmpHeaderID");
            Literal ltrCourseID = (Literal)e.Item.FindControl("ltrCourseID");
            Literal ltrCourseVersion = (Literal)e.Item.FindControl("ltrCourseVersion");
            Literal ltrCourseExternalID = (Literal)e.Item.FindControl("ltrCourseExternalID");
            Literal ltrEmpCourseRegID = (Literal)e.Item.FindControl("ltrEmpCourseRegID");
            Literal ltrRoleID = (Literal)e.Item.FindControl("ltrRoleID");
            Literal ltrIsRepeated = (Literal)e.Item.FindControl("ltrIsRepeated");
            Literal ltrEmpCourseAssignedID = (Literal)e.Item.FindControl("ltrEmpCourseAssignedID");
            Literal ltrEmpTestCourseRegID = (Literal)e.Item.FindControl("ltrEmpTestCourseRegID");


            // To Launch Course in French Language if System work in French Language
            if (Globals.CurrentAppLanguageCode == AppLanguageCode.FR)
            {
                Event objEventFrench = new Event();
                objEventFrench.GetEventDetail(BusinessUtility.GetInt(ltrCourseID.Text), Globals.CurrentAppLanguageCode);
                ltrCourseExternalID.Text = objEventFrench.CourseExtIDFr;
            }
            // End


            Event objEventVersion = new Event();
            objEventVersion.GetEventDetail(BusinessUtility.GetInt(ltrCourseID.Text), Globals.CurrentAppLanguageCode);

            if (BusinessUtility.GetInt(ltrEmpCourseRegID.Text) <= 0)
            {
                EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();
                //int iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(ltrCourseVersion.Text)));
                int iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEventVersion.CourseVerNo), BusinessUtility.GetInt(ltrIsRepeated.Text)));
                if (iRegID <= 0)
                {

                    //iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.InsertEmployeeRegistraionID( BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(ltrCourseVersion.Text),  BusinessUtility.GetInt(ltrRoleID.Text),  Globals.CurrentAppLanguageCode));
                    iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.InsertEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEventVersion.CourseVerNo), BusinessUtility.GetInt(ltrRoleID.Text), Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(ltrIsRepeated.Text), BusinessUtility.GetInt(ltrEmpCourseAssignedID.Text)));
                    if (iRegID > 0)
                    {
                        Employee objEmployee = new Employee();
                        objEmployee.GetEmployeeDetail(BusinessUtility.GetInt(ltrEmpHeaderID.Text));

                        String postBackUrl = this.GetScormCallbackURL;
                        if (!ScormCloud.RegistrationService.RegistrationExists(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID)))
                        {
                            try
                            {
                                if (BusinessUtility.GetString(objEmployee.EmpFirstName) == "")
                                {
                                    objEmployee.EmpFirstName = "Name";
                                }

                                if (BusinessUtility.GetString(objEmployee.EmpLastName) == "")
                                {
                                    objEmployee.EmpLastName = "Name";
                                }

                                //ScormCloud.RegistrationService.CreateRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID), ltrCourseExternalID.Text, BusinessUtility.GetInt(ltrCourseVersion.Text),
                                //  objEmployee.EmpLogInID, Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-"), Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-"), postBackUrl, RegistrationResultsAuthType.FORM, "", "", RegistrationResultsFormat.FULL);

                                ScormCloud.RegistrationService.CreateRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID), ltrCourseExternalID.Text, BusinessUtility.GetInt("0"),
                                objEmployee.EmpLogInID, Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-"), Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-"), postBackUrl, RegistrationResultsAuthType.FORM, "", "", RegistrationResultsFormat.FULL);


                                if (!ScormCloud.RegistrationService.RegistrationExists(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID)))
                                {
                                    ErrorLog.createLog("Reg ID " + Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID) + " Course Ext ID " + ltrCourseExternalID.Text
                                        + " LogIn ID " + objEmployee.EmpLogInID + " First Name " + Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-")
                                        + " Last Name " + Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-") + " Post Back URL " + postBackUrl);
                                    throw new ArgumentNullException("Course Not Registered on Scorm");
                                }


                                EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();


                                objempCourseReg = new EmployeeCourseRegistration();
                                objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(iRegID));

                                //if (ltrIsRepeated.Text == "1")
                                //{
                                //    objempCourseReg.SetEmpAssignedRepeatFalse(BusinessUtility.GetInt(CurrentEmployee.EmpID), BusinessUtility.GetInt(objempCourseReg.eventID));
                                //}
                                objempCourseReg.SetEmpAssignedRepeatFalse(BusinessUtility.GetInt(ltrEmpCourseAssignedID.Text));

                                Event objEvent = new Event();
                                objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
                                EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.TransferredToRustici,
                                BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), objEvent.EventName, BusinessUtility.GetInt(iRegID), (int)RusticRegStatusCode.Sucess, "1");

                                #region CreateReportSummary
                                Report objReport = new Report();
                                objReport.SaveReportSummary(BusinessUtility.GetInt(objEmployee.EmpID), iRegID, objEvent.CourseTitleEn, BusinessUtility.GetInt(objempCourseReg.eventID), "P", objEmployee.EmpExtID, objEvent.TestType);
                                #endregion
                            }
                            catch (Exception error)
                            {
                                EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();
                                objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(iRegID));

                                Event objEvent = new Event();
                                objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
                                EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.ReturnedToPibFromRustici,
                                BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), objEvent.EventName, BusinessUtility.GetInt(0), (int)RusticRegStatusCode.Failure, "");

                                objEmployeeCourseReg.RemoveEmployeeRegistraionID(iRegID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowPensivoMessage('" + Resources.Resource.lblNotAbleToStartCourse + "');", true);
                                ErrorLog.createLog(error.Message);
                                return;
                            }
                            LaunchUrl(BusinessUtility.GetString(iRegID), BusinessUtility.GetString(ltrEmpCourseRegID.Text));
                        }
                        else
                        {
                            LaunchUrl(BusinessUtility.GetString(iRegID), BusinessUtility.GetString(ltrEmpCourseRegID.Text));
                        }
                    }
                }
                else
                {
                    LaunchUrl(BusinessUtility.GetString(iRegID), BusinessUtility.GetString(ltrEmpCourseRegID.Text));
                }
            }
            else
            {
                ScormCloud.RegistrationService.ResetRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)));
                LaunchUrl(BusinessUtility.GetString(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)), BusinessUtility.GetString(ltrEmpCourseRegID.Text));
            }
        }
        else if (e.CommandName == CourseAction.CourseLaunchTest)
        {
            EmployeeCourses objempCourse = new EmployeeCourses();
            Literal ltrEmpCourseRegID = (Literal)e.Item.FindControl("ltrEmpCourseRegID");
            Literal ltrIsRepeated = (Literal)e.Item.FindControl("ltrIsRepeated");
            Literal ltrEmpCourseAssignedID = (Literal)e.Item.FindControl("ltrEmpCourseAssignedID");

            if (objempCourse.IsEmployeeCompleteEvent(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == false)
            {
                return;
            }

            Literal ltrEmpHeaderID = (Literal)e.Item.FindControl("ltrEmpHeaderID");
            Literal ltrCourseID = (Literal)e.Item.FindControl("ltrCourseTestID");
            Literal ltrEmpTestCourseRegID = (Literal)e.Item.FindControl("ltrEmpTestCourseRegID");

            Event objEvent = new Event();
            objEvent.GetEventDetail(BusinessUtility.GetInt(ltrCourseID.Text), Globals.CurrentAppLanguageCode);
            if (!string.IsNullOrEmpty(objEvent.EventName))
            {
                if (BusinessUtility.GetInt(ltrEmpTestCourseRegID.Text) <= 0)
                {
                    EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();
                    int iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo), BusinessUtility.GetInt(ltrIsRepeated.Text)));
                    if (iRegID <= 0)
                    {
                        iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.InsertEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo), 0, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(ltrIsRepeated.Text), BusinessUtility.GetInt(ltrEmpCourseAssignedID.Text)));
                        if (iRegID > 0)
                        {
                            Employee objEmployee = new Employee();
                            objEmployee.GetEmployeeDetail(BusinessUtility.GetInt(ltrEmpHeaderID.Text));

                            String postBackUrl = this.GetScormCallbackURL;
                            if (!ScormCloud.RegistrationService.RegistrationExists(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID)))
                            {
                                try
                                {
                                    if (BusinessUtility.GetString(objEmployee.EmpFirstName) == "")
                                    {
                                        objEmployee.EmpFirstName = "Name";
                                    }

                                    if (BusinessUtility.GetString(objEmployee.EmpLastName) == "")
                                    {
                                        objEmployee.EmpLastName = "Name";
                                    }


                                    EmployeeCourseRegistration objCourseReg = new EmployeeCourseRegistration();
                                    objCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));

                                    if (objCourseReg.eventLaunchedLanguage == AppLanguageCode.FR)
                                    {
                                        //ScormCloud.RegistrationService.CreateRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID), BusinessUtility.GetString(objEvent.CourseExtIDFr), BusinessUtility.GetInt(objEvent.CourseVerNo),
                                        //  objEmployee.EmpLogInID, Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-"), Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-"), postBackUrl, RegistrationResultsAuthType.FORM, "", "", RegistrationResultsFormat.FULL);

                                        ScormCloud.RegistrationService.CreateRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID), BusinessUtility.GetString(objEvent.CourseExtIDFr), BusinessUtility.GetInt("0"),
      objEmployee.EmpLogInID, Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-"), Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-"), postBackUrl, RegistrationResultsAuthType.FORM, "", "", RegistrationResultsFormat.FULL);

                                        if (!ScormCloud.RegistrationService.RegistrationExists(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID)))
                                        {
                                            ErrorLog.createLog("Reg ID " + Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID) + " Course Ext ID " + BusinessUtility.GetString(objEvent.CourseExtIDFr)
                                                + " LogIn ID " + objEmployee.EmpLogInID + " First Name " + Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-")
                                                + " Last Name " + Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-") + " Post Back URL " + postBackUrl);
                                            throw new ArgumentNullException(" Test Course Not Registered on Scorm");
                                        }

                                    }
                                    else
                                    {
                                        //ScormCloud.RegistrationService.CreateRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID), BusinessUtility.GetString(objEvent.CourseExtID), BusinessUtility.GetInt(objEvent.CourseVerNo),
                                        //  objEmployee.EmpLogInID, Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-"), Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-"), postBackUrl, RegistrationResultsAuthType.FORM, "", "", RegistrationResultsFormat.FULL);

                                        ScormCloud.RegistrationService.CreateRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID), BusinessUtility.GetString(objEvent.CourseExtID), BusinessUtility.GetInt("0"),
                                        objEmployee.EmpLogInID, Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-"), Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-"), postBackUrl, RegistrationResultsAuthType.FORM, "", "", RegistrationResultsFormat.FULL);

                                        if (!ScormCloud.RegistrationService.RegistrationExists(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID)))
                                        {
                                            ErrorLog.createLog("Reg ID " + Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID) + " Course Ext ID " + BusinessUtility.GetString(objEvent.CourseExtID)
                                                + " LogIn ID " + objEmployee.EmpLogInID + " First Name " + Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-")
                                                + " Last Name " + Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-") + " Post Back URL " + postBackUrl);
                                            throw new ArgumentNullException(" Test Course Not Registered on Scorm");
                                        }
                                    }

                                    EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();
                                    objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(iRegID));

                                    objEvent = new Event();
                                    objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
                                    objempCourseReg.SaveCourseTestRegID(BusinessUtility.GetInt(ltrEmpCourseRegID.Text), iRegID);

                                    EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.TransferredToRustici,
                                    BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), objEvent.EventName, BusinessUtility.GetInt(iRegID), (int)RusticRegStatusCode.Sucess, "1");
                                }
                                catch (Exception error)
                                {
                                    EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();
                                    objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(iRegID));

                                    objEvent = new Event();
                                    objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
                                    EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.ReturnedToPibFromRustici,
                                    BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), objEvent.EventName, BusinessUtility.GetInt(0), (int)RusticRegStatusCode.Failure, "");

                                    objEmployeeCourseReg.RemoveEmployeeRegistraionID(iRegID);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowPensivoMessage('" + Resources.Resource.lblNotAbleToStartCourse + "');", true);
                                    ErrorLog.createLog(error.Message);
                                    return;
                                }
                                LaunchUrl(BusinessUtility.GetString(iRegID), BusinessUtility.GetString(ltrEmpCourseRegID.Text));
                            }
                            else
                            {
                                LaunchUrl(BusinessUtility.GetString(iRegID), BusinessUtility.GetString(ltrEmpCourseRegID.Text));
                            }
                        }
                    }
                    else
                    {
                        ScormCloud.RegistrationService.ResetRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID));
                        LaunchUrl(BusinessUtility.GetString(iRegID), BusinessUtility.GetString(ltrEmpCourseRegID.Text));
                    }
                }
                else
                {
                    ScormCloud.RegistrationService.ResetRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(BusinessUtility.GetInt(ltrEmpTestCourseRegID.Text)));
                    LaunchUrl(BusinessUtility.GetString(BusinessUtility.GetInt(ltrEmpTestCourseRegID.Text)), BusinessUtility.GetString(ltrEmpCourseRegID.Text));
                }

            }
        }
        else if (e.CommandName == CourseAction.ProgressStatus)
        {
            string sCourseStatus = "";
            sCourseStatus += "<header class='master-header'>";
            sCourseStatus += "<div class='course-title'>";
            sCourseStatus += "<div class='column span-4'>";
            sCourseStatus += "<h6>" + Resources.Resource.lblCourse + "</h6>";
            sCourseStatus += "</div>";
            sCourseStatus += "<div class='column span-8'>";
            sCourseStatus += "<p>#CourseTitle#</p>";
            sCourseStatus += "</div>";
            sCourseStatus += "</div>";
            sCourseStatus += "<div class='course-status'>";
            sCourseStatus += "<div class='column span-4'>";
            sCourseStatus += "<h6>" + Resources.Resource.lblStatus + "</h6>";
            sCourseStatus += "</div>";
            sCourseStatus += "<div class='column span-8'>";
            sCourseStatus += "<p>#CourseStatus#</p>";
            sCourseStatus += "</div>";
            sCourseStatus += "</div>";
            sCourseStatus += "</header>";

            Literal ltrScormType = (Literal)e.Item.FindControl("ltrScormType");
            Literal ltrCourseTestType = (Literal)e.Item.FindControl("ltrCourseTestType");
            Literal ltrEmpCourseRegID = (Literal)e.Item.FindControl("ltrEmpCourseRegID");
            Literal ltrStartDate = (Literal)e.Item.FindControl("ltrStartDate");
            Literal ltrProgressStatus = (Literal)e.Item.FindControl("ltrProgressStatus");
            Literal ltrEmpHeaderID = (Literal)e.Item.FindControl("ltrEmpHeaderID");
            Literal ltrCourseID = (Literal)e.Item.FindControl("ltrCourseTestID");
            Literal ltrCourseTitle = (Literal)e.Item.FindControl("ltrCourseTitle");
            Literal ltrIsRepeated = (Literal)e.Item.FindControl("ltrIsRepeated");

            Literal ltrEventID = (Literal)e.Item.FindControl("ltrCourseID");
            Literal ltrEventVersion = (Literal)e.Item.FindControl("ltrCourseVersion");


            int iTestCourseRegID = 0;
            int iPassingGrade = 0;
            int iNoAttempts = 0;
            Boolean isCompletedCount = false;
            Event objEvent = new Event();

            // Get Course Registration Passing Grade
            EmployeeCourseRegistration objCourseRegPassingGrade = new EmployeeCourseRegistration();
            objCourseRegPassingGrade.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));

            if (BusinessUtility.GetInt(objCourseRegPassingGrade.passingScore) > 0)
            {
                iPassingGrade = BusinessUtility.GetInt(objCourseRegPassingGrade.passingScore);
            }
            else
            {
                iPassingGrade = objCourseRegPassingGrade.GetCourseVersionPassingGrade(BusinessUtility.GetInt(ltrEventID.Text), BusinessUtility.GetInt(ltrEventVersion.Text));
            }

            StringBuilder sbHtml = new StringBuilder();
            if (ltrScormType.Text == "REG")
            {
                #region Regular
                if (ltrCourseTestType.Text == "CT")
                {
                    EmployeeCourses objempCourse = new EmployeeCourses();
                    if (objempCourse.IsCourseCompleted(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == true)
                    {

                        objEvent.GetEventDetail(BusinessUtility.GetInt(ltrCourseID.Text), Globals.CurrentAppLanguageCode);
                        if (iPassingGrade < 0)
                        {
                            iPassingGrade = BusinessUtility.GetInt(objEvent.CoursePassingGrade);
                        }
                        iNoAttempts = BusinessUtility.GetInt(objEvent.Course_no_of_attempts);
                        if (BusinessUtility.GetInt(objEvent.TestCompletedCount) == 1)
                        {
                            isCompletedCount = true;
                        }
                        else
                        {
                            isCompletedCount = false;
                        }

                        if (!string.IsNullOrEmpty(objEvent.EventName))
                        {
                            EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();
                            objEmployeeCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));
                            iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.courseTestRegID);
                            if (BusinessUtility.GetInt(iTestCourseRegID) <= 0)
                            {
                                iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo)));
                            }
                        }

                        if (objempCourse.IsEmployeeCompleteEvent(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == true)
                        {
                            Literal ltrTestAttempted = (Literal)e.Item.FindControl("ltrTestAttempted");
                            string sCourseCompletedDate = objempCourse.CourseCompletedDate(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));

                            sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblCourseSuccessfullyCompleted).Replace("#CourseTitle#", ltrCourseTitle.Text));
                            string sCourseContent = "";
                            sCourseContent += "<div class='metadata-group'>";
                            sCourseContent += "<div class='column span-4'>";
                            sCourseContent += "<h6 class='metadata-value title'>" + Resources.Resource.lblCourseContent + "</h6>";
                            sCourseContent += "</div>";
                            sCourseContent += "<div class='column span-8'>";
                            sCourseContent += "<p>" + Resources.Resource.lblCourseSuccessfullyCompleted + "</p>";
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + Convert.ToDateTime(ltrStartDate.Text).ToString("MMMM dd, yyyy ") + "</span>";
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseCompletedDate + " — " + Convert.ToDateTime(sCourseCompletedDate).ToString("MMMM dd, yyyy ") + "</span>";
                            sCourseContent += "</div>";
                            sCourseContent += "</div>";
                            sCourseContent += "<div class='metadata-group'>";
                            sCourseContent += "<div class='column span-4'>";
                            sCourseContent += "<h6>" + Resources.Resource.lblTest + "</h6>";
                            sCourseContent += "</div>";
                            sCourseContent += "<div class='column span-8'>";

                            DataTable dtTestAttempts = objempCourse.TestCourseAttempts(iTestCourseRegID, isCompletedCount);
                            if (dtTestAttempts.Rows.Count > 0)
                            {
                                int iAttemptsCount = 1;
                                sCourseContent += "<div class='child-metadata'>";
                                foreach (DataRow dRowAttempt in dtTestAttempts.Rows)
                                {
                                    sCourseContent += "<dl class='child-metadata-group'>";
                                    sCourseContent += "<dt class='metadata-value title'>" + Resources.Resource.lblAttempt + " " + BusinessUtility.GetString(iAttemptsCount) + "</dt>";


                                    ErrorLog.createLog("1 Reg ID " + iTestCourseRegID + "  raw score " + BusinessUtility.GetString(dRowAttempt["score_raw"]) + " Passing Grade " + iPassingGrade);
                                    if (BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])) >= iPassingGrade)
                                    {
                                        sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblPassed + "</dd>";
                                    }
                                    else
                                    {
                                        sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblFailed + "</dd>";
                                    }
                                    sCourseContent += "<dd class='metadata-value'>" + Convert.ToDateTime(BusinessUtility.GetString(dRowAttempt["createdDateTime"])).ToString("MMMM dd, yyyy ") + "</dd>";
                                    if (BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])).ToString("###.##") != "")
                                    {
                                        sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblScore + " &mdash;  " + BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])).ToString("###.##") + "%</dd>";
                                    }
                                    else
                                    {
                                        sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblScore + " &mdash;  0%</dd>";
                                    }
                                    sCourseContent += "</dl>";
                                    iAttemptsCount += 1;
                                }
                                sCourseContent += "</div>";
                            }
                            else
                            {
                                sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblHaveNotMadeAnyAttempts + "</span>";
                            }
                            sCourseContent += "</div>";
                            sCourseContent += "</div>";

                            sbHtml.Append("<div class='metadata'>");
                            sbHtml.Append(BusinessUtility.GetString(sCourseContent));
                            sbHtml.Append("</div>");
                        }
                        System.Web.UI.HtmlControls.HtmlGenericControl divCourseProgressContentStatus1 = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvCourseProgressContentStatus");
                        divCourseProgressContentStatus1.InnerHtml = BusinessUtility.GetString(sbHtml);

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "showProgress", "ShowProgressStatus();", true);
                        return;
                    }

                    objEvent.GetEventDetail(BusinessUtility.GetInt(ltrCourseID.Text), Globals.CurrentAppLanguageCode);
                    if (iPassingGrade < 0)
                    {
                        iPassingGrade = BusinessUtility.GetInt(objEvent.CoursePassingGrade);
                    }
                    iNoAttempts = BusinessUtility.GetInt(objEvent.Course_no_of_attempts);
                    if (BusinessUtility.GetInt(objEvent.TestCompletedCount) == 1)
                    {
                        isCompletedCount = true;
                    }
                    else
                    {
                        isCompletedCount = false;
                    }

                    if (!string.IsNullOrEmpty(objEvent.EventName))
                    {
                        EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();
                        //Literal ltrIsRepeated = (Literal)e.Item.FindControl("ltrIsRepeated");

                        objEmployeeCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));
                        iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.courseTestRegID);
                        if (BusinessUtility.GetInt(iTestCourseRegID) <= 0)
                        {
                            iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo)));
                        }
                    }

                    if (objempCourse.IsEmployeeCompleteEvent(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == true)
                    {
                        Literal ltrTestAttempted = (Literal)e.Item.FindControl("ltrTestAttempted");
                        string sCourseCompletedDate = objempCourse.CourseCompletedDate(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));

                        EmployeeCourseRegistration objempTestAttmpCount = new EmployeeCourseRegistration();
                        int iTestAttemptRequestedCount = 0;
                        iTestAttemptRequestedCount = objempTestAttmpCount.GetRequestedTestAttemptCount(iTestCourseRegID);

                        if (objempTestAttmpCount.GetRequestedTestEligable(iTestCourseRegID) == true)
                        {
                            sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblTestAttempRequestMadeMessage).Replace("#CourseTitle#", ltrCourseTitle.Text));
                        }
                        else
                        {
                            if (BusinessUtility.GetInt(ltrTestAttempted.Text) >= iNoAttempts + iTestAttemptRequestedCount)
                            {
                                sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblNoAttemptsLeftontheTest).Replace("#CourseTitle#", ltrCourseTitle.Text));
                            }
                            else
                            {
                                sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblEligleToTakeTest).Replace("#CourseTitle#", ltrCourseTitle.Text));
                            }
                        }

                        string sCourseContent = "";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6 class='metadata-value title'>" + Resources.Resource.lblCourseContent + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";
                        sCourseContent += "<p>" + Resources.Resource.lblCourseSuccessfullyCompleted + "</p>";
                        sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + Convert.ToDateTime(ltrStartDate.Text).ToString("MMMM dd, yyyy ") + "</span>";
                        sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseCompletedDate + " — " + Convert.ToDateTime(sCourseCompletedDate).ToString("MMMM dd, yyyy ") + "</span>";
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6>" + Resources.Resource.lblTest + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";

                        if (objempTestAttmpCount.GetRequestedTestEligable(iTestCourseRegID) == true)
                        {
                            sCourseContent += "<p>" + Resources.Resource.lblTestAttempRequestMadeMessage + "</p>";
                        }
                        else
                        {
                            if (BusinessUtility.GetInt(ltrTestAttempted.Text) >= iNoAttempts + iTestAttemptRequestedCount)
                            {
                                sCourseContent += "<p>" + Resources.Resource.lblNoAttemptsLeft + "</p>";
                            }
                            else
                            {
                                sCourseContent += "<p>" + Resources.Resource.lblEligleToTakeTest + "</p>";
                            }
                        }



                        DataTable dtTestAttempts = objempCourse.TestCourseAttempts(iTestCourseRegID, isCompletedCount);
                        if (dtTestAttempts.Rows.Count > 0)
                        {
                            int iAttemptsCount = 1;
                            sCourseContent += "<div class='child-metadata'>";
                            foreach (DataRow dRowAttempt in dtTestAttempts.Rows)
                            {
                                sCourseContent += "<dl class='child-metadata-group'>";
                                sCourseContent += "<dt class='metadata-value title'>" + Resources.Resource.lblAttempt + " " + BusinessUtility.GetString(iAttemptsCount) + "</dt>";

                                ErrorLog.createLog("2 Reg ID " + iTestCourseRegID + " raw score " + BusinessUtility.GetString(dRowAttempt["score_raw"]) + " Passing Grade " + iPassingGrade);
                                if (BusinessUtility.GetDecimal(BusinessUtility.GetString(BusinessUtility.GetString(dRowAttempt["score_raw"]))) >= iPassingGrade)
                                {
                                    sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblPassed + "</dd>";
                                }
                                else
                                {
                                    sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblFailed + "</dd>";
                                }
                                sCourseContent += "<dd class='metadata-value'>" + Convert.ToDateTime(BusinessUtility.GetString(dRowAttempt["createdDateTime"])).ToString("MMMM dd, yyyy ") + "</dd>";

                                if (BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])).ToString("###.##") != "")
                                {
                                    sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblScore + " &mdash;  " + BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])).ToString("###.##") + "%</dd>";
                                }
                                else
                                {
                                    sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblScore + " &mdash;  0%</dd>";
                                }
                                sCourseContent += "</dl>";
                                iAttemptsCount += 1;
                            }
                            sCourseContent += "</div>";
                        }
                        else
                        {
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblHaveNotMadeAnyAttempts + "</span>";
                        }
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";

                        sbHtml.Append("<div class='metadata'>");
                        sbHtml.Append(BusinessUtility.GetString(sCourseContent));
                        sbHtml.Append("</div>");
                    }
                    else
                    {
                        //sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", "You have completed " + ltrProgressStatus.Text.Replace(" ", "") + "  of the course content").Replace("#CourseTitle#", ltrCourseTitle.Text));
                        sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblCourseYouHaveCompleted + ltrProgressStatus.Text.Replace(" ", "") + Resources.Resource.lblCourseOfTheCourseContent).Replace("#CourseTitle#", ltrCourseTitle.Text));
                        string sCourseContent = "";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6 class='metadata-value title'>" + Resources.Resource.lblCourseContent + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";
                        sCourseContent += "<span class='metadata-value'>";
                        sCourseContent += "<strong>#Percentage#</strong>".Replace("#Percentage#", ltrProgressStatus.Text.Replace(" ", ""));
                        sCourseContent += " " + Resources.Resource.lblCompleted + "";
                        sCourseContent += "</span>";
                        try
                        {
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + Convert.ToDateTime(ltrStartDate.Text).ToString("MMMM dd, yyyy ") + "</span>";
                        }
                        catch (Exception err)
                        {
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + DateTime.Now.ToString("MMMM dd, yyyy ") + "</span>";
                        }

                        sCourseContent += "</div>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6>" + Resources.Resource.lblTest + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";
                        sCourseContent += "<p>" + Resources.Resource.lblNotEligbleToTakeTest + "</p>";
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";

                        sbHtml.Append("<div class='metadata'>");
                        sbHtml.Append(BusinessUtility.GetString(sCourseContent));
                        sbHtml.Append("</div>");
                    }
                }
                else if (ltrCourseTestType.Text == "CO")
                {

                    EmployeeCourses objempCourse = new EmployeeCourses();
                    if (objempCourse.IsCourseCompleted(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == true)
                    {
                        string sCourseCompletedDate = objempCourse.CourseCompletedDate(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));
                        sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblSuccessfullyCompleted).Replace("#CourseTitle#", ltrCourseTitle.Text));
                        string sCourseContent = "";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6 class='metadata-value title'>" + Resources.Resource.lblCourseContent + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";
                        sCourseContent += "<span class='metadata-value'>";
                        sCourseContent += Resources.Resource.lblSuccessfullyCompleted;
                        sCourseContent += "</span>";
                        sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + Convert.ToDateTime(ltrStartDate.Text).ToString("MMMM dd, yyyy ") + "</span>";
                        sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseCompletedDate + " — " + Convert.ToDateTime(sCourseCompletedDate).ToString("MMMM dd, yyyy ") + "</span>";
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";

                        sbHtml.Append("<div class='metadata'>");
                        sbHtml.Append(BusinessUtility.GetString(sCourseContent));
                        sbHtml.Append("</div>");
                    }
                    else
                    {
                        sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblCourseYouHaveCompleted + ltrProgressStatus.Text.Replace(" ", "") + Resources.Resource.lblCourseOfTheCourseContent).Replace("#CourseTitle#", ltrCourseTitle.Text));
                        string sCourseContent = "";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6 class='metadata-value title'>" + Resources.Resource.lblCourseContent + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";
                        sCourseContent += "<span class='metadata-value'>";
                        sCourseContent += "<strong>#Percentage#</strong>".Replace("#Percentage#", ltrProgressStatus.Text.Replace(" ", ""));
                        sCourseContent += " " + Resources.Resource.lblComplete + "";
                        sCourseContent += "</span>";
                        if (ltrStartDate.Text != "Not Started")
                        {
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + Convert.ToDateTime(ltrStartDate.Text).ToString("MMMM dd, yyyy ") + "</span>";
                        }
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";
                        sbHtml.Append("<div class='metadata'>");
                        sbHtml.Append(BusinessUtility.GetString(sCourseContent));
                        sbHtml.Append("</div>");
                    }
                }
                #endregion
            }
            else if (ltrScormType.Text == "SIG")
            {
                #region SINGLE
                if (ltrCourseTestType.Text == "CT")
                {
                    EmployeeCourses objempCourse = new EmployeeCourses();
                    if (objempCourse.IsCourseCompleted(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == true)
                    {
                        objEvent.GetEventDetail(BusinessUtility.GetInt(ltrCourseID.Text), Globals.CurrentAppLanguageCode);
                        if (iPassingGrade < 0)
                        {
                            iPassingGrade = BusinessUtility.GetInt(objEvent.CoursePassingGrade);
                        }
                        iNoAttempts = BusinessUtility.GetInt(objEvent.Course_no_of_attempts);
                        if (BusinessUtility.GetInt(objEvent.TestCompletedCount) == 1)
                        {
                            isCompletedCount = true;
                        }
                        else
                        {
                            isCompletedCount = false;
                        }

                        if (!string.IsNullOrEmpty(objEvent.EventName))
                        {
                            EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();

                            objEmployeeCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));
                            iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.courseTestRegID);
                            if (BusinessUtility.GetInt(iTestCourseRegID) <= 0)
                            {
                                //iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo)));
                                iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo)));
                            }
                        }

                        if (objempCourse.IsEmployeeCompleteEvent(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == true)
                        {
                            Literal ltrTestAttempted = (Literal)e.Item.FindControl("ltrTestAttempted");
                            string sCourseCompletedDate = objempCourse.CourseCompletedDate(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));

                            sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblCourseSuccessfullyCompleted).Replace("#CourseTitle#", ltrCourseTitle.Text));
                            string sCourseContent = "";
                            sCourseContent += "<div class='metadata-group'>";
                            sCourseContent += "<div class='column span-4'>";
                            sCourseContent += "<h6 class='metadata-value title'>" + Resources.Resource.lblCourseContent + "</h6>";
                            sCourseContent += "</div>";
                            sCourseContent += "<div class='column span-8'>";
                            sCourseContent += "<p>" + Resources.Resource.lblCourseSuccessfullyCompleted + "</p>";
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + Convert.ToDateTime(ltrStartDate.Text).ToString("MMMM dd, yyyy ") + "</span>";
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseCompletedDate + " — " + Convert.ToDateTime(sCourseCompletedDate).ToString("MMMM dd, yyyy ") + "</span>";
                            sCourseContent += "</div>";
                            sCourseContent += "</div>";
                            sCourseContent += "<div class='metadata-group'>";
                            sCourseContent += "<div class='column span-4'>";
                            sCourseContent += "<h6>" + Resources.Resource.lblTest + "</h6>";
                            sCourseContent += "</div>";
                            sCourseContent += "<div class='column span-8'>";

                            DataTable dtTestAttempts = objempCourse.TestCourseAttempts(iTestCourseRegID, isCompletedCount);
                            if (dtTestAttempts.Rows.Count > 0)
                            {
                                int iAttemptsCount = 1;
                                sCourseContent += "<div class='child-metadata'>";
                                foreach (DataRow dRowAttempt in dtTestAttempts.Rows)
                                {
                                    sCourseContent += "<dl class='child-metadata-group'>";
                                    sCourseContent += "<dt class='metadata-value title'>" + Resources.Resource.lblAttempt + " " + BusinessUtility.GetString(iAttemptsCount) + "</dt>";
                                    ErrorLog.createLog("3  Reg ID " + iTestCourseRegID + "raw score " + BusinessUtility.GetString(dRowAttempt["score_raw"]) + " Passing Grade " + iPassingGrade);
                                    if (BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])) >= iPassingGrade)
                                    {
                                        sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblPassed + "</dd>";
                                    }
                                    else
                                    {
                                        sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblFailed + "</dd>";
                                    }
                                    sCourseContent += "<dd class='metadata-value'>" + Convert.ToDateTime(BusinessUtility.GetString(dRowAttempt["createdDateTime"])).ToString("MMMM dd, yyyy ") + "</dd>";
                                    if (BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])).ToString("###.##") != "")
                                    {
                                        sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblScore + " &mdash;  " + BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])).ToString("###.##") + "%</dd>";
                                    }
                                    else
                                    {
                                        sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblScore + " &mdash;  0%</dd>";
                                    }
                                    sCourseContent += "</dl>";
                                    iAttemptsCount += 1;
                                }
                                sCourseContent += "</div>";
                            }
                            else
                            {
                                sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblHaveNotMadeAnyAttempts + "</span>";
                            }
                            sCourseContent += "</div>";
                            sCourseContent += "</div>";

                            sbHtml.Append("<div class='metadata'>");
                            sbHtml.Append(BusinessUtility.GetString(sCourseContent));
                            sbHtml.Append("</div>");
                        }
                        System.Web.UI.HtmlControls.HtmlGenericControl divCourseProgressContentStatus1 = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvCourseProgressContentStatus");
                        divCourseProgressContentStatus1.InnerHtml = BusinessUtility.GetString(sbHtml);

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "showProgress", "ShowProgressStatus();", true);
                        return;
                    }

                    objEvent.GetEventDetail(BusinessUtility.GetInt(ltrCourseID.Text), Globals.CurrentAppLanguageCode);
                    if (iPassingGrade < 0)
                    {
                        iPassingGrade = BusinessUtility.GetInt(objEvent.CoursePassingGrade);
                    }
                    iNoAttempts = BusinessUtility.GetInt(objEvent.Course_no_of_attempts);
                    if (BusinessUtility.GetInt(objEvent.TestCompletedCount) == 1)
                    {
                        isCompletedCount = true;
                    }
                    else
                    {
                        isCompletedCount = false;
                    }

                    if (!string.IsNullOrEmpty(objEvent.EventName))
                    {
                        EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();

                        objEmployeeCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));
                        iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.courseTestRegID);


                        if (BusinessUtility.GetInt(iTestCourseRegID) <= 0)
                        {
                            iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo)));
                        }

                        //Literal ltrIsRepeated = (Literal)e.Item.FindControl("ltrIsRepeated");
                        //iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo), BusinessUtility.GetInt(ltrIsRepeated.Text)));
                    }

                    if (objempCourse.IsEmployeeCompleteEvent(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == true)
                    {
                        Literal ltrTestAttempted = (Literal)e.Item.FindControl("ltrTestAttempted");
                        string sCourseCompletedDate = objempCourse.CourseCompletedDate(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));

                        EmployeeCourseRegistration objempTestAttmpCount = new EmployeeCourseRegistration();
                        int iTestAttemptRequestedCount = 0;
                        iTestAttemptRequestedCount = objempTestAttmpCount.GetRequestedTestAttemptCount(iTestCourseRegID);

                        if (objempTestAttmpCount.GetRequestedTestEligable(iTestCourseRegID) == true)
                        {
                            sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblTestAttempRequestMadeMessage).Replace("#CourseTitle#", ltrCourseTitle.Text));
                        }
                        else
                        {
                            if (BusinessUtility.GetInt(ltrTestAttempted.Text) >= iNoAttempts + iTestAttemptRequestedCount)
                            {
                                sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblNoAttemptsLeftontheTest).Replace("#CourseTitle#", ltrCourseTitle.Text));
                            }
                            else
                            {
                                sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblEligleToTakeTest).Replace("#CourseTitle#", ltrCourseTitle.Text));
                            }
                        }

                        string sCourseContent = "";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6 class='metadata-value title'>" + Resources.Resource.lblCourseContent + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";
                        sCourseContent += "<p>" + Resources.Resource.lblCourseSuccessfullyCompleted + "</p>";
                        sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + Convert.ToDateTime(ltrStartDate.Text).ToString("MMMM dd, yyyy ") + "</span>";
                        sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseCompletedDate + " — " + Convert.ToDateTime(sCourseCompletedDate).ToString("MMMM dd, yyyy ") + "</span>";
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6>" + Resources.Resource.lblTest + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";

                        if (objempTestAttmpCount.GetRequestedTestEligable(iTestCourseRegID) == true)
                        {
                            sCourseContent += "<p>" + Resources.Resource.lblTestAttempRequestMadeMessage + "</p>";
                        }
                        else
                        {
                            if (BusinessUtility.GetInt(ltrTestAttempted.Text) >= iNoAttempts + iTestAttemptRequestedCount)
                            {
                                sCourseContent += "<p>" + Resources.Resource.lblNoAttemptsLeft + "</p>";
                            }
                            else
                            {
                                sCourseContent += "<p>" + Resources.Resource.lblEligleToTakeTest + "</p>";
                            }
                        }

                        DataTable dtTestAttempts = objempCourse.TestCourseAttempts(iTestCourseRegID, isCompletedCount);
                        if (dtTestAttempts.Rows.Count > 0)
                        {
                            int iAttemptsCount = 1;
                            sCourseContent += "<div class='child-metadata'>";
                            foreach (DataRow dRowAttempt in dtTestAttempts.Rows)
                            {
                                sCourseContent += "<dl class='child-metadata-group'>";
                                sCourseContent += "<dt class='metadata-value title'>" + Resources.Resource.lblAttempt + " " + BusinessUtility.GetString(iAttemptsCount) + "</dt>";
                                //ErrorLog.createLog("4 raw score " + BusinessUtility.GetString(dRowAttempt["score_raw"]) + " Passing Grade " + iPassingGrade);
                                if (BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])) >= iPassingGrade)
                                {
                                    sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblPassed + "</dd>";
                                }
                                else
                                {
                                    sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblFailed + "</dd>";
                                }
                                sCourseContent += "<dd class='metadata-value'>" + Convert.ToDateTime(BusinessUtility.GetString(dRowAttempt["createdDateTime"])).ToString("MMMM dd, yyyy ") + "</dd>";
                                if (BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])).ToString("###.##") != "")
                                {
                                    sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblScore + " &mdash;  " + BusinessUtility.GetDecimal(BusinessUtility.GetString(dRowAttempt["score_raw"])).ToString("###.##") + "%</dd>";
                                }
                                else
                                {
                                    sCourseContent += "<dd class='metadata-value'>" + Resources.Resource.lblScore + " &mdash;  0%</dd>";
                                }
                                sCourseContent += "</dl>";
                                iAttemptsCount += 1;
                            }
                            sCourseContent += "</div>";
                        }
                        else
                        {
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblHaveNotMadeAnyAttempts + "</span>";
                        }
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";

                        sbHtml.Append("<div class='metadata'>");
                        sbHtml.Append(BusinessUtility.GetString(sCourseContent));
                        sbHtml.Append("</div>");
                    }
                    else
                    {
                        sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblCourseStartedButNotFinistContent).Replace("#CourseTitle#", ltrCourseTitle.Text));
                        string sCourseContent = "";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6 class='metadata-value title'>" + Resources.Resource.lblCourseContent + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";
                        sCourseContent += "<span class='metadata-value'>";
                        sCourseContent += Resources.Resource.lblCourseStartedButNotFinished;
                        sCourseContent += "</span>";
                        try
                        {
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + Convert.ToDateTime(ltrStartDate.Text).ToString("MMMM dd, yyyy ") + "</span>";
                        }
                        catch (Exception err)
                        {
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + DateTime.Now.ToString("MMMM dd, yyyy ") + "</span>";
                        }
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6>" + Resources.Resource.lblTest + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";
                        sCourseContent += "<p>" + Resources.Resource.lblNotEligableToTakeTestUntilCompletContent + "</p>";
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";

                        sbHtml.Append("<div class='metadata'>");
                        sbHtml.Append(BusinessUtility.GetString(sCourseContent));
                        sbHtml.Append("</div>");
                    }
                }
                else if (ltrCourseTestType.Text == "CO")
                {
                    EmployeeCourses objempCourse = new EmployeeCourses();
                    if (objempCourse.IsCourseCompleted(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == true)
                    {
                        string sCourseCompletedDate = objempCourse.CourseCompletedDate(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));
                        sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblCourseSuccessfullyCompleted).Replace("#CourseTitle#", ltrCourseTitle.Text));
                        string sCourseContent = "";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6 class='metadata-value title'>" + Resources.Resource.lblCourseContent + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";
                        sCourseContent += "<span class='metadata-value'>";
                        sCourseContent += " " + Resources.Resource.lblCourseSuccessfullyCompleted + "";
                        sCourseContent += "</span>";
                        sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + Convert.ToDateTime(ltrStartDate.Text).ToString("MMMM dd, yyyy ") + "</span>";
                        sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseCompletedDate + " — " + Convert.ToDateTime(sCourseCompletedDate).ToString("MMMM dd, yyyy ") + "</span>";
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";

                        sbHtml.Append("<div class='metadata'>");
                        sbHtml.Append(BusinessUtility.GetString(sCourseContent));
                        sbHtml.Append("</div>");
                    }
                    else
                    {
                        sbHtml.Append(sCourseStatus.Replace("#CourseStatus#", Resources.Resource.lblCourseStartedButNotFinistContent).Replace("#CourseTitle#", ltrCourseTitle.Text));
                        string sCourseContent = "";
                        sCourseContent += "<div class='metadata-group'>";
                        sCourseContent += "<div class='column span-4'>";
                        sCourseContent += "<h6 class='metadata-value title'>" + Resources.Resource.lblCourseContent + "</h6>";
                        sCourseContent += "</div>";
                        sCourseContent += "<div class='column span-8'>";
                        sCourseContent += "<span class='metadata-value'>";
                        sCourseContent += Resources.Resource.lblCourseStartedButNotFinished;
                        sCourseContent += "</span>";
                        try
                        {
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + Convert.ToDateTime(ltrStartDate.Text).ToString("MMMM dd, yyyy ") + "</span>";
                        }
                        catch (Exception err)
                        {
                            sCourseContent += "<span class='metadata-value'>" + Resources.Resource.lblCourseStartDate + " — " + DateTime.Now.ToString("MMMM dd, yyyy ") + "</span>";
                        }
                        sCourseContent += "</div>";
                        sCourseContent += "</div>";

                        sbHtml.Append("<div class='metadata'>");
                        sbHtml.Append(BusinessUtility.GetString(sCourseContent));
                        sbHtml.Append("</div>");
                    }
                }
                #endregion
            }

            System.Web.UI.HtmlControls.HtmlGenericControl divCourseProgressContentStatus = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvCourseProgressContentStatus");
            divCourseProgressContentStatus.InnerHtml = BusinessUtility.GetString(sbHtml);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "showProgress", "ShowProgressStatus();", true);
        }
        else if (e.CommandName == CourseAction.DownloadTest)
        {
            Literal ltrEmpHeaderID = (Literal)e.Item.FindControl("ltrEmpHeaderID");
            Literal ltrCourseTestID = (Literal)e.Item.FindControl("ltrCourseTestID");
            Literal ltrEmpCourseRegID = (Literal)e.Item.FindControl("ltrEmpCourseRegID");
            Literal ltrIsRepeated = (Literal)e.Item.FindControl("ltrIsRepeated");

            Event objEvent = new Event();
            objEvent.GetEventDetail(BusinessUtility.GetInt(ltrCourseTestID.Text), Globals.CurrentAppLanguageCode);
            if (objEvent.CourseCertOffered == true)
            {
                EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();
                objEmployeeCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));
                int iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.courseTestRegID);
                //int iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseTestID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo)));
                if (BusinessUtility.GetInt(iTestCourseRegID) <= 0)
                {
                    //iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo)));
                    iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseTestID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo)));
                }

                CertificateCreate objCertificate = new CertificateCreate();
                if (objCertificate.DownloadTestCertificate(iTestCourseRegID) == true)
                {
                    Certificate objCert = new Certificate();
                    string sSaveFileName = objCert.GetCertificateFilePath(iTestCourseRegID);

                    if (sSaveFileName != "")
                    {
                        string sFolderPath = this.CertificatePath;
                        objCertificate.DownloadCertificate(sFolderPath + sSaveFileName, sSaveFileName.Replace("/", ""));
                    }
                }
            }
        }
        else if (e.CommandName == CourseAction.ReviewCourseContent)
        {
            Literal ltrEmpHeaderID = (Literal)e.Item.FindControl("ltrEmpHeaderID");
            Literal ltrCourseID = (Literal)e.Item.FindControl("ltrCourseID");
            Literal ltrCourseVersion = (Literal)e.Item.FindControl("ltrCourseVersion");
            Literal ltrCourseExternalID = (Literal)e.Item.FindControl("ltrCourseExternalID");
            Literal ltrEmpCourseRegID = (Literal)e.Item.FindControl("ltrEmpCourseRegID");
            Literal ltrIsRepeated = (Literal)e.Item.FindControl("ltrIsRepeated");




            //EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();
            //int iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(ltrCourseVersion.Text), BusinessUtility.GetInt(ltrIsRepeated.Text)));
            //if (iRegID <= 0)
            //{
            //}
            //else
            //{
            //    LaunchUrlReviewContent(BusinessUtility.GetString(ltrEmpCourseRegID.Text), BusinessUtility.GetString(ltrEmpCourseRegID.Text));
            //}

            if (BusinessUtility.GetInt(BusinessUtility.GetString(ltrEmpCourseRegID.Text)) > 0)
            {
                LaunchUrlReviewContent(BusinessUtility.GetString(ltrEmpCourseRegID.Text), BusinessUtility.GetString(ltrEmpCourseRegID.Text));
            }
            //ErrorLog.createLog("Reg ID "+ iRegID +" CoureRegID =" +  BusinessUtility.GetString(ltrEmpCourseRegID.Text) +" ");
        }
    }

    /// <summary>
    /// To Launch Course URL
    /// </summary>
    /// <param name="sRegID">Pass Registration ID</param>
    /// <param name="sParentRegID">Pass Course Registration ID in case of test course</param>
    private void LaunchUrl(string sRegID, string sParentRegID)
    {
        if (ScormCloud.RegistrationService.RegistrationExists(Utils.ScromRegPrfx + BusinessUtility.GetString(sRegID)) == true)
        {
            EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();
            objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(sRegID));

            Event objEvent = new Event();
            objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
            EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.TransferredToRustici,
            BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), objEvent.EventName, BusinessUtility.GetInt(sRegID), (int)RusticRegStatusCode.None, "0");

            EmployeeCourseRegistration objEmpEventReg = new EmployeeCourseRegistration();
            objEmpEventReg.SaveCourseAttempts(BusinessUtility.GetInt(sRegID));

            String postBackUrl = GetScormExitURL + "?CourseStatus=" + BusinessUtility.GetString((int)CourseStatus.Traning_Employee_should_take) + "&regID=" + sRegID + "&pregID=" + sParentRegID;
            string sLaunchURL = ScormCloud.RegistrationService.GetLaunchUrl(Utils.ScromRegPrfx + BusinessUtility.GetString(sRegID), postBackUrl, Utils.ScormCssUrl);
            Response.Redirect(sLaunchURL);
        }
        else
        {
            EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();
            objEmployeeCourseReg.RemoveEmployeeRegistraionID(BusinessUtility.GetInt(sRegID));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowPensivoMessage('" + Resources.Resource.ScromPlayerisNotAvailable + "');", true);
        }
    }

    /// <summary>
    /// To Launch Review Course URL
    /// </summary>
    /// <param name="sRegID">Pass Registration ID</param>
    /// <param name="sParentRegID">Pass Course Registration ID in case of test course</param>
    private void LaunchUrlReviewContent(string sRegID, string sParentRegID)
    {
        if (ScormCloud.RegistrationService.RegistrationExists(Utils.ScromRegPrfx + BusinessUtility.GetString(sRegID)) == true)
        {
            EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();
            objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(sRegID));

            Event objEvent = new Event();
            objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
            EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.TransferredToRustici,
            BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), objEvent.EventName, BusinessUtility.GetInt(sRegID), (int)RusticRegStatusCode.None, "0");

            EmployeeCourseRegistration objEmpEventReg = new EmployeeCourseRegistration();
            objEmpEventReg.SaveCourseAttempts(BusinessUtility.GetInt(sRegID));

            String postBackUrl = GetScormExitURL + "?CourseStatus=" + BusinessUtility.GetString((int)CourseStatus.Traning_Employee_Taking) + "&regID=" + sRegID + "&pregID=" + sParentRegID + "&isReviewContent=1";
            string sLaunchURL = ScormCloud.RegistrationService.GetLaunchUrl(Utils.ScromRegPrfx + BusinessUtility.GetString(sRegID), postBackUrl, Utils.ScormCssUrl);

            ErrorLog.createLog(" Review Course Launch 1 " + sLaunchURL);

            Response.Redirect(sLaunchURL);
        }
        else
        {
            EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();
            objEmployeeCourseReg.RemoveEmployeeRegistraionID(BusinessUtility.GetInt(sRegID));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowPensivoMessage('" + Resources.Resource.ScromPlayerisNotAvailable + "');", true);
        }
    }

    /// <summary>
    /// To Save Registration Scorm XML Result
    /// </summary>
    /// <param name="regID">Pass Registration ID</param>
    private void SaveRegistrationXMLResult(int regID)
    {
        try
        {
            if (this.IsReviewCourseContent)
            {
                return;
            }


            string result2 = "";
            DbHelper dbHelp = new DbHelper(false);
            PostXml postXmlTrack = new PostXml();
            string sXMLFilePath = "";
            int postXmlTrackID = 0;
            Boolean result = false;
            String postBackUrl = "";

            result2 = ScormCloud.RegistrationService.GetRegistrationResult(Utils.ScromRegPrfx + BusinessUtility.GetString(regID), RegistrationResultsFormat.FULL);

            // temporary fix for descripton tag  by rajat
            //int firstIndexOfDesc = result2.IndexOf("<description>");
            //int lastIndexOfDesc = 36;
            //StringBuilder sb = new StringBuilder(result2);
            //sb.Remove(firstIndexOfDesc, lastIndexOfDesc);
            //sb.Insert(firstIndexOfDesc, "<description>Chris’ spoke loud enough for me to hear him during his presentation</description>");

            ////result2.Replace(result2.Substring(firstIndexOfDesc, 36), "<description>Chris’ spoke loud enough for me to hear him during his presentation</description>");

            //firstIndexOfDesc = sb.ToString().IndexOf("<description>", sb.ToString().IndexOf("<description>") + 10);
            
            //sb.Remove(firstIndexOfDesc, lastIndexOfDesc);
            //sb.Insert(firstIndexOfDesc, "<description>Chris’ spoke at speed that was easy to understand during his presentation.</description>");

            ////result2.Replace(result2.Substring(firstIndexOfDesc, 36), "<description>Chris’ spoke at speed that was easy to understand during his presentation.</description>");
            //result2 = sb.ToString();
            // temporary fix for descripton tag  by rajat

            sXMLFilePath = Import.SaveXMLFile(result2, BusinessUtility.GetString(PostType.G), regID);
            postXmlTrackID = postXmlTrack.SetPostedData(dbHelp, result2, PostType.G, PostDirection.INT, regID, sXMLFilePath);
            //ErrorLog.createLog("Save XML 1");
            result = Import.SaveRegistrationXML(result2, BusinessUtility.GetString(PostType.G));
            postBackUrl = Utils.GetSiteName() + "/EmployeeCourses.aspx?CourseStatus=" + this.CourseSelectedStatus;


            if (BusinessUtility.GetInt(regID) > 0)
            {
                EmployeeCourses objEmpCourse = new EmployeeCourses();
                EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();
                objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(regID));

                Event objEvent = new Event();
                objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);

                RegisterEventResult objRegEvntRslt = new RegisterEventResult();
                if (objRegEvntRslt.IsGTypeRequestCompleted(regID) == false)
                {
                    if ((objEvent.TestType != CourseTestType.TestOnly))
                    {
                        result2 = ScormCloud.RegistrationService.GetRegistrationResult(Utils.ScromRegPrfx + BusinessUtility.GetString(regID), RegistrationResultsFormat.FULL);
                        sXMLFilePath = Import.SaveXMLFile(result2, BusinessUtility.GetString(PostType.G), regID);
                        postXmlTrackID = postXmlTrack.SetPostedData(dbHelp, result2, PostType.G, PostDirection.INT, regID, sXMLFilePath);
                        result = Import.SaveRegistrationXML(result2, BusinessUtility.GetString(PostType.G));
                    }
                    //else if ((objEvent.TestType == CourseTestType.TestOnly) && (objEvent.TestCompletedCount == 1))
                    //{
                    //    //result2 = ScormCloud.RegistrationService.GetRegistrationResult(Utils.ScromRegPrfx + BusinessUtility.GetString(regID), RegistrationResultsFormat.FULL);
                    //    //sXMLFilePath = Import.SaveXMLFile(result2, BusinessUtility.GetString(PostType.G), regID);
                    //    //postXmlTrackID = postXmlTrack.SetPostedData(dbHelp, result2, PostType.G, PostDirection.INT, regID, sXMLFilePath);
                    //    //result = Import.SaveRegistrationXML(result2, BusinessUtility.GetString(PostType.G));
                    //}
                    postBackUrl = Utils.GetSiteName() + "/EmployeeCourses.aspx?CourseStatus=" + this.CourseSelectedStatus;
                }


                EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.ReturnedToPibFromRustici,
                    BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), objEvent.EventName, BusinessUtility.GetInt(regID), (int)RusticRegStatusCode.None, "");

                if (BusinessUtility.GetInt(Request.QueryString["pregID"]) > 0)
                {
                    if ((objEmpCourse.IsCourseCompleted(BusinessUtility.GetInt(Request.QueryString["pregID"])) == true))
                    {
                        objempCourseReg.SaveCourseCompleted(BusinessUtility.GetInt(regID));
                        Employee objEmployee = new Employee();
                        objEmployee.GetAssignedFeedbackEmployeeDetailByRegID(regID);
                        showFeedbackCompletedConfirmationMessage(Resources.Resource.lblfeedbackCompletedConfirmationTitle,objEmployee.EmpName, Resources.Resource.lblFeedbackCompletedMessage);
                        return;
                        /*
                        CertificateCreate objCertificate = new CertificateCreate();
                        postBackUrl = Utils.GetSiteName() + "/EmployeeCourses.aspx?CourseStatus=" + (int)CourseStatus.Traning_Employee_have_taken;
                        hdnPostURL.Value = postBackUrl;
                        hdnCertificateDownloadRegID.Value = BusinessUtility.GetString(Request.QueryString["pregID"]);

                        if ((objEvent.TestType == CourseTestType.TestOnly))
                        {
                            if (objEvent.CourseCertOffered == true)
                            {
                                if (objCertificate.DownloadTestCertificate(regID) == true)
                                {
                                }
                            }
                        }

                        objempCourseReg = new EmployeeCourseRegistration();
                        objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(BusinessUtility.GetInt(Request.QueryString["pregID"])));
                        objEvent = new Event();
                        objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
                        if (objEvent.CourseCertOffered == true)
                        {
                            objCertificate.DownloadCertificate(BusinessUtility.GetInt(hdnCertificateDownloadRegID.Value), false);

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "launchDownload", "downloadCertificate();", true);
                            return;
                        }
                        else
                        {
                            objempCourseReg = new EmployeeCourseRegistration();
                            objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(BusinessUtility.GetInt(Request.QueryString["pregID"])));
                            objEvent = new Event();
                            objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
                            if (objEvent.CourseCertOffered == true)
                            {
                                objCertificate.DownloadCertificate(BusinessUtility.GetInt(hdnCertificateDownloadRegID.Value), false);
                            }

                            postBackUrl = Utils.GetSiteName() + "/EmployeeCourses.aspx?CourseStatus=" + (int)CourseStatus.Traning_Employee_have_taken;
                            Response.Redirect(postBackUrl, false);
                        }
                        */

                        //if (objEvent.CourseCertOffered == true)
                        //{
                        //    if ((objEvent.TestType == CourseTestType.TestOnly))
                        //    {
                        //        if (objEvent.CourseCertOffered == true)
                        //        {
                        //            if (objCertificate.DownloadTestCertificate(regID) == true)
                        //            {
                        //            }
                        //        }
                        //    }

                        //    objempCourseReg = new EmployeeCourseRegistration();
                        //    objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(BusinessUtility.GetInt(Request.QueryString["pregID"])));
                        //    objEvent = new Event();
                        //    objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
                        //    if (objEvent.CourseCertOffered == true)
                        //    {
                        //        objCertificate.DownloadCertificate(BusinessUtility.GetInt(hdnCertificateDownloadRegID.Value), false);
                        //    }

                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "launchDownload", "downloadCertificate();", true);
                        //    return;
                        //}
                        //else
                        //{
                        //    objempCourseReg = new EmployeeCourseRegistration();
                        //    objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(BusinessUtility.GetInt(Request.QueryString["pregID"])));
                        //    objEvent = new Event();
                        //    objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
                        //    if (objEvent.CourseCertOffered == true)
                        //    {
                        //        objCertificate.DownloadCertificate(BusinessUtility.GetInt(hdnCertificateDownloadRegID.Value), false);
                        //    }

                        //    postBackUrl = Utils.GetSiteName() + "/EmployeeCourses.aspx?CourseStatus=" + (int)CourseStatus.Traning_Employee_have_taken;
                        //    Response.Redirect(postBackUrl, false);
                        //}
                    }
                    else
                    {
                        if ((objEvent.TestType == CourseTestType.TestOnly))
                        {
                            /*
                            // commented by rajat on 20160425 for not displaying test attempt completed messsage
                            EmployeeCourseRegistration objempTestAttmpCount = new EmployeeCourseRegistration();
                            int iTotalTestAttempted = objempTestAttmpCount.GetTestAttemptCount(BusinessUtility.GetInt(Request.QueryString["pregID"]));
                            if (iTotalTestAttempted > 0)
                            {
                                int iTestAttemptRequestedCount = 0;
                                iTestAttemptRequestedCount = objempTestAttmpCount.GetRequestedTestAttemptCount(regID);

                                if (iTotalTestAttempted >= (objEvent.Course_no_of_attempts + iTestAttemptRequestedCount))
                                {
                                    EmployeeCourseRegistration objEmpReg = new EmployeeCourseRegistration();


                                    int testAttemptsEliggableMins = Utils.TestAttemptsEligableMins;
                                    if (BusinessUtility.GetInt(Request.QueryString["pregID"]) > 0)
                                    {
                                        objEmpReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(Request.QueryString["pregID"]));
                                        if (BusinessUtility.GetInt(objEmpReg.attemptGrantWaitingTime) > 0)
                                        {
                                            testAttemptsEliggableMins = BusinessUtility.GetInt(objEmpReg.attemptGrantWaitingTime);
                                        }
                                    }

                                    //objEmpReg.SaveTestAttemptRequest(BusinessUtility.GetInt(regID), Utils.TestAttemptsNo, "", Utils.TestAttemptsEligableMins);
                                    objEmpReg.SaveTestAttemptRequest(BusinessUtility.GetInt(regID), Utils.TestAttemptsNo, "", testAttemptsEliggableMins);

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "AttemptRequestDialog", "ShowTestAttemptsFailure(" + regID + "," + BusinessUtility.GetInt(Request.QueryString["pregID"]) + ");", true);
                                    return;
                                } 
                              }
                            */
                        }

                    }
                }
                else
                {
                    if ((objEmpCourse.IsCourseCompleted(regID) == true))
                    {
                        //commented by rajat
                        //postBackUrl = Utils.GetSiteName() + "/EmployeeCourses.aspx?CourseStatus=" + (int)CourseStatus.Traning_Employee_have_taken;
                        //hdnPostURL.Value = postBackUrl;
                        //hdnCertificateDownloadRegID.Value = BusinessUtility.GetString(regID);

                        //objempCourseReg = new EmployeeCourseRegistration();
                        //objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(BusinessUtility.GetInt(hdnCertificateDownloadRegID.Value)));
                        //objEvent = new Event();
                        //objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
                        //commented by rajat
                        /*// commented by rajat
                        if (objEvent.CourseCertOffered == true)
                        {
                            //CertificateCreate objCertificate = new CertificateCreate(); 
                            //objCertificate.DownloadCertificate(BusinessUtility.GetInt(hdnCertificateDownloadRegID.Value), false);
                        }
                        */// commented by rajat

                        objempCourseReg.SaveCourseCompleted(BusinessUtility.GetInt(regID));
                        Employee objEmployee = new Employee();
                        objEmployee.GetAssignedFeedbackEmployeeDetailByRegID(regID);
                        showFeedbackCompletedConfirmationMessage(Resources.Resource.lblfeedbackCompletedConfirmationTitleWhenCameThroughLink,objEmployee.EmpName, Resources.Resource.lblFeedbackCompletedMessage);

                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "launchDownload", "downloadCertificate();", true);// commented by rajat
                        return;
                    }
                }

                if ((objEvent.TestType == CourseTestType.CourseTest)) 
                {
                    int iTestEventID = BusinessUtility.GetInt(objEvent.TestExtID);
                    int iEmpID = objempCourseReg.empID;
                    int iVerNo = objEvent.CourseVerNo;
                    if (objempCourseReg.IsUserStartCourse(iEmpID, iTestEventID, iVerNo, BusinessUtility.GetInt(regID)) == false)
                    {
                        if ((objEmpCourse.IsEmployeeCompleteEvent(BusinessUtility.GetInt(regID)) == true))
                        {
                            EmployeeCourseRegistration obeEmpReg = new EmployeeCourseRegistration();
                            obeEmpReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(regID));

                            hdnCourseVerNo.Value = BusinessUtility.GetString(iVerNo);
                            hdnEmpID.Value = BusinessUtility.GetString(iEmpID);
                            hdnTestEventID.Value = BusinessUtility.GetString(iTestEventID);
                            hdnpregID.Value = BusinessUtility.GetString(regID);
                            hdnPostURL.Value = postBackUrl;
                            hdnIsRepeated.Value = BusinessUtility.GetString(BusinessUtility.GetInt(obeEmpReg.isRepeated));
                            hdnEmpCourseAssignedID.Value = BusinessUtility.GetString(BusinessUtility.GetInt(obeEmpReg.empCourseAssignedID));

                            EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();

                            objEmployeeCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(regID));
                            int iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.courseTestRegID);

                            if (iTestCourseRegID <= 0) 
                            {
                                objempCourseReg.SaveCourseCompleted(BusinessUtility.GetInt(regID));
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "launchCOurse", "launchCourse();", true);
                            }
                            else
                            {
                                Response.Redirect(postBackUrl, false);
                            }
                        }
                        else
                        {
                            Response.Redirect(postBackUrl, false);
                        }
                    }
                    else
                    {
                        Response.Redirect(postBackUrl, false);
                    }
                }
                else
                {
                    Response.Redirect(postBackUrl, false);
                }
            }
            else
            {
                Response.Redirect(postBackUrl, false);
            }
        }
        catch (Exception e)
        {
            ErrorLog.createLog(e.ToString());
        }
    }

    /// <summary>
    /// To Get Scorm Exit URL
    /// </summary>
    public string GetScormExitURL
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("ScormExitURL"));
        }
    }

    /// <summary>
    /// To Get Scorm Callback URL
    /// </summary>
    public string GetScormCallbackURL
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("ScormCallbackURL"));
        }
    }

    /// <summary>
    /// To Get Registration ID
    /// </summary>
    public int RegID
    {
        get
        {
            int cStatus = 0;
            int.TryParse(Request.QueryString["regID"], out cStatus);
            return cStatus;
        }
    }

    /// <summary>
    /// To Get Is Review Content
    /// </summary>
    public bool IsReviewCourseContent
    {
        get
        {
            int cReviewContent = 0;
            int.TryParse(Request.QueryString["isReviewContent"], out cReviewContent);
            return cReviewContent == 1;
        }
    }

    /// <summary>
    /// To Get Course Launch Button Text
    /// </summary>
    /// <returns>String</returns>
    protected string GetLaunchText()
    {
        if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_Taking)
        {
            return Resources.Resource.lblCountinue;
        }
        else if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_have_taken)
        {
            return Resources.Resource.lblDownloadCertificate;
        }
        else
        {
            return Resources.Resource.lblStart;
        }
    }

    /// <summary>
    /// To Get Course Status Text
    /// </summary>
    /// <returns>String</returns>
    protected string GetCourseStatus()
    {
        if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_Taking)
        {
            return Resources.Resource.lblCourseStarted;
        }
        else if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_should_take)
        {
            return Resources.Resource.lblCourseNotStarted;
        }
        else
        {
            return Resources.Resource.lblCourseCompleted;
        }
    }

    /// <summary>
    /// To Get No Record Found Message
    /// </summary>
    /// <returns>String</returns>
    protected string GetNoRecordFoundMessage()
    {
        if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_Taking)
        {
            return Resources.Resource.lblNoTraininAtThisTime;
        }
        else if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_should_take)
        {
            return Resources.Resource.lblNoTrainingToTake;
        }
        else
        {
            return Resources.Resource.lblNoTrainingHaveTaken;
        }
    }

    /// <summary>
    /// To Define List Bound Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass List View Item Event Args</param>
    protected void lstEmployeeCourses_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        HiddenField hdnSummary = (HiddenField)e.Item.FindControl("hdnSummary");
        HtmlGenericControl liSummary = (HtmlGenericControl)e.Item.FindControl("liSummary");
        HtmlGenericControl divShowContent = (HtmlGenericControl)e.Item.FindControl("divShowContent");
        Literal ltrCourseTestType = (Literal)e.Item.FindControl("ltrCourseTestType");
        Literal ltrEmpCourseRegID = (Literal)e.Item.FindControl("ltrEmpCourseRegID");
        Literal ltrCourseTestID = (Literal)e.Item.FindControl("ltrCourseTestID");
        Literal ltrTestAttempted = (Literal)e.Item.FindControl("ltrTestAttempted");
        Literal ltrStatusTitle = (Literal)e.Item.FindControl("ltrStatusTitle");
        Literal ltrCourseStatus = (Literal)e.Item.FindControl("ltrCourseStatus");
        Literal ltrScormType = (Literal)e.Item.FindControl("ltrScormType");
        Literal ltrProgressStatus = (Literal)e.Item.FindControl("ltrProgressStatus");
        Literal ltrTrainingEventID = (Literal)e.Item.FindControl("ltrCourseID");
        Literal ltrCourseTitle = (Literal)e.Item.FindControl("ltrCourseTitle");
        HtmlGenericControl hCourseTitle = (HtmlGenericControl)e.Item.FindControl("hCourseTitle");

        if (this.TraningYear == 2015)
        {
            LinkButton lnbProgressStatus = (LinkButton)e.Item.FindControl("lnbProgressStatus");
            //lnbProgressStatus.Visible = false;
        }


        //if (hCourseTitle != null)
        //{
        //    Event objEvent = new Event();
        //    objEvent.GetEventDetail(BusinessUtility.GetInt(ltrTrainingEventID.Text), Globals.CurrentAppLanguageCode);
        //    hCourseTitle.InnerHtml = BusinessUtility.GetString(objEvent.EventName);
        //    ltrCourseTitle.Text = BusinessUtility.GetString(objEvent.EventName);
        //}

        if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_Taking)
        {
            if (ltrCourseTestType.Text.ToUpper() == CourseTestType.CourseTest.ToUpper())
            {
                Event objEvent = new Event();
                objEvent.GetEventDetail(BusinessUtility.GetInt(ltrCourseTestID.Text), Globals.CurrentAppLanguageCode);
                int iNoAttempts = BusinessUtility.GetInt(objEvent.Course_no_of_attempts);

                LinkButton lnkbLaunchTest = (LinkButton)e.Item.FindControl("lnkbLaunchTest");
                lnkbLaunchTest.Visible = true;

                EmployeeCourses objempCourse = new EmployeeCourses();
                if (objempCourse.IsEmployeeCompleteEvent(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == true)
                {
                    LinkButton lnkbLaunch = (LinkButton)e.Item.FindControl("lnkbLaunch");
                    lnkbLaunch.Text = Resources.Resource.lblReviewTheCompletedContent;
                    lnkbLaunchTest.Attributes.Add("class", "btn fluid skin2");
                }
                else
                {
                    lnkbLaunchTest.Attributes.Add("class", "btn fluid skin2 is-disabled");
                    lnkbLaunchTest.Attributes.Add("onclick", "return ShowPensivoMessage('" + Resources.Resource.lblPleaseCompleteCourse + "')");
                }

                if (iNoAttempts > 0)
                {
                    Literal ltrEmpHeaderID = (Literal)e.Item.FindControl("ltrEmpHeaderID");
                    Literal ltrCourseID = (Literal)e.Item.FindControl("ltrCourseTestID");

                    EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();



                    objEmployeeCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(ltrEmpCourseRegID.Text));
                    int iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.courseTestRegID);

                    //int iTestCourseRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(BusinessUtility.GetInt(ltrEmpHeaderID.Text), BusinessUtility.GetInt(ltrCourseID.Text), BusinessUtility.GetInt(objEvent.CourseVerNo)));

                    int iTestAttemptRequestedCount = 0;
                    iTestAttemptRequestedCount = objEmployeeCourseReg.GetRequestedTestAttemptCount(iTestCourseRegID);

                    if (BusinessUtility.GetInt(ltrTestAttempted.Text) >= (iNoAttempts + iTestAttemptRequestedCount))
                    {
                        lnkbLaunchTest.Attributes.Add("class", "btn fluid skin2 is-disabled");
                        lnkbLaunchTest.Attributes.Add("onclick", "return ShowNoAttemptsDialog()");
                    }
                }
            }
        }

        if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_have_taken)
        {
            if (ltrCourseTestType.Text.ToUpper() == CourseTestType.CourseTest.ToUpper())
            {
                ltrStatusTitle.Text = Resources.Resource.lblTest;
                ltrCourseStatus.Text = Resources.Resource.lblPassed;
            }
            else
            {
                ltrStatusTitle.Text = Resources.Resource.lblCourse;
                ltrCourseStatus.Text = Resources.Resource.lblCompleted;
            }

            Literal ltrCourseID = (Literal)e.Item.FindControl("ltrCourseID");
            Event objEvent = new Event();
            objEvent.GetEventDetail(BusinessUtility.GetInt(ltrCourseID.Text), Globals.CurrentAppLanguageCode);

            if (objEvent.CourseCertOffered == false)
            {
                LinkButton lnkbLaunch = (LinkButton)e.Item.FindControl("lnkbLaunch");
                lnkbLaunch.Visible = false;
            }

            if (objEvent.IsReviewContent == true)
            {
                LinkButton lnkReviewContent = (LinkButton)e.Item.FindControl("lnkReviewContent");
                lnkReviewContent.Visible = true;
            }

            objEvent = new Event();
            objEvent.GetEventDetail(BusinessUtility.GetInt(ltrCourseTestID.Text), Globals.CurrentAppLanguageCode);
            if (objEvent.CourseCertOffered == true)
            {
                LinkButton lnbDownloadTest = (LinkButton)e.Item.FindControl("lnbDownloadTest");
                lnbDownloadTest.Visible = true;
            }
        }

        if (ltrScormType.Text == ScormType.SingleSCO)
        {
            if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_Taking)
            {
                ltrStatusTitle.Text = Resources.Resource.lblCourse;
                ltrCourseStatus.Text = Resources.Resource.lblCourseStartedNotFinistContent;

                if (ltrCourseTestType.Text.ToUpper() == CourseTestType.CourseTest.ToUpper())
                {
                    EmployeeCourses objEmpCourse = new EmployeeCourses();
                    if ((objEmpCourse.IsEmployeeCompleteEvent(BusinessUtility.GetInt(ltrEmpCourseRegID.Text)) == true))
                    {
                        ltrStatusTitle.Text = Resources.Resource.lblCourse;
                        ltrCourseStatus.Text = Resources.Resource.lblEligibleToTakeTest;
                    }
                }
            }
        }

        if (hdnSummary.Value == "")
        {
            liSummary.Visible = false;
        }

        HiddenField hdnPreview = (HiddenField)e.Item.FindControl("hdnPreview");
        HtmlGenericControl liPreview = (HtmlGenericControl)e.Item.FindControl("liPreview");
        if (hdnPreview.Value == "")
        {
            liPreview.Visible = false;
        }

        HiddenField hdnSampleTest = (HiddenField)e.Item.FindControl("hdnSampleTest");
        HtmlGenericControl liSampleTest = (HtmlGenericControl)e.Item.FindControl("liSampleTest");
        if (hdnSampleTest.Value == "")
        {
            liSampleTest.Visible = false;
        }

        HiddenField hdnArticle = (HiddenField)e.Item.FindControl("hdnArticle");
        HtmlGenericControl liArticle = (HtmlGenericControl)e.Item.FindControl("liArticle");
        if (hdnArticle.Value == "")
        {
            liArticle.Visible = false;
        }

        if (CourseSelectedStatus == (int)CourseStatus.Traning_Employee_should_take) //3
        {
            LinkButton lnbProgressStatus = (LinkButton)e.Item.FindControl("lnbProgressStatus");
            lnbProgressStatus.Visible = false;
        }

        if (hdnSummary.Value != "")
        {
            divShowContent.InnerHtml = hdnSummary.Value;
            liSummary.Attributes.Add("class", "tabs cur row" + e.Item.DataItemIndex);
            liPreview.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            liSampleTest.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            liArticle.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            return;
        }
        else if (hdnPreview.Value != "")
        {
            divShowContent.InnerHtml = hdnPreview.Value;
            liPreview.Attributes.Add("class", "tabs cur row" + e.Item.DataItemIndex);
            liSummary.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            liSampleTest.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            liArticle.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            return;
        }
        else if (hdnSampleTest.Value != "")
        {
            divShowContent.InnerHtml = hdnSampleTest.Value;
            liSampleTest.Attributes.Add("class", "tabs cur row" + e.Item.DataItemIndex);
            liSummary.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            liPreview.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            liArticle.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            return;
        }
        else if (hdnArticle.Value != "")
        {
            divShowContent.InnerHtml = hdnArticle.Value;
            liArticle.Attributes.Add("class", "tabs cur row" + e.Item.DataItemIndex);
            liSummary.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            liPreview.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            liSampleTest.Attributes.Add("class", "tabs row" + e.Item.DataItemIndex);
            return;
        }
    }

    /// <summary>
    /// To Launch Test Course 
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnLaunchTest_Click(object sender, EventArgs e)
    {
        if (hdnCourseVerNo.Value != "" && hdnEmpID.Value != "" && hdnTestEventID.Value != "" && hdnpregID.Value != "")
        {
            int iEmpID = BusinessUtility.GetInt(hdnEmpID.Value);
            int iCourseID = BusinessUtility.GetInt(hdnTestEventID.Value);
            int iCourseVerNo = BusinessUtility.GetInt(hdnCourseVerNo.Value);
            int iIsRepeated = BusinessUtility.GetInt(hdnIsRepeated.Value);
            int empCourseAssignedID = BusinessUtility.GetInt(hdnEmpCourseAssignedID.Value);


            Event objEvent = new Event();
            objEvent.GetEventDetail(BusinessUtility.GetInt(iCourseID), Globals.CurrentAppLanguageCode);

            EmployeeCourseRegistration objEmployeeCourseReg = new EmployeeCourseRegistration();
            //int iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(iEmpID, iCourseID, iCourseVerNo));
            int iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.GetEmployeeRegistraionID(iEmpID, iCourseID, objEvent.CourseVerNo, iIsRepeated));
            if (iRegID <= 0)
            {
                //iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.InsertEmployeeRegistraionID(iEmpID, iCourseID, iCourseVerNo, 0,  Globals.CurrentAppLanguageCode));
                iRegID = BusinessUtility.GetInt(objEmployeeCourseReg.InsertEmployeeRegistraionID(iEmpID, iCourseID, objEvent.CourseVerNo, 0, Globals.CurrentAppLanguageCode, iIsRepeated, empCourseAssignedID));
                if (iRegID > 0)
                {
                    Employee objEmployee = new Employee();
                    objEmployee.GetEmployeeDetail(iEmpID);

                    String postBackUrl = this.GetScormCallbackURL;
                    if (!ScormCloud.RegistrationService.RegistrationExists(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID)))
                    {
                        try
                        {
                            if (BusinessUtility.GetString(objEmployee.EmpFirstName) == "")
                            {
                                objEmployee.EmpFirstName = "Name";
                            }
                            if (BusinessUtility.GetString(objEmployee.EmpLastName) == "")
                            {
                                objEmployee.EmpLastName = "Name";
                            }




                            EmployeeCourseRegistration objCourseReg = new EmployeeCourseRegistration();
                            objCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(hdnpregID.Value));

                            if (objCourseReg.eventLaunchedLanguage == AppLanguageCode.FR)
                            {
                                //ScormCloud.RegistrationService.CreateRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID), BusinessUtility.GetString(objEvent.CourseExtIDFr), iCourseVerNo,
                                //  objEmployee.EmpLogInID, Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-"), Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-"), postBackUrl, RegistrationResultsAuthType.FORM, "", "", RegistrationResultsFormat.FULL);

                                ScormCloud.RegistrationService.CreateRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID), BusinessUtility.GetString(objEvent.CourseExtIDFr), 0,
                                objEmployee.EmpLogInID, Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-"), Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-"), postBackUrl, RegistrationResultsAuthType.FORM, "", "", RegistrationResultsFormat.FULL);

                                if (!ScormCloud.RegistrationService.RegistrationExists(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID)))
                                {
                                    ErrorLog.createLog("Reg ID " + Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID) + " Course Ext ID " + BusinessUtility.GetString(objEvent.CourseExtIDFr)
                                        + " LogIn ID " + objEmployee.EmpLogInID + " First Name " + Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-")
                                        + " Last Name " + Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-") + " Post Back URL " + postBackUrl);
                                    throw new ArgumentNullException(" Test Course Not Registered on Scorm");
                                }
                            }
                            else
                            {

                                ScormCloud.RegistrationService.CreateRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID), BusinessUtility.GetString(objEvent.CourseExtID), 0,
                                objEmployee.EmpLogInID, Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-"), Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-"), postBackUrl, RegistrationResultsAuthType.FORM, "", "", RegistrationResultsFormat.FULL);

                                //ScormCloud.RegistrationService.CreateRegistration(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID), BusinessUtility.GetString(objEvent.CourseExtID), iCourseVerNo,
                                //  objEmployee.EmpLogInID, Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-"), Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-"), postBackUrl, RegistrationResultsAuthType.FORM, "", "", RegistrationResultsFormat.FULL);

                                if (!ScormCloud.RegistrationService.RegistrationExists(Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID)))
                                {
                                    ErrorLog.createLog("Reg ID " + Utils.ScromRegPrfx + BusinessUtility.GetString(iRegID) + " Course Ext ID " + BusinessUtility.GetString(objEvent.CourseExtID)
                                        + " LogIn ID " + objEmployee.EmpLogInID + " First Name " + Utils.ReplaceSpecialCharacter(objEmployee.EmpFirstName, "-")
                                        + " Last Name " + Utils.ReplaceSpecialCharacter(objEmployee.EmpLastName, "-") + " Post Back URL " + postBackUrl);
                                    throw new ArgumentNullException(" Test Course Not Registered on Scorm");
                                }
                            }
                            EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();
                            objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(iRegID));

                            objEvent = new Event();
                            objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);

                            objempCourseReg.SaveCourseTestRegID(BusinessUtility.GetInt(hdnpregID.Value), iRegID);

                            EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.TransferredToRustici,
                            BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), objEvent.EventName, BusinessUtility.GetInt(iRegID), (int)RusticRegStatusCode.Sucess, "1");
                        }
                        catch (Exception error)
                        {
                            EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();
                            objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(iRegID));

                            objEvent = new Event();
                            objEvent.GetEventDetail(BusinessUtility.GetInt(objempCourseReg.eventID), Globals.CurrentAppLanguageCode);
                            EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.ReturnedToPibFromRustici,
                            BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), objEvent.EventName, BusinessUtility.GetInt(0), (int)RusticRegStatusCode.Failure, "");

                            objEmployeeCourseReg.RemoveEmployeeRegistraionID(iRegID);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowPensivoMessage('" + Resources.Resource.lblNotAbleToStartCourse + "');", true);
                            ErrorLog.createLog(error.Message);
                            return;
                        }
                        LaunchUrl(BusinessUtility.GetString(iRegID), hdnpregID.Value);
                    }
                    else
                    {
                        LaunchUrl(BusinessUtility.GetString(iRegID), hdnpregID.Value);
                    }
                }
            }
            else
            {
                LaunchUrl(BusinessUtility.GetString(iRegID), hdnpregID.Value);
            }
        }
    }

    /// <summary>
    /// To Save Certificate Download Registration and Move to Next
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnDownloadCertificate_Click(object sender, EventArgs e)
    {
        Session["CertificateDownloadRegID"] = BusinessUtility.GetInt(hdnCertificateDownloadRegID.Value);
        Response.Redirect("EmployeeCourses.aspx?CourseStatus=" + (int)CourseStatus.Traning_Employee_have_taken);
    }

    /// <summary>
    /// To Create Course Certificatae and Download
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnDownloadCertificateFile_Click(object sender, EventArgs e)
    {
        if (BusinessUtility.GetInt(Session["CertificateDownloadRegID"]) > 0)
        {
            int iCertificateRegID = BusinessUtility.GetInt(Session["CertificateDownloadRegID"]);
            Session["CertificateDownloadRegID"] = null;
            CertificateCreate objCertificate = new CertificateCreate();

            Certificate objCert = new Certificate();
            string sSaveFileName = objCert.GetCertificateFilePath(iCertificateRegID);

            if (!string.IsNullOrEmpty(sSaveFileName))
            {
                string sFolderPath = this.CertificatePath;
                objCertificate.DownloadCertificate(sFolderPath + sSaveFileName, sSaveFileName.Replace("/", ""));
            }
            else
            {
                objCertificate.DownloadCertificate(iCertificateRegID, true);
            }
        }
    }

    /// <summary>
    /// To Save Test Attempt Request Alert
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnAttemptRequestAlert_Click(object sender, EventArgs e)
    {
        TextBox txtAttemptsEmail = (TextBox)Master.FindControl("txtAttemptsEmail");
        EmployeeCourseRegistration objEmpReg = new EmployeeCourseRegistration();

        int testAttemptsEliggableMins = Utils.TestAttemptsEligableMins;
        if (BusinessUtility.GetInt(hdnAttemptCourseRegID.Value) > 0)
        {
            objEmpReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(hdnAttemptCourseRegID.Value));
            if (BusinessUtility.GetInt(objEmpReg.attemptGrantWaitingTime) > 0)
            {
                testAttemptsEliggableMins = BusinessUtility.GetInt(objEmpReg.attemptGrantWaitingTime);
            }
        }

        //if (objEmpReg.SaveTestAttemptRequest(BusinessUtility.GetInt(hdnAttemptRegID.Value), Utils.TestAttemptsNo, txtAttemptsEmail.Text, Utils.TestAttemptsEligableMins))
        if (objEmpReg.SaveTestAttemptRequest(BusinessUtility.GetInt(hdnAttemptRegID.Value), Utils.TestAttemptsNo, txtAttemptsEmail.Text, testAttemptsEliggableMins))
        {
            if (txtAttemptsEmail.Text != "")
            {
                EmployeeCourseRegistration objempCourseReg = new EmployeeCourseRegistration();
                objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(hdnAttemptRegID.Value));

                Event objEvent = new Event();
                if (BusinessUtility.GetInt(hdnAttemptCourseRegID.Value) > 0)
                {

                    objempCourseReg = new EmployeeCourseRegistration();
                    objempCourseReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(hdnAttemptCourseRegID.Value));
                    objEvent.GetEventDetail(objempCourseReg.eventID, Globals.CurrentAppLanguageCode);
                }
                string emailFrom = TestAttemptEmailFrom;
                string emailSubject = Resources.Resource.lblAttemptsRequestMailSub;
                string emailTo = txtAttemptsEmail.Text;
                string sMessage = BusinessUtility.GetString(Resources.Resource.lblAttemptsRequestMailContent).Replace("#COURSENAME#", objEvent.EventName);

                Employee objEmp = new Employee();
                objEmp.GetEmployeeDetail(BusinessUtility.GetInt(objempCourseReg.empID));

                string mailTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/TestAttemptsRequestMail.html"));
                mailTemplet = mailTemplet.Replace("#IMGURL#", Utils.GetSiteName())
                      .Replace("#EMPFIRSTNAME#", objEmp.EmpFirstName)
                      .Replace("#MAILCONTENT#", sMessage);

                objEmpReg.SaveTestAttemptAlert(emailFrom, emailTo, SMTPServer, SMTPPort, SMTPUserEmail, SMTPPassword, BusinessUtility.GetString(UseDefaultCredentials), "HTML", objEmpReg.testAttempEligiableDateTime, 0, CurrentEmployee.EmpID, EnableSsl, emailSubject, mailTemplet);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowAlertThanksDialog", "ShowAlertThanksDialog(" + BusinessUtility.GetInt(hdnAttemptRegID.Value) + "," + BusinessUtility.GetInt(hdnAttemptCourseRegID.Value) + ", " + objEmpReg.testAttemptRequestID + "," + objEmpReg.messageAlertID + " );", true);
        }
    }

    /// <summary>
    /// To Save Attempt Test Request
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnAttemptRequest_Click(object sender, EventArgs e)
    {
        EmployeeCourseRegistration objEmpReg = new EmployeeCourseRegistration();

        int testAttemptsEliggableMins = Utils.TestAttemptsEligableMins;
        if (BusinessUtility.GetInt(hdnAttemptCourseRegID.Value) > 0)
        {
            objEmpReg.GetEmployeeRegistraionDetail(BusinessUtility.GetInt(hdnAttemptCourseRegID.Value));
            if (BusinessUtility.GetInt(objEmpReg.attemptGrantWaitingTime) > 0)
            {
                testAttemptsEliggableMins = BusinessUtility.GetInt(objEmpReg.attemptGrantWaitingTime);
            }
        }


        //if (objEmpReg.SaveTestAttemptRequest(BusinessUtility.GetInt(hdnAttemptRegID.Value), Utils.TestAttemptsNo, "", Utils.TestAttemptsEligableMins))
        if (objEmpReg.SaveTestAttemptRequest(BusinessUtility.GetInt(hdnAttemptRegID.Value), Utils.TestAttemptsNo, "", testAttemptsEliggableMins))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AttemptRequestDialog", "ShowNoThanksAlert();", true);
        }
    }

    /// <summary>
    /// To Change Test Attempt Alert Mail ID
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnChangeAlertMailID_Click(object sender, EventArgs e)
    {
        Int64 attemptRequestID = BusinessUtility.GetInt(hdnAttemptRequestID.Value);
        Int64 messageAlertID = BusinessUtility.GetInt(hdnMessageAlertID.Value);
        TextBox txtAttemptsEmail = (TextBox)Master.FindControl("txtAttemptsEmail");
        EmployeeCourseRegistration objEmpReg = new EmployeeCourseRegistration();

        if (objEmpReg.UpdateTestAttemptAlertMailID(txtAttemptsEmail.Text, attemptRequestID, messageAlertID) == true)
        {
            GlobalMessage.showAlertMessage(Resources.Resource.lblAlertMailIDChanged, "EmployeeCourses.aspx?CourseStatus=1");
        }
    }


    protected void btnLogOut_OnClick(object sender, EventArgs e)
    {
        Session["Logout"] = null;
        EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.SignedOutofPIB,
            BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), "", 0, (int)RusticRegStatusCode.None, "");

        CurrentEmployee.RemoveEmployeeFromSession();
        CurrentEmployee.RemoveSuperAdminFromSession();
        Response.Redirect("~/Public.aspx");
    }
    public void showFeedbackCompletedConfirmationMessage(string title,string empName,string message)
    {
        
        Page page = HttpContext.Current.Handler as Page;
        ClientScriptManager cs = page.ClientScript;
        
        ScriptManager.RegisterStartupScript(this, this.GetType(), "showFeedbackCompletedConfirmationMessage", "javascript:  showFeedbackCompletedConfirmationMessage('"+ title + "','" + empName + "','" + message + "');", true);
    }

    /// <summary>
    /// To Show Alert Message
    /// </summary>
    private void showAlertMessage()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowPensivoMessage('" + Resources.Resource.lblNoFeedbackExists + "','" + "FeedbackHome.aspx" + "');", true);
    }



    /// <summary>
    /// To Get Certificate Path
    /// </summary>
    public string CertificatePath
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("CertificateDiractory"));
        }
    }


    public int TraningYear
    {
        get
        {
            int cStatus = 0;
            int.TryParse(Request.QueryString["TYear"], out cStatus);
            return cStatus;
        }
    }

    #region MailSettings

    /// <summary>
    /// To Get SMTP Mail Server
    /// </summary>
    public static string SMTPServer
    {
        get
        {
            return BusinessUtility.GetString(AppConfig.GetAppConfigValue("SMTPServer"));
        }
    }

    /// <summary>
    /// To Get SMTP Port
    /// </summary>
    public static int SMTPPort
    {
        get
        {
            if (!string.IsNullOrEmpty(AppConfig.GetAppConfigValue("SMTPPort")))
            {
                int id = 25;
                if (int.TryParse(AppConfig.GetAppConfigValue("SMTPPort"), out id))
                {
                    return id;
                }
                return 25;
            }
            return 25;
        }
    }

    /// <summary>
    /// To Get SMTP User Email
    /// </summary>
    public static string SMTPUserEmail
    {
        get
        {
            if (!string.IsNullOrEmpty(AppConfig.GetAppConfigValue("SMTPUserEmail")))
            {
                return AppConfig.GetAppConfigValue("SMTPUserEmail");
            }
            return string.Empty;
        }
    }

    /// <summary>
    /// To Get SMPT Password
    /// </summary>
    public static string SMTPPassword
    {
        get
        {
            if (!string.IsNullOrEmpty(AppConfig.GetAppConfigValue("SMTPPassword")))
            {
                return AppConfig.GetAppConfigValue("SMTPPassword");
            }
            return string.Empty;
        }
    }

    /// <summary>
    /// To Get Is Asynchronous Mail
    /// </summary>
    public static bool IsAsynchronousMail
    {
        get
        {
            if (!string.IsNullOrEmpty(AppConfig.GetAppConfigValue("IsAsynchronousMail")))
            {
                return AppConfig.GetAppConfigValue("IsAsynchronousMail").ToLower() == "yes";
            }
            return false;
        }
    }

    /// <summary>
    /// To Get Use Default Credentials
    /// </summary>
    public static int UseDefaultCredentials
    {
        get
        {
            if (!string.IsNullOrEmpty(SMTPUserEmail) && !string.IsNullOrEmpty(SMTPPassword))
            {
                return 1;
            }
            return 0;
        }
    }

    /// <summary>
    /// To Get Is Enable SSL
    /// </summary>
    public static int EnableSsl
    {
        get
        {
            if (!string.IsNullOrEmpty(AppConfig.GetAppConfigValue("SMTPEnableSsl")))
            {
                if (AppConfig.GetAppConfigValue("SMTPEnableSsl").ToLower() == "true")
                {
                    return 1;
                }
            }
            return 0;
        }
    }

    /// <summary>
    /// To Get Test Attempt Email From
    /// </summary>
    public static string TestAttemptEmailFrom
    {
        get
        {
            if (!string.IsNullOrEmpty(AppConfig.GetAppConfigValue("TestAttemptEmailFrom")))
            {
                return AppConfig.GetAppConfigValue("TestAttemptEmailFrom");
            }
            return string.Empty;
        }
    }

    #endregion


}