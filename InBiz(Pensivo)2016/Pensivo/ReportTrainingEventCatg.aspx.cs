﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Text;
using System.Data;
using System.IO;


public partial class ReportTrainingEventCatg : BasePage
{
    /// <summary>
    /// To Store HTML Text
    /// </summary>
    protected string htmlText = "";

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ReportOption != "")
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
        }

        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblTrainingByCategory, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            htmlText = loadEmpSearchValue();
        }
    }

    /// <summary>
    /// To Get Training Event Type                 
    /// </summary>
    /// <returns>String</returns>
    private string loadEmpSearchValue()
    {
        string sbHtml = "";
        Event objRole = new Event();
        DataTable dt = new DataTable();
        dt = objRole.GetCourseTypeList(Globals.CurrentAppLanguageCode);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sbHtml += " <div class='boxset' id='dvAdminDashBoard"+ BusinessUtility.GetString(i) +"'> ";
            for (int j = 0; j <= 2; j++)
            {
                if (i < dt.Rows.Count)
                {
                    string sText = BusinessUtility.GetString(dt.Rows[i]["TypeText"]);
                    string sValue = BusinessUtility.GetString(dt.Rows[i]["TypeValue"]);
                    if (sText != "")
                    {
                        string sUrl = "";
                        sUrl = string.Format("ReportFilterSummary.aspx?roption={0}&searchby={1}&searchval={2}&isAllEvent={3}", this.ReportOption, this.SearchBy, sValue, this.IsAllEvent);
                        sbHtml += " <div class='boxset-box'> ";
                        sbHtml += " <a id='hrfSearchEmpName' runat='server' href='" + sUrl + "' class='boxset-box-wrapper'> ";
                        sbHtml += " <div class='boxset-box-inner'> ";
                        sbHtml += " <h4 class='boxset-title'> " + sText + " </h4> ";
                        sbHtml += " </div> ";
                        sbHtml += " </a> ";
                        sbHtml += " </div> ";
                        if (j <= 1)
                        {
                            i += 1;
                        }
                    }
                }
            }
            sbHtml += " </div> ";
        }
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// To Get Report Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Report Search Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Report Option
    /// </summary>
    public string ReportOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }

    /// <summary>
    /// To Get Is ALL Event
    /// </summary>
    public int IsAllEvent
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["isAllEvent"]);
        }
    }
}