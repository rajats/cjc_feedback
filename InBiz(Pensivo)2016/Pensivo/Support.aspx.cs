﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Library.Utilities;
using System.IO;
using System.Text;
using iTECH.Pensivo.BusinessLogic;

public partial class Support : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
        currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblSupport, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
    }

    /// <summary>
    /// To Define Request Send to Support
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnsend_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            StringBuilder sbMessage = new StringBuilder();
            Employee objEmp = new Employee();
            objEmp.GetEmployeeDetail(BusinessUtility.GetInt(CurrentEmployee.EmpID));

            if (BusinessUtility.GetInt(objEmp.EmpID) > 0)
            {
                sbMessage.Append(Resources.Resource.lblEmpFirstName + ": " + BusinessUtility.GetString(objEmp.EmpFirstName) + "<br />");
                sbMessage.Append(Resources.Resource.lblEmpLastName + ": " + BusinessUtility.GetString(objEmp.EmpLastName) + "<br />");
                sbMessage.Append(Resources.Resource.lblSpuportFormEmployeeNo + ": " + BusinessUtility.GetString(objEmp.EmpExtID) + "<br />");
                sbMessage.Append(Resources.Resource.lblSupportEmpHdrID + ": " + BusinessUtility.GetString(objEmp.EmpID) + "<br />");
                sbMessage.Append(Resources.Resource.lblSupportEmpUUID + ": " + BusinessUtility.GetString(objEmp.PensivoUUID) + "<br /><br />");
            }

            string clientIp = (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();
            HttpBrowserCapabilities browse = Request.Browser;
            string sUserAgents = Request.UserAgent;
            string platform = browse.Platform;

            string sBrowserDetail = browse.Browser + " ver " + browse.Version;
            sbMessage.Append(Resources.Resource.lblSupportBrowserDetail + ": " + BusinessUtility.GetString(sBrowserDetail) + "<br />");
            sbMessage.Append(Resources.Resource.lblSupportFormIPAddress + ": " + BusinessUtility.GetString(clientIp) + "<br />");
            sbMessage.Append(Resources.Resource.lblUserAgent + ": " + BusinessUtility.GetString(sUserAgents) + "<br /><br />");
            sbMessage.Append(Resources.Resource.lblEmailID + ": " + txtemail.Value + "<br />");
            sbMessage.Append(Resources.Resource.lblQuestion + ": " + ddlQuestion.SelectedItem.Text + "<br />");
            sbMessage.Append(Resources.Resource.lblContent + ": " + txtArea.InnerHtml + "<br />");

            string sDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string EmailFrom = Utils.SupportEmailFrom;
            string EmailSubject = Convert.ToString(txtemail.Value)  + " on " + sDateTime + "  " + ddlQuestion.SelectedValue;
            string[] EmailTo = Utils.SupportEmailTo.Split(',');
            foreach (string sMailTo in EmailTo)
            {
                EmailHelper.SendEmail(EmailFrom, sMailTo, BusinessUtility.GetString(sbMessage), EmailSubject, true);
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowPensivoMessage('" + Resources.Resource.lblSupportMessageSent + "', '" + "Faq.aspx" + "');", true);
        }
    }
}