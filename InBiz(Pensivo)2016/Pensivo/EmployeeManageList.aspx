﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeeManageList.aspx.cs" Inherits="EmployeeManageList" %>

<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.EDE2)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>
    <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>

    <%--Define Inline Css to Wrap Grid Header and Row Data--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th div {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
        }

        .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
            vertical-align: text-top;
        }
    </style>
    <section id="main-content" class="pg-advanced-list">
        <div class="wrapper">
            <h1><%=Resources.Resource.lblSelectTheListYouWantToManage %></h1>
            <aside class="column span-4 fixed-onscroll">
                <div class="search-box" style="width: 384px; left: 216px;">
                    <header class="search-box-header">
                        <h2 class="h6"><%=Resources.Resource.lblFindList %></h2>
                        <p class="last-child"><%=Resources.Resource.lblFindTheListYouWantToManage %></p>
                    </header>
                    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
                        <div class="search-box-body">
                            <div class="plms-input-group">
                                <div class="plms-fieldset">
                                    <asp:Label ID="Label2" AssociatedControlID="txtListName" class="filter-key plms-label is-hidden no-height" runat="server"
                                        Text="<%$ Resources:Resource, lblListName %>" for="name"></asp:Label>
                                    <asp:TextBox ID="txtListName" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblListName %>" MaxLength="135"> 
                                    </asp:TextBox>
                                </div>
                            </div>
                            <input id="btnSearch" class="btn fluid clearfix" type="button" style="width: 100%;" value="<%=Resources.Resource.lbSearch%>" />
                        </div>
                    </asp:Panel>
                </div>
            </aside>
            <div class="main-content-body column span-8" onkeypress="return disableEnterKey(event)">
                <div id="grid_wrapper" style="width: 100%;">
                    <trirand:JQGrid runat="server" ID="gvManageList" Height="300px"
                        AutoWidth="True" OnCellBinding="gvManageList_CellBinding" OnDataRequesting="gvManageList_DataRequesting">
                        <Columns>
                            <trirand:JQGridColumn DataField="mstListTblID" Visible="false" PrimaryKey="True" />
                            <trirand:JQGridColumn DataField="listname" HeaderText="<%$ Resources:Resource, lblListName %>"
                                Editable="false" Width="150" />
                            <trirand:JQGridColumn DataField="mstListTblID" HeaderText="<%$ Resources:Resource, BtnManageListSelect %>" TextAlign="Center" Width="20"
                                Editable="false" />
                        </Columns>
                        <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn=""></SortSettings>
                        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                        <ClientSideEvents LoadComplete="loadComplete" />
                    </trirand:JQGrid>
                </div>

            </div>
        </div>
        <asp:HiddenField ID="HdnRoleID" runat="server" />
        <asp:HiddenField ID="HdnSearchBy" runat="server" />
        <asp:HiddenField ID="HdnFlag" runat="server" />
        <asp:HiddenField ID="HdnActionID" runat="server" />
        <asp:HiddenField ID="HdnFunctionalityType" runat="server" />
        <asp:HiddenField ID="HdnCreatedBy" runat="server" />
        <asp:HiddenField ID="HdnMessage" runat="server" />
    </section>
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvManageList.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        // Resized JQ Grid
        function jqGridResize() {
            $("#<%=gvManageList.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // Define JQ Grid Load Complete
        function loadComplete(data) {
            $("#cb_ContentPlaceHolder1_gvManageList").hide();
            jqGridResize();
        }


        // To Go Employee Site Manager List 
        function SelectManageList(manageListID, manageListName, searchBy, functionalityID, ListID) {
            if (manageListID > 0) {
                if (ListID == 3) {
                    window.location.href = "EmployeeCrossDockList.aspx?roleManageListID=" + manageListID + "&roleManageListName=" + manageListName + "&SearchBy=" + searchBy + "&FunctionalityID=" + functionalityID + "&ListID=" + ListID;
                }
                else if (ListID == 4) {
                    window.location.href = "EmployeeSiteReportingDistrict.aspx?roleManageListID=" + manageListID + "&roleManageListName=" + manageListName + "&SearchBy=" + searchBy + "&FunctionalityID=" + functionalityID + "&ListID=" + ListID;
                }
                else if (ListID == 5) {
                    window.location.href = "EmployeeSiteReportingRegionalDirector.aspx?roleManageListID=" + manageListID + "&roleManageListName=" + manageListName + "&SearchBy=" + searchBy + "&FunctionalityID=" + functionalityID + "&ListID=" + ListID;
                }
                else {
                    window.location.href = "EmployeeSiteManager.aspx?roleManageListID=" + manageListID + "&roleManageListName=" + manageListName + "&SearchBy=" + searchBy + "&FunctionalityID=" + functionalityID + "&ListID=" + ListID;
                }
            }
        }

        $(document).ready(function () {
            $("#<%=txtListName.ClientID%>").focus();
        });

    </script>
</asp:Content>
