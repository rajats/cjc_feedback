﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using System.IO;
using System.Text;


public partial class RoleDetails : BasePage
{
    /// <summary>
    /// To Report Summar Detail HTML
    /// </summary>
    protected string sHTML = "";

    /// <summary>
    /// Set Report Filter Summary Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
            if (!IsPostBack)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblRoleDetail, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                ReportFilter objReportFilter = new ReportFilter();
                sHTML = BusinessUtility.GetString(GetReportDetails());
                hrefYes.HRef = "Report.aspx" + Request.Url.Query;
            }
        }

        hrefYes.HRef = "ViewSystemRoleDetails.aspx";
    }

    /// <summary>
    /// To Get Role Details
    /// </summary>
    /// <param name="reportFilterList">Pass Report Filter List</param>
    /// <returns>String</returns>
    private string GetReportDetails()
    {
        StringBuilder sbHtml = new StringBuilder();
        Event objEvent = new Event();
        Employee objEmp = new Employee();
        


        sbHtml = new StringBuilder();
        sbHtml.Append("<style>.reportTable{width:100%;}  .reportCol1{width:200px;height:auto;word-break:break-word}  .reportCol2{width:100px;height:auto;word-break:break-word;vertical-align:top;padding-left:0px} .reportCol3{width:200px;height:auto;word-break:break-all;max-width: 200px;}  .reportTable tr:nth-child(2n+1) td{background: none repeat scroll 0 0 transparent;} .reportTable tr{ border-bottom: 0px solid rgba(0, 0, 0, 0.1);}    .reportTableDesc tr:nth-child(2n+1) td{background: none repeat scroll 0 0 transparent;padding-left: 0px;} .reportTableDesc tr{ border-bottom: 0px solid rgba(0, 0, 0, 0.1);} .titlelable{font-weight:bold;font-size:18px} .subtitlelable{font-weight:normal;font-size:16px;font-style:italic;padding-left: 15px;}  </style>");
        sbHtml.Append("<table class='reportTable' style='border:1'>");
        int rowIndex = 1;

        string hypLink = "";

        sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblSystemRoleName + ":"));
        //sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", objRole.GetRoleName(this.RoleID)));

        //hypLink = string.Format(@"<a class='btn' style='float:none;font-size:9px;' href=""RoleUserList.aspx?roleID={0}""  >" + Resources.Resource.BtnRoleDetailUsersList + "</a>", this.RoleID);  //onclick=""RoleDetails('{0}')""
        hypLink = string.Format(@"<a class='btn' style='float:none;font-size:9px;' href=""RoleRefDetail.aspx?roleID={0}""  >" + Resources.Resource.BtnRoleDetailUsersList + "</a>", this.RoleID);  //onclick=""RoleDetails('{0}')""
        sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblRoleDetailUsers + ":"));
        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", hypLink));

        hypLink = string.Format(@"<a class='btn' style='float:none;font-size:9px;' href=""RoleTrainingEventList.aspx?roleID={0}""  >" + Resources.Resource.BtnRoleDetailTraningEventList + "</a>", this.RoleID); //onclick=""RoleDetails('{0}')""
        sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblRoleDetailTraningEvents + ":"));
        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", hypLink));

        hypLink = string.Format(@"<a class='btn' style='float:none;font-size:9px;'  href=""RoleFunctionalityList.aspx?roleID={0}""  >" + Resources.Resource.BtnRoleDetailFunctionalityList + "</a>", this.RoleID); //onclick=""RoleDetails('{0}')""
        sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resources.Resource.lblRoleDetailFunctionality + ":"));
        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", hypLink));

        sbHtml.Append("</table>");
        return BusinessUtility.GetString(sbHtml);
    }

    /// <summary>
    /// Create Report Filter Sub Header Row in Report Filter Detail
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <param name="label">Pass Label</param>
    /// <returns>String</returns>
    private string CreateFilterSubHeader(ref int rowIndex, string label)
    {
        string rValue = "";
        if (rowIndex == 1)
        {
            Role objRole = new Role();
            rValue = (" <tr><td class='reportCol1'>" + Resources.Resource.lblSystemRoleDetailMessage + "</td><td class='reportCol2 titlelable'>" + label + "</td><td class='reportCol3 titlelable' style='padding-top:0px;word-break:break-word'>" + objRole.GetRoleName(this.RoleID) + "</td></tr>");
        }
        else
        {
            rValue = (" <tr><td class='reportCol1'>" + "" + "</td><td class='reportCol2 titlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + "" + "</td></tr>");
        }
        rowIndex += 1;
        return rValue;
    }

    /// <summary>
    /// Create Report Filter Data Row in Report Filter Detail
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <param name="label">Pass Label</param>
    /// <param name="labelValue">Pass Label Value</param>
    /// <returns>String</returns>
    private string CreateFilterDataRow(ref int rowIndex, string label, string labelValue)
    {
        rowIndex += 1;
        return (" <tr><td class='reportCol1'>" + "" + "</td><td class='reportCol2 subtitlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + labelValue + "</td></tr>");
    }

    /// <summary>
    /// Create Report Filter Blank Row
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <returns>String</returns>
    private string CreateFilterBlankRow(ref int rowIndex)
    {
        rowIndex += 1;
        return (" <tr><td class='reportCol1'>" + "" + "</td><td class='reportCol2 titlelable'> &nbsp; </td><td class='reportCol3' style='padding-top:0px'>&nbsp;</td></tr>");
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }
}