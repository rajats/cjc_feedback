﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Globalization;


public partial class RoleEventDefault : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        showMessge("");
        if (!IsPostBack)
        {
            GetTrainingEventDetail();
            if (this.EvenID > 0)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAddEvent, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
                ltrTitle.Text = Resources.Resource.lblAddEvent;
                btnEventConfirmationMessage.Text = Resources.Resource.lblAddEvent;
            }
            else
            {
                System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAddNewTrainingEvent, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

                ltrTitle.Text = Resources.Resource.lblCreateEvent;
                btnEventConfirmationMessage.Text = Resources.Resource.lblCreateEvent;
            }
        }




    }


    protected void btnEventConfirmationMessage_OnClick(object sender, EventArgs e)
    {
        //if (!Page.IsValid)
        {
            Event objEvent = new Event();
            objEvent = new Event();
            objEvent.GetEventDetail(this.EvenID, Globals.CurrentAppLanguageCode);

            Role objRole = new Role();
            string sRoleName = objRole.GetRoleName(this.RoleID);
            string str = string.Empty;
            str += Resources.Resource.lblRoleAddEvent.Replace("#ROLENAME#", sRoleName).Replace("#EVENTNAME#", BusinessUtility.GetString(objEvent.EventName));
            string sMessage = str.Replace("\"", "{-").Replace("'", "{_");
            //btnSubmit.Attributes.Add("onclick", string.Format(@"AddEvent({0},{1},'{2}')", this.EvenID, this.RoleID, sMessage));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowConfirmationMessage", string.Format("AddEvent({0},{1},'{2}');", this.EvenID, this.RoleID, sMessage), true);
        }
    }


    /// <summary>
    /// To Define Create Training Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnCreateEvent_OnClick(object sender, EventArgs e)
    {
        //if (Page.IsValid)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "showWaitMessage", "ShowPensivoWaitingMessage('" + Resources.Resource.lblRoleModifyPleaseWait + "');", true);
            try
            {
                //Event objEvent = new Event();
                //objEvent.CourseVerNo = BusinessUtility.GetInt(ddlCourseVersion.SelectedItem.Value);

                RoleTrainingEventDefault objEventDefaults = new RoleTrainingEventDefault();

                //objEventDefaults.TrainingEventVer = BusinessUtility.GetInt(ddlCourseVersion.SelectedItem.Value);
                objEventDefaults.ReportingKeywords = txtReportingKeyWords.Text;
                objEventDefaults.MinimumPassScore = BusinessUtility.GetInt(txtMinimumPassScore.Text);
                objEventDefaults.MaximumPassScore = BusinessUtility.GetInt(txtMaximumPassScore.Text);
                objEventDefaults.AttemptResetType = ddlAttemptResetType.SelectedItem.Value;
                objEventDefaults.AttemptGrantWaitingTime = BusinessUtility.GetInt(txtAttemptGrantWaitingTime.Text);
                objEventDefaults.OrderSeqInDisplay = BusinessUtility.GetInt(txtOrderSeqInDisplay.Text);
                objEventDefaults.UserDisplayPriority = BusinessUtility.GetInt(txtUserDisplayPriority.Text);
                objEventDefaults.RepeatRequired = rdRepeatRequiredYes.Checked == true ? true : false;
                objEventDefaults.CourseStartDateTime = BusinessUtility.GetDateTime(txtCourseStartDate.Text, DateFormat.ddMMyyyy);

                if (rdRepeatRequiredYes.Checked == true)
                {
                    objEventDefaults.RepeatTriggerType = ddlRepeatTriggerType.SelectedItem.Value;
                    if (objEventDefaults.RepeatTriggerType == TrainingRepeatTriggerType.FixedDate)
                    {
                        objEventDefaults.FixedDay = BusinessUtility.GetInt(ddlFixedDay.SelectedItem.Value);
                        objEventDefaults.FixedMonth = BusinessUtility.GetInt(ddlFixedMonth.SelectedItem.Value);
                    }
                    else
                    {
                        objEventDefaults.RepeatInDays = BusinessUtility.GetInt(txtRepeatInDays.Text);
                    }
                }
                else
                {
                    objEventDefaults.RepeatTriggerType = ddlRepeatTriggerType.SelectedItem.Value;
                }

                objEventDefaults.AlertRequired = rdAlertRequiredYes.Checked == true ? true : false;
                if (rdAlertRequiredYes.Checked == true)
                {
                    objEventDefaults.AlertBeforeLaunch = rdBeforeLaunchYes.Checked == true ? true : false;
                    objEventDefaults.AlertNotCompleted = rdNotCompletedYes.Checked == true ? true : false;
                    objEventDefaults.AlertManagerNotCompleted = rdManagerNotCompletedYes.Checked == true ? true : false;
                    objEventDefaults.AlertRepeatInDays = BusinessUtility.GetInt(txtAlertInDaysRepeat.Text);

                    objEventDefaults.AlertInDays = BusinessUtility.GetInt(txtAlertInDays.Text);
                    objEventDefaults.AlertFormat = ddlAlertFormat.SelectedItem.Value;
                    objEventDefaults.AlertPortalMsg = txtAlertPortalMsg.Text;
                    objEventDefaults.AlertEmailMsg = txtAlertEmailMsg.Text;
                    objEventDefaults.AlertEscalationEmailMsg = txtAlertEscalationEmailMsg.Text;
                    objEventDefaults.AlertEmailSubject = txtAlertEmailSubject.Text;
                    objEventDefaults.AlertEscalationEmailSubject = txtAlertEmailSubject.Text;
                }
                objEventDefaults.AlertType = "U";

                objEventDefaults.TrainingEventID = BusinessUtility.GetInt(this.EvenID);
                objEventDefaults.EmpRoleID = BusinessUtility.GetInt(this.RoleID);


                List<Role> lRoleEvent = new List<Role>();
                lRoleEvent.Add(new Role { EventID = this.EvenID });
                Role objRole = new Role();
                //if (objRole.RoleAddEvent(roleID, lRoleEvent) == true)
                //{
                //    ExecutingThread.ThreadEmployeeAssignedCoursesByRole(roleID);
                //    Response.Write("ok");
                //}

                if ((objEventDefaults.SaveUpdateRoleTrainingEventDefault() == true) && (objRole.RoleAddEvent(this.RoleID, lRoleEvent) == true))
                {
                    //objEvent = new Event();
                    //objEvent.GetEventDetail(this.EvenID, Globals.CurrentAppLanguageCode);
                    //if (objEventDefaults.TrainingEventVer > objEvent.CourseVerNo)
                    //{
                    //    objEvent.UpdatEventVersion(this.EvenID, objEventDefaults.TrainingEventVer);
                    //}

                    //ExecutingThread.ThreadEmployeeAssignedCoursesByRole(this.RoleID);

                    EmpAssignedCourses objEmpAssignedCourses = new EmpAssignedCourses();
                    //objEmpAssignedCourses.RoleID = this.RoleID;
                    //objEmpAssignedCourses.ResetEmployeeAssignedCoursesByRole(BusinessUtility.GetInt(this.EvenID));

                    ExecutingThread.ThreadEmployeeAssignedCoursesByRole(this.RoleID, BusinessUtility.GetInt(this.EvenID), (int)AssignedCourseInActiveReason.BySysAdminRole, (int)AssignedCourseUpdatedSource.BySysAdminRoleUpdate);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideWaitMessage", "ClosePensivoWaitingMessage();", true);
                    Response.Redirect("TraningEventRoleDashBoard.aspx?roleID= " + this.RoleID + "", false);
                }
                else
                {
                    showMessge("Unable to Update");
                }
            }
            catch (Exception err)
            {
                ErrorLog.createLog(err.ToString());
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideWaitMessage", "ClosePensivoWaitingMessage();", true);
        }
    }

    /// <summary>
    /// To Get Training Event Detail
    /// </summary>
    private void GetTrainingEventDetail()
    {
        Event objEvent = new Event();
        EventVersion objEventVersion = new EventVersion();
        BindCourseVersion();

        if (this.EvenID > 0)
        {
            //dvCreateEventVersion.Visible = true;

            //objEvent = new Event();
            //objEvent.GetEventDetail(this.EvenID, Globals.CurrentAppLanguageCode);
            //ddlCourseVersion.SelectedValue = BusinessUtility.GetString(objEvent.CourseVerNo);
            
            EventDefaults objEventDefaults = new EventDefaults();
            objEventDefaults.GetTrainingEventDefault(this.EvenID, Globals.CurrentAppLanguageCode);

            txtReportingKeyWords.Text = objEventDefaults.ReportingKeywords;
            txtMinimumPassScore.Text = BusinessUtility.GetString(objEventDefaults.MinimumPassScore);
            txtMaximumPassScore.Text = BusinessUtility.GetString(objEventDefaults.MaximumPassScore);
            ddlAttemptResetType.SelectedValue = BusinessUtility.GetString(objEventDefaults.AttemptResetType);
            txtAttemptGrantWaitingTime.Text = BusinessUtility.GetString(objEventDefaults.AttemptGrantWaitingTime);
            txtOrderSeqInDisplay.Text = BusinessUtility.GetString(objEventDefaults.OrderSeqInDisplay);
            txtUserDisplayPriority.Text = BusinessUtility.GetString(objEventDefaults.UserDisplayPriority);

            //if (objEventDefaults.CourseStartDateTime.ToString("dd/MM/yyyy") != "01/01/0001")
            //{
            //    txtCourseStartDate.Text = objEventDefaults.CourseStartDateTime.ToString("dd/MM/yyyy");
            //}
            txtCourseStartDate.Text = Convert.ToDateTime(objEventDefaults.CourseStartDateTime).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            if (txtCourseStartDate.Text == "01/01/0001")
            {
                txtCourseStartDate.Text = "";
            }


            if (objEventDefaults.RepeatRequired)
            {
                hdnShowRepeat.Value = "1";
                rdRepeatRequiredYes.Checked = true;

                ddlRepeatTriggerType.SelectedValue = objEventDefaults.RepeatTriggerType;

                if (objEventDefaults.RepeatTriggerType == TrainingRepeatTriggerType.FixedDate)
                {
                    ddlFixedDay.SelectedValue = BusinessUtility.GetString(objEventDefaults.FixedDay);
                    ddlFixedMonth.SelectedValue = BusinessUtility.GetString(objEventDefaults.FixedMonth);
                }
                else
                {
                    txtRepeatInDays.Text = BusinessUtility.GetString(objEventDefaults.RepeatInDays);
                }
            }
            else
            {
                rdRepeatRequiredNo.Checked = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "repeatRequired", "HideRepeatDetail();", true);
            }


            if (objEventDefaults.AlertRequired)
            {
                hdnShowAlert.Value = "1";
                rdAlertRequiredYes.Checked = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertRequired", "ShowAlertDetail();", true);

                if (objEventDefaults.AlertBeforeLaunch == true)
                {
                    rdBeforeLaunchYes.Checked = true;
                }
                else
                {
                    rdBeforeLaunchNo.Checked = true;
                }


                if (objEventDefaults.AlertNotCompleted == true)
                {
                    rdNotCompletedYes.Checked = true;
                }
                else
                {
                    rdNotCompletedNo.Checked = true;
                }

                if (objEventDefaults.AlertManagerNotCompleted == true)
                {
                    rdManagerNotCompletedYes.Checked = true;
                }
                else
                {
                    rdManagerNotCompletedNo.Checked = true;
                }

                txtAlertInDaysRepeat.Text = BusinessUtility.GetString(objEventDefaults.AlertRepeatInDays);
                txtAlertInDays.Text = BusinessUtility.GetString(objEventDefaults.AlertInDays);
                ddlAlertFormat.SelectedValue = BusinessUtility.GetString(objEventDefaults.AlertFormat);
                txtAlertPortalMsg.Text = BusinessUtility.GetString(objEventDefaults.AlertPortalMsg);
                txtAlertEmailMsg.Text = BusinessUtility.GetString(objEventDefaults.AlertEmailMsg);
                txtAlertEscalationEmailMsg.Text = BusinessUtility.GetString(objEventDefaults.AlertEscalationEmailMsg);
                txtAlertEmailSubject.Text = BusinessUtility.GetString(objEventDefaults.AlertEmailSubject);
                txtAlertEscalationEmailSubject.Text = BusinessUtility.GetString(objEventDefaults.AlertEscalationEmailSubject);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertRequired", "HideAlertDetail();", true);
                rdAlertRequiredNo.Checked = true;
            }
        }
        else
        {
            //txtCourseVerNo.Text = BusinessUtility.GetString(objEventVersion.GetCourseCurrentVersion(this.EvenID));
            //ddlCourseVersion.SelectedValue = BusinessUtility.GetString("0");
        }
    }

    /// <summary>
    /// To Get Training Event ID
    /// </summary>
    public int EvenID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["eventID"]);
        }
    }

    /// <summary>
    /// To Show Message
    /// </summary>
    /// <param name="message">Pass Message String</param>
    public void showMessge(string message)
    {
        if (message != "")
        {
            ltrErrMsg.Text = message;
            ltrErrMsg.Visible = true;
        }
        else
        {
            ltrErrMsg.Visible = false;
        }
    }

    /// <summary>
    /// To Create Training Event Version
    /// </summary>
    /// <param name="sender">Pass Sender Object </param>
    /// <param name="e">Pass Event Args</param>
    protected void btnCreateVersion_Click(object sender, EventArgs e)
    {
        ////if (!IsPostBack)
        //{
        //    EventVersion objEventVersion = new EventVersion();
        //    objEventVersion.CreateEventVersion(this.EvenID, BusinessUtility.GetInt(CurrentEmployee.EmpID));
        //    BindCourseVersion();
        //    //ddlCourseVersion.SelectedValue = BusinessUtility.GetString(objEventVersion.GetCourseCurrentVersion(this.EvenID));
        //    txtReportingKeyWords.Focus();
        //}
    }

    /// <summary>
    /// To Bind Course Version List
    /// </summary>
    private void BindCourseVersion()
    {
        //EventVersion objEventVersion = new EventVersion();
        //ddlCourseVersion.Items.Clear();
        //ddlCourseVersion.DataSource = objEventVersion.GetCourseVersionList(this.EvenID);
        //ddlCourseVersion.DataTextField = "trainingEventVer";
        //ddlCourseVersion.DataValueField = "trainingEventVer";
        //ddlCourseVersion.DataBind();
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }
}