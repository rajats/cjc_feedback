﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleRemoveUser.aspx.cs" Inherits="RoleAddRemoveUser" %>
<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.EDE2)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>

     <%--Define Inline Css to Wrap Grid Header and Row Data--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th div {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
        }

        .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
            vertical-align: text-top;
        }
    </style>
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <h4>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h4>
            <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" Visible="false">
                <div class="search-box-body">
                    <div class="plms-input-group">
                        <div class="plms-fieldset">
                            <asp:Label ID="Label2" CssClass="filter-key" AssociatedControlID="txtRoleName" class="plms-label is-hidden no-height" runat="server"
                                Text=""></asp:Label>
                            <asp:TextBox runat="server" Width="180px" placeholder="role name" ID="txtRoleName" class="plms-input skin2" CssClass="filter-key"></asp:TextBox>
                        </div>
                    </div>
                    <input id="btnSearch" type="button" class="btn fluid clearfix" value="search" />
                </div>
            </asp:Panel>
            <div id="grid_wrapper" style="width: 100%;" onkeypress="return disableEnterKey(event)">
                <trirand:JQGrid runat="server" ID="gvRoles" Height="300px"
                    AutoWidth="True" OnCellBinding="gvRoles_CellBinding" OnDataRequesting="gvRoles_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="EmpID" Visible="false" PrimaryKey="True" />
                        <trirand:JQGridColumn DataField="empFirstName" HeaderText="<%$ Resources:Resource, lblEmpFirstName %>"
                            Editable="false" Width="200" />
                        <trirand:JQGridColumn DataField="empLastName" HeaderText="<%$ Resources:Resource, lblEmpLastName %>"
                            Editable="false" Width="200" />
                        <trirand:JQGridColumn DataField="empExtID" HeaderText="<%$ Resources:Resource, lblEmpoyeeID %>"
                            Editable="false" Width="200" />
                        <trirand:JQGridColumn DataField="EmpID" HeaderText="<%$ Resources:Resource, lblRemove %>"
                            Sortable="false" TextAlign="Center" Width="100" />
                    </Columns>
                    <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="loadComplete" BeforeRowSelect="beforeSelectRow" />
                </trirand:JQGrid>
            </div>
        </div>
    </section>
    
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvRoles.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        // Resized JQ Grid
        function jqGridResize() {
            $("#<%=gvRoles.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // Define JQ Grid Load Complete
        function loadComplete(data) {
                jqGridResize();
        }

        // To Reload JQ Grid
        function reloadGrid() {
            $('#<%=gvRoles.ClientID%>').trigger("reloadGrid");
        }

        // To Reset JQ Grid Row Selection
        function beforeSelectRow(id) {
            $('#<%=gvRoles.ClientID%>').jqGrid('resetSelection');
            return (true);

        }

        // To Get JQ Grid Selected Row and move to next page
        function getSelectedRole() {
            var arrRooms = $grid.getGridParam('selarrrow');
            if (arrRooms.length <= 0) {
                ShowPensivoMessage("<%=Resources.Resource.errMsgSelectRole%>");
                return false;
            }
            else {
                window.location.href = "roleActivity.aspx?roleID=" + arrRooms.join(",");
            }
        }

        // To Role Remove User Confirmation
        function RemoveUsers(userID, roleID, searchBy, searchValue, message) {
            var title = "<%=Resources.Resource.lblConfirmation%>";
            //var messageText = "<%=Resources.Resource.msgRuWantToRemoveUser%>";
            var messageText = message.replace(new RegExp("{-", "gm"), '"').replace(new RegExp("{_", "gm"), "'");
            okButtonText = "<%=Resources.Resource.lblOk%>";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "ConfirmRemoveUserOk(" + userID + " , " + roleID + ")";
            LaterCnclButtonRedirectLink = "ConfirmRemoveUsrCancel();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink);
        }

        // To Trigger Role Remove Confirmation OK Button
        function ConfirmRemoveUserOk(userID, roleID) {
            ShowPensivoWaitingMessage("<%=Resources.Resource.lblRoleModifyPleaseWait%>");
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "removeUser";
            datatoPost.UserID = userID;
            datatoPost.RoleID = roleID;
            $.post("RoleRemoveUser.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    ClosePensivoWaitingMessage();
                    $("#dvPensivoCnfrmDialog").removeClass("active");
                    reloadGrid();
                }
                else {
                    ClosePensivoWaitingMessage();
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotRemoveUser%>")
                }
            });
        }

        // To Trigger Role Remove Confirmation Cancel Button
        function ConfirmRemoveUsrCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
        }

        $(document).ready(function () {
            $("#<%=txtRoleName.ClientID%>").focus();
    });
    </script>
</asp:Content>

