﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleRemoveEvent.aspx.cs" Inherits="RoleRemoveEvent" %>
<%@ Import Namespace="iTECH.Pensivo.BusinessLogic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Include Required Css and Js File in Page--%>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <%if (Utils.TrainingInst == (int)Institute.tdc)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/tdcui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.navcanada)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.EDE2)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else if (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/navcanadaui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <%else %>
    <%{ %>
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <%} %>
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>

    <%--Define Inline Css To Increase Row Height--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            height: 40px;
            vertical-align: middle;
        }
    </style>

     <%--Define Inline Css to Wrap Grid Header and Row Data--%>
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .ui-jqgrid .ui-jqgrid-htable th div {
            word-wrap: break-word;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            white-space: normal !important;
            height: auto;
            vertical-align: text-top;
        }

        .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
            vertical-align: text-top;
        }
    </style>

    <section id="main-content" class="pg-advanced-list">
        <div class="wrapper">
            <h1>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h1>
            <aside class="column span-4 fixed-onscroll">
                <div class="search-box" style="width: 384px; left: 216px;">
                    <header class="search-box-header">
                        <h2 class="h6"><%=Resources.Resource.lblFindEvent %></h2>
                        <p class="last-child"><%=Resources.Resource.lblFindEventMessage %></p>
                    </header>
                    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
                        <div class="search-box-body">
                            <div class="plms-input-group">
                                <div class="plms-fieldset">
                                    <asp:Label ID="Label2" AssociatedControlID="txtRoleName" class="filter-key plms-label is-hidden no-height" runat="server"
                                        Text="<%$ Resources:Resource, lblEvents %>" for="name"></asp:Label>
                                    <asp:TextBox ID="txtRoleName" runat="server" CssClass="filter-key plms-input skin2" placeholder="<%$ Resources:Resource, lblEvents %>">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <input id="btnSearch" class="btn fluid clearfix" type="button" style="width: 100%;" value="<%=Resources.Resource.lbSearch%>" />
                        </div>
                    </asp:Panel>
                </div>
            </aside>
            <div class="main-content-body column span-8" onkeypress="return disableEnterKey(event)">
                <div id="grid_wrapper" style="width: 100%;">
                    <trirand:JQGrid runat="server" ID="gvRoles" Height="300px"
                        AutoWidth="True" OnCellBinding="gvRoles_CellBinding" OnDataRequesting="gvRoles_DataRequesting">
                        <Columns>
                            <trirand:JQGridColumn DataField="eventID" Visible="false" PrimaryKey="True" />
                            <trirand:JQGridColumn DataField="eventID" HeaderText="<%$ Resources:Resource, lblTrainingEventID %>"
                                Editable="false" Width="80" />
                            <trirand:JQGridColumn DataField="EventTitle" HeaderText="<%$ Resources:Resource, lblTrainingEventName %>"
                                Editable="false" Width="300" />
                            <trirand:JQGridColumn DataField="EventType" HeaderText="<%$ Resources:Resource, lblTrainingEventType %>"
                                Editable="false" Width="60" />
                            <trirand:JQGridColumn DataField="Duration" HeaderText="<%$ Resources:Resource, lblTrainingEventDuration %>"
                                Editable="false" Width="50" />
                            <trirand:JQGridColumn DataField="eventID" HeaderText="<%$ Resources:Resource, lblRemove %>"
                                Sortable="false" TextAlign="Center" Width="100" />
                        </Columns>
                        <PagerSettings PageSize="1000" PageSizeOptions="[1000,1500,3000,10000]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn=""></SortSettings>
                        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                        <ClientSideEvents LoadComplete="loadComplete" BeforeRowSelect="beforeSelectRow" />
                    </trirand:JQGrid>
                </div>
            </div>
        </div>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Initilized Grid Object
        $grid = $("#<%=gvRoles.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        // Resized JQ Grid
        function jqGridResize() {
            $("#<%=gvRoles.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        // Define JQ Grid Load Complete
        function loadComplete(data) {
            if (data.rows.length == 1 && data.total == 1) {
            }
            else {
                jqGridResize();
            }
        }

        // Define JQ Grid Reload
        function reloadGrid() {
            $('#<%=gvRoles.ClientID%>').trigger("reloadGrid");
        }

        // Allow JQ Grid To Single Row Selection
        function beforeSelectRow(id) {
            $('#<%=gvRoles.ClientID%>').jqGrid('resetSelection');
            return (true);
        }

        // To Confirmation Dialog To Remove Event From Role
        function RemoveEvent(eventID, roleID, message) {
            var title = "<%=Resources.Resource.lblConfirmation%>";
            var messageText = message.replace(new RegExp("{-", "gm"), '"').replace(new RegExp("{_", "gm"), "'");
            okButtonText = "<%=Resources.Resource.lblOk%>";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "ConfirmRoleRemoveEventOk(" + eventID + ", " + roleID + ")";
            LaterCnclButtonRedirectLink = "ConfirmRoleRemoveEventCancel();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Define Role Remove Confirmation OK Button Event
        function ConfirmRoleRemoveEventOk(eventID, roleID) {
            ShowPensivoWaitingMessage("<%=Resources.Resource.lblRoleModifyPleaseWait%>");
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "removeEvent";
            datatoPost.EventID = eventID;
            datatoPost.RoleID = roleID;
            $.post("RoleRemoveEvent.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    $("#dvPensivoCnfrmDialog").removeClass("active");
                    reloadGrid();
                    ClosePensivoWaitingMessage();
                }
                else {
                    ClosePensivoWaitingMessage();
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotRemoveEvent%>");
                }
            });
        }

        // To Define Role Remove Confirmation Cancel Button Event
        function ConfirmRoleRemoveEventCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
        }


        $(document).ready(function () {
            $("#<%=txtRoleName.ClientID%>").focus();
        });

    </script>

</asp:Content>

