﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmpOtherSearchDashBoard.aspx.cs" Inherits="EmpOtherSearchDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <%=htmlText %>
        </div>
        <asp:HiddenField ID="HdnMessage" runat="server" />
    </section>
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // Role Sys Reference Association Confirmation
        function RoleSysRefAssociationConfirmation(roleID, searchBy, searchValue, flag, sysRoleOperator, searchText)
        {
            var title = "<%=Resources.Resource.lblConfirmation%>";
            var messageText = ($("#<%=HdnMessage.ClientID%>").val()).replace("#SEARCHVALUE#", searchText);
            okButtonText = "OK";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "TriggerRoleSysRefAssociationOkButton('" + roleID + "', '" + searchBy + "', '" + searchValue + "',  '" + flag + "', '" + sysRoleOperator + "');";
            LaterCnclButtonRedirectLink = " HideConfirmationDialog();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Trigger Role Sys Ref Association Ok Button
        function TriggerRoleSysRefAssociationOkButton(roleID, searchBy, searchValue, flag, sysRoleOperator) {
            ShowPensivoWaitingMessage("<%=Resources.Resource.lblRoleModifyPleaseWait%>");
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            if (flag == 'add') {
                datatoPost.callBack = "addActionRoleSysRefAssociation";
            }
            else if (flag == 'remove') {
                datatoPost.callBack = "removeActionRoleSysRefAssociation";
            }
            datatoPost.SearchBy = searchBy;
            datatoPost.SearchValue = searchValue;
            datatoPost.RoleID = roleID;
            datatoPost.SysRoleOperator = sysRoleOperator;
            $.post("EmpOtherSearchDashBoard.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    if (flag == 'add') {
                        //ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueAddedToRole%>", "EmpSearchDashBoard.aspx?roleID=" + roleID + "&flag=" + flag + "");
                        window.location.href = "EmpSearchDashBoard.aspx?roleID=" + roleID + "&flag=" + flag;
                    }
                    else if (flag == 'remove') {
                        //ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueRemovedToRole%>", "EmpSearchDashBoard.aspx?roleID=" + roleID + "&flag=" + flag + "");
                        window.location.href = "EmpSearchDashBoard.aspx?roleID=" + roleID + "&flag=" + flag;
                    }
                    ClosePensivoWaitingMessage();
                }
                else {
                    if (flag == 'add') {
                        ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueNotAddedToRole%>")
                    }
                    else if (flag == 'remove') {
                        ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueNoRemovedToRole%>")
                    }
                    ClosePensivoWaitingMessage();
                }
            });
        }
    </script>
</asp:Content>

