﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.Xml;
using System.IO;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;


public partial class AdministrationDashBoard : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (BusinessUtility.GetString(Request.QueryString["returnfromoldPIB"]) == "1")
            {
                EmployeeTracking.TrackEmployeeAction(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)UserTrackingActionItemID.ReturnToPibFromOldSystem,
                    BusinessUtility.GetString(CurrentEmployee.EmpSessionID), BusinessUtility.GetString(CurrentEmployee.EmpBrowserAgent), BusinessUtility.GetString(CurrentEmployee.EmpIPAddress), "", 0, (int)RusticRegStatusCode.None, "");
            }

            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            if (Request.QueryString["error"] != "")
            {
                
                Breadcrumbs.BreadcrumbsSetRootMenu();
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblHome, Path.GetFileName(Request.Url.AbsolutePath), Path.GetFileName(Request.Url.AbsolutePath));
            }
            else
            {
                currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblHome, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);
            }
        }
        Employee objEmpNew = new Employee();
        Role objRole = new Role();
        if ((objRole.GetUserHasFunctionality(BusinessUtility.GetInt(CurrentEmployee.EmpID), (int)RoleAction.Manage_List, BusinessUtility.GetString(objEmpNew.GetEmpRefCodeValue(CurrentEmployee.EmpID, EmpSearchBy.Store))) == true))
        {
            dvManageList.Visible = true;
        }

    }

}