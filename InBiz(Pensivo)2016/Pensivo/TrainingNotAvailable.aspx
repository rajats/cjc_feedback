﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="TrainingNotAvailable.aspx.cs" Inherits="TrainingNotAvailable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">

            <h5 style="text-align:center">There is no training available for a few days  due to the festivities and some seasonal maintenance.
                <br />
                It will be available again on Monday January 4th at 9AM.
            </h5>

        </div>
    </section>
</asp:Content>

