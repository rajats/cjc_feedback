﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AdminReporting.aspx.cs" Inherits="AdminReporting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div id="dvAccessAllData" class="boxset-box" runat="server">
                    <a id="hrfAccessAllData" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblCanAccessAllDataforReporting%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnRoleID" runat="server" />
        <asp:HiddenField ID="hdnActionID" runat="server" />
        <asp:HiddenField ID="hdnActionType" runat="server" />
        <asp:HiddenField ID="hdnFlag" runat="server" />
        <asp:HiddenField ID="HdnMessage" runat="server" />
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Remove Functionality Confirmation
        function RemoveAction() {
            var title = "Confirmation";
            var messageText = "<%=Resources.Resource.msgRuWantToRemoveFunctionality%>";
            okButtonText = "OK";
            LaterCnclButtonText = "Cancel";
            okButtonRedirectlink = "ConfirmRemoveActionOk()";
            LaterCnclButtonRedirectLink = "ConfirmRemoveActionCancel();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Trigger Confirm Remove Action OK
        function ConfirmRemoveActionOk() {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "removeAction";
            datatoPost.ActionID = $("#<%=hdnActionID.ClientID%>").val();
            datatoPost.ActionType = $("#<%=hdnActionType.ClientID%>").val();
            datatoPost.RoleID = $("#<%=hdnRoleID.ClientID%>").val();
            $.post("AddReportingDashboard.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    window.location.href = "roleActivity.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val();
                }
                else {
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotRemoveFunctionality%>");
                }
            });
        }

        // To Trigger Confirm Remove Action Cancel
        function ConfirmRemoveActionCancel() {
            $("#dvPensivoCnfrmDialog").removeClass("active");
        }

        // To Add Action Confirmation
        function AddAction() {
            var title = "<%=Resources.Resource.lblConfirmation%>";
            var messageText = ($("#<%=HdnMessage.ClientID%>").val());
            okButtonText = "OK";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "TriggerAddAction();";
            LaterCnclButtonRedirectLink = " HideConfirmationDialog();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);
        }

        // To Trigger Add Action OK
        function TriggerAddAction() {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            datatoPost.callBack = "addAction";
            datatoPost.ActionID = $("#<%=hdnActionID.ClientID%>").val();
            datatoPost.ActionType = $("#<%=hdnActionType.ClientID%>").val();
            datatoPost.RoleID = $("#<%=hdnRoleID.ClientID%>").val();
            $.post("AdminReporting.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    window.location.href = "roleActivity.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val();
                }
                else {
                    ShowPensivoMessage("<%=Resources.Resource.msgCouldNotAddFunctionality%>")
                }
            });
        }
    </script>
</asp:Content>

