﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AdminLogOut.aspx.cs" Inherits="AdminLogOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box" id="dvReturnToSuperAdminAccount" runat="server" onclick="LoginAsSupperAdminAccount();">
                    <a href="#" class="boxset-box-wrapper" id="hrfReturnToSuperAdminAccount" runat="server">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblReturnToSuperAdminAccount%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" id="dvLoginAsAnotherUser" runat="server">
                    <a href="#" class="boxset-box-wrapper" id="hrfLoginAsAnotherUser" runat="server" onclick="LoginAsAnotherUser();">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblLoginAsAnotherUser%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" id="dvLogoutoftheSystem" runat="server">
                    <a href="#" class="boxset-box-wrapper" id="hrfLogoutoftheSystem" onclick="LogOutofTheSystem();" runat="server">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblLogoutoftheSystem%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <asp:Button ID="btnReturnToSuperAdminAccount" Style="display: none;" runat="server" OnClick="btnReturnToSuperAdminAccount_Click" />
    <asp:Button ID="btnLoginAsAnotherAccount" Style="display: none;" runat="server" OnClick="btnLoginAsAnotherAccount_Click" />
    <asp:Button ID="btnLogOutofTheSystem" Style="display: none;" runat="server" OnClick="btnLogOutofTheSystem_Click" />

    <%--Define JavaScript Function--%>
        <script type="text/javascript">
            // To Trigger Return To Super Admin Account  Event
            function LoginAsSupperAdminAccount() {
                $("#<%=btnReturnToSuperAdminAccount.ClientID%>").trigger("click");
        }

        // To Trigger Login As Another Account
        function LoginAsAnotherUser() {
            $("#<%=btnLoginAsAnotherAccount.ClientID%>").trigger("click");
        }

        // To Trigger Logout of The System
        function LogOutofTheSystem() {
            $("#<%=btnLogOutofTheSystem.ClientID%>").trigger("click");
        }
    </script>
</asp:Content>



