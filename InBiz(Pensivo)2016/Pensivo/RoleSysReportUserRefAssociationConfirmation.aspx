﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleSysReportUserRefAssociationConfirmation.aspx.cs" Inherits="RoleSysReportUserRefAssociationConfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <h4>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h4>
            <h5>
                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, lblSummary %>"></asp:Literal>
                : </h5>
            <br />
            <br />
            <br />
            <br />
            <h6>
                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, lblIsItCorrect %>"></asp:Literal></h6>
            <div class="btngrp " style="float: right">
                <a href="#nogo" class="btn" onclick="AddRole();"><%=Resources.Resource.lblYes %></a>
                <a id="hrefNo" runat="server" href="#nogo" class="btn "><%=Resources.Resource.lblNo %></a>
            </div>
        </div>

        <asp:HiddenField ID="hdnRoleID" runat="server" />
        <asp:HiddenField ID="HdnSearchValue" runat="server" />
        <asp:HiddenField ID="HdnSearchBy" runat="server" />
        <asp:HiddenField ID="HdnFlag" runat="server" />
        <asp:HiddenField ID="HdnActionID" runat="server" />
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Add Role SysReport User Managment
        function AddRole() {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                datatoPost.callBack = "addAction";
            }
            else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                datatoPost.callBack = "removeAction";
            }
            datatoPost.SearchBy = $("#<%=HdnSearchBy.ClientID%>").val();
            datatoPost.SearchValue = $("#<%=HdnSearchValue.ClientID%>").val();
            datatoPost.RoleID = $("#<%=hdnRoleID.ClientID%>").val();
            datatoPost.ActionID = $("#<%=HdnActionID.ClientID%>").val();
            $.post("RoleSysReportUserRefAssociationConfirmation.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                        window.location.href = "roleActivity.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val() + "&flag=" + $("#<%=HdnFlag.ClientID%>").val();
                    }
                    else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                        window.location.href = "roleActivity.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val() + "&flag=" + $("#<%=HdnFlag.ClientID%>").val();
                    }
                }
                else {
                    if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                        ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueNotAddedToRole%>")
                    }
                    else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                        ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueNoRemovedToRole%>")
                    }
                }
            });
        }
    </script>

</asp:Content>

