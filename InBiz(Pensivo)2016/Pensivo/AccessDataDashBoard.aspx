﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AccessDataDashBoard.aspx.cs" Inherits="AccessDataDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset">
                <div class="boxset-box">
                    <a id="hrfSearchEmpSite" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title" id="hSite" runat="server"></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfSearchEmpRegion" runat="server" href="#" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblSearchJobCode%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

