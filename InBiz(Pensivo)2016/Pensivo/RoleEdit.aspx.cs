﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections;

public partial class RoleEdit : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvRoles))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblEditSystemRole, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
        }
    }

    /// <summary>
    /// To Define JQ Grid Cell Binding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event</param>
    protected void gvRoles_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        Int64 iRoleID = BusinessUtility.GetInt(e.RowKey);
        if (e.ColumnIndex == 3)
        {
            e.CellHtml = string.Format(@"<a class='btn' style='float:none;font-size:9px;'   onclick=""EditRole('{0}')"">" + Resources.Resource.BtnEditRoleName + "</a>", iRoleID);
        }
    }

    /// <summary>
    /// To Bind JQ Grid with DataSource
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event </param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Role objRole = new Role();
        string txtRole = Utils.ReplaceDBSpecialCharacter(BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]));
        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            gvRoles.DataSource = objRole.GetRoleList(txtRole);
        }
        else
        {
            gvRoles.DataSource = objRole.GetRoleList("");
        }
        gvRoles.DataBind();
    }



    /// <summary>
    /// To Create Web Service To Get Role Editable or Not
    /// </summary>
    /// <returns>string</returns>
    [System.Web.Services.WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string IsRoleLocked(int roleID)
    {
        int isLockRole = 0;
        try
        {
            Role objRole = new Role();
           if (objRole.IsRoleLocked(roleID)==true)
           {
               isLockRole = 1;
           }
        }
        catch (Exception ex)
        {
            ErrorLog.CreateLog(ex);
        }
        return BusinessUtility.GetString( isLockRole);
    }

}