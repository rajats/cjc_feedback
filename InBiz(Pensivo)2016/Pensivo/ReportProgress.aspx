﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ReportProgress.aspx.cs" Inherits="ReportProgress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box">
                    <a id="hrfAllTrainingProgress" runat="server" href="#" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblAllCourseProgress%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfThoseNotCompletedTraining" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblRptThoseThathaveNotCompleteTraining%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfTrainingCompleteOnce" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblRptTrainingCompleteOnce%></h4>
                        </div>
                    </a>
                </div>
               <%-- <div class="boxset-box">
                    <a id="hrfTrainingNotCompletedOnce" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblRptProgressTrainingNotCompletedOnce%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfNotStartedAtOnce" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTrainingNotStartedAtOnce%></h4>
                        </div>
                    </a>
                </div>--%>
            </div>
        </div>
        <asp:HiddenField ID="hdnReportType" runat="server" />
        <asp:Button ID="btnShowReport" runat="server" OnClick="btnShowReport_Click" />
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        function ShowReport(reportType) {
            $("#<%=hdnReportType.ClientID %>").val(reportType);
            $("#<%=btnShowReport.ClientID %>").trigger("click");
        }
    </script>
</asp:Content>


