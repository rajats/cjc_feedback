﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="TraningYearSelection.aspx.cs" Inherits="TraningYearSelection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <header class="main-content-header">
                <h1><%=Resources.Resource.lblTraningYear %></h1>
            </header>
            <div class="boxset">
                <div class="boxset-box">
                    <a href="EmployeeCourses.aspx?CourseStatus=5&TYear=2015" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTraningYear2015%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a href="EmployeeCourses.aspx?CourseStatus=5&TYear=2016" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTraningYear2016%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

</asp:Content>

