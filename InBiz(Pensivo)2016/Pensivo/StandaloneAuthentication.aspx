﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="StandaloneAuthentication.aspx.cs" Inherits="StandaloneAuthentication" %>

<head>
    <%--Include Required Css and Js File in Page--%>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script language="javascript">
        $(document).ready(function () {
            $('#clear').click(function () {
                location.href = ($('#clearURL').val());
            });
        });
    </script>
    <style>
        body, select, input {
            font-size: 16px;
        }

        input {
            margin: 10px;
            padding: 5px;
        }

        #sess, #back, #expiry, #error {
            width: 600px;
        }
    </style>
</head>
<body>
    <form id="thisForm" method="post" action="http://cdtbs.ws53aws.pensivo.com/index.php?fuseaction=authentication.standalone" runat="server">
        <%=Resources.Resource.lblForwardingToServer %>
        <label for="uuid" style="display: none;">UUID</label>
        <input id="uuid" type="text" name="uuid" style="display: none;" runat="server" />
        <br />
        <label for="backURL" style="display: none;">Back URL</label>
        <input type="text" name="backURL" id="backURL" style="display: none;" runat="server" />
        <br />
        <label for="errorURL" style="display: none;">Error URL</label>
        <input type="text" name="errorURL" id="errorURL" style="display: none;" runat="server" />
        <br />
        <label for="sessionTimeoutURL" style="display: none;">Session Timeout URL</label>
        <input type="text" name="sessionTimeoutURL" style="display: none;" id="sessionTimeoutURL" runat="server" />
        <br />
        <label for="destination" style="display: none;">Destination</label>
        <input id="destination" name="destination" style="display: none;" runat="server" />
        <input type="hidden" name="key" id="key" style="display: none;" runat="server" />
        <br />
        <label for="secret" style="display: none;">Secret Key</label>
        <input type="text" name="secret" id="secret" style="display: none;" runat="server" />
        <br />
        <input id="submit" style="display: none;" type="submit">
    </form>
</body>
<%--Define JavaScript Function--%>
<script type="text/javascript">
    $(document).ready(function () {
        $("#submit").trigger("click");
    });
</script>

