﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;

public partial class RoleTrainingEvent : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvRoles))
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblAssignTrainingEvent, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
            ltrTitle.Text = Resources.Resource.lblSelectTheTrainingEvents;
        }

        HdnRoleID.Value = BusinessUtility.GetString(this.RoleID);
        HdnSearchBy.Value = BusinessUtility.GetString(this.SearchBy);
        HdnFlag.Value = BusinessUtility.GetString(this.Flag);
        HdnActionID.Value = BusinessUtility.GetString(this.ActionID);
        HdnFunctionalityType.Value = BusinessUtility.GetString(this.FunctionalityType);

        Event objEvent = new Event();
        string roleName = "";
        if (this.RoleID > 0)
        {
            objRole = new Role();
            roleName = objRole.GetRoleName(this.RoleID);
        }

        string sTrainingEventName = "";
        string[] searchval = this.SearchValue.Split(',');
        foreach (string sValues in searchval)
        {
            objEvent = new Event();
            objEvent.GetEventDetail(BusinessUtility.GetInt(sValues), Globals.CurrentAppLanguageCode);

            if (sTrainingEventName == "")
            {
                sTrainingEventName = objEvent.EventName;
            }
            else
            {
                sTrainingEventName += ",<br/>" + objEvent.EventName;
            }
        }

        if (this.Flag == "add")
        {
            HdnMessage.Value = Resources.Resource.lblAssignTrainingEventConfirmationAllow.Replace("#ROLENAME#", roleName);
        }
        else if (this.Flag == "remove")
        {
            HdnMessage.Value = Resources.Resource.lblAssignTrainingEventConfirmationDisAllow.Replace("#ROLENAME#", roleName);
        }
        HdnSearchBy.Value = BusinessUtility.GetString(this.SearchBy);
        HdnFlag.Value = BusinessUtility.GetString(this.Flag);
        HdnActionID.Value = BusinessUtility.GetString(this.ActionID);
        HdnFunctionalityType.Value = BusinessUtility.GetString(this.FunctionalityType);
        HdnCreatedBy.Value = BusinessUtility.GetString(CurrentEmployee.EmpID);

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "addAction")
        {
            try
            {
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                string functionalityType = BusinessUtility.GetString(Request.Form["FunctionalityType"]);
                int createdBy = BusinessUtility.GetInt(Request.Form["createdBy"]);
                int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                if (searchValue != "" && roleID > 0 && actionID > 0)
                {
                    objRole = new Role();
                    if (objRole.ActionExistsInRole(roleID, actionID, functionalityType) == false)
                    {
                        objRole.RoleAddFunctionality(roleID, actionID, functionalityType);
                    }

                    if (objRole.RoleAssignTrainingEvent(roleID, searchValue, createdBy) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
        else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "removeAction")
        {
            try
            {
                int roleID = BusinessUtility.GetInt(Request.Form["RoleID"]);
                string searchBy = BusinessUtility.GetString(Request.Form["SearchBy"]);
                string searchValue = BusinessUtility.GetString(Request.Form["SearchValue"]);
                int actionID = BusinessUtility.GetInt(Request.Form["ActionID"]);
                string functionalityType = BusinessUtility.GetString(Request.Form["FunctionalityType"]);
                if (roleID > 0 && searchValue != "" && actionID > 0)
                {
                    objRole = new Role();
                    if (objRole.RemoveRoleAssignTrainingEvent(roleID, searchValue) == true)
                    {
                        objRole = new Role();
                        if (objRole.IsRoleAssignTrainingEvent(roleID) == false)
                        {
                            objRole.RoleRemoveFunctionality(roleID, actionID, functionalityType);
                        }
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }
            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
    }

    /// <summary>
    /// To Bind JQ Grid With Training Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event Args</param>
    protected void gvRoles_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtRole = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtRoleName"]);
        Role objRole = new Role();
        if (this.Flag == "add")
        {
            gvRoles.DataSource = objRole.GetAssignTrainingNotInRole(this.RoleID, Globals.CurrentAppLanguageCode, Utils.ReplaceDBSpecialCharacter(txtRole));
        }
        else if (this.Flag == "remove")
        {
            gvRoles.DataSource = objRole.GetAssignTrainingInRole(this.RoleID, Globals.CurrentAppLanguageCode, Utils.ReplaceDBSpecialCharacter(txtRole));
        }
        gvRoles.DataBind();
    }

    /// <summary>
    /// To Get Role ID
    /// </summary>
    public int RoleID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["roleID"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string SearchBy
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchby"]);
        }
    }

    /// <summary>
    /// To Get Search By Value
    /// </summary>
    public string SearchValue
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["searchval"]);
        }
    }

    /// <summary>
    /// To Get Action ID
    /// </summary>
    public int ActionID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["actionID"]);
        }
    }

    /// <summary>
    /// To Get Flag Add/Remove
    /// </summary>
    public string Flag
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["flag"]);
        }
    }

    /// <summary>
    /// To Get Functionality Type
    /// </summary>
    public string FunctionalityType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["functionalityType"]);
        }
    }

    /// <summary>
    /// Service To Get Training Event Name
    /// </summary>
    /// <param name="eventID">Pass Event ID</param>
    /// <returns>String</returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetEventName(string eventID)
    {
        Event objEvent = new Event();
        string sTrainingEventName = "";
        string[] searchval = eventID.Split(',');
        foreach (string sValues in searchval)
        {
            objEvent = new Event();
            objEvent.GetEventDetail(BusinessUtility.GetInt(sValues), Globals.CurrentAppLanguageCode);

            if (sTrainingEventName == "")
            {
                sTrainingEventName = objEvent.EventName;
            }
            else
            {
                sTrainingEventName += ",<br/>" + objEvent.EventName;
            }
        }
        return sTrainingEventName;
    }
}