﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;

public partial class EmployeeManageList : BasePage
{
    /// <summary>
    /// To Create Role Class Object
    /// </summary>
    Role objRole;

    /// <summary>
    /// To Create Role Manage List Class object
    /// </summary>
    RoleManageLists objRoleManageLists;

    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Manage_List);
        //DataTable dtHasRecord = (DataTable)gvManageList.DataSource;
        objRoleManageLists = new RoleManageLists();
        DataTable dtHasRecord = objRoleManageLists.GetEmployeeManageList(CurrentEmployee.EmpID, Globals.CurrentAppLanguageCode, "");

        if (dtHasRecord.Rows.Count == 1)
        {
            string sManageListName = BusinessUtility.GetString( BusinessUtility.GetString( dtHasRecord.Rows[0][1]) ).Replace("\"", "{-").Replace("'", "{_");
            int manageListID = BusinessUtility.GetInt(dtHasRecord.Rows[0][3]);
            int iRoleID = BusinessUtility.GetInt(dtHasRecord.Rows[0][0]);
            int iFunctionalityID = (int)RoleAction.Reporting;

            if ((manageListID == 1) || (manageListID == 2))
            {
                iFunctionalityID = (int)RoleAction.Reporting;
                Response.Redirect("EmployeeSiteManager.aspx?roleManageListID=" + iRoleID + "&roleManageListName=" + Server.UrlEncode(sManageListName) + "&SearchBy=" + EmpRefCode.Store + "&FunctionalityID=" + BusinessUtility.GetString(iFunctionalityID) + "&ListID=" + BusinessUtility.GetString(manageListID), false);
            }
            else if ((manageListID == 3))
            {
                iFunctionalityID = (int)RoleAction.Reporting;
                Response.Redirect("EmployeeCrossDocklist.aspx?roleManageListID=" + iRoleID + "&roleManageListName=" + Server.UrlEncode(sManageListName) + "&SearchBy=" + EmpRefCode.Store + "&FunctionalityID=" + BusinessUtility.GetString(iFunctionalityID) + "&ListID=" + BusinessUtility.GetString(manageListID), false);
                //iFunctionalityID = (int)RoleAction.AI_Report_Initiation;
            }
            else if ((manageListID == 4))
            {
                iFunctionalityID = (int)RoleAction.Reporting;
                Response.Redirect("EmployeeSiteReportingDistrict.aspx?roleManageListID=" + iRoleID + "&roleManageListName=" + Server.UrlEncode(sManageListName) + "&SearchBy=" + EmpRefCode.District + "&FunctionalityID=" + BusinessUtility.GetString(iFunctionalityID) + "&ListID=" + BusinessUtility.GetString(manageListID), false);
            }
            else if ((manageListID == 5))
            {
                iFunctionalityID = (int)RoleAction.Reporting;
                Response.Redirect("EmployeeSiteReportingRegionalDirector.aspx?roleManageListID=" + iRoleID + "&roleManageListName=" + Server.UrlEncode(sManageListName) + "&SearchBy=" + EmpRefCode.District + "&FunctionalityID=" + BusinessUtility.GetString(iFunctionalityID) + "&ListID=" + BusinessUtility.GetString(manageListID), false);
            }
        }

        if (!IsPostBack && !IsPagePostBack(gvManageList))
        {

            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblManageLists, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));
        }
    }

    /// <summary>
    /// To Define JQ Grid Cell Binding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Cell Bind Event</param>
    protected void gvManageList_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        Int64 iRoleID = BusinessUtility.GetInt(e.RowKey);
        if (e.ColumnIndex == 2)
        {
            string sManageListName = BusinessUtility.GetString(e.RowValues[1]).Replace("\"", "{-").Replace("'", "{_");
            int manageListID = BusinessUtility.GetInt(e.RowValues[3]);
            int iFunctionalityID = (int)RoleAction.Reporting;

            if ((manageListID == 1) || (manageListID == 2))
            {
                iFunctionalityID = (int)RoleAction.Reporting;
            }
            else if ((manageListID == 3))
            {
                iFunctionalityID = (int)RoleAction.AI_Report_Initiation;
            }
            e.CellHtml = string.Format(@"<a class='btn' style='float:none;'   onclick=""SelectManageList('{0}','{1}','{2}','{3}','{4}')"">" + Resources.Resource.BtnManageListSelect + "</a>", iRoleID, Server.UrlEncode(sManageListName), EmpRefCode.Store, BusinessUtility.GetString(iFunctionalityID), BusinessUtility.GetString(manageListID));
        }
    }

    /// <summary>
    /// To Bind JQ Grid with DataSource with Manage List Data Source
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass JQ Grid Data Request Event </param>
    protected void gvManageList_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtListName = BusinessUtility.GetString(Request.QueryString["ContentPlaceHolder1_txtListName"]);
        objRoleManageLists = new RoleManageLists();
        gvManageList.DataSource = objRoleManageLists.GetEmployeeManageList(CurrentEmployee.EmpID, Globals.CurrentAppLanguageCode, Utils.ReplaceDBSpecialCharacter(txtListName));
        gvManageList.DataBind();
    }


}