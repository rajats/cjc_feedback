﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="FeedbackHome.aspx.cs" Inherits="FeedbackHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box">
                    <a href="EventFeedbackOn.aspx" class="boxset-box-wrapper "><%--is-selected--%>
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblGetFeedback%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a href="employeecourses.aspx?CourseStatus=3" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblGiveFeedback%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>



