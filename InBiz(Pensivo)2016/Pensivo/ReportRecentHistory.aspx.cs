﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.Data;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;


public partial class ReportRecentHistory : BasePage
{
    /// <summary>
    /// To Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsPagePostBack(gvReportRecentHistory))
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
            ltrTitle.Text = Resources.Resource.lblRecentReport;
            Breadcrumbs.ResetReportMenu();
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblRecentTrainingReports, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery);
        }
    }

    /// <summary>
    /// To Define JQ Grid Cell Binding Event
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void gvReportRecentHistory_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        Int64 iReportID = BusinessUtility.GetInt(e.RowKey);
        if (e.ColumnIndex == 1)
        {
            e.CellHtml = string.Format(@"<a href=""Report.aspx?repID={1}""  >{0}</a>", e.CellHtml, iReportID);
        }
        else if (e.ColumnIndex == 2)
        {
            e.CellHtml = string.Format(@"<a class='btn' style='float:none;font-size:9px;'   onclick=""ShowReportDetails('{0}')"">" + Resources.Resource.BtnDetails + "</a>", iReportID);
        }
    }

    /// <summary>
    /// Bind Report Recent History JQ Grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvReportRecentHistory_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Report objReport = new Report();
        gvReportRecentHistory.DataSource = objReport.GetReportRecentHistory(BusinessUtility.GetInt(CurrentEmployee.EmpID));
        gvReportRecentHistory.DataBind();
    }

    /// <summary>
    /// Get Report Summary Detail
    /// </summary>
    /// <param name="reportID">Pass Report ID</param>
    /// <returns></returns>
    protected static string GetReportSummaryDetails(int reportID)
    {
        if (reportID > 0)
        {
            Report objReport = new Report();
            List<ReportFilter> reportFilterList = objReport.GetReportFilter(reportID);

            StringBuilder sbHtml = new StringBuilder();
            Event objEvent = new Event();
            Employee objEmp = new Employee();
            Role objRole = new Role();

            sbHtml = new StringBuilder();
            sbHtml.Append("<style>.reportTable{width:100%;}  .reportCol1{width:auto;height:auto;word-break:break-word}  .reportCol2{width:100px;height:auto;word-break:break-word;vertical-align:top;padding-left:0px} .reportCol3{width:200px;height:auto;word-break:break-all;max-width: 200px;}  .reportTable tr:nth-child(2n+1) td{background: none repeat scroll 0 0 transparent;} .reportTable tr{ border-bottom: 0px solid rgba(0, 0, 0, 0.1);}    .reportTableDesc tr:nth-child(2n+1) td{background: none repeat scroll 0 0 transparent;padding-left: 0px;} .reportTableDesc tr{ border-bottom: 0px solid rgba(0, 0, 0, 0.1);} .titlelable{font-weight:bold;font-size:18px} .subtitlelable{font-weight:normal;font-size:16px;font-style:italic;padding-left: 15px;}  </style>");
            sbHtml.Append("<table class='reportTable' style='border:1'>");
            int rowIndex = 1;

            sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resource.ResourceValue("lblReportWho", Globals.CurrentCultureName) + ":"));
            var resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.Who.ToLower()).ToList();
            if (resultItem != null && resultItem.Count <= 0)
            {
                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblAllTrainingUser", Globals.CurrentCultureName)));
            }
            else
            {
                if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "".ToLower()) && (resultItem.Count == 1))
                {
                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblAllTrainingUser", Globals.CurrentCultureName)));
                }
                else
                {

                    foreach (var filterItem in resultItem)
                    {
                        if (filterItem.ReportOption.ToLower() == ReportOption.Who.ToLower())
                        {
                            if (filterItem.SearchBy.ToLower() == EmpSearchBy.Name.ToLower())
                            {
                                string[] sName = BusinessUtility.GetString(filterItem.SearchValue).Split(',');
                                int i = 0;
                                foreach (string sEmName in sName)
                                {

                                    objEmp = new Employee();
                                    objEmp.GetEmployeeDetail(BusinessUtility.GetInt(sEmName));

                                    if (i == 0)
                                    {
                                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, BusinessUtility.GetString(Resource.ResourceValue("lblSearchName", Globals.CurrentCultureName)) + ":", BusinessUtility.GetString(objEmp.EmpName)));
                                    }
                                    else
                                    {
                                        sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", BusinessUtility.GetString(objEmp.EmpName)));
                                    }
                                    i += 1;
                                }

                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.EmpCode.ToLower())
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblSearchEmployeeCode", Globals.CurrentCultureName) + ":", filterItem.SearchValue));
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.District.ToLower())
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblSearchDistrict", Globals.CurrentCultureName) + ":", filterItem.SearchValue));
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.RegionTBS.ToLower())
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblSearchRegion", Globals.CurrentCultureName) + ":", filterItem.SearchValue));
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.DivisionTBS.ToLower())
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblSearchDivision", Globals.CurrentCultureName) + ":", filterItem.SearchValue));
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Type.ToLower())
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblEmpType", Globals.CurrentCultureName) + ":", filterItem.SearchValue));
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.JobCode.ToLower())
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblSearchJobCode", Globals.CurrentCultureName) + ":", filterItem.SearchValue));
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Division.ToLower())
                            {
                                if (filterItem.SearchValue.ToLower() == Resource.ResourceValue("lblSubTileAll", Globals.CurrentCultureName).ToLower())
                                {
                                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblSearchLocation", Globals.CurrentCultureName) + ":", Resource.ResourceValue("lblSubTileAll", Globals.CurrentCultureName)));
                                }
                                else
                                {
                                    objRole = new Role();
                                    DataTable dt = new DataTable();

                                    DataView dv = objRole.GetSysRef(filterItem.SearchBy, "", Globals.CurrentAppLanguageCode).DefaultView;
                                    dv.RowFilter = "sysRefCodeValue ='" + filterItem.SearchValue + "'";
                                    string sValue = BusinessUtility.GetString(dv[0]["tiles"]);
                                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblSearchLocation", Globals.CurrentCultureName) + ":", sValue));
                                }
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Store.ToLower())
                            {
                                if (Utils.TrainingInst == (int)Institute.bdl)
                                {
                                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblDeptID", Globals.CurrentCultureName) + ":", filterItem.SearchValue));
                                }
                                else
                                {
                                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblSearchSite", Globals.CurrentCultureName) + ":", filterItem.SearchValue));
                                }
                            }
                            else if (filterItem.SearchBy.ToLower() == EmpSearchBy.Site.ToLower())
                            {
                                sbHtml.Append(CreateFilterDataRow(ref rowIndex, Resource.ResourceValue("lblSearchSite", Globals.CurrentCultureName) + ":", filterItem.SearchValue));
                            }
                        }
                    }
                }
            }

            sbHtml.Append(CreateFilterBlankRow(ref rowIndex));
            sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resource.ResourceValue("lblReportWhich", Globals.CurrentCultureName) + ":"));
            resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.Which.ToLower()).ToList();
            if (resultItem != null && resultItem.Count <= 0)
            {
                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblAllTrainingEvents", Globals.CurrentCultureName)));
            }
            else
            {
                if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "".ToLower()) && (resultItem.Count == 1))
                {
                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblAllTrainingEvents", Globals.CurrentCultureName)));
                }
                else
                {
                    foreach (var filterItem in resultItem)
                    {
                        string searchBy = "";
                        if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByEventName.ToLower()))
                        {
                            searchBy = Resource.ResourceValue("lblTrainingByTitle", Globals.CurrentCultureName);
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByList.ToLower()))
                        {
                            searchBy = Resource.ResourceValue("lblTrainingByList", Globals.CurrentCultureName);
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportFilterOption.ByCategory.ToLower()))
                        {
                            searchBy = Resource.ResourceValue("lblTrainingByCategory", Globals.CurrentCultureName);
                            objEvent = new Event();
                            DataTable dt = objEvent.GetCourseTypeList(Globals.CurrentAppLanguageCode);
                            DataView dv = dt.DefaultView;
                            dv.RowFilter = "TypeValue='" + filterItem.SearchValue + "'";
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, BusinessUtility.GetString(searchBy) + ":", BusinessUtility.GetString(dv[0][1])));
                        }


                        if ((filterItem.SearchBy.ToLower() != ReportFilterOption.ByCategory.ToLower()))
                        {
                            string[] sName = BusinessUtility.GetString(filterItem.SearchValue).Replace("<br/>", "").Split(',');
                            int i = 0;
                            foreach (string sEmName in sName)
                            {
                                objEvent = new Event();
                                objEvent.GetEventDetail(BusinessUtility.GetInt(sEmName), Globals.CurrentAppLanguageCode);

                                if (i == 0)
                                {
                                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, BusinessUtility.GetString(searchBy) + ":", BusinessUtility.GetString(objEvent.EventName)));
                                }
                                else
                                {
                                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", BusinessUtility.GetString(objEvent.EventName)));
                                }
                                i += 1;
                            }
                        }
                    }
                }
            }
            sbHtml.Append(CreateFilterBlankRow(ref rowIndex));
            sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resource.ResourceValue("lblReportWhatProgress", Globals.CurrentCultureName) + ":"));
            resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.What.ToLower()).ToList();
            if (resultItem != null && resultItem.Count <= 0)
            {
                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblAllTraininProgress", Globals.CurrentCultureName)));
            }
            else
            {
                if ((BusinessUtility.GetString(resultItem[0].SearchBy).ToLower() == "all".ToLower()) && (resultItem.Count == 1))
                {
                    sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblAllTraininProgress", Globals.CurrentCultureName)));
                }
                else
                {

                    foreach (var filterItem in resultItem)
                    {
                        if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Once_Completed.ToLower()))
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblTrainingCompleteOnce", Globals.CurrentCultureName)));
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Completed.ToLower()))
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblTrainingNotCompletedOnce", Globals.CurrentCultureName)));
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Course_Not_Once_Started.ToLower()))
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblTrainingNotStartedAtOnce", Globals.CurrentCultureName)));
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.All_Course_Progress.ToLower()))
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblAllCourseProgress", Globals.CurrentCultureName)));
                        }
                        else if ((filterItem.SearchBy.ToLower() == ReportEventProgressStatus.Only_Those_Not_Completed_Training.ToLower()))
                        {
                            sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblTrainingNotCompletedOnce", Globals.CurrentCultureName)));
                        }
                    }
                }
            }

            sbHtml.Append(CreateFilterBlankRow(ref rowIndex));
            sbHtml.Append(CreateFilterSubHeader(ref rowIndex, Resource.ResourceValue("lblReportWhen", Globals.CurrentCultureName) + ":"));
            resultItem = reportFilterList.Where(x => x.ReportOption.ToLower() == ReportOption.When.ToLower()).ToList();
            if (resultItem != null && resultItem.Count <= 1)
            {
                sbHtml.Append(CreateFilterDataRow(ref rowIndex, "", Resource.ResourceValue("lblReportTrainingYearToDate", Globals.CurrentCultureName)));
            }
            sbHtml.Append("</table>");
            return BusinessUtility.GetString(sbHtml);
        }
        else
        {
            return "";
        }
    }

    /// <summary>
    /// Create Report Filter Sub Header Row in Report Filter Detail
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <param name="label">Pass Label</param>
    /// <returns>String</returns>
    private static string CreateFilterSubHeader(ref int rowIndex, string label)
    {
        string rValue = "";
        if (rowIndex == 1)
        {
            rValue = (" <tr><td class='reportCol2 titlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + "" + "</td></tr>");
        }
        else
        {
            rValue = (" <tr><td class='reportCol2 titlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + "" + "</td></tr>");
        }
        rowIndex += 1;
        return rValue;
    }

    /// <summary>
    /// Create Report Filter Data Row in Report Filter Detail
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <param name="label">Pass Label</param>
    /// <param name="labelValue">Pass Label Value</param>
    /// <returns></returns>
    private static string CreateFilterDataRow(ref int rowIndex, string label, string labelValue)
    {
        rowIndex += 1;
        return (" <tr><td class='reportCol2 subtitlelable'>" + label + "</td><td class='reportCol3' style='padding-top:0px'>" + labelValue + "</td></tr>");
    }

    /// <summary>
    /// Create Report Filter Blank Row
    /// </summary>
    /// <param name="rowIndex">Pass Row Index</param>
    /// <returns></returns>
    private static string CreateFilterBlankRow(ref int rowIndex)
    {
        rowIndex += 1;
        return (" <tr><td class='reportCol2 titlelable'> &nbsp; </td><td class='reportCol3' style='padding-top:0px'>&nbsp;</td></tr>");
    }

    /// <summary>
    /// Create Web Service to get Report Summary Detial
    /// </summary>
    /// <param name="reportID"></param>
    /// <returns></returns>
    //[WebMethod ]
    [System.Web.Services.WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetReportSummary(int reportID)
    {
        return GetReportSummaryDetails(reportID);
    }
}