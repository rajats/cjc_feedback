﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;

public partial class ReportDateDashBoard : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ReportOption != "")
        {
            AuthorizedToViewPage.RedirectNotAuthorized(CurrentEmployee.EmpID, (int)RoleAction.Admin_Reporting);
        }

        if (!IsPostBack)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("dvBreadCrumbs");
            currdiv.InnerHtml = Breadcrumbs.BreadcrumbsAdd(Resources.Resource.lblWhen, Path.GetFileName(Request.Url.AbsolutePath), Request.Url.PathAndQuery, (Convert.ToString(Request.UrlReferrer) == "" ? "" : BusinessUtility.GetString(Request.UrlReferrer.PathAndQuery)));

            hrfCurrentMonth.HRef = "ReportFilterSummary.aspx?roption=" + this.ReportOption + "&searchby=" + BusinessUtility.GetString(ReportDateFilterOption.CurrentMonth);
            hrfCurrentQuarter.HRef = "ReportFilterSummary.aspx?roption=" + this.ReportOption + "&searchby=" + BusinessUtility.GetString(ReportDateFilterOption.CurrentQuarter);
            hrfCalendarYearToDate.HRef = "ReportFilterSummary.aspx?roption=" + this.ReportOption + "&searchby=" + BusinessUtility.GetString(ReportDateFilterOption.CurrentYearToDate);
            hrfSpecificDates.HRef = "ReportSpecficDate.aspx?roption=" + this.ReportOption + "&searchby=" + BusinessUtility.GetString(ReportDateFilterOption.SpecficDate);
        }
    }

    /// <summary>
    /// To Get Report Option
    /// </summary>
    public string ReportOption
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["roption"]);
        }
    }
}