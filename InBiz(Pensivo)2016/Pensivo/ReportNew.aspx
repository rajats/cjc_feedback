﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ReportNew.aspx.cs" Inherits="ReportNew" %>
<%@ Import Namespace ="iTECH.Pensivo.BusinessLogic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box">
                    <a href="#" class="boxset-box-wrapper" id="hrfWho" onclick="selectdiv('hrfWho');" value="<%=ReportOption.Who %>">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblWho%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" id="dvWhich" runat="server">
                    <a href="#" class="boxset-box-wrapper" id="hrfWhich" onclick="selectdiv('hrfWhich');" value="<%=ReportOption.Which %>">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblWhich%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" id="dvWhen" runat="server" visible="false">
                    <a href="#" class="boxset-box-wrapper" id="hrfWhen" onclick="selectdiv('hrfWhen');" value="<%=ReportOption.When %>">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblWhen%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a href="#" class="boxset-box-wrapper" id="hrfWhat" onclick="selectdiv('hrfWhat');" value="<%=ReportOption.What %>">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblWhatProgress%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Get User Selected Report option and Move to NextPage
        function selectdiv( dvID )
        {
            $( '.boxset-box-wrapper' ).each( function ()
            {
                if ( $( this ).attr( "id" ) == dvID )
                {
                    $( this ).addClass( "is-selected" );
                    moveNext();
                }
                else
                {
                    $( this ).removeClass( "is-selected" );
                }
            } );
        }

        // To Move Next Page user selected Report Option
        function moveNext()
        {
            if ( isOptionSelected() == true )
            {
                var vSelectedValue = '';
                $( '.is-selected' ).each( function ()
                {
                    vSelectedValue = $( this ).attr( "value" );
                } );

                if (vSelectedValue == '<%= ReportOption.Who%>') {
                    window.location.href = 'EmpSearchDashBoard.aspx?roption=' + vSelectedValue;
                }
                else if (vSelectedValue == '<%= ReportOption.Which%>') {
                    window.location.href = 'ReportEventDashBoard.aspx?roption=' + vSelectedValue;
                }
                else if (vSelectedValue == '<%= ReportOption.What%>') {
                    window.location.href = 'ReportTrainingProgress.aspx?roption=' + vSelectedValue;
                }
                else if (vSelectedValue == '<%= ReportOption.When%>') {
                    window.location.href = 'ReportDateDashBoard.aspx?roption=' + vSelectedValue;
                }
            }
            else
            {
                ShowPensivoMessage( '<%=Resources.Resource.errMsgSelectOption%>' );
            }
        }

        // To Get Report Option Selected or not
        function isOptionSelected()
        {
            var i = 0;
            $( '.is-selected' ).each( function ()
            {
                i = 1;
            } )

            if ( i == 0 )
            {
                return false
            }
            else
            {
                return true;
            }
        }

    </script>

</asp:Content>

