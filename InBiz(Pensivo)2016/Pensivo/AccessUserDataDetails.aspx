﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AccessUserDataDetails.aspx.cs" Inherits="AccessUserDataDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvTypeRegDistDiv" runat="server">
                <div class="boxset-box">
                    <a id="hrfOnlyCanSeeSameInfo" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title" id="hTitle" runat="server"></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <asp:HiddenField ID="hdnRoleID" runat="server" />
    <asp:HiddenField ID="HdnSearchValue" runat="server" />
    <asp:HiddenField ID="HdnSearchBy" runat="server" />
    <asp:HiddenField ID="HdnFlag" runat="server" />
    <asp:HiddenField ID="HdnActionID" runat="server" />
    <asp:HiddenField ID="HdnFunctionalityType" runat="server" />
    <asp:HiddenField ID="hdnMessage" runat="server" />

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Allow All Site Detail Data Confirmation
        function AllowAllSiteData() {
            var title = "<%=Resources.Resource.lblConfirmation%>";
            var messageText = ($("#<%=hdnMessage.ClientID%>").val());
            okButtonText = "OK";
            LaterCnclButtonText = "<%=Resources.Resource.btnCancel%>";
            okButtonRedirectlink = "AddRole();";
            LaterCnclButtonRedirectLink = " HideConfirmationDialog();";
            PensivoConfirmMessage(title, messageText, okButtonText, LaterCnclButtonText, okButtonRedirectlink, LaterCnclButtonRedirectLink, LaterCnclButtonRedirectLink);

        }

        // To Trigger Allow All Site Detail Data Confirmation OK Button
        function AddRole() {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                datatoPost.callBack = "addAction";
            }
            else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                datatoPost.callBack = "removeAction";
            }
            datatoPost.SearchBy = $("#<%=HdnSearchBy.ClientID%>").val();
            datatoPost.SearchValue = $("#<%=HdnSearchValue.ClientID%>").val();
            datatoPost.RoleID = $("#<%=hdnRoleID.ClientID%>").val();
            datatoPost.ActionID = $("#<%=HdnActionID.ClientID%>").val();
            datatoPost.FunctionalityType = $("#<%=HdnFunctionalityType.ClientID%>").val();
            $.post("AccessUserDataDetails.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                        window.location.href = "roleActivity.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val();
                    }
                    else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                        window.location.href = "roleActivity.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val();
                    }
            }
            else {
                if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                        ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueNotAddedToRole%>")
                }
                else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                    ShowPensivoMessage("<%=Resources.Resource.msgSystemRefValueNoRemovedToRole%>")
                }
        }
            });
}
    </script>
</asp:Content>

