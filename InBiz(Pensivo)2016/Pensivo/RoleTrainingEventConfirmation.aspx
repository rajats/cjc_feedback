﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleTrainingEventConfirmation.aspx.cs" Inherits="RoleTrainingEventConfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <h4>
                <asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h4>
<%--            <h5>
                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, lblSummary %>"></asp:Literal>
                : </h5>--%>
            <h5>
                <asp:Literal ID="ltrSubSummary" runat="server"></asp:Literal></h5>
            <br />
            <br />
            <h6>
                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, lblIsItCorrect %>"></asp:Literal></h6>
            <div class="btngrp " style="float:right">
                <a id="hrefYes" runat="server" href="#nogo" class="btn " onclick="AddRole();"><%=Resources.Resource.lblYes %></a>
                <a id="hrefNo" runat="server" href="#nogo" class="btn "><%=Resources.Resource.lblNo %></a>
            </div>
        </div>

        <asp:HiddenField ID="hdnRoleID" runat="server" />
        <asp:HiddenField ID="HdnSearchValue" runat="server" />
        <asp:HiddenField ID="HdnSearchBy" runat="server" />
        <asp:HiddenField ID="HdnFlag" runat="server" />
        <asp:HiddenField ID="HdnActionID" runat="server" />
        <asp:HiddenField ID="HdnFunctionalityType" runat="server" />
        <asp:HiddenField ID="HdnCreatedBy" runat="server" />
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Add/Remove Role Training Event
        function AddRole() {
            var datatoPost = {};
            datatoPost.isAjaxCall = 1;
            if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                datatoPost.callBack = "addAction";
            }
            else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                datatoPost.callBack = "removeAction";
            }
            datatoPost.SearchBy = $("#<%=HdnSearchBy.ClientID%>").val();
            datatoPost.SearchValue = $("#<%=HdnSearchValue.ClientID%>").val();
            datatoPost.RoleID = $("#<%=hdnRoleID.ClientID%>").val();
            datatoPost.ActionID = $("#<%=HdnActionID.ClientID%>").val();
            datatoPost.FunctionalityType = $("#<%=HdnFunctionalityType.ClientID%>").val();
            datatoPost.createdBy = $("#<%=HdnCreatedBy.ClientID%>").val();
            $.post("RoleTrainingEventConfirmation.aspx", datatoPost, function (data) {
                if (data == "ok") {
                    if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                        ShowPensivoMessage("<%=Resources.Resource.lblTrainingEventAddedtoRole%>", "roleActivity.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val() + "");
                    }
                    else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                        ShowPensivoMessage("<%=Resources.Resource.lblTrainingEventRemovedtoRole%>", "roleActivity.aspx?roleID=" + $("#<%=hdnRoleID.ClientID%>").val() + "");
                    }
            }
            else {
                if ($("#<%=HdnFlag.ClientID%>").val() == 'add') {
                        ShowPensivoMessage("<%=Resources.Resource.msgTrainingEventNotAddedToRole%>")
                }
                else if ($("#<%=HdnFlag.ClientID%>").val() == 'remove') {
                    ShowPensivoMessage("<%=Resources.Resource.msgTrainingNotRemovedToRole%>")
                    }
            }
            });
    }
    </script>

</asp:Content>




