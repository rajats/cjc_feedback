﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="AccessReportingDataDashboard.aspx.cs" Inherits="AccessReportingDataDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <%=htmlText %>
        </div>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Show Message 
        function ShowMessageNotAvailable() {
            ShowPensivoMessage("<%=Resources.Resource.lblNotYetAvailable %>");
        }
    </script>
</asp:Content>



