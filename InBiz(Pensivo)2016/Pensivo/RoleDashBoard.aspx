﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="RoleDashBoard.aspx.cs" Inherits="Roles_RoleDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box">
                    <a href="RoleAdd.aspx" class="boxset-box-wrapper "><%--is-selected--%>
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblCreateSystemRole%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a href="RoleEdit.aspx" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblEditSystemRole%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a href="ViewSystemRoleDetails.aspx" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblViewSystemRoleDetails%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

