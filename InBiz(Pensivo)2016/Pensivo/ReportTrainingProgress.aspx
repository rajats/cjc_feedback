﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="ReportTrainingProgress.aspx.cs" Inherits="ReportTrainingProgress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <div class="wrapper">
            <div class="boxset" id="dvAdminDashBoard">
                <div class="boxset-box">
                    <a id="hrfAllTrainingProgress" runat="server" href="#" class="boxset-box-wrapper ">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblAllCourseProgress%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfTrainingCompleteOnce" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTrainingCompleteOnce%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box">
                    <a id="hrfTrainingNotCompletedOnce" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTrainingNotCompletedOnce%></h4>
                        </div>
                    </a>
                </div>
                <div class="boxset-box" style="display: none;">
                    <a id="hrfNotStartedAtOnce" runat="server" href="#" class="boxset-box-wrapper">
                        <div class="boxset-box-inner">
                            <h4 class="boxset-title"><%= Resources.Resource.lblTrainingNotStartedAtOnce%></h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
