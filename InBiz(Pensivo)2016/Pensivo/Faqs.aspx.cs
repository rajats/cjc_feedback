﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;

public partial class Faqs : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) || (Utils.TrainingInst == (int)Institute.EDE2))
        {
            pPortalMessage.Visible = false;
        }


        if (Utils.TrainingInst == (int)Institute.navcanada || Utils.TrainingInst == (int)Institute.AlMurrayDentistry || Utils.TrainingInst == (int)Institute.EDE2)
        {
            liIETBS.Visible = false;
            liIENNAV.Visible = true;
        }
    }
}