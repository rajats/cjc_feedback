﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-404">
        <h1 class="h3"><%=Resources.Resource.lblSorryPageNotAvailable %></h1>
        <p class="hero"><%=Resources.Resource.lblLinkNotAvailableOrMybeBroken %></p>
        <ul>
            <li>
                <a href="javascript:void(0)" onclick="goBack();"><%=Resources.Resource.lblGoBackToPreviousPage %></a>
            </li>
            <li><a href="public.aspx"><%=Resources.Resource.lblGoToHomePage %></a></li>
        </ul>
    </section>

    <%--Define JavaScript Function--%>
    <script type="text/ecmascript">
        // To Define Document Ready Function
        $( document ).ready( function ()
        {
            $( "#dvMainWrapper" ).addClass( " width-med" );
        } );

        // To Define go back
        function goBack()
        {
            window.history.back()
        }
    </script>
</asp:Content>
