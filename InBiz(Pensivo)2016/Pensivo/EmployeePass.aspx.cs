﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Pensivo.BusinessLogic;
using iTECH.Library.Utilities;

public partial class EmployeePass : BasePage
{
    /// <summary>
    /// Set Page Load Setting
    /// </summary>
    /// <param name="sender">Pass Sender Object</param>
    /// <param name="e">Pass Sender Event</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry)  )
        {
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblEmpEmailID);
            lblUserName.InnerHtml = Resources.Resource.lblEmpEmailID;
            pTooltipUserName.InnerHtml = Resources.Resource.lblEmpEmailID;
        }
        else if (Utils.TrainingInst == (int)Institute.EDE2)
        {
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblEDE2UserName);
            lblUserName.InnerHtml = Resources.Resource.lblEDE2UserName;
            pTooltipUserName.InnerHtml = Resources.Resource.lblEDE2UserName;
            valEmpIDExp.Text = Resources.Resource.lblEDE2InvalidLogInIDFormat;
        }
        else if (Utils.TrainingInst == (int)Institute.bdl)
        {
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblBDLUserName);
            lblUserName.InnerHtml = Resources.Resource.lblBDLUserName;
            pTooltipUserName.InnerHtml = Resources.Resource.lblBDLUserName;
            valEmpIDExp.Text = Resources.Resource.lblBdlInvalidLogInIDFormat;
        }
        else
        {
            txtUserName.Attributes.Add("placeholder", Resources.Resource.lblUserName);
            lblUserName.InnerHtml = Resources.Resource.lblUserName;
            pTooltipUserName.InnerHtml = Resources.Resource.lblUserName;
        }

        txtUserName.Text = BusinessUtility.GetString(Request.QueryString["EMPID"]);

        
    }

    /// <summary>
    /// To Save User Details and move to next page
    /// </summary>
    /// <param name="sender">Pass Sender object</param>
    /// <param name="e">Pass Event Args</param>
    protected void btnPass_OnClick(object sender, EventArgs e)
    {
        if (Utils.TrainingInst == (int)Institute.beercollege)
        {
            if (this.EMPID == txtNewPassword.Text)
            {
                GlobalMessage.showAlertMessageWithFocus(Resources.Resource.lblCannotUserUserIDasPassword, txtNewPassword.ClientID);
                txtNewPassword.Focus();
                return;
            }
            Response.Redirect("EmployeeSiteDetail.aspx" + Request.Url.Query + "&PP=" + BusinessUtility.GetString(txtNewPassword.Text) + "&searchby=" + EmpSearchBy.Division + "");
        }
        else if (Utils.TrainingInst == (int)Institute.bdl || Utils.TrainingInst == (int)Institute.tdc || Utils.TrainingInst == (int)Institute.navcanada || Utils.TrainingInst == (int)Institute.AlMurrayDentistry || Utils.TrainingInst == (int)Institute.EDE2)
        {

            if (this.EMPID == txtNewPassword.Text)
            {
                GlobalMessage.showAlertMessageWithFocus(Resources.Resource.lblCannotUserUserIDasPassword, txtNewPassword.ClientID);
                txtNewPassword.Focus();
                return;
            }

            Employee objEmp = new Employee();
            objEmp.EmpLogInID = this.EMPID;
            objEmp.EmpPassword = BusinessUtility.GetString(txtNewPassword.Text);
            objEmp.PensivoUUID = this.UUID;
            objEmp.CreatedSource = this.Src;
            objEmp.EmpFirstName = this.Fname;
            objEmp.EmpLastName = this.Lname;

            string sJobCodeValue = "";
            sJobCodeValue = "99997";

            if (Utils.TrainingInst == (int)Institute.bdl || Utils.TrainingInst == (int)Institute.beercollege)
            {
                if (string.IsNullOrEmpty(this.UUID))
                {
                    objEmp.PensivoUUID = PensivoExecutions.PensivoPostUserData(Convert.ToString(this.EMPID), Convert.ToString(this.Pass), Convert.ToString(this.Fname), Convert.ToString(this.Lname), "UNION", this.searchbyvalue, "", sJobCodeValue);
                }
                else
                {
                    objEmp.PensivoUUID = this.UUID;
                }


                if (
                    (objEmp.InsertEmployeeShortDesc() == true) &&
                    (objEmp.UpdateEmployeeFirstLastName(BusinessUtility.GetInt(objEmp.EmpID)) == true) &&
                    (objEmp.InsertEmployeeAnswer(BusinessUtility.GetInt(objEmp.EmpID), BusinessUtility.GetInt(this.QID), this.QAns) == true) &&
                    (objEmp.InsertEmployeeAnswer(BusinessUtility.GetInt(objEmp.EmpID), BusinessUtility.GetInt(this.QOtherID), this.QOtherAns) == true) &&
                    (objEmp.InsertEmployeeSysRef(BusinessUtility.GetInt(objEmp.EmpID), this.searchby, this.searchbyvalue) == true) &&
                    (objEmp.InsertEmployeeSysRef(BusinessUtility.GetInt(objEmp.EmpID), EmpRefCode.JobCode, sJobCodeValue) == true)
                    )
                {
                    RoleManageLists objRoleList = new RoleManageLists();
                    objRoleList.UpdateEmployeeHasFunctionalityStatus(BusinessUtility.GetInt(objEmp.EmpID), "");

                    EmpAssignedCourses objAssignedCourse = new EmpAssignedCourses();
                    objAssignedCourse.ResetEmployeeAssignedCoursesByEmpID(BusinessUtility.GetInt(objEmp.EmpID), 0, (int)AssignedCourseInActiveReason.ByUserProfileChange , (int)AssignedCourseUpdatedSource.ByUserProfileChange);

                    EmployeeValidation objEmpValidate = new EmployeeValidation();
                    if (objEmpValidate.ValidateEmployee(this.EMPID, BusinessUtility.GetString(txtNewPassword.Text)) == true)
                    {
                        PensivoExecutions.PensivoPostData(this.Fname, this.Lname, this.Pass, "", BusinessUtility.GetInt(this.UUID));
                        Response.Redirect("employeedashboard.aspx");
                    }
                }
                else
                {
                    showAlertMessage();
                }
            }
            else if ((Utils.TrainingInst == (int)Institute.tdc) || (Utils.TrainingInst == (int)Institute.navcanada) || (Utils.TrainingInst == (int)Institute.AlMurrayDentistry) || (Utils.TrainingInst == (int)Institute.EDE2))
            {
                if (
                (objEmp.InsertEmployeeShortDesc() == true) &&
                (objEmp.UpdateEmployeeFirstLastName(BusinessUtility.GetInt(objEmp.EmpID)) == true) &&
                (objEmp.InsertEmployeeAnswer(BusinessUtility.GetInt(objEmp.EmpID), BusinessUtility.GetInt(this.QID), this.QAns) == true) &&
                (objEmp.InsertEmployeeAnswer(BusinessUtility.GetInt(objEmp.EmpID), BusinessUtility.GetInt(this.QOtherID), this.QOtherAns) == true)
                )
                {
                    EmployeeValidation objEmpValidate = new EmployeeValidation();
                    if (objEmpValidate.ValidateEmployee(this.EMPID, BusinessUtility.GetString(txtNewPassword.Text)) == true)
                    {
                        PensivoExecutions.PensivoPostData(this.Fname, this.Lname, this.Pass, "", BusinessUtility.GetInt(this.UUID));
                        Response.Redirect("employeedashboard.aspx");
                    }
                }
                else
                {
                    showAlertMessage();
                }
            }
        }
    }

    /// <summary>
    /// To Get Employee ID
    /// </summary>
    public string EMPID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["EMPID"]);
        }
    }

    /// <summary>
    /// To Get Pensivo UUID
    /// </summary>
    public string UUID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["UUID"]);
        }
    }

    /// <summary>
    /// To Get Pensivo User Created Source
    /// </summary>
    public string Src
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["src"]);
        }
    }

    /// <summary>
    /// To Get Employee First Name
    /// </summary>
    public string Fname
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["Fname"]);
        }
    }

    /// <summary>
    /// To Get Employee Last Name 
    /// </summary>
    public string Lname
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["Lname"]);
        }
    }

    /// <summary>
    /// To Get Question ID
    /// </summary>
    public int QID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["QID"]);
        }
    }

    /// <summary>
    /// To Get Question Answer
    /// </summary>
    public string QAns
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["QAns"]);
        }
    }

    /// <summary>
    /// To Get Other Question ID 
    /// </summary>
    public int QOtherID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["QOtherID"]);
        }
    }

    /// <summary>
    /// To Get Other Question Answer
    /// </summary>
    public string QOtherAns
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["QOtherAns"]);
        }
    }

    /// <summary>
    /// To Get Search By
    /// </summary>
    public string searchby
    {
        get
        {
            return "DIV";
        }
    }

    /// <summary>
    /// To Get Search By Value
    /// </summary>
    public string searchbyvalue
    {
        get
        {
            return "LOGS";
        }
    }

    /// <summary>
    /// To Get Pass
    /// </summary>
    public string Pass
    {
        get
        {
            return BusinessUtility.GetString(txtNewPassword.Text);
        }
    }

    /// <summary>
    /// To Show Alert Message
    /// </summary>
    private void showAlertMessage()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowPensivoMessage('" + Resources.Resource.lblPleaseContactAdministrator + "');", true);
    }

}