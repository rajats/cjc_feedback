﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomePage.master" AutoEventWireup="true" CodeFile="EmployeeQuestionOther.aspx.cs" Inherits="EmployeeQuestionOther" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="main-content" class="pg-dashboard">
        <asp:Panel ID="cntPannel" runat="server" DefaultButton="btnNext">
            <div class="wrapper width-med">
                <h1><%= Resources.Resource.lblCreateNewAccount%>  </h1>
                <div class="boxed-content">
                    <div class="boxed-content-body">
                        <header class="form-section-header">
                            <h6>
                                <asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:Resource, lblSecretQuestion1 %>"></asp:Label></h6>
                            <p></p>
                        </header>
                        <section class="form-section">
                            <div class="plms-fieldset is-first">
                                <label class="plms-label is-hidden" for="last-name"><%= Resources.Resource.lblSecretQuestion1%></label>
                                <div class="plms-tooltip-parent">
                                    <asp:DropDownList ID="ddlQuestion" runat="server" class="plms-select skin2 is-placeholder ">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfJobCode" runat="server" class="formels-feedback invalid" Display="Dynamic" ValidationGroup="grpRole"
                                        ControlToValidate="ddlQuestion" Text="<%$ Resources:Resource, reqMsgSecretQuestion %>" />
                                    <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                        <div class="plms-tooltip-body">
                                            <p><%= Resources.Resource.lblSecretQuestion1%></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="plms-fieldset">
                                <label class="plms-label is-hidden" for="answer"><%= Resources.Resource.lblAnswer%></label>
                                <div class="plms-tooltip-parent">
                                    <asp:TextBox ID="txtAnswer" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblAnswer %>" />
                                    <asp:RequiredFieldValidator ID="rfAnswer" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic"
                                        ControlToValidate="txtAnswer" Text="<%$ Resources:Resource, reqMsgRequiredAnswer %>" />
                                    <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                        <div class="plms-tooltip-body">
                                            <p>
                                                <asp:Label ID="lblAnswer" runat="server" Text="<%$ Resources:Resource, lblAnswer %>"></asp:Label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="plms-fieldset">
                                <label class="plms-label is-hidden" for="confirm-answer"><%= Resources.Resource.lblConfirmAnswer%></label>
                                <div class="plms-tooltip-parent">
                                    <asp:TextBox ID="txtConfirmAnswer" runat="server" class="plms-input skin2" placeholder="<%$ Resources:Resource, lblConfirmAnswer %>" />
                                    <asp:RequiredFieldValidator ID="rfConfirmAnswer" runat="server" class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic"
                                        ControlToValidate="txtConfirmAnswer" Text="<%$ Resources:Resource, reqMsgRequiredConfirmAnswer %>" />
                                    <asp:CompareValidator ID="cmpValidator" ControlToValidate="txtConfirmAnswer" ControlToCompare="txtAnswer" Type="String"
                                        Operator="Equal" Text="<%$ Resources:Resource, reqMsgAnswerNotMatch %>" runat="Server"
                                        class="formels-feedback invalid" ValidationGroup="grpRole" Display="Dynamic" />
                                    <div class="plms-tooltip plms-tooltip-micro active left-top outside autow label-replacement">
                                        <div class="plms-tooltip-body">
                                            <p>
                                                <asp:Label ID="lblLastName" runat="server" Text="<%$ Resources:Resource, lblConfirmAnswer %>"></asp:Label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <footer class="form-footer">
                            <asp:Button ID="btnNext" class="btn" runat="server" Text="<%$ Resources:Resource, btnNext %>" ValidationGroup="grpRole" OnClientClick="return BtnClick();" OnClick="btnNext_OnClick" />
                        </footer>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </section>
    <%--Define JavaScript Function--%>
    <script type="text/javascript">
        // To Show Custom Error Message for each Control
        function BtnClick() {
            var val = Page_ClientValidate();
            if (!val) {
                var i = 0;
                for (; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $("#" + Page_Validators[i].controltovalidate).addClass("has-error");
                        break;
                    }
                    else {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            else {
                var i = 0;
                for (; i < Page_Validators.length; i++) {

                    {
                        $("#" + Page_Validators[i].controltovalidate).removeClass("has-error");
                    }
                }
            }
            return val;
        }
    </script>
</asp:Content>

